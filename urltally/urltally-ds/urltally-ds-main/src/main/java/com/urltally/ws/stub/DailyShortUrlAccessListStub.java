package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "dailyShortUrlAccesses")
@XmlType(propOrder = {"dailyShortUrlAccess", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DailyShortUrlAccessListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DailyShortUrlAccessListStub.class.getName());

    private List<DailyShortUrlAccessStub> dailyShortUrlAccesses = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public DailyShortUrlAccessListStub()
    {
        this(new ArrayList<DailyShortUrlAccessStub>());
    }
    public DailyShortUrlAccessListStub(List<DailyShortUrlAccessStub> dailyShortUrlAccesses)
    {
        this(dailyShortUrlAccesses, null);
    }
    public DailyShortUrlAccessListStub(List<DailyShortUrlAccessStub> dailyShortUrlAccesses, String forwardCursor)
    {
        this.dailyShortUrlAccesses = dailyShortUrlAccesses;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(dailyShortUrlAccesses == null) {
            return true;
        } else {
            return dailyShortUrlAccesses.isEmpty();
        }
    }
    public int getSize()
    {
        if(dailyShortUrlAccesses == null) {
            return 0;
        } else {
            return dailyShortUrlAccesses.size();
        }
    }


    @XmlElement(name = "dailyShortUrlAccess")
    public List<DailyShortUrlAccessStub> getDailyShortUrlAccess()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<DailyShortUrlAccessStub> getList()
    {
        return dailyShortUrlAccesses;
    }
    public void setList(List<DailyShortUrlAccessStub> dailyShortUrlAccesses)
    {
        this.dailyShortUrlAccesses = dailyShortUrlAccesses;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<DailyShortUrlAccessStub> it = this.dailyShortUrlAccesses.iterator();
        while(it.hasNext()) {
            DailyShortUrlAccessStub dailyShortUrlAccess = it.next();
            sb.append(dailyShortUrlAccess.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static DailyShortUrlAccessListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of DailyShortUrlAccessListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write DailyShortUrlAccessListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write DailyShortUrlAccessListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write DailyShortUrlAccessListStub object as a string.", e);
        }
        
        return null;
    }
    public static DailyShortUrlAccessListStub fromJsonString(String jsonStr)
    {
        try {
            DailyShortUrlAccessListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, DailyShortUrlAccessListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into DailyShortUrlAccessListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into DailyShortUrlAccessListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into DailyShortUrlAccessListStub object.", e);
        }
        
        return null;
    }

}
