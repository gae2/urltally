package com.urltally.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.ws.bean.TotalLongUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.TotalLongUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.TotalLongUrlAccessService;


// TotalLongUrlAccessMockService is a decorator.
// It can be used as a base class to mock TotalLongUrlAccessService objects.
public abstract class TotalLongUrlAccessMockService implements TotalLongUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessMockService.class.getName());

    // TotalLongUrlAccessMockService uses the decorator design pattern.
    private TotalLongUrlAccessService decoratedService;

    public TotalLongUrlAccessMockService(TotalLongUrlAccessService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TotalLongUrlAccessService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TotalLongUrlAccessService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TotalLongUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TotalLongUrlAccess getTotalLongUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTotalLongUrlAccess(): guid = " + guid);
        TotalLongUrlAccess bean = decoratedService.getTotalLongUrlAccess(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTotalLongUrlAccess(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTotalLongUrlAccess(guid, field);
        return obj;
    }

    @Override
    public List<TotalLongUrlAccess> getTotalLongUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getTotalLongUrlAccesses()");
        List<TotalLongUrlAccess> totalLongUrlAccesses = decoratedService.getTotalLongUrlAccesses(guids);
        log.finer("END");
        return totalLongUrlAccesses;
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses() throws BaseException
    {
        return getAllTotalLongUrlAccesses(null, null, null);
    }


    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalLongUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalLongUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TotalLongUrlAccess> totalLongUrlAccesses = decoratedService.getAllTotalLongUrlAccesses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return totalLongUrlAccesses;
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalLongUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalLongUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTotalLongUrlAccessKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalLongUrlAccessMockService.findTotalLongUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TotalLongUrlAccess> totalLongUrlAccesses = decoratedService.findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return totalLongUrlAccesses;
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalLongUrlAccessMockService.findTotalLongUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalLongUrlAccessMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTotalLongUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {
        return decoratedService.createTotalLongUrlAccess(tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
    }

    @Override
    public String createTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTotalLongUrlAccess(totalLongUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTotalLongUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {
        return decoratedService.updateTotalLongUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
    }
        
    @Override
    public Boolean updateTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTotalLongUrlAccess(totalLongUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteTotalLongUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTotalLongUrlAccess(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTotalLongUrlAccess(totalLongUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTotalLongUrlAccesses(filter, params, values);
        return count;
    }

}
