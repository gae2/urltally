package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "totalLongUrlAccesses")
@XmlType(propOrder = {"totalLongUrlAccess", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TotalLongUrlAccessListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessListStub.class.getName());

    private List<TotalLongUrlAccessStub> totalLongUrlAccesses = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public TotalLongUrlAccessListStub()
    {
        this(new ArrayList<TotalLongUrlAccessStub>());
    }
    public TotalLongUrlAccessListStub(List<TotalLongUrlAccessStub> totalLongUrlAccesses)
    {
        this(totalLongUrlAccesses, null);
    }
    public TotalLongUrlAccessListStub(List<TotalLongUrlAccessStub> totalLongUrlAccesses, String forwardCursor)
    {
        this.totalLongUrlAccesses = totalLongUrlAccesses;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(totalLongUrlAccesses == null) {
            return true;
        } else {
            return totalLongUrlAccesses.isEmpty();
        }
    }
    public int getSize()
    {
        if(totalLongUrlAccesses == null) {
            return 0;
        } else {
            return totalLongUrlAccesses.size();
        }
    }


    @XmlElement(name = "totalLongUrlAccess")
    public List<TotalLongUrlAccessStub> getTotalLongUrlAccess()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TotalLongUrlAccessStub> getList()
    {
        return totalLongUrlAccesses;
    }
    public void setList(List<TotalLongUrlAccessStub> totalLongUrlAccesses)
    {
        this.totalLongUrlAccesses = totalLongUrlAccesses;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<TotalLongUrlAccessStub> it = this.totalLongUrlAccesses.iterator();
        while(it.hasNext()) {
            TotalLongUrlAccessStub totalLongUrlAccess = it.next();
            sb.append(totalLongUrlAccess.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TotalLongUrlAccessListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TotalLongUrlAccessListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TotalLongUrlAccessListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TotalLongUrlAccessListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TotalLongUrlAccessListStub object as a string.", e);
        }
        
        return null;
    }
    public static TotalLongUrlAccessListStub fromJsonString(String jsonStr)
    {
        try {
            TotalLongUrlAccessListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TotalLongUrlAccessListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TotalLongUrlAccessListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TotalLongUrlAccessListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TotalLongUrlAccessListStub object.", e);
        }
        
        return null;
    }

}
