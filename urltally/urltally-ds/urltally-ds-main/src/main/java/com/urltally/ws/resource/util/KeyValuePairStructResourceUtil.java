package com.urltally.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.KeyValuePairStruct;
import com.urltally.ws.bean.KeyValuePairStructBean;
import com.urltally.ws.stub.KeyValuePairStructStub;


public class KeyValuePairStructResourceUtil
{
    private static final Logger log = Logger.getLogger(KeyValuePairStructResourceUtil.class.getName());

    // Static methods only.
    private KeyValuePairStructResourceUtil() {}

    public static KeyValuePairStructBean convertKeyValuePairStructStubToBean(KeyValuePairStruct stub)
    {
        KeyValuePairStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null KeyValuePairStructBean is returned.");
        } else {
            bean = new KeyValuePairStructBean();
            bean.setUuid(stub.getUuid());
            bean.setKey(stub.getKey());
            bean.setValue(stub.getValue());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
