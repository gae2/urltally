package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "weeklyShortUrlAccesses")
@XmlType(propOrder = {"weeklyShortUrlAccess", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeeklyShortUrlAccessListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(WeeklyShortUrlAccessListStub.class.getName());

    private List<WeeklyShortUrlAccessStub> weeklyShortUrlAccesses = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public WeeklyShortUrlAccessListStub()
    {
        this(new ArrayList<WeeklyShortUrlAccessStub>());
    }
    public WeeklyShortUrlAccessListStub(List<WeeklyShortUrlAccessStub> weeklyShortUrlAccesses)
    {
        this(weeklyShortUrlAccesses, null);
    }
    public WeeklyShortUrlAccessListStub(List<WeeklyShortUrlAccessStub> weeklyShortUrlAccesses, String forwardCursor)
    {
        this.weeklyShortUrlAccesses = weeklyShortUrlAccesses;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(weeklyShortUrlAccesses == null) {
            return true;
        } else {
            return weeklyShortUrlAccesses.isEmpty();
        }
    }
    public int getSize()
    {
        if(weeklyShortUrlAccesses == null) {
            return 0;
        } else {
            return weeklyShortUrlAccesses.size();
        }
    }


    @XmlElement(name = "weeklyShortUrlAccess")
    public List<WeeklyShortUrlAccessStub> getWeeklyShortUrlAccess()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<WeeklyShortUrlAccessStub> getList()
    {
        return weeklyShortUrlAccesses;
    }
    public void setList(List<WeeklyShortUrlAccessStub> weeklyShortUrlAccesses)
    {
        this.weeklyShortUrlAccesses = weeklyShortUrlAccesses;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<WeeklyShortUrlAccessStub> it = this.weeklyShortUrlAccesses.iterator();
        while(it.hasNext()) {
            WeeklyShortUrlAccessStub weeklyShortUrlAccess = it.next();
            sb.append(weeklyShortUrlAccess.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static WeeklyShortUrlAccessListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of WeeklyShortUrlAccessListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write WeeklyShortUrlAccessListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write WeeklyShortUrlAccessListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write WeeklyShortUrlAccessListStub object as a string.", e);
        }
        
        return null;
    }
    public static WeeklyShortUrlAccessListStub fromJsonString(String jsonStr)
    {
        try {
            WeeklyShortUrlAccessListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, WeeklyShortUrlAccessListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into WeeklyShortUrlAccessListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into WeeklyShortUrlAccessListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into WeeklyShortUrlAccessListStub object.", e);
        }
        
        return null;
    }

}
