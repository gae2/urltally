package com.urltally.ws.dao;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.data.DailyShortUrlAccessDataObject;


// TBD: Add offset/count to getAllDailyShortUrlAccesses() and findDailyShortUrlAccesses(), etc.
public interface DailyShortUrlAccessDAO
{
    DailyShortUrlAccessDataObject getDailyShortUrlAccess(String guid) throws BaseException;
    List<DailyShortUrlAccessDataObject> getDailyShortUrlAccesses(List<String> guids) throws BaseException;
    List<DailyShortUrlAccessDataObject> getAllDailyShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<DailyShortUrlAccessDataObject> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<DailyShortUrlAccessDataObject> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<DailyShortUrlAccessDataObject> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<DailyShortUrlAccessDataObject> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<DailyShortUrlAccessDataObject> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<DailyShortUrlAccessDataObject> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createDailyShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return DailyShortUrlAccessDataObject?)
    String createDailyShortUrlAccess(DailyShortUrlAccessDataObject dailyShortUrlAccess) throws BaseException;          // Returns Guid.  (Return DailyShortUrlAccessDataObject?)
    //Boolean updateDailyShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateDailyShortUrlAccess(DailyShortUrlAccessDataObject dailyShortUrlAccess) throws BaseException;
    Boolean deleteDailyShortUrlAccess(String guid) throws BaseException;
    Boolean deleteDailyShortUrlAccess(DailyShortUrlAccessDataObject dailyShortUrlAccess) throws BaseException;
    Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;
}
