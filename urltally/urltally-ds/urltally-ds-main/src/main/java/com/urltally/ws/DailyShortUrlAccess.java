package com.urltally.ws;



public interface DailyShortUrlAccess extends ShortUrlAccess
{
    Integer  getYear();
    Integer  getDay();
}
