package com.urltally.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class TotalLongUrlAccessBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessBasePermission.class.getName());

    public TotalLongUrlAccessBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "TotalLongUrlAccess::" + action;
    }


}
