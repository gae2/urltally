package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.ws.bean.TotalLongUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.TotalLongUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.TotalLongUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TotalLongUrlAccessServiceImpl implements TotalLongUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // TotalLongUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TotalLongUrlAccess getTotalLongUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TotalLongUrlAccessDataObject dataObj = getDAOFactory().getTotalLongUrlAccessDAO().getTotalLongUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        TotalLongUrlAccessBean bean = new TotalLongUrlAccessBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTotalLongUrlAccess(String guid, String field) throws BaseException
    {
        TotalLongUrlAccessDataObject dataObj = getDAOFactory().getTotalLongUrlAccessDAO().getTotalLongUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyType")) {
            return dataObj.getTallyType();
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("count")) {
            return dataObj.getCount();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return dataObj.getLongUrlDomain();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TotalLongUrlAccess> getTotalLongUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TotalLongUrlAccess> list = new ArrayList<TotalLongUrlAccess>();
        List<TotalLongUrlAccessDataObject> dataObjs = getDAOFactory().getTotalLongUrlAccessDAO().getTotalLongUrlAccesses(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccessDataObject list.");
        } else {
            Iterator<TotalLongUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TotalLongUrlAccessDataObject dataObj = (TotalLongUrlAccessDataObject) it.next();
                list.add(new TotalLongUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses() throws BaseException
    {
        return getAllTotalLongUrlAccesses(null, null, null);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalLongUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalLongUrlAccesses(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TotalLongUrlAccess> list = new ArrayList<TotalLongUrlAccess>();
        List<TotalLongUrlAccessDataObject> dataObjs = getDAOFactory().getTotalLongUrlAccessDAO().getAllTotalLongUrlAccesses(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccessDataObject list.");
        } else {
            Iterator<TotalLongUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TotalLongUrlAccessDataObject dataObj = (TotalLongUrlAccessDataObject) it.next();
                list.add(new TotalLongUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalLongUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllTotalLongUrlAccessKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getTotalLongUrlAccessDAO().getAllTotalLongUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccess key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TotalLongUrlAccessServiceImpl.findTotalLongUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TotalLongUrlAccess> list = new ArrayList<TotalLongUrlAccess>();
        List<TotalLongUrlAccessDataObject> dataObjs = getDAOFactory().getTotalLongUrlAccessDAO().findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find totalLongUrlAccesses for the given criterion.");
        } else {
            Iterator<TotalLongUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TotalLongUrlAccessDataObject dataObj = (TotalLongUrlAccessDataObject) it.next();
                list.add(new TotalLongUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TotalLongUrlAccessServiceImpl.findTotalLongUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getTotalLongUrlAccessDAO().findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TotalLongUrlAccess keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TotalLongUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getTotalLongUrlAccessDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createTotalLongUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        TotalLongUrlAccessDataObject dataObj = new TotalLongUrlAccessDataObject(null, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
        return createTotalLongUrlAccess(dataObj);
    }

    @Override
    public String createTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalLongUrlAccess cannot be null.....
        if(totalLongUrlAccess == null) {
            log.log(Level.INFO, "Param totalLongUrlAccess is null!");
            throw new BadRequestException("Param totalLongUrlAccess object is null!");
        }
        TotalLongUrlAccessDataObject dataObj = null;
        if(totalLongUrlAccess instanceof TotalLongUrlAccessDataObject) {
            dataObj = (TotalLongUrlAccessDataObject) totalLongUrlAccess;
        } else if(totalLongUrlAccess instanceof TotalLongUrlAccessBean) {
            dataObj = ((TotalLongUrlAccessBean) totalLongUrlAccess).toDataObject();
        } else {  // if(totalLongUrlAccess instanceof TotalLongUrlAccess)
            //dataObj = new TotalLongUrlAccessDataObject(null, totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new TotalLongUrlAccessDataObject(totalLongUrlAccess.getGuid(), totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
        }
        String guid = getDAOFactory().getTotalLongUrlAccessDAO().createTotalLongUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTotalLongUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TotalLongUrlAccessDataObject dataObj = new TotalLongUrlAccessDataObject(guid, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
        return updateTotalLongUrlAccess(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalLongUrlAccess cannot be null.....
        if(totalLongUrlAccess == null || totalLongUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalLongUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalLongUrlAccess object or its guid is null!");
        }
        TotalLongUrlAccessDataObject dataObj = null;
        if(totalLongUrlAccess instanceof TotalLongUrlAccessDataObject) {
            dataObj = (TotalLongUrlAccessDataObject) totalLongUrlAccess;
        } else if(totalLongUrlAccess instanceof TotalLongUrlAccessBean) {
            dataObj = ((TotalLongUrlAccessBean) totalLongUrlAccess).toDataObject();
        } else {  // if(totalLongUrlAccess instanceof TotalLongUrlAccess)
            dataObj = new TotalLongUrlAccessDataObject(totalLongUrlAccess.getGuid(), totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
        }
        Boolean suc = getDAOFactory().getTotalLongUrlAccessDAO().updateTotalLongUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteTotalLongUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getTotalLongUrlAccessDAO().deleteTotalLongUrlAccess(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalLongUrlAccess cannot be null.....
        if(totalLongUrlAccess == null || totalLongUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalLongUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalLongUrlAccess object or its guid is null!");
        }
        TotalLongUrlAccessDataObject dataObj = null;
        if(totalLongUrlAccess instanceof TotalLongUrlAccessDataObject) {
            dataObj = (TotalLongUrlAccessDataObject) totalLongUrlAccess;
        } else if(totalLongUrlAccess instanceof TotalLongUrlAccessBean) {
            dataObj = ((TotalLongUrlAccessBean) totalLongUrlAccess).toDataObject();
        } else {  // if(totalLongUrlAccess instanceof TotalLongUrlAccess)
            dataObj = new TotalLongUrlAccessDataObject(totalLongUrlAccess.getGuid(), totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
        }
        Boolean suc = getDAOFactory().getTotalLongUrlAccessDAO().deleteTotalLongUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getTotalLongUrlAccessDAO().deleteTotalLongUrlAccesses(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
