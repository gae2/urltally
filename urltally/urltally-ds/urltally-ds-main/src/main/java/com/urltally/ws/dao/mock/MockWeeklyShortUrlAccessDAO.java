package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.WeeklyShortUrlAccessDAO;
import com.urltally.ws.data.WeeklyShortUrlAccessDataObject;


// MockWeeklyShortUrlAccessDAO is a decorator.
// It can be used as a base class to mock WeeklyShortUrlAccessDAO objects.
public abstract class MockWeeklyShortUrlAccessDAO implements WeeklyShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(MockWeeklyShortUrlAccessDAO.class.getName()); 

    // MockWeeklyShortUrlAccessDAO uses the decorator design pattern.
    private WeeklyShortUrlAccessDAO decoratedDAO;

    public MockWeeklyShortUrlAccessDAO(WeeklyShortUrlAccessDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected WeeklyShortUrlAccessDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(WeeklyShortUrlAccessDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public WeeklyShortUrlAccessDataObject getWeeklyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.getWeeklyShortUrlAccess(guid);
	}

    @Override
    public List<WeeklyShortUrlAccessDataObject> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedDAO.getWeeklyShortUrlAccesses(guids);
    }

    @Override
    public List<WeeklyShortUrlAccessDataObject> getAllWeeklyShortUrlAccesses() throws BaseException
	{
	    return getAllWeeklyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<WeeklyShortUrlAccessDataObject> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllWeeklyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccessDataObject> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllWeeklyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllWeeklyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllWeeklyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException
    {
        return decoratedDAO.createWeeklyShortUrlAccess( weeklyShortUrlAccess);
    }

    @Override
	public Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException
	{
        return decoratedDAO.updateWeeklyShortUrlAccess(weeklyShortUrlAccess);
	}
	
    @Override
    public Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException
    {
        return decoratedDAO.deleteWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.deleteWeeklyShortUrlAccess(guid);
	}

    @Override
    public Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteWeeklyShortUrlAccesses(filter, params, values);
    }

}
