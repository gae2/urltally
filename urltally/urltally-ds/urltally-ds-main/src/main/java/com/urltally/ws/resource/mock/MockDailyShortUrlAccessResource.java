package com.urltally.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ResourceAlreadyPresentException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.DataStoreRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.bean.DailyShortUrlAccessBean;
import com.urltally.ws.stub.DailyShortUrlAccessListStub;
import com.urltally.ws.stub.DailyShortUrlAccessStub;
import com.urltally.ws.resource.ServiceManager;
import com.urltally.ws.resource.DailyShortUrlAccessResource;

// MockDailyShortUrlAccessResource is a decorator.
// It can be used as a base class to mock DailyShortUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/dailyShortUrlAccesses/")
public abstract class MockDailyShortUrlAccessResource implements DailyShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockDailyShortUrlAccessResource.class.getName());

    // MockDailyShortUrlAccessResource uses the decorator design pattern.
    private DailyShortUrlAccessResource decoratedResource;

    public MockDailyShortUrlAccessResource(DailyShortUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected DailyShortUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(DailyShortUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getDailyShortUrlAccessKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getDailyShortUrlAccessKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getDailyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getDailyShortUrlAccess(guid);
    }

    @Override
    public Response getDailyShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getDailyShortUrlAccess(guid, field);
    }

    @Override
    public Response createDailyShortUrlAccess(DailyShortUrlAccessStub dailyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createDailyShortUrlAccess(dailyShortUrlAccess);
    }

    @Override
    public Response updateDailyShortUrlAccess(String guid, DailyShortUrlAccessStub dailyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateDailyShortUrlAccess(guid, dailyShortUrlAccess);
    }

    @Override
    public Response updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseResourceException
    {
        return decoratedResource.updateDailyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
    }

    @Override
    public Response deleteDailyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteDailyShortUrlAccess(guid);
    }

    @Override
    public Response deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteDailyShortUrlAccesses(filter, params, values);
    }


}
