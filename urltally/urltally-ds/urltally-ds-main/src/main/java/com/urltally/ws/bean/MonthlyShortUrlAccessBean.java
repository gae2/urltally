package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.data.MonthlyShortUrlAccessDataObject;

public class MonthlyShortUrlAccessBean extends ShortUrlAccessBean implements MonthlyShortUrlAccess
{
    private static final Logger log = Logger.getLogger(MonthlyShortUrlAccessBean.class.getName());

    // Embedded data object.
    private MonthlyShortUrlAccessDataObject dobj = null;

    public MonthlyShortUrlAccessBean()
    {
        this(new MonthlyShortUrlAccessDataObject());
    }
    public MonthlyShortUrlAccessBean(String guid)
    {
        this(new MonthlyShortUrlAccessDataObject(guid));
    }
    public MonthlyShortUrlAccessBean(MonthlyShortUrlAccessDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public MonthlyShortUrlAccessDataObject getDataObject()
    {
        return this.dobj;
    }

    public Integer getYear()
    {
        if(getDataObject() != null) {
            return getDataObject().getYear();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MonthlyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setYear(Integer year)
    {
        if(getDataObject() != null) {
            getDataObject().setYear(year);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MonthlyShortUrlAccessDataObject is null!");
        }
    }

    public Integer getMonth()
    {
        if(getDataObject() != null) {
            return getDataObject().getMonth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MonthlyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setMonth(Integer month)
    {
        if(getDataObject() != null) {
            getDataObject().setMonth(month);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MonthlyShortUrlAccessDataObject is null!");
        }
    }

    public Integer getNumberOfDays()
    {
        if(getDataObject() != null) {
            return getDataObject().getNumberOfDays();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MonthlyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setNumberOfDays(Integer numberOfDays)
    {
        if(getDataObject() != null) {
            getDataObject().setNumberOfDays(numberOfDays);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MonthlyShortUrlAccessDataObject is null!");
        }
    }


    // TBD
    public MonthlyShortUrlAccessDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
