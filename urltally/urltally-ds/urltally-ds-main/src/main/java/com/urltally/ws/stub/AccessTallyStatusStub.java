package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "accessTallyStatus")
@XmlType(propOrder = {"guid", "remoteRecordGuid", "shortUrl", "tallyType", "tallyTime", "tallyEpoch", "processed", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessTallyStatusStub implements AccessTallyStatus, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AccessTallyStatusStub.class.getName());

    private String guid;
    private String remoteRecordGuid;
    private String shortUrl;
    private String tallyType;
    private String tallyTime;
    private Long tallyEpoch;
    private Boolean processed;
    private Long createdTime;
    private Long modifiedTime;

    public AccessTallyStatusStub()
    {
        this(null);
    }
    public AccessTallyStatusStub(AccessTallyStatus bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.remoteRecordGuid = bean.getRemoteRecordGuid();
            this.shortUrl = bean.getShortUrl();
            this.tallyType = bean.getTallyType();
            this.tallyTime = bean.getTallyTime();
            this.tallyEpoch = bean.getTallyEpoch();
            this.processed = bean.isProcessed();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getRemoteRecordGuid()
    {
        return this.remoteRecordGuid;
    }
    public void setRemoteRecordGuid(String remoteRecordGuid)
    {
        this.remoteRecordGuid = remoteRecordGuid;
    }

    @XmlElement
    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    @XmlElement
    public String getTallyType()
    {
        return this.tallyType;
    }
    public void setTallyType(String tallyType)
    {
        this.tallyType = tallyType;
    }

    @XmlElement
    public String getTallyTime()
    {
        return this.tallyTime;
    }
    public void setTallyTime(String tallyTime)
    {
        this.tallyTime = tallyTime;
    }

    @XmlElement
    public Long getTallyEpoch()
    {
        return this.tallyEpoch;
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        this.tallyEpoch = tallyEpoch;
    }

    @XmlElement
    public Boolean isProcessed()
    {
        return this.processed;
    }
    public void setProcessed(Boolean processed)
    {
        this.processed = processed;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("remoteRecordGuid", this.remoteRecordGuid);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("tallyType", this.tallyType);
        dataMap.put("tallyTime", this.tallyTime);
        dataMap.put("tallyEpoch", this.tallyEpoch);
        dataMap.put("processed", this.processed);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = remoteRecordGuid == null ? 0 : remoteRecordGuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyType == null ? 0 : tallyType.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyTime == null ? 0 : tallyTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyEpoch == null ? 0 : tallyEpoch.hashCode();
        _hash = 31 * _hash + delta;
        delta = processed == null ? 0 : processed.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static AccessTallyStatusStub convertBeanToStub(AccessTallyStatus bean)
    {
        AccessTallyStatusStub stub = null;
        if(bean instanceof AccessTallyStatusStub) {
            stub = (AccessTallyStatusStub) bean;
        } else {
            if(bean != null) {
                stub = new AccessTallyStatusStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AccessTallyStatusStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of AccessTallyStatusStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AccessTallyStatusStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AccessTallyStatusStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AccessTallyStatusStub object as a string.", e);
        }
        
        return null;
    }
    public static AccessTallyStatusStub fromJsonString(String jsonStr)
    {
        try {
            AccessTallyStatusStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AccessTallyStatusStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AccessTallyStatusStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AccessTallyStatusStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AccessTallyStatusStub object.", e);
        }
        
        return null;
    }

}
