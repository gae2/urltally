package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.data.CumulativeShortUrlAccessDataObject;

public class CumulativeShortUrlAccessBean extends ShortUrlAccessBean implements CumulativeShortUrlAccess
{
    private static final Logger log = Logger.getLogger(CumulativeShortUrlAccessBean.class.getName());

    // Embedded data object.
    private CumulativeShortUrlAccessDataObject dobj = null;

    public CumulativeShortUrlAccessBean()
    {
        this(new CumulativeShortUrlAccessDataObject());
    }
    public CumulativeShortUrlAccessBean(String guid)
    {
        this(new CumulativeShortUrlAccessDataObject(guid));
    }
    public CumulativeShortUrlAccessBean(CumulativeShortUrlAccessDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public CumulativeShortUrlAccessDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getStartDayHour()
    {
        if(getDataObject() != null) {
            return getDataObject().getStartDayHour();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CumulativeShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setStartDayHour(String startDayHour)
    {
        if(getDataObject() != null) {
            getDataObject().setStartDayHour(startDayHour);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CumulativeShortUrlAccessDataObject is null!");
        }
    }

    public String getEndDayHour()
    {
        if(getDataObject() != null) {
            return getDataObject().getEndDayHour();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CumulativeShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setEndDayHour(String endDayHour)
    {
        if(getDataObject() != null) {
            getDataObject().setEndDayHour(endDayHour);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CumulativeShortUrlAccessDataObject is null!");
        }
    }

    public Long getStartTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getStartTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CumulativeShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setStartTime(Long startTime)
    {
        if(getDataObject() != null) {
            getDataObject().setStartTime(startTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CumulativeShortUrlAccessDataObject is null!");
        }
    }

    public Long getEndTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getEndTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CumulativeShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setEndTime(Long endTime)
    {
        if(getDataObject() != null) {
            getDataObject().setEndTime(endTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CumulativeShortUrlAccessDataObject is null!");
        }
    }


    // TBD
    public CumulativeShortUrlAccessDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
