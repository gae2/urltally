package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.bean.AccessTallyMasterBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.AccessTallyMasterDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.AccessTallyMasterService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AccessTallyMasterServiceImpl implements AccessTallyMasterService
{
    private static final Logger log = Logger.getLogger(AccessTallyMasterServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // AccessTallyMaster related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException
    {
        log.finer("BEGIN");

        AccessTallyMasterDataObject dataObj = getDAOFactory().getAccessTallyMasterDAO().getAccessTallyMaster(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AccessTallyMasterDataObject for guid = " + guid);
            return null;  // ????
        }
        AccessTallyMasterBean bean = new AccessTallyMasterBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAccessTallyMaster(String guid, String field) throws BaseException
    {
        AccessTallyMasterDataObject dataObj = getDAOFactory().getAccessTallyMasterDAO().getAccessTallyMaster(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AccessTallyMasterDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyType")) {
            return dataObj.getTallyType();
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("tallyStatus")) {
            return dataObj.getTallyStatus();
        } else if(field.equals("accessRecordCount")) {
            return dataObj.getAccessRecordCount();
        } else if(field.equals("procesingStartedTime")) {
            return dataObj.getProcesingStartedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AccessTallyMaster> list = new ArrayList<AccessTallyMaster>();
        List<AccessTallyMasterDataObject> dataObjs = getDAOFactory().getAccessTallyMasterDAO().getAccessTallyMasters(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyMasterDataObject list.");
        } else {
            Iterator<AccessTallyMasterDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AccessTallyMasterDataObject dataObj = (AccessTallyMasterDataObject) it.next();
                list.add(new AccessTallyMasterBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException
    {
        return getAllAccessTallyMasters(null, null, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyMasters(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<AccessTallyMaster> list = new ArrayList<AccessTallyMaster>();
        List<AccessTallyMasterDataObject> dataObjs = getDAOFactory().getAccessTallyMasterDAO().getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyMasterDataObject list.");
        } else {
            Iterator<AccessTallyMasterDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AccessTallyMasterDataObject dataObj = (AccessTallyMasterDataObject) it.next();
                list.add(new AccessTallyMasterBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllAccessTallyMasterKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getAccessTallyMasterDAO().getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyMaster key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AccessTallyMasterServiceImpl.findAccessTallyMasters(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<AccessTallyMaster> list = new ArrayList<AccessTallyMaster>();
        List<AccessTallyMasterDataObject> dataObjs = getDAOFactory().getAccessTallyMasterDAO().findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find accessTallyMasters for the given criterion.");
        } else {
            Iterator<AccessTallyMasterDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AccessTallyMasterDataObject dataObj = (AccessTallyMasterDataObject) it.next();
                list.add(new AccessTallyMasterBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AccessTallyMasterServiceImpl.findAccessTallyMasterKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getAccessTallyMasterDAO().findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AccessTallyMaster keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AccessTallyMasterServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getAccessTallyMasterDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        AccessTallyMasterDataObject dataObj = new AccessTallyMasterDataObject(null, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        return createAccessTallyMaster(dataObj);
    }

    @Override
    public String createAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyMaster cannot be null.....
        if(accessTallyMaster == null) {
            log.log(Level.INFO, "Param accessTallyMaster is null!");
            throw new BadRequestException("Param accessTallyMaster object is null!");
        }
        AccessTallyMasterDataObject dataObj = null;
        if(accessTallyMaster instanceof AccessTallyMasterDataObject) {
            dataObj = (AccessTallyMasterDataObject) accessTallyMaster;
        } else if(accessTallyMaster instanceof AccessTallyMasterBean) {
            dataObj = ((AccessTallyMasterBean) accessTallyMaster).toDataObject();
        } else {  // if(accessTallyMaster instanceof AccessTallyMaster)
            //dataObj = new AccessTallyMasterDataObject(null, accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new AccessTallyMasterDataObject(accessTallyMaster.getGuid(), accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
        }
        String guid = getDAOFactory().getAccessTallyMasterDAO().createAccessTallyMaster(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AccessTallyMasterDataObject dataObj = new AccessTallyMasterDataObject(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        return updateAccessTallyMaster(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyMaster cannot be null.....
        if(accessTallyMaster == null || accessTallyMaster.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyMaster or its guid is null!");
            throw new BadRequestException("Param accessTallyMaster object or its guid is null!");
        }
        AccessTallyMasterDataObject dataObj = null;
        if(accessTallyMaster instanceof AccessTallyMasterDataObject) {
            dataObj = (AccessTallyMasterDataObject) accessTallyMaster;
        } else if(accessTallyMaster instanceof AccessTallyMasterBean) {
            dataObj = ((AccessTallyMasterBean) accessTallyMaster).toDataObject();
        } else {  // if(accessTallyMaster instanceof AccessTallyMaster)
            dataObj = new AccessTallyMasterDataObject(accessTallyMaster.getGuid(), accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
        }
        Boolean suc = getDAOFactory().getAccessTallyMasterDAO().updateAccessTallyMaster(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getAccessTallyMasterDAO().deleteAccessTallyMaster(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyMaster cannot be null.....
        if(accessTallyMaster == null || accessTallyMaster.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyMaster or its guid is null!");
            throw new BadRequestException("Param accessTallyMaster object or its guid is null!");
        }
        AccessTallyMasterDataObject dataObj = null;
        if(accessTallyMaster instanceof AccessTallyMasterDataObject) {
            dataObj = (AccessTallyMasterDataObject) accessTallyMaster;
        } else if(accessTallyMaster instanceof AccessTallyMasterBean) {
            dataObj = ((AccessTallyMasterBean) accessTallyMaster).toDataObject();
        } else {  // if(accessTallyMaster instanceof AccessTallyMaster)
            dataObj = new AccessTallyMasterDataObject(accessTallyMaster.getGuid(), accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
        }
        Boolean suc = getDAOFactory().getAccessTallyMasterDAO().deleteAccessTallyMaster(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getAccessTallyMasterDAO().deleteAccessTallyMasters(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
