package com.urltally.cert.ws;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseConsumerRegistry.class.getName());

    // Consumer key-secret map.
    // TBD: Use a better data structure which reflects OAuthConsumerInfo.
    private Map<String, String> consumerSecretMap;
    
    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD...
        consumerSecretMap.put("4961dfac-6312-4bca-a8cd-683fb9918717", "5dee993b-9bfd-4151-9a02-dd3af5242391");  // UrlTally + UrlTallyApp
        consumerSecretMap.put("c502e7c2-1870-420a-b6bc-d80e3954aebf", "c4718f88-6464-4191-9a44-eb90dfbdb2df");  // UrlTally + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
