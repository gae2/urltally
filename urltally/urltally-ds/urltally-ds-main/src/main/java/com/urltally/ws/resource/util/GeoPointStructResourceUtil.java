package com.urltally.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.GeoPointStruct;
import com.urltally.ws.bean.GeoPointStructBean;
import com.urltally.ws.stub.GeoPointStructStub;


public class GeoPointStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GeoPointStructResourceUtil.class.getName());

    // Static methods only.
    private GeoPointStructResourceUtil() {}

    public static GeoPointStructBean convertGeoPointStructStubToBean(GeoPointStruct stub)
    {
        GeoPointStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null GeoPointStructBean is returned.");
        } else {
            bean = new GeoPointStructBean();
            bean.setUuid(stub.getUuid());
            bean.setLatitude(stub.getLatitude());
            bean.setLongitude(stub.getLongitude());
            bean.setAltitude(stub.getAltitude());
            bean.setSensorUsed(stub.isSensorUsed());
        }
        return bean;
    }

}
