package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.DailyShortUrlAccessDAO;
import com.urltally.ws.data.DailyShortUrlAccessDataObject;


// MockDailyShortUrlAccessDAO is a decorator.
// It can be used as a base class to mock DailyShortUrlAccessDAO objects.
public abstract class MockDailyShortUrlAccessDAO implements DailyShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(MockDailyShortUrlAccessDAO.class.getName()); 

    // MockDailyShortUrlAccessDAO uses the decorator design pattern.
    private DailyShortUrlAccessDAO decoratedDAO;

    public MockDailyShortUrlAccessDAO(DailyShortUrlAccessDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected DailyShortUrlAccessDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(DailyShortUrlAccessDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public DailyShortUrlAccessDataObject getDailyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.getDailyShortUrlAccess(guid);
	}

    @Override
    public List<DailyShortUrlAccessDataObject> getDailyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedDAO.getDailyShortUrlAccesses(guids);
    }

    @Override
    public List<DailyShortUrlAccessDataObject> getAllDailyShortUrlAccesses() throws BaseException
	{
	    return getAllDailyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<DailyShortUrlAccessDataObject> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllDailyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccessDataObject> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDailyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<DailyShortUrlAccessDataObject> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<DailyShortUrlAccessDataObject> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<DailyShortUrlAccessDataObject> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<DailyShortUrlAccessDataObject> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createDailyShortUrlAccess(DailyShortUrlAccessDataObject dailyShortUrlAccess) throws BaseException
    {
        return decoratedDAO.createDailyShortUrlAccess( dailyShortUrlAccess);
    }

    @Override
	public Boolean updateDailyShortUrlAccess(DailyShortUrlAccessDataObject dailyShortUrlAccess) throws BaseException
	{
        return decoratedDAO.updateDailyShortUrlAccess(dailyShortUrlAccess);
	}
	
    @Override
    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccessDataObject dailyShortUrlAccess) throws BaseException
    {
        return decoratedDAO.deleteDailyShortUrlAccess(dailyShortUrlAccess);
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.deleteDailyShortUrlAccess(guid);
	}

    @Override
    public Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteDailyShortUrlAccesses(filter, params, values);
    }

}
