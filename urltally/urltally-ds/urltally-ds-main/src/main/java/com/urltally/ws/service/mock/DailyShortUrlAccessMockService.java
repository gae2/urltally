package com.urltally.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.bean.DailyShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.DailyShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.DailyShortUrlAccessService;


// DailyShortUrlAccessMockService is a decorator.
// It can be used as a base class to mock DailyShortUrlAccessService objects.
public abstract class DailyShortUrlAccessMockService implements DailyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(DailyShortUrlAccessMockService.class.getName());

    // DailyShortUrlAccessMockService uses the decorator design pattern.
    private DailyShortUrlAccessService decoratedService;

    public DailyShortUrlAccessMockService(DailyShortUrlAccessService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected DailyShortUrlAccessService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(DailyShortUrlAccessService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // DailyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DailyShortUrlAccess getDailyShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDailyShortUrlAccess(): guid = " + guid);
        DailyShortUrlAccess bean = decoratedService.getDailyShortUrlAccess(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getDailyShortUrlAccess(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getDailyShortUrlAccess(guid, field);
        return obj;
    }

    @Override
    public List<DailyShortUrlAccess> getDailyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getDailyShortUrlAccesses()");
        List<DailyShortUrlAccess> dailyShortUrlAccesses = decoratedService.getDailyShortUrlAccesses(guids);
        log.finer("END");
        return dailyShortUrlAccesses;
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses() throws BaseException
    {
        return getAllDailyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDailyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDailyShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<DailyShortUrlAccess> dailyShortUrlAccesses = decoratedService.getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return dailyShortUrlAccesses;
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDailyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDailyShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DailyShortUrlAccessMockService.findDailyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<DailyShortUrlAccess> dailyShortUrlAccesses = decoratedService.findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return dailyShortUrlAccesses;
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DailyShortUrlAccessMockService.findDailyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DailyShortUrlAccessMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDailyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        return decoratedService.createDailyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
    }

    @Override
    public String createDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createDailyShortUrlAccess(dailyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        return decoratedService.updateDailyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
    }
        
    @Override
    public Boolean updateDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateDailyShortUrlAccess(dailyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDailyShortUrlAccess(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteDailyShortUrlAccess(dailyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteDailyShortUrlAccesses(filter, params, values);
        return count;
    }

}
