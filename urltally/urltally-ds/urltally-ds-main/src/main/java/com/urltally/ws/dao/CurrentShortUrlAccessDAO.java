package com.urltally.ws.dao;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.data.CurrentShortUrlAccessDataObject;


// TBD: Add offset/count to getAllCurrentShortUrlAccesses() and findCurrentShortUrlAccesses(), etc.
public interface CurrentShortUrlAccessDAO
{
    CurrentShortUrlAccessDataObject getCurrentShortUrlAccess(String guid) throws BaseException;
    List<CurrentShortUrlAccessDataObject> getCurrentShortUrlAccesses(List<String> guids) throws BaseException;
    List<CurrentShortUrlAccessDataObject> getAllCurrentShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<CurrentShortUrlAccessDataObject> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<CurrentShortUrlAccessDataObject> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createCurrentShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return CurrentShortUrlAccessDataObject?)
    String createCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException;          // Returns Guid.  (Return CurrentShortUrlAccessDataObject?)
    //Boolean updateCurrentShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException;
    Boolean deleteCurrentShortUrlAccess(String guid) throws BaseException;
    Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException;
    Long deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;
}
