package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.data.TotalShortUrlAccessDataObject;

public class TotalShortUrlAccessBean extends BeanBase implements TotalShortUrlAccess
{
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessBean.class.getName());

    // Embedded data object.
    private TotalShortUrlAccessDataObject dobj = null;

    public TotalShortUrlAccessBean()
    {
        this(new TotalShortUrlAccessDataObject());
    }
    public TotalShortUrlAccessBean(String guid)
    {
        this(new TotalShortUrlAccessDataObject(guid));
    }
    public TotalShortUrlAccessBean(TotalShortUrlAccessDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public TotalShortUrlAccessDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
        }
    }

    public String getTallyType()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyType(String tallyType)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyType(tallyType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
        }
    }

    public String getTallyTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyTime(String tallyTime)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyTime(tallyTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
        }
    }

    public Long getTallyEpoch()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyEpoch();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyEpoch(tallyEpoch);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
        }
    }

    public Integer getCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setCount(Integer count)
    {
        if(getDataObject() != null) {
            getDataObject().setCount(count);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
        }
    }

    public String getShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrl(shortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
        }
    }

    public String getShortUrlDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrlDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrlDomain(shortUrlDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalShortUrlAccessDataObject is null!");
        }
    }


    // TBD
    public TotalShortUrlAccessDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
