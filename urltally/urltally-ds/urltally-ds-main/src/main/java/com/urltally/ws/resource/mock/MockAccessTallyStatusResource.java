package com.urltally.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ResourceAlreadyPresentException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.DataStoreRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.bean.AccessTallyStatusBean;
import com.urltally.ws.stub.AccessTallyStatusListStub;
import com.urltally.ws.stub.AccessTallyStatusStub;
import com.urltally.ws.resource.ServiceManager;
import com.urltally.ws.resource.AccessTallyStatusResource;

// MockAccessTallyStatusResource is a decorator.
// It can be used as a base class to mock AccessTallyStatusResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/accessTallyStatuses/")
public abstract class MockAccessTallyStatusResource implements AccessTallyStatusResource
{
    private static final Logger log = Logger.getLogger(MockAccessTallyStatusResource.class.getName());

    // MockAccessTallyStatusResource uses the decorator design pattern.
    private AccessTallyStatusResource decoratedResource;

    public MockAccessTallyStatusResource(AccessTallyStatusResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected AccessTallyStatusResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(AccessTallyStatusResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getAccessTallyStatusKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getAccessTallyStatusKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getAccessTallyStatus(String guid) throws BaseResourceException
    {
        return decoratedResource.getAccessTallyStatus(guid);
    }

    @Override
    public Response getAccessTallyStatus(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getAccessTallyStatus(guid, field);
    }

    @Override
    public Response createAccessTallyStatus(AccessTallyStatusStub accessTallyStatus) throws BaseResourceException
    {
        return decoratedResource.createAccessTallyStatus(accessTallyStatus);
    }

    @Override
    public Response updateAccessTallyStatus(String guid, AccessTallyStatusStub accessTallyStatus) throws BaseResourceException
    {
        return decoratedResource.updateAccessTallyStatus(guid, accessTallyStatus);
    }

    @Override
    public Response updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseResourceException
    {
        return decoratedResource.updateAccessTallyStatus(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
    }

    @Override
    public Response deleteAccessTallyStatus(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteAccessTallyStatus(guid);
    }

    @Override
    public Response deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteAccessTallyStatuses(filter, params, values);
    }


}
