package com.urltally.ws.service;

import java.util.List;

import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface AccessTallyMasterService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException;
    Object getAccessTallyMaster(String guid, String field) throws BaseException;
    List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException;
    List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException;
    /* @Deprecated */ List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException;
    List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException;
    //String createAccessTallyMaster(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AccessTallyMaster?)
    String createAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException;          // Returns Guid.  (Return AccessTallyMaster?)
    Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException;
    //Boolean updateAccessTallyMaster(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException;
    Boolean deleteAccessTallyMaster(String guid) throws BaseException;
    Boolean deleteAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException;
    Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException;

//    Integer createAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException;
//    Boolean updateeAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException;

}
