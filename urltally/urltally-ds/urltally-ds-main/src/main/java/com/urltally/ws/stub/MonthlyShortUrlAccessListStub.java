package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "monthlyShortUrlAccesses")
@XmlType(propOrder = {"monthlyShortUrlAccess", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class MonthlyShortUrlAccessListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(MonthlyShortUrlAccessListStub.class.getName());

    private List<MonthlyShortUrlAccessStub> monthlyShortUrlAccesses = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public MonthlyShortUrlAccessListStub()
    {
        this(new ArrayList<MonthlyShortUrlAccessStub>());
    }
    public MonthlyShortUrlAccessListStub(List<MonthlyShortUrlAccessStub> monthlyShortUrlAccesses)
    {
        this(monthlyShortUrlAccesses, null);
    }
    public MonthlyShortUrlAccessListStub(List<MonthlyShortUrlAccessStub> monthlyShortUrlAccesses, String forwardCursor)
    {
        this.monthlyShortUrlAccesses = monthlyShortUrlAccesses;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(monthlyShortUrlAccesses == null) {
            return true;
        } else {
            return monthlyShortUrlAccesses.isEmpty();
        }
    }
    public int getSize()
    {
        if(monthlyShortUrlAccesses == null) {
            return 0;
        } else {
            return monthlyShortUrlAccesses.size();
        }
    }


    @XmlElement(name = "monthlyShortUrlAccess")
    public List<MonthlyShortUrlAccessStub> getMonthlyShortUrlAccess()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<MonthlyShortUrlAccessStub> getList()
    {
        return monthlyShortUrlAccesses;
    }
    public void setList(List<MonthlyShortUrlAccessStub> monthlyShortUrlAccesses)
    {
        this.monthlyShortUrlAccesses = monthlyShortUrlAccesses;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<MonthlyShortUrlAccessStub> it = this.monthlyShortUrlAccesses.iterator();
        while(it.hasNext()) {
            MonthlyShortUrlAccessStub monthlyShortUrlAccess = it.next();
            sb.append(monthlyShortUrlAccess.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static MonthlyShortUrlAccessListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of MonthlyShortUrlAccessListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write MonthlyShortUrlAccessListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write MonthlyShortUrlAccessListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write MonthlyShortUrlAccessListStub object as a string.", e);
        }
        
        return null;
    }
    public static MonthlyShortUrlAccessListStub fromJsonString(String jsonStr)
    {
        try {
            MonthlyShortUrlAccessListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, MonthlyShortUrlAccessListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into MonthlyShortUrlAccessListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into MonthlyShortUrlAccessListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into MonthlyShortUrlAccessListStub object.", e);
        }
        
        return null;
    }

}
