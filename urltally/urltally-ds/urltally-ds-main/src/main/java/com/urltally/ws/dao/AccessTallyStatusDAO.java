package com.urltally.ws.dao;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.data.AccessTallyStatusDataObject;


// TBD: Add offset/count to getAllAccessTallyStatuses() and findAccessTallyStatuses(), etc.
public interface AccessTallyStatusDAO
{
    AccessTallyStatusDataObject getAccessTallyStatus(String guid) throws BaseException;
    List<AccessTallyStatusDataObject> getAccessTallyStatuses(List<String> guids) throws BaseException;
    List<AccessTallyStatusDataObject> getAllAccessTallyStatuses() throws BaseException;
    /* @Deprecated */ List<AccessTallyStatusDataObject> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException;
    List<AccessTallyStatusDataObject> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<AccessTallyStatusDataObject> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<AccessTallyStatusDataObject> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<AccessTallyStatusDataObject> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<AccessTallyStatusDataObject> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createAccessTallyStatus(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AccessTallyStatusDataObject?)
    String createAccessTallyStatus(AccessTallyStatusDataObject accessTallyStatus) throws BaseException;          // Returns Guid.  (Return AccessTallyStatusDataObject?)
    //Boolean updateAccessTallyStatus(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAccessTallyStatus(AccessTallyStatusDataObject accessTallyStatus) throws BaseException;
    Boolean deleteAccessTallyStatus(String guid) throws BaseException;
    Boolean deleteAccessTallyStatus(AccessTallyStatusDataObject accessTallyStatus) throws BaseException;
    Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException;
}
