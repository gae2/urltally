package com.urltally.ws;



public interface KeyValueRelationStruct extends KeyValuePairStruct
{
    String  getRelation();
    boolean isEmpty();
}
