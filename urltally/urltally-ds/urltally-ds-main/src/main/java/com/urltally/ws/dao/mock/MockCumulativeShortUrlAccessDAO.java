package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.CumulativeShortUrlAccessDAO;
import com.urltally.ws.data.CumulativeShortUrlAccessDataObject;


// MockCumulativeShortUrlAccessDAO is a decorator.
// It can be used as a base class to mock CumulativeShortUrlAccessDAO objects.
public abstract class MockCumulativeShortUrlAccessDAO implements CumulativeShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(MockCumulativeShortUrlAccessDAO.class.getName()); 

    // MockCumulativeShortUrlAccessDAO uses the decorator design pattern.
    private CumulativeShortUrlAccessDAO decoratedDAO;

    public MockCumulativeShortUrlAccessDAO(CumulativeShortUrlAccessDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected CumulativeShortUrlAccessDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(CumulativeShortUrlAccessDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public CumulativeShortUrlAccessDataObject getCumulativeShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.getCumulativeShortUrlAccess(guid);
	}

    @Override
    public List<CumulativeShortUrlAccessDataObject> getCumulativeShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedDAO.getCumulativeShortUrlAccesses(guids);
    }

    @Override
    public List<CumulativeShortUrlAccessDataObject> getAllCumulativeShortUrlAccesses() throws BaseException
	{
	    return getAllCumulativeShortUrlAccesses(null, null, null);
    }


    @Override
    public List<CumulativeShortUrlAccessDataObject> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllCumulativeShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccessDataObject> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllCumulativeShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCumulativeShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllCumulativeShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<CumulativeShortUrlAccessDataObject> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<CumulativeShortUrlAccessDataObject> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<CumulativeShortUrlAccessDataObject> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<CumulativeShortUrlAccessDataObject> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createCumulativeShortUrlAccess(CumulativeShortUrlAccessDataObject cumulativeShortUrlAccess) throws BaseException
    {
        return decoratedDAO.createCumulativeShortUrlAccess( cumulativeShortUrlAccess);
    }

    @Override
	public Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccessDataObject cumulativeShortUrlAccess) throws BaseException
	{
        return decoratedDAO.updateCumulativeShortUrlAccess(cumulativeShortUrlAccess);
	}
	
    @Override
    public Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccessDataObject cumulativeShortUrlAccess) throws BaseException
    {
        return decoratedDAO.deleteCumulativeShortUrlAccess(cumulativeShortUrlAccess);
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.deleteCumulativeShortUrlAccess(guid);
	}

    @Override
    public Long deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteCumulativeShortUrlAccesses(filter, params, values);
    }

}
