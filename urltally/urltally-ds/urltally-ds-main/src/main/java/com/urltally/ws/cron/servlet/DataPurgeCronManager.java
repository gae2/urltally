package com.urltally.ws.cron.servlet;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.service.ApiConsumerService;
import com.urltally.ws.service.UserService;
import com.urltally.ws.service.AccessTallyMasterService;
import com.urltally.ws.service.AccessTallyStatusService;
import com.urltally.ws.service.MonthlyShortUrlAccessService;
import com.urltally.ws.service.WeeklyShortUrlAccessService;
import com.urltally.ws.service.DailyShortUrlAccessService;
import com.urltally.ws.service.HourlyShortUrlAccessService;
import com.urltally.ws.service.CumulativeShortUrlAccessService;
import com.urltally.ws.service.CurrentShortUrlAccessService;
import com.urltally.ws.service.TotalShortUrlAccessService;
import com.urltally.ws.service.TotalLongUrlAccessService;
import com.urltally.ws.service.ServiceInfoService;
import com.urltally.ws.service.FiveTenService;
import com.urltally.ws.service.impl.ApiConsumerServiceImpl;
import com.urltally.ws.service.impl.UserServiceImpl;
import com.urltally.ws.service.impl.AccessTallyMasterServiceImpl;
import com.urltally.ws.service.impl.AccessTallyStatusServiceImpl;
import com.urltally.ws.service.impl.MonthlyShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.WeeklyShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.DailyShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.HourlyShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.CumulativeShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.CurrentShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.TotalShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.TotalLongUrlAccessServiceImpl;
import com.urltally.ws.service.impl.ServiceInfoServiceImpl;
import com.urltally.ws.service.impl.FiveTenServiceImpl;


public final class DataPurgeCronManager
{
    private static final Logger log = Logger.getLogger(DataPurgeCronManager.class.getName());   

    // TBD...
    // Count is per entity, not per cron run.
    public static final int DEFAULT_MAX_COUNT = 10;
    public static final int MAXIMUM_MAX_COUNT = 1000;   // ????
    // ...
    public static final int DEFAULT_DELTA_HOURS = 168;    // 1 week.
    public static final int MAXIMUM_DELTA_HOURS = 8760;   // 1 year.
    // ...
    

    // TBD: Is this safe for concurrent calls??
    private ApiConsumerService apiConsumerService = null;
    private UserService userService = null;
    private AccessTallyMasterService accessTallyMasterService = null;
    private AccessTallyStatusService accessTallyStatusService = null;
    private MonthlyShortUrlAccessService monthlyShortUrlAccessService = null;
    private WeeklyShortUrlAccessService weeklyShortUrlAccessService = null;
    private DailyShortUrlAccessService dailyShortUrlAccessService = null;
    private HourlyShortUrlAccessService hourlyShortUrlAccessService = null;
    private CumulativeShortUrlAccessService cumulativeShortUrlAccessService = null;
    private CurrentShortUrlAccessService currentShortUrlAccessService = null;
    private TotalShortUrlAccessService totalShortUrlAccessService = null;
    private TotalLongUrlAccessService totalLongUrlAccessService = null;
    private ServiceInfoService serviceInfoService = null;
    private FiveTenService fiveTenService = null;
    // etc...

    // Lazy initialized.
    private ApiConsumerService getApiConsumerService()
    {
        if(apiConsumerService == null) {
            apiConsumerService = new ApiConsumerServiceImpl();
        }
        return apiConsumerService;
    }
    private UserService getUserService()
    {
        if(userService == null) {
            userService = new UserServiceImpl();
        }
        return userService;
    }
    private AccessTallyMasterService getAccessTallyMasterService()
    {
        if(accessTallyMasterService == null) {
            accessTallyMasterService = new AccessTallyMasterServiceImpl();
        }
        return accessTallyMasterService;
    }
    private AccessTallyStatusService getAccessTallyStatusService()
    {
        if(accessTallyStatusService == null) {
            accessTallyStatusService = new AccessTallyStatusServiceImpl();
        }
        return accessTallyStatusService;
    }
    private MonthlyShortUrlAccessService getMonthlyShortUrlAccessService()
    {
        if(monthlyShortUrlAccessService == null) {
            monthlyShortUrlAccessService = new MonthlyShortUrlAccessServiceImpl();
        }
        return monthlyShortUrlAccessService;
    }
    private WeeklyShortUrlAccessService getWeeklyShortUrlAccessService()
    {
        if(weeklyShortUrlAccessService == null) {
            weeklyShortUrlAccessService = new WeeklyShortUrlAccessServiceImpl();
        }
        return weeklyShortUrlAccessService;
    }
    private DailyShortUrlAccessService getDailyShortUrlAccessService()
    {
        if(dailyShortUrlAccessService == null) {
            dailyShortUrlAccessService = new DailyShortUrlAccessServiceImpl();
        }
        return dailyShortUrlAccessService;
    }
    private HourlyShortUrlAccessService getHourlyShortUrlAccessService()
    {
        if(hourlyShortUrlAccessService == null) {
            hourlyShortUrlAccessService = new HourlyShortUrlAccessServiceImpl();
        }
        return hourlyShortUrlAccessService;
    }
    private CumulativeShortUrlAccessService getCumulativeShortUrlAccessService()
    {
        if(cumulativeShortUrlAccessService == null) {
            cumulativeShortUrlAccessService = new CumulativeShortUrlAccessServiceImpl();
        }
        return cumulativeShortUrlAccessService;
    }
    private CurrentShortUrlAccessService getCurrentShortUrlAccessService()
    {
        if(currentShortUrlAccessService == null) {
            currentShortUrlAccessService = new CurrentShortUrlAccessServiceImpl();
        }
        return currentShortUrlAccessService;
    }
    private TotalShortUrlAccessService getTotalShortUrlAccessService()
    {
        if(totalShortUrlAccessService == null) {
            totalShortUrlAccessService = new TotalShortUrlAccessServiceImpl();
        }
        return totalShortUrlAccessService;
    }
    private TotalLongUrlAccessService getTotalLongUrlAccessService()
    {
        if(totalLongUrlAccessService == null) {
            totalLongUrlAccessService = new TotalLongUrlAccessServiceImpl();
        }
        return totalLongUrlAccessService;
    }
    private ServiceInfoService getServiceInfoService()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new ServiceInfoServiceImpl();
        }
        return serviceInfoService;
    }
    private FiveTenService getFiveTenService()
    {
        if(fiveTenService == null) {
            fiveTenService = new FiveTenServiceImpl();
        }
        return fiveTenService;
    }
    // etc. ...

    
    private DataPurgeCronManager()
    {
        init();
    }
    private void init()
    {
        // TBD: ...
    }

    // Initialization-on-demand holder.
    private static class RefreshCronManagerHolder
    {
        private static final DataPurgeCronManager INSTANCE = new DataPurgeCronManager();
    }
    // Singleton method
    public static DataPurgeCronManager getInstance()
    {
        return RefreshCronManagerHolder.INSTANCE;
    }
    
    
    public int processDeletion(String entity, int deltaHours, int maxCount)
    {
        if(deltaHours <= 0) {
            deltaHours = DEFAULT_DELTA_HOURS;
        } else if(deltaHours > MAXIMUM_DELTA_HOURS) {
            deltaHours = MAXIMUM_DELTA_HOURS;
        }
        if(maxCount <= 0) {
            maxCount = DEFAULT_MAX_COUNT;            
        } else if(maxCount > MAXIMUM_MAX_COUNT) {
            maxCount = MAXIMUM_MAX_COUNT;            
        }

        // temporary
        int cnt = 0;
        switch(entity) {
        case "ApiConsumer":
            cnt = deleteApiConsumers(entity, deltaHours, maxCount);
            break;
        case "User":
            cnt = deleteUsers(entity, deltaHours, maxCount);
            break;
        case "AccessTallyMaster":
            cnt = deleteAccessTallyMasters(entity, deltaHours, maxCount);
            break;
        case "AccessTallyStatus":
            cnt = deleteAccessTallyStatuses(entity, deltaHours, maxCount);
            break;
        case "MonthlyShortUrlAccess":
            cnt = deleteMonthlyShortUrlAccesses(entity, deltaHours, maxCount);
            break;
        case "WeeklyShortUrlAccess":
            cnt = deleteWeeklyShortUrlAccesses(entity, deltaHours, maxCount);
            break;
        case "DailyShortUrlAccess":
            cnt = deleteDailyShortUrlAccesses(entity, deltaHours, maxCount);
            break;
        case "HourlyShortUrlAccess":
            cnt = deleteHourlyShortUrlAccesses(entity, deltaHours, maxCount);
            break;
        case "CumulativeShortUrlAccess":
            cnt = deleteCumulativeShortUrlAccesses(entity, deltaHours, maxCount);
            break;
        case "CurrentShortUrlAccess":
            cnt = deleteCurrentShortUrlAccesses(entity, deltaHours, maxCount);
            break;
        case "TotalShortUrlAccess":
            cnt = deleteTotalShortUrlAccesses(entity, deltaHours, maxCount);
            break;
        case "TotalLongUrlAccess":
            cnt = deleteTotalLongUrlAccesses(entity, deltaHours, maxCount);
            break;
        case "ServiceInfo":
            cnt = deleteServiceInfos(entity, deltaHours, maxCount);
            break;
        case "FiveTen":
            cnt = deleteFiveTens(entity, deltaHours, maxCount);
            break;
        default:
            log.warning("Unsupported entity type for bulk deletion: " + entity);
        }
        return cnt;
    }


    // Purge based on createdTime.
    // Note that for many entity types, createdTime may not be the best choice.
    // TBD: Pass order/filtering as args?
    

    public int deleteApiConsumers(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getApiConsumerService().deleteApiConsumers(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getApiConsumerService().findApiConsumerKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getApiConsumerService().deleteApiConsumer(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete ApiConsumer: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete ApiConsumers.", e);
        }
        return cnt;
    }

    public int deleteUsers(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getUserService().deleteUsers(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getUserService().findUserKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getUserService().deleteUser(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete User: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete Users.", e);
        }
        return cnt;
    }

    public int deleteAccessTallyMasters(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getAccessTallyMasterService().deleteAccessTallyMasters(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getAccessTallyMasterService().findAccessTallyMasterKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getAccessTallyMasterService().deleteAccessTallyMaster(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete AccessTallyMaster: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete AccessTallyMasters.", e);
        }
        return cnt;
    }

    public int deleteAccessTallyStatuses(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getAccessTallyStatusService().deleteAccessTallyStatuses(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getAccessTallyStatusService().findAccessTallyStatusKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getAccessTallyStatusService().deleteAccessTallyStatus(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete AccessTallyStatus: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete AccessTallyStatuses.", e);
        }
        return cnt;
    }

    public int deleteMonthlyShortUrlAccesses(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getMonthlyShortUrlAccessService().deleteMonthlyShortUrlAccesses(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getMonthlyShortUrlAccessService().findMonthlyShortUrlAccessKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getMonthlyShortUrlAccessService().deleteMonthlyShortUrlAccess(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete MonthlyShortUrlAccess: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete MonthlyShortUrlAccesses.", e);
        }
        return cnt;
    }

    public int deleteWeeklyShortUrlAccesses(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getWeeklyShortUrlAccessService().deleteWeeklyShortUrlAccesses(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getWeeklyShortUrlAccessService().findWeeklyShortUrlAccessKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getWeeklyShortUrlAccessService().deleteWeeklyShortUrlAccess(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete WeeklyShortUrlAccess: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete WeeklyShortUrlAccesses.", e);
        }
        return cnt;
    }

    public int deleteDailyShortUrlAccesses(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getDailyShortUrlAccessService().deleteDailyShortUrlAccesses(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getDailyShortUrlAccessService().findDailyShortUrlAccessKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getDailyShortUrlAccessService().deleteDailyShortUrlAccess(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete DailyShortUrlAccess: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete DailyShortUrlAccesses.", e);
        }
        return cnt;
    }

    public int deleteHourlyShortUrlAccesses(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getHourlyShortUrlAccessService().deleteHourlyShortUrlAccesses(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getHourlyShortUrlAccessService().findHourlyShortUrlAccessKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getHourlyShortUrlAccessService().deleteHourlyShortUrlAccess(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete HourlyShortUrlAccess: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete HourlyShortUrlAccesses.", e);
        }
        return cnt;
    }

    public int deleteCumulativeShortUrlAccesses(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getCumulativeShortUrlAccessService().deleteCumulativeShortUrlAccesses(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getCumulativeShortUrlAccessService().findCumulativeShortUrlAccessKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getCumulativeShortUrlAccessService().deleteCumulativeShortUrlAccess(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete CumulativeShortUrlAccess: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete CumulativeShortUrlAccesses.", e);
        }
        return cnt;
    }

    public int deleteCurrentShortUrlAccesses(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getCurrentShortUrlAccessService().deleteCurrentShortUrlAccesses(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getCurrentShortUrlAccessService().findCurrentShortUrlAccessKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getCurrentShortUrlAccessService().deleteCurrentShortUrlAccess(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete CurrentShortUrlAccess: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete CurrentShortUrlAccesses.", e);
        }
        return cnt;
    }

    public int deleteTotalShortUrlAccesses(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getTotalShortUrlAccessService().deleteTotalShortUrlAccesses(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getTotalShortUrlAccessService().findTotalShortUrlAccessKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getTotalShortUrlAccessService().deleteTotalShortUrlAccess(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete TotalShortUrlAccess: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete TotalShortUrlAccesses.", e);
        }
        return cnt;
    }

    public int deleteTotalLongUrlAccesses(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getTotalLongUrlAccessService().deleteTotalLongUrlAccesses(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getTotalLongUrlAccessService().findTotalLongUrlAccessKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getTotalLongUrlAccessService().deleteTotalLongUrlAccess(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete TotalLongUrlAccess: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete TotalLongUrlAccesses.", e);
        }
        return cnt;
    }

    public int deleteServiceInfos(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getServiceInfoService().deleteServiceInfos(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getServiceInfoService().findServiceInfoKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getServiceInfoService().deleteServiceInfo(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete ServiceInfo: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete ServiceInfos.", e);
        }
        return cnt;
    }

    public int deleteFiveTens(String entity, int deltaHours, int maxCount)
    {
        log.finer("Deleting entities: " + entity);
        int cnt = 0;
        try {
            long now = System.currentTimeMillis();
            long cutoff = now - (deltaHours * 3600 * 1000L);
            String filter = "createdTime <= " + cutoff;
            String ordering = "createdTime asc";
            // TBD:
//            Long deletedCount = getFiveTenService().deleteFiveTens(filter, null, null);
//            if(deletedCount != null) {
//                cnt = deletedCount.intValue();
//            }
            List<String> keys = getFiveTenService().findFiveTenKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            if(keys != null && !keys.isEmpty()) {
                for(String key : keys) {
                    try {
                        // TBD: Check updatedTime (or, viewed/used time?) ????
                        Boolean suc = getFiveTenService().deleteFiveTen(key);
                        if(Boolean.TRUE.equals(suc)) {
                            ++cnt;
                        }
                    } catch (BaseException e) {
                        // Ignore this, and move on to the next record.
                        log.log(Level.WARNING, "Failed to delete FiveTen: key = " + key, e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to delete FiveTens.", e);
        }
        return cnt;
    }


}
