package com.urltally.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ResourceAlreadyPresentException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.DataStoreRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.bean.MonthlyShortUrlAccessBean;
import com.urltally.ws.stub.MonthlyShortUrlAccessListStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessStub;
import com.urltally.ws.resource.ServiceManager;
import com.urltally.ws.resource.MonthlyShortUrlAccessResource;

// MockMonthlyShortUrlAccessResource is a decorator.
// It can be used as a base class to mock MonthlyShortUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/monthlyShortUrlAccesses/")
public abstract class MockMonthlyShortUrlAccessResource implements MonthlyShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockMonthlyShortUrlAccessResource.class.getName());

    // MockMonthlyShortUrlAccessResource uses the decorator design pattern.
    private MonthlyShortUrlAccessResource decoratedResource;

    public MockMonthlyShortUrlAccessResource(MonthlyShortUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected MonthlyShortUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(MonthlyShortUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getMonthlyShortUrlAccessKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getMonthlyShortUrlAccessKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getMonthlyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getMonthlyShortUrlAccess(guid);
    }

    @Override
    public Response getMonthlyShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getMonthlyShortUrlAccess(guid, field);
    }

    @Override
    public Response createMonthlyShortUrlAccess(MonthlyShortUrlAccessStub monthlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createMonthlyShortUrlAccess(monthlyShortUrlAccess);
    }

    @Override
    public Response updateMonthlyShortUrlAccess(String guid, MonthlyShortUrlAccessStub monthlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateMonthlyShortUrlAccess(guid, monthlyShortUrlAccess);
    }

    @Override
    public Response updateMonthlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseResourceException
    {
        return decoratedResource.updateMonthlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
    }

    @Override
    public Response deleteMonthlyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteMonthlyShortUrlAccess(guid);
    }

    @Override
    public Response deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteMonthlyShortUrlAccesses(filter, params, values);
    }


}
