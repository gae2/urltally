package com.urltally.ws.dao;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.data.MonthlyShortUrlAccessDataObject;


// TBD: Add offset/count to getAllMonthlyShortUrlAccesses() and findMonthlyShortUrlAccesses(), etc.
public interface MonthlyShortUrlAccessDAO
{
    MonthlyShortUrlAccessDataObject getMonthlyShortUrlAccess(String guid) throws BaseException;
    List<MonthlyShortUrlAccessDataObject> getMonthlyShortUrlAccesses(List<String> guids) throws BaseException;
    List<MonthlyShortUrlAccessDataObject> getAllMonthlyShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<MonthlyShortUrlAccessDataObject> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<MonthlyShortUrlAccessDataObject> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<MonthlyShortUrlAccessDataObject> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<MonthlyShortUrlAccessDataObject> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<MonthlyShortUrlAccessDataObject> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<MonthlyShortUrlAccessDataObject> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createMonthlyShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return MonthlyShortUrlAccessDataObject?)
    String createMonthlyShortUrlAccess(MonthlyShortUrlAccessDataObject monthlyShortUrlAccess) throws BaseException;          // Returns Guid.  (Return MonthlyShortUrlAccessDataObject?)
    //Boolean updateMonthlyShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateMonthlyShortUrlAccess(MonthlyShortUrlAccessDataObject monthlyShortUrlAccess) throws BaseException;
    Boolean deleteMonthlyShortUrlAccess(String guid) throws BaseException;
    Boolean deleteMonthlyShortUrlAccess(MonthlyShortUrlAccessDataObject monthlyShortUrlAccess) throws BaseException;
    Long deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;
}
