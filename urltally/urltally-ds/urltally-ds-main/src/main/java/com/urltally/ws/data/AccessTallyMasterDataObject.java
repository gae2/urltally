package com.urltally.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.util.CommonUtil;
import com.urltally.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class AccessTallyMasterDataObject extends KeyedDataObject implements AccessTallyMaster
{
    private static final Logger log = Logger.getLogger(AccessTallyMasterDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(AccessTallyMasterDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(AccessTallyMasterDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String tallyType;

    @Persistent(defaultFetchGroup = "true")
    private String tallyTime;

    @Persistent(defaultFetchGroup = "true")
    private Long tallyEpoch;

    @Persistent(defaultFetchGroup = "true")
    private String tallyStatus;

    @Persistent(defaultFetchGroup = "true")
    private Integer accessRecordCount;

    @Persistent(defaultFetchGroup = "true")
    private Long procesingStartedTime;

    public AccessTallyMasterDataObject()
    {
        this(null);
    }
    public AccessTallyMasterDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public AccessTallyMasterDataObject(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime)
    {
        this(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime, null, null);
    }
    public AccessTallyMasterDataObject(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.tallyType = tallyType;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.tallyStatus = tallyStatus;
        this.accessRecordCount = accessRecordCount;
        this.procesingStartedTime = procesingStartedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return AccessTallyMasterDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return AccessTallyMasterDataObject.composeKey(getGuid());
    }

    public String getTallyType()
    {
        return this.tallyType;
    }
    public void setTallyType(String tallyType)
    {
        this.tallyType = tallyType;
    }

    public String getTallyTime()
    {
        return this.tallyTime;
    }
    public void setTallyTime(String tallyTime)
    {
        this.tallyTime = tallyTime;
    }

    public Long getTallyEpoch()
    {
        return this.tallyEpoch;
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        this.tallyEpoch = tallyEpoch;
    }

    public String getTallyStatus()
    {
        return this.tallyStatus;
    }
    public void setTallyStatus(String tallyStatus)
    {
        this.tallyStatus = tallyStatus;
    }

    public Integer getAccessRecordCount()
    {
        return this.accessRecordCount;
    }
    public void setAccessRecordCount(Integer accessRecordCount)
    {
        this.accessRecordCount = accessRecordCount;
    }

    public Long getProcesingStartedTime()
    {
        return this.procesingStartedTime;
    }
    public void setProcesingStartedTime(Long procesingStartedTime)
    {
        this.procesingStartedTime = procesingStartedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("tallyType", this.tallyType);
        dataMap.put("tallyTime", this.tallyTime);
        dataMap.put("tallyEpoch", this.tallyEpoch);
        dataMap.put("tallyStatus", this.tallyStatus);
        dataMap.put("accessRecordCount", this.accessRecordCount);
        dataMap.put("procesingStartedTime", this.procesingStartedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        AccessTallyMaster thatObj = (AccessTallyMaster) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.tallyType == null && thatObj.getTallyType() != null)
            || (this.tallyType != null && thatObj.getTallyType() == null)
            || !this.tallyType.equals(thatObj.getTallyType()) ) {
            return false;
        }
        if( (this.tallyTime == null && thatObj.getTallyTime() != null)
            || (this.tallyTime != null && thatObj.getTallyTime() == null)
            || !this.tallyTime.equals(thatObj.getTallyTime()) ) {
            return false;
        }
        if( (this.tallyEpoch == null && thatObj.getTallyEpoch() != null)
            || (this.tallyEpoch != null && thatObj.getTallyEpoch() == null)
            || !this.tallyEpoch.equals(thatObj.getTallyEpoch()) ) {
            return false;
        }
        if( (this.tallyStatus == null && thatObj.getTallyStatus() != null)
            || (this.tallyStatus != null && thatObj.getTallyStatus() == null)
            || !this.tallyStatus.equals(thatObj.getTallyStatus()) ) {
            return false;
        }
        if( (this.accessRecordCount == null && thatObj.getAccessRecordCount() != null)
            || (this.accessRecordCount != null && thatObj.getAccessRecordCount() == null)
            || !this.accessRecordCount.equals(thatObj.getAccessRecordCount()) ) {
            return false;
        }
        if( (this.procesingStartedTime == null && thatObj.getProcesingStartedTime() != null)
            || (this.procesingStartedTime != null && thatObj.getProcesingStartedTime() == null)
            || !this.procesingStartedTime.equals(thatObj.getProcesingStartedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyType == null ? 0 : tallyType.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyTime == null ? 0 : tallyTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyEpoch == null ? 0 : tallyEpoch.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyStatus == null ? 0 : tallyStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = accessRecordCount == null ? 0 : accessRecordCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = procesingStartedTime == null ? 0 : procesingStartedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
