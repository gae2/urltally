package com.urltally.ws.service;

import java.util.List;

import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface WeeklyShortUrlAccessService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    WeeklyShortUrlAccess getWeeklyShortUrlAccess(String guid) throws BaseException;
    Object getWeeklyShortUrlAccess(String guid, String field) throws BaseException;
    List<WeeklyShortUrlAccess> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException;
    List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createWeeklyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException;
    //String createWeeklyShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return WeeklyShortUrlAccess?)
    String createWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException;          // Returns Guid.  (Return WeeklyShortUrlAccess?)
    Boolean updateWeeklyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException;
    //Boolean updateWeeklyShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException;
    Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException;
    Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException;
    Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;

//    Integer createWeeklyShortUrlAccesses(List<WeeklyShortUrlAccess> weeklyShortUrlAccesses) throws BaseException;
//    Boolean updateeWeeklyShortUrlAccesses(List<WeeklyShortUrlAccess> weeklyShortUrlAccesses) throws BaseException;

}
