package com.urltally.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.bean.TotalShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.TotalShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.TotalShortUrlAccessService;


// TotalShortUrlAccessMockService is a decorator.
// It can be used as a base class to mock TotalShortUrlAccessService objects.
public abstract class TotalShortUrlAccessMockService implements TotalShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessMockService.class.getName());

    // TotalShortUrlAccessMockService uses the decorator design pattern.
    private TotalShortUrlAccessService decoratedService;

    public TotalShortUrlAccessMockService(TotalShortUrlAccessService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TotalShortUrlAccessService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TotalShortUrlAccessService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TotalShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TotalShortUrlAccess getTotalShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTotalShortUrlAccess(): guid = " + guid);
        TotalShortUrlAccess bean = decoratedService.getTotalShortUrlAccess(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTotalShortUrlAccess(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTotalShortUrlAccess(guid, field);
        return obj;
    }

    @Override
    public List<TotalShortUrlAccess> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getTotalShortUrlAccesses()");
        List<TotalShortUrlAccess> totalShortUrlAccesses = decoratedService.getTotalShortUrlAccesses(guids);
        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses() throws BaseException
    {
        return getAllTotalShortUrlAccesses(null, null, null);
    }


    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TotalShortUrlAccess> totalShortUrlAccesses = decoratedService.getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessMockService.findTotalShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TotalShortUrlAccess> totalShortUrlAccesses = decoratedService.findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessMockService.findTotalShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        return decoratedService.createTotalShortUrlAccess(tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
    }

    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTotalShortUrlAccess(totalShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        return decoratedService.updateTotalShortUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
    }
        
    @Override
    public Boolean updateTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTotalShortUrlAccess(totalShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTotalShortUrlAccess(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTotalShortUrlAccess(totalShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTotalShortUrlAccesses(filter, params, values);
        return count;
    }

}
