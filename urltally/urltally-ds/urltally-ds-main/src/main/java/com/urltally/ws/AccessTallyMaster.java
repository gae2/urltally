package com.urltally.ws;



public interface AccessTallyMaster 
{
    String  getGuid();
    String  getTallyType();
    String  getTallyTime();
    Long  getTallyEpoch();
    String  getTallyStatus();
    Integer  getAccessRecordCount();
    Long  getProcesingStartedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
