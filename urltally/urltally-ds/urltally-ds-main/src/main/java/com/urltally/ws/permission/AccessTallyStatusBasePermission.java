package com.urltally.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class AccessTallyStatusBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AccessTallyStatusBasePermission.class.getName());

    public AccessTallyStatusBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "AccessTallyStatus::" + action;
    }


}
