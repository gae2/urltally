package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.MonthlyShortUrlAccessDAO;
import com.urltally.ws.data.MonthlyShortUrlAccessDataObject;


// MockMonthlyShortUrlAccessDAO is a decorator.
// It can be used as a base class to mock MonthlyShortUrlAccessDAO objects.
public abstract class MockMonthlyShortUrlAccessDAO implements MonthlyShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(MockMonthlyShortUrlAccessDAO.class.getName()); 

    // MockMonthlyShortUrlAccessDAO uses the decorator design pattern.
    private MonthlyShortUrlAccessDAO decoratedDAO;

    public MockMonthlyShortUrlAccessDAO(MonthlyShortUrlAccessDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected MonthlyShortUrlAccessDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(MonthlyShortUrlAccessDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public MonthlyShortUrlAccessDataObject getMonthlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.getMonthlyShortUrlAccess(guid);
	}

    @Override
    public List<MonthlyShortUrlAccessDataObject> getMonthlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedDAO.getMonthlyShortUrlAccesses(guids);
    }

    @Override
    public List<MonthlyShortUrlAccessDataObject> getAllMonthlyShortUrlAccesses() throws BaseException
	{
	    return getAllMonthlyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<MonthlyShortUrlAccessDataObject> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllMonthlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccessDataObject> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllMonthlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<MonthlyShortUrlAccessDataObject> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<MonthlyShortUrlAccessDataObject> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<MonthlyShortUrlAccessDataObject> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<MonthlyShortUrlAccessDataObject> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createMonthlyShortUrlAccess(MonthlyShortUrlAccessDataObject monthlyShortUrlAccess) throws BaseException
    {
        return decoratedDAO.createMonthlyShortUrlAccess( monthlyShortUrlAccess);
    }

    @Override
	public Boolean updateMonthlyShortUrlAccess(MonthlyShortUrlAccessDataObject monthlyShortUrlAccess) throws BaseException
	{
        return decoratedDAO.updateMonthlyShortUrlAccess(monthlyShortUrlAccess);
	}
	
    @Override
    public Boolean deleteMonthlyShortUrlAccess(MonthlyShortUrlAccessDataObject monthlyShortUrlAccess) throws BaseException
    {
        return decoratedDAO.deleteMonthlyShortUrlAccess(monthlyShortUrlAccess);
    }

    @Override
    public Boolean deleteMonthlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.deleteMonthlyShortUrlAccess(guid);
	}

    @Override
    public Long deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteMonthlyShortUrlAccesses(filter, params, values);
    }

}
