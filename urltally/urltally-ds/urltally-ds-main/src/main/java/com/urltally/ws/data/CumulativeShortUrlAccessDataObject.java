package com.urltally.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.util.CommonUtil;
import com.urltally.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class CumulativeShortUrlAccessDataObject extends ShortUrlAccessDataObject implements CumulativeShortUrlAccess
{
    private static final Logger log = Logger.getLogger(CumulativeShortUrlAccessDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(CumulativeShortUrlAccessDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(CumulativeShortUrlAccessDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String startDayHour;

    @Persistent(defaultFetchGroup = "true")
    private String endDayHour;

    @Persistent(defaultFetchGroup = "true")
    private Long startTime;

    @Persistent(defaultFetchGroup = "true")
    private Long endTime;

    public CumulativeShortUrlAccessDataObject()
    {
        this(null);
    }
    public CumulativeShortUrlAccessDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public CumulativeShortUrlAccessDataObject(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime)
    {
        this(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime, null, null);
    }
    public CumulativeShortUrlAccessDataObject(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime, Long createdTime, Long modifiedTime)
    {
        super(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, createdTime, modifiedTime);
        this.startDayHour = startDayHour;
        this.endDayHour = endDayHour;
        this.startTime = startTime;
        this.endTime = endTime;
    }

//    @Override
//    protected Key createKey()
//    {
//        return CumulativeShortUrlAccessDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return CumulativeShortUrlAccessDataObject.composeKey(getGuid());
    }

    public String getStartDayHour()
    {
        return this.startDayHour;
    }
    public void setStartDayHour(String startDayHour)
    {
        this.startDayHour = startDayHour;
    }

    public String getEndDayHour()
    {
        return this.endDayHour;
    }
    public void setEndDayHour(String endDayHour)
    {
        this.endDayHour = endDayHour;
    }

    public Long getStartTime()
    {
        return this.startTime;
    }
    public void setStartTime(Long startTime)
    {
        this.startTime = startTime;
    }

    public Long getEndTime()
    {
        return this.endTime;
    }
    public void setEndTime(Long endTime)
    {
        this.endTime = endTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("startDayHour", this.startDayHour);
        dataMap.put("endDayHour", this.endDayHour);
        dataMap.put("startTime", this.startTime);
        dataMap.put("endTime", this.endTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        CumulativeShortUrlAccess thatObj = (CumulativeShortUrlAccess) obj;
        if( (this.startDayHour == null && thatObj.getStartDayHour() != null)
            || (this.startDayHour != null && thatObj.getStartDayHour() == null)
            || !this.startDayHour.equals(thatObj.getStartDayHour()) ) {
            return false;
        }
        if( (this.endDayHour == null && thatObj.getEndDayHour() != null)
            || (this.endDayHour != null && thatObj.getEndDayHour() == null)
            || !this.endDayHour.equals(thatObj.getEndDayHour()) ) {
            return false;
        }
        if( (this.startTime == null && thatObj.getStartTime() != null)
            || (this.startTime != null && thatObj.getStartTime() == null)
            || !this.startTime.equals(thatObj.getStartTime()) ) {
            return false;
        }
        if( (this.endTime == null && thatObj.getEndTime() != null)
            || (this.endTime != null && thatObj.getEndTime() == null)
            || !this.endTime.equals(thatObj.getEndTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = startDayHour == null ? 0 : startDayHour.hashCode();
        _hash = 31 * _hash + delta;
        delta = endDayHour == null ? 0 : endDayHour.hashCode();
        _hash = 31 * _hash + delta;
        delta = startTime == null ? 0 : startTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = endTime == null ? 0 : endTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
