package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.AccessTallyMasterDAO;
import com.urltally.ws.data.AccessTallyMasterDataObject;


// MockAccessTallyMasterDAO is a decorator.
// It can be used as a base class to mock AccessTallyMasterDAO objects.
public abstract class MockAccessTallyMasterDAO implements AccessTallyMasterDAO
{
    private static final Logger log = Logger.getLogger(MockAccessTallyMasterDAO.class.getName()); 

    // MockAccessTallyMasterDAO uses the decorator design pattern.
    private AccessTallyMasterDAO decoratedDAO;

    public MockAccessTallyMasterDAO(AccessTallyMasterDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected AccessTallyMasterDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(AccessTallyMasterDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public AccessTallyMasterDataObject getAccessTallyMaster(String guid) throws BaseException
    {
        return decoratedDAO.getAccessTallyMaster(guid);
	}

    @Override
    public List<AccessTallyMasterDataObject> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        return decoratedDAO.getAccessTallyMasters(guids);
    }

    @Override
    public List<AccessTallyMasterDataObject> getAllAccessTallyMasters() throws BaseException
	{
	    return getAllAccessTallyMasters(null, null, null);
    }


    @Override
    public List<AccessTallyMasterDataObject> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyMasterDataObject> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<AccessTallyMasterDataObject> findAccessTallyMasters(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findAccessTallyMasters(filter, ordering, params, values, null, null);
    }

    @Override
	public List<AccessTallyMasterDataObject> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findAccessTallyMasters(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<AccessTallyMasterDataObject> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<AccessTallyMasterDataObject> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createAccessTallyMaster(AccessTallyMasterDataObject accessTallyMaster) throws BaseException
    {
        return decoratedDAO.createAccessTallyMaster( accessTallyMaster);
    }

    @Override
	public Boolean updateAccessTallyMaster(AccessTallyMasterDataObject accessTallyMaster) throws BaseException
	{
        return decoratedDAO.updateAccessTallyMaster(accessTallyMaster);
	}
	
    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMasterDataObject accessTallyMaster) throws BaseException
    {
        return decoratedDAO.deleteAccessTallyMaster(accessTallyMaster);
    }

    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        return decoratedDAO.deleteAccessTallyMaster(guid);
	}

    @Override
    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteAccessTallyMasters(filter, params, values);
    }

}
