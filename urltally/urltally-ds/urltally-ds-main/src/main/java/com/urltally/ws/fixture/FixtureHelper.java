package com.urltally.ws.fixture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.config.Config;
import com.urltally.ws.BaseException;
import com.urltally.ws.bean.ApiConsumerBean;
import com.urltally.ws.bean.UserBean;
import com.urltally.ws.bean.AccessTallyMasterBean;
import com.urltally.ws.bean.AccessTallyStatusBean;
import com.urltally.ws.bean.MonthlyShortUrlAccessBean;
import com.urltally.ws.bean.WeeklyShortUrlAccessBean;
import com.urltally.ws.bean.DailyShortUrlAccessBean;
import com.urltally.ws.bean.HourlyShortUrlAccessBean;
import com.urltally.ws.bean.CumulativeShortUrlAccessBean;
import com.urltally.ws.bean.CurrentShortUrlAccessBean;
import com.urltally.ws.bean.TotalShortUrlAccessBean;
import com.urltally.ws.bean.TotalLongUrlAccessBean;
import com.urltally.ws.bean.ServiceInfoBean;
import com.urltally.ws.bean.FiveTenBean;
import com.urltally.ws.stub.ApiConsumerStub;
import com.urltally.ws.stub.UserStub;
import com.urltally.ws.stub.AccessTallyMasterStub;
import com.urltally.ws.stub.AccessTallyStatusStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessStub;
import com.urltally.ws.stub.WeeklyShortUrlAccessStub;
import com.urltally.ws.stub.DailyShortUrlAccessStub;
import com.urltally.ws.stub.HourlyShortUrlAccessStub;
import com.urltally.ws.stub.CumulativeShortUrlAccessStub;
import com.urltally.ws.stub.CurrentShortUrlAccessStub;
import com.urltally.ws.stub.TotalShortUrlAccessStub;
import com.urltally.ws.stub.TotalLongUrlAccessStub;
import com.urltally.ws.stub.ServiceInfoStub;
import com.urltally.ws.stub.FiveTenStub;
import com.urltally.ws.stub.ApiConsumerListStub;
import com.urltally.ws.stub.UserListStub;
import com.urltally.ws.stub.AccessTallyMasterListStub;
import com.urltally.ws.stub.AccessTallyStatusListStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessListStub;
import com.urltally.ws.stub.WeeklyShortUrlAccessListStub;
import com.urltally.ws.stub.DailyShortUrlAccessListStub;
import com.urltally.ws.stub.HourlyShortUrlAccessListStub;
import com.urltally.ws.stub.CumulativeShortUrlAccessListStub;
import com.urltally.ws.stub.CurrentShortUrlAccessListStub;
import com.urltally.ws.stub.TotalShortUrlAccessListStub;
import com.urltally.ws.stub.TotalLongUrlAccessListStub;
import com.urltally.ws.stub.ServiceInfoListStub;
import com.urltally.ws.stub.FiveTenListStub;
import com.urltally.ws.resource.ServiceManager;
import com.urltally.ws.resource.impl.ApiConsumerResourceImpl;
import com.urltally.ws.resource.impl.UserResourceImpl;
import com.urltally.ws.resource.impl.AccessTallyMasterResourceImpl;
import com.urltally.ws.resource.impl.AccessTallyStatusResourceImpl;
import com.urltally.ws.resource.impl.MonthlyShortUrlAccessResourceImpl;
import com.urltally.ws.resource.impl.WeeklyShortUrlAccessResourceImpl;
import com.urltally.ws.resource.impl.DailyShortUrlAccessResourceImpl;
import com.urltally.ws.resource.impl.HourlyShortUrlAccessResourceImpl;
import com.urltally.ws.resource.impl.CumulativeShortUrlAccessResourceImpl;
import com.urltally.ws.resource.impl.CurrentShortUrlAccessResourceImpl;
import com.urltally.ws.resource.impl.TotalShortUrlAccessResourceImpl;
import com.urltally.ws.resource.impl.TotalLongUrlAccessResourceImpl;
import com.urltally.ws.resource.impl.ServiceInfoResourceImpl;
import com.urltally.ws.resource.impl.FiveTenResourceImpl;


// "Helper" functions...
// Get the list of fixture files,
// Read them, and
// Load the data into the datastore, etc....
// See the note in FixtureUtil...
public class FixtureHelper
{
    private static final Logger log = Logger.getLogger(FixtureHelper.class.getName());

    // ???
    private static final String CONFIG_KEY_FIXTURE_LOAD = "urltally.fixture.load";
    private static final String CONFIG_KEY_FIXTURE_DIR = "urltally.fixture.directory";
    // ...

    // Dummy var.
    protected boolean dummyFalse1 = false;
    // ...

    
    private FixtureHelper()
    {
        // ...
    }
    
    // Initialization-on-demand holder.
    private static class FixtureHelperHolder
    {
        private static final FixtureHelper INSTANCE = new FixtureHelper();
    }

    // Singleton method
    public static FixtureHelper getInstance()
    {
        return FixtureHelperHolder.INSTANCE;
    }


    public boolean isFixtureLoad()
    {
        // temporary
        return Config.getInstance().getBoolean(CONFIG_KEY_FIXTURE_LOAD, FixtureUtil.getDefaultFixtureLoad());
    }

    public String getFixtureDirName()
    {
        // temporary
        return Config.getInstance().getString(CONFIG_KEY_FIXTURE_DIR, FixtureUtil.getDefaultFixtureDir());
    }

    
    // TBD
    private List<FixtureFile> buildFixtureFileList()
    {
        List<FixtureFile> list = new ArrayList<FixtureFile>();

        String fixtureDirName = getFixtureDirName();
        File fixtureDir = new File(fixtureDirName);
        if(!fixtureDir.exists() || !fixtureDir.isDirectory()) {
            // error. what to do????
            if(log.isLoggable(Level.WARNING)) log.warning("Fixture file directory does not exist. fixtureDirName = " + fixtureDirName);
        } else {
            File[] files = fixtureDir.listFiles();
            for(File f : files) {
                if(f.isFile()) {
                    String filePath = f.getPath();  // ???
                    FixtureFile ff = new FixtureFile(filePath);
                    list.add(ff);
                } else {
                    // This should not happen. Ignore...                    
                }
            }
        }
        
        return list;
    }
    
    public int processFixtureFiles()
    {
        List<FixtureFile> list = buildFixtureFileList();
        if(log.isLoggable(Level.FINE)) {
            for(FixtureFile f : list) {
                log.fine("FixtureFile f = " + f);
            }
        }
        
        int count = 0;
        for(FixtureFile f : list) {  // list cannot be null.
            // [1] Read the file
            // [2] Convert the file content to objects
            // [3] Then, save it. (We use update/overwrite rather than create to ensure "idempotency".)
            String filePath = f.getFilePath();
            String objectType = f.getObjectType();
            String mediaType = f.getMediaType();
            
            // JSON only, for now....
            if(! FixtureUtil.MEDIA_TYPE_JSON.equals(mediaType)) {
                if(log.isLoggable(Level.WARNING)) log.warning("Currently supports only Json fixture files. Skipping filePath = " + filePath);
                continue;
            }

            BufferedReader reader = null;
            File inputFile = new File(filePath);
            if(inputFile.canRead()) {
                try {
                    reader = new BufferedReader(new FileReader(inputFile));
                    StringBuilder sb = new StringBuilder(0x1000);
                    final char[] buf = new char[0x1000];
                    int read = -1;
                    do {
                        read = reader.read(buf, 0, buf.length);
                        if (read>0) {
                            sb.append(buf, 0, read);
                        }
                    } while (read>=0);

                    if(dummyFalse1) {
                    } else if("ApiConsumer".equals(objectType)) {
                        ApiConsumerStub stub = ApiConsumerStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ApiConsumerBean bean = ApiConsumerResourceImpl.convertApiConsumerStubToBean(stub);
                        boolean suc = ServiceManager.getApiConsumerService().updateApiConsumer(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ApiConsumerList".equals(objectType)) {
                        ApiConsumerListStub listStub = ApiConsumerListStub.fromJsonString(sb.toString());
                        List<ApiConsumerBean> beanList = ApiConsumerResourceImpl.convertApiConsumerListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ApiConsumerBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getApiConsumerService().updateApiConsumer(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("User".equals(objectType)) {
                        UserStub stub = UserStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        UserBean bean = UserResourceImpl.convertUserStubToBean(stub);
                        boolean suc = ServiceManager.getUserService().updateUser(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("UserList".equals(objectType)) {
                        UserListStub listStub = UserListStub.fromJsonString(sb.toString());
                        List<UserBean> beanList = UserResourceImpl.convertUserListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(UserBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getUserService().updateUser(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("AccessTallyMaster".equals(objectType)) {
                        AccessTallyMasterStub stub = AccessTallyMasterStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        AccessTallyMasterBean bean = AccessTallyMasterResourceImpl.convertAccessTallyMasterStubToBean(stub);
                        boolean suc = ServiceManager.getAccessTallyMasterService().updateAccessTallyMaster(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("AccessTallyMasterList".equals(objectType)) {
                        AccessTallyMasterListStub listStub = AccessTallyMasterListStub.fromJsonString(sb.toString());
                        List<AccessTallyMasterBean> beanList = AccessTallyMasterResourceImpl.convertAccessTallyMasterListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(AccessTallyMasterBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getAccessTallyMasterService().updateAccessTallyMaster(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("AccessTallyStatus".equals(objectType)) {
                        AccessTallyStatusStub stub = AccessTallyStatusStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        AccessTallyStatusBean bean = AccessTallyStatusResourceImpl.convertAccessTallyStatusStubToBean(stub);
                        boolean suc = ServiceManager.getAccessTallyStatusService().updateAccessTallyStatus(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("AccessTallyStatusList".equals(objectType)) {
                        AccessTallyStatusListStub listStub = AccessTallyStatusListStub.fromJsonString(sb.toString());
                        List<AccessTallyStatusBean> beanList = AccessTallyStatusResourceImpl.convertAccessTallyStatusListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(AccessTallyStatusBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getAccessTallyStatusService().updateAccessTallyStatus(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("MonthlyShortUrlAccess".equals(objectType)) {
                        MonthlyShortUrlAccessStub stub = MonthlyShortUrlAccessStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        MonthlyShortUrlAccessBean bean = MonthlyShortUrlAccessResourceImpl.convertMonthlyShortUrlAccessStubToBean(stub);
                        boolean suc = ServiceManager.getMonthlyShortUrlAccessService().updateMonthlyShortUrlAccess(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("MonthlyShortUrlAccessList".equals(objectType)) {
                        MonthlyShortUrlAccessListStub listStub = MonthlyShortUrlAccessListStub.fromJsonString(sb.toString());
                        List<MonthlyShortUrlAccessBean> beanList = MonthlyShortUrlAccessResourceImpl.convertMonthlyShortUrlAccessListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(MonthlyShortUrlAccessBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getMonthlyShortUrlAccessService().updateMonthlyShortUrlAccess(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("WeeklyShortUrlAccess".equals(objectType)) {
                        WeeklyShortUrlAccessStub stub = WeeklyShortUrlAccessStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        WeeklyShortUrlAccessBean bean = WeeklyShortUrlAccessResourceImpl.convertWeeklyShortUrlAccessStubToBean(stub);
                        boolean suc = ServiceManager.getWeeklyShortUrlAccessService().updateWeeklyShortUrlAccess(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("WeeklyShortUrlAccessList".equals(objectType)) {
                        WeeklyShortUrlAccessListStub listStub = WeeklyShortUrlAccessListStub.fromJsonString(sb.toString());
                        List<WeeklyShortUrlAccessBean> beanList = WeeklyShortUrlAccessResourceImpl.convertWeeklyShortUrlAccessListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(WeeklyShortUrlAccessBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getWeeklyShortUrlAccessService().updateWeeklyShortUrlAccess(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("DailyShortUrlAccess".equals(objectType)) {
                        DailyShortUrlAccessStub stub = DailyShortUrlAccessStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        DailyShortUrlAccessBean bean = DailyShortUrlAccessResourceImpl.convertDailyShortUrlAccessStubToBean(stub);
                        boolean suc = ServiceManager.getDailyShortUrlAccessService().updateDailyShortUrlAccess(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("DailyShortUrlAccessList".equals(objectType)) {
                        DailyShortUrlAccessListStub listStub = DailyShortUrlAccessListStub.fromJsonString(sb.toString());
                        List<DailyShortUrlAccessBean> beanList = DailyShortUrlAccessResourceImpl.convertDailyShortUrlAccessListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(DailyShortUrlAccessBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getDailyShortUrlAccessService().updateDailyShortUrlAccess(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("HourlyShortUrlAccess".equals(objectType)) {
                        HourlyShortUrlAccessStub stub = HourlyShortUrlAccessStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        HourlyShortUrlAccessBean bean = HourlyShortUrlAccessResourceImpl.convertHourlyShortUrlAccessStubToBean(stub);
                        boolean suc = ServiceManager.getHourlyShortUrlAccessService().updateHourlyShortUrlAccess(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("HourlyShortUrlAccessList".equals(objectType)) {
                        HourlyShortUrlAccessListStub listStub = HourlyShortUrlAccessListStub.fromJsonString(sb.toString());
                        List<HourlyShortUrlAccessBean> beanList = HourlyShortUrlAccessResourceImpl.convertHourlyShortUrlAccessListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(HourlyShortUrlAccessBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getHourlyShortUrlAccessService().updateHourlyShortUrlAccess(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("CumulativeShortUrlAccess".equals(objectType)) {
                        CumulativeShortUrlAccessStub stub = CumulativeShortUrlAccessStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        CumulativeShortUrlAccessBean bean = CumulativeShortUrlAccessResourceImpl.convertCumulativeShortUrlAccessStubToBean(stub);
                        boolean suc = ServiceManager.getCumulativeShortUrlAccessService().updateCumulativeShortUrlAccess(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("CumulativeShortUrlAccessList".equals(objectType)) {
                        CumulativeShortUrlAccessListStub listStub = CumulativeShortUrlAccessListStub.fromJsonString(sb.toString());
                        List<CumulativeShortUrlAccessBean> beanList = CumulativeShortUrlAccessResourceImpl.convertCumulativeShortUrlAccessListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(CumulativeShortUrlAccessBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getCumulativeShortUrlAccessService().updateCumulativeShortUrlAccess(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("CurrentShortUrlAccess".equals(objectType)) {
                        CurrentShortUrlAccessStub stub = CurrentShortUrlAccessStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        CurrentShortUrlAccessBean bean = CurrentShortUrlAccessResourceImpl.convertCurrentShortUrlAccessStubToBean(stub);
                        boolean suc = ServiceManager.getCurrentShortUrlAccessService().updateCurrentShortUrlAccess(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("CurrentShortUrlAccessList".equals(objectType)) {
                        CurrentShortUrlAccessListStub listStub = CurrentShortUrlAccessListStub.fromJsonString(sb.toString());
                        List<CurrentShortUrlAccessBean> beanList = CurrentShortUrlAccessResourceImpl.convertCurrentShortUrlAccessListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(CurrentShortUrlAccessBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getCurrentShortUrlAccessService().updateCurrentShortUrlAccess(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TotalShortUrlAccess".equals(objectType)) {
                        TotalShortUrlAccessStub stub = TotalShortUrlAccessStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TotalShortUrlAccessBean bean = TotalShortUrlAccessResourceImpl.convertTotalShortUrlAccessStubToBean(stub);
                        boolean suc = ServiceManager.getTotalShortUrlAccessService().updateTotalShortUrlAccess(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TotalShortUrlAccessList".equals(objectType)) {
                        TotalShortUrlAccessListStub listStub = TotalShortUrlAccessListStub.fromJsonString(sb.toString());
                        List<TotalShortUrlAccessBean> beanList = TotalShortUrlAccessResourceImpl.convertTotalShortUrlAccessListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TotalShortUrlAccessBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTotalShortUrlAccessService().updateTotalShortUrlAccess(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TotalLongUrlAccess".equals(objectType)) {
                        TotalLongUrlAccessStub stub = TotalLongUrlAccessStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        TotalLongUrlAccessBean bean = TotalLongUrlAccessResourceImpl.convertTotalLongUrlAccessStubToBean(stub);
                        boolean suc = ServiceManager.getTotalLongUrlAccessService().updateTotalLongUrlAccess(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("TotalLongUrlAccessList".equals(objectType)) {
                        TotalLongUrlAccessListStub listStub = TotalLongUrlAccessListStub.fromJsonString(sb.toString());
                        List<TotalLongUrlAccessBean> beanList = TotalLongUrlAccessResourceImpl.convertTotalLongUrlAccessListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(TotalLongUrlAccessBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getTotalLongUrlAccessService().updateTotalLongUrlAccess(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfo".equals(objectType)) {
                        ServiceInfoStub stub = ServiceInfoStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        ServiceInfoBean bean = ServiceInfoResourceImpl.convertServiceInfoStubToBean(stub);
                        boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("ServiceInfoList".equals(objectType)) {
                        ServiceInfoListStub listStub = ServiceInfoListStub.fromJsonString(sb.toString());
                        List<ServiceInfoBean> beanList = ServiceInfoResourceImpl.convertServiceInfoListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(ServiceInfoBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getServiceInfoService().updateServiceInfo(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTen".equals(objectType)) {
                        FiveTenStub stub = FiveTenStub.fromJsonString(sb.toString());
                        String guid = stub.getGuid();
                        if(guid == null || guid.isEmpty()) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping filePath = " + filePath);
                            continue;
                        }
                        FiveTenBean bean = FiveTenResourceImpl.convertFiveTenStubToBean(stub);
                        boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                        if(suc) {
                            count++;
                            if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data for filePath = " + filePath);
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else if("FiveTenList".equals(objectType)) {
                        FiveTenListStub listStub = FiveTenListStub.fromJsonString(sb.toString());
                        List<FiveTenBean> beanList = FiveTenResourceImpl.convertFiveTenListStubToBeanList(listStub);
                        if(beanList != null && !beanList.isEmpty()) {
                            for(FiveTenBean bean : beanList) {
                                String guid = bean.getGuid();
                                if(guid == null || guid.isEmpty()) {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Fixture data requires an explicitly set guid. Skipping a bean from filePath = " + filePath);
                                    continue;
                                }
                                boolean suc = ServiceManager.getFiveTenService().updateFiveTen(bean);
                                if(suc) {
                                    count++;
                                    if(log.isLoggable(Level.FINER)) log.finer("Successfully processed fixture data: guid = " + guid + " for filePath = " + filePath);
                                } else {
                                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: guid = " + guid + " for filePath = " + filePath);
                                }
                            }
                        } else {
                            if(log.isLoggable(Level.WARNING)) log.warning("Failed to process a fixture data: filePath = " + filePath);
                        }
                    } else {
                        // This cannot happen
                    }
                } catch (FileNotFoundException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (IOException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to read a fixture file: filePath = " + filePath, e);
                } catch (BaseException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process a fixture file: filePath = " + filePath, e);
                } catch (Exception e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected error while processing a fixture file: filePath = " + filePath, e);
                } finally {
                    if(reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Error while closing the fixture file: filePath = " + filePath, e);
                        }
                    }
                }                
            } else {
                if(log.isLoggable(Level.WARNING)) log.warning("Skipping a fixture file because it is not readable: filePath = " + filePath);
            }
        }
                
        return count;
    }
    
}
