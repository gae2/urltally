package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.ws.data.TotalLongUrlAccessDataObject;

public class TotalLongUrlAccessBean extends BeanBase implements TotalLongUrlAccess
{
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessBean.class.getName());

    // Embedded data object.
    private TotalLongUrlAccessDataObject dobj = null;

    public TotalLongUrlAccessBean()
    {
        this(new TotalLongUrlAccessDataObject());
    }
    public TotalLongUrlAccessBean(String guid)
    {
        this(new TotalLongUrlAccessDataObject(guid));
    }
    public TotalLongUrlAccessBean(TotalLongUrlAccessDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public TotalLongUrlAccessDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
        }
    }

    public String getTallyType()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyType(String tallyType)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyType(tallyType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
        }
    }

    public String getTallyTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyTime(String tallyTime)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyTime(tallyTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
        }
    }

    public Long getTallyEpoch()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyEpoch();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyEpoch(tallyEpoch);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
        }
    }

    public Integer getCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setCount(Integer count)
    {
        if(getDataObject() != null) {
            getDataObject().setCount(count);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
        }
    }

    public String getLongUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrl(longUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
        }
    }

    public String getLongUrlDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrlDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrlDomain(longUrlDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TotalLongUrlAccessDataObject is null!");
        }
    }


    // TBD
    public TotalLongUrlAccessDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
