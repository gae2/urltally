package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.bean.HourlyShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.HourlyShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.HourlyShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class HourlyShortUrlAccessServiceImpl implements HourlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(HourlyShortUrlAccessServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // HourlyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public HourlyShortUrlAccess getHourlyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        HourlyShortUrlAccessDataObject dataObj = getDAOFactory().getHourlyShortUrlAccessDAO().getHourlyShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve HourlyShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        HourlyShortUrlAccessBean bean = new HourlyShortUrlAccessBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getHourlyShortUrlAccess(String guid, String field) throws BaseException
    {
        HourlyShortUrlAccessDataObject dataObj = getDAOFactory().getHourlyShortUrlAccessDAO().getHourlyShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve HourlyShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("count")) {
            return dataObj.getCount();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return dataObj.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return dataObj.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return dataObj.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return dataObj.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return dataObj.getUserAgent();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("country")) {
            return dataObj.getCountry();
        } else if(field.equals("talliedTime")) {
            return dataObj.getTalliedTime();
        } else if(field.equals("year")) {
            return dataObj.getYear();
        } else if(field.equals("day")) {
            return dataObj.getDay();
        } else if(field.equals("hour")) {
            return dataObj.getHour();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<HourlyShortUrlAccess> getHourlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<HourlyShortUrlAccess> list = new ArrayList<HourlyShortUrlAccess>();
        List<HourlyShortUrlAccessDataObject> dataObjs = getDAOFactory().getHourlyShortUrlAccessDAO().getHourlyShortUrlAccesses(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve HourlyShortUrlAccessDataObject list.");
        } else {
            Iterator<HourlyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                HourlyShortUrlAccessDataObject dataObj = (HourlyShortUrlAccessDataObject) it.next();
                list.add(new HourlyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses() throws BaseException
    {
        return getAllHourlyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllHourlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllHourlyShortUrlAccesses(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<HourlyShortUrlAccess> list = new ArrayList<HourlyShortUrlAccess>();
        List<HourlyShortUrlAccessDataObject> dataObjs = getDAOFactory().getHourlyShortUrlAccessDAO().getAllHourlyShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve HourlyShortUrlAccessDataObject list.");
        } else {
            Iterator<HourlyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                HourlyShortUrlAccessDataObject dataObj = (HourlyShortUrlAccessDataObject) it.next();
                list.add(new HourlyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllHourlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllHourlyShortUrlAccessKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getHourlyShortUrlAccessDAO().getAllHourlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve HourlyShortUrlAccess key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("HourlyShortUrlAccessServiceImpl.findHourlyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<HourlyShortUrlAccess> list = new ArrayList<HourlyShortUrlAccess>();
        List<HourlyShortUrlAccessDataObject> dataObjs = getDAOFactory().getHourlyShortUrlAccessDAO().findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find hourlyShortUrlAccesses for the given criterion.");
        } else {
            Iterator<HourlyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                HourlyShortUrlAccessDataObject dataObj = (HourlyShortUrlAccessDataObject) it.next();
                list.add(new HourlyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("HourlyShortUrlAccessServiceImpl.findHourlyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getHourlyShortUrlAccessDAO().findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find HourlyShortUrlAccess keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("HourlyShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getHourlyShortUrlAccessDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createHourlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        HourlyShortUrlAccessDataObject dataObj = new HourlyShortUrlAccessDataObject(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
        return createHourlyShortUrlAccess(dataObj);
    }

    @Override
    public String createHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param hourlyShortUrlAccess cannot be null.....
        if(hourlyShortUrlAccess == null) {
            log.log(Level.INFO, "Param hourlyShortUrlAccess is null!");
            throw new BadRequestException("Param hourlyShortUrlAccess object is null!");
        }
        HourlyShortUrlAccessDataObject dataObj = null;
        if(hourlyShortUrlAccess instanceof HourlyShortUrlAccessDataObject) {
            dataObj = (HourlyShortUrlAccessDataObject) hourlyShortUrlAccess;
        } else if(hourlyShortUrlAccess instanceof HourlyShortUrlAccessBean) {
            dataObj = ((HourlyShortUrlAccessBean) hourlyShortUrlAccess).toDataObject();
        } else {  // if(hourlyShortUrlAccess instanceof HourlyShortUrlAccess)
            //dataObj = new HourlyShortUrlAccessDataObject(null, hourlyShortUrlAccess.getTallyTime(), hourlyShortUrlAccess.getTallyEpoch(), hourlyShortUrlAccess.getCount(), hourlyShortUrlAccess.getShortUrl(), hourlyShortUrlAccess.getShortUrlDomain(), hourlyShortUrlAccess.getLongUrl(), hourlyShortUrlAccess.getLongUrlDomain(), hourlyShortUrlAccess.getRedirectType(), hourlyShortUrlAccess.getRefererDomain(), hourlyShortUrlAccess.getUserAgent(), hourlyShortUrlAccess.getLanguage(), hourlyShortUrlAccess.getCountry(), hourlyShortUrlAccess.getTalliedTime(), hourlyShortUrlAccess.getYear(), hourlyShortUrlAccess.getDay(), hourlyShortUrlAccess.getHour());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new HourlyShortUrlAccessDataObject(hourlyShortUrlAccess.getGuid(), hourlyShortUrlAccess.getTallyTime(), hourlyShortUrlAccess.getTallyEpoch(), hourlyShortUrlAccess.getCount(), hourlyShortUrlAccess.getShortUrl(), hourlyShortUrlAccess.getShortUrlDomain(), hourlyShortUrlAccess.getLongUrl(), hourlyShortUrlAccess.getLongUrlDomain(), hourlyShortUrlAccess.getRedirectType(), hourlyShortUrlAccess.getRefererDomain(), hourlyShortUrlAccess.getUserAgent(), hourlyShortUrlAccess.getLanguage(), hourlyShortUrlAccess.getCountry(), hourlyShortUrlAccess.getTalliedTime(), hourlyShortUrlAccess.getYear(), hourlyShortUrlAccess.getDay(), hourlyShortUrlAccess.getHour());
        }
        String guid = getDAOFactory().getHourlyShortUrlAccessDAO().createHourlyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        HourlyShortUrlAccessDataObject dataObj = new HourlyShortUrlAccessDataObject(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
        return updateHourlyShortUrlAccess(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param hourlyShortUrlAccess cannot be null.....
        if(hourlyShortUrlAccess == null || hourlyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param hourlyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param hourlyShortUrlAccess object or its guid is null!");
        }
        HourlyShortUrlAccessDataObject dataObj = null;
        if(hourlyShortUrlAccess instanceof HourlyShortUrlAccessDataObject) {
            dataObj = (HourlyShortUrlAccessDataObject) hourlyShortUrlAccess;
        } else if(hourlyShortUrlAccess instanceof HourlyShortUrlAccessBean) {
            dataObj = ((HourlyShortUrlAccessBean) hourlyShortUrlAccess).toDataObject();
        } else {  // if(hourlyShortUrlAccess instanceof HourlyShortUrlAccess)
            dataObj = new HourlyShortUrlAccessDataObject(hourlyShortUrlAccess.getGuid(), hourlyShortUrlAccess.getTallyTime(), hourlyShortUrlAccess.getTallyEpoch(), hourlyShortUrlAccess.getCount(), hourlyShortUrlAccess.getShortUrl(), hourlyShortUrlAccess.getShortUrlDomain(), hourlyShortUrlAccess.getLongUrl(), hourlyShortUrlAccess.getLongUrlDomain(), hourlyShortUrlAccess.getRedirectType(), hourlyShortUrlAccess.getRefererDomain(), hourlyShortUrlAccess.getUserAgent(), hourlyShortUrlAccess.getLanguage(), hourlyShortUrlAccess.getCountry(), hourlyShortUrlAccess.getTalliedTime(), hourlyShortUrlAccess.getYear(), hourlyShortUrlAccess.getDay(), hourlyShortUrlAccess.getHour());
        }
        Boolean suc = getDAOFactory().getHourlyShortUrlAccessDAO().updateHourlyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getHourlyShortUrlAccessDAO().deleteHourlyShortUrlAccess(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param hourlyShortUrlAccess cannot be null.....
        if(hourlyShortUrlAccess == null || hourlyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param hourlyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param hourlyShortUrlAccess object or its guid is null!");
        }
        HourlyShortUrlAccessDataObject dataObj = null;
        if(hourlyShortUrlAccess instanceof HourlyShortUrlAccessDataObject) {
            dataObj = (HourlyShortUrlAccessDataObject) hourlyShortUrlAccess;
        } else if(hourlyShortUrlAccess instanceof HourlyShortUrlAccessBean) {
            dataObj = ((HourlyShortUrlAccessBean) hourlyShortUrlAccess).toDataObject();
        } else {  // if(hourlyShortUrlAccess instanceof HourlyShortUrlAccess)
            dataObj = new HourlyShortUrlAccessDataObject(hourlyShortUrlAccess.getGuid(), hourlyShortUrlAccess.getTallyTime(), hourlyShortUrlAccess.getTallyEpoch(), hourlyShortUrlAccess.getCount(), hourlyShortUrlAccess.getShortUrl(), hourlyShortUrlAccess.getShortUrlDomain(), hourlyShortUrlAccess.getLongUrl(), hourlyShortUrlAccess.getLongUrlDomain(), hourlyShortUrlAccess.getRedirectType(), hourlyShortUrlAccess.getRefererDomain(), hourlyShortUrlAccess.getUserAgent(), hourlyShortUrlAccess.getLanguage(), hourlyShortUrlAccess.getCountry(), hourlyShortUrlAccess.getTalliedTime(), hourlyShortUrlAccess.getYear(), hourlyShortUrlAccess.getDay(), hourlyShortUrlAccess.getHour());
        }
        Boolean suc = getDAOFactory().getHourlyShortUrlAccessDAO().deleteHourlyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getHourlyShortUrlAccessDAO().deleteHourlyShortUrlAccesses(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
