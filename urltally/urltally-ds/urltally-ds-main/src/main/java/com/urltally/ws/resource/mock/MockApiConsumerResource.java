package com.urltally.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ResourceAlreadyPresentException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.DataStoreRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.ApiConsumer;
import com.urltally.ws.bean.ApiConsumerBean;
import com.urltally.ws.stub.ApiConsumerListStub;
import com.urltally.ws.stub.ApiConsumerStub;
import com.urltally.ws.resource.ServiceManager;
import com.urltally.ws.resource.ApiConsumerResource;

// MockApiConsumerResource is a decorator.
// It can be used as a base class to mock ApiConsumerResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/apiConsumers/")
public abstract class MockApiConsumerResource implements ApiConsumerResource
{
    private static final Logger log = Logger.getLogger(MockApiConsumerResource.class.getName());

    // MockApiConsumerResource uses the decorator design pattern.
    private ApiConsumerResource decoratedResource;

    public MockApiConsumerResource(ApiConsumerResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected ApiConsumerResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(ApiConsumerResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllApiConsumers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllApiConsumers(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllApiConsumerKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllApiConsumerKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findApiConsumerKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findApiConsumerKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findApiConsumers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findApiConsumers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getApiConsumerKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getApiConsumerKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getApiConsumer(String guid) throws BaseResourceException
    {
        return decoratedResource.getApiConsumer(guid);
    }

    @Override
    public Response getApiConsumer(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getApiConsumer(guid, field);
    }

    @Override
    public Response createApiConsumer(ApiConsumerStub apiConsumer) throws BaseResourceException
    {
        return decoratedResource.createApiConsumer(apiConsumer);
    }

    @Override
    public Response updateApiConsumer(String guid, ApiConsumerStub apiConsumer) throws BaseResourceException
    {
        return decoratedResource.updateApiConsumer(guid, apiConsumer);
    }

    @Override
    public Response updateApiConsumer(String guid, String aeryId, String name, String description, String appKey, String appSecret, String status) throws BaseResourceException
    {
        return decoratedResource.updateApiConsumer(guid, aeryId, name, description, appKey, appSecret, status);
    }

    @Override
    public Response deleteApiConsumer(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteApiConsumer(guid);
    }

    @Override
    public Response deleteApiConsumers(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteApiConsumers(filter, params, values);
    }


}
