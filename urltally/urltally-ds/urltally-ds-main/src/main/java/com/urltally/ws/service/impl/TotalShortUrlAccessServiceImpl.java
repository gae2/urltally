package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.bean.TotalShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.TotalShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.TotalShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TotalShortUrlAccessServiceImpl implements TotalShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // TotalShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TotalShortUrlAccess getTotalShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TotalShortUrlAccessDataObject dataObj = getDAOFactory().getTotalShortUrlAccessDAO().getTotalShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        TotalShortUrlAccessBean bean = new TotalShortUrlAccessBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTotalShortUrlAccess(String guid, String field) throws BaseException
    {
        TotalShortUrlAccessDataObject dataObj = getDAOFactory().getTotalShortUrlAccessDAO().getTotalShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyType")) {
            return dataObj.getTallyType();
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("count")) {
            return dataObj.getCount();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return dataObj.getShortUrlDomain();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TotalShortUrlAccess> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TotalShortUrlAccess> list = new ArrayList<TotalShortUrlAccess>();
        List<TotalShortUrlAccessDataObject> dataObjs = getDAOFactory().getTotalShortUrlAccessDAO().getTotalShortUrlAccesses(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccessDataObject list.");
        } else {
            Iterator<TotalShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TotalShortUrlAccessDataObject dataObj = (TotalShortUrlAccessDataObject) it.next();
                list.add(new TotalShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses() throws BaseException
    {
        return getAllTotalShortUrlAccesses(null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalShortUrlAccesses(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TotalShortUrlAccess> list = new ArrayList<TotalShortUrlAccess>();
        List<TotalShortUrlAccessDataObject> dataObjs = getDAOFactory().getTotalShortUrlAccessDAO().getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccessDataObject list.");
        } else {
            Iterator<TotalShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TotalShortUrlAccessDataObject dataObj = (TotalShortUrlAccessDataObject) it.next();
                list.add(new TotalShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllTotalShortUrlAccessKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getTotalShortUrlAccessDAO().getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccess key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TotalShortUrlAccessServiceImpl.findTotalShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TotalShortUrlAccess> list = new ArrayList<TotalShortUrlAccess>();
        List<TotalShortUrlAccessDataObject> dataObjs = getDAOFactory().getTotalShortUrlAccessDAO().findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find totalShortUrlAccesses for the given criterion.");
        } else {
            Iterator<TotalShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TotalShortUrlAccessDataObject dataObj = (TotalShortUrlAccessDataObject) it.next();
                list.add(new TotalShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TotalShortUrlAccessServiceImpl.findTotalShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getTotalShortUrlAccessDAO().findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TotalShortUrlAccess keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TotalShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getTotalShortUrlAccessDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        TotalShortUrlAccessDataObject dataObj = new TotalShortUrlAccessDataObject(null, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        return createTotalShortUrlAccess(dataObj);
    }

    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalShortUrlAccess cannot be null.....
        if(totalShortUrlAccess == null) {
            log.log(Level.INFO, "Param totalShortUrlAccess is null!");
            throw new BadRequestException("Param totalShortUrlAccess object is null!");
        }
        TotalShortUrlAccessDataObject dataObj = null;
        if(totalShortUrlAccess instanceof TotalShortUrlAccessDataObject) {
            dataObj = (TotalShortUrlAccessDataObject) totalShortUrlAccess;
        } else if(totalShortUrlAccess instanceof TotalShortUrlAccessBean) {
            dataObj = ((TotalShortUrlAccessBean) totalShortUrlAccess).toDataObject();
        } else {  // if(totalShortUrlAccess instanceof TotalShortUrlAccess)
            //dataObj = new TotalShortUrlAccessDataObject(null, totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new TotalShortUrlAccessDataObject(totalShortUrlAccess.getGuid(), totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
        }
        String guid = getDAOFactory().getTotalShortUrlAccessDAO().createTotalShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TotalShortUrlAccessDataObject dataObj = new TotalShortUrlAccessDataObject(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        return updateTotalShortUrlAccess(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalShortUrlAccess cannot be null.....
        if(totalShortUrlAccess == null || totalShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalShortUrlAccess object or its guid is null!");
        }
        TotalShortUrlAccessDataObject dataObj = null;
        if(totalShortUrlAccess instanceof TotalShortUrlAccessDataObject) {
            dataObj = (TotalShortUrlAccessDataObject) totalShortUrlAccess;
        } else if(totalShortUrlAccess instanceof TotalShortUrlAccessBean) {
            dataObj = ((TotalShortUrlAccessBean) totalShortUrlAccess).toDataObject();
        } else {  // if(totalShortUrlAccess instanceof TotalShortUrlAccess)
            dataObj = new TotalShortUrlAccessDataObject(totalShortUrlAccess.getGuid(), totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
        }
        Boolean suc = getDAOFactory().getTotalShortUrlAccessDAO().updateTotalShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getTotalShortUrlAccessDAO().deleteTotalShortUrlAccess(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalShortUrlAccess cannot be null.....
        if(totalShortUrlAccess == null || totalShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalShortUrlAccess object or its guid is null!");
        }
        TotalShortUrlAccessDataObject dataObj = null;
        if(totalShortUrlAccess instanceof TotalShortUrlAccessDataObject) {
            dataObj = (TotalShortUrlAccessDataObject) totalShortUrlAccess;
        } else if(totalShortUrlAccess instanceof TotalShortUrlAccessBean) {
            dataObj = ((TotalShortUrlAccessBean) totalShortUrlAccess).toDataObject();
        } else {  // if(totalShortUrlAccess instanceof TotalShortUrlAccess)
            dataObj = new TotalShortUrlAccessDataObject(totalShortUrlAccess.getGuid(), totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
        }
        Boolean suc = getDAOFactory().getTotalShortUrlAccessDAO().deleteTotalShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getTotalShortUrlAccessDAO().deleteTotalShortUrlAccesses(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
