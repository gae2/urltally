package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.bean.MonthlyShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.MonthlyShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.MonthlyShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class MonthlyShortUrlAccessServiceImpl implements MonthlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MonthlyShortUrlAccessServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // MonthlyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public MonthlyShortUrlAccess getMonthlyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        MonthlyShortUrlAccessDataObject dataObj = getDAOFactory().getMonthlyShortUrlAccessDAO().getMonthlyShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        MonthlyShortUrlAccessBean bean = new MonthlyShortUrlAccessBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getMonthlyShortUrlAccess(String guid, String field) throws BaseException
    {
        MonthlyShortUrlAccessDataObject dataObj = getDAOFactory().getMonthlyShortUrlAccessDAO().getMonthlyShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("count")) {
            return dataObj.getCount();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return dataObj.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return dataObj.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return dataObj.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return dataObj.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return dataObj.getUserAgent();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("country")) {
            return dataObj.getCountry();
        } else if(field.equals("talliedTime")) {
            return dataObj.getTalliedTime();
        } else if(field.equals("year")) {
            return dataObj.getYear();
        } else if(field.equals("month")) {
            return dataObj.getMonth();
        } else if(field.equals("numberOfDays")) {
            return dataObj.getNumberOfDays();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<MonthlyShortUrlAccess> getMonthlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<MonthlyShortUrlAccess> list = new ArrayList<MonthlyShortUrlAccess>();
        List<MonthlyShortUrlAccessDataObject> dataObjs = getDAOFactory().getMonthlyShortUrlAccessDAO().getMonthlyShortUrlAccesses(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccessDataObject list.");
        } else {
            Iterator<MonthlyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                MonthlyShortUrlAccessDataObject dataObj = (MonthlyShortUrlAccessDataObject) it.next();
                list.add(new MonthlyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses() throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllMonthlyShortUrlAccesses(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<MonthlyShortUrlAccess> list = new ArrayList<MonthlyShortUrlAccess>();
        List<MonthlyShortUrlAccessDataObject> dataObjs = getDAOFactory().getMonthlyShortUrlAccessDAO().getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccessDataObject list.");
        } else {
            Iterator<MonthlyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                MonthlyShortUrlAccessDataObject dataObj = (MonthlyShortUrlAccessDataObject) it.next();
                list.add(new MonthlyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllMonthlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllMonthlyShortUrlAccessKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getMonthlyShortUrlAccessDAO().getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccess key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MonthlyShortUrlAccessServiceImpl.findMonthlyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<MonthlyShortUrlAccess> list = new ArrayList<MonthlyShortUrlAccess>();
        List<MonthlyShortUrlAccessDataObject> dataObjs = getDAOFactory().getMonthlyShortUrlAccessDAO().findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find monthlyShortUrlAccesses for the given criterion.");
        } else {
            Iterator<MonthlyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                MonthlyShortUrlAccessDataObject dataObj = (MonthlyShortUrlAccessDataObject) it.next();
                list.add(new MonthlyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MonthlyShortUrlAccessServiceImpl.findMonthlyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getMonthlyShortUrlAccessDAO().findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find MonthlyShortUrlAccess keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MonthlyShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getMonthlyShortUrlAccessDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createMonthlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        MonthlyShortUrlAccessDataObject dataObj = new MonthlyShortUrlAccessDataObject(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        return createMonthlyShortUrlAccess(dataObj);
    }

    @Override
    public String createMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param monthlyShortUrlAccess cannot be null.....
        if(monthlyShortUrlAccess == null) {
            log.log(Level.INFO, "Param monthlyShortUrlAccess is null!");
            throw new BadRequestException("Param monthlyShortUrlAccess object is null!");
        }
        MonthlyShortUrlAccessDataObject dataObj = null;
        if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessDataObject) {
            dataObj = (MonthlyShortUrlAccessDataObject) monthlyShortUrlAccess;
        } else if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessBean) {
            dataObj = ((MonthlyShortUrlAccessBean) monthlyShortUrlAccess).toDataObject();
        } else {  // if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccess)
            //dataObj = new MonthlyShortUrlAccessDataObject(null, monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new MonthlyShortUrlAccessDataObject(monthlyShortUrlAccess.getGuid(), monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
        }
        String guid = getDAOFactory().getMonthlyShortUrlAccessDAO().createMonthlyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateMonthlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        MonthlyShortUrlAccessDataObject dataObj = new MonthlyShortUrlAccessDataObject(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        return updateMonthlyShortUrlAccess(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param monthlyShortUrlAccess cannot be null.....
        if(monthlyShortUrlAccess == null || monthlyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param monthlyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param monthlyShortUrlAccess object or its guid is null!");
        }
        MonthlyShortUrlAccessDataObject dataObj = null;
        if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessDataObject) {
            dataObj = (MonthlyShortUrlAccessDataObject) monthlyShortUrlAccess;
        } else if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessBean) {
            dataObj = ((MonthlyShortUrlAccessBean) monthlyShortUrlAccess).toDataObject();
        } else {  // if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccess)
            dataObj = new MonthlyShortUrlAccessDataObject(monthlyShortUrlAccess.getGuid(), monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
        }
        Boolean suc = getDAOFactory().getMonthlyShortUrlAccessDAO().updateMonthlyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteMonthlyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getMonthlyShortUrlAccessDAO().deleteMonthlyShortUrlAccess(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param monthlyShortUrlAccess cannot be null.....
        if(monthlyShortUrlAccess == null || monthlyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param monthlyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param monthlyShortUrlAccess object or its guid is null!");
        }
        MonthlyShortUrlAccessDataObject dataObj = null;
        if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessDataObject) {
            dataObj = (MonthlyShortUrlAccessDataObject) monthlyShortUrlAccess;
        } else if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessBean) {
            dataObj = ((MonthlyShortUrlAccessBean) monthlyShortUrlAccess).toDataObject();
        } else {  // if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccess)
            dataObj = new MonthlyShortUrlAccessDataObject(monthlyShortUrlAccess.getGuid(), monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
        }
        Boolean suc = getDAOFactory().getMonthlyShortUrlAccessDAO().deleteMonthlyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getMonthlyShortUrlAccessDAO().deleteMonthlyShortUrlAccesses(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
