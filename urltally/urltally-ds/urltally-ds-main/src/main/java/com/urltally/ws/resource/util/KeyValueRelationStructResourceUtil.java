package com.urltally.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.KeyValueRelationStruct;
import com.urltally.ws.bean.KeyValueRelationStructBean;
import com.urltally.ws.stub.KeyValueRelationStructStub;


public class KeyValueRelationStructResourceUtil
{
    private static final Logger log = Logger.getLogger(KeyValueRelationStructResourceUtil.class.getName());

    // Static methods only.
    private KeyValueRelationStructResourceUtil() {}

    public static KeyValueRelationStructBean convertKeyValueRelationStructStubToBean(KeyValueRelationStruct stub)
    {
        KeyValueRelationStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null KeyValueRelationStructBean is returned.");
        } else {
            bean = new KeyValueRelationStructBean();
            bean.setUuid(stub.getUuid());
            bean.setKey(stub.getKey());
            bean.setValue(stub.getValue());
            bean.setNote(stub.getNote());
            bean.setRelation(stub.getRelation());
        }
        return bean;
    }

}
