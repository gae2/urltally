package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.CellLatitudeLongitude;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "cellLatitudeLongitudes")
@XmlType(propOrder = {"cellLatitudeLongitude", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CellLatitudeLongitudeListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeListStub.class.getName());

    private List<CellLatitudeLongitudeStub> cellLatitudeLongitudes = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public CellLatitudeLongitudeListStub()
    {
        this(new ArrayList<CellLatitudeLongitudeStub>());
    }
    public CellLatitudeLongitudeListStub(List<CellLatitudeLongitudeStub> cellLatitudeLongitudes)
    {
        this(cellLatitudeLongitudes, null);
    }
    public CellLatitudeLongitudeListStub(List<CellLatitudeLongitudeStub> cellLatitudeLongitudes, String forwardCursor)
    {
        this.cellLatitudeLongitudes = cellLatitudeLongitudes;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(cellLatitudeLongitudes == null) {
            return true;
        } else {
            return cellLatitudeLongitudes.isEmpty();
        }
    }
    public int getSize()
    {
        if(cellLatitudeLongitudes == null) {
            return 0;
        } else {
            return cellLatitudeLongitudes.size();
        }
    }


    @XmlElement(name = "cellLatitudeLongitude")
    public List<CellLatitudeLongitudeStub> getCellLatitudeLongitude()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<CellLatitudeLongitudeStub> getList()
    {
        return cellLatitudeLongitudes;
    }
    public void setList(List<CellLatitudeLongitudeStub> cellLatitudeLongitudes)
    {
        this.cellLatitudeLongitudes = cellLatitudeLongitudes;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<CellLatitudeLongitudeStub> it = this.cellLatitudeLongitudes.iterator();
        while(it.hasNext()) {
            CellLatitudeLongitudeStub cellLatitudeLongitude = it.next();
            sb.append(cellLatitudeLongitude.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CellLatitudeLongitudeListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of CellLatitudeLongitudeListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CellLatitudeLongitudeListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CellLatitudeLongitudeListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CellLatitudeLongitudeListStub object as a string.", e);
        }
        
        return null;
    }
    public static CellLatitudeLongitudeListStub fromJsonString(String jsonStr)
    {
        try {
            CellLatitudeLongitudeListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CellLatitudeLongitudeListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CellLatitudeLongitudeListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CellLatitudeLongitudeListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CellLatitudeLongitudeListStub object.", e);
        }
        
        return null;
    }

}
