package com.urltally.ws.platform.gae;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.platform.PlatformServiceFactory;
import com.urltally.ws.platform.AppIdentityPlatformService;
import com.urltally.ws.platform.ConfigPlatformService;
import com.urltally.ws.platform.UserPlatformService;
import com.urltally.ws.platform.OAuthPlatformService;
import com.urltally.ws.platform.MemcachePlatformService;
import com.urltally.ws.platform.MailPlatformService;
import com.urltally.ws.platform.MessagingPlatformService;


public class GaePlatformServiceFactory extends PlatformServiceFactory
{
    private static final Logger log = Logger.getLogger(GaePlatformServiceFactory.class.getName());

    private GaePlatformServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class GaePlatformServiceFactoryHolder
    {
        private static final GaePlatformServiceFactory INSTANCE = new GaePlatformServiceFactory();
    }

    // Singleton method
    public static GaePlatformServiceFactory getInstance()
    {
        return GaePlatformServiceFactoryHolder.INSTANCE;
    }


    // Platform Services

    public AppIdentityPlatformService getAppIdentityPlatformService()
    {
        return GaeAppIdentityPlatformService.getInstance();
    }

    public ConfigPlatformService getConfigPlatformService()
    {
        return new GaeConfigPlatformService();
    }

    public UserPlatformService getUserPlatformService()
    {
        return GaeUserPlatformService.getInstance();
    }

    public OAuthPlatformService getOAuthPlatformService()
    {
        return GaeOAuthPlatformService.getInstance();
    }

    public MemcachePlatformService getMemcachePlatformService()
    {
        return new GaeMemcachePlatformService();
    }

    public MailPlatformService getMailPlatformService()
    {
        return new GaeMailPlatformService();
    }

    public MessagingPlatformService getMessagingPlatformService()
    {
        return new GaeMessagingPlatformService();
    }


}
