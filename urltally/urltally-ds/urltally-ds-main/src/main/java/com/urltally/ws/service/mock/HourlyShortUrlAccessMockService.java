package com.urltally.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.bean.HourlyShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.HourlyShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.HourlyShortUrlAccessService;


// HourlyShortUrlAccessMockService is a decorator.
// It can be used as a base class to mock HourlyShortUrlAccessService objects.
public abstract class HourlyShortUrlAccessMockService implements HourlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(HourlyShortUrlAccessMockService.class.getName());

    // HourlyShortUrlAccessMockService uses the decorator design pattern.
    private HourlyShortUrlAccessService decoratedService;

    public HourlyShortUrlAccessMockService(HourlyShortUrlAccessService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected HourlyShortUrlAccessService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(HourlyShortUrlAccessService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // HourlyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public HourlyShortUrlAccess getHourlyShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getHourlyShortUrlAccess(): guid = " + guid);
        HourlyShortUrlAccess bean = decoratedService.getHourlyShortUrlAccess(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getHourlyShortUrlAccess(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getHourlyShortUrlAccess(guid, field);
        return obj;
    }

    @Override
    public List<HourlyShortUrlAccess> getHourlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getHourlyShortUrlAccesses()");
        List<HourlyShortUrlAccess> hourlyShortUrlAccesses = decoratedService.getHourlyShortUrlAccesses(guids);
        log.finer("END");
        return hourlyShortUrlAccesses;
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses() throws BaseException
    {
        return getAllHourlyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllHourlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllHourlyShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<HourlyShortUrlAccess> hourlyShortUrlAccesses = decoratedService.getAllHourlyShortUrlAccesses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return hourlyShortUrlAccesses;
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllHourlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllHourlyShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllHourlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("HourlyShortUrlAccessMockService.findHourlyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<HourlyShortUrlAccess> hourlyShortUrlAccesses = decoratedService.findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return hourlyShortUrlAccesses;
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("HourlyShortUrlAccessMockService.findHourlyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("HourlyShortUrlAccessMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createHourlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {
        return decoratedService.createHourlyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
    }

    @Override
    public String createHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createHourlyShortUrlAccess(hourlyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {
        return decoratedService.updateHourlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
    }
        
    @Override
    public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateHourlyShortUrlAccess(hourlyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteHourlyShortUrlAccess(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteHourlyShortUrlAccess(hourlyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteHourlyShortUrlAccesses(filter, params, values);
        return count;
    }

}
