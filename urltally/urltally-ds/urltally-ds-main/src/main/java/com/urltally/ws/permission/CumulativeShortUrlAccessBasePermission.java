package com.urltally.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class CumulativeShortUrlAccessBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CumulativeShortUrlAccessBasePermission.class.getName());

    public CumulativeShortUrlAccessBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "CumulativeShortUrlAccess::" + action;
    }

    @Override
    public boolean isCreatePermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isReadPermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isUpdatePermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isDeletePermissionRequired()
    {
        return true;
    }

}
