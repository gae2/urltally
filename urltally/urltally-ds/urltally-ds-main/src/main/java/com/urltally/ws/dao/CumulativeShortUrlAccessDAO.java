package com.urltally.ws.dao;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.data.CumulativeShortUrlAccessDataObject;


// TBD: Add offset/count to getAllCumulativeShortUrlAccesses() and findCumulativeShortUrlAccesses(), etc.
public interface CumulativeShortUrlAccessDAO
{
    CumulativeShortUrlAccessDataObject getCumulativeShortUrlAccess(String guid) throws BaseException;
    List<CumulativeShortUrlAccessDataObject> getCumulativeShortUrlAccesses(List<String> guids) throws BaseException;
    List<CumulativeShortUrlAccessDataObject> getAllCumulativeShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<CumulativeShortUrlAccessDataObject> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<CumulativeShortUrlAccessDataObject> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<CumulativeShortUrlAccessDataObject> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<CumulativeShortUrlAccessDataObject> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<CumulativeShortUrlAccessDataObject> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<CumulativeShortUrlAccessDataObject> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createCumulativeShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return CumulativeShortUrlAccessDataObject?)
    String createCumulativeShortUrlAccess(CumulativeShortUrlAccessDataObject cumulativeShortUrlAccess) throws BaseException;          // Returns Guid.  (Return CumulativeShortUrlAccessDataObject?)
    //Boolean updateCumulativeShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccessDataObject cumulativeShortUrlAccess) throws BaseException;
    Boolean deleteCumulativeShortUrlAccess(String guid) throws BaseException;
    Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccessDataObject cumulativeShortUrlAccess) throws BaseException;
    Long deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;
}
