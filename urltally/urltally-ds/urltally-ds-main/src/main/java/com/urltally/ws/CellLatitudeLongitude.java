package com.urltally.ws;



public interface CellLatitudeLongitude 
{
    Integer  getScale();
    Integer  getLatitude();
    Integer  getLongitude();
    boolean isEmpty();
}
