package com.urltally.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.bean.CumulativeShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.CumulativeShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.CumulativeShortUrlAccessService;


// CumulativeShortUrlAccessMockService is a decorator.
// It can be used as a base class to mock CumulativeShortUrlAccessService objects.
public abstract class CumulativeShortUrlAccessMockService implements CumulativeShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(CumulativeShortUrlAccessMockService.class.getName());

    // CumulativeShortUrlAccessMockService uses the decorator design pattern.
    private CumulativeShortUrlAccessService decoratedService;

    public CumulativeShortUrlAccessMockService(CumulativeShortUrlAccessService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected CumulativeShortUrlAccessService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(CumulativeShortUrlAccessService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // CumulativeShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public CumulativeShortUrlAccess getCumulativeShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getCumulativeShortUrlAccess(): guid = " + guid);
        CumulativeShortUrlAccess bean = decoratedService.getCumulativeShortUrlAccess(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getCumulativeShortUrlAccess(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getCumulativeShortUrlAccess(guid, field);
        return obj;
    }

    @Override
    public List<CumulativeShortUrlAccess> getCumulativeShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getCumulativeShortUrlAccesses()");
        List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses = decoratedService.getCumulativeShortUrlAccesses(guids);
        log.finer("END");
        return cumulativeShortUrlAccesses;
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses() throws BaseException
    {
        return getAllCumulativeShortUrlAccesses(null, null, null);
    }


    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCumulativeShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllCumulativeShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses = decoratedService.getAllCumulativeShortUrlAccesses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return cumulativeShortUrlAccesses;
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCumulativeShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllCumulativeShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllCumulativeShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("CumulativeShortUrlAccessMockService.findCumulativeShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses = decoratedService.findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return cumulativeShortUrlAccesses;
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("CumulativeShortUrlAccessMockService.findCumulativeShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("CumulativeShortUrlAccessMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createCumulativeShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {
        return decoratedService.createCumulativeShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
    }

    @Override
    public String createCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createCumulativeShortUrlAccess(cumulativeShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateCumulativeShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {
        return decoratedService.updateCumulativeShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
    }
        
    @Override
    public Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateCumulativeShortUrlAccess(cumulativeShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteCumulativeShortUrlAccess(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteCumulativeShortUrlAccess(cumulativeShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteCumulativeShortUrlAccesses(filter, params, values);
        return count;
    }

}
