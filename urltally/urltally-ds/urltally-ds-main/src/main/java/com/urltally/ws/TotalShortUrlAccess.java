package com.urltally.ws;



public interface TotalShortUrlAccess 
{
    String  getGuid();
    String  getTallyType();
    String  getTallyTime();
    Long  getTallyEpoch();
    Integer  getCount();
    String  getShortUrl();
    String  getShortUrlDomain();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
