package com.urltally.ws.dao;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.data.HourlyShortUrlAccessDataObject;


// TBD: Add offset/count to getAllHourlyShortUrlAccesses() and findHourlyShortUrlAccesses(), etc.
public interface HourlyShortUrlAccessDAO
{
    HourlyShortUrlAccessDataObject getHourlyShortUrlAccess(String guid) throws BaseException;
    List<HourlyShortUrlAccessDataObject> getHourlyShortUrlAccesses(List<String> guids) throws BaseException;
    List<HourlyShortUrlAccessDataObject> getAllHourlyShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<HourlyShortUrlAccessDataObject> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<HourlyShortUrlAccessDataObject> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createHourlyShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return HourlyShortUrlAccessDataObject?)
    String createHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException;          // Returns Guid.  (Return HourlyShortUrlAccessDataObject?)
    //Boolean updateHourlyShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException;
    Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException;
    Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException;
    Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;
}
