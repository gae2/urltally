package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.data.AccessTallyStatusDataObject;

public class AccessTallyStatusBean extends BeanBase implements AccessTallyStatus
{
    private static final Logger log = Logger.getLogger(AccessTallyStatusBean.class.getName());

    // Embedded data object.
    private AccessTallyStatusDataObject dobj = null;

    public AccessTallyStatusBean()
    {
        this(new AccessTallyStatusDataObject());
    }
    public AccessTallyStatusBean(String guid)
    {
        this(new AccessTallyStatusDataObject(guid));
    }
    public AccessTallyStatusBean(AccessTallyStatusDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public AccessTallyStatusDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
        }
    }

    public String getRemoteRecordGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getRemoteRecordGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
            return null;   // ???
        }
    }
    public void setRemoteRecordGuid(String remoteRecordGuid)
    {
        if(getDataObject() != null) {
            getDataObject().setRemoteRecordGuid(remoteRecordGuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
        }
    }

    public String getShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrl(shortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
        }
    }

    public String getTallyType()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyType(String tallyType)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyType(tallyType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
        }
    }

    public String getTallyTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyTime(String tallyTime)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyTime(tallyTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
        }
    }

    public Long getTallyEpoch()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyEpoch();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyEpoch(tallyEpoch);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
        }
    }

    public Boolean isProcessed()
    {
        if(getDataObject() != null) {
            return getDataObject().isProcessed();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
            return null;   // ???
        }
    }
    public void setProcessed(Boolean processed)
    {
        if(getDataObject() != null) {
            getDataObject().setProcessed(processed);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyStatusDataObject is null!");
        }
    }


    // TBD
    public AccessTallyStatusDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
