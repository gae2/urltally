package com.urltally.ws;



public interface TotalLongUrlAccess 
{
    String  getGuid();
    String  getTallyType();
    String  getTallyTime();
    Long  getTallyEpoch();
    Integer  getCount();
    String  getLongUrl();
    String  getLongUrlDomain();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
