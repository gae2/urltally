package com.urltally.ws;



public interface MonthlyShortUrlAccess extends ShortUrlAccess
{
    Integer  getYear();
    Integer  getMonth();
    Integer  getNumberOfDays();
}
