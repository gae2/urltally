package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "cumulativeShortUrlAccesses")
@XmlType(propOrder = {"cumulativeShortUrlAccess", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CumulativeShortUrlAccessListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CumulativeShortUrlAccessListStub.class.getName());

    private List<CumulativeShortUrlAccessStub> cumulativeShortUrlAccesses = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public CumulativeShortUrlAccessListStub()
    {
        this(new ArrayList<CumulativeShortUrlAccessStub>());
    }
    public CumulativeShortUrlAccessListStub(List<CumulativeShortUrlAccessStub> cumulativeShortUrlAccesses)
    {
        this(cumulativeShortUrlAccesses, null);
    }
    public CumulativeShortUrlAccessListStub(List<CumulativeShortUrlAccessStub> cumulativeShortUrlAccesses, String forwardCursor)
    {
        this.cumulativeShortUrlAccesses = cumulativeShortUrlAccesses;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(cumulativeShortUrlAccesses == null) {
            return true;
        } else {
            return cumulativeShortUrlAccesses.isEmpty();
        }
    }
    public int getSize()
    {
        if(cumulativeShortUrlAccesses == null) {
            return 0;
        } else {
            return cumulativeShortUrlAccesses.size();
        }
    }


    @XmlElement(name = "cumulativeShortUrlAccess")
    public List<CumulativeShortUrlAccessStub> getCumulativeShortUrlAccess()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<CumulativeShortUrlAccessStub> getList()
    {
        return cumulativeShortUrlAccesses;
    }
    public void setList(List<CumulativeShortUrlAccessStub> cumulativeShortUrlAccesses)
    {
        this.cumulativeShortUrlAccesses = cumulativeShortUrlAccesses;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<CumulativeShortUrlAccessStub> it = this.cumulativeShortUrlAccesses.iterator();
        while(it.hasNext()) {
            CumulativeShortUrlAccessStub cumulativeShortUrlAccess = it.next();
            sb.append(cumulativeShortUrlAccess.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CumulativeShortUrlAccessListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of CumulativeShortUrlAccessListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CumulativeShortUrlAccessListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CumulativeShortUrlAccessListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CumulativeShortUrlAccessListStub object as a string.", e);
        }
        
        return null;
    }
    public static CumulativeShortUrlAccessListStub fromJsonString(String jsonStr)
    {
        try {
            CumulativeShortUrlAccessListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CumulativeShortUrlAccessListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CumulativeShortUrlAccessListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CumulativeShortUrlAccessListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CumulativeShortUrlAccessListStub object.", e);
        }
        
        return null;
    }

}
