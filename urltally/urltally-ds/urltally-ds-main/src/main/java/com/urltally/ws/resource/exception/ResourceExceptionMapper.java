package com.urltally.ws.resource.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.stub.ErrorStub;


@Provider
public class ResourceExceptionMapper implements ExceptionMapper<BaseResourceException>
{
    public Response toResponse(BaseResourceException ex) 
    {
        return Response.status(Status.SERVICE_UNAVAILABLE)
            .entity(new ErrorStub(ex))
            .build();
    }
}
