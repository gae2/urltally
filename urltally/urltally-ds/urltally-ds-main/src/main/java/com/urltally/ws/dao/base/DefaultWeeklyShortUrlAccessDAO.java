package com.urltally.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.WeeklyShortUrlAccessDAO;
import com.urltally.ws.data.WeeklyShortUrlAccessDataObject;


public class DefaultWeeklyShortUrlAccessDAO extends DefaultDAOBase implements WeeklyShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(DefaultWeeklyShortUrlAccessDAO.class.getName()); 

    // Returns the weeklyShortUrlAccess for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public WeeklyShortUrlAccessDataObject getWeeklyShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        WeeklyShortUrlAccessDataObject weeklyShortUrlAccess = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = WeeklyShortUrlAccessDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = WeeklyShortUrlAccessDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                weeklyShortUrlAccess = pm.getObjectById(WeeklyShortUrlAccessDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve weeklyShortUrlAccess for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return weeklyShortUrlAccess;
	}

    @Override
    public List<WeeklyShortUrlAccessDataObject> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<WeeklyShortUrlAccessDataObject> weeklyShortUrlAccesses = null;
        if(guids != null && !guids.isEmpty()) {
            weeklyShortUrlAccesses = new ArrayList<WeeklyShortUrlAccessDataObject>();
            for(String guid : guids) {
                WeeklyShortUrlAccessDataObject obj = getWeeklyShortUrlAccess(guid); 
                weeklyShortUrlAccesses.add(obj);
            }
	    }

        log.finer("END");
        return weeklyShortUrlAccesses;
    }

    @Override
    public List<WeeklyShortUrlAccessDataObject> getAllWeeklyShortUrlAccesses() throws BaseException
	{
	    return getAllWeeklyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<WeeklyShortUrlAccessDataObject> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllWeeklyShortUrlAccesses(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<WeeklyShortUrlAccessDataObject> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<WeeklyShortUrlAccessDataObject> weeklyShortUrlAccesses = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(WeeklyShortUrlAccessDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            weeklyShortUrlAccesses = (List<WeeklyShortUrlAccessDataObject>) q.execute();
            if(weeklyShortUrlAccesses != null) {
                weeklyShortUrlAccesses.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(weeklyShortUrlAccesses);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            /*
            // ???
            Collection<WeeklyShortUrlAccessDataObject> rs_weeklyShortUrlAccesses = (Collection<WeeklyShortUrlAccessDataObject>) q.execute();
            if(rs_weeklyShortUrlAccesses == null) {
                log.log(Level.WARNING, "Failed to retrieve all weeklyShortUrlAccesses.");
                weeklyShortUrlAccesses = new ArrayList<WeeklyShortUrlAccessDataObject>();  // ???           
            } else {
                weeklyShortUrlAccesses = new ArrayList<WeeklyShortUrlAccessDataObject>(pm.detachCopyAll(rs_weeklyShortUrlAccesses));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all weeklyShortUrlAccesses.", ex);
            //weeklyShortUrlAccesses = new ArrayList<WeeklyShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all weeklyShortUrlAccesses.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return weeklyShortUrlAccesses;
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllWeeklyShortUrlAccessKeys(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + WeeklyShortUrlAccessDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all WeeklyShortUrlAccess keys.", ex);
            throw new DataStoreException("Failed to retrieve all WeeklyShortUrlAccess keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultWeeklyShortUrlAccessDAO.findWeeklyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findWeeklyShortUrlAccesses() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<WeeklyShortUrlAccessDataObject> weeklyShortUrlAccesses = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(WeeklyShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                weeklyShortUrlAccesses = (List<WeeklyShortUrlAccessDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                weeklyShortUrlAccesses = (List<WeeklyShortUrlAccessDataObject>) q.execute();
            }
            if(weeklyShortUrlAccesses != null) {
                weeklyShortUrlAccesses.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(weeklyShortUrlAccesses);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(WeeklyShortUrlAccessDataObject dobj : weeklyShortUrlAccesses) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find weeklyShortUrlAccesses because index is missing.", ex);
            //weeklyShortUrlAccesses = new ArrayList<WeeklyShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to find weeklyShortUrlAccesses because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find weeklyShortUrlAccesses meeting the criterion.", ex);
            //weeklyShortUrlAccesses = new ArrayList<WeeklyShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to find weeklyShortUrlAccesses meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return weeklyShortUrlAccesses;
	}

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultWeeklyShortUrlAccessDAO.findWeeklyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + WeeklyShortUrlAccessDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find WeeklyShortUrlAccess keys because index is missing.", ex);
            throw new DataStoreException("Failed to find WeeklyShortUrlAccess keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find WeeklyShortUrlAccess keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find WeeklyShortUrlAccess keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultWeeklyShortUrlAccessDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(WeeklyShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get weeklyShortUrlAccess count because index is missing.", ex);
            throw new DataStoreException("Failed to get weeklyShortUrlAccess count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get weeklyShortUrlAccess count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get weeklyShortUrlAccess count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the weeklyShortUrlAccess in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException
    {
        log.fine("storeWeeklyShortUrlAccess() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = weeklyShortUrlAccess.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                weeklyShortUrlAccess.setCreatedTime(createdTime);
            }
            Long modifiedTime = weeklyShortUrlAccess.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                weeklyShortUrlAccess.setModifiedTime(createdTime);
            }
            pm.makePersistent(weeklyShortUrlAccess); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = weeklyShortUrlAccess.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store weeklyShortUrlAccess because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store weeklyShortUrlAccess because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store weeklyShortUrlAccess.", ex);
            throw new DataStoreException("Failed to store weeklyShortUrlAccess.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeWeeklyShortUrlAccess(): guid = " + guid);
        return guid;
    }

    @Override
    public String createWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException
    {
        // The createdTime field will be automatically set in storeWeeklyShortUrlAccess().
        //Long createdTime = weeklyShortUrlAccess.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    weeklyShortUrlAccess.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = weeklyShortUrlAccess.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    weeklyShortUrlAccess.setModifiedTime(createdTime);
        //}
        return storeWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }

    @Override
	public Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeWeeklyShortUrlAccess()
	    // (in which case modifiedTime might be updated again).
	    weeklyShortUrlAccess.setModifiedTime(System.currentTimeMillis());
	    String guid = storeWeeklyShortUrlAccess(weeklyShortUrlAccess);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(weeklyShortUrlAccess);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete weeklyShortUrlAccess because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete weeklyShortUrlAccess because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete weeklyShortUrlAccess.", ex);
            throw new DataStoreException("Failed to delete weeklyShortUrlAccess.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = WeeklyShortUrlAccessDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = WeeklyShortUrlAccessDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                WeeklyShortUrlAccessDataObject weeklyShortUrlAccess = pm.getObjectById(WeeklyShortUrlAccessDataObject.class, key);
                pm.deletePersistent(weeklyShortUrlAccess);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete weeklyShortUrlAccess because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete weeklyShortUrlAccess because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete weeklyShortUrlAccess for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete weeklyShortUrlAccess for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultWeeklyShortUrlAccessDAO.deleteWeeklyShortUrlAccesses(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(WeeklyShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deleteweeklyShortUrlAccesses because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete weeklyShortUrlAccesses because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete weeklyShortUrlAccesses because index is missing", ex);
            throw new DataStoreException("Failed to delete weeklyShortUrlAccesses because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete weeklyShortUrlAccesses", ex);
            throw new DataStoreException("Failed to delete weeklyShortUrlAccesses", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
