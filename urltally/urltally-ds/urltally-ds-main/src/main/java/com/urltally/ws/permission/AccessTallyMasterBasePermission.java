package com.urltally.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class AccessTallyMasterBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AccessTallyMasterBasePermission.class.getName());

    public AccessTallyMasterBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "AccessTallyMaster::" + action;
    }


}
