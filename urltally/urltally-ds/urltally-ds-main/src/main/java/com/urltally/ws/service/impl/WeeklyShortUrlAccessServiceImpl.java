package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.ws.bean.WeeklyShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.WeeklyShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.WeeklyShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class WeeklyShortUrlAccessServiceImpl implements WeeklyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(WeeklyShortUrlAccessServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // WeeklyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public WeeklyShortUrlAccess getWeeklyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        WeeklyShortUrlAccessDataObject dataObj = getDAOFactory().getWeeklyShortUrlAccessDAO().getWeeklyShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        WeeklyShortUrlAccessBean bean = new WeeklyShortUrlAccessBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getWeeklyShortUrlAccess(String guid, String field) throws BaseException
    {
        WeeklyShortUrlAccessDataObject dataObj = getDAOFactory().getWeeklyShortUrlAccessDAO().getWeeklyShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("count")) {
            return dataObj.getCount();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return dataObj.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return dataObj.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return dataObj.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return dataObj.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return dataObj.getUserAgent();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("country")) {
            return dataObj.getCountry();
        } else if(field.equals("talliedTime")) {
            return dataObj.getTalliedTime();
        } else if(field.equals("year")) {
            return dataObj.getYear();
        } else if(field.equals("week")) {
            return dataObj.getWeek();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<WeeklyShortUrlAccess> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<WeeklyShortUrlAccess> list = new ArrayList<WeeklyShortUrlAccess>();
        List<WeeklyShortUrlAccessDataObject> dataObjs = getDAOFactory().getWeeklyShortUrlAccessDAO().getWeeklyShortUrlAccesses(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccessDataObject list.");
        } else {
            Iterator<WeeklyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                WeeklyShortUrlAccessDataObject dataObj = (WeeklyShortUrlAccessDataObject) it.next();
                list.add(new WeeklyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses() throws BaseException
    {
        return getAllWeeklyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllWeeklyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllWeeklyShortUrlAccesses(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<WeeklyShortUrlAccess> list = new ArrayList<WeeklyShortUrlAccess>();
        List<WeeklyShortUrlAccessDataObject> dataObjs = getDAOFactory().getWeeklyShortUrlAccessDAO().getAllWeeklyShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccessDataObject list.");
        } else {
            Iterator<WeeklyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                WeeklyShortUrlAccessDataObject dataObj = (WeeklyShortUrlAccessDataObject) it.next();
                list.add(new WeeklyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllWeeklyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllWeeklyShortUrlAccessKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getWeeklyShortUrlAccessDAO().getAllWeeklyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccess key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("WeeklyShortUrlAccessServiceImpl.findWeeklyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<WeeklyShortUrlAccess> list = new ArrayList<WeeklyShortUrlAccess>();
        List<WeeklyShortUrlAccessDataObject> dataObjs = getDAOFactory().getWeeklyShortUrlAccessDAO().findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find weeklyShortUrlAccesses for the given criterion.");
        } else {
            Iterator<WeeklyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                WeeklyShortUrlAccessDataObject dataObj = (WeeklyShortUrlAccessDataObject) it.next();
                list.add(new WeeklyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("WeeklyShortUrlAccessServiceImpl.findWeeklyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getWeeklyShortUrlAccessDAO().findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find WeeklyShortUrlAccess keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("WeeklyShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getWeeklyShortUrlAccessDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createWeeklyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        WeeklyShortUrlAccessDataObject dataObj = new WeeklyShortUrlAccessDataObject(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
        return createWeeklyShortUrlAccess(dataObj);
    }

    @Override
    public String createWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param weeklyShortUrlAccess cannot be null.....
        if(weeklyShortUrlAccess == null) {
            log.log(Level.INFO, "Param weeklyShortUrlAccess is null!");
            throw new BadRequestException("Param weeklyShortUrlAccess object is null!");
        }
        WeeklyShortUrlAccessDataObject dataObj = null;
        if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessDataObject) {
            dataObj = (WeeklyShortUrlAccessDataObject) weeklyShortUrlAccess;
        } else if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessBean) {
            dataObj = ((WeeklyShortUrlAccessBean) weeklyShortUrlAccess).toDataObject();
        } else {  // if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccess)
            //dataObj = new WeeklyShortUrlAccessDataObject(null, weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new WeeklyShortUrlAccessDataObject(weeklyShortUrlAccess.getGuid(), weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
        }
        String guid = getDAOFactory().getWeeklyShortUrlAccessDAO().createWeeklyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateWeeklyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        WeeklyShortUrlAccessDataObject dataObj = new WeeklyShortUrlAccessDataObject(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
        return updateWeeklyShortUrlAccess(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param weeklyShortUrlAccess cannot be null.....
        if(weeklyShortUrlAccess == null || weeklyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param weeklyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param weeklyShortUrlAccess object or its guid is null!");
        }
        WeeklyShortUrlAccessDataObject dataObj = null;
        if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessDataObject) {
            dataObj = (WeeklyShortUrlAccessDataObject) weeklyShortUrlAccess;
        } else if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessBean) {
            dataObj = ((WeeklyShortUrlAccessBean) weeklyShortUrlAccess).toDataObject();
        } else {  // if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccess)
            dataObj = new WeeklyShortUrlAccessDataObject(weeklyShortUrlAccess.getGuid(), weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
        }
        Boolean suc = getDAOFactory().getWeeklyShortUrlAccessDAO().updateWeeklyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getWeeklyShortUrlAccessDAO().deleteWeeklyShortUrlAccess(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param weeklyShortUrlAccess cannot be null.....
        if(weeklyShortUrlAccess == null || weeklyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param weeklyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param weeklyShortUrlAccess object or its guid is null!");
        }
        WeeklyShortUrlAccessDataObject dataObj = null;
        if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessDataObject) {
            dataObj = (WeeklyShortUrlAccessDataObject) weeklyShortUrlAccess;
        } else if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessBean) {
            dataObj = ((WeeklyShortUrlAccessBean) weeklyShortUrlAccess).toDataObject();
        } else {  // if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccess)
            dataObj = new WeeklyShortUrlAccessDataObject(weeklyShortUrlAccess.getGuid(), weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
        }
        Boolean suc = getDAOFactory().getWeeklyShortUrlAccessDAO().deleteWeeklyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getWeeklyShortUrlAccessDAO().deleteWeeklyShortUrlAccesses(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
