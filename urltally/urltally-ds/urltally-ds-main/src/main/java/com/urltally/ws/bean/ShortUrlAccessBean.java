package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.ShortUrlAccess;
import com.urltally.ws.data.ShortUrlAccessDataObject;

public abstract class ShortUrlAccessBean extends BeanBase implements ShortUrlAccess
{
    private static final Logger log = Logger.getLogger(ShortUrlAccessBean.class.getName());

    public ShortUrlAccessBean()
    {
        super();
    }

    @Override
    public abstract ShortUrlAccessDataObject getDataObject();

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getTallyTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyTime(String tallyTime)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyTime(tallyTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public Long getTallyEpoch()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyEpoch();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyEpoch(tallyEpoch);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public Integer getCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setCount(Integer count)
    {
        if(getDataObject() != null) {
            getDataObject().setCount(count);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getShortUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrl(shortUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getShortUrlDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getShortUrlDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setShortUrlDomain(shortUrlDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getLongUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrl(longUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getLongUrlDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongUrlDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setLongUrlDomain(longUrlDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getRedirectType()
    {
        if(getDataObject() != null) {
            return getDataObject().getRedirectType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getDataObject() != null) {
            getDataObject().setRedirectType(redirectType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getRefererDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getRefererDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setRefererDomain(String refererDomain)
    {
        if(getDataObject() != null) {
            getDataObject().setRefererDomain(refererDomain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getUserAgent()
    {
        if(getDataObject() != null) {
            return getDataObject().getUserAgent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setUserAgent(String userAgent)
    {
        if(getDataObject() != null) {
            getDataObject().setUserAgent(userAgent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getLanguage()
    {
        if(getDataObject() != null) {
            return getDataObject().getLanguage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setLanguage(String language)
    {
        if(getDataObject() != null) {
            getDataObject().setLanguage(language);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public String getCountry()
    {
        if(getDataObject() != null) {
            return getDataObject().getCountry();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setCountry(String country)
    {
        if(getDataObject() != null) {
            getDataObject().setCountry(country);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }

    public Long getTalliedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getTalliedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setTalliedTime(Long talliedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setTalliedTime(talliedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ShortUrlAccessDataObject is null!");
        }
    }



    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
