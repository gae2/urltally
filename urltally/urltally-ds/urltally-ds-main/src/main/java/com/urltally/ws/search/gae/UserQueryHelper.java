package com.urltally.ws.search.gae;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.SearchException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SortExpression;
import com.google.appengine.api.search.SortOptions;
import com.google.appengine.api.search.SearchException;

import com.urltally.ws.BaseException;
import com.urltally.ws.util.CommonUtil;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.GeoPointStruct;
import com.urltally.ws.StreetAddressStruct;
import com.urltally.ws.GaeAppStruct;
import com.urltally.ws.FullNameStruct;
import com.urltally.ws.GaeUserStruct;
import com.urltally.ws.bean.GeoPointStructBean;
import com.urltally.ws.bean.StreetAddressStructBean;
import com.urltally.ws.bean.GaeAppStructBean;
import com.urltally.ws.bean.FullNameStructBean;
import com.urltally.ws.bean.GaeUserStructBean;
import com.urltally.ws.User;
import com.urltally.ws.dao.UserDAO;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.search.bean.UserQueryBean;


public class UserQueryHelper extends BaseQueryHelper
{
    private static final Logger log = Logger.getLogger(UserQueryHelper.class.getName());
    
    
    private UserIndexBuilder userIndexBuilder = null;

    public UserQueryHelper()
    {
    	this(null);
    }
    public UserQueryHelper(UserIndexBuilder userIndexBuilder)
    {
        super();
        if(userIndexBuilder != null) {
        	this.userIndexBuilder = userIndexBuilder;
        } else {
        	this.userIndexBuilder = new UserIndexBuilder();
        }
    }

    // TBD:    
    public List<UserQueryBean> findUserQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        return findUserQueryBeans(queryString, ordering, offset, count, null);
    }
    // Note: strCursor is an inout param. 
    public List<UserQueryBean> findUserQueryBeans(String queryString, String ordering, Long offset, Integer count, StringCursor strCursor)
    {
        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count, strCursor);
    	if(docs == null) {
    		return null;
    	}
    	List<UserQueryBean> userQueryBeans = new ArrayList<UserQueryBean>();
    	for(ScoredDocument doc : docs) {
    		UserQueryBean bean = new UserQueryBean();

    	    int rank = doc.getRank();
    		bean.setRank(rank);
    		String guid = doc.getId();
    		bean.setUserGuid(guid);
            Field fNickname = doc.getOnlyField("nickname");
            String vNickname = fNickname.getText();
            bean.setNickname(vNickname);
            Field fLocation = doc.getOnlyField("location");
            String vLocation = fLocation.getText();
            bean.setLocation(vLocation);
            Field fGeoPoint = doc.getOnlyField("geoPoint");
            GeoPoint gpGeoPoint = fGeoPoint.getGeoPoint();
            GeoPointStructBean vGeoPoint = new GeoPointStructBean();
            vGeoPoint.setLatitude(gpGeoPoint.getLatitude());
            vGeoPoint.setLongitude(gpGeoPoint.getLongitude());
            bean.setGeoPoint(vGeoPoint);

   	        userQueryBeans.add(bean);
    	}
        return userQueryBeans;
    }

    // Note: This is very inefficient....
    public List<User> findUsers(String queryString, String ordering,  Long offset, Integer count, StringCursor strCursor)
    {
        // TBD
    	Results<ScoredDocument> docs = findDocuments(queryString, ordering, offset, count, strCursor);
    	if(docs == null) {
    		return null;
    	}
    	UserDAO userDAO = DAOFactoryManager.getDAOFactory().getUserDAO();
    	List<User> users = new ArrayList<User>();
    	for(ScoredDocument doc : docs) {
    		String guid = doc.getId();
    		User user = null;
    		try {
    			user = userDAO.getUser(guid);
			} catch (BaseException e) {
				if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Failed to get User for guid = " + guid, e);
			}
    		if(user != null) {
    			users.add(user);
    		}
    	}
        return users;
    }


    // Note: strCursor is an inout param. 
    // if (strCursor != null && !strCursor.isEmpty()), then strCursor is used.
    // next, if (offset != null), then offset is used.
    // if both are null/empty, then it means page==0L and cursor will be used for query.
    // if non-null strCursor is inputted and cursor is used, then strCursor will be updated with the next page cursor.  
    public Results<ScoredDocument> findDocuments(String queryString, String ordering, Long offset, Integer count, StringCursor strCursor)
    {
        try {
        	if(count == null || count <= 0) {
        		count = DEFAULT_COUNT;
        	} else if(count > MAX_COUNT) {
        		count = MAX_COUNT;
        	}
        	Cursor cursor = null;
        	if(strCursor != null && !strCursor.isEmpty()) {
        	    cursor = Cursor.newBuilder().build(strCursor.getWebSafeString());
        	} else {
                if(offset == null) {
                    cursor = Cursor.newBuilder().build();
                } else {
                    if(offset < 0L) {
                        offset = 0L;    // ????
                    }
                }
        	}

        	QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
        	
        	if(ordering != null && !ordering.isEmpty()) {
        		String[] parts = ordering.split("\\s+", 2);
        		SortExpression.Builder sortExpressionBuilder = SortExpression.newBuilder();
        		if(parts.length > 0) {
        			sortExpressionBuilder.setExpression(parts[0]);
        			if(parts.length > 1) {
        				if(parts[1].equalsIgnoreCase("desc")) {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.DESCENDING);
        				} else {
        					sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        				}
        			} else {
        				// Default...
        				sortExpressionBuilder.setDirection(SortExpression.SortDirection.ASCENDING);
        			}
        		}
        		sortExpressionBuilder.setDefaultValue("");   // ???
        		SortExpression sortExpression = sortExpressionBuilder.build();
        		SortOptions.Builder sortOptionsBuilder = SortOptions.newBuilder(); 
        		sortOptionsBuilder.addSortExpression(sortExpression);
                // temporary
        	    // sortOptionsBuilder.setLimit(count + 1000);  // ???
        	    sortOptionsBuilder.setLimit(1000);  // ???
                // ...
        		SortOptions sortOptions = sortOptionsBuilder.build();
        		queryOptionsBuilder.setSortOptions(sortOptions);
        	}

        	queryOptionsBuilder.setLimit(count);
        	if(cursor != null) {
        	    queryOptionsBuilder.setCursor(cursor);
        	} else {
                queryOptionsBuilder.setOffset(offset.intValue());        	    
        	}

        	List<String> fieldsToReturn = new ArrayList<String>();
        	List<String> fieldsToSnippet = new ArrayList<String>();        	
            fieldsToReturn.add("nickname");
            fieldsToReturn.add("location");
            fieldsToReturn.add("geoPoint");
        	queryOptionsBuilder.setFieldsToReturn(fieldsToReturn.toArray(new String[0]));
        	if(!fieldsToSnippet.isEmpty()) {
                queryOptionsBuilder.setFieldsToSnippet(fieldsToSnippet.toArray(new String[0]));
            }

            QueryOptions options = queryOptionsBuilder.build();
            Query query = Query.newBuilder().setOptions(options).build(queryString);
            Results<ScoredDocument> documents = userIndexBuilder.getIndex().search(query);

            // TBD: Does this work even if setOffset() is used???
            if(strCursor != null) {
                String nextCursorWebSafeString = null;
                Cursor nextCursor = documents.getCursor();
                if(nextCursor != null) {
                    nextCursorWebSafeString = nextCursor.toWebSafeString();   // Could be null.
                }
                strCursor.setWebSafeString(nextCursorWebSafeString);
            }

            log.finer("END: findDocuments()");
            return documents;
        } catch (SearchException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Search request with query " + queryString + " failed", e);
            return null;
        }
    }

}

