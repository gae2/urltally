package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.HourlyShortUrlAccessDAO;
import com.urltally.ws.data.HourlyShortUrlAccessDataObject;


// MockHourlyShortUrlAccessDAO is a decorator.
// It can be used as a base class to mock HourlyShortUrlAccessDAO objects.
public abstract class MockHourlyShortUrlAccessDAO implements HourlyShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(MockHourlyShortUrlAccessDAO.class.getName()); 

    // MockHourlyShortUrlAccessDAO uses the decorator design pattern.
    private HourlyShortUrlAccessDAO decoratedDAO;

    public MockHourlyShortUrlAccessDAO(HourlyShortUrlAccessDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected HourlyShortUrlAccessDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(HourlyShortUrlAccessDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public HourlyShortUrlAccessDataObject getHourlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.getHourlyShortUrlAccess(guid);
	}

    @Override
    public List<HourlyShortUrlAccessDataObject> getHourlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedDAO.getHourlyShortUrlAccesses(guids);
    }

    @Override
    public List<HourlyShortUrlAccessDataObject> getAllHourlyShortUrlAccesses() throws BaseException
	{
	    return getAllHourlyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<HourlyShortUrlAccessDataObject> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllHourlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccessDataObject> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllHourlyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllHourlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllHourlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException
    {
        return decoratedDAO.createHourlyShortUrlAccess( hourlyShortUrlAccess);
    }

    @Override
	public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException
	{
        return decoratedDAO.updateHourlyShortUrlAccess(hourlyShortUrlAccess);
	}
	
    @Override
    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException
    {
        return decoratedDAO.deleteHourlyShortUrlAccess(hourlyShortUrlAccess);
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.deleteHourlyShortUrlAccess(guid);
	}

    @Override
    public Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteHourlyShortUrlAccesses(filter, params, values);
    }

}
