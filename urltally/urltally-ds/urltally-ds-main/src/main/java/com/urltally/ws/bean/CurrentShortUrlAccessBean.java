package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.data.CurrentShortUrlAccessDataObject;

public class CurrentShortUrlAccessBean extends ShortUrlAccessBean implements CurrentShortUrlAccess
{
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessBean.class.getName());

    // Embedded data object.
    private CurrentShortUrlAccessDataObject dobj = null;

    public CurrentShortUrlAccessBean()
    {
        this(new CurrentShortUrlAccessDataObject());
    }
    public CurrentShortUrlAccessBean(String guid)
    {
        this(new CurrentShortUrlAccessDataObject(guid));
    }
    public CurrentShortUrlAccessBean(CurrentShortUrlAccessDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public CurrentShortUrlAccessDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getStartDayHour()
    {
        if(getDataObject() != null) {
            return getDataObject().getStartDayHour();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CurrentShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setStartDayHour(String startDayHour)
    {
        if(getDataObject() != null) {
            getDataObject().setStartDayHour(startDayHour);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CurrentShortUrlAccessDataObject is null!");
        }
    }

    public Long getStartTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getStartTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CurrentShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setStartTime(Long startTime)
    {
        if(getDataObject() != null) {
            getDataObject().setStartTime(startTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded CurrentShortUrlAccessDataObject is null!");
        }
    }


    // TBD
    public CurrentShortUrlAccessDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
