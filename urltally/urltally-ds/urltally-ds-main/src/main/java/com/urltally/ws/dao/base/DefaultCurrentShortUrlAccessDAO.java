package com.urltally.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.CurrentShortUrlAccessDAO;
import com.urltally.ws.data.CurrentShortUrlAccessDataObject;


public class DefaultCurrentShortUrlAccessDAO extends DefaultDAOBase implements CurrentShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(DefaultCurrentShortUrlAccessDAO.class.getName()); 

    // Returns the currentShortUrlAccess for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public CurrentShortUrlAccessDataObject getCurrentShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        CurrentShortUrlAccessDataObject currentShortUrlAccess = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = CurrentShortUrlAccessDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = CurrentShortUrlAccessDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                currentShortUrlAccess = pm.getObjectById(CurrentShortUrlAccessDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve currentShortUrlAccess for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return currentShortUrlAccess;
	}

    @Override
    public List<CurrentShortUrlAccessDataObject> getCurrentShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<CurrentShortUrlAccessDataObject> currentShortUrlAccesses = null;
        if(guids != null && !guids.isEmpty()) {
            currentShortUrlAccesses = new ArrayList<CurrentShortUrlAccessDataObject>();
            for(String guid : guids) {
                CurrentShortUrlAccessDataObject obj = getCurrentShortUrlAccess(guid); 
                currentShortUrlAccesses.add(obj);
            }
	    }

        log.finer("END");
        return currentShortUrlAccesses;
    }

    @Override
    public List<CurrentShortUrlAccessDataObject> getAllCurrentShortUrlAccesses() throws BaseException
	{
	    return getAllCurrentShortUrlAccesses(null, null, null);
    }


    @Override
    public List<CurrentShortUrlAccessDataObject> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllCurrentShortUrlAccesses(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<CurrentShortUrlAccessDataObject> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<CurrentShortUrlAccessDataObject> currentShortUrlAccesses = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(CurrentShortUrlAccessDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            currentShortUrlAccesses = (List<CurrentShortUrlAccessDataObject>) q.execute();
            if(currentShortUrlAccesses != null) {
                currentShortUrlAccesses.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(currentShortUrlAccesses);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            /*
            // ???
            Collection<CurrentShortUrlAccessDataObject> rs_currentShortUrlAccesses = (Collection<CurrentShortUrlAccessDataObject>) q.execute();
            if(rs_currentShortUrlAccesses == null) {
                log.log(Level.WARNING, "Failed to retrieve all currentShortUrlAccesses.");
                currentShortUrlAccesses = new ArrayList<CurrentShortUrlAccessDataObject>();  // ???           
            } else {
                currentShortUrlAccesses = new ArrayList<CurrentShortUrlAccessDataObject>(pm.detachCopyAll(rs_currentShortUrlAccesses));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all currentShortUrlAccesses.", ex);
            //currentShortUrlAccesses = new ArrayList<CurrentShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all currentShortUrlAccesses.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return currentShortUrlAccesses;
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccessKeys(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + CurrentShortUrlAccessDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all CurrentShortUrlAccess keys.", ex);
            throw new DataStoreException("Failed to retrieve all CurrentShortUrlAccess keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findCurrentShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findCurrentShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultCurrentShortUrlAccessDAO.findCurrentShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findCurrentShortUrlAccesses() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<CurrentShortUrlAccessDataObject> currentShortUrlAccesses = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(CurrentShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                currentShortUrlAccesses = (List<CurrentShortUrlAccessDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                currentShortUrlAccesses = (List<CurrentShortUrlAccessDataObject>) q.execute();
            }
            if(currentShortUrlAccesses != null) {
                currentShortUrlAccesses.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(currentShortUrlAccesses);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(CurrentShortUrlAccessDataObject dobj : currentShortUrlAccesses) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find currentShortUrlAccesses because index is missing.", ex);
            //currentShortUrlAccesses = new ArrayList<CurrentShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to find currentShortUrlAccesses because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find currentShortUrlAccesses meeting the criterion.", ex);
            //currentShortUrlAccesses = new ArrayList<CurrentShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to find currentShortUrlAccesses meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return currentShortUrlAccesses;
	}

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultCurrentShortUrlAccessDAO.findCurrentShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + CurrentShortUrlAccessDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find CurrentShortUrlAccess keys because index is missing.", ex);
            throw new DataStoreException("Failed to find CurrentShortUrlAccess keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find CurrentShortUrlAccess keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find CurrentShortUrlAccess keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultCurrentShortUrlAccessDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(CurrentShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get currentShortUrlAccess count because index is missing.", ex);
            throw new DataStoreException("Failed to get currentShortUrlAccess count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get currentShortUrlAccess count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get currentShortUrlAccess count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the currentShortUrlAccess in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException
    {
        log.fine("storeCurrentShortUrlAccess() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = currentShortUrlAccess.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                currentShortUrlAccess.setCreatedTime(createdTime);
            }
            Long modifiedTime = currentShortUrlAccess.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                currentShortUrlAccess.setModifiedTime(createdTime);
            }
            pm.makePersistent(currentShortUrlAccess); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = currentShortUrlAccess.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store currentShortUrlAccess because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store currentShortUrlAccess because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store currentShortUrlAccess.", ex);
            throw new DataStoreException("Failed to store currentShortUrlAccess.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeCurrentShortUrlAccess(): guid = " + guid);
        return guid;
    }

    @Override
    public String createCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException
    {
        // The createdTime field will be automatically set in storeCurrentShortUrlAccess().
        //Long createdTime = currentShortUrlAccess.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    currentShortUrlAccess.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = currentShortUrlAccess.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    currentShortUrlAccess.setModifiedTime(createdTime);
        //}
        return storeCurrentShortUrlAccess(currentShortUrlAccess);
    }

    @Override
	public Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeCurrentShortUrlAccess()
	    // (in which case modifiedTime might be updated again).
	    currentShortUrlAccess.setModifiedTime(System.currentTimeMillis());
	    String guid = storeCurrentShortUrlAccess(currentShortUrlAccess);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(currentShortUrlAccess);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete currentShortUrlAccess because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete currentShortUrlAccess because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete currentShortUrlAccess.", ex);
            throw new DataStoreException("Failed to delete currentShortUrlAccess.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteCurrentShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = CurrentShortUrlAccessDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = CurrentShortUrlAccessDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                CurrentShortUrlAccessDataObject currentShortUrlAccess = pm.getObjectById(CurrentShortUrlAccessDataObject.class, key);
                pm.deletePersistent(currentShortUrlAccess);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete currentShortUrlAccess because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete currentShortUrlAccess because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete currentShortUrlAccess for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete currentShortUrlAccess for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultCurrentShortUrlAccessDAO.deleteCurrentShortUrlAccesses(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(CurrentShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletecurrentShortUrlAccesses because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete currentShortUrlAccesses because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete currentShortUrlAccesses because index is missing", ex);
            throw new DataStoreException("Failed to delete currentShortUrlAccesses because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete currentShortUrlAccesses", ex);
            throw new DataStoreException("Failed to delete currentShortUrlAccesses", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
