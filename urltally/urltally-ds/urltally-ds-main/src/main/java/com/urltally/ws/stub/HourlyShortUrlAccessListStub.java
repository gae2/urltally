package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "hourlyShortUrlAccesses")
@XmlType(propOrder = {"hourlyShortUrlAccess", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class HourlyShortUrlAccessListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(HourlyShortUrlAccessListStub.class.getName());

    private List<HourlyShortUrlAccessStub> hourlyShortUrlAccesses = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public HourlyShortUrlAccessListStub()
    {
        this(new ArrayList<HourlyShortUrlAccessStub>());
    }
    public HourlyShortUrlAccessListStub(List<HourlyShortUrlAccessStub> hourlyShortUrlAccesses)
    {
        this(hourlyShortUrlAccesses, null);
    }
    public HourlyShortUrlAccessListStub(List<HourlyShortUrlAccessStub> hourlyShortUrlAccesses, String forwardCursor)
    {
        this.hourlyShortUrlAccesses = hourlyShortUrlAccesses;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(hourlyShortUrlAccesses == null) {
            return true;
        } else {
            return hourlyShortUrlAccesses.isEmpty();
        }
    }
    public int getSize()
    {
        if(hourlyShortUrlAccesses == null) {
            return 0;
        } else {
            return hourlyShortUrlAccesses.size();
        }
    }


    @XmlElement(name = "hourlyShortUrlAccess")
    public List<HourlyShortUrlAccessStub> getHourlyShortUrlAccess()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<HourlyShortUrlAccessStub> getList()
    {
        return hourlyShortUrlAccesses;
    }
    public void setList(List<HourlyShortUrlAccessStub> hourlyShortUrlAccesses)
    {
        this.hourlyShortUrlAccesses = hourlyShortUrlAccesses;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<HourlyShortUrlAccessStub> it = this.hourlyShortUrlAccesses.iterator();
        while(it.hasNext()) {
            HourlyShortUrlAccessStub hourlyShortUrlAccess = it.next();
            sb.append(hourlyShortUrlAccess.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static HourlyShortUrlAccessListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of HourlyShortUrlAccessListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write HourlyShortUrlAccessListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write HourlyShortUrlAccessListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write HourlyShortUrlAccessListStub object as a string.", e);
        }
        
        return null;
    }
    public static HourlyShortUrlAccessListStub fromJsonString(String jsonStr)
    {
        try {
            HourlyShortUrlAccessListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, HourlyShortUrlAccessListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into HourlyShortUrlAccessListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into HourlyShortUrlAccessListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into HourlyShortUrlAccessListStub object.", e);
        }
        
        return null;
    }

}
