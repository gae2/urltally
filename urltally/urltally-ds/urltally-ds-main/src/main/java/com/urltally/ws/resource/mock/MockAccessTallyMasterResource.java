package com.urltally.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ResourceAlreadyPresentException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.DataStoreRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.bean.AccessTallyMasterBean;
import com.urltally.ws.stub.AccessTallyMasterListStub;
import com.urltally.ws.stub.AccessTallyMasterStub;
import com.urltally.ws.resource.ServiceManager;
import com.urltally.ws.resource.AccessTallyMasterResource;

// MockAccessTallyMasterResource is a decorator.
// It can be used as a base class to mock AccessTallyMasterResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/accessTallyMasters/")
public abstract class MockAccessTallyMasterResource implements AccessTallyMasterResource
{
    private static final Logger log = Logger.getLogger(MockAccessTallyMasterResource.class.getName());

    // MockAccessTallyMasterResource uses the decorator design pattern.
    private AccessTallyMasterResource decoratedResource;

    public MockAccessTallyMasterResource(AccessTallyMasterResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected AccessTallyMasterResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(AccessTallyMasterResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getAccessTallyMasterKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getAccessTallyMasterKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getAccessTallyMaster(String guid) throws BaseResourceException
    {
        return decoratedResource.getAccessTallyMaster(guid);
    }

    @Override
    public Response getAccessTallyMaster(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getAccessTallyMaster(guid, field);
    }

    @Override
    public Response createAccessTallyMaster(AccessTallyMasterStub accessTallyMaster) throws BaseResourceException
    {
        return decoratedResource.createAccessTallyMaster(accessTallyMaster);
    }

    @Override
    public Response updateAccessTallyMaster(String guid, AccessTallyMasterStub accessTallyMaster) throws BaseResourceException
    {
        return decoratedResource.updateAccessTallyMaster(guid, accessTallyMaster);
    }

    @Override
    public Response updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseResourceException
    {
        return decoratedResource.updateAccessTallyMaster(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }

    @Override
    public Response deleteAccessTallyMaster(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteAccessTallyMaster(guid);
    }

    @Override
    public Response deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteAccessTallyMasters(filter, params, values);
    }


}
