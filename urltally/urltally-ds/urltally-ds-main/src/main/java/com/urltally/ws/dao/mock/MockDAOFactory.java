package com.urltally.ws.dao.mock;

import java.util.logging.Logger;

import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.dao.ApiConsumerDAO;
import com.urltally.ws.dao.UserDAO;
import com.urltally.ws.dao.AccessTallyMasterDAO;
import com.urltally.ws.dao.AccessTallyStatusDAO;
import com.urltally.ws.dao.MonthlyShortUrlAccessDAO;
import com.urltally.ws.dao.WeeklyShortUrlAccessDAO;
import com.urltally.ws.dao.DailyShortUrlAccessDAO;
import com.urltally.ws.dao.HourlyShortUrlAccessDAO;
import com.urltally.ws.dao.CumulativeShortUrlAccessDAO;
import com.urltally.ws.dao.CurrentShortUrlAccessDAO;
import com.urltally.ws.dao.TotalShortUrlAccessDAO;
import com.urltally.ws.dao.TotalLongUrlAccessDAO;
import com.urltally.ws.dao.ServiceInfoDAO;
import com.urltally.ws.dao.FiveTenDAO;


// Create your own mock object factory using MockDAOFactory as a template.
public class MockDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(MockDAOFactory.class.getName());

    // Using the Decorator pattern.
    private MockDAOFactory decoratedDAOFactory;
    private MockDAOFactory()
    {
        this(null);   // ????
    }
    private MockDAOFactory(MockDAOFactory decoratedDAOFactory)
    {
        this.decoratedDAOFactory = decoratedDAOFactory;
    }

    // Initialization-on-demand holder.
    private static class MockDAOFactoryHolder
    {
        private static final MockDAOFactory INSTANCE = new MockDAOFactory();
    }

    // Singleton method
    public static MockDAOFactory getInstance()
    {
        return MockDAOFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public MockDAOFactory getDecoratedDAOFactory()
    {
        return decoratedDAOFactory;
    }
    public void setDecoratedDAOFactory(MockDAOFactory decoratedDAOFactory)
    {
        this.decoratedDAOFactory = decoratedDAOFactory;
    }


    @Override
    public ApiConsumerDAO getApiConsumerDAO()
    {
        return new MockApiConsumerDAO(decoratedDAOFactory.getApiConsumerDAO()) {};
    }

    @Override
    public UserDAO getUserDAO()
    {
        return new MockUserDAO(decoratedDAOFactory.getUserDAO()) {};
    }

    @Override
    public AccessTallyMasterDAO getAccessTallyMasterDAO()
    {
        return new MockAccessTallyMasterDAO(decoratedDAOFactory.getAccessTallyMasterDAO()) {};
    }

    @Override
    public AccessTallyStatusDAO getAccessTallyStatusDAO()
    {
        return new MockAccessTallyStatusDAO(decoratedDAOFactory.getAccessTallyStatusDAO()) {};
    }

    @Override
    public MonthlyShortUrlAccessDAO getMonthlyShortUrlAccessDAO()
    {
        return new MockMonthlyShortUrlAccessDAO(decoratedDAOFactory.getMonthlyShortUrlAccessDAO()) {};
    }

    @Override
    public WeeklyShortUrlAccessDAO getWeeklyShortUrlAccessDAO()
    {
        return new MockWeeklyShortUrlAccessDAO(decoratedDAOFactory.getWeeklyShortUrlAccessDAO()) {};
    }

    @Override
    public DailyShortUrlAccessDAO getDailyShortUrlAccessDAO()
    {
        return new MockDailyShortUrlAccessDAO(decoratedDAOFactory.getDailyShortUrlAccessDAO()) {};
    }

    @Override
    public HourlyShortUrlAccessDAO getHourlyShortUrlAccessDAO()
    {
        return new MockHourlyShortUrlAccessDAO(decoratedDAOFactory.getHourlyShortUrlAccessDAO()) {};
    }

    @Override
    public CumulativeShortUrlAccessDAO getCumulativeShortUrlAccessDAO()
    {
        return new MockCumulativeShortUrlAccessDAO(decoratedDAOFactory.getCumulativeShortUrlAccessDAO()) {};
    }

    @Override
    public CurrentShortUrlAccessDAO getCurrentShortUrlAccessDAO()
    {
        return new MockCurrentShortUrlAccessDAO(decoratedDAOFactory.getCurrentShortUrlAccessDAO()) {};
    }

    @Override
    public TotalShortUrlAccessDAO getTotalShortUrlAccessDAO()
    {
        return new MockTotalShortUrlAccessDAO(decoratedDAOFactory.getTotalShortUrlAccessDAO()) {};
    }

    @Override
    public TotalLongUrlAccessDAO getTotalLongUrlAccessDAO()
    {
        return new MockTotalLongUrlAccessDAO(decoratedDAOFactory.getTotalLongUrlAccessDAO()) {};
    }

    @Override
    public ServiceInfoDAO getServiceInfoDAO()
    {
        return new MockServiceInfoDAO(decoratedDAOFactory.getServiceInfoDAO()) {};
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new MockFiveTenDAO(decoratedDAOFactory.getFiveTenDAO()) {};
    }

}
