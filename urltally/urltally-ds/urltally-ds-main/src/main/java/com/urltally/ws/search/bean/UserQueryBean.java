package com.urltally.ws.search.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.util.CommonUtil;
import com.urltally.ws.GeoPointStruct;
import com.urltally.ws.StreetAddressStruct;
import com.urltally.ws.GaeAppStruct;
import com.urltally.ws.FullNameStruct;
import com.urltally.ws.GaeUserStruct;
import com.urltally.ws.core.GUID;
import com.urltally.ws.User;


public class UserQueryBean
{
    private static final Logger log = Logger.getLogger(UserQueryBean.class.getName());

    private int rank;
    private String userGuid;
    private String nickname;
    private String location;
    private GeoPointStruct geoPoint;

    public UserQueryBean()
    {
        this(0, null, null, null, null);  
    }
    public UserQueryBean(int rank, String userGuid, String nickname, String location, GeoPointStruct geoPoint)
    {
        this.rank = rank;
        this.userGuid = userGuid;
        this.nickname = nickname;
        this.location = location;
        this.geoPoint = geoPoint;
    }
    
    public int getRank()
    {
        return this.rank;
    }
    public void setRank(int rank)
    {
        this.rank = rank;
    }

    public String getUserGuid()
    {
        return this.userGuid;
    }
    public void setUserGuid(String userGuid)
    {
        this.userGuid = userGuid;
    }

    public String getNickname()
    {
        return this.nickname;
    }
    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public GeoPointStruct getGeoPoint()
    {
        return this.geoPoint;
    }
    public void setGeoPoint(GeoPointStruct geoPoint)
    {
        this.geoPoint = geoPoint;
    }


    @Override
    public String toString()
    {
            StringBuilder sb = new StringBuilder();

            sb.append("rank = " + this.rank).append(";");
            sb.append("userGuid = " + this.userGuid).append(";");
            sb.append("nickname = " + this.nickname).append(";");
            sb.append("location = " + this.location).append(";");
            sb.append("geoPoint = " + this.geoPoint).append(";");

            return sb.toString();
    }


}

