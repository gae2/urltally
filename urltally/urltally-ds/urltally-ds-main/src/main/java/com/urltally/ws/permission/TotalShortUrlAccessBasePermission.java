package com.urltally.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class TotalShortUrlAccessBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessBasePermission.class.getName());

    public TotalShortUrlAccessBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "TotalShortUrlAccess::" + action;
    }


}
