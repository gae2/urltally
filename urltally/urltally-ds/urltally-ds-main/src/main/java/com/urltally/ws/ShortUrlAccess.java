package com.urltally.ws;



public interface ShortUrlAccess 
{
    String  getGuid();
    String  getTallyTime();
    Long  getTallyEpoch();
    Integer  getCount();
    String  getShortUrl();
    String  getShortUrlDomain();
    String  getLongUrl();
    String  getLongUrlDomain();
    String  getRedirectType();
    String  getRefererDomain();
    String  getUserAgent();
    String  getLanguage();
    String  getCountry();
    Long  getTalliedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
