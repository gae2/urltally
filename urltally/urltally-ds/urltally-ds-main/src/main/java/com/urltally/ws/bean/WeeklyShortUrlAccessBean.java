package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.ws.data.WeeklyShortUrlAccessDataObject;

public class WeeklyShortUrlAccessBean extends ShortUrlAccessBean implements WeeklyShortUrlAccess
{
    private static final Logger log = Logger.getLogger(WeeklyShortUrlAccessBean.class.getName());

    // Embedded data object.
    private WeeklyShortUrlAccessDataObject dobj = null;

    public WeeklyShortUrlAccessBean()
    {
        this(new WeeklyShortUrlAccessDataObject());
    }
    public WeeklyShortUrlAccessBean(String guid)
    {
        this(new WeeklyShortUrlAccessDataObject(guid));
    }
    public WeeklyShortUrlAccessBean(WeeklyShortUrlAccessDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public WeeklyShortUrlAccessDataObject getDataObject()
    {
        return this.dobj;
    }

    public Integer getYear()
    {
        if(getDataObject() != null) {
            return getDataObject().getYear();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WeeklyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setYear(Integer year)
    {
        if(getDataObject() != null) {
            getDataObject().setYear(year);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WeeklyShortUrlAccessDataObject is null!");
        }
    }

    public Integer getWeek()
    {
        if(getDataObject() != null) {
            return getDataObject().getWeek();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WeeklyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setWeek(Integer week)
    {
        if(getDataObject() != null) {
            getDataObject().setWeek(week);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded WeeklyShortUrlAccessDataObject is null!");
        }
    }


    // TBD
    public WeeklyShortUrlAccessDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
