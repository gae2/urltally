package com.urltally.ws.dao;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.data.TotalLongUrlAccessDataObject;


// TBD: Add offset/count to getAllTotalLongUrlAccesses() and findTotalLongUrlAccesses(), etc.
public interface TotalLongUrlAccessDAO
{
    TotalLongUrlAccessDataObject getTotalLongUrlAccess(String guid) throws BaseException;
    List<TotalLongUrlAccessDataObject> getTotalLongUrlAccesses(List<String> guids) throws BaseException;
    List<TotalLongUrlAccessDataObject> getAllTotalLongUrlAccesses() throws BaseException;
    /* @Deprecated */ List<TotalLongUrlAccessDataObject> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<TotalLongUrlAccessDataObject> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<TotalLongUrlAccessDataObject> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<TotalLongUrlAccessDataObject> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<TotalLongUrlAccessDataObject> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<TotalLongUrlAccessDataObject> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createTotalLongUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TotalLongUrlAccessDataObject?)
    String createTotalLongUrlAccess(TotalLongUrlAccessDataObject totalLongUrlAccess) throws BaseException;          // Returns Guid.  (Return TotalLongUrlAccessDataObject?)
    //Boolean updateTotalLongUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTotalLongUrlAccess(TotalLongUrlAccessDataObject totalLongUrlAccess) throws BaseException;
    Boolean deleteTotalLongUrlAccess(String guid) throws BaseException;
    Boolean deleteTotalLongUrlAccess(TotalLongUrlAccessDataObject totalLongUrlAccess) throws BaseException;
    Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws BaseException;
}
