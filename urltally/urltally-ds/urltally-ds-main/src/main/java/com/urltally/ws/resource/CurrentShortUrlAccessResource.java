package com.urltally.ws.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.stub.CurrentShortUrlAccessStub;
import com.urltally.ws.stub.CurrentShortUrlAccessListStub;


// TBD: Partial update/overwrite?
// TBD: Field-based filtering in getCurrentShortUrlAccess(guid). (e.g., ?field1=x&field2=y)
// Note: Jersey (possibly, new version 1.9.1) seems to have a weird bug and
//       it throws exception with the format @Path("{guid : [0-9a-fA-F\\-]+}") (and, other variations)
//       (which somehow translates into 405 error).
// --> Workaround. Use this format: @Path("{guid: [a-zA-Z0-9\\-_]+}") across all guid path params...
public interface CurrentShortUrlAccessResource
{
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllCurrentShortUrlAccesses(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("allkeys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllCurrentShortUrlAccessKeys(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("keys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findCurrentShortUrlAccessKeys(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("subset")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCurrentShortUrlAccessKeys(@QueryParam("guids") List<String> guids) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

    @GET
    // @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Support both guid and key ???  (Note: We have to be consistent!)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCurrentShortUrlAccess(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    @Path("{guid: [a-f0-9\\-]+}/{field: [a-zA-Z_][a-zA-Z0-9_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCurrentShortUrlAccess(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findCurrentShortUrlAccesses(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createCurrentShortUrlAccess(CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateCurrentShortUrlAccess(@PathParam("guid") String guid, CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateCurrentShortUrlAccess(@PathParam("guid") String guid, @QueryParam("tallyTime") String tallyTime, @QueryParam("tallyEpoch") Long tallyEpoch, @QueryParam("count") Integer count, @QueryParam("shortUrl") String shortUrl, @QueryParam("shortUrlDomain") String shortUrlDomain, @QueryParam("longUrl") String longUrl, @QueryParam("longUrlDomain") String longUrlDomain, @QueryParam("redirectType") String redirectType, @QueryParam("refererDomain") String refererDomain, @QueryParam("userAgent") String userAgent, @QueryParam("language") String language, @QueryParam("country") String country, @QueryParam("talliedTime") Long talliedTime, @QueryParam("startDayHour") String startDayHour, @QueryParam("startTime") Long startTime) throws BaseResourceException;

    @DELETE
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    Response deleteCurrentShortUrlAccess(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteCurrentShortUrlAccesses(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

//    @POST
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response createCurrentShortUrlAccesses(CurrentShortUrlAccessListStub currentShortUrlAccesses) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateeCurrentShortUrlAccesses(CurrentShortUrlAccessListStub currentShortUrlAccesses) throws BaseResourceException;

}
