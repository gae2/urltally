package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "dailyShortUrlAccess")
@XmlType(propOrder = {"guid", "tallyTime", "tallyEpoch", "count", "shortUrl", "shortUrlDomain", "longUrl", "longUrlDomain", "redirectType", "refererDomain", "userAgent", "language", "country", "talliedTime", "year", "day", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DailyShortUrlAccessStub extends ShortUrlAccessStub implements DailyShortUrlAccess, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DailyShortUrlAccessStub.class.getName());

    private Integer year;
    private Integer day;

    public DailyShortUrlAccessStub()
    {
        this(null);
    }
    public DailyShortUrlAccessStub(DailyShortUrlAccess bean)
    {
        super(bean);
        if(bean != null) {
            this.year = bean.getYear();
            this.day = bean.getDay();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getTallyTime()
    {
        return super.getTallyTime();
    }
    public void setTallyTime(String tallyTime)
    {
        super.setTallyTime(tallyTime);
    }

    @XmlElement
    public Long getTallyEpoch()
    {
        return super.getTallyEpoch();
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        super.setTallyEpoch(tallyEpoch);
    }

    @XmlElement
    public Integer getCount()
    {
        return super.getCount();
    }
    public void setCount(Integer count)
    {
        super.setCount(count);
    }

    @XmlElement
    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    @XmlElement
    public String getShortUrlDomain()
    {
        return super.getShortUrlDomain();
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        super.setShortUrlDomain(shortUrlDomain);
    }

    @XmlElement
    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    @XmlElement
    public String getLongUrlDomain()
    {
        return super.getLongUrlDomain();
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        super.setLongUrlDomain(longUrlDomain);
    }

    @XmlElement
    public String getRedirectType()
    {
        return super.getRedirectType();
    }
    public void setRedirectType(String redirectType)
    {
        super.setRedirectType(redirectType);
    }

    @XmlElement
    public String getRefererDomain()
    {
        return super.getRefererDomain();
    }
    public void setRefererDomain(String refererDomain)
    {
        super.setRefererDomain(refererDomain);
    }

    @XmlElement
    public String getUserAgent()
    {
        return super.getUserAgent();
    }
    public void setUserAgent(String userAgent)
    {
        super.setUserAgent(userAgent);
    }

    @XmlElement
    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    @XmlElement
    public String getCountry()
    {
        return super.getCountry();
    }
    public void setCountry(String country)
    {
        super.setCountry(country);
    }

    @XmlElement
    public Long getTalliedTime()
    {
        return super.getTalliedTime();
    }
    public void setTalliedTime(Long talliedTime)
    {
        super.setTalliedTime(talliedTime);
    }

    @XmlElement
    public Integer getYear()
    {
        return this.year;
    }
    public void setYear(Integer year)
    {
        this.year = year;
    }

    @XmlElement
    public Integer getDay()
    {
        return this.day;
    }
    public void setDay(Integer day)
    {
        this.day = day;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("year", this.year);
        dataMap.put("day", this.day);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = year == null ? 0 : year.hashCode();
        _hash = 31 * _hash + delta;
        delta = day == null ? 0 : day.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static DailyShortUrlAccessStub convertBeanToStub(DailyShortUrlAccess bean)
    {
        DailyShortUrlAccessStub stub = null;
        if(bean instanceof DailyShortUrlAccessStub) {
            stub = (DailyShortUrlAccessStub) bean;
        } else {
            if(bean != null) {
                stub = new DailyShortUrlAccessStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static DailyShortUrlAccessStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of DailyShortUrlAccessStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write DailyShortUrlAccessStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write DailyShortUrlAccessStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write DailyShortUrlAccessStub object as a string.", e);
        }
        
        return null;
    }
    public static DailyShortUrlAccessStub fromJsonString(String jsonStr)
    {
        try {
            DailyShortUrlAccessStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, DailyShortUrlAccessStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into DailyShortUrlAccessStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into DailyShortUrlAccessStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into DailyShortUrlAccessStub object.", e);
        }
        
        return null;
    }

}
