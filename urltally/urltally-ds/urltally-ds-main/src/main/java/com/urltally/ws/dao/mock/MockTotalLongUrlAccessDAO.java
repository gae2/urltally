package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.TotalLongUrlAccessDAO;
import com.urltally.ws.data.TotalLongUrlAccessDataObject;


// MockTotalLongUrlAccessDAO is a decorator.
// It can be used as a base class to mock TotalLongUrlAccessDAO objects.
public abstract class MockTotalLongUrlAccessDAO implements TotalLongUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(MockTotalLongUrlAccessDAO.class.getName()); 

    // MockTotalLongUrlAccessDAO uses the decorator design pattern.
    private TotalLongUrlAccessDAO decoratedDAO;

    public MockTotalLongUrlAccessDAO(TotalLongUrlAccessDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected TotalLongUrlAccessDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(TotalLongUrlAccessDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public TotalLongUrlAccessDataObject getTotalLongUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.getTotalLongUrlAccess(guid);
	}

    @Override
    public List<TotalLongUrlAccessDataObject> getTotalLongUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedDAO.getTotalLongUrlAccesses(guids);
    }

    @Override
    public List<TotalLongUrlAccessDataObject> getAllTotalLongUrlAccesses() throws BaseException
	{
	    return getAllTotalLongUrlAccesses(null, null, null);
    }


    @Override
    public List<TotalLongUrlAccessDataObject> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTotalLongUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccessDataObject> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllTotalLongUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalLongUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllTotalLongUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<TotalLongUrlAccessDataObject> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTotalLongUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TotalLongUrlAccessDataObject> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTotalLongUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TotalLongUrlAccessDataObject> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<TotalLongUrlAccessDataObject> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTotalLongUrlAccess(TotalLongUrlAccessDataObject totalLongUrlAccess) throws BaseException
    {
        return decoratedDAO.createTotalLongUrlAccess( totalLongUrlAccess);
    }

    @Override
	public Boolean updateTotalLongUrlAccess(TotalLongUrlAccessDataObject totalLongUrlAccess) throws BaseException
	{
        return decoratedDAO.updateTotalLongUrlAccess(totalLongUrlAccess);
	}
	
    @Override
    public Boolean deleteTotalLongUrlAccess(TotalLongUrlAccessDataObject totalLongUrlAccess) throws BaseException
    {
        return decoratedDAO.deleteTotalLongUrlAccess(totalLongUrlAccess);
    }

    @Override
    public Boolean deleteTotalLongUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.deleteTotalLongUrlAccess(guid);
	}

    @Override
    public Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteTotalLongUrlAccesses(filter, params, values);
    }

}
