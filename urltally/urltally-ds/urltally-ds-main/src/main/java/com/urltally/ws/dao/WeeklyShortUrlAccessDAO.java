package com.urltally.ws.dao;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.data.WeeklyShortUrlAccessDataObject;


// TBD: Add offset/count to getAllWeeklyShortUrlAccesses() and findWeeklyShortUrlAccesses(), etc.
public interface WeeklyShortUrlAccessDAO
{
    WeeklyShortUrlAccessDataObject getWeeklyShortUrlAccess(String guid) throws BaseException;
    List<WeeklyShortUrlAccessDataObject> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException;
    List<WeeklyShortUrlAccessDataObject> getAllWeeklyShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<WeeklyShortUrlAccessDataObject> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<WeeklyShortUrlAccessDataObject> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<WeeklyShortUrlAccessDataObject> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createWeeklyShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return WeeklyShortUrlAccessDataObject?)
    String createWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException;          // Returns Guid.  (Return WeeklyShortUrlAccessDataObject?)
    //Boolean updateWeeklyShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException;
    Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException;
    Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccessDataObject weeklyShortUrlAccess) throws BaseException;
    Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;
}
