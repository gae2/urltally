package com.urltally.ws;



public interface AccessTallyStatus 
{
    String  getGuid();
    String  getRemoteRecordGuid();
    String  getShortUrl();
    String  getTallyType();
    String  getTallyTime();
    Long  getTallyEpoch();
    Boolean  isProcessed();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
