package com.urltally.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.util.CommonUtil;
import com.urltally.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class AccessTallyStatusDataObject extends KeyedDataObject implements AccessTallyStatus
{
    private static final Logger log = Logger.getLogger(AccessTallyStatusDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(AccessTallyStatusDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(AccessTallyStatusDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String remoteRecordGuid;

    @Persistent(defaultFetchGroup = "true")
    private String shortUrl;

    @Persistent(defaultFetchGroup = "true")
    private String tallyType;

    @Persistent(defaultFetchGroup = "true")
    private String tallyTime;

    @Persistent(defaultFetchGroup = "true")
    private Long tallyEpoch;

    @Persistent(defaultFetchGroup = "true")
    private Boolean processed;

    public AccessTallyStatusDataObject()
    {
        this(null);
    }
    public AccessTallyStatusDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public AccessTallyStatusDataObject(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed)
    {
        this(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed, null, null);
    }
    public AccessTallyStatusDataObject(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.remoteRecordGuid = remoteRecordGuid;
        this.shortUrl = shortUrl;
        this.tallyType = tallyType;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.processed = processed;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return AccessTallyStatusDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return AccessTallyStatusDataObject.composeKey(getGuid());
    }

    public String getRemoteRecordGuid()
    {
        return this.remoteRecordGuid;
    }
    public void setRemoteRecordGuid(String remoteRecordGuid)
    {
        this.remoteRecordGuid = remoteRecordGuid;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public String getTallyType()
    {
        return this.tallyType;
    }
    public void setTallyType(String tallyType)
    {
        this.tallyType = tallyType;
    }

    public String getTallyTime()
    {
        return this.tallyTime;
    }
    public void setTallyTime(String tallyTime)
    {
        this.tallyTime = tallyTime;
    }

    public Long getTallyEpoch()
    {
        return this.tallyEpoch;
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        this.tallyEpoch = tallyEpoch;
    }

    public Boolean isProcessed()
    {
        return this.processed;
    }
    public void setProcessed(Boolean processed)
    {
        this.processed = processed;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("remoteRecordGuid", this.remoteRecordGuid);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("tallyType", this.tallyType);
        dataMap.put("tallyTime", this.tallyTime);
        dataMap.put("tallyEpoch", this.tallyEpoch);
        dataMap.put("processed", this.processed);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        AccessTallyStatus thatObj = (AccessTallyStatus) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.remoteRecordGuid == null && thatObj.getRemoteRecordGuid() != null)
            || (this.remoteRecordGuid != null && thatObj.getRemoteRecordGuid() == null)
            || !this.remoteRecordGuid.equals(thatObj.getRemoteRecordGuid()) ) {
            return false;
        }
        if( (this.shortUrl == null && thatObj.getShortUrl() != null)
            || (this.shortUrl != null && thatObj.getShortUrl() == null)
            || !this.shortUrl.equals(thatObj.getShortUrl()) ) {
            return false;
        }
        if( (this.tallyType == null && thatObj.getTallyType() != null)
            || (this.tallyType != null && thatObj.getTallyType() == null)
            || !this.tallyType.equals(thatObj.getTallyType()) ) {
            return false;
        }
        if( (this.tallyTime == null && thatObj.getTallyTime() != null)
            || (this.tallyTime != null && thatObj.getTallyTime() == null)
            || !this.tallyTime.equals(thatObj.getTallyTime()) ) {
            return false;
        }
        if( (this.tallyEpoch == null && thatObj.getTallyEpoch() != null)
            || (this.tallyEpoch != null && thatObj.getTallyEpoch() == null)
            || !this.tallyEpoch.equals(thatObj.getTallyEpoch()) ) {
            return false;
        }
        if( (this.processed == null && thatObj.isProcessed() != null)
            || (this.processed != null && thatObj.isProcessed() == null)
            || !this.processed.equals(thatObj.isProcessed()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = remoteRecordGuid == null ? 0 : remoteRecordGuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyType == null ? 0 : tallyType.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyTime == null ? 0 : tallyTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyEpoch == null ? 0 : tallyEpoch.hashCode();
        _hash = 31 * _hash + delta;
        delta = processed == null ? 0 : processed.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
