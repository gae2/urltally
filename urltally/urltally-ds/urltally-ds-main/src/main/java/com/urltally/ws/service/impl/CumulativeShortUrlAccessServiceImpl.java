package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.bean.CumulativeShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.CumulativeShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.CumulativeShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CumulativeShortUrlAccessServiceImpl implements CumulativeShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(CumulativeShortUrlAccessServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // CumulativeShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public CumulativeShortUrlAccess getCumulativeShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        CumulativeShortUrlAccessDataObject dataObj = getDAOFactory().getCumulativeShortUrlAccessDAO().getCumulativeShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve CumulativeShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        CumulativeShortUrlAccessBean bean = new CumulativeShortUrlAccessBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getCumulativeShortUrlAccess(String guid, String field) throws BaseException
    {
        CumulativeShortUrlAccessDataObject dataObj = getDAOFactory().getCumulativeShortUrlAccessDAO().getCumulativeShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve CumulativeShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("count")) {
            return dataObj.getCount();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return dataObj.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return dataObj.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return dataObj.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return dataObj.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return dataObj.getUserAgent();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("country")) {
            return dataObj.getCountry();
        } else if(field.equals("talliedTime")) {
            return dataObj.getTalliedTime();
        } else if(field.equals("startDayHour")) {
            return dataObj.getStartDayHour();
        } else if(field.equals("endDayHour")) {
            return dataObj.getEndDayHour();
        } else if(field.equals("startTime")) {
            return dataObj.getStartTime();
        } else if(field.equals("endTime")) {
            return dataObj.getEndTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<CumulativeShortUrlAccess> getCumulativeShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<CumulativeShortUrlAccess> list = new ArrayList<CumulativeShortUrlAccess>();
        List<CumulativeShortUrlAccessDataObject> dataObjs = getDAOFactory().getCumulativeShortUrlAccessDAO().getCumulativeShortUrlAccesses(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve CumulativeShortUrlAccessDataObject list.");
        } else {
            Iterator<CumulativeShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                CumulativeShortUrlAccessDataObject dataObj = (CumulativeShortUrlAccessDataObject) it.next();
                list.add(new CumulativeShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses() throws BaseException
    {
        return getAllCumulativeShortUrlAccesses(null, null, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCumulativeShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllCumulativeShortUrlAccesses(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<CumulativeShortUrlAccess> list = new ArrayList<CumulativeShortUrlAccess>();
        List<CumulativeShortUrlAccessDataObject> dataObjs = getDAOFactory().getCumulativeShortUrlAccessDAO().getAllCumulativeShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve CumulativeShortUrlAccessDataObject list.");
        } else {
            Iterator<CumulativeShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                CumulativeShortUrlAccessDataObject dataObj = (CumulativeShortUrlAccessDataObject) it.next();
                list.add(new CumulativeShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCumulativeShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllCumulativeShortUrlAccessKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getCumulativeShortUrlAccessDAO().getAllCumulativeShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve CumulativeShortUrlAccess key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("CumulativeShortUrlAccessServiceImpl.findCumulativeShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<CumulativeShortUrlAccess> list = new ArrayList<CumulativeShortUrlAccess>();
        List<CumulativeShortUrlAccessDataObject> dataObjs = getDAOFactory().getCumulativeShortUrlAccessDAO().findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find cumulativeShortUrlAccesses for the given criterion.");
        } else {
            Iterator<CumulativeShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                CumulativeShortUrlAccessDataObject dataObj = (CumulativeShortUrlAccessDataObject) it.next();
                list.add(new CumulativeShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("CumulativeShortUrlAccessServiceImpl.findCumulativeShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getCumulativeShortUrlAccessDAO().findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find CumulativeShortUrlAccess keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("CumulativeShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getCumulativeShortUrlAccessDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createCumulativeShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        CumulativeShortUrlAccessDataObject dataObj = new CumulativeShortUrlAccessDataObject(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
        return createCumulativeShortUrlAccess(dataObj);
    }

    @Override
    public String createCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param cumulativeShortUrlAccess cannot be null.....
        if(cumulativeShortUrlAccess == null) {
            log.log(Level.INFO, "Param cumulativeShortUrlAccess is null!");
            throw new BadRequestException("Param cumulativeShortUrlAccess object is null!");
        }
        CumulativeShortUrlAccessDataObject dataObj = null;
        if(cumulativeShortUrlAccess instanceof CumulativeShortUrlAccessDataObject) {
            dataObj = (CumulativeShortUrlAccessDataObject) cumulativeShortUrlAccess;
        } else if(cumulativeShortUrlAccess instanceof CumulativeShortUrlAccessBean) {
            dataObj = ((CumulativeShortUrlAccessBean) cumulativeShortUrlAccess).toDataObject();
        } else {  // if(cumulativeShortUrlAccess instanceof CumulativeShortUrlAccess)
            //dataObj = new CumulativeShortUrlAccessDataObject(null, cumulativeShortUrlAccess.getTallyTime(), cumulativeShortUrlAccess.getTallyEpoch(), cumulativeShortUrlAccess.getCount(), cumulativeShortUrlAccess.getShortUrl(), cumulativeShortUrlAccess.getShortUrlDomain(), cumulativeShortUrlAccess.getLongUrl(), cumulativeShortUrlAccess.getLongUrlDomain(), cumulativeShortUrlAccess.getRedirectType(), cumulativeShortUrlAccess.getRefererDomain(), cumulativeShortUrlAccess.getUserAgent(), cumulativeShortUrlAccess.getLanguage(), cumulativeShortUrlAccess.getCountry(), cumulativeShortUrlAccess.getTalliedTime(), cumulativeShortUrlAccess.getStartDayHour(), cumulativeShortUrlAccess.getEndDayHour(), cumulativeShortUrlAccess.getStartTime(), cumulativeShortUrlAccess.getEndTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new CumulativeShortUrlAccessDataObject(cumulativeShortUrlAccess.getGuid(), cumulativeShortUrlAccess.getTallyTime(), cumulativeShortUrlAccess.getTallyEpoch(), cumulativeShortUrlAccess.getCount(), cumulativeShortUrlAccess.getShortUrl(), cumulativeShortUrlAccess.getShortUrlDomain(), cumulativeShortUrlAccess.getLongUrl(), cumulativeShortUrlAccess.getLongUrlDomain(), cumulativeShortUrlAccess.getRedirectType(), cumulativeShortUrlAccess.getRefererDomain(), cumulativeShortUrlAccess.getUserAgent(), cumulativeShortUrlAccess.getLanguage(), cumulativeShortUrlAccess.getCountry(), cumulativeShortUrlAccess.getTalliedTime(), cumulativeShortUrlAccess.getStartDayHour(), cumulativeShortUrlAccess.getEndDayHour(), cumulativeShortUrlAccess.getStartTime(), cumulativeShortUrlAccess.getEndTime());
        }
        String guid = getDAOFactory().getCumulativeShortUrlAccessDAO().createCumulativeShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateCumulativeShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        CumulativeShortUrlAccessDataObject dataObj = new CumulativeShortUrlAccessDataObject(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
        return updateCumulativeShortUrlAccess(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param cumulativeShortUrlAccess cannot be null.....
        if(cumulativeShortUrlAccess == null || cumulativeShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param cumulativeShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param cumulativeShortUrlAccess object or its guid is null!");
        }
        CumulativeShortUrlAccessDataObject dataObj = null;
        if(cumulativeShortUrlAccess instanceof CumulativeShortUrlAccessDataObject) {
            dataObj = (CumulativeShortUrlAccessDataObject) cumulativeShortUrlAccess;
        } else if(cumulativeShortUrlAccess instanceof CumulativeShortUrlAccessBean) {
            dataObj = ((CumulativeShortUrlAccessBean) cumulativeShortUrlAccess).toDataObject();
        } else {  // if(cumulativeShortUrlAccess instanceof CumulativeShortUrlAccess)
            dataObj = new CumulativeShortUrlAccessDataObject(cumulativeShortUrlAccess.getGuid(), cumulativeShortUrlAccess.getTallyTime(), cumulativeShortUrlAccess.getTallyEpoch(), cumulativeShortUrlAccess.getCount(), cumulativeShortUrlAccess.getShortUrl(), cumulativeShortUrlAccess.getShortUrlDomain(), cumulativeShortUrlAccess.getLongUrl(), cumulativeShortUrlAccess.getLongUrlDomain(), cumulativeShortUrlAccess.getRedirectType(), cumulativeShortUrlAccess.getRefererDomain(), cumulativeShortUrlAccess.getUserAgent(), cumulativeShortUrlAccess.getLanguage(), cumulativeShortUrlAccess.getCountry(), cumulativeShortUrlAccess.getTalliedTime(), cumulativeShortUrlAccess.getStartDayHour(), cumulativeShortUrlAccess.getEndDayHour(), cumulativeShortUrlAccess.getStartTime(), cumulativeShortUrlAccess.getEndTime());
        }
        Boolean suc = getDAOFactory().getCumulativeShortUrlAccessDAO().updateCumulativeShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteCumulativeShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getCumulativeShortUrlAccessDAO().deleteCumulativeShortUrlAccess(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param cumulativeShortUrlAccess cannot be null.....
        if(cumulativeShortUrlAccess == null || cumulativeShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param cumulativeShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param cumulativeShortUrlAccess object or its guid is null!");
        }
        CumulativeShortUrlAccessDataObject dataObj = null;
        if(cumulativeShortUrlAccess instanceof CumulativeShortUrlAccessDataObject) {
            dataObj = (CumulativeShortUrlAccessDataObject) cumulativeShortUrlAccess;
        } else if(cumulativeShortUrlAccess instanceof CumulativeShortUrlAccessBean) {
            dataObj = ((CumulativeShortUrlAccessBean) cumulativeShortUrlAccess).toDataObject();
        } else {  // if(cumulativeShortUrlAccess instanceof CumulativeShortUrlAccess)
            dataObj = new CumulativeShortUrlAccessDataObject(cumulativeShortUrlAccess.getGuid(), cumulativeShortUrlAccess.getTallyTime(), cumulativeShortUrlAccess.getTallyEpoch(), cumulativeShortUrlAccess.getCount(), cumulativeShortUrlAccess.getShortUrl(), cumulativeShortUrlAccess.getShortUrlDomain(), cumulativeShortUrlAccess.getLongUrl(), cumulativeShortUrlAccess.getLongUrlDomain(), cumulativeShortUrlAccess.getRedirectType(), cumulativeShortUrlAccess.getRefererDomain(), cumulativeShortUrlAccess.getUserAgent(), cumulativeShortUrlAccess.getLanguage(), cumulativeShortUrlAccess.getCountry(), cumulativeShortUrlAccess.getTalliedTime(), cumulativeShortUrlAccess.getStartDayHour(), cumulativeShortUrlAccess.getEndDayHour(), cumulativeShortUrlAccess.getStartTime(), cumulativeShortUrlAccess.getEndTime());
        }
        Boolean suc = getDAOFactory().getCumulativeShortUrlAccessDAO().deleteCumulativeShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getCumulativeShortUrlAccessDAO().deleteCumulativeShortUrlAccesses(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
