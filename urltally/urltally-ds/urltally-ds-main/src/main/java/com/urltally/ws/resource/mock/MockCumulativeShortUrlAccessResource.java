package com.urltally.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ResourceAlreadyPresentException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.DataStoreRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.bean.CumulativeShortUrlAccessBean;
import com.urltally.ws.stub.CumulativeShortUrlAccessListStub;
import com.urltally.ws.stub.CumulativeShortUrlAccessStub;
import com.urltally.ws.resource.ServiceManager;
import com.urltally.ws.resource.CumulativeShortUrlAccessResource;

// MockCumulativeShortUrlAccessResource is a decorator.
// It can be used as a base class to mock CumulativeShortUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/cumulativeShortUrlAccesses/")
public abstract class MockCumulativeShortUrlAccessResource implements CumulativeShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockCumulativeShortUrlAccessResource.class.getName());

    // MockCumulativeShortUrlAccessResource uses the decorator design pattern.
    private CumulativeShortUrlAccessResource decoratedResource;

    public MockCumulativeShortUrlAccessResource(CumulativeShortUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected CumulativeShortUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(CumulativeShortUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllCumulativeShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllCumulativeShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getCumulativeShortUrlAccessKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getCumulativeShortUrlAccessKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getCumulativeShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getCumulativeShortUrlAccess(guid);
    }

    @Override
    public Response getCumulativeShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getCumulativeShortUrlAccess(guid, field);
    }

    @Override
    public Response createCumulativeShortUrlAccess(CumulativeShortUrlAccessStub cumulativeShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createCumulativeShortUrlAccess(cumulativeShortUrlAccess);
    }

    @Override
    public Response updateCumulativeShortUrlAccess(String guid, CumulativeShortUrlAccessStub cumulativeShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateCumulativeShortUrlAccess(guid, cumulativeShortUrlAccess);
    }

    @Override
    public Response updateCumulativeShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseResourceException
    {
        return decoratedResource.updateCumulativeShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
    }

    @Override
    public Response deleteCumulativeShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteCumulativeShortUrlAccess(guid);
    }

    @Override
    public Response deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteCumulativeShortUrlAccesses(filter, params, values);
    }


}
