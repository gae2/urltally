package com.urltally.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.TotalShortUrlAccessDAO;
import com.urltally.ws.data.TotalShortUrlAccessDataObject;


public class DefaultTotalShortUrlAccessDAO extends DefaultDAOBase implements TotalShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(DefaultTotalShortUrlAccessDAO.class.getName()); 

    // Returns the totalShortUrlAccess for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public TotalShortUrlAccessDataObject getTotalShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        TotalShortUrlAccessDataObject totalShortUrlAccess = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = TotalShortUrlAccessDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = TotalShortUrlAccessDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                totalShortUrlAccess = pm.getObjectById(TotalShortUrlAccessDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve totalShortUrlAccess for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return totalShortUrlAccess;
	}

    @Override
    public List<TotalShortUrlAccessDataObject> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<TotalShortUrlAccessDataObject> totalShortUrlAccesses = null;
        if(guids != null && !guids.isEmpty()) {
            totalShortUrlAccesses = new ArrayList<TotalShortUrlAccessDataObject>();
            for(String guid : guids) {
                TotalShortUrlAccessDataObject obj = getTotalShortUrlAccess(guid); 
                totalShortUrlAccesses.add(obj);
            }
	    }

        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<TotalShortUrlAccessDataObject> getAllTotalShortUrlAccesses() throws BaseException
	{
	    return getAllTotalShortUrlAccesses(null, null, null);
    }


    @Override
    public List<TotalShortUrlAccessDataObject> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<TotalShortUrlAccessDataObject> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<TotalShortUrlAccessDataObject> totalShortUrlAccesses = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TotalShortUrlAccessDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            totalShortUrlAccesses = (List<TotalShortUrlAccessDataObject>) q.execute();
            if(totalShortUrlAccesses != null) {
                totalShortUrlAccesses.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(totalShortUrlAccesses);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            /*
            // ???
            Collection<TotalShortUrlAccessDataObject> rs_totalShortUrlAccesses = (Collection<TotalShortUrlAccessDataObject>) q.execute();
            if(rs_totalShortUrlAccesses == null) {
                log.log(Level.WARNING, "Failed to retrieve all totalShortUrlAccesses.");
                totalShortUrlAccesses = new ArrayList<TotalShortUrlAccessDataObject>();  // ???           
            } else {
                totalShortUrlAccesses = new ArrayList<TotalShortUrlAccessDataObject>(pm.detachCopyAll(rs_totalShortUrlAccesses));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all totalShortUrlAccesses.", ex);
            //totalShortUrlAccesses = new ArrayList<TotalShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all totalShortUrlAccesses.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + TotalShortUrlAccessDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all TotalShortUrlAccess keys.", ex);
            throw new DataStoreException("Failed to retrieve all TotalShortUrlAccess keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<TotalShortUrlAccessDataObject> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TotalShortUrlAccessDataObject> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TotalShortUrlAccessDataObject> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<TotalShortUrlAccessDataObject> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultTotalShortUrlAccessDAO.findTotalShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findTotalShortUrlAccesses() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<TotalShortUrlAccessDataObject> totalShortUrlAccesses = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TotalShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                totalShortUrlAccesses = (List<TotalShortUrlAccessDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                totalShortUrlAccesses = (List<TotalShortUrlAccessDataObject>) q.execute();
            }
            if(totalShortUrlAccesses != null) {
                totalShortUrlAccesses.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(totalShortUrlAccesses);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(TotalShortUrlAccessDataObject dobj : totalShortUrlAccesses) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find totalShortUrlAccesses because index is missing.", ex);
            //totalShortUrlAccesses = new ArrayList<TotalShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to find totalShortUrlAccesses because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find totalShortUrlAccesses meeting the criterion.", ex);
            //totalShortUrlAccesses = new ArrayList<TotalShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to find totalShortUrlAccesses meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return totalShortUrlAccesses;
	}

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTotalShortUrlAccessDAO.findTotalShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + TotalShortUrlAccessDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find TotalShortUrlAccess keys because index is missing.", ex);
            throw new DataStoreException("Failed to find TotalShortUrlAccess keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find TotalShortUrlAccess keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find TotalShortUrlAccess keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTotalShortUrlAccessDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(TotalShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get totalShortUrlAccess count because index is missing.", ex);
            throw new DataStoreException("Failed to get totalShortUrlAccess count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get totalShortUrlAccess count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get totalShortUrlAccess count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the totalShortUrlAccess in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeTotalShortUrlAccess(TotalShortUrlAccessDataObject totalShortUrlAccess) throws BaseException
    {
        log.fine("storeTotalShortUrlAccess() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = totalShortUrlAccess.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                totalShortUrlAccess.setCreatedTime(createdTime);
            }
            Long modifiedTime = totalShortUrlAccess.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                totalShortUrlAccess.setModifiedTime(createdTime);
            }
            pm.makePersistent(totalShortUrlAccess); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = totalShortUrlAccess.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store totalShortUrlAccess because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store totalShortUrlAccess because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store totalShortUrlAccess.", ex);
            throw new DataStoreException("Failed to store totalShortUrlAccess.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeTotalShortUrlAccess(): guid = " + guid);
        return guid;
    }

    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccessDataObject totalShortUrlAccess) throws BaseException
    {
        // The createdTime field will be automatically set in storeTotalShortUrlAccess().
        //Long createdTime = totalShortUrlAccess.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    totalShortUrlAccess.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = totalShortUrlAccess.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    totalShortUrlAccess.setModifiedTime(createdTime);
        //}
        return storeTotalShortUrlAccess(totalShortUrlAccess);
    }

    @Override
	public Boolean updateTotalShortUrlAccess(TotalShortUrlAccessDataObject totalShortUrlAccess) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeTotalShortUrlAccess()
	    // (in which case modifiedTime might be updated again).
	    totalShortUrlAccess.setModifiedTime(System.currentTimeMillis());
	    String guid = storeTotalShortUrlAccess(totalShortUrlAccess);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccessDataObject totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(totalShortUrlAccess);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete totalShortUrlAccess because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete totalShortUrlAccess because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete totalShortUrlAccess.", ex);
            throw new DataStoreException("Failed to delete totalShortUrlAccess.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = TotalShortUrlAccessDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = TotalShortUrlAccessDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                TotalShortUrlAccessDataObject totalShortUrlAccess = pm.getObjectById(TotalShortUrlAccessDataObject.class, key);
                pm.deletePersistent(totalShortUrlAccess);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete totalShortUrlAccess because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete totalShortUrlAccess because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete totalShortUrlAccess for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete totalShortUrlAccess for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultTotalShortUrlAccessDAO.deleteTotalShortUrlAccesses(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(TotalShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletetotalShortUrlAccesses because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete totalShortUrlAccesses because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete totalShortUrlAccesses because index is missing", ex);
            throw new DataStoreException("Failed to delete totalShortUrlAccesses because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete totalShortUrlAccesses", ex);
            throw new DataStoreException("Failed to delete totalShortUrlAccesses", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
