package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "currentShortUrlAccesses")
@XmlType(propOrder = {"currentShortUrlAccess", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentShortUrlAccessListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessListStub.class.getName());

    private List<CurrentShortUrlAccessStub> currentShortUrlAccesses = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public CurrentShortUrlAccessListStub()
    {
        this(new ArrayList<CurrentShortUrlAccessStub>());
    }
    public CurrentShortUrlAccessListStub(List<CurrentShortUrlAccessStub> currentShortUrlAccesses)
    {
        this(currentShortUrlAccesses, null);
    }
    public CurrentShortUrlAccessListStub(List<CurrentShortUrlAccessStub> currentShortUrlAccesses, String forwardCursor)
    {
        this.currentShortUrlAccesses = currentShortUrlAccesses;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(currentShortUrlAccesses == null) {
            return true;
        } else {
            return currentShortUrlAccesses.isEmpty();
        }
    }
    public int getSize()
    {
        if(currentShortUrlAccesses == null) {
            return 0;
        } else {
            return currentShortUrlAccesses.size();
        }
    }


    @XmlElement(name = "currentShortUrlAccess")
    public List<CurrentShortUrlAccessStub> getCurrentShortUrlAccess()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<CurrentShortUrlAccessStub> getList()
    {
        return currentShortUrlAccesses;
    }
    public void setList(List<CurrentShortUrlAccessStub> currentShortUrlAccesses)
    {
        this.currentShortUrlAccesses = currentShortUrlAccesses;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<CurrentShortUrlAccessStub> it = this.currentShortUrlAccesses.iterator();
        while(it.hasNext()) {
            CurrentShortUrlAccessStub currentShortUrlAccess = it.next();
            sb.append(currentShortUrlAccess.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CurrentShortUrlAccessListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of CurrentShortUrlAccessListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CurrentShortUrlAccessListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CurrentShortUrlAccessListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CurrentShortUrlAccessListStub object as a string.", e);
        }
        
        return null;
    }
    public static CurrentShortUrlAccessListStub fromJsonString(String jsonStr)
    {
        try {
            CurrentShortUrlAccessListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CurrentShortUrlAccessListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CurrentShortUrlAccessListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CurrentShortUrlAccessListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CurrentShortUrlAccessListStub object.", e);
        }
        
        return null;
    }

}
