package com.urltally.ws;



public interface CurrentShortUrlAccess extends ShortUrlAccess
{
    String  getStartDayHour();
    Long  getStartTime();
}
