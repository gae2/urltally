package com.urltally.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.bean.AccessTallyMasterBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.AccessTallyMasterDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.AccessTallyMasterService;


// AccessTallyMasterMockService is a decorator.
// It can be used as a base class to mock AccessTallyMasterService objects.
public abstract class AccessTallyMasterMockService implements AccessTallyMasterService
{
    private static final Logger log = Logger.getLogger(AccessTallyMasterMockService.class.getName());

    // AccessTallyMasterMockService uses the decorator design pattern.
    private AccessTallyMasterService decoratedService;

    public AccessTallyMasterMockService(AccessTallyMasterService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected AccessTallyMasterService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(AccessTallyMasterService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // AccessTallyMaster related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAccessTallyMaster(): guid = " + guid);
        AccessTallyMaster bean = decoratedService.getAccessTallyMaster(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getAccessTallyMaster(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getAccessTallyMaster(guid, field);
        return obj;
    }

    @Override
    public List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        log.fine("getAccessTallyMasters()");
        List<AccessTallyMaster> accessTallyMasters = decoratedService.getAccessTallyMasters(guids);
        log.finer("END");
        return accessTallyMasters;
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException
    {
        return getAllAccessTallyMasters(null, null, null);
    }


    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyMasters(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<AccessTallyMaster> accessTallyMasters = decoratedService.getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
        log.finer("END");
        return accessTallyMasters;
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyMasterKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyMasterMockService.findAccessTallyMasters(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<AccessTallyMaster> accessTallyMasters = decoratedService.findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return accessTallyMasters;
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyMasterMockService.findAccessTallyMasterKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyMasterMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        return decoratedService.createAccessTallyMaster(tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }

    @Override
    public String createAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createAccessTallyMaster(accessTallyMaster);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        return decoratedService.updateAccessTallyMaster(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }
        
    @Override
    public Boolean updateAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateAccessTallyMaster(accessTallyMaster);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteAccessTallyMaster(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteAccessTallyMaster(accessTallyMaster);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteAccessTallyMasters(filter, params, values);
        return count;
    }

}
