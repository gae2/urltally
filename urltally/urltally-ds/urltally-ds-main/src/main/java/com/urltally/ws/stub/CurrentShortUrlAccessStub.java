package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "currentShortUrlAccess")
@XmlType(propOrder = {"guid", "tallyTime", "tallyEpoch", "count", "shortUrl", "shortUrlDomain", "longUrl", "longUrlDomain", "redirectType", "refererDomain", "userAgent", "language", "country", "talliedTime", "startDayHour", "startTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentShortUrlAccessStub extends ShortUrlAccessStub implements CurrentShortUrlAccess, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessStub.class.getName());

    private String startDayHour;
    private Long startTime;

    public CurrentShortUrlAccessStub()
    {
        this(null);
    }
    public CurrentShortUrlAccessStub(CurrentShortUrlAccess bean)
    {
        super(bean);
        if(bean != null) {
            this.startDayHour = bean.getStartDayHour();
            this.startTime = bean.getStartTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getTallyTime()
    {
        return super.getTallyTime();
    }
    public void setTallyTime(String tallyTime)
    {
        super.setTallyTime(tallyTime);
    }

    @XmlElement
    public Long getTallyEpoch()
    {
        return super.getTallyEpoch();
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        super.setTallyEpoch(tallyEpoch);
    }

    @XmlElement
    public Integer getCount()
    {
        return super.getCount();
    }
    public void setCount(Integer count)
    {
        super.setCount(count);
    }

    @XmlElement
    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    @XmlElement
    public String getShortUrlDomain()
    {
        return super.getShortUrlDomain();
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        super.setShortUrlDomain(shortUrlDomain);
    }

    @XmlElement
    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    @XmlElement
    public String getLongUrlDomain()
    {
        return super.getLongUrlDomain();
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        super.setLongUrlDomain(longUrlDomain);
    }

    @XmlElement
    public String getRedirectType()
    {
        return super.getRedirectType();
    }
    public void setRedirectType(String redirectType)
    {
        super.setRedirectType(redirectType);
    }

    @XmlElement
    public String getRefererDomain()
    {
        return super.getRefererDomain();
    }
    public void setRefererDomain(String refererDomain)
    {
        super.setRefererDomain(refererDomain);
    }

    @XmlElement
    public String getUserAgent()
    {
        return super.getUserAgent();
    }
    public void setUserAgent(String userAgent)
    {
        super.setUserAgent(userAgent);
    }

    @XmlElement
    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    @XmlElement
    public String getCountry()
    {
        return super.getCountry();
    }
    public void setCountry(String country)
    {
        super.setCountry(country);
    }

    @XmlElement
    public Long getTalliedTime()
    {
        return super.getTalliedTime();
    }
    public void setTalliedTime(Long talliedTime)
    {
        super.setTalliedTime(talliedTime);
    }

    @XmlElement
    public String getStartDayHour()
    {
        return this.startDayHour;
    }
    public void setStartDayHour(String startDayHour)
    {
        this.startDayHour = startDayHour;
    }

    @XmlElement
    public Long getStartTime()
    {
        return this.startTime;
    }
    public void setStartTime(Long startTime)
    {
        this.startTime = startTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("startDayHour", this.startDayHour);
        dataMap.put("startTime", this.startTime);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = startDayHour == null ? 0 : startDayHour.hashCode();
        _hash = 31 * _hash + delta;
        delta = startTime == null ? 0 : startTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static CurrentShortUrlAccessStub convertBeanToStub(CurrentShortUrlAccess bean)
    {
        CurrentShortUrlAccessStub stub = null;
        if(bean instanceof CurrentShortUrlAccessStub) {
            stub = (CurrentShortUrlAccessStub) bean;
        } else {
            if(bean != null) {
                stub = new CurrentShortUrlAccessStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static CurrentShortUrlAccessStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Json string representation of CurrentShortUrlAccessStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write CurrentShortUrlAccessStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write CurrentShortUrlAccessStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write CurrentShortUrlAccessStub object as a string.", e);
        }
        
        return null;
    }
    public static CurrentShortUrlAccessStub fromJsonString(String jsonStr)
    {
        try {
            CurrentShortUrlAccessStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, CurrentShortUrlAccessStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into CurrentShortUrlAccessStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into CurrentShortUrlAccessStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into CurrentShortUrlAccessStub object.", e);
        }
        
        return null;
    }

}
