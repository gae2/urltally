package com.urltally.ws.resource;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.service.ApiConsumerService;
import com.urltally.ws.service.impl.ApiConsumerServiceImpl;
import com.urltally.ws.service.UserService;
import com.urltally.ws.service.impl.UserServiceImpl;
import com.urltally.ws.service.AccessTallyMasterService;
import com.urltally.ws.service.impl.AccessTallyMasterServiceImpl;
import com.urltally.ws.service.AccessTallyStatusService;
import com.urltally.ws.service.impl.AccessTallyStatusServiceImpl;
import com.urltally.ws.service.MonthlyShortUrlAccessService;
import com.urltally.ws.service.impl.MonthlyShortUrlAccessServiceImpl;
import com.urltally.ws.service.WeeklyShortUrlAccessService;
import com.urltally.ws.service.impl.WeeklyShortUrlAccessServiceImpl;
import com.urltally.ws.service.DailyShortUrlAccessService;
import com.urltally.ws.service.impl.DailyShortUrlAccessServiceImpl;
import com.urltally.ws.service.HourlyShortUrlAccessService;
import com.urltally.ws.service.impl.HourlyShortUrlAccessServiceImpl;
import com.urltally.ws.service.CumulativeShortUrlAccessService;
import com.urltally.ws.service.impl.CumulativeShortUrlAccessServiceImpl;
import com.urltally.ws.service.CurrentShortUrlAccessService;
import com.urltally.ws.service.impl.CurrentShortUrlAccessServiceImpl;
import com.urltally.ws.service.TotalShortUrlAccessService;
import com.urltally.ws.service.impl.TotalShortUrlAccessServiceImpl;
import com.urltally.ws.service.TotalLongUrlAccessService;
import com.urltally.ws.service.impl.TotalLongUrlAccessServiceImpl;
import com.urltally.ws.service.ServiceInfoService;
import com.urltally.ws.service.impl.ServiceInfoServiceImpl;
import com.urltally.ws.service.FiveTenService;
import com.urltally.ws.service.impl.FiveTenServiceImpl;


// TBD:
// Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

	private static ApiConsumerService apiConsumerService = null;
	private static UserService userService = null;
	private static AccessTallyMasterService accessTallyMasterService = null;
	private static AccessTallyStatusService accessTallyStatusService = null;
	private static MonthlyShortUrlAccessService monthlyShortUrlAccessService = null;
	private static WeeklyShortUrlAccessService weeklyShortUrlAccessService = null;
	private static DailyShortUrlAccessService dailyShortUrlAccessService = null;
	private static HourlyShortUrlAccessService hourlyShortUrlAccessService = null;
	private static CumulativeShortUrlAccessService cumulativeShortUrlAccessService = null;
	private static CurrentShortUrlAccessService currentShortUrlAccessService = null;
	private static TotalShortUrlAccessService totalShortUrlAccessService = null;
	private static TotalLongUrlAccessService totalLongUrlAccessService = null;
	private static ServiceInfoService serviceInfoService = null;
	private static FiveTenService fiveTenService = null;

    // Prevents instantiation.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(ServiceManager.apiConsumerService == null) {
            ServiceManager.apiConsumerService = new ApiConsumerServiceImpl();
        }
        return ServiceManager.apiConsumerService;
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(ServiceManager.userService == null) {
            ServiceManager.userService = new UserServiceImpl();
        }
        return ServiceManager.userService;
    }

    // Returns a AccessTallyMasterService instance.
	public static AccessTallyMasterService getAccessTallyMasterService() 
    {
        if(ServiceManager.accessTallyMasterService == null) {
            ServiceManager.accessTallyMasterService = new AccessTallyMasterServiceImpl();
        }
        return ServiceManager.accessTallyMasterService;
    }

    // Returns a AccessTallyStatusService instance.
	public static AccessTallyStatusService getAccessTallyStatusService() 
    {
        if(ServiceManager.accessTallyStatusService == null) {
            ServiceManager.accessTallyStatusService = new AccessTallyStatusServiceImpl();
        }
        return ServiceManager.accessTallyStatusService;
    }

    // Returns a MonthlyShortUrlAccessService instance.
	public static MonthlyShortUrlAccessService getMonthlyShortUrlAccessService() 
    {
        if(ServiceManager.monthlyShortUrlAccessService == null) {
            ServiceManager.monthlyShortUrlAccessService = new MonthlyShortUrlAccessServiceImpl();
        }
        return ServiceManager.monthlyShortUrlAccessService;
    }

    // Returns a WeeklyShortUrlAccessService instance.
	public static WeeklyShortUrlAccessService getWeeklyShortUrlAccessService() 
    {
        if(ServiceManager.weeklyShortUrlAccessService == null) {
            ServiceManager.weeklyShortUrlAccessService = new WeeklyShortUrlAccessServiceImpl();
        }
        return ServiceManager.weeklyShortUrlAccessService;
    }

    // Returns a DailyShortUrlAccessService instance.
	public static DailyShortUrlAccessService getDailyShortUrlAccessService() 
    {
        if(ServiceManager.dailyShortUrlAccessService == null) {
            ServiceManager.dailyShortUrlAccessService = new DailyShortUrlAccessServiceImpl();
        }
        return ServiceManager.dailyShortUrlAccessService;
    }

    // Returns a HourlyShortUrlAccessService instance.
	public static HourlyShortUrlAccessService getHourlyShortUrlAccessService() 
    {
        if(ServiceManager.hourlyShortUrlAccessService == null) {
            ServiceManager.hourlyShortUrlAccessService = new HourlyShortUrlAccessServiceImpl();
        }
        return ServiceManager.hourlyShortUrlAccessService;
    }

    // Returns a CumulativeShortUrlAccessService instance.
	public static CumulativeShortUrlAccessService getCumulativeShortUrlAccessService() 
    {
        if(ServiceManager.cumulativeShortUrlAccessService == null) {
            ServiceManager.cumulativeShortUrlAccessService = new CumulativeShortUrlAccessServiceImpl();
        }
        return ServiceManager.cumulativeShortUrlAccessService;
    }

    // Returns a CurrentShortUrlAccessService instance.
	public static CurrentShortUrlAccessService getCurrentShortUrlAccessService() 
    {
        if(ServiceManager.currentShortUrlAccessService == null) {
            ServiceManager.currentShortUrlAccessService = new CurrentShortUrlAccessServiceImpl();
        }
        return ServiceManager.currentShortUrlAccessService;
    }

    // Returns a TotalShortUrlAccessService instance.
	public static TotalShortUrlAccessService getTotalShortUrlAccessService() 
    {
        if(ServiceManager.totalShortUrlAccessService == null) {
            ServiceManager.totalShortUrlAccessService = new TotalShortUrlAccessServiceImpl();
        }
        return ServiceManager.totalShortUrlAccessService;
    }

    // Returns a TotalLongUrlAccessService instance.
	public static TotalLongUrlAccessService getTotalLongUrlAccessService() 
    {
        if(ServiceManager.totalLongUrlAccessService == null) {
            ServiceManager.totalLongUrlAccessService = new TotalLongUrlAccessServiceImpl();
        }
        return ServiceManager.totalLongUrlAccessService;
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(ServiceManager.serviceInfoService == null) {
            ServiceManager.serviceInfoService = new ServiceInfoServiceImpl();
        }
        return ServiceManager.serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(ServiceManager.fiveTenService == null) {
            ServiceManager.fiveTenService = new FiveTenServiceImpl();
        }
        return ServiceManager.fiveTenService;
    }

}
