package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "accessTallyStatuses")
@XmlType(propOrder = {"accessTallyStatus", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessTallyStatusListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AccessTallyStatusListStub.class.getName());

    private List<AccessTallyStatusStub> accessTallyStatuses = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public AccessTallyStatusListStub()
    {
        this(new ArrayList<AccessTallyStatusStub>());
    }
    public AccessTallyStatusListStub(List<AccessTallyStatusStub> accessTallyStatuses)
    {
        this(accessTallyStatuses, null);
    }
    public AccessTallyStatusListStub(List<AccessTallyStatusStub> accessTallyStatuses, String forwardCursor)
    {
        this.accessTallyStatuses = accessTallyStatuses;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(accessTallyStatuses == null) {
            return true;
        } else {
            return accessTallyStatuses.isEmpty();
        }
    }
    public int getSize()
    {
        if(accessTallyStatuses == null) {
            return 0;
        } else {
            return accessTallyStatuses.size();
        }
    }


    @XmlElement(name = "accessTallyStatus")
    public List<AccessTallyStatusStub> getAccessTallyStatus()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AccessTallyStatusStub> getList()
    {
        return accessTallyStatuses;
    }
    public void setList(List<AccessTallyStatusStub> accessTallyStatuses)
    {
        this.accessTallyStatuses = accessTallyStatuses;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<AccessTallyStatusStub> it = this.accessTallyStatuses.iterator();
        while(it.hasNext()) {
            AccessTallyStatusStub accessTallyStatus = it.next();
            sb.append(accessTallyStatus.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AccessTallyStatusListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AccessTallyStatusListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AccessTallyStatusListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AccessTallyStatusListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AccessTallyStatusListStub object as a string.", e);
        }
        
        return null;
    }
    public static AccessTallyStatusListStub fromJsonString(String jsonStr)
    {
        try {
            AccessTallyStatusListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AccessTallyStatusListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AccessTallyStatusListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AccessTallyStatusListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AccessTallyStatusListStub object.", e);
        }
        
        return null;
    }

}
