package com.urltally.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.urltally.ws.ShortUrlAccess;
import com.urltally.ws.util.CommonUtil;
import com.urltally.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class ShortUrlAccessDataObject extends KeyedDataObject implements ShortUrlAccess
{
    private static final Logger log = Logger.getLogger(ShortUrlAccessDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ShortUrlAccessDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ShortUrlAccessDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String tallyTime;

    @Persistent(defaultFetchGroup = "true")
    private Long tallyEpoch;

    @Persistent(defaultFetchGroup = "true")
    private Integer count;

    @Persistent(defaultFetchGroup = "true")
    private String shortUrl;

    @Persistent(defaultFetchGroup = "true")
    private String shortUrlDomain;

    @Persistent(defaultFetchGroup = "true")
    private String longUrl;

    @Persistent(defaultFetchGroup = "true")
    private String longUrlDomain;

    @Persistent(defaultFetchGroup = "true")
    private String redirectType;

    @Persistent(defaultFetchGroup = "true")
    private String refererDomain;

    @Persistent(defaultFetchGroup = "true")
    private String userAgent;

    @Persistent(defaultFetchGroup = "true")
    private String language;

    @Persistent(defaultFetchGroup = "true")
    private String country;

    @Persistent(defaultFetchGroup = "true")
    private Long talliedTime;

    public ShortUrlAccessDataObject()
    {
        this(null);
    }
    public ShortUrlAccessDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ShortUrlAccessDataObject(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime)
    {
        this(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, null, null);
    }
    public ShortUrlAccessDataObject(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.count = count;
        this.shortUrl = shortUrl;
        this.shortUrlDomain = shortUrlDomain;
        this.longUrl = longUrl;
        this.longUrlDomain = longUrlDomain;
        this.redirectType = redirectType;
        this.refererDomain = refererDomain;
        this.userAgent = userAgent;
        this.language = language;
        this.country = country;
        this.talliedTime = talliedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return ShortUrlAccessDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ShortUrlAccessDataObject.composeKey(getGuid());
    }

    public String getTallyTime()
    {
        return this.tallyTime;
    }
    public void setTallyTime(String tallyTime)
    {
        this.tallyTime = tallyTime;
    }

    public Long getTallyEpoch()
    {
        return this.tallyEpoch;
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        this.tallyEpoch = tallyEpoch;
    }

    public Integer getCount()
    {
        return this.count;
    }
    public void setCount(Integer count)
    {
        this.count = count;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public String getShortUrlDomain()
    {
        return this.shortUrlDomain;
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        this.shortUrlDomain = shortUrlDomain;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getLongUrlDomain()
    {
        return this.longUrlDomain;
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        this.longUrlDomain = longUrlDomain;
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    public String getRefererDomain()
    {
        return this.refererDomain;
    }
    public void setRefererDomain(String refererDomain)
    {
        this.refererDomain = refererDomain;
    }

    public String getUserAgent()
    {
        return this.userAgent;
    }
    public void setUserAgent(String userAgent)
    {
        this.userAgent = userAgent;
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getCountry()
    {
        return this.country;
    }
    public void setCountry(String country)
    {
        this.country = country;
    }

    public Long getTalliedTime()
    {
        return this.talliedTime;
    }
    public void setTalliedTime(Long talliedTime)
    {
        this.talliedTime = talliedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("tallyTime", this.tallyTime);
        dataMap.put("tallyEpoch", this.tallyEpoch);
        dataMap.put("count", this.count);
        dataMap.put("shortUrl", this.shortUrl);
        dataMap.put("shortUrlDomain", this.shortUrlDomain);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("longUrlDomain", this.longUrlDomain);
        dataMap.put("redirectType", this.redirectType);
        dataMap.put("refererDomain", this.refererDomain);
        dataMap.put("userAgent", this.userAgent);
        dataMap.put("language", this.language);
        dataMap.put("country", this.country);
        dataMap.put("talliedTime", this.talliedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ShortUrlAccess thatObj = (ShortUrlAccess) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.tallyTime == null && thatObj.getTallyTime() != null)
            || (this.tallyTime != null && thatObj.getTallyTime() == null)
            || !this.tallyTime.equals(thatObj.getTallyTime()) ) {
            return false;
        }
        if( (this.tallyEpoch == null && thatObj.getTallyEpoch() != null)
            || (this.tallyEpoch != null && thatObj.getTallyEpoch() == null)
            || !this.tallyEpoch.equals(thatObj.getTallyEpoch()) ) {
            return false;
        }
        if( (this.count == null && thatObj.getCount() != null)
            || (this.count != null && thatObj.getCount() == null)
            || !this.count.equals(thatObj.getCount()) ) {
            return false;
        }
        if( (this.shortUrl == null && thatObj.getShortUrl() != null)
            || (this.shortUrl != null && thatObj.getShortUrl() == null)
            || !this.shortUrl.equals(thatObj.getShortUrl()) ) {
            return false;
        }
        if( (this.shortUrlDomain == null && thatObj.getShortUrlDomain() != null)
            || (this.shortUrlDomain != null && thatObj.getShortUrlDomain() == null)
            || !this.shortUrlDomain.equals(thatObj.getShortUrlDomain()) ) {
            return false;
        }
        if( (this.longUrl == null && thatObj.getLongUrl() != null)
            || (this.longUrl != null && thatObj.getLongUrl() == null)
            || !this.longUrl.equals(thatObj.getLongUrl()) ) {
            return false;
        }
        if( (this.longUrlDomain == null && thatObj.getLongUrlDomain() != null)
            || (this.longUrlDomain != null && thatObj.getLongUrlDomain() == null)
            || !this.longUrlDomain.equals(thatObj.getLongUrlDomain()) ) {
            return false;
        }
        if( (this.redirectType == null && thatObj.getRedirectType() != null)
            || (this.redirectType != null && thatObj.getRedirectType() == null)
            || !this.redirectType.equals(thatObj.getRedirectType()) ) {
            return false;
        }
        if( (this.refererDomain == null && thatObj.getRefererDomain() != null)
            || (this.refererDomain != null && thatObj.getRefererDomain() == null)
            || !this.refererDomain.equals(thatObj.getRefererDomain()) ) {
            return false;
        }
        if( (this.userAgent == null && thatObj.getUserAgent() != null)
            || (this.userAgent != null && thatObj.getUserAgent() == null)
            || !this.userAgent.equals(thatObj.getUserAgent()) ) {
            return false;
        }
        if( (this.language == null && thatObj.getLanguage() != null)
            || (this.language != null && thatObj.getLanguage() == null)
            || !this.language.equals(thatObj.getLanguage()) ) {
            return false;
        }
        if( (this.country == null && thatObj.getCountry() != null)
            || (this.country != null && thatObj.getCountry() == null)
            || !this.country.equals(thatObj.getCountry()) ) {
            return false;
        }
        if( (this.talliedTime == null && thatObj.getTalliedTime() != null)
            || (this.talliedTime != null && thatObj.getTalliedTime() == null)
            || !this.talliedTime.equals(thatObj.getTalliedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyTime == null ? 0 : tallyTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyEpoch == null ? 0 : tallyEpoch.hashCode();
        _hash = 31 * _hash + delta;
        delta = count == null ? 0 : count.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrl == null ? 0 : shortUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = shortUrlDomain == null ? 0 : shortUrlDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlDomain == null ? 0 : longUrlDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectType == null ? 0 : redirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = refererDomain == null ? 0 : refererDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAgent == null ? 0 : userAgent.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = country == null ? 0 : country.hashCode();
        _hash = 31 * _hash + delta;
        delta = talliedTime == null ? 0 : talliedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
