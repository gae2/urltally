package com.urltally.ws;



public interface CumulativeShortUrlAccess extends ShortUrlAccess
{
    String  getStartDayHour();
    String  getEndDayHour();
    Long  getStartTime();
    Long  getEndTime();
}
