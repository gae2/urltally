package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.data.DailyShortUrlAccessDataObject;

public class DailyShortUrlAccessBean extends ShortUrlAccessBean implements DailyShortUrlAccess
{
    private static final Logger log = Logger.getLogger(DailyShortUrlAccessBean.class.getName());

    // Embedded data object.
    private DailyShortUrlAccessDataObject dobj = null;

    public DailyShortUrlAccessBean()
    {
        this(new DailyShortUrlAccessDataObject());
    }
    public DailyShortUrlAccessBean(String guid)
    {
        this(new DailyShortUrlAccessDataObject(guid));
    }
    public DailyShortUrlAccessBean(DailyShortUrlAccessDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public DailyShortUrlAccessDataObject getDataObject()
    {
        return this.dobj;
    }

    public Integer getYear()
    {
        if(getDataObject() != null) {
            return getDataObject().getYear();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DailyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setYear(Integer year)
    {
        if(getDataObject() != null) {
            getDataObject().setYear(year);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DailyShortUrlAccessDataObject is null!");
        }
    }

    public Integer getDay()
    {
        if(getDataObject() != null) {
            return getDataObject().getDay();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DailyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setDay(Integer day)
    {
        if(getDataObject() != null) {
            getDataObject().setDay(day);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DailyShortUrlAccessDataObject is null!");
        }
    }


    // TBD
    public DailyShortUrlAccessDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
