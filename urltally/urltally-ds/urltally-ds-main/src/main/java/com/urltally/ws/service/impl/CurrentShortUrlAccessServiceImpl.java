package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.bean.CurrentShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.CurrentShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.CurrentShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CurrentShortUrlAccessServiceImpl implements CurrentShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // CurrentShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public CurrentShortUrlAccess getCurrentShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        CurrentShortUrlAccessDataObject dataObj = getDAOFactory().getCurrentShortUrlAccessDAO().getCurrentShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        CurrentShortUrlAccessBean bean = new CurrentShortUrlAccessBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getCurrentShortUrlAccess(String guid, String field) throws BaseException
    {
        CurrentShortUrlAccessDataObject dataObj = getDAOFactory().getCurrentShortUrlAccessDAO().getCurrentShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("count")) {
            return dataObj.getCount();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return dataObj.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return dataObj.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return dataObj.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return dataObj.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return dataObj.getUserAgent();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("country")) {
            return dataObj.getCountry();
        } else if(field.equals("talliedTime")) {
            return dataObj.getTalliedTime();
        } else if(field.equals("startDayHour")) {
            return dataObj.getStartDayHour();
        } else if(field.equals("startTime")) {
            return dataObj.getStartTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<CurrentShortUrlAccess> getCurrentShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<CurrentShortUrlAccess> list = new ArrayList<CurrentShortUrlAccess>();
        List<CurrentShortUrlAccessDataObject> dataObjs = getDAOFactory().getCurrentShortUrlAccessDAO().getCurrentShortUrlAccesses(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccessDataObject list.");
        } else {
            Iterator<CurrentShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                CurrentShortUrlAccessDataObject dataObj = (CurrentShortUrlAccessDataObject) it.next();
                list.add(new CurrentShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses() throws BaseException
    {
        return getAllCurrentShortUrlAccesses(null, null, null);
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllCurrentShortUrlAccesses(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<CurrentShortUrlAccess> list = new ArrayList<CurrentShortUrlAccess>();
        List<CurrentShortUrlAccessDataObject> dataObjs = getDAOFactory().getCurrentShortUrlAccessDAO().getAllCurrentShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccessDataObject list.");
        } else {
            Iterator<CurrentShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                CurrentShortUrlAccessDataObject dataObj = (CurrentShortUrlAccessDataObject) it.next();
                list.add(new CurrentShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllCurrentShortUrlAccessKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getCurrentShortUrlAccessDAO().getAllCurrentShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccess key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("CurrentShortUrlAccessServiceImpl.findCurrentShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<CurrentShortUrlAccess> list = new ArrayList<CurrentShortUrlAccess>();
        List<CurrentShortUrlAccessDataObject> dataObjs = getDAOFactory().getCurrentShortUrlAccessDAO().findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find currentShortUrlAccesses for the given criterion.");
        } else {
            Iterator<CurrentShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                CurrentShortUrlAccessDataObject dataObj = (CurrentShortUrlAccessDataObject) it.next();
                list.add(new CurrentShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("CurrentShortUrlAccessServiceImpl.findCurrentShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getCurrentShortUrlAccessDAO().findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find CurrentShortUrlAccess keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("CurrentShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getCurrentShortUrlAccessDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createCurrentShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        CurrentShortUrlAccessDataObject dataObj = new CurrentShortUrlAccessDataObject(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
        return createCurrentShortUrlAccess(dataObj);
    }

    @Override
    public String createCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param currentShortUrlAccess cannot be null.....
        if(currentShortUrlAccess == null) {
            log.log(Level.INFO, "Param currentShortUrlAccess is null!");
            throw new BadRequestException("Param currentShortUrlAccess object is null!");
        }
        CurrentShortUrlAccessDataObject dataObj = null;
        if(currentShortUrlAccess instanceof CurrentShortUrlAccessDataObject) {
            dataObj = (CurrentShortUrlAccessDataObject) currentShortUrlAccess;
        } else if(currentShortUrlAccess instanceof CurrentShortUrlAccessBean) {
            dataObj = ((CurrentShortUrlAccessBean) currentShortUrlAccess).toDataObject();
        } else {  // if(currentShortUrlAccess instanceof CurrentShortUrlAccess)
            //dataObj = new CurrentShortUrlAccessDataObject(null, currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new CurrentShortUrlAccessDataObject(currentShortUrlAccess.getGuid(), currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
        }
        String guid = getDAOFactory().getCurrentShortUrlAccessDAO().createCurrentShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateCurrentShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        CurrentShortUrlAccessDataObject dataObj = new CurrentShortUrlAccessDataObject(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
        return updateCurrentShortUrlAccess(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param currentShortUrlAccess cannot be null.....
        if(currentShortUrlAccess == null || currentShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param currentShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param currentShortUrlAccess object or its guid is null!");
        }
        CurrentShortUrlAccessDataObject dataObj = null;
        if(currentShortUrlAccess instanceof CurrentShortUrlAccessDataObject) {
            dataObj = (CurrentShortUrlAccessDataObject) currentShortUrlAccess;
        } else if(currentShortUrlAccess instanceof CurrentShortUrlAccessBean) {
            dataObj = ((CurrentShortUrlAccessBean) currentShortUrlAccess).toDataObject();
        } else {  // if(currentShortUrlAccess instanceof CurrentShortUrlAccess)
            dataObj = new CurrentShortUrlAccessDataObject(currentShortUrlAccess.getGuid(), currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
        }
        Boolean suc = getDAOFactory().getCurrentShortUrlAccessDAO().updateCurrentShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteCurrentShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getCurrentShortUrlAccessDAO().deleteCurrentShortUrlAccess(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param currentShortUrlAccess cannot be null.....
        if(currentShortUrlAccess == null || currentShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param currentShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param currentShortUrlAccess object or its guid is null!");
        }
        CurrentShortUrlAccessDataObject dataObj = null;
        if(currentShortUrlAccess instanceof CurrentShortUrlAccessDataObject) {
            dataObj = (CurrentShortUrlAccessDataObject) currentShortUrlAccess;
        } else if(currentShortUrlAccess instanceof CurrentShortUrlAccessBean) {
            dataObj = ((CurrentShortUrlAccessBean) currentShortUrlAccess).toDataObject();
        } else {  // if(currentShortUrlAccess instanceof CurrentShortUrlAccess)
            dataObj = new CurrentShortUrlAccessDataObject(currentShortUrlAccess.getGuid(), currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
        }
        Boolean suc = getDAOFactory().getCurrentShortUrlAccessDAO().deleteCurrentShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getCurrentShortUrlAccessDAO().deleteCurrentShortUrlAccesses(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
