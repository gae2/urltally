package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.bean.DailyShortUrlAccessBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.DailyShortUrlAccessDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.DailyShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DailyShortUrlAccessServiceImpl implements DailyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(DailyShortUrlAccessServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // DailyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DailyShortUrlAccess getDailyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        DailyShortUrlAccessDataObject dataObj = getDAOFactory().getDailyShortUrlAccessDAO().getDailyShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        DailyShortUrlAccessBean bean = new DailyShortUrlAccessBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getDailyShortUrlAccess(String guid, String field) throws BaseException
    {
        DailyShortUrlAccessDataObject dataObj = getDAOFactory().getDailyShortUrlAccessDAO().getDailyShortUrlAccess(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccessDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("count")) {
            return dataObj.getCount();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return dataObj.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return dataObj.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return dataObj.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return dataObj.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return dataObj.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return dataObj.getUserAgent();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("country")) {
            return dataObj.getCountry();
        } else if(field.equals("talliedTime")) {
            return dataObj.getTalliedTime();
        } else if(field.equals("year")) {
            return dataObj.getYear();
        } else if(field.equals("day")) {
            return dataObj.getDay();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DailyShortUrlAccess> getDailyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<DailyShortUrlAccess> list = new ArrayList<DailyShortUrlAccess>();
        List<DailyShortUrlAccessDataObject> dataObjs = getDAOFactory().getDailyShortUrlAccessDAO().getDailyShortUrlAccesses(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccessDataObject list.");
        } else {
            Iterator<DailyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DailyShortUrlAccessDataObject dataObj = (DailyShortUrlAccessDataObject) it.next();
                list.add(new DailyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses() throws BaseException
    {
        return getAllDailyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDailyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDailyShortUrlAccesses(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DailyShortUrlAccess> list = new ArrayList<DailyShortUrlAccess>();
        List<DailyShortUrlAccessDataObject> dataObjs = getDAOFactory().getDailyShortUrlAccessDAO().getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccessDataObject list.");
        } else {
            Iterator<DailyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DailyShortUrlAccessDataObject dataObj = (DailyShortUrlAccessDataObject) it.next();
                list.add(new DailyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDailyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllDailyShortUrlAccessKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getDailyShortUrlAccessDAO().getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccess key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DailyShortUrlAccessServiceImpl.findDailyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DailyShortUrlAccess> list = new ArrayList<DailyShortUrlAccess>();
        List<DailyShortUrlAccessDataObject> dataObjs = getDAOFactory().getDailyShortUrlAccessDAO().findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find dailyShortUrlAccesses for the given criterion.");
        } else {
            Iterator<DailyShortUrlAccessDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                DailyShortUrlAccessDataObject dataObj = (DailyShortUrlAccessDataObject) it.next();
                list.add(new DailyShortUrlAccessBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DailyShortUrlAccessServiceImpl.findDailyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getDailyShortUrlAccessDAO().findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find DailyShortUrlAccess keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("DailyShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getDailyShortUrlAccessDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createDailyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        DailyShortUrlAccessDataObject dataObj = new DailyShortUrlAccessDataObject(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
        return createDailyShortUrlAccess(dataObj);
    }

    @Override
    public String createDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param dailyShortUrlAccess cannot be null.....
        if(dailyShortUrlAccess == null) {
            log.log(Level.INFO, "Param dailyShortUrlAccess is null!");
            throw new BadRequestException("Param dailyShortUrlAccess object is null!");
        }
        DailyShortUrlAccessDataObject dataObj = null;
        if(dailyShortUrlAccess instanceof DailyShortUrlAccessDataObject) {
            dataObj = (DailyShortUrlAccessDataObject) dailyShortUrlAccess;
        } else if(dailyShortUrlAccess instanceof DailyShortUrlAccessBean) {
            dataObj = ((DailyShortUrlAccessBean) dailyShortUrlAccess).toDataObject();
        } else {  // if(dailyShortUrlAccess instanceof DailyShortUrlAccess)
            //dataObj = new DailyShortUrlAccessDataObject(null, dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new DailyShortUrlAccessDataObject(dailyShortUrlAccess.getGuid(), dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
        }
        String guid = getDAOFactory().getDailyShortUrlAccessDAO().createDailyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DailyShortUrlAccessDataObject dataObj = new DailyShortUrlAccessDataObject(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
        return updateDailyShortUrlAccess(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param dailyShortUrlAccess cannot be null.....
        if(dailyShortUrlAccess == null || dailyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param dailyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param dailyShortUrlAccess object or its guid is null!");
        }
        DailyShortUrlAccessDataObject dataObj = null;
        if(dailyShortUrlAccess instanceof DailyShortUrlAccessDataObject) {
            dataObj = (DailyShortUrlAccessDataObject) dailyShortUrlAccess;
        } else if(dailyShortUrlAccess instanceof DailyShortUrlAccessBean) {
            dataObj = ((DailyShortUrlAccessBean) dailyShortUrlAccess).toDataObject();
        } else {  // if(dailyShortUrlAccess instanceof DailyShortUrlAccess)
            dataObj = new DailyShortUrlAccessDataObject(dailyShortUrlAccess.getGuid(), dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
        }
        Boolean suc = getDAOFactory().getDailyShortUrlAccessDAO().updateDailyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteDailyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getDailyShortUrlAccessDAO().deleteDailyShortUrlAccess(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param dailyShortUrlAccess cannot be null.....
        if(dailyShortUrlAccess == null || dailyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param dailyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param dailyShortUrlAccess object or its guid is null!");
        }
        DailyShortUrlAccessDataObject dataObj = null;
        if(dailyShortUrlAccess instanceof DailyShortUrlAccessDataObject) {
            dataObj = (DailyShortUrlAccessDataObject) dailyShortUrlAccess;
        } else if(dailyShortUrlAccess instanceof DailyShortUrlAccessBean) {
            dataObj = ((DailyShortUrlAccessBean) dailyShortUrlAccess).toDataObject();
        } else {  // if(dailyShortUrlAccess instanceof DailyShortUrlAccess)
            dataObj = new DailyShortUrlAccessDataObject(dailyShortUrlAccess.getGuid(), dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
        }
        Boolean suc = getDAOFactory().getDailyShortUrlAccessDAO().deleteDailyShortUrlAccess(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getDailyShortUrlAccessDAO().deleteDailyShortUrlAccesses(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
