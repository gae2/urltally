package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.TotalShortUrlAccessDAO;
import com.urltally.ws.data.TotalShortUrlAccessDataObject;


// MockTotalShortUrlAccessDAO is a decorator.
// It can be used as a base class to mock TotalShortUrlAccessDAO objects.
public abstract class MockTotalShortUrlAccessDAO implements TotalShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(MockTotalShortUrlAccessDAO.class.getName()); 

    // MockTotalShortUrlAccessDAO uses the decorator design pattern.
    private TotalShortUrlAccessDAO decoratedDAO;

    public MockTotalShortUrlAccessDAO(TotalShortUrlAccessDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected TotalShortUrlAccessDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(TotalShortUrlAccessDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public TotalShortUrlAccessDataObject getTotalShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.getTotalShortUrlAccess(guid);
	}

    @Override
    public List<TotalShortUrlAccessDataObject> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedDAO.getTotalShortUrlAccesses(guids);
    }

    @Override
    public List<TotalShortUrlAccessDataObject> getAllTotalShortUrlAccesses() throws BaseException
	{
	    return getAllTotalShortUrlAccesses(null, null, null);
    }


    @Override
    public List<TotalShortUrlAccessDataObject> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccessDataObject> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<TotalShortUrlAccessDataObject> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<TotalShortUrlAccessDataObject> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<TotalShortUrlAccessDataObject> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<TotalShortUrlAccessDataObject> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccessDataObject totalShortUrlAccess) throws BaseException
    {
        return decoratedDAO.createTotalShortUrlAccess( totalShortUrlAccess);
    }

    @Override
	public Boolean updateTotalShortUrlAccess(TotalShortUrlAccessDataObject totalShortUrlAccess) throws BaseException
	{
        return decoratedDAO.updateTotalShortUrlAccess(totalShortUrlAccess);
	}
	
    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccessDataObject totalShortUrlAccess) throws BaseException
    {
        return decoratedDAO.deleteTotalShortUrlAccess(totalShortUrlAccess);
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.deleteTotalShortUrlAccess(guid);
	}

    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteTotalShortUrlAccesses(filter, params, values);
    }

}
