package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.data.HourlyShortUrlAccessDataObject;

public class HourlyShortUrlAccessBean extends ShortUrlAccessBean implements HourlyShortUrlAccess
{
    private static final Logger log = Logger.getLogger(HourlyShortUrlAccessBean.class.getName());

    // Embedded data object.
    private HourlyShortUrlAccessDataObject dobj = null;

    public HourlyShortUrlAccessBean()
    {
        this(new HourlyShortUrlAccessDataObject());
    }
    public HourlyShortUrlAccessBean(String guid)
    {
        this(new HourlyShortUrlAccessDataObject(guid));
    }
    public HourlyShortUrlAccessBean(HourlyShortUrlAccessDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public HourlyShortUrlAccessDataObject getDataObject()
    {
        return this.dobj;
    }

    public Integer getYear()
    {
        if(getDataObject() != null) {
            return getDataObject().getYear();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HourlyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setYear(Integer year)
    {
        if(getDataObject() != null) {
            getDataObject().setYear(year);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HourlyShortUrlAccessDataObject is null!");
        }
    }

    public Integer getDay()
    {
        if(getDataObject() != null) {
            return getDataObject().getDay();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HourlyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setDay(Integer day)
    {
        if(getDataObject() != null) {
            getDataObject().setDay(day);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HourlyShortUrlAccessDataObject is null!");
        }
    }

    public Integer getHour()
    {
        if(getDataObject() != null) {
            return getDataObject().getHour();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HourlyShortUrlAccessDataObject is null!");
            return null;   // ???
        }
    }
    public void setHour(Integer hour)
    {
        if(getDataObject() != null) {
            getDataObject().setHour(hour);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded HourlyShortUrlAccessDataObject is null!");
        }
    }


    // TBD
    public HourlyShortUrlAccessDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
