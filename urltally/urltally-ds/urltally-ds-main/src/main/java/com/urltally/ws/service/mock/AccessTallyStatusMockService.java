package com.urltally.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.bean.AccessTallyStatusBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.AccessTallyStatusDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.AccessTallyStatusService;


// AccessTallyStatusMockService is a decorator.
// It can be used as a base class to mock AccessTallyStatusService objects.
public abstract class AccessTallyStatusMockService implements AccessTallyStatusService
{
    private static final Logger log = Logger.getLogger(AccessTallyStatusMockService.class.getName());

    // AccessTallyStatusMockService uses the decorator design pattern.
    private AccessTallyStatusService decoratedService;

    public AccessTallyStatusMockService(AccessTallyStatusService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected AccessTallyStatusService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(AccessTallyStatusService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // AccessTallyStatus related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAccessTallyStatus(): guid = " + guid);
        AccessTallyStatus bean = decoratedService.getAccessTallyStatus(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getAccessTallyStatus(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getAccessTallyStatus(guid, field);
        return obj;
    }

    @Override
    public List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        log.fine("getAccessTallyStatuses()");
        List<AccessTallyStatus> accessTallyStatuses = decoratedService.getAccessTallyStatuses(guids);
        log.finer("END");
        return accessTallyStatuses;
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException
    {
        return getAllAccessTallyStatuses(null, null, null);
    }


    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyStatuses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<AccessTallyStatus> accessTallyStatuses = decoratedService.getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return accessTallyStatuses;
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyStatusKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusMockService.findAccessTallyStatuses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<AccessTallyStatus> accessTallyStatuses = decoratedService.findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return accessTallyStatuses;
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusMockService.findAccessTallyStatusKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        return decoratedService.createAccessTallyStatus(remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
    }

    @Override
    public String createAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createAccessTallyStatus(accessTallyStatus);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        return decoratedService.updateAccessTallyStatus(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
    }
        
    @Override
    public Boolean updateAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateAccessTallyStatus(accessTallyStatus);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteAccessTallyStatus(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteAccessTallyStatus(accessTallyStatus);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteAccessTallyStatuses(filter, params, values);
        return count;
    }

}
