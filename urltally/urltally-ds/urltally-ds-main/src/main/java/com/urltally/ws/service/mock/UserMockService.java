package com.urltally.ws.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.GeoPointStruct;
import com.urltally.ws.StreetAddressStruct;
import com.urltally.ws.GaeAppStruct;
import com.urltally.ws.FullNameStruct;
import com.urltally.ws.GaeUserStruct;
import com.urltally.ws.User;
import com.urltally.ws.bean.GeoPointStructBean;
import com.urltally.ws.bean.StreetAddressStructBean;
import com.urltally.ws.bean.GaeAppStructBean;
import com.urltally.ws.bean.FullNameStructBean;
import com.urltally.ws.bean.GaeUserStructBean;
import com.urltally.ws.bean.UserBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.GeoPointStructDataObject;
import com.urltally.ws.data.StreetAddressStructDataObject;
import com.urltally.ws.data.GaeAppStructDataObject;
import com.urltally.ws.data.FullNameStructDataObject;
import com.urltally.ws.data.GaeUserStructDataObject;
import com.urltally.ws.data.UserDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.UserService;


// UserMockService is a decorator.
// It can be used as a base class to mock UserService objects.
public abstract class UserMockService implements UserService
{
    private static final Logger log = Logger.getLogger(UserMockService.class.getName());

    // UserMockService uses the decorator design pattern.
    private UserService decoratedService;

    public UserMockService(UserService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected UserService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(UserService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // User related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public User getUser(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getUser(): guid = " + guid);
        User bean = decoratedService.getUser(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getUser(guid, field);
        return obj;
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        log.fine("getUsers()");
        List<User> users = decoratedService.getUsers(guids);
        log.finer("END");
        return users;
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }


    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUsers(ordering, offset, count, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUsers(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<User> users = decoratedService.getAllUsers(ordering, offset, count, forwardCursor);
        log.finer("END");
        return users;
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllUserKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllUserKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllUserKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUsers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserMockService.findUsers(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<User> users = decoratedService.findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return users;
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserMockService.findUserKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("UserMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return decoratedService.createUser(managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createUser(user);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return decoratedService.updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }
        
    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateUser(user);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteUser(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteUser(user);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteUsers(filter, params, values);
        return count;
    }

}
