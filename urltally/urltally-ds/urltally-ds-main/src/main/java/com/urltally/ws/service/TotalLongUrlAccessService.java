package com.urltally.ws.service;

import java.util.List;

import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface TotalLongUrlAccessService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    TotalLongUrlAccess getTotalLongUrlAccess(String guid) throws BaseException;
    Object getTotalLongUrlAccess(String guid, String field) throws BaseException;
    List<TotalLongUrlAccess> getTotalLongUrlAccesses(List<String> guids) throws BaseException;
    List<TotalLongUrlAccess> getAllTotalLongUrlAccesses() throws BaseException;
    /* @Deprecated */ List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createTotalLongUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException;
    //String createTotalLongUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TotalLongUrlAccess?)
    String createTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException;          // Returns Guid.  (Return TotalLongUrlAccess?)
    Boolean updateTotalLongUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException;
    //Boolean updateTotalLongUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException;
    Boolean deleteTotalLongUrlAccess(String guid) throws BaseException;
    Boolean deleteTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException;
    Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws BaseException;

//    Integer createTotalLongUrlAccesses(List<TotalLongUrlAccess> totalLongUrlAccesses) throws BaseException;
//    Boolean updateeTotalLongUrlAccesses(List<TotalLongUrlAccess> totalLongUrlAccesses) throws BaseException;

}
