package com.urltally.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.data.AccessTallyMasterDataObject;

public class AccessTallyMasterBean extends BeanBase implements AccessTallyMaster
{
    private static final Logger log = Logger.getLogger(AccessTallyMasterBean.class.getName());

    // Embedded data object.
    private AccessTallyMasterDataObject dobj = null;

    public AccessTallyMasterBean()
    {
        this(new AccessTallyMasterDataObject());
    }
    public AccessTallyMasterBean(String guid)
    {
        this(new AccessTallyMasterDataObject(guid));
    }
    public AccessTallyMasterBean(AccessTallyMasterDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public AccessTallyMasterDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
        }
    }

    public String getTallyType()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyType(String tallyType)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyType(tallyType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
        }
    }

    public String getTallyTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyTime(String tallyTime)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyTime(tallyTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
        }
    }

    public Long getTallyEpoch()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyEpoch();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyEpoch(tallyEpoch);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
        }
    }

    public String getTallyStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getTallyStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
            return null;   // ???
        }
    }
    public void setTallyStatus(String tallyStatus)
    {
        if(getDataObject() != null) {
            getDataObject().setTallyStatus(tallyStatus);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
        }
    }

    public Integer getAccessRecordCount()
    {
        if(getDataObject() != null) {
            return getDataObject().getAccessRecordCount();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
            return null;   // ???
        }
    }
    public void setAccessRecordCount(Integer accessRecordCount)
    {
        if(getDataObject() != null) {
            getDataObject().setAccessRecordCount(accessRecordCount);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
        }
    }

    public Long getProcesingStartedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getProcesingStartedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
            return null;   // ???
        }
    }
    public void setProcesingStartedTime(Long procesingStartedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setProcesingStartedTime(procesingStartedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AccessTallyMasterDataObject is null!");
        }
    }


    // TBD
    public AccessTallyMasterDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
