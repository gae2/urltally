package com.urltally.ws;



public interface HourlyShortUrlAccess extends ShortUrlAccess
{
    Integer  getYear();
    Integer  getDay();
    Integer  getHour();
}
