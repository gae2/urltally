package com.urltally.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.ws.util.CommonUtil;
import com.urltally.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class TotalLongUrlAccessDataObject extends KeyedDataObject implements TotalLongUrlAccess
{
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(TotalLongUrlAccessDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(TotalLongUrlAccessDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String tallyType;

    @Persistent(defaultFetchGroup = "true")
    private String tallyTime;

    @Persistent(defaultFetchGroup = "true")
    private Long tallyEpoch;

    @Persistent(defaultFetchGroup = "true")
    private Integer count;

    @Persistent(defaultFetchGroup = "true")
    private String longUrl;

    @Persistent(defaultFetchGroup = "true")
    private String longUrlDomain;

    public TotalLongUrlAccessDataObject()
    {
        this(null);
    }
    public TotalLongUrlAccessDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public TotalLongUrlAccessDataObject(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain)
    {
        this(guid, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain, null, null);
    }
    public TotalLongUrlAccessDataObject(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.tallyType = tallyType;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.count = count;
        this.longUrl = longUrl;
        this.longUrlDomain = longUrlDomain;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return TotalLongUrlAccessDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return TotalLongUrlAccessDataObject.composeKey(getGuid());
    }

    public String getTallyType()
    {
        return this.tallyType;
    }
    public void setTallyType(String tallyType)
    {
        this.tallyType = tallyType;
    }

    public String getTallyTime()
    {
        return this.tallyTime;
    }
    public void setTallyTime(String tallyTime)
    {
        this.tallyTime = tallyTime;
    }

    public Long getTallyEpoch()
    {
        return this.tallyEpoch;
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        this.tallyEpoch = tallyEpoch;
    }

    public Integer getCount()
    {
        return this.count;
    }
    public void setCount(Integer count)
    {
        this.count = count;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getLongUrlDomain()
    {
        return this.longUrlDomain;
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        this.longUrlDomain = longUrlDomain;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("tallyType", this.tallyType);
        dataMap.put("tallyTime", this.tallyTime);
        dataMap.put("tallyEpoch", this.tallyEpoch);
        dataMap.put("count", this.count);
        dataMap.put("longUrl", this.longUrl);
        dataMap.put("longUrlDomain", this.longUrlDomain);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        TotalLongUrlAccess thatObj = (TotalLongUrlAccess) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.tallyType == null && thatObj.getTallyType() != null)
            || (this.tallyType != null && thatObj.getTallyType() == null)
            || !this.tallyType.equals(thatObj.getTallyType()) ) {
            return false;
        }
        if( (this.tallyTime == null && thatObj.getTallyTime() != null)
            || (this.tallyTime != null && thatObj.getTallyTime() == null)
            || !this.tallyTime.equals(thatObj.getTallyTime()) ) {
            return false;
        }
        if( (this.tallyEpoch == null && thatObj.getTallyEpoch() != null)
            || (this.tallyEpoch != null && thatObj.getTallyEpoch() == null)
            || !this.tallyEpoch.equals(thatObj.getTallyEpoch()) ) {
            return false;
        }
        if( (this.count == null && thatObj.getCount() != null)
            || (this.count != null && thatObj.getCount() == null)
            || !this.count.equals(thatObj.getCount()) ) {
            return false;
        }
        if( (this.longUrl == null && thatObj.getLongUrl() != null)
            || (this.longUrl != null && thatObj.getLongUrl() == null)
            || !this.longUrl.equals(thatObj.getLongUrl()) ) {
            return false;
        }
        if( (this.longUrlDomain == null && thatObj.getLongUrlDomain() != null)
            || (this.longUrlDomain != null && thatObj.getLongUrlDomain() == null)
            || !this.longUrlDomain.equals(thatObj.getLongUrlDomain()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyType == null ? 0 : tallyType.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyTime == null ? 0 : tallyTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = tallyEpoch == null ? 0 : tallyEpoch.hashCode();
        _hash = 31 * _hash + delta;
        delta = count == null ? 0 : count.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrl == null ? 0 : longUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = longUrlDomain == null ? 0 : longUrlDomain.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
