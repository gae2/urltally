package com.urltally.ws;



public interface WeeklyShortUrlAccess extends ShortUrlAccess
{
    Integer  getYear();
    Integer  getWeek();
}
