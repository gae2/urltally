package com.urltally.ws.dao;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.data.AccessTallyMasterDataObject;


// TBD: Add offset/count to getAllAccessTallyMasters() and findAccessTallyMasters(), etc.
public interface AccessTallyMasterDAO
{
    AccessTallyMasterDataObject getAccessTallyMaster(String guid) throws BaseException;
    List<AccessTallyMasterDataObject> getAccessTallyMasters(List<String> guids) throws BaseException;
    List<AccessTallyMasterDataObject> getAllAccessTallyMasters() throws BaseException;
    /* @Deprecated */ List<AccessTallyMasterDataObject> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException;
    List<AccessTallyMasterDataObject> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<AccessTallyMasterDataObject> findAccessTallyMasters(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<AccessTallyMasterDataObject> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<AccessTallyMasterDataObject> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<AccessTallyMasterDataObject> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createAccessTallyMaster(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AccessTallyMasterDataObject?)
    String createAccessTallyMaster(AccessTallyMasterDataObject accessTallyMaster) throws BaseException;          // Returns Guid.  (Return AccessTallyMasterDataObject?)
    //Boolean updateAccessTallyMaster(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAccessTallyMaster(AccessTallyMasterDataObject accessTallyMaster) throws BaseException;
    Boolean deleteAccessTallyMaster(String guid) throws BaseException;
    Boolean deleteAccessTallyMaster(AccessTallyMasterDataObject accessTallyMaster) throws BaseException;
    Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException;
}
