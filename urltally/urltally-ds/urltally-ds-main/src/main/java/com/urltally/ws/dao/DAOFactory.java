package com.urltally.ws.dao;

// Abstract factory.
public abstract class DAOFactory
{
    public abstract ApiConsumerDAO getApiConsumerDAO();
    public abstract UserDAO getUserDAO();
    public abstract AccessTallyMasterDAO getAccessTallyMasterDAO();
    public abstract AccessTallyStatusDAO getAccessTallyStatusDAO();
    public abstract MonthlyShortUrlAccessDAO getMonthlyShortUrlAccessDAO();
    public abstract WeeklyShortUrlAccessDAO getWeeklyShortUrlAccessDAO();
    public abstract DailyShortUrlAccessDAO getDailyShortUrlAccessDAO();
    public abstract HourlyShortUrlAccessDAO getHourlyShortUrlAccessDAO();
    public abstract CumulativeShortUrlAccessDAO getCumulativeShortUrlAccessDAO();
    public abstract CurrentShortUrlAccessDAO getCurrentShortUrlAccessDAO();
    public abstract TotalShortUrlAccessDAO getTotalShortUrlAccessDAO();
    public abstract TotalLongUrlAccessDAO getTotalLongUrlAccessDAO();
    public abstract ServiceInfoDAO getServiceInfoDAO();
    public abstract FiveTenDAO getFiveTenDAO();
}
