package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.AccessTallyStatusDAO;
import com.urltally.ws.data.AccessTallyStatusDataObject;


// MockAccessTallyStatusDAO is a decorator.
// It can be used as a base class to mock AccessTallyStatusDAO objects.
public abstract class MockAccessTallyStatusDAO implements AccessTallyStatusDAO
{
    private static final Logger log = Logger.getLogger(MockAccessTallyStatusDAO.class.getName()); 

    // MockAccessTallyStatusDAO uses the decorator design pattern.
    private AccessTallyStatusDAO decoratedDAO;

    public MockAccessTallyStatusDAO(AccessTallyStatusDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected AccessTallyStatusDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(AccessTallyStatusDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public AccessTallyStatusDataObject getAccessTallyStatus(String guid) throws BaseException
    {
        return decoratedDAO.getAccessTallyStatus(guid);
	}

    @Override
    public List<AccessTallyStatusDataObject> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        return decoratedDAO.getAccessTallyStatuses(guids);
    }

    @Override
    public List<AccessTallyStatusDataObject> getAllAccessTallyStatuses() throws BaseException
	{
	    return getAllAccessTallyStatuses(null, null, null);
    }


    @Override
    public List<AccessTallyStatusDataObject> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyStatusDataObject> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<AccessTallyStatusDataObject> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findAccessTallyStatuses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<AccessTallyStatusDataObject> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<AccessTallyStatusDataObject> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<AccessTallyStatusDataObject> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createAccessTallyStatus(AccessTallyStatusDataObject accessTallyStatus) throws BaseException
    {
        return decoratedDAO.createAccessTallyStatus( accessTallyStatus);
    }

    @Override
	public Boolean updateAccessTallyStatus(AccessTallyStatusDataObject accessTallyStatus) throws BaseException
	{
        return decoratedDAO.updateAccessTallyStatus(accessTallyStatus);
	}
	
    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatusDataObject accessTallyStatus) throws BaseException
    {
        return decoratedDAO.deleteAccessTallyStatus(accessTallyStatus);
    }

    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        return decoratedDAO.deleteAccessTallyStatus(guid);
	}

    @Override
    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteAccessTallyStatuses(filter, params, values);
    }

}
