package com.urltally.ws.dao.base;

import java.util.logging.Logger;

import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.dao.ApiConsumerDAO;
import com.urltally.ws.dao.UserDAO;
import com.urltally.ws.dao.AccessTallyMasterDAO;
import com.urltally.ws.dao.AccessTallyStatusDAO;
import com.urltally.ws.dao.MonthlyShortUrlAccessDAO;
import com.urltally.ws.dao.WeeklyShortUrlAccessDAO;
import com.urltally.ws.dao.DailyShortUrlAccessDAO;
import com.urltally.ws.dao.HourlyShortUrlAccessDAO;
import com.urltally.ws.dao.CumulativeShortUrlAccessDAO;
import com.urltally.ws.dao.CurrentShortUrlAccessDAO;
import com.urltally.ws.dao.TotalShortUrlAccessDAO;
import com.urltally.ws.dao.TotalLongUrlAccessDAO;
import com.urltally.ws.dao.ServiceInfoDAO;
import com.urltally.ws.dao.FiveTenDAO;

// Default DAO factory uses JDO on Google AppEngine.
public class DefaultDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(DefaultDAOFactory.class.getName());

    // Ctor. Prevents instantiation.
    private DefaultDAOFactory()
    {
        // ...
    }

    // Initialization-on-demand holder.
    private static class DefaultDAOFactoryHolder
    {
        private static final DefaultDAOFactory INSTANCE = new DefaultDAOFactory();
    }

    // Singleton method
    public static DefaultDAOFactory getInstance()
    {
        return DefaultDAOFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerDAO getApiConsumerDAO()
    {
        return new DefaultApiConsumerDAO();
    }

    @Override
    public UserDAO getUserDAO()
    {
        return new DefaultUserDAO();
    }

    @Override
    public AccessTallyMasterDAO getAccessTallyMasterDAO()
    {
        return new DefaultAccessTallyMasterDAO();
    }

    @Override
    public AccessTallyStatusDAO getAccessTallyStatusDAO()
    {
        return new DefaultAccessTallyStatusDAO();
    }

    @Override
    public MonthlyShortUrlAccessDAO getMonthlyShortUrlAccessDAO()
    {
        return new DefaultMonthlyShortUrlAccessDAO();
    }

    @Override
    public WeeklyShortUrlAccessDAO getWeeklyShortUrlAccessDAO()
    {
        return new DefaultWeeklyShortUrlAccessDAO();
    }

    @Override
    public DailyShortUrlAccessDAO getDailyShortUrlAccessDAO()
    {
        return new DefaultDailyShortUrlAccessDAO();
    }

    @Override
    public HourlyShortUrlAccessDAO getHourlyShortUrlAccessDAO()
    {
        return new DefaultHourlyShortUrlAccessDAO();
    }

    @Override
    public CumulativeShortUrlAccessDAO getCumulativeShortUrlAccessDAO()
    {
        return new DefaultCumulativeShortUrlAccessDAO();
    }

    @Override
    public CurrentShortUrlAccessDAO getCurrentShortUrlAccessDAO()
    {
        return new DefaultCurrentShortUrlAccessDAO();
    }

    @Override
    public TotalShortUrlAccessDAO getTotalShortUrlAccessDAO()
    {
        return new DefaultTotalShortUrlAccessDAO();
    }

    @Override
    public TotalLongUrlAccessDAO getTotalLongUrlAccessDAO()
    {
        return new DefaultTotalLongUrlAccessDAO();
    }

    @Override
    public ServiceInfoDAO getServiceInfoDAO()
    {
        return new DefaultServiceInfoDAO();
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new DefaultFiveTenDAO();
    }

}
