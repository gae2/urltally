package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "accessTallyMasters")
@XmlType(propOrder = {"accessTallyMaster", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessTallyMasterListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AccessTallyMasterListStub.class.getName());

    private List<AccessTallyMasterStub> accessTallyMasters = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public AccessTallyMasterListStub()
    {
        this(new ArrayList<AccessTallyMasterStub>());
    }
    public AccessTallyMasterListStub(List<AccessTallyMasterStub> accessTallyMasters)
    {
        this(accessTallyMasters, null);
    }
    public AccessTallyMasterListStub(List<AccessTallyMasterStub> accessTallyMasters, String forwardCursor)
    {
        this.accessTallyMasters = accessTallyMasters;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(accessTallyMasters == null) {
            return true;
        } else {
            return accessTallyMasters.isEmpty();
        }
    }
    public int getSize()
    {
        if(accessTallyMasters == null) {
            return 0;
        } else {
            return accessTallyMasters.size();
        }
    }


    @XmlElement(name = "accessTallyMaster")
    public List<AccessTallyMasterStub> getAccessTallyMaster()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AccessTallyMasterStub> getList()
    {
        return accessTallyMasters;
    }
    public void setList(List<AccessTallyMasterStub> accessTallyMasters)
    {
        this.accessTallyMasters = accessTallyMasters;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<AccessTallyMasterStub> it = this.accessTallyMasters.iterator();
        while(it.hasNext()) {
            AccessTallyMasterStub accessTallyMaster = it.next();
            sb.append(accessTallyMaster.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AccessTallyMasterListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AccessTallyMasterListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AccessTallyMasterListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AccessTallyMasterListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AccessTallyMasterListStub object as a string.", e);
        }
        
        return null;
    }
    public static AccessTallyMasterListStub fromJsonString(String jsonStr)
    {
        try {
            AccessTallyMasterListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AccessTallyMasterListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AccessTallyMasterListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AccessTallyMasterListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AccessTallyMasterListStub object.", e);
        }
        
        return null;
    }

}
