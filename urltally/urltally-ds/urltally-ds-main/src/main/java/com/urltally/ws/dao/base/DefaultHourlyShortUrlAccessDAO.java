package com.urltally.ws.dao.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.HourlyShortUrlAccessDAO;
import com.urltally.ws.data.HourlyShortUrlAccessDataObject;


public class DefaultHourlyShortUrlAccessDAO extends DefaultDAOBase implements HourlyShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(DefaultHourlyShortUrlAccessDAO.class.getName()); 

    // Returns the hourlyShortUrlAccess for the given guid/key.
    // Returns null if none is found in the datastore.
    @Override
    public HourlyShortUrlAccessDataObject getHourlyShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: guid = " + guid);

        HourlyShortUrlAccessDataObject hourlyShortUrlAccess = null;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            //Transaction tx = pm.currentTransaction();
            try {
                //tx.setOptimistic(true);
                //tx.begin();
                //Key key = HourlyShortUrlAccessDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = HourlyShortUrlAccessDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                hourlyShortUrlAccess = pm.getObjectById(HourlyShortUrlAccessDataObject.class, key);
                //tx.commit();
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve hourlyShortUrlAccess for guid = " + guid, ex);
                throw new ResourceNotFoundException(ex);
            } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        log.finer("END");
	    return hourlyShortUrlAccess;
	}

    @Override
    public List<HourlyShortUrlAccessDataObject> getHourlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // temporary implementation
        List<HourlyShortUrlAccessDataObject> hourlyShortUrlAccesses = null;
        if(guids != null && !guids.isEmpty()) {
            hourlyShortUrlAccesses = new ArrayList<HourlyShortUrlAccessDataObject>();
            for(String guid : guids) {
                HourlyShortUrlAccessDataObject obj = getHourlyShortUrlAccess(guid); 
                hourlyShortUrlAccesses.add(obj);
            }
	    }

        log.finer("END");
        return hourlyShortUrlAccesses;
    }

    @Override
    public List<HourlyShortUrlAccessDataObject> getAllHourlyShortUrlAccesses() throws BaseException
	{
	    return getAllHourlyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<HourlyShortUrlAccessDataObject> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllHourlyShortUrlAccesses(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<HourlyShortUrlAccessDataObject> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<HourlyShortUrlAccessDataObject> hourlyShortUrlAccesses = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(HourlyShortUrlAccessDataObject.class);
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            hourlyShortUrlAccesses = (List<HourlyShortUrlAccessDataObject>) q.execute();
            if(hourlyShortUrlAccesses != null) {
                hourlyShortUrlAccesses.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(hourlyShortUrlAccesses);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            /*
            // ???
            Collection<HourlyShortUrlAccessDataObject> rs_hourlyShortUrlAccesses = (Collection<HourlyShortUrlAccessDataObject>) q.execute();
            if(rs_hourlyShortUrlAccesses == null) {
                log.log(Level.WARNING, "Failed to retrieve all hourlyShortUrlAccesses.");
                hourlyShortUrlAccesses = new ArrayList<HourlyShortUrlAccessDataObject>();  // ???           
            } else {
                hourlyShortUrlAccesses = new ArrayList<HourlyShortUrlAccessDataObject>(pm.detachCopyAll(rs_hourlyShortUrlAccesses));
            }
            */
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all hourlyShortUrlAccesses.", ex);
            //hourlyShortUrlAccesses = new ArrayList<HourlyShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to retrieve all hourlyShortUrlAccesses.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return hourlyShortUrlAccesses;
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllHourlyShortUrlAccessKeys(ordering, offset, count, null);
    }

	@SuppressWarnings("unchecked")
    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

    	List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + HourlyShortUrlAccessDataObject.class.getName());  // ???
            if(ordering != null) {
                q.setOrdering(ordering);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            keys = (List<String>) q.execute();
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to retrieve all HourlyShortUrlAccess keys.", ex);
            throw new DataStoreException("Failed to retrieve all HourlyShortUrlAccess keys.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return keys;
    }

    @Override
	public List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<HourlyShortUrlAccessDataObject> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        if(log.isLoggable(Level.INFO)) log.info("DefaultHourlyShortUrlAccessDAO.findHourlyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("findHourlyShortUrlAccesses() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        List<HourlyShortUrlAccessDataObject> hourlyShortUrlAccesses = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(HourlyShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                hourlyShortUrlAccesses = (List<HourlyShortUrlAccessDataObject>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                hourlyShortUrlAccesses = (List<HourlyShortUrlAccessDataObject>) q.execute();
            }
            if(hourlyShortUrlAccesses != null) {
                hourlyShortUrlAccesses.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(hourlyShortUrlAccesses);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //while($TypeUtil.deCapitalizePluralize($typeName).iterator().hasNext()) {
            //    for(HourlyShortUrlAccessDataObject dobj : hourlyShortUrlAccesses) {
            //        //
            //    }
            //}
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find hourlyShortUrlAccesses because index is missing.", ex);
            //hourlyShortUrlAccesses = new ArrayList<HourlyShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to find hourlyShortUrlAccesses because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find hourlyShortUrlAccesses meeting the criterion.", ex);
            //hourlyShortUrlAccesses = new ArrayList<HourlyShortUrlAccessDataObject>();  // ???
            throw new DataStoreException("Failed to find hourlyShortUrlAccesses meeting the criterion.", ex);
        } finally {
                try {
                    //if(tx.isActive()) {
                    //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                    //    tx.rollback();
                    //}
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
        }

        log.finer("END");
        return hourlyShortUrlAccesses;
	}

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultHourlyShortUrlAccessDAO.findHourlyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = null;
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery("select key from " + HourlyShortUrlAccessDataObject.class.getName());  // ???
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(ordering != null && !ordering.isEmpty()) {
                q.setOrdering(ordering);
            }
            if(grouping != null && !grouping.isEmpty()) {
                q.setGrouping(grouping);
            }
            if(unique != null) {
                q.setUnique(unique);
            }

            long fromIncl;
            long toExcl;
            if(count == null || count <= 0) {
                // Maybe, just ignore range ???
                count = MAX_FETCH_COUNT;
            }
            Cursor cursor = null;
            if(forwardCursor != null && !forwardCursor.isEmpty()) {
                cursor = Cursor.fromWebSafeString(forwardCursor.getWebSafeString());
            }
            if(cursor != null) {
                Map<String, Object> extensionMap = new HashMap<String, Object>();
                extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
                q.setExtensions(extensionMap);
                fromIncl = 0L;
                toExcl = count;
            } else {
                if(offset == null || offset < 0L) {
                    offset = 0L;
                }
                fromIncl = offset;
                toExcl = offset + count;
            }
            q.setRange(fromIncl, toExcl);

            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                keys = (List<String>) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                keys = (List<String>) q.execute();
            }
            if(keys != null) {
                keys.size();   // ???? Without this, DataNucleus throws "Object Manager has been closed" exception.

                // Do this only if the in-out forwardCursor param is not null.
                if(forwardCursor != null) {
                    cursor = JDOCursorHelper.getCursor(keys);
                    String webSafeString = null;
                    if(cursor != null) {
                        webSafeString = cursor.toWebSafeString();
                    }
                    forwardCursor.setWebSafeString(webSafeString);
                }
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to find HourlyShortUrlAccess keys because index is missing.", ex);
            throw new DataStoreException("Failed to find HourlyShortUrlAccess keys because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to find HourlyShortUrlAccess keys meeting the criterion.", ex);
            throw new DataStoreException("Failed to find HourlyShortUrlAccess keys meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        log.finer("END");
        return keys;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultHourlyShortUrlAccessDAO.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        //if(filter == null || filter.isEmpty()) {
        //    throw new DataStoreException("getCount() without filter is not supported for performance reasons");
        //}
        // TBD: Parameter validation...

        Long count = 0L; 
        PersistenceManager pm = getPersistenceManager();
        //Transaction tx = pm.currentTransaction();
        try {
            //tx.setOptimistic(true);
            //tx.begin();
            Query q = pm.newQuery(HourlyShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(aggregate == null || aggregate.isEmpty()) {
                aggregate = "count(this)";  // By default, count. Other examples: "max(this.createdTime)", avg, sum, etc. (Note: These are currently not supported by Google App Engine.)
            }
            q.setResult(aggregate);
            //q.setResultClass(Long.class);
            // For now, we only support a single aggregate function. (Eg, no comma-separated list.)
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.executeWithArray(values.toArray(new Object[0]));
            } else {
                count = (Long) q.execute();
            }
            //tx.commit();
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to get hourlyShortUrlAccess count because index is missing.", ex);
            throw new DataStoreException("Failed to get hourlyShortUrlAccess count because index is missing.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to get hourlyShortUrlAccess count meeting the criterion.", ex);
            throw new DataStoreException("Failed to get hourlyShortUrlAccess count meeting the criterion.", ex);
        } finally {
            try {
                //if(tx.isActive()) {
                //    log.warning("TRANSACTION IS STILL ACTIVE. ROLLING BACK...");
                //    tx.rollback();
                //}
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(count == null) {
            count = 0L;
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

	// Stores the hourlyShortUrlAccess in the datastore and returns its guid.
	// Returns null if the operation fails.
    private String storeHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException
    {
        log.fine("storeHourlyShortUrlAccess() called.");

        String guid = null;
        PersistenceManager pm = getPersistenceManager();
        try {
            // TBD: If the object on the datastore has non-null created date and the arg shortLink has no created date timestamp,
            //      do not overwrite the timestamp. How to do that???
            // For now, this is ok since we use "overwrite/replace" semantics (not partial update).
            Long createdTime = hourlyShortUrlAccess.getCreatedTime();
            if(createdTime == null) {
                createdTime = System.currentTimeMillis();
                hourlyShortUrlAccess.setCreatedTime(createdTime);
            }
            Long modifiedTime = hourlyShortUrlAccess.getModifiedTime();
            if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
                hourlyShortUrlAccess.setModifiedTime(createdTime);
            }
            pm.makePersistent(hourlyShortUrlAccess); 
            // TBD: How do you know the makePersistent() call was successful???
            guid = hourlyShortUrlAccess.getGuid();
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to store hourlyShortUrlAccess because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to store hourlyShortUrlAccess because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to store hourlyShortUrlAccess.", ex);
            throw new DataStoreException("Failed to store hourlyShortUrlAccess.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINE)) log.fine("storeHourlyShortUrlAccess(): guid = " + guid);
        return guid;
    }

    @Override
    public String createHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException
    {
        // The createdTime field will be automatically set in storeHourlyShortUrlAccess().
        //Long createdTime = hourlyShortUrlAccess.getCreatedTime();
        //if(createdTime == null) {
        //    createdTime = System.currentTimeMillis();
        //    hourlyShortUrlAccess.setCreatedTime(createdTime);
        //}
        //Long modifiedTime = hourlyShortUrlAccess.getModifiedTime();
        //if(modifiedTime != null && modifiedTime < createdTime) {  // This cannot happen.
        //    hourlyShortUrlAccess.setModifiedTime(createdTime);
        //}
        return storeHourlyShortUrlAccess(hourlyShortUrlAccess);
    }

    @Override
	public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException
	{
	    // Modify the modifiedTime timestamp.
	    // CreatedTime should, normally, be non-null.
	    // If null, however, it will be automatically set in storeHourlyShortUrlAccess()
	    // (in which case modifiedTime might be updated again).
	    hourlyShortUrlAccess.setModifiedTime(System.currentTimeMillis());
	    String guid = storeHourlyShortUrlAccess(hourlyShortUrlAccess);
	    if(guid != null) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
    @Override
    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccessDataObject hourlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        PersistenceManager pm = getPersistenceManager();
        try {
            pm.deletePersistent(hourlyShortUrlAccess);
            // TBD: How do you know the deletePersistent() call was successful???
            suc = true;
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to delete hourlyShortUrlAccess because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete hourlyShortUrlAccess because the datastore is currently read-only.", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete hourlyShortUrlAccess.", ex);
            throw new DataStoreException("Failed to delete hourlyShortUrlAccess.", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        boolean suc = false;
        if(guid != null) {
            PersistenceManager pm = getPersistenceManager();
            try {
                //Key key = HourlyShortUrlAccessDataObject.composeKey(guid);
                String key;
                if(GUID.isValid(guid)) {
                    key = HourlyShortUrlAccessDataObject.composeKey(guid);
                } else {
                    key = guid;
                }
                HourlyShortUrlAccessDataObject hourlyShortUrlAccess = pm.getObjectById(HourlyShortUrlAccessDataObject.class, key);
                pm.deletePersistent(hourlyShortUrlAccess);
                // TBD: How do you know the deletePersistent() call was successful???
                suc = true;
            } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
                // Datastore is read-only, possibly due to the Google App Engine maintenance.
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete hourlyShortUrlAccess because the datastore is currently read-only. guid = " + guid, ex);
                throw new ServiceUnavailableException("Failed to delete hourlyShortUrlAccess because the datastore is currently read-only.", ex);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete hourlyShortUrlAccess for guid = " + guid, ex);
                throw new DataStoreException("Failed to delete hourlyShortUrlAccess for guid = " + guid, ex);
            } finally {
                try {
                    pm.close(); 
                } catch(Exception ex) {
                    log.log(Level.SEVERE, "Unknown data store error.", ex);
                    throw new DataStoreException("Unknown data store error...", ex);
                }
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
	}

    @Override
    public Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.INFO)) log.info("DefaultHourlyShortUrlAccessDAO.deleteHourlyShortUrlAccesses(): filter=" + filter + "; params=" + params);

        // TBD: Delete-by-query requests should be carefully checked because of its potential harms.
        if(filter == null || filter.isEmpty()) {
            throw new RequestConflictException("deleteAll() without filter is not supported for performance/safety reasons");
        }

        Long count = 0L;
        PersistenceManager pm = getPersistenceManager();
        try {
            Query q = pm.newQuery(HourlyShortUrlAccessDataObject.class);
            if(filter != null && !filter.isEmpty()) {
                q.setFilter(filter);
            }
            if(params != null && !params.isEmpty()) {
                q.declareParameters(params);   // Comma-separated list of parameter declarations.
                count = (Long) q.deletePersistentAll(values.toArray(new Object[0]));
            } else {
                count = (Long) q.deletePersistentAll();
            }
            // TBD: How do you know the deletePersistent() call was successful???
        } catch (com.google.apphosting.api.ApiProxy.CapabilityDisabledException ex) {
            // Datastore is read-only, possibly due to the Google App Engine maintenance.
            log.log(Level.WARNING, "Failed to deletehourlyShortUrlAccesses because the datastore is currently read-only.", ex);
            throw new ServiceUnavailableException("Failed to delete hourlyShortUrlAccesses because the datastore is currently read-only.", ex);
        } catch(com.google.appengine.api.datastore.DatastoreNeedIndexException ex) {
            log.log(Level.WARNING, "Failed to delete hourlyShortUrlAccesses because index is missing", ex);
            throw new DataStoreException("Failed to delete hourlyShortUrlAccesses because index is missing", ex);
        } catch(Exception ex) {
            log.log(Level.WARNING, "Failed to delete hourlyShortUrlAccesses", ex);
            throw new DataStoreException("Failed to delete hourlyShortUrlAccesses", ex);
        } finally {
            try {
                pm.close(); 
            } catch(Exception ex) {
                log.log(Level.SEVERE, "Unknown data store error.", ex);
                throw new DataStoreException("Unknown data store error...", ex);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
