package com.urltally.ws.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ResourceAlreadyPresentException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.DataStoreRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.bean.TotalShortUrlAccessBean;
import com.urltally.ws.stub.TotalShortUrlAccessListStub;
import com.urltally.ws.stub.TotalShortUrlAccessStub;
import com.urltally.ws.resource.ServiceManager;
import com.urltally.ws.resource.TotalShortUrlAccessResource;

// MockTotalShortUrlAccessResource is a decorator.
// It can be used as a base class to mock TotalShortUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/w/totalShortUrlAccesses/")
public abstract class MockTotalShortUrlAccessResource implements TotalShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockTotalShortUrlAccessResource.class.getName());

    // MockTotalShortUrlAccessResource uses the decorator design pattern.
    private TotalShortUrlAccessResource decoratedResource;

    public MockTotalShortUrlAccessResource(TotalShortUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TotalShortUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TotalShortUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response getTotalShortUrlAccessKeys(List<String> guids) throws BaseResourceException
    {
        return decoratedResource.getTotalShortUrlAccessKeys(guids);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

    @Override
    public Response getTotalShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getTotalShortUrlAccess(guid);
    }

    @Override
    public Response getTotalShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTotalShortUrlAccess(guid, field);
    }

    @Override
    public Response createTotalShortUrlAccess(TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createTotalShortUrlAccess(totalShortUrlAccess);
    }

    @Override
    public Response updateTotalShortUrlAccess(String guid, TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateTotalShortUrlAccess(guid, totalShortUrlAccess);
    }

    @Override
    public Response updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseResourceException
    {
        return decoratedResource.updateTotalShortUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
    }

    @Override
    public Response deleteTotalShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTotalShortUrlAccess(guid);
    }

    @Override
    public Response deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTotalShortUrlAccesses(filter, params, values);
    }


}
