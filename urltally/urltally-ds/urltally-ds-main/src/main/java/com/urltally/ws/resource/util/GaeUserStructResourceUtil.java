package com.urltally.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.GUID;
import com.urltally.ws.GaeUserStruct;
import com.urltally.ws.bean.GaeUserStructBean;
import com.urltally.ws.stub.GaeUserStructStub;


public class GaeUserStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GaeUserStructResourceUtil.class.getName());

    // Static methods only.
    private GaeUserStructResourceUtil() {}

    public static GaeUserStructBean convertGaeUserStructStubToBean(GaeUserStruct stub)
    {
        GaeUserStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null GaeUserStructBean is returned.");
        } else {
            bean = new GaeUserStructBean();
            bean.setAuthDomain(stub.getAuthDomain());
            bean.setFederatedIdentity(stub.getFederatedIdentity());
            bean.setNickname(stub.getNickname());
            bean.setUserId(stub.getUserId());
            bean.setEmail(stub.getEmail());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
