package com.urltally.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.util.JsonUtil;


@XmlRootElement(name = "totalShortUrlAccesses")
@XmlType(propOrder = {"totalShortUrlAccess", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TotalShortUrlAccessListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessListStub.class.getName());

    private List<TotalShortUrlAccessStub> totalShortUrlAccesses = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public TotalShortUrlAccessListStub()
    {
        this(new ArrayList<TotalShortUrlAccessStub>());
    }
    public TotalShortUrlAccessListStub(List<TotalShortUrlAccessStub> totalShortUrlAccesses)
    {
        this(totalShortUrlAccesses, null);
    }
    public TotalShortUrlAccessListStub(List<TotalShortUrlAccessStub> totalShortUrlAccesses, String forwardCursor)
    {
        this.totalShortUrlAccesses = totalShortUrlAccesses;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(totalShortUrlAccesses == null) {
            return true;
        } else {
            return totalShortUrlAccesses.isEmpty();
        }
    }
    public int getSize()
    {
        if(totalShortUrlAccesses == null) {
            return 0;
        } else {
            return totalShortUrlAccesses.size();
        }
    }


    @XmlElement(name = "totalShortUrlAccess")
    public List<TotalShortUrlAccessStub> getTotalShortUrlAccess()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TotalShortUrlAccessStub> getList()
    {
        return totalShortUrlAccesses;
    }
    public void setList(List<TotalShortUrlAccessStub> totalShortUrlAccesses)
    {
        this.totalShortUrlAccesses = totalShortUrlAccesses;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<TotalShortUrlAccessStub> it = this.totalShortUrlAccesses.iterator();
        while(it.hasNext()) {
            TotalShortUrlAccessStub totalShortUrlAccess = it.next();
            sb.append(totalShortUrlAccess.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TotalShortUrlAccessListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TotalShortUrlAccessListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TotalShortUrlAccessListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TotalShortUrlAccessListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TotalShortUrlAccessListStub object as a string.", e);
        }
        
        return null;
    }
    public static TotalShortUrlAccessListStub fromJsonString(String jsonStr)
    {
        try {
            TotalShortUrlAccessListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TotalShortUrlAccessListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TotalShortUrlAccessListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TotalShortUrlAccessListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TotalShortUrlAccessListStub object.", e);
        }
        
        return null;
    }

}
