package com.urltally.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.bean.AccessTallyStatusBean;
import com.urltally.ws.dao.DAOFactory;
import com.urltally.ws.data.AccessTallyStatusDataObject;
import com.urltally.ws.service.DAOFactoryManager;
import com.urltally.ws.service.AccessTallyStatusService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AccessTallyStatusServiceImpl implements AccessTallyStatusService
{
    private static final Logger log = Logger.getLogger(AccessTallyStatusServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // AccessTallyStatus related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException
    {
        log.finer("BEGIN");

        AccessTallyStatusDataObject dataObj = getDAOFactory().getAccessTallyStatusDAO().getAccessTallyStatus(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AccessTallyStatusDataObject for guid = " + guid);
            return null;  // ????
        }
        AccessTallyStatusBean bean = new AccessTallyStatusBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAccessTallyStatus(String guid, String field) throws BaseException
    {
        AccessTallyStatusDataObject dataObj = getDAOFactory().getAccessTallyStatusDAO().getAccessTallyStatus(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AccessTallyStatusDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("remoteRecordGuid")) {
            return dataObj.getRemoteRecordGuid();
        } else if(field.equals("shortUrl")) {
            return dataObj.getShortUrl();
        } else if(field.equals("tallyType")) {
            return dataObj.getTallyType();
        } else if(field.equals("tallyTime")) {
            return dataObj.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return dataObj.getTallyEpoch();
        } else if(field.equals("processed")) {
            return dataObj.isProcessed();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<AccessTallyStatus> list = new ArrayList<AccessTallyStatus>();
        List<AccessTallyStatusDataObject> dataObjs = getDAOFactory().getAccessTallyStatusDAO().getAccessTallyStatuses(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatusDataObject list.");
        } else {
            Iterator<AccessTallyStatusDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AccessTallyStatusDataObject dataObj = (AccessTallyStatusDataObject) it.next();
                list.add(new AccessTallyStatusBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException
    {
        return getAllAccessTallyStatuses(null, null, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyStatuses(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<AccessTallyStatus> list = new ArrayList<AccessTallyStatus>();
        List<AccessTallyStatusDataObject> dataObjs = getDAOFactory().getAccessTallyStatusDAO().getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatusDataObject list.");
        } else {
            Iterator<AccessTallyStatusDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AccessTallyStatusDataObject dataObj = (AccessTallyStatusDataObject) it.next();
                list.add(new AccessTallyStatusBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllAccessTallyStatusKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getAccessTallyStatusDAO().getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatus key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AccessTallyStatusServiceImpl.findAccessTallyStatuses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<AccessTallyStatus> list = new ArrayList<AccessTallyStatus>();
        List<AccessTallyStatusDataObject> dataObjs = getDAOFactory().getAccessTallyStatusDAO().findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find accessTallyStatuses for the given criterion.");
        } else {
            Iterator<AccessTallyStatusDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                AccessTallyStatusDataObject dataObj = (AccessTallyStatusDataObject) it.next();
                list.add(new AccessTallyStatusBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AccessTallyStatusServiceImpl.findAccessTallyStatusKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getAccessTallyStatusDAO().findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AccessTallyStatus keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("AccessTallyStatusServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getAccessTallyStatusDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        AccessTallyStatusDataObject dataObj = new AccessTallyStatusDataObject(null, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
        return createAccessTallyStatus(dataObj);
    }

    @Override
    public String createAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyStatus cannot be null.....
        if(accessTallyStatus == null) {
            log.log(Level.INFO, "Param accessTallyStatus is null!");
            throw new BadRequestException("Param accessTallyStatus object is null!");
        }
        AccessTallyStatusDataObject dataObj = null;
        if(accessTallyStatus instanceof AccessTallyStatusDataObject) {
            dataObj = (AccessTallyStatusDataObject) accessTallyStatus;
        } else if(accessTallyStatus instanceof AccessTallyStatusBean) {
            dataObj = ((AccessTallyStatusBean) accessTallyStatus).toDataObject();
        } else {  // if(accessTallyStatus instanceof AccessTallyStatus)
            //dataObj = new AccessTallyStatusDataObject(null, accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new AccessTallyStatusDataObject(accessTallyStatus.getGuid(), accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
        }
        String guid = getDAOFactory().getAccessTallyStatusDAO().createAccessTallyStatus(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AccessTallyStatusDataObject dataObj = new AccessTallyStatusDataObject(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
        return updateAccessTallyStatus(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyStatus cannot be null.....
        if(accessTallyStatus == null || accessTallyStatus.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyStatus or its guid is null!");
            throw new BadRequestException("Param accessTallyStatus object or its guid is null!");
        }
        AccessTallyStatusDataObject dataObj = null;
        if(accessTallyStatus instanceof AccessTallyStatusDataObject) {
            dataObj = (AccessTallyStatusDataObject) accessTallyStatus;
        } else if(accessTallyStatus instanceof AccessTallyStatusBean) {
            dataObj = ((AccessTallyStatusBean) accessTallyStatus).toDataObject();
        } else {  // if(accessTallyStatus instanceof AccessTallyStatus)
            dataObj = new AccessTallyStatusDataObject(accessTallyStatus.getGuid(), accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
        }
        Boolean suc = getDAOFactory().getAccessTallyStatusDAO().updateAccessTallyStatus(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getAccessTallyStatusDAO().deleteAccessTallyStatus(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyStatus cannot be null.....
        if(accessTallyStatus == null || accessTallyStatus.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyStatus or its guid is null!");
            throw new BadRequestException("Param accessTallyStatus object or its guid is null!");
        }
        AccessTallyStatusDataObject dataObj = null;
        if(accessTallyStatus instanceof AccessTallyStatusDataObject) {
            dataObj = (AccessTallyStatusDataObject) accessTallyStatus;
        } else if(accessTallyStatus instanceof AccessTallyStatusBean) {
            dataObj = ((AccessTallyStatusBean) accessTallyStatus).toDataObject();
        } else {  // if(accessTallyStatus instanceof AccessTallyStatus)
            dataObj = new AccessTallyStatusDataObject(accessTallyStatus.getGuid(), accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
        }
        Boolean suc = getDAOFactory().getAccessTallyStatusDAO().deleteAccessTallyStatus(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getAccessTallyStatusDAO().deleteAccessTallyStatuses(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
