package com.urltally.ws.dao.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

// import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Cursor;
// TBD: Which one to use ???
import com.google.appengine.datanucleus.query.JDOCursorHelper;
// import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.config.Config;
import com.urltally.ws.exception.DataStoreException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.dao.CurrentShortUrlAccessDAO;
import com.urltally.ws.data.CurrentShortUrlAccessDataObject;


// MockCurrentShortUrlAccessDAO is a decorator.
// It can be used as a base class to mock CurrentShortUrlAccessDAO objects.
public abstract class MockCurrentShortUrlAccessDAO implements CurrentShortUrlAccessDAO
{
    private static final Logger log = Logger.getLogger(MockCurrentShortUrlAccessDAO.class.getName()); 

    // MockCurrentShortUrlAccessDAO uses the decorator design pattern.
    private CurrentShortUrlAccessDAO decoratedDAO;

    public MockCurrentShortUrlAccessDAO(CurrentShortUrlAccessDAO decoratedDAO)
    {
        this.decoratedDAO = decoratedDAO;
    }

    // To be used by subclasses
    protected CurrentShortUrlAccessDAO getDecoratedDAO()
    {
        return decoratedDAO;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedDAO(CurrentShortUrlAccessDAO decoratedDAO)
    // {
    //     this.decoratedDAO = decoratedDAO;
    // }


    @Override
    public CurrentShortUrlAccessDataObject getCurrentShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.getCurrentShortUrlAccess(guid);
	}

    @Override
    public List<CurrentShortUrlAccessDataObject> getCurrentShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedDAO.getCurrentShortUrlAccesses(guids);
    }

    @Override
    public List<CurrentShortUrlAccessDataObject> getAllCurrentShortUrlAccesses() throws BaseException
	{
	    return getAllCurrentShortUrlAccesses(null, null, null);
    }


    @Override
    public List<CurrentShortUrlAccessDataObject> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
	{
	    return getAllCurrentShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccessDataObject> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.getAllCurrentShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.getAllCurrentShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
	public List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
	{
        return findCurrentShortUrlAccesses(filter, ordering, params, values, null, null);
    }

    @Override
	public List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException
	{
        return findCurrentShortUrlAccesses(filter, ordering, params, values, null, null, offset, count);
	}

    @Override
	public List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
	{
	    return findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
	}

    @Override
	public List<CurrentShortUrlAccessDataObject> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
	{
        return decoratedDAO.findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
	}

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedDAO.findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedDAO.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException
    {
        return decoratedDAO.createCurrentShortUrlAccess( currentShortUrlAccess);
    }

    @Override
	public Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException
	{
        return decoratedDAO.updateCurrentShortUrlAccess(currentShortUrlAccess);
	}
	
    @Override
    public Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccessDataObject currentShortUrlAccess) throws BaseException
    {
        return decoratedDAO.deleteCurrentShortUrlAccess(currentShortUrlAccess);
    }

    @Override
    public Boolean deleteCurrentShortUrlAccess(String guid) throws BaseException
    {
        return decoratedDAO.deleteCurrentShortUrlAccess(guid);
	}

    @Override
    public Long deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedDAO.deleteCurrentShortUrlAccesses(filter, params, values);
    }

}
