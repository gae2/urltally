package com.urltally.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.util.CommonUtil;
import com.urltally.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class MonthlyShortUrlAccessDataObject extends ShortUrlAccessDataObject implements MonthlyShortUrlAccess
{
    private static final Logger log = Logger.getLogger(MonthlyShortUrlAccessDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(MonthlyShortUrlAccessDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(MonthlyShortUrlAccessDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private Integer year;

    @Persistent(defaultFetchGroup = "true")
    private Integer month;

    @Persistent(defaultFetchGroup = "true")
    private Integer numberOfDays;

    public MonthlyShortUrlAccessDataObject()
    {
        this(null);
    }
    public MonthlyShortUrlAccessDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public MonthlyShortUrlAccessDataObject(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays)
    {
        this(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays, null, null);
    }
    public MonthlyShortUrlAccessDataObject(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays, Long createdTime, Long modifiedTime)
    {
        super(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, createdTime, modifiedTime);
        this.year = year;
        this.month = month;
        this.numberOfDays = numberOfDays;
    }

//    @Override
//    protected Key createKey()
//    {
//        return MonthlyShortUrlAccessDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return MonthlyShortUrlAccessDataObject.composeKey(getGuid());
    }

    public Integer getYear()
    {
        return this.year;
    }
    public void setYear(Integer year)
    {
        this.year = year;
    }

    public Integer getMonth()
    {
        return this.month;
    }
    public void setMonth(Integer month)
    {
        this.month = month;
    }

    public Integer getNumberOfDays()
    {
        return this.numberOfDays;
    }
    public void setNumberOfDays(Integer numberOfDays)
    {
        this.numberOfDays = numberOfDays;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("year", this.year);
        dataMap.put("month", this.month);
        dataMap.put("numberOfDays", this.numberOfDays);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        MonthlyShortUrlAccess thatObj = (MonthlyShortUrlAccess) obj;
        if( (this.year == null && thatObj.getYear() != null)
            || (this.year != null && thatObj.getYear() == null)
            || !this.year.equals(thatObj.getYear()) ) {
            return false;
        }
        if( (this.month == null && thatObj.getMonth() != null)
            || (this.month != null && thatObj.getMonth() == null)
            || !this.month.equals(thatObj.getMonth()) ) {
            return false;
        }
        if( (this.numberOfDays == null && thatObj.getNumberOfDays() != null)
            || (this.numberOfDays != null && thatObj.getNumberOfDays() == null)
            || !this.numberOfDays.equals(thatObj.getNumberOfDays()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = year == null ? 0 : year.hashCode();
        _hash = 31 * _hash + delta;
        delta = month == null ? 0 : month.hashCode();
        _hash = 31 * _hash + delta;
        delta = numberOfDays == null ? 0 : numberOfDays.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
