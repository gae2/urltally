//////////////////////////////////////////////////////////
// <script src="/js/bean/totalshorturlaccessjsbean-1.0.js"></script>
// Last modified time: 1363421920878.
//////////////////////////////////////////////////////////

var urltally = urltally || {};
urltally.wa = urltally.wa || {};
urltally.wa.bean = urltally.wa.bean || {};
urltally.wa.bean.TotalShortUrlAccessJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var tallyType;
    var tallyTime;
    var tallyEpoch;
    var count;
    var shortUrl;
    var shortUrlDomain;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getTallyType = function() { return tallyType; };
    this.setTallyType = function(value) { tallyType = value; };
    this.getTallyTime = function() { return tallyTime; };
    this.setTallyTime = function(value) { tallyTime = value; };
    this.getTallyEpoch = function() { return tallyEpoch; };
    this.setTallyEpoch = function(value) { tallyEpoch = value; };
    this.getCount = function() { return count; };
    this.setCount = function(value) { count = value; };
    this.getShortUrl = function() { return shortUrl; };
    this.setShortUrl = function(value) { shortUrl = value; };
    this.getShortUrlDomain = function() { return shortUrlDomain; };
    this.setShortUrlDomain = function(value) { shortUrlDomain = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new urltally.wa.bean.TotalShortUrlAccessJsBean();

      o.setGuid(generateUuid());
      if(tallyType !== undefined && tallyType != null) {
        o.setTallyType(tallyType);
      }
      if(tallyTime !== undefined && tallyTime != null) {
        o.setTallyTime(tallyTime);
      }
      if(tallyEpoch !== undefined && tallyEpoch != null) {
        o.setTallyEpoch(tallyEpoch);
      }
      if(count !== undefined && count != null) {
        o.setCount(count);
      }
      if(shortUrl !== undefined && shortUrl != null) {
        o.setShortUrl(shortUrl);
      }
      if(shortUrlDomain !== undefined && shortUrlDomain != null) {
        o.setShortUrlDomain(shortUrlDomain);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(tallyType !== undefined && tallyType != null) {
        jsonObj.tallyType = tallyType;
      } // Otherwise ignore...
      if(tallyTime !== undefined && tallyTime != null) {
        jsonObj.tallyTime = tallyTime;
      } // Otherwise ignore...
      if(tallyEpoch !== undefined && tallyEpoch != null) {
        jsonObj.tallyEpoch = tallyEpoch;
      } // Otherwise ignore...
      if(count !== undefined && count != null) {
        jsonObj.count = count;
      } // Otherwise ignore...
      if(shortUrl !== undefined && shortUrl != null) {
        jsonObj.shortUrl = shortUrl;
      } // Otherwise ignore...
      if(shortUrlDomain !== undefined && shortUrlDomain != null) {
        jsonObj.shortUrlDomain = shortUrlDomain;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(tallyType) {
        str += "\"tallyType\":\"" + tallyType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tallyType\":null, ";
      }
      if(tallyTime) {
        str += "\"tallyTime\":\"" + tallyTime + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tallyTime\":null, ";
      }
      if(tallyEpoch) {
        str += "\"tallyEpoch\":" + tallyEpoch + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tallyEpoch\":null, ";
      }
      if(count) {
        str += "\"count\":" + count + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"count\":null, ";
      }
      if(shortUrl) {
        str += "\"shortUrl\":\"" + shortUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrl\":null, ";
      }
      if(shortUrlDomain) {
        str += "\"shortUrlDomain\":\"" + shortUrlDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrlDomain\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "tallyType:" + tallyType + ", ";
      str += "tallyTime:" + tallyTime + ", ";
      str += "tallyEpoch:" + tallyEpoch + ", ";
      str += "count:" + count + ", ";
      str += "shortUrl:" + shortUrl + ", ";
      str += "shortUrlDomain:" + shortUrlDomain + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

urltally.wa.bean.TotalShortUrlAccessJsBean.create = function(obj) {
  var o = new urltally.wa.bean.TotalShortUrlAccessJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.tallyType !== undefined && obj.tallyType != null) {
    o.setTallyType(obj.tallyType);
  }
  if(obj.tallyTime !== undefined && obj.tallyTime != null) {
    o.setTallyTime(obj.tallyTime);
  }
  if(obj.tallyEpoch !== undefined && obj.tallyEpoch != null) {
    o.setTallyEpoch(obj.tallyEpoch);
  }
  if(obj.count !== undefined && obj.count != null) {
    o.setCount(obj.count);
  }
  if(obj.shortUrl !== undefined && obj.shortUrl != null) {
    o.setShortUrl(obj.shortUrl);
  }
  if(obj.shortUrlDomain !== undefined && obj.shortUrlDomain != null) {
    o.setShortUrlDomain(obj.shortUrlDomain);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

urltally.wa.bean.TotalShortUrlAccessJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = urltally.wa.bean.TotalShortUrlAccessJsBean.create(jsonObj);
  return obj;
};
