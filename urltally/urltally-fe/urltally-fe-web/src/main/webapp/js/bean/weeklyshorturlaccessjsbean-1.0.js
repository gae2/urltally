//////////////////////////////////////////////////////////
// <script src="/js/bean/weeklyshorturlaccessjsbean-1.0.js"></script>
// Last modified time: 1363421920814.
//////////////////////////////////////////////////////////

var urltally = urltally || {};
urltally.wa = urltally.wa || {};
urltally.wa.bean = urltally.wa.bean || {};
urltally.wa.bean.WeeklyShortUrlAccessJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var tallyTime;
    var tallyEpoch;
    var count;
    var shortUrl;
    var shortUrlDomain;
    var longUrl;
    var longUrlDomain;
    var redirectType;
    var refererDomain;
    var userAgent;
    var language;
    var country;
    var talliedTime;
    var year;
    var week;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getTallyTime = function() { return tallyTime; };
    this.setTallyTime = function(value) { tallyTime = value; };
    this.getTallyEpoch = function() { return tallyEpoch; };
    this.setTallyEpoch = function(value) { tallyEpoch = value; };
    this.getCount = function() { return count; };
    this.setCount = function(value) { count = value; };
    this.getShortUrl = function() { return shortUrl; };
    this.setShortUrl = function(value) { shortUrl = value; };
    this.getShortUrlDomain = function() { return shortUrlDomain; };
    this.setShortUrlDomain = function(value) { shortUrlDomain = value; };
    this.getLongUrl = function() { return longUrl; };
    this.setLongUrl = function(value) { longUrl = value; };
    this.getLongUrlDomain = function() { return longUrlDomain; };
    this.setLongUrlDomain = function(value) { longUrlDomain = value; };
    this.getRedirectType = function() { return redirectType; };
    this.setRedirectType = function(value) { redirectType = value; };
    this.getRefererDomain = function() { return refererDomain; };
    this.setRefererDomain = function(value) { refererDomain = value; };
    this.getUserAgent = function() { return userAgent; };
    this.setUserAgent = function(value) { userAgent = value; };
    this.getLanguage = function() { return language; };
    this.setLanguage = function(value) { language = value; };
    this.getCountry = function() { return country; };
    this.setCountry = function(value) { country = value; };
    this.getTalliedTime = function() { return talliedTime; };
    this.setTalliedTime = function(value) { talliedTime = value; };
    this.getYear = function() { return year; };
    this.setYear = function(value) { year = value; };
    this.getWeek = function() { return week; };
    this.setWeek = function(value) { week = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new urltally.wa.bean.WeeklyShortUrlAccessJsBean();

      o.setGuid(generateUuid());
      if(tallyTime !== undefined && tallyTime != null) {
        o.setTallyTime(tallyTime);
      }
      if(tallyEpoch !== undefined && tallyEpoch != null) {
        o.setTallyEpoch(tallyEpoch);
      }
      if(count !== undefined && count != null) {
        o.setCount(count);
      }
      if(shortUrl !== undefined && shortUrl != null) {
        o.setShortUrl(shortUrl);
      }
      if(shortUrlDomain !== undefined && shortUrlDomain != null) {
        o.setShortUrlDomain(shortUrlDomain);
      }
      if(longUrl !== undefined && longUrl != null) {
        o.setLongUrl(longUrl);
      }
      if(longUrlDomain !== undefined && longUrlDomain != null) {
        o.setLongUrlDomain(longUrlDomain);
      }
      if(redirectType !== undefined && redirectType != null) {
        o.setRedirectType(redirectType);
      }
      if(refererDomain !== undefined && refererDomain != null) {
        o.setRefererDomain(refererDomain);
      }
      if(userAgent !== undefined && userAgent != null) {
        o.setUserAgent(userAgent);
      }
      if(language !== undefined && language != null) {
        o.setLanguage(language);
      }
      if(country !== undefined && country != null) {
        o.setCountry(country);
      }
      if(talliedTime !== undefined && talliedTime != null) {
        o.setTalliedTime(talliedTime);
      }
      if(year !== undefined && year != null) {
        o.setYear(year);
      }
      if(week !== undefined && week != null) {
        o.setWeek(week);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(tallyTime !== undefined && tallyTime != null) {
        jsonObj.tallyTime = tallyTime;
      } // Otherwise ignore...
      if(tallyEpoch !== undefined && tallyEpoch != null) {
        jsonObj.tallyEpoch = tallyEpoch;
      } // Otherwise ignore...
      if(count !== undefined && count != null) {
        jsonObj.count = count;
      } // Otherwise ignore...
      if(shortUrl !== undefined && shortUrl != null) {
        jsonObj.shortUrl = shortUrl;
      } // Otherwise ignore...
      if(shortUrlDomain !== undefined && shortUrlDomain != null) {
        jsonObj.shortUrlDomain = shortUrlDomain;
      } // Otherwise ignore...
      if(longUrl !== undefined && longUrl != null) {
        jsonObj.longUrl = longUrl;
      } // Otherwise ignore...
      if(longUrlDomain !== undefined && longUrlDomain != null) {
        jsonObj.longUrlDomain = longUrlDomain;
      } // Otherwise ignore...
      if(redirectType !== undefined && redirectType != null) {
        jsonObj.redirectType = redirectType;
      } // Otherwise ignore...
      if(refererDomain !== undefined && refererDomain != null) {
        jsonObj.refererDomain = refererDomain;
      } // Otherwise ignore...
      if(userAgent !== undefined && userAgent != null) {
        jsonObj.userAgent = userAgent;
      } // Otherwise ignore...
      if(language !== undefined && language != null) {
        jsonObj.language = language;
      } // Otherwise ignore...
      if(country !== undefined && country != null) {
        jsonObj.country = country;
      } // Otherwise ignore...
      if(talliedTime !== undefined && talliedTime != null) {
        jsonObj.talliedTime = talliedTime;
      } // Otherwise ignore...
      if(year !== undefined && year != null) {
        jsonObj.year = year;
      } // Otherwise ignore...
      if(week !== undefined && week != null) {
        jsonObj.week = week;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(tallyTime) {
        str += "\"tallyTime\":\"" + tallyTime + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tallyTime\":null, ";
      }
      if(tallyEpoch) {
        str += "\"tallyEpoch\":" + tallyEpoch + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tallyEpoch\":null, ";
      }
      if(count) {
        str += "\"count\":" + count + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"count\":null, ";
      }
      if(shortUrl) {
        str += "\"shortUrl\":\"" + shortUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrl\":null, ";
      }
      if(shortUrlDomain) {
        str += "\"shortUrlDomain\":\"" + shortUrlDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"shortUrlDomain\":null, ";
      }
      if(longUrl) {
        str += "\"longUrl\":\"" + longUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrl\":null, ";
      }
      if(longUrlDomain) {
        str += "\"longUrlDomain\":\"" + longUrlDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"longUrlDomain\":null, ";
      }
      if(redirectType) {
        str += "\"redirectType\":\"" + redirectType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"redirectType\":null, ";
      }
      if(refererDomain) {
        str += "\"refererDomain\":\"" + refererDomain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"refererDomain\":null, ";
      }
      if(userAgent) {
        str += "\"userAgent\":\"" + userAgent + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userAgent\":null, ";
      }
      if(language) {
        str += "\"language\":\"" + language + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"language\":null, ";
      }
      if(country) {
        str += "\"country\":\"" + country + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"country\":null, ";
      }
      if(talliedTime) {
        str += "\"talliedTime\":" + talliedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"talliedTime\":null, ";
      }
      if(year) {
        str += "\"year\":" + year + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"year\":null, ";
      }
      if(week) {
        str += "\"week\":" + week + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"week\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "tallyTime:" + tallyTime + ", ";
      str += "tallyEpoch:" + tallyEpoch + ", ";
      str += "count:" + count + ", ";
      str += "shortUrl:" + shortUrl + ", ";
      str += "shortUrlDomain:" + shortUrlDomain + ", ";
      str += "longUrl:" + longUrl + ", ";
      str += "longUrlDomain:" + longUrlDomain + ", ";
      str += "redirectType:" + redirectType + ", ";
      str += "refererDomain:" + refererDomain + ", ";
      str += "userAgent:" + userAgent + ", ";
      str += "language:" + language + ", ";
      str += "country:" + country + ", ";
      str += "talliedTime:" + talliedTime + ", ";
      str += "year:" + year + ", ";
      str += "week:" + week + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

urltally.wa.bean.WeeklyShortUrlAccessJsBean.create = function(obj) {
  var o = new urltally.wa.bean.WeeklyShortUrlAccessJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.tallyTime !== undefined && obj.tallyTime != null) {
    o.setTallyTime(obj.tallyTime);
  }
  if(obj.tallyEpoch !== undefined && obj.tallyEpoch != null) {
    o.setTallyEpoch(obj.tallyEpoch);
  }
  if(obj.count !== undefined && obj.count != null) {
    o.setCount(obj.count);
  }
  if(obj.shortUrl !== undefined && obj.shortUrl != null) {
    o.setShortUrl(obj.shortUrl);
  }
  if(obj.shortUrlDomain !== undefined && obj.shortUrlDomain != null) {
    o.setShortUrlDomain(obj.shortUrlDomain);
  }
  if(obj.longUrl !== undefined && obj.longUrl != null) {
    o.setLongUrl(obj.longUrl);
  }
  if(obj.longUrlDomain !== undefined && obj.longUrlDomain != null) {
    o.setLongUrlDomain(obj.longUrlDomain);
  }
  if(obj.redirectType !== undefined && obj.redirectType != null) {
    o.setRedirectType(obj.redirectType);
  }
  if(obj.refererDomain !== undefined && obj.refererDomain != null) {
    o.setRefererDomain(obj.refererDomain);
  }
  if(obj.userAgent !== undefined && obj.userAgent != null) {
    o.setUserAgent(obj.userAgent);
  }
  if(obj.language !== undefined && obj.language != null) {
    o.setLanguage(obj.language);
  }
  if(obj.country !== undefined && obj.country != null) {
    o.setCountry(obj.country);
  }
  if(obj.talliedTime !== undefined && obj.talliedTime != null) {
    o.setTalliedTime(obj.talliedTime);
  }
  if(obj.year !== undefined && obj.year != null) {
    o.setYear(obj.year);
  }
  if(obj.week !== undefined && obj.week != null) {
    o.setWeek(obj.week);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

urltally.wa.bean.WeeklyShortUrlAccessJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = urltally.wa.bean.WeeklyShortUrlAccessJsBean.create(jsonObj);
  return obj;
};
