//////////////////////////////////////////////////////////
// <script src="/js/bean/accesstallymasterjsbean-1.0.js"></script>
// Last modified time: 1363421920788.
//////////////////////////////////////////////////////////

var urltally = urltally || {};
urltally.wa = urltally.wa || {};
urltally.wa.bean = urltally.wa.bean || {};
urltally.wa.bean.AccessTallyMasterJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var tallyType;
    var tallyTime;
    var tallyEpoch;
    var tallyStatus;
    var accessRecordCount;
    var procesingStartedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getTallyType = function() { return tallyType; };
    this.setTallyType = function(value) { tallyType = value; };
    this.getTallyTime = function() { return tallyTime; };
    this.setTallyTime = function(value) { tallyTime = value; };
    this.getTallyEpoch = function() { return tallyEpoch; };
    this.setTallyEpoch = function(value) { tallyEpoch = value; };
    this.getTallyStatus = function() { return tallyStatus; };
    this.setTallyStatus = function(value) { tallyStatus = value; };
    this.getAccessRecordCount = function() { return accessRecordCount; };
    this.setAccessRecordCount = function(value) { accessRecordCount = value; };
    this.getProcesingStartedTime = function() { return procesingStartedTime; };
    this.setProcesingStartedTime = function(value) { procesingStartedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new urltally.wa.bean.AccessTallyMasterJsBean();

      o.setGuid(generateUuid());
      if(tallyType !== undefined && tallyType != null) {
        o.setTallyType(tallyType);
      }
      if(tallyTime !== undefined && tallyTime != null) {
        o.setTallyTime(tallyTime);
      }
      if(tallyEpoch !== undefined && tallyEpoch != null) {
        o.setTallyEpoch(tallyEpoch);
      }
      if(tallyStatus !== undefined && tallyStatus != null) {
        o.setTallyStatus(tallyStatus);
      }
      if(accessRecordCount !== undefined && accessRecordCount != null) {
        o.setAccessRecordCount(accessRecordCount);
      }
      if(procesingStartedTime !== undefined && procesingStartedTime != null) {
        o.setProcesingStartedTime(procesingStartedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(tallyType !== undefined && tallyType != null) {
        jsonObj.tallyType = tallyType;
      } // Otherwise ignore...
      if(tallyTime !== undefined && tallyTime != null) {
        jsonObj.tallyTime = tallyTime;
      } // Otherwise ignore...
      if(tallyEpoch !== undefined && tallyEpoch != null) {
        jsonObj.tallyEpoch = tallyEpoch;
      } // Otherwise ignore...
      if(tallyStatus !== undefined && tallyStatus != null) {
        jsonObj.tallyStatus = tallyStatus;
      } // Otherwise ignore...
      if(accessRecordCount !== undefined && accessRecordCount != null) {
        jsonObj.accessRecordCount = accessRecordCount;
      } // Otherwise ignore...
      if(procesingStartedTime !== undefined && procesingStartedTime != null) {
        jsonObj.procesingStartedTime = procesingStartedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(tallyType) {
        str += "\"tallyType\":\"" + tallyType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tallyType\":null, ";
      }
      if(tallyTime) {
        str += "\"tallyTime\":\"" + tallyTime + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tallyTime\":null, ";
      }
      if(tallyEpoch) {
        str += "\"tallyEpoch\":" + tallyEpoch + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tallyEpoch\":null, ";
      }
      if(tallyStatus) {
        str += "\"tallyStatus\":\"" + tallyStatus + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tallyStatus\":null, ";
      }
      if(accessRecordCount) {
        str += "\"accessRecordCount\":" + accessRecordCount + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"accessRecordCount\":null, ";
      }
      if(procesingStartedTime) {
        str += "\"procesingStartedTime\":" + procesingStartedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"procesingStartedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "tallyType:" + tallyType + ", ";
      str += "tallyTime:" + tallyTime + ", ";
      str += "tallyEpoch:" + tallyEpoch + ", ";
      str += "tallyStatus:" + tallyStatus + ", ";
      str += "accessRecordCount:" + accessRecordCount + ", ";
      str += "procesingStartedTime:" + procesingStartedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

urltally.wa.bean.AccessTallyMasterJsBean.create = function(obj) {
  var o = new urltally.wa.bean.AccessTallyMasterJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.tallyType !== undefined && obj.tallyType != null) {
    o.setTallyType(obj.tallyType);
  }
  if(obj.tallyTime !== undefined && obj.tallyTime != null) {
    o.setTallyTime(obj.tallyTime);
  }
  if(obj.tallyEpoch !== undefined && obj.tallyEpoch != null) {
    o.setTallyEpoch(obj.tallyEpoch);
  }
  if(obj.tallyStatus !== undefined && obj.tallyStatus != null) {
    o.setTallyStatus(obj.tallyStatus);
  }
  if(obj.accessRecordCount !== undefined && obj.accessRecordCount != null) {
    o.setAccessRecordCount(obj.accessRecordCount);
  }
  if(obj.procesingStartedTime !== undefined && obj.procesingStartedTime != null) {
    o.setProcesingStartedTime(obj.procesingStartedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

urltally.wa.bean.AccessTallyMasterJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = urltally.wa.bean.AccessTallyMasterJsBean.create(jsonObj);
  return obj;
};
