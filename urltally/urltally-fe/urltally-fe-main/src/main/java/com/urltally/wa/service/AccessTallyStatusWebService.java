package com.urltally.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.service.manager.ServiceManager;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.AccessTallyStatusJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AccessTallyStatusWebService // implements AccessTallyStatusService
{
    private static final Logger log = Logger.getLogger(AccessTallyStatusWebService.class.getName());
     
    // Af service interface.
    private AccessTallyStatusService mService = null;

    public AccessTallyStatusWebService()
    {
        this(ServiceManager.getAccessTallyStatusService());
    }
    public AccessTallyStatusWebService(AccessTallyStatusService service)
    {
        mService = service;
    }
    
    private AccessTallyStatusService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getAccessTallyStatusService();
        }
        return mService;
    }
    
    
    public AccessTallyStatusJsBean getAccessTallyStatus(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyStatus accessTallyStatus = getService().getAccessTallyStatus(guid);
            AccessTallyStatusJsBean bean = convertAccessTallyStatusToJsBean(accessTallyStatus);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getAccessTallyStatus(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getAccessTallyStatus(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AccessTallyStatusJsBean> getAccessTallyStatuses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AccessTallyStatusJsBean> jsBeans = new ArrayList<AccessTallyStatusJsBean>();
            List<AccessTallyStatus> accessTallyStatuses = getService().getAccessTallyStatuses(guids);
            if(accessTallyStatuses != null) {
                for(AccessTallyStatus accessTallyStatus : accessTallyStatuses) {
                    jsBeans.add(convertAccessTallyStatusToJsBean(accessTallyStatus));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AccessTallyStatusJsBean> getAllAccessTallyStatuses() throws WebException
    {
        return getAllAccessTallyStatuses(null, null, null);
    }

    // @Deprecated
    public List<AccessTallyStatusJsBean> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    public List<AccessTallyStatusJsBean> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AccessTallyStatusJsBean> jsBeans = new ArrayList<AccessTallyStatusJsBean>();
            List<AccessTallyStatus> accessTallyStatuses = getService().getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
            if(accessTallyStatuses != null) {
                for(AccessTallyStatus accessTallyStatus : accessTallyStatuses) {
                    jsBeans.add(convertAccessTallyStatusToJsBean(accessTallyStatus));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<AccessTallyStatusJsBean> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<AccessTallyStatusJsBean> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<AccessTallyStatusJsBean> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AccessTallyStatusJsBean> jsBeans = new ArrayList<AccessTallyStatusJsBean>();
            List<AccessTallyStatus> accessTallyStatuses = getService().findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(accessTallyStatuses != null) {
                for(AccessTallyStatus accessTallyStatus : accessTallyStatuses) {
                    jsBeans.add(convertAccessTallyStatusToJsBean(accessTallyStatus));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws WebException
    {
        try {
            return getService().createAccessTallyStatus(remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAccessTallyStatus(String jsonStr) throws WebException
    {
        return createAccessTallyStatus(AccessTallyStatusJsBean.fromJsonString(jsonStr));
    }

    public String createAccessTallyStatus(AccessTallyStatusJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyStatus accessTallyStatus = convertAccessTallyStatusJsBeanToBean(jsBean);
            return getService().createAccessTallyStatus(accessTallyStatus);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AccessTallyStatusJsBean constructAccessTallyStatus(String jsonStr) throws WebException
    {
        return constructAccessTallyStatus(AccessTallyStatusJsBean.fromJsonString(jsonStr));
    }

    public AccessTallyStatusJsBean constructAccessTallyStatus(AccessTallyStatusJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyStatus accessTallyStatus = convertAccessTallyStatusJsBeanToBean(jsBean);
            accessTallyStatus = getService().constructAccessTallyStatus(accessTallyStatus);
            jsBean = convertAccessTallyStatusToJsBean(accessTallyStatus);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws WebException
    {
        try {
            return getService().updateAccessTallyStatus(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateAccessTallyStatus(String jsonStr) throws WebException
    {
        return updateAccessTallyStatus(AccessTallyStatusJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateAccessTallyStatus(AccessTallyStatusJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyStatus accessTallyStatus = convertAccessTallyStatusJsBeanToBean(jsBean);
            return getService().updateAccessTallyStatus(accessTallyStatus);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AccessTallyStatusJsBean refreshAccessTallyStatus(String jsonStr) throws WebException
    {
        return refreshAccessTallyStatus(AccessTallyStatusJsBean.fromJsonString(jsonStr));
    }

    public AccessTallyStatusJsBean refreshAccessTallyStatus(AccessTallyStatusJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyStatus accessTallyStatus = convertAccessTallyStatusJsBeanToBean(jsBean);
            accessTallyStatus = getService().refreshAccessTallyStatus(accessTallyStatus);
            jsBean = convertAccessTallyStatusToJsBean(accessTallyStatus);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAccessTallyStatus(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteAccessTallyStatus(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAccessTallyStatus(AccessTallyStatusJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyStatus accessTallyStatus = convertAccessTallyStatusJsBeanToBean(jsBean);
            return getService().deleteAccessTallyStatus(accessTallyStatus);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteAccessTallyStatuses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static AccessTallyStatusJsBean convertAccessTallyStatusToJsBean(AccessTallyStatus accessTallyStatus)
    {
        AccessTallyStatusJsBean jsBean = null;
        if(accessTallyStatus != null) {
            jsBean = new AccessTallyStatusJsBean();
            jsBean.setGuid(accessTallyStatus.getGuid());
            jsBean.setRemoteRecordGuid(accessTallyStatus.getRemoteRecordGuid());
            jsBean.setShortUrl(accessTallyStatus.getShortUrl());
            jsBean.setTallyType(accessTallyStatus.getTallyType());
            jsBean.setTallyTime(accessTallyStatus.getTallyTime());
            jsBean.setTallyEpoch(accessTallyStatus.getTallyEpoch());
            jsBean.setProcessed(accessTallyStatus.isProcessed());
            jsBean.setCreatedTime(accessTallyStatus.getCreatedTime());
            jsBean.setModifiedTime(accessTallyStatus.getModifiedTime());
        }
        return jsBean;
    }

    public static AccessTallyStatus convertAccessTallyStatusJsBeanToBean(AccessTallyStatusJsBean jsBean)
    {
        AccessTallyStatusBean accessTallyStatus = null;
        if(jsBean != null) {
            accessTallyStatus = new AccessTallyStatusBean();
            accessTallyStatus.setGuid(jsBean.getGuid());
            accessTallyStatus.setRemoteRecordGuid(jsBean.getRemoteRecordGuid());
            accessTallyStatus.setShortUrl(jsBean.getShortUrl());
            accessTallyStatus.setTallyType(jsBean.getTallyType());
            accessTallyStatus.setTallyTime(jsBean.getTallyTime());
            accessTallyStatus.setTallyEpoch(jsBean.getTallyEpoch());
            accessTallyStatus.setProcessed(jsBean.isProcessed());
            accessTallyStatus.setCreatedTime(jsBean.getCreatedTime());
            accessTallyStatus.setModifiedTime(jsBean.getModifiedTime());
        }
        return accessTallyStatus;
    }

}
