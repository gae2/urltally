package com.urltally.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.service.manager.ServiceManager;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.AccessTallyMasterJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AccessTallyMasterWebService // implements AccessTallyMasterService
{
    private static final Logger log = Logger.getLogger(AccessTallyMasterWebService.class.getName());
     
    // Af service interface.
    private AccessTallyMasterService mService = null;

    public AccessTallyMasterWebService()
    {
        this(ServiceManager.getAccessTallyMasterService());
    }
    public AccessTallyMasterWebService(AccessTallyMasterService service)
    {
        mService = service;
    }
    
    private AccessTallyMasterService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getAccessTallyMasterService();
        }
        return mService;
    }
    
    
    public AccessTallyMasterJsBean getAccessTallyMaster(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyMaster accessTallyMaster = getService().getAccessTallyMaster(guid);
            AccessTallyMasterJsBean bean = convertAccessTallyMasterToJsBean(accessTallyMaster);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getAccessTallyMaster(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getAccessTallyMaster(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AccessTallyMasterJsBean> getAccessTallyMasters(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AccessTallyMasterJsBean> jsBeans = new ArrayList<AccessTallyMasterJsBean>();
            List<AccessTallyMaster> accessTallyMasters = getService().getAccessTallyMasters(guids);
            if(accessTallyMasters != null) {
                for(AccessTallyMaster accessTallyMaster : accessTallyMasters) {
                    jsBeans.add(convertAccessTallyMasterToJsBean(accessTallyMaster));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AccessTallyMasterJsBean> getAllAccessTallyMasters() throws WebException
    {
        return getAllAccessTallyMasters(null, null, null);
    }

    // @Deprecated
    public List<AccessTallyMasterJsBean> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    public List<AccessTallyMasterJsBean> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AccessTallyMasterJsBean> jsBeans = new ArrayList<AccessTallyMasterJsBean>();
            List<AccessTallyMaster> accessTallyMasters = getService().getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
            if(accessTallyMasters != null) {
                for(AccessTallyMaster accessTallyMaster : accessTallyMasters) {
                    jsBeans.add(convertAccessTallyMasterToJsBean(accessTallyMaster));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<AccessTallyMasterJsBean> findAccessTallyMasters(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<AccessTallyMasterJsBean> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<AccessTallyMasterJsBean> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AccessTallyMasterJsBean> jsBeans = new ArrayList<AccessTallyMasterJsBean>();
            List<AccessTallyMaster> accessTallyMasters = getService().findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(accessTallyMasters != null) {
                for(AccessTallyMaster accessTallyMaster : accessTallyMasters) {
                    jsBeans.add(convertAccessTallyMasterToJsBean(accessTallyMaster));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws WebException
    {
        try {
            return getService().createAccessTallyMaster(tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAccessTallyMaster(String jsonStr) throws WebException
    {
        return createAccessTallyMaster(AccessTallyMasterJsBean.fromJsonString(jsonStr));
    }

    public String createAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            return getService().createAccessTallyMaster(accessTallyMaster);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AccessTallyMasterJsBean constructAccessTallyMaster(String jsonStr) throws WebException
    {
        return constructAccessTallyMaster(AccessTallyMasterJsBean.fromJsonString(jsonStr));
    }

    public AccessTallyMasterJsBean constructAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            accessTallyMaster = getService().constructAccessTallyMaster(accessTallyMaster);
            jsBean = convertAccessTallyMasterToJsBean(accessTallyMaster);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws WebException
    {
        try {
            return getService().updateAccessTallyMaster(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateAccessTallyMaster(String jsonStr) throws WebException
    {
        return updateAccessTallyMaster(AccessTallyMasterJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            return getService().updateAccessTallyMaster(accessTallyMaster);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AccessTallyMasterJsBean refreshAccessTallyMaster(String jsonStr) throws WebException
    {
        return refreshAccessTallyMaster(AccessTallyMasterJsBean.fromJsonString(jsonStr));
    }

    public AccessTallyMasterJsBean refreshAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            accessTallyMaster = getService().refreshAccessTallyMaster(accessTallyMaster);
            jsBean = convertAccessTallyMasterToJsBean(accessTallyMaster);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAccessTallyMaster(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteAccessTallyMaster(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            return getService().deleteAccessTallyMaster(accessTallyMaster);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteAccessTallyMasters(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static AccessTallyMasterJsBean convertAccessTallyMasterToJsBean(AccessTallyMaster accessTallyMaster)
    {
        AccessTallyMasterJsBean jsBean = null;
        if(accessTallyMaster != null) {
            jsBean = new AccessTallyMasterJsBean();
            jsBean.setGuid(accessTallyMaster.getGuid());
            jsBean.setTallyType(accessTallyMaster.getTallyType());
            jsBean.setTallyTime(accessTallyMaster.getTallyTime());
            jsBean.setTallyEpoch(accessTallyMaster.getTallyEpoch());
            jsBean.setTallyStatus(accessTallyMaster.getTallyStatus());
            jsBean.setAccessRecordCount(accessTallyMaster.getAccessRecordCount());
            jsBean.setProcesingStartedTime(accessTallyMaster.getProcesingStartedTime());
            jsBean.setCreatedTime(accessTallyMaster.getCreatedTime());
            jsBean.setModifiedTime(accessTallyMaster.getModifiedTime());
        }
        return jsBean;
    }

    public static AccessTallyMaster convertAccessTallyMasterJsBeanToBean(AccessTallyMasterJsBean jsBean)
    {
        AccessTallyMasterBean accessTallyMaster = null;
        if(jsBean != null) {
            accessTallyMaster = new AccessTallyMasterBean();
            accessTallyMaster.setGuid(jsBean.getGuid());
            accessTallyMaster.setTallyType(jsBean.getTallyType());
            accessTallyMaster.setTallyTime(jsBean.getTallyTime());
            accessTallyMaster.setTallyEpoch(jsBean.getTallyEpoch());
            accessTallyMaster.setTallyStatus(jsBean.getTallyStatus());
            accessTallyMaster.setAccessRecordCount(jsBean.getAccessRecordCount());
            accessTallyMaster.setProcesingStartedTime(jsBean.getProcesingStartedTime());
            accessTallyMaster.setCreatedTime(jsBean.getCreatedTime());
            accessTallyMaster.setModifiedTime(jsBean.getModifiedTime());
        }
        return accessTallyMaster;
    }

}
