package com.urltally.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.CurrentShortUrlAccessService;


// CurrentShortUrlAccessMockService is a decorator.
// It can be used as a base class to mock CurrentShortUrlAccessService objects.
public abstract class CurrentShortUrlAccessMockService implements CurrentShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessMockService.class.getName());

    // CurrentShortUrlAccessMockService uses the decorator design pattern.
    private CurrentShortUrlAccessService decoratedService;

    public CurrentShortUrlAccessMockService(CurrentShortUrlAccessService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected CurrentShortUrlAccessService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(CurrentShortUrlAccessService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // CurrentShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public CurrentShortUrlAccess getCurrentShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getCurrentShortUrlAccess(): guid = " + guid);
        CurrentShortUrlAccess bean = decoratedService.getCurrentShortUrlAccess(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getCurrentShortUrlAccess(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getCurrentShortUrlAccess(guid, field);
        return obj;
    }

    @Override
    public List<CurrentShortUrlAccess> getCurrentShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getCurrentShortUrlAccesses()");
        List<CurrentShortUrlAccess> currentShortUrlAccesses = decoratedService.getCurrentShortUrlAccesses(guids);
        log.finer("END");
        return currentShortUrlAccesses;
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses() throws BaseException
    {
        return getAllCurrentShortUrlAccesses(null, null, null);
    }


    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllCurrentShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<CurrentShortUrlAccess> currentShortUrlAccesses = decoratedService.getAllCurrentShortUrlAccesses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return currentShortUrlAccesses;
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllCurrentShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllCurrentShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("CurrentShortUrlAccessMockService.findCurrentShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<CurrentShortUrlAccess> currentShortUrlAccesses = decoratedService.findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return currentShortUrlAccesses;
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("CurrentShortUrlAccessMockService.findCurrentShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("CurrentShortUrlAccessMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createCurrentShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException
    {
        CurrentShortUrlAccessBean bean = new CurrentShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
        return createCurrentShortUrlAccess(bean);
    }

    @Override
    public String createCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createCurrentShortUrlAccess(currentShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public CurrentShortUrlAccess constructCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        CurrentShortUrlAccess bean = decoratedService.constructCurrentShortUrlAccess(currentShortUrlAccess);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateCurrentShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        CurrentShortUrlAccessBean bean = new CurrentShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
        return updateCurrentShortUrlAccess(bean);
    }
        
    @Override
    public Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateCurrentShortUrlAccess(currentShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public CurrentShortUrlAccess refreshCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        CurrentShortUrlAccess bean = decoratedService.refreshCurrentShortUrlAccess(currentShortUrlAccess);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteCurrentShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteCurrentShortUrlAccess(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteCurrentShortUrlAccess(currentShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteCurrentShortUrlAccesses(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createCurrentShortUrlAccesses(List<CurrentShortUrlAccess> currentShortUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createCurrentShortUrlAccesses(currentShortUrlAccesses);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateCurrentShortUrlAccesses(List<CurrentShortUrlAccess> currentShortUrlAccesses) throws BaseException
    //{
    //}

}
