package com.urltally.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.urltally.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessTallyMasterJsBean implements Serializable, Cloneable  //, AccessTallyMaster
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AccessTallyMasterJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String tallyType;
    private String tallyTime;
    private Long tallyEpoch;
    private String tallyStatus;
    private Integer accessRecordCount;
    private Long procesingStartedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public AccessTallyMasterJsBean()
    {
        //this((String) null);
    }
    public AccessTallyMasterJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public AccessTallyMasterJsBean(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime)
    {
        this(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime, null, null);
    }
    public AccessTallyMasterJsBean(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.tallyType = tallyType;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.tallyStatus = tallyStatus;
        this.accessRecordCount = accessRecordCount;
        this.procesingStartedTime = procesingStartedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public AccessTallyMasterJsBean(AccessTallyMasterJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setTallyType(bean.getTallyType());
            setTallyTime(bean.getTallyTime());
            setTallyEpoch(bean.getTallyEpoch());
            setTallyStatus(bean.getTallyStatus());
            setAccessRecordCount(bean.getAccessRecordCount());
            setProcesingStartedTime(bean.getProcesingStartedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static AccessTallyMasterJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        AccessTallyMasterJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(AccessTallyMasterJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, AccessTallyMasterJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getTallyType()
    {
        return this.tallyType;
    }
    public void setTallyType(String tallyType)
    {
        this.tallyType = tallyType;
    }

    public String getTallyTime()
    {
        return this.tallyTime;
    }
    public void setTallyTime(String tallyTime)
    {
        this.tallyTime = tallyTime;
    }

    public Long getTallyEpoch()
    {
        return this.tallyEpoch;
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        this.tallyEpoch = tallyEpoch;
    }

    public String getTallyStatus()
    {
        return this.tallyStatus;
    }
    public void setTallyStatus(String tallyStatus)
    {
        this.tallyStatus = tallyStatus;
    }

    public Integer getAccessRecordCount()
    {
        return this.accessRecordCount;
    }
    public void setAccessRecordCount(Integer accessRecordCount)
    {
        this.accessRecordCount = accessRecordCount;
    }

    public Long getProcesingStartedTime()
    {
        return this.procesingStartedTime;
    }
    public void setProcesingStartedTime(Long procesingStartedTime)
    {
        this.procesingStartedTime = procesingStartedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("tallyType:null, ");
        sb.append("tallyTime:null, ");
        sb.append("tallyEpoch:0, ");
        sb.append("tallyStatus:null, ");
        sb.append("accessRecordCount:0, ");
        sb.append("procesingStartedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("tallyType:");
        if(this.getTallyType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTallyType()).append("\", ");
        }
        sb.append("tallyTime:");
        if(this.getTallyTime() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTallyTime()).append("\", ");
        }
        sb.append("tallyEpoch:" + this.getTallyEpoch()).append(", ");
        sb.append("tallyStatus:");
        if(this.getTallyStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTallyStatus()).append("\", ");
        }
        sb.append("accessRecordCount:" + this.getAccessRecordCount()).append(", ");
        sb.append("procesingStartedTime:" + this.getProcesingStartedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getTallyType() != null) {
            sb.append("\"tallyType\":").append("\"").append(this.getTallyType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyType\":").append("null, ");
        }
        if(this.getTallyTime() != null) {
            sb.append("\"tallyTime\":").append("\"").append(this.getTallyTime()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyTime\":").append("null, ");
        }
        if(this.getTallyEpoch() != null) {
            sb.append("\"tallyEpoch\":").append("").append(this.getTallyEpoch()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyEpoch\":").append("null, ");
        }
        if(this.getTallyStatus() != null) {
            sb.append("\"tallyStatus\":").append("\"").append(this.getTallyStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyStatus\":").append("null, ");
        }
        if(this.getAccessRecordCount() != null) {
            sb.append("\"accessRecordCount\":").append("").append(this.getAccessRecordCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"accessRecordCount\":").append("null, ");
        }
        if(this.getProcesingStartedTime() != null) {
            sb.append("\"procesingStartedTime\":").append("").append(this.getProcesingStartedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"procesingStartedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("tallyType = " + this.tallyType).append(";");
        sb.append("tallyTime = " + this.tallyTime).append(";");
        sb.append("tallyEpoch = " + this.tallyEpoch).append(";");
        sb.append("tallyStatus = " + this.tallyStatus).append(";");
        sb.append("accessRecordCount = " + this.accessRecordCount).append(";");
        sb.append("procesingStartedTime = " + this.procesingStartedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        AccessTallyMasterJsBean cloned = new AccessTallyMasterJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setTallyType(this.getTallyType());   
        cloned.setTallyTime(this.getTallyTime());   
        cloned.setTallyEpoch(this.getTallyEpoch());   
        cloned.setTallyStatus(this.getTallyStatus());   
        cloned.setAccessRecordCount(this.getAccessRecordCount());   
        cloned.setProcesingStartedTime(this.getProcesingStartedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
