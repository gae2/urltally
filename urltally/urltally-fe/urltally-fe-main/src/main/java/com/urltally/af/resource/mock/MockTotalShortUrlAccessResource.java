package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.stub.TotalShortUrlAccessStub;
import com.urltally.ws.stub.TotalShortUrlAccessListStub;
import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.resource.TotalShortUrlAccessResource;


// MockTotalShortUrlAccessResource is a decorator.
// It can be used as a base class to mock TotalShortUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/totalShortUrlAccesses/")
public abstract class MockTotalShortUrlAccessResource implements TotalShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockTotalShortUrlAccessResource.class.getName());

    // MockTotalShortUrlAccessResource uses the decorator design pattern.
    private TotalShortUrlAccessResource decoratedResource;

    public MockTotalShortUrlAccessResource(TotalShortUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TotalShortUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TotalShortUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTotalShortUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findTotalShortUrlAccessesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getTotalShortUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getTotalShortUrlAccessAsHtml(guid);
//    }

    @Override
    public Response getTotalShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getTotalShortUrlAccess(guid);
    }

    @Override
    public Response getTotalShortUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getTotalShortUrlAccessAsJsonp(guid, callback);
    }

    @Override
    public Response getTotalShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTotalShortUrlAccess(guid, field);
    }

    // TBD
    @Override
    public Response constructTotalShortUrlAccess(TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.constructTotalShortUrlAccess(totalShortUrlAccess);
    }

    @Override
    public Response createTotalShortUrlAccess(TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createTotalShortUrlAccess(totalShortUrlAccess);
    }

//    @Override
//    public Response createTotalShortUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createTotalShortUrlAccess(formParams);
//    }

    // TBD
    @Override
    public Response refreshTotalShortUrlAccess(String guid, TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.refreshTotalShortUrlAccess(guid, totalShortUrlAccess);
    }

    @Override
    public Response updateTotalShortUrlAccess(String guid, TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateTotalShortUrlAccess(guid, totalShortUrlAccess);
    }

    @Override
    public Response updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain)
    {
        return decoratedResource.updateTotalShortUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
    }

//    @Override
//    public Response updateTotalShortUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateTotalShortUrlAccess(guid, formParams);
//    }

    @Override
    public Response deleteTotalShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTotalShortUrlAccess(guid);
    }

    @Override
    public Response deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTotalShortUrlAccesses(filter, params, values);
    }


// TBD ....
    @Override
    public Response createTotalShortUrlAccesses(TotalShortUrlAccessListStub totalShortUrlAccesses) throws BaseResourceException
    {
        return decoratedResource.createTotalShortUrlAccesses(totalShortUrlAccesses);
    }


}
