package com.urltally.af.cert.proxy;

public abstract class AbstractProxyFactory
{
    public abstract PublicCertificateInfoServiceProxy getPublicCertificateInfoServiceProxy();
}
