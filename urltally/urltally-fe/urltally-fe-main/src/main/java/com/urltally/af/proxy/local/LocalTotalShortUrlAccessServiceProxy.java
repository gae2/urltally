package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalShortUrlAccess;
// import com.urltally.ws.bean.TotalShortUrlAccessBean;
import com.urltally.ws.service.TotalShortUrlAccessService;
import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.proxy.TotalShortUrlAccessServiceProxy;


public class LocalTotalShortUrlAccessServiceProxy extends BaseLocalServiceProxy implements TotalShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTotalShortUrlAccessServiceProxy.class.getName());

    public LocalTotalShortUrlAccessServiceProxy()
    {
    }

    @Override
    public TotalShortUrlAccess getTotalShortUrlAccess(String guid) throws BaseException
    {
        TotalShortUrlAccess serverBean = getTotalShortUrlAccessService().getTotalShortUrlAccess(guid);
        TotalShortUrlAccess appBean = convertServerTotalShortUrlAccessBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getTotalShortUrlAccess(String guid, String field) throws BaseException
    {
        return getTotalShortUrlAccessService().getTotalShortUrlAccess(guid, field);       
    }

    @Override
    public List<TotalShortUrlAccess> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        List<TotalShortUrlAccess> serverBeanList = getTotalShortUrlAccessService().getTotalShortUrlAccesses(guids);
        List<TotalShortUrlAccess> appBeanList = convertServerTotalShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses() throws BaseException
    {
        return getAllTotalShortUrlAccesses(null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTotalShortUrlAccessService().getAllTotalShortUrlAccesses(ordering, offset, count);
        return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TotalShortUrlAccess> serverBeanList = getTotalShortUrlAccessService().getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
        List<TotalShortUrlAccess> appBeanList = convertServerTotalShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTotalShortUrlAccessService().getAllTotalShortUrlAccessKeys(ordering, offset, count);
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTotalShortUrlAccessService().getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTotalShortUrlAccessService().findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TotalShortUrlAccess> serverBeanList = getTotalShortUrlAccessService().findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<TotalShortUrlAccess> appBeanList = convertServerTotalShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTotalShortUrlAccessService().findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTotalShortUrlAccessService().findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTotalShortUrlAccessService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        return getTotalShortUrlAccessService().createTotalShortUrlAccess(tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
    }

    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.TotalShortUrlAccessBean serverBean =  convertAppTotalShortUrlAccessBeanToServerBean(totalShortUrlAccess);
        return getTotalShortUrlAccessService().createTotalShortUrlAccess(serverBean);
    }

    @Override
    public Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        return getTotalShortUrlAccessService().updateTotalShortUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
    }

    @Override
    public Boolean updateTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.TotalShortUrlAccessBean serverBean =  convertAppTotalShortUrlAccessBeanToServerBean(totalShortUrlAccess);
        return getTotalShortUrlAccessService().updateTotalShortUrlAccess(serverBean);
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        return getTotalShortUrlAccessService().deleteTotalShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.TotalShortUrlAccessBean serverBean =  convertAppTotalShortUrlAccessBeanToServerBean(totalShortUrlAccess);
        return getTotalShortUrlAccessService().deleteTotalShortUrlAccess(serverBean);
    }

    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return getTotalShortUrlAccessService().deleteTotalShortUrlAccesses(filter, params, values);
    }




    public static TotalShortUrlAccessBean convertServerTotalShortUrlAccessBeanToAppBean(TotalShortUrlAccess serverBean)
    {
        TotalShortUrlAccessBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new TotalShortUrlAccessBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTallyType(serverBean.getTallyType());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setCount(serverBean.getCount());
            bean.setShortUrl(serverBean.getShortUrl());
            bean.setShortUrlDomain(serverBean.getShortUrlDomain());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TotalShortUrlAccess> convertServerTotalShortUrlAccessBeanListToAppBeanList(List<TotalShortUrlAccess> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<TotalShortUrlAccess> beanList = new ArrayList<TotalShortUrlAccess>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(TotalShortUrlAccess sb : serverBeanList) {
                TotalShortUrlAccessBean bean = convertServerTotalShortUrlAccessBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.TotalShortUrlAccessBean convertAppTotalShortUrlAccessBeanToServerBean(TotalShortUrlAccess appBean)
    {
        com.urltally.ws.bean.TotalShortUrlAccessBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.TotalShortUrlAccessBean();
            bean.setGuid(appBean.getGuid());
            bean.setTallyType(appBean.getTallyType());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setCount(appBean.getCount());
            bean.setShortUrl(appBean.getShortUrl());
            bean.setShortUrlDomain(appBean.getShortUrlDomain());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TotalShortUrlAccess> convertAppTotalShortUrlAccessBeanListToServerBeanList(List<TotalShortUrlAccess> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<TotalShortUrlAccess> beanList = new ArrayList<TotalShortUrlAccess>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(TotalShortUrlAccess sb : appBeanList) {
                com.urltally.ws.bean.TotalShortUrlAccessBean bean = convertAppTotalShortUrlAccessBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
