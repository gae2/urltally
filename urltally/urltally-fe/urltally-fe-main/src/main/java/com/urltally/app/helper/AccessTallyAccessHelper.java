package com.urltally.app.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.app.service.AccessTallyMasterAppService;
import com.urltally.app.service.AccessTallyStatusAppService;
import com.urltally.app.service.CumulativeShortUrlAccessAppService;
import com.urltally.app.service.CurrentShortUrlAccessAppService;
import com.urltally.app.service.DailyShortUrlAccessAppService;
import com.urltally.app.service.HourlyShortUrlAccessAppService;
import com.urltally.app.service.MonthlyShortUrlAccessAppService;
import com.urltally.app.service.UserAppService;
import com.urltally.app.service.WeeklyShortUrlAccessAppService;
import com.urltally.app.util.TallyTimeUtil;
import com.urltally.common.TallyStatus;
import com.urltally.common.TallyType;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.BaseException;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.ShortUrlAccess;
import com.urltally.ws.WeeklyShortUrlAccess;


public class AccessTallyAccessHelper
{
    private static final Logger log = Logger.getLogger(AccessTallyAccessHelper.class.getName());

    private AccessTallyAccessHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private AccessTallyMasterAppService tallyMasterAppService = null;
    private AccessTallyStatusAppService tallyStatusAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private AccessTallyMasterAppService getAccessTallyMasterService()
    {
        if(tallyMasterAppService == null) {
            tallyMasterAppService = new AccessTallyMasterAppService();
        }
        return tallyMasterAppService;
    }
    private AccessTallyStatusAppService getAccessTallyStatusService()
    {
        if(tallyStatusAppService == null) {
            tallyStatusAppService = new AccessTallyStatusAppService();
        }
        return tallyStatusAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class AccessTallyAccessHelperHolder
    {
        private static final AccessTallyAccessHelper INSTANCE = new AccessTallyAccessHelper();
    }

    // Singleton method
    public static AccessTallyAccessHelper getInstance()
    {
        return AccessTallyAccessHelperHolder.INSTANCE;
    }

    
    public AccessTallyMaster getAccessTallyMaster(String guid) 
    {
        AccessTallyMaster bean = null;
        try {
            bean = getAccessTallyMasterService().getAccessTallyMaster(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the AccessTallyMaster for guid = " + guid, e);
        }
        return bean;
    }
    
    public AccessTallyStatus getAccessTallyStatus(String guid) 
    {
        AccessTallyStatus bean = null;
        try {
            bean = getAccessTallyStatusService().getAccessTallyStatus(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the AccessTallyStatus for guid = " + guid, e);
        }
        return bean;
    }
    

    
    public AccessTallyMaster getLastAccessTallyMaster(String tallyType) 
    {
        AccessTallyMaster bean = null;
        try {
            String filter = "tallyType == '" + tallyType + "'";
            String ordering = "tallyTime desc";
            //String ordering = "tallyTime asc";   // Note: There could be more than one.... Get the oldest one...  with tallyStatus != completed  ?????
            // Also note that, due to a bug/incorrectness in the algorithm (in the way crons are run and/or taskqueues are used),
            // multiple tallyMasters can be stored (even with the same statuses) at the same time...
            List<AccessTallyMaster> beans = getAccessTallyMasterService().findAccessTallyMasters(filter, ordering, null, null, null, null, 0L, 10);
            if(beans != null && beans.size() > 0) {
            	int size = beans.size();
            	if(size == 1) {
            		bean = beans.get(0);
            	} else {
            		List<AccessTallyMaster> recentTallyMasters = new ArrayList<AccessTallyMaster>();
            		String tallyTime = beans.get(0).getTallyTime();
            		for(AccessTallyMaster b : beans) {
            			if(tallyTime.equals(b.getTallyTime())) {
            				recentTallyMasters.add(b);
            			} else {
            				break;
            			}
            		}
            		int newsize = recentTallyMasters.size();
            		if(newsize == 1) {
            		    // Only one found. Good.
            			bean = recentTallyMasters.get(0);   // == beans.get(0);
            		} else {
            			log.warning("More than one AccessTallyMaster found, total of " + newsize + ", for tallyTime = " + tallyTime);
            			// Pick the most "progressed" one.
            			String topTallyStatus = null;
            			AccessTallyMaster topBean = null; 
                		for(AccessTallyMaster m : recentTallyMasters) {
                			// If any one of them is "completed", just return it....
                			// so that the caller can move on to the next day...
                			if(TallyStatus.isCompleted(m.getTallyStatus())) {
                				log.warning("At least one completed tally master found for tallyTime = " + tallyTime);
                				return m;
                			}
                			// TBD:
                			// if both are started or both are processing, etc... (e.g., compare()==0)
                			// which one to pick???
                			// maybe, based on accessRecordCount (for started) or on procesingStartedTime (for processing), etc..
                			// ?????
                		    if(TallyStatus.compare(topTallyStatus, m.getTallyStatus()) < 0) {
                		        topTallyStatus = m.getTallyStatus();
                		        topBean = m;
                		    }
                		}
                		log.info("Most progressed tally master status = " + topTallyStatus + " for tallyTime = " + tallyTime);
                		bean = topBean;
            		}
            	}
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the AccessTallyMaster for tallyType = " + tallyType, e);
        }
        return bean;
    }

    public AccessTallyMaster constructNewAccessTallyMaster(String tallyType, String dayHour) 
    {
        AccessTallyMasterBean bean = new AccessTallyMasterBean();
        
        //bean.setGuid(GUID.generate());
        bean.setTallyType(tallyType);
        bean.setTallyTime(dayHour);
        bean.setTallyEpoch(TallyTimeUtil.getMilli(dayHour));
        bean.setTallyStatus(TallyStatus.STATUS_INITIALIZED);
        bean.setAccessRecordCount(0);
        bean.setProcesingStartedTime(0L);
        //bean.setCreatedTime(System.currentTimeMillis());
        // ...
        
        try {
            bean = (AccessTallyMasterBean) getAccessTallyMasterService().constructAccessTallyMaster(bean);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to construct a new AccessTallyMaster for tallyType = " + tallyType + " and dayHour = " + dayHour, e);
        }

        return bean;
    }
    
    public Boolean updateAccessTallyMaster(AccessTallyMaster master)
    {
        Boolean suc = null;
        try {
            suc = getAccessTallyMasterService().updateAccessTallyMaster(master);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to update AccessTallyMaster", e);
        }
        return suc;
    }
    
    public AccessTallyMaster refreshAccessTallyMaster(AccessTallyMaster master)
    {
        try {
            master = getAccessTallyMasterService().refreshAccessTallyMaster(master);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to update AccessTallyMaster", e);
        }
        return master;
    }
    
    
    
    // TBD
    public String createAccessTallyStatus(AccessTallyStatus bean)
    {
        String guid = null;
        try {
            guid = getAccessTallyStatusService().createAccessTallyStatus(bean);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create an AccessTallyStatus.", e);
        }
        return guid;
    }
    
    public int createAccessTallyStatuses(List<AccessTallyStatus> beans)
    {
//        int count = -1;
//        try {
//            Integer cnt = getAccessTallyStatusService().createAccessTallyStatuses(beans);
//            if(cnt != null) {
//                count = cnt;
//            }
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to create an AccessTallyStatus.", e);
//        }
//        return count;
        int count = 0;
        if(beans != null) {
        	for(AccessTallyStatus bean : beans) {
		        try {
		            String guid = getAccessTallyStatusService().createAccessTallyStatus(bean);
		            if(guid != null) {
		                count++;
		            }
		        } catch (BaseException e) {
		            log.log(Level.WARNING, "Failed to create an AccessTallyStatus.", e);
		        }
        	}
        }
        return count;
    }
    // excludeDuplicates==true --> Do not include the records with same remoteRecordGuid more than once... 
    public int createAccessTallyStatuses(List<AccessTallyStatus> beans, boolean excludeDuplicates)
    {
    	if(excludeDuplicates == false) {
    		return createAccessTallyStatuses(beans);
    	}
    	// TBD:
    	// tempoary
    	// We are assuming here that {shortUrl, tallyType, tallyTime} is the "primary key".
    	// ...
        int count = 0;
        if(beans != null) {
        	for(AccessTallyStatus bean : beans) {
		        try {
		        	String shortUrl = bean.getShortUrl();
		        	String remoteRecordGuid = bean.getRemoteRecordGuid();
		        	String tallyType = bean.getTallyType();
		        	String tallyTime = bean.getTallyTime();
		        	List<String> keys = getAccessTallyStatusKeysForRemoteRecordGuid(remoteRecordGuid, tallyType);
		        	if(keys == null || keys.isEmpty()) {
		        		String guid = getAccessTallyStatusService().createAccessTallyStatus(bean);
		        		log.fine("New AccessTallyStatus inserted: " + guid + " for shortUrl = " + shortUrl + "; tallyType = " + tallyType + "; tallyTime = " + tallyTime);
		        		count++;
		        	} else {
		        		log.warning("AccessTallyStatus already exists for remoteRecordGuid = " + remoteRecordGuid + "; shortUrl = " + shortUrl + "; tallyType = " + tallyType + "; tallyTime = " + tallyTime);
		        	}
		        } catch (BaseException e) {
		            log.log(Level.WARNING, "Failed to update an AccessTallyStatus.", e);
		        }
        	}
        }
        return count;
    }
    
    public boolean updateAccessTallyStatus(AccessTallyStatus bean)
    {
        boolean suc = false;
        try {
            Boolean result = getAccessTallyStatusService().updateAccessTallyStatus(bean);
            if(Boolean.TRUE.equals(result)) {
                suc = true;
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to update the AccessTallyStatus.", e);
        }
        return suc;
    }

    public int updateAccessTallyStatuses(List<AccessTallyStatus> beans)
    {
        int count = 0;
        if(beans != null) {
        	for(AccessTallyStatus bean : beans) {
		        try {
		            Boolean result = getAccessTallyStatusService().updateAccessTallyStatus(bean);
		            if(Boolean.TRUE.equals(result)) {
		                count++;
		            }
		        } catch (BaseException e) {
		            log.log(Level.WARNING, "Failed to update an AccessTallyStatus.", e);
		        }
        	}
        }
        return count;
    }
//    // excludeDuplicates==true does not make sense for AccessTallyStatus table...
//    private int updateAccessTallyStatuses(List<AccessTallyStatus> beans, boolean excludeDuplicates)
//    {
//    	if(excludeDuplicates == false) {
//    		return updateAccessTallyStatuses(beans);
//    	}
//    	// TBD:
//    	// temporary
//    	// We are assuming here that {shortUrl, tallyType, tallyTime} is the "primary key".
//    	// ...
//        int count = 0;
//        if(beans != null) {
//        	for(AccessTallyStatus bean : beans) {
//		        try {
//                    String shortUrl = bean.getShortUrl();
//                    String remoteRecordGuid = bean.getRemoteRecordGuid();
//                    String tallyType = bean.getTallyType();
//                    String tallyTime = bean.getTallyTime();
//                    //List<String> keys = getAccessTallyStatusKeysForRemoteRecordGuid(remoteRecordGuid, tallyType);
//		        	//if(keys == null || keys.isEmpty()) {
//		        		Boolean suc = getAccessTallyStatusService().updateAccessTallyStatus(bean);
//		        		log.fine("New AccessTallyStatus inserted: " + suc + " for shortUrl + " + "; tallyType = " + tallyType + "; tallyTime = " + tallyTime);
//		        		if(Boolean.TRUE.equals(suc)) {
//		        			count++;
//		        		}
//		        	//} else {
//		        	//	log.warning("AccessTallyStatus already exists for shortUrl + " + shortUrl + "; tallyType = " + tallyType + "; tallyTime = " + tallyTime);
//		        	//}
//		        } catch (BaseException e) {
//		            log.log(Level.WARNING, "Failed to update an AccessTallyStatus.", e);
//		        }
//        	}
//        }
//        return count;
//    }

    
    // This should return no more than 1 record...
    // Note that tallyTime should not be necessary in the filter....
    public List<AccessTallyStatus> getAccessTallyStatusesForRemoteRecordGuid(String remoteRecordGuid, String tallyType)
    {
        return getAccessTallyStatusesForRemoteRecordGuid(remoteRecordGuid, tallyType, null);
    }
    private List<AccessTallyStatus> getAccessTallyStatusesForRemoteRecordGuid(String remoteRecordGuid, String tallyType, String tallyTime)
    {
        log.finer("BEGIN: getAccessTallyStatusesForRemoteRecordGuid()");

        if(remoteRecordGuid == null || remoteRecordGuid.isEmpty()) {
            log.warning("Null or empty remoteRecordGuid.");
            return null;
        }
        if(! TallyType.isValid(tallyType)) {
            // ????
            log.warning("Invalid tallyType = " + tallyType);
            return null;
        }
//        if(! TallyTimeUtil.isValid(tallyTime)) {
//            // ????
//            log.warning("Invalid tallyTime = " + tallyTime);
//            return null;
//        }

        List<AccessTallyStatus> beans = null;
        String filter = "remoteRecordGuid == '" + remoteRecordGuid + "' && tallyType == '" + tallyType + "'";
        // Need to set up DB index in order to be able to support this...
//        if(TallyTimeUtil.isValid(tallyTime)) {
//            filter += " && tallyTime == '" + tallyTime + "'";
//        }
        //String ordering = "createdTime desc";
        String ordering = null;
        try {
            beans = getAccessTallyStatusService().findAccessTallyStatuses(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatus list for remoteRecordGuid = " + remoteRecordGuid, e);
        }
        
        log.finer("END: getAccessTallyStatusesForRemoteRecordGuid()");
        return beans;
    }
    // This should return no more than 1 record...
    // Note that tallyTime should not be necessary in the filter....
    public List<String> getAccessTallyStatusKeysForRemoteRecordGuid(String remoteRecordGuid, String tallyType)
    {
        return getAccessTallyStatusKeysForRemoteRecordGuid(remoteRecordGuid, tallyType, null);
    }
    private List<String> getAccessTallyStatusKeysForRemoteRecordGuid(String remoteRecordGuid, String tallyType, String tallyTime)
    {
        log.finer("BEGIN: getAccessTallyStatusKeysForRemoteRecordGuid()");

        if(remoteRecordGuid == null || remoteRecordGuid.isEmpty()) {
            log.warning("Null or empty remoteRecordGuid.");
            return null;
        }
        if(! TallyType.isValid(tallyType)) {
            // ????
            log.warning("Invalid tallyType = " + tallyType);
            return null;
        }
//        if(! TallyTimeUtil.isValid(tallyTime)) {
//            // ????
//            log.warning("Invalid tallyTime = " + tallyTime);
//            return null;
//        }

        List<String> keys = null;
        String filter = "remoteRecordGuid == '" + remoteRecordGuid + "' && tallyType == '" + tallyType + "'";
        // Need to set up DB index in order to be able to support this...
//        if(TallyTimeUtil.isValid(tallyTime)) {
//            filter += " && tallyTime == '" + tallyTime + "'";
//        }
        //String ordering = "createdTime desc";
        String ordering = null;
        try {
            keys = getAccessTallyStatusService().findAccessTallyStatusKeys(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatus key list for remoteRecordGuid = " + remoteRecordGuid, e);
        }
        
        log.finer("END: getAccessTallyStatusKeysForRemoteRecordGuid()");
        return keys;
    }

    
    // TBD: Do we need this at all???
    // This should return no more than 1 record...
    private List<AccessTallyStatus> getAccessTallyStatusesForShortUrl(String shortUrl, String tallyType, String tallyTime)
    {
        log.finer("BEGIN: getAccessTallyStatusesForShortUrl()");

        if(shortUrl == null || shortUrl.isEmpty()) {
        	log.warning("Null or empty shortUrl.");
        	return null;
        }
        if(! TallyType.isValid(tallyType)) {
            // ????
            log.warning("Invalid tallyType = " + tallyType);
            return null;
        }
        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            log.warning("Invalid tallyTime = " + tallyTime);
            return null;
        }

        List<AccessTallyStatus> beans = null;
        String filter = "shortUrl == '" + shortUrl + "' && tallyType == '" + tallyType + "' && tallyTime == '" + tallyTime + "'";
        //String ordering = "createdTime desc";
        String ordering = null;
        try {
            beans = getAccessTallyStatusService().findAccessTallyStatuses(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatus list for shortUrl = " + shortUrl, e);
        }
        
        log.finer("END: getAccessTallyStatusesForShortUrl()");
        return beans;
    }
    // TBD: Do we need this at all???
    // This should return no more than 1 record...
    private List<String> getAccessTallyStatusKeysForShortUrl(String shortUrl, String tallyType, String tallyTime)
    {
        log.finer("BEGIN: getAccessTallyStatusKeysForShortUrl()");

        if(shortUrl == null || shortUrl.isEmpty()) {
        	log.warning("Null or empty shortUrl.");
        	return null;
        }
        if(! TallyType.isValid(tallyType)) {
            // ????
            log.warning("Invalid tallyType = " + tallyType);
            return null;
        }
        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            log.warning("Invalid tallyTime = " + tallyTime);
            return null;
        }

        List<String> keys = null;
        String filter = "shortUrl == '" + shortUrl + "' && tallyType == '" + tallyType + "' && tallyTime == '" + tallyTime + "'";
        //String ordering = "createdTime desc";
        String ordering = null;
        try {
            keys = getAccessTallyStatusService().findAccessTallyStatusKeys(filter, ordering, null, null, null, null, null, null);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatus key list for shortUrl = " + shortUrl, e);
        }
        
        log.finer("END: getAccessTallyStatusKeysForShortUrl()");
        return keys;
    }

    
    public List<AccessTallyStatus> getAccessTallyStatusesForProcessing(String tallyType, String tallyTime, Integer maxCount)
    {
        log.finer("BEGIN: getAccessTallyStatusesForProcessing()");
        //log.warning("BEGIN: getAccessTallyStatusesForProcessing()");

        List<AccessTallyStatus> beans = null;

        if(! TallyType.isValid(tallyType)) {
            // ????
            log.warning("Invalid tallyType = " + tallyType);
            return null;
        }
        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            log.warning("Invalid tallyTime = " + tallyTime);
            return null;
        }

        String filter = "tallyType == '" + tallyType + "' && tallyTime == '" + tallyTime + "' && processed == false";     // || processed==null ???
        //String ordering = "createdTime desc";
        String ordering = null;
        try {
            beans = getAccessTallyStatusService().findAccessTallyStatuses(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans == null) {
                // ????
                beans = new ArrayList<AccessTallyStatus>();
                // ????
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatus list for processing.", e);
        }
        
        log.finer("END: getAccessTallyStatusesForProcessing()");
        return beans;
    }
    

    public List<AccessTallyStatus> getAccessTallyStatusesForDeletion(String tallyType, String tallyTime, Integer maxCount)
    {
        List<AccessTallyStatus> beans = null;

        if(! TallyType.isValid(tallyType)) {
            // ????
            return null;
        }
        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            return null;
        }

        //String filter = "tallyType == '" + tallyType + "' && tallyTime == '" + tallyTime + "'";
        String filter = "tallyType == '" + tallyType + "' && tallyTime <= '" + tallyTime + "'";
        //String ordering = "createdTime desc";
        String ordering = null;
        try {
            beans = getAccessTallyStatusService().findAccessTallyStatuses(filter, ordering, null, null, null, null, 0L, maxCount);
            // ????
            if(beans == null) {
                beans = new ArrayList<AccessTallyStatus>();  // ????
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatus list for deletion.", e);
        }
        
        return beans;
    }
    // temporary  --> Deletion using keys (not guids) do not seem to work...  
    //                Need investigation (on the backend DAO implementation)
    private List<String> getAccessTallyStatusKeysForDeletion(String tallyType, String tallyTime, Integer maxCount)
    {
        List<String> keys = null;

        if(! TallyType.isValid(tallyType)) {
            // ????
            return null;
        }
        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            return null;
        }

        //String filter = "tallyType == '" + tallyType + "' && tallyTime == '" + tallyTime + "'";
        String filter = "tallyType == '" + tallyType + "' && tallyTime <= '" + tallyTime + "'";
        //String ordering = "createdTime desc";
        String ordering = null;
        try {
            keys = getAccessTallyStatusService().findAccessTallyStatusKeys(filter, ordering, null, null, null, null, 0L, maxCount);
            // ????
            if(keys == null) {
                keys = new ArrayList<String>();  // ????
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatus list for deletion.", e);
        }
        
        return keys;
    }
    
    public boolean deleteAccessTallyStatus(String guid)
    {
        Boolean suc = null;
        try {
            suc = getAccessTallyStatusService().deleteAccessTallyStatus(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to delete AccessTallyStatus for guid = " + guid, e);
        }
        if(Boolean.TRUE.equals(suc)) {
            return true;
        } else {
            return false;
        }
    }
    
    

}
