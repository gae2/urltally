package com.urltally.app.tally;

import java.util.logging.Logger;


// Utility class to be used as a key for AccessRecord map...
public class AccessTallyKey
{
    private static final Logger log = Logger.getLogger(AccessTallyKey.class.getName());

    // Keys.
    private String redirectType;
    private String refererDomain;
    private String userAgent;
    private String language;
    private String country;

    // These are not part of keys.
    // Not to be included in hashCode(), equals(), etc...
    private String longUrl;
    // etc...

    
    public AccessTallyKey(String redirectType, String refererDomain,
            String userAgent, String language, String country)
    {
        this.redirectType = redirectType;
        this.refererDomain = refererDomain;
        this.userAgent = userAgent;
        this.language = language;
        this.country = country;
    }

    
    public String getRedirectType()
    {
        return redirectType;
    }

    public String getRefererDomain()
    {
        return refererDomain;
    }

    public String getUserAgent()
    {
        return userAgent;
    }

    public String getLanguage()
    {
        return language;
    }

    public String getCountry()
    {
        return country;
    }


    public String getLongUrl()
    {
        return longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }


    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result
                + ((language == null) ? 0 : language.hashCode());
        result = prime * result
                + ((redirectType == null) ? 0 : redirectType.hashCode());
        result = prime * result
                + ((refererDomain == null) ? 0 : refererDomain.hashCode());
        result = prime * result
                + ((userAgent == null) ? 0 : userAgent.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccessTallyKey other = (AccessTallyKey) obj;
        if (country == null) {
            if (other.country != null)
                return false;
        } else if (!country.equals(other.country))
            return false;
        if (language == null) {
            if (other.language != null)
                return false;
        } else if (!language.equals(other.language))
            return false;
        if (redirectType == null) {
            if (other.redirectType != null)
                return false;
        } else if (!redirectType.equals(other.redirectType))
            return false;
        if (refererDomain == null) {
            if (other.refererDomain != null)
                return false;
        } else if (!refererDomain.equals(other.refererDomain))
            return false;
        if (userAgent == null) {
            if (other.userAgent != null)
                return false;
        } else if (!userAgent.equals(other.userAgent))
            return false;
        return true;
    }


    @Override
    public String toString()
    {
        return "AccessTallyKey [redirectType=" + redirectType
                + ", refererDomain=" + refererDomain + ", userAgent="
                + userAgent + ", language=" + language + ", country=" + country
                + ", longUrl=" + longUrl + "]";
    }


}
