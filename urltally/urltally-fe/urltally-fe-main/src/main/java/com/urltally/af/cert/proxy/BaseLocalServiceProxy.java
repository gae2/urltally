package com.urltally.af.cert.proxy;

import com.urltally.ws.cert.service.PublicCertificateInfoService;


// TBD: How to best inject the service instances?
public abstract class BaseLocalServiceProxy
{
    private PublicCertificateInfoService publicCertificateInfoService;

    public BaseLocalServiceProxy()
    {
        this(null);
    }
    public BaseLocalServiceProxy(PublicCertificateInfoService publicCertificateInfoService)
    {
        this.publicCertificateInfoService = publicCertificateInfoService;
    }
    
    // Inject dependencies.
    public void setPublicCertificateInfoService(PublicCertificateInfoService publicCertificateInfoService)
    {
        this.publicCertificateInfoService = publicCertificateInfoService;
    }

    // Returns a PublicCertificateInfoService instance.
    public PublicCertificateInfoService getPublicCertificateInfoService() 
    {
        return publicCertificateInfoService;
    }

}
