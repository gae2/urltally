package com.urltally.af.service.dummy;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.service.AbstractServiceFactory;
import com.urltally.af.service.ApiConsumerService;
import com.urltally.af.service.UserService;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.af.service.ServiceInfoService;
import com.urltally.af.service.FiveTenService;


// The primary purpose of a dummy service is to fake the service api.
// The caller/client can use the same API as other abstract factory-based services,
// but the dummy services do not do anything.
public class DummyServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(DummyServiceFactory.class.getName());

    private DummyServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class DummyServiceFactoryHolder
    {
        private static final DummyServiceFactory INSTANCE = new DummyServiceFactory();
    }

    // Singleton method
    public static DummyServiceFactory getInstance()
    {
        return DummyServiceFactoryHolder.INSTANCE;
    }


    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerDummyService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserDummyService();
    }

    @Override
    public AccessTallyMasterService getAccessTallyMasterService()
    {
        return new AccessTallyMasterDummyService();
    }

    @Override
    public AccessTallyStatusService getAccessTallyStatusService()
    {
        return new AccessTallyStatusDummyService();
    }

    @Override
    public MonthlyShortUrlAccessService getMonthlyShortUrlAccessService()
    {
        return new MonthlyShortUrlAccessDummyService();
    }

    @Override
    public WeeklyShortUrlAccessService getWeeklyShortUrlAccessService()
    {
        return new WeeklyShortUrlAccessDummyService();
    }

    @Override
    public DailyShortUrlAccessService getDailyShortUrlAccessService()
    {
        return new DailyShortUrlAccessDummyService();
    }

    @Override
    public HourlyShortUrlAccessService getHourlyShortUrlAccessService()
    {
        return new HourlyShortUrlAccessDummyService();
    }

    @Override
    public CumulativeShortUrlAccessService getCumulativeShortUrlAccessService()
    {
        return new CumulativeShortUrlAccessDummyService();
    }

    @Override
    public CurrentShortUrlAccessService getCurrentShortUrlAccessService()
    {
        return new CurrentShortUrlAccessDummyService();
    }

    @Override
    public TotalShortUrlAccessService getTotalShortUrlAccessService()
    {
        return new TotalShortUrlAccessDummyService();
    }

    @Override
    public TotalLongUrlAccessService getTotalLongUrlAccessService()
    {
        return new TotalLongUrlAccessDummyService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoDummyService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenDummyService();
    }


}
