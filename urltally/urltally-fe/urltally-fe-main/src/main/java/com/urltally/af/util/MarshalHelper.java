package com.urltally.af.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.core.StringCursor;
import com.urltally.ws.KeyValuePairStruct;
import com.urltally.ws.KeyValueRelationStruct;
import com.urltally.ws.GeoPointStruct;
import com.urltally.ws.GeoCoordinateStruct;
import com.urltally.ws.CellLatitudeLongitude;
import com.urltally.ws.StreetAddressStruct;
import com.urltally.ws.FullNameStruct;
import com.urltally.ws.ReferrerInfoStruct;
import com.urltally.ws.GaeAppStruct;
import com.urltally.ws.GaeUserStruct;
import com.urltally.ws.ApiConsumer;
import com.urltally.ws.User;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.ShortUrlAccess;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.ws.ServiceInfo;
import com.urltally.ws.FiveTen;
import com.urltally.ws.stub.KeyValuePairStructStub;
import com.urltally.ws.stub.KeyValuePairStructListStub;
import com.urltally.ws.stub.KeyValueRelationStructStub;
import com.urltally.ws.stub.KeyValueRelationStructListStub;
import com.urltally.ws.stub.GeoPointStructStub;
import com.urltally.ws.stub.GeoPointStructListStub;
import com.urltally.ws.stub.GeoCoordinateStructStub;
import com.urltally.ws.stub.GeoCoordinateStructListStub;
import com.urltally.ws.stub.CellLatitudeLongitudeStub;
import com.urltally.ws.stub.CellLatitudeLongitudeListStub;
import com.urltally.ws.stub.StreetAddressStructStub;
import com.urltally.ws.stub.StreetAddressStructListStub;
import com.urltally.ws.stub.FullNameStructStub;
import com.urltally.ws.stub.FullNameStructListStub;
import com.urltally.ws.stub.ReferrerInfoStructStub;
import com.urltally.ws.stub.ReferrerInfoStructListStub;
import com.urltally.ws.stub.GaeAppStructStub;
import com.urltally.ws.stub.GaeAppStructListStub;
import com.urltally.ws.stub.GaeUserStructStub;
import com.urltally.ws.stub.GaeUserStructListStub;
import com.urltally.ws.stub.ApiConsumerStub;
import com.urltally.ws.stub.ApiConsumerListStub;
import com.urltally.ws.stub.UserStub;
import com.urltally.ws.stub.UserListStub;
import com.urltally.ws.stub.AccessTallyMasterStub;
import com.urltally.ws.stub.AccessTallyMasterListStub;
import com.urltally.ws.stub.AccessTallyStatusStub;
import com.urltally.ws.stub.AccessTallyStatusListStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessListStub;
import com.urltally.ws.stub.WeeklyShortUrlAccessStub;
import com.urltally.ws.stub.WeeklyShortUrlAccessListStub;
import com.urltally.ws.stub.DailyShortUrlAccessStub;
import com.urltally.ws.stub.DailyShortUrlAccessListStub;
import com.urltally.ws.stub.HourlyShortUrlAccessStub;
import com.urltally.ws.stub.HourlyShortUrlAccessListStub;
import com.urltally.ws.stub.CumulativeShortUrlAccessStub;
import com.urltally.ws.stub.CumulativeShortUrlAccessListStub;
import com.urltally.ws.stub.CurrentShortUrlAccessStub;
import com.urltally.ws.stub.CurrentShortUrlAccessListStub;
import com.urltally.ws.stub.TotalShortUrlAccessStub;
import com.urltally.ws.stub.TotalShortUrlAccessListStub;
import com.urltally.ws.stub.TotalLongUrlAccessStub;
import com.urltally.ws.stub.TotalLongUrlAccessListStub;
import com.urltally.ws.stub.ServiceInfoStub;
import com.urltally.ws.stub.ServiceInfoListStub;
import com.urltally.ws.stub.FiveTenStub;
import com.urltally.ws.stub.FiveTenListStub;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.GaeUserStructBean;
import com.urltally.af.bean.ApiConsumerBean;
import com.urltally.af.bean.UserBean;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.af.bean.ShortUrlAccessBean;
import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.bean.CumulativeShortUrlAccessBean;
import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.bean.TotalLongUrlAccessBean;
import com.urltally.af.bean.ServiceInfoBean;
import com.urltally.af.bean.FiveTenBean;


public final class MarshalHelper
{
    private static final Logger log = Logger.getLogger(MarshalHelper.class.getName());

    // Static methods only.
    private MarshalHelper() {}

    // temporary
    public static KeyValuePairStructBean convertKeyValuePairStructToBean(KeyValuePairStruct stub)
    {
        KeyValuePairStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValuePairStructBean();
        } else if(stub instanceof KeyValuePairStructBean) {
        	bean = (KeyValuePairStructBean) stub;
        } else {
        	bean = new KeyValuePairStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValuePairStructBean> convertKeyValuePairStructListToBeanList(List<KeyValuePairStruct> stubList)
    {
        List<KeyValuePairStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStructBean>();
            for(KeyValuePairStruct stub : stubList) {
                beanList.add(convertKeyValuePairStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValuePairStruct> convertKeyValuePairStructListStubToBeanList(KeyValuePairStructListStub listStub)
    {
        return convertKeyValuePairStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<KeyValuePairStruct> convertKeyValuePairStructListStubToBeanList(KeyValuePairStructListStub listStub, StringCursor forwardCursor)
    {
        List<KeyValuePairStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStruct>();
            for(KeyValuePairStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValuePairStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValuePairStructStub convertKeyValuePairStructToStub(KeyValuePairStruct bean)
    {
        KeyValuePairStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValuePairStructStub();
        } else if(bean instanceof KeyValuePairStructStub) {
            stub = (KeyValuePairStructStub) bean;
        } else if(bean instanceof KeyValuePairStructBean && ((KeyValuePairStructBean) bean).isWrapper()) {
            stub = ((KeyValuePairStructBean) bean).getStub();
        } else {
            stub = KeyValuePairStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static KeyValueRelationStructBean convertKeyValueRelationStructToBean(KeyValueRelationStruct stub)
    {
        KeyValueRelationStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValueRelationStructBean();
        } else if(stub instanceof KeyValueRelationStructBean) {
        	bean = (KeyValueRelationStructBean) stub;
        } else {
        	bean = new KeyValueRelationStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValueRelationStructBean> convertKeyValueRelationStructListToBeanList(List<KeyValueRelationStruct> stubList)
    {
        List<KeyValueRelationStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStructBean>();
            for(KeyValueRelationStruct stub : stubList) {
                beanList.add(convertKeyValueRelationStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValueRelationStruct> convertKeyValueRelationStructListStubToBeanList(KeyValueRelationStructListStub listStub)
    {
        return convertKeyValueRelationStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<KeyValueRelationStruct> convertKeyValueRelationStructListStubToBeanList(KeyValueRelationStructListStub listStub, StringCursor forwardCursor)
    {
        List<KeyValueRelationStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStruct>();
            for(KeyValueRelationStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValueRelationStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValueRelationStructStub convertKeyValueRelationStructToStub(KeyValueRelationStruct bean)
    {
        KeyValueRelationStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValueRelationStructStub();
        } else if(bean instanceof KeyValueRelationStructStub) {
            stub = (KeyValueRelationStructStub) bean;
        } else if(bean instanceof KeyValueRelationStructBean && ((KeyValueRelationStructBean) bean).isWrapper()) {
            stub = ((KeyValueRelationStructBean) bean).getStub();
        } else {
            stub = KeyValueRelationStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GeoPointStructBean convertGeoPointStructToBean(GeoPointStruct stub)
    {
        GeoPointStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GeoPointStructBean();
        } else if(stub instanceof GeoPointStructBean) {
        	bean = (GeoPointStructBean) stub;
        } else {
        	bean = new GeoPointStructBean(stub);
        }
        return bean;
    }
    public static List<GeoPointStructBean> convertGeoPointStructListToBeanList(List<GeoPointStruct> stubList)
    {
        List<GeoPointStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GeoPointStruct>();
        } else {
            beanList = new ArrayList<GeoPointStructBean>();
            for(GeoPointStruct stub : stubList) {
                beanList.add(convertGeoPointStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GeoPointStruct> convertGeoPointStructListStubToBeanList(GeoPointStructListStub listStub)
    {
        return convertGeoPointStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GeoPointStruct> convertGeoPointStructListStubToBeanList(GeoPointStructListStub listStub, StringCursor forwardCursor)
    {
        List<GeoPointStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GeoPointStruct>();
        } else {
            beanList = new ArrayList<GeoPointStruct>();
            for(GeoPointStructStub stub : listStub.getList()) {
            	beanList.add(convertGeoPointStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GeoPointStructStub convertGeoPointStructToStub(GeoPointStruct bean)
    {
        GeoPointStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GeoPointStructStub();
        } else if(bean instanceof GeoPointStructStub) {
            stub = (GeoPointStructStub) bean;
        } else if(bean instanceof GeoPointStructBean && ((GeoPointStructBean) bean).isWrapper()) {
            stub = ((GeoPointStructBean) bean).getStub();
        } else {
            stub = GeoPointStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GeoCoordinateStructBean convertGeoCoordinateStructToBean(GeoCoordinateStruct stub)
    {
        GeoCoordinateStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GeoCoordinateStructBean();
        } else if(stub instanceof GeoCoordinateStructBean) {
        	bean = (GeoCoordinateStructBean) stub;
        } else {
        	bean = new GeoCoordinateStructBean(stub);
        }
        return bean;
    }
    public static List<GeoCoordinateStructBean> convertGeoCoordinateStructListToBeanList(List<GeoCoordinateStruct> stubList)
    {
        List<GeoCoordinateStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GeoCoordinateStruct>();
        } else {
            beanList = new ArrayList<GeoCoordinateStructBean>();
            for(GeoCoordinateStruct stub : stubList) {
                beanList.add(convertGeoCoordinateStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GeoCoordinateStruct> convertGeoCoordinateStructListStubToBeanList(GeoCoordinateStructListStub listStub)
    {
        return convertGeoCoordinateStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GeoCoordinateStruct> convertGeoCoordinateStructListStubToBeanList(GeoCoordinateStructListStub listStub, StringCursor forwardCursor)
    {
        List<GeoCoordinateStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GeoCoordinateStruct>();
        } else {
            beanList = new ArrayList<GeoCoordinateStruct>();
            for(GeoCoordinateStructStub stub : listStub.getList()) {
            	beanList.add(convertGeoCoordinateStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GeoCoordinateStructStub convertGeoCoordinateStructToStub(GeoCoordinateStruct bean)
    {
        GeoCoordinateStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GeoCoordinateStructStub();
        } else if(bean instanceof GeoCoordinateStructStub) {
            stub = (GeoCoordinateStructStub) bean;
        } else if(bean instanceof GeoCoordinateStructBean && ((GeoCoordinateStructBean) bean).isWrapper()) {
            stub = ((GeoCoordinateStructBean) bean).getStub();
        } else {
            stub = GeoCoordinateStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static CellLatitudeLongitudeBean convertCellLatitudeLongitudeToBean(CellLatitudeLongitude stub)
    {
        CellLatitudeLongitudeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new CellLatitudeLongitudeBean();
        } else if(stub instanceof CellLatitudeLongitudeBean) {
        	bean = (CellLatitudeLongitudeBean) stub;
        } else {
        	bean = new CellLatitudeLongitudeBean(stub);
        }
        return bean;
    }
    public static List<CellLatitudeLongitudeBean> convertCellLatitudeLongitudeListToBeanList(List<CellLatitudeLongitude> stubList)
    {
        List<CellLatitudeLongitudeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<CellLatitudeLongitude>();
        } else {
            beanList = new ArrayList<CellLatitudeLongitudeBean>();
            for(CellLatitudeLongitude stub : stubList) {
                beanList.add(convertCellLatitudeLongitudeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<CellLatitudeLongitude> convertCellLatitudeLongitudeListStubToBeanList(CellLatitudeLongitudeListStub listStub)
    {
        return convertCellLatitudeLongitudeListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<CellLatitudeLongitude> convertCellLatitudeLongitudeListStubToBeanList(CellLatitudeLongitudeListStub listStub, StringCursor forwardCursor)
    {
        List<CellLatitudeLongitude> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<CellLatitudeLongitude>();
        } else {
            beanList = new ArrayList<CellLatitudeLongitude>();
            for(CellLatitudeLongitudeStub stub : listStub.getList()) {
            	beanList.add(convertCellLatitudeLongitudeToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static CellLatitudeLongitudeStub convertCellLatitudeLongitudeToStub(CellLatitudeLongitude bean)
    {
        CellLatitudeLongitudeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new CellLatitudeLongitudeStub();
        } else if(bean instanceof CellLatitudeLongitudeStub) {
            stub = (CellLatitudeLongitudeStub) bean;
        } else if(bean instanceof CellLatitudeLongitudeBean && ((CellLatitudeLongitudeBean) bean).isWrapper()) {
            stub = ((CellLatitudeLongitudeBean) bean).getStub();
        } else {
            stub = CellLatitudeLongitudeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static StreetAddressStructBean convertStreetAddressStructToBean(StreetAddressStruct stub)
    {
        StreetAddressStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new StreetAddressStructBean();
        } else if(stub instanceof StreetAddressStructBean) {
        	bean = (StreetAddressStructBean) stub;
        } else {
        	bean = new StreetAddressStructBean(stub);
        }
        return bean;
    }
    public static List<StreetAddressStructBean> convertStreetAddressStructListToBeanList(List<StreetAddressStruct> stubList)
    {
        List<StreetAddressStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<StreetAddressStruct>();
        } else {
            beanList = new ArrayList<StreetAddressStructBean>();
            for(StreetAddressStruct stub : stubList) {
                beanList.add(convertStreetAddressStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<StreetAddressStruct> convertStreetAddressStructListStubToBeanList(StreetAddressStructListStub listStub)
    {
        return convertStreetAddressStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<StreetAddressStruct> convertStreetAddressStructListStubToBeanList(StreetAddressStructListStub listStub, StringCursor forwardCursor)
    {
        List<StreetAddressStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<StreetAddressStruct>();
        } else {
            beanList = new ArrayList<StreetAddressStruct>();
            for(StreetAddressStructStub stub : listStub.getList()) {
            	beanList.add(convertStreetAddressStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static StreetAddressStructStub convertStreetAddressStructToStub(StreetAddressStruct bean)
    {
        StreetAddressStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new StreetAddressStructStub();
        } else if(bean instanceof StreetAddressStructStub) {
            stub = (StreetAddressStructStub) bean;
        } else if(bean instanceof StreetAddressStructBean && ((StreetAddressStructBean) bean).isWrapper()) {
            stub = ((StreetAddressStructBean) bean).getStub();
        } else {
            stub = StreetAddressStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FullNameStructBean convertFullNameStructToBean(FullNameStruct stub)
    {
        FullNameStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FullNameStructBean();
        } else if(stub instanceof FullNameStructBean) {
        	bean = (FullNameStructBean) stub;
        } else {
        	bean = new FullNameStructBean(stub);
        }
        return bean;
    }
    public static List<FullNameStructBean> convertFullNameStructListToBeanList(List<FullNameStruct> stubList)
    {
        List<FullNameStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FullNameStruct>();
        } else {
            beanList = new ArrayList<FullNameStructBean>();
            for(FullNameStruct stub : stubList) {
                beanList.add(convertFullNameStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FullNameStruct> convertFullNameStructListStubToBeanList(FullNameStructListStub listStub)
    {
        return convertFullNameStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FullNameStruct> convertFullNameStructListStubToBeanList(FullNameStructListStub listStub, StringCursor forwardCursor)
    {
        List<FullNameStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FullNameStruct>();
        } else {
            beanList = new ArrayList<FullNameStruct>();
            for(FullNameStructStub stub : listStub.getList()) {
            	beanList.add(convertFullNameStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FullNameStructStub convertFullNameStructToStub(FullNameStruct bean)
    {
        FullNameStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FullNameStructStub();
        } else if(bean instanceof FullNameStructStub) {
            stub = (FullNameStructStub) bean;
        } else if(bean instanceof FullNameStructBean && ((FullNameStructBean) bean).isWrapper()) {
            stub = ((FullNameStructBean) bean).getStub();
        } else {
            stub = FullNameStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ReferrerInfoStructBean convertReferrerInfoStructToBean(ReferrerInfoStruct stub)
    {
        ReferrerInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ReferrerInfoStructBean();
        } else if(stub instanceof ReferrerInfoStructBean) {
        	bean = (ReferrerInfoStructBean) stub;
        } else {
        	bean = new ReferrerInfoStructBean(stub);
        }
        return bean;
    }
    public static List<ReferrerInfoStructBean> convertReferrerInfoStructListToBeanList(List<ReferrerInfoStruct> stubList)
    {
        List<ReferrerInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStructBean>();
            for(ReferrerInfoStruct stub : stubList) {
                beanList.add(convertReferrerInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub)
    {
        return convertReferrerInfoStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub, StringCursor forwardCursor)
    {
        List<ReferrerInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStruct>();
            for(ReferrerInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertReferrerInfoStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ReferrerInfoStructStub convertReferrerInfoStructToStub(ReferrerInfoStruct bean)
    {
        ReferrerInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ReferrerInfoStructStub();
        } else if(bean instanceof ReferrerInfoStructStub) {
            stub = (ReferrerInfoStructStub) bean;
        } else if(bean instanceof ReferrerInfoStructBean && ((ReferrerInfoStructBean) bean).isWrapper()) {
            stub = ((ReferrerInfoStructBean) bean).getStub();
        } else {
            stub = ReferrerInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeAppStructBean convertGaeAppStructToBean(GaeAppStruct stub)
    {
        GaeAppStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeAppStructBean();
        } else if(stub instanceof GaeAppStructBean) {
        	bean = (GaeAppStructBean) stub;
        } else {
        	bean = new GaeAppStructBean(stub);
        }
        return bean;
    }
    public static List<GaeAppStructBean> convertGaeAppStructListToBeanList(List<GaeAppStruct> stubList)
    {
        List<GaeAppStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStructBean>();
            for(GaeAppStruct stub : stubList) {
                beanList.add(convertGaeAppStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub)
    {
        return convertGaeAppStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub, StringCursor forwardCursor)
    {
        List<GaeAppStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStruct>();
            for(GaeAppStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeAppStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GaeAppStructStub convertGaeAppStructToStub(GaeAppStruct bean)
    {
        GaeAppStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeAppStructStub();
        } else if(bean instanceof GaeAppStructStub) {
            stub = (GaeAppStructStub) bean;
        } else if(bean instanceof GaeAppStructBean && ((GaeAppStructBean) bean).isWrapper()) {
            stub = ((GaeAppStructBean) bean).getStub();
        } else {
            stub = GaeAppStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeUserStructBean convertGaeUserStructToBean(GaeUserStruct stub)
    {
        GaeUserStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeUserStructBean();
        } else if(stub instanceof GaeUserStructBean) {
        	bean = (GaeUserStructBean) stub;
        } else {
        	bean = new GaeUserStructBean(stub);
        }
        return bean;
    }
    public static List<GaeUserStructBean> convertGaeUserStructListToBeanList(List<GaeUserStruct> stubList)
    {
        List<GaeUserStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStructBean>();
            for(GaeUserStruct stub : stubList) {
                beanList.add(convertGaeUserStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub)
    {
        return convertGaeUserStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub, StringCursor forwardCursor)
    {
        List<GaeUserStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStruct>();
            for(GaeUserStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeUserStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GaeUserStructStub convertGaeUserStructToStub(GaeUserStruct bean)
    {
        GaeUserStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeUserStructStub();
        } else if(bean instanceof GaeUserStructStub) {
            stub = (GaeUserStructStub) bean;
        } else if(bean instanceof GaeUserStructBean && ((GaeUserStructBean) bean).isWrapper()) {
            stub = ((GaeUserStructBean) bean).getStub();
        } else {
            stub = GaeUserStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ApiConsumerBean convertApiConsumerToBean(ApiConsumer stub)
    {
        ApiConsumerBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ApiConsumerBean();
        } else if(stub instanceof ApiConsumerBean) {
        	bean = (ApiConsumerBean) stub;
        } else {
        	bean = new ApiConsumerBean(stub);
        }
        return bean;
    }
    public static List<ApiConsumerBean> convertApiConsumerListToBeanList(List<ApiConsumer> stubList)
    {
        List<ApiConsumerBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumerBean>();
            for(ApiConsumer stub : stubList) {
                beanList.add(convertApiConsumerToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub)
    {
        return convertApiConsumerListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub, StringCursor forwardCursor)
    {
        List<ApiConsumer> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumer>();
            for(ApiConsumerStub stub : listStub.getList()) {
            	beanList.add(convertApiConsumerToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ApiConsumerStub convertApiConsumerToStub(ApiConsumer bean)
    {
        ApiConsumerStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ApiConsumerStub();
        } else if(bean instanceof ApiConsumerStub) {
            stub = (ApiConsumerStub) bean;
        } else if(bean instanceof ApiConsumerBean && ((ApiConsumerBean) bean).isWrapper()) {
            stub = ((ApiConsumerBean) bean).getStub();
        } else {
            stub = ApiConsumerStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserBean convertUserToBean(User stub)
    {
        UserBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserBean();
        } else if(stub instanceof UserBean) {
        	bean = (UserBean) stub;
        } else {
        	bean = new UserBean(stub);
        }
        return bean;
    }
    public static List<UserBean> convertUserListToBeanList(List<User> stubList)
    {
        List<UserBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<UserBean>();
            for(User stub : stubList) {
                beanList.add(convertUserToBean(stub));
            }
        }
        return beanList;
    }
    public static List<User> convertUserListStubToBeanList(UserListStub listStub)
    {
        return convertUserListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<User> convertUserListStubToBeanList(UserListStub listStub, StringCursor forwardCursor)
    {
        List<User> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<User>();
            for(UserStub stub : listStub.getList()) {
            	beanList.add(convertUserToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UserStub convertUserToStub(User bean)
    {
        UserStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserStub();
        } else if(bean instanceof UserStub) {
            stub = (UserStub) bean;
        } else if(bean instanceof UserBean && ((UserBean) bean).isWrapper()) {
            stub = ((UserBean) bean).getStub();
        } else {
            stub = UserStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AccessTallyMasterBean convertAccessTallyMasterToBean(AccessTallyMaster stub)
    {
        AccessTallyMasterBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AccessTallyMasterBean();
        } else if(stub instanceof AccessTallyMasterBean) {
        	bean = (AccessTallyMasterBean) stub;
        } else {
        	bean = new AccessTallyMasterBean(stub);
        }
        return bean;
    }
    public static List<AccessTallyMasterBean> convertAccessTallyMasterListToBeanList(List<AccessTallyMaster> stubList)
    {
        List<AccessTallyMasterBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AccessTallyMaster>();
        } else {
            beanList = new ArrayList<AccessTallyMasterBean>();
            for(AccessTallyMaster stub : stubList) {
                beanList.add(convertAccessTallyMasterToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AccessTallyMaster> convertAccessTallyMasterListStubToBeanList(AccessTallyMasterListStub listStub)
    {
        return convertAccessTallyMasterListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<AccessTallyMaster> convertAccessTallyMasterListStubToBeanList(AccessTallyMasterListStub listStub, StringCursor forwardCursor)
    {
        List<AccessTallyMaster> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AccessTallyMaster>();
        } else {
            beanList = new ArrayList<AccessTallyMaster>();
            for(AccessTallyMasterStub stub : listStub.getList()) {
            	beanList.add(convertAccessTallyMasterToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static AccessTallyMasterStub convertAccessTallyMasterToStub(AccessTallyMaster bean)
    {
        AccessTallyMasterStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AccessTallyMasterStub();
        } else if(bean instanceof AccessTallyMasterStub) {
            stub = (AccessTallyMasterStub) bean;
        } else if(bean instanceof AccessTallyMasterBean && ((AccessTallyMasterBean) bean).isWrapper()) {
            stub = ((AccessTallyMasterBean) bean).getStub();
        } else {
            stub = AccessTallyMasterStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AccessTallyStatusBean convertAccessTallyStatusToBean(AccessTallyStatus stub)
    {
        AccessTallyStatusBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AccessTallyStatusBean();
        } else if(stub instanceof AccessTallyStatusBean) {
        	bean = (AccessTallyStatusBean) stub;
        } else {
        	bean = new AccessTallyStatusBean(stub);
        }
        return bean;
    }
    public static List<AccessTallyStatusBean> convertAccessTallyStatusListToBeanList(List<AccessTallyStatus> stubList)
    {
        List<AccessTallyStatusBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AccessTallyStatus>();
        } else {
            beanList = new ArrayList<AccessTallyStatusBean>();
            for(AccessTallyStatus stub : stubList) {
                beanList.add(convertAccessTallyStatusToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AccessTallyStatus> convertAccessTallyStatusListStubToBeanList(AccessTallyStatusListStub listStub)
    {
        return convertAccessTallyStatusListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<AccessTallyStatus> convertAccessTallyStatusListStubToBeanList(AccessTallyStatusListStub listStub, StringCursor forwardCursor)
    {
        List<AccessTallyStatus> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AccessTallyStatus>();
        } else {
            beanList = new ArrayList<AccessTallyStatus>();
            for(AccessTallyStatusStub stub : listStub.getList()) {
            	beanList.add(convertAccessTallyStatusToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static AccessTallyStatusStub convertAccessTallyStatusToStub(AccessTallyStatus bean)
    {
        AccessTallyStatusStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AccessTallyStatusStub();
        } else if(bean instanceof AccessTallyStatusStub) {
            stub = (AccessTallyStatusStub) bean;
        } else if(bean instanceof AccessTallyStatusBean && ((AccessTallyStatusBean) bean).isWrapper()) {
            stub = ((AccessTallyStatusBean) bean).getStub();
        } else {
            stub = AccessTallyStatusStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static MonthlyShortUrlAccessBean convertMonthlyShortUrlAccessToBean(MonthlyShortUrlAccess stub)
    {
        MonthlyShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new MonthlyShortUrlAccessBean();
        } else if(stub instanceof MonthlyShortUrlAccessBean) {
        	bean = (MonthlyShortUrlAccessBean) stub;
        } else {
        	bean = new MonthlyShortUrlAccessBean(stub);
        }
        return bean;
    }
    public static List<MonthlyShortUrlAccessBean> convertMonthlyShortUrlAccessListToBeanList(List<MonthlyShortUrlAccess> stubList)
    {
        List<MonthlyShortUrlAccessBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<MonthlyShortUrlAccess>();
        } else {
            beanList = new ArrayList<MonthlyShortUrlAccessBean>();
            for(MonthlyShortUrlAccess stub : stubList) {
                beanList.add(convertMonthlyShortUrlAccessToBean(stub));
            }
        }
        return beanList;
    }
    public static List<MonthlyShortUrlAccess> convertMonthlyShortUrlAccessListStubToBeanList(MonthlyShortUrlAccessListStub listStub)
    {
        return convertMonthlyShortUrlAccessListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<MonthlyShortUrlAccess> convertMonthlyShortUrlAccessListStubToBeanList(MonthlyShortUrlAccessListStub listStub, StringCursor forwardCursor)
    {
        List<MonthlyShortUrlAccess> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<MonthlyShortUrlAccess>();
        } else {
            beanList = new ArrayList<MonthlyShortUrlAccess>();
            for(MonthlyShortUrlAccessStub stub : listStub.getList()) {
            	beanList.add(convertMonthlyShortUrlAccessToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static MonthlyShortUrlAccessStub convertMonthlyShortUrlAccessToStub(MonthlyShortUrlAccess bean)
    {
        MonthlyShortUrlAccessStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new MonthlyShortUrlAccessStub();
        } else if(bean instanceof MonthlyShortUrlAccessStub) {
            stub = (MonthlyShortUrlAccessStub) bean;
        } else if(bean instanceof MonthlyShortUrlAccessBean && ((MonthlyShortUrlAccessBean) bean).isWrapper()) {
            stub = ((MonthlyShortUrlAccessBean) bean).getStub();
        } else {
            stub = MonthlyShortUrlAccessStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static WeeklyShortUrlAccessBean convertWeeklyShortUrlAccessToBean(WeeklyShortUrlAccess stub)
    {
        WeeklyShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new WeeklyShortUrlAccessBean();
        } else if(stub instanceof WeeklyShortUrlAccessBean) {
        	bean = (WeeklyShortUrlAccessBean) stub;
        } else {
        	bean = new WeeklyShortUrlAccessBean(stub);
        }
        return bean;
    }
    public static List<WeeklyShortUrlAccessBean> convertWeeklyShortUrlAccessListToBeanList(List<WeeklyShortUrlAccess> stubList)
    {
        List<WeeklyShortUrlAccessBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<WeeklyShortUrlAccess>();
        } else {
            beanList = new ArrayList<WeeklyShortUrlAccessBean>();
            for(WeeklyShortUrlAccess stub : stubList) {
                beanList.add(convertWeeklyShortUrlAccessToBean(stub));
            }
        }
        return beanList;
    }
    public static List<WeeklyShortUrlAccess> convertWeeklyShortUrlAccessListStubToBeanList(WeeklyShortUrlAccessListStub listStub)
    {
        return convertWeeklyShortUrlAccessListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<WeeklyShortUrlAccess> convertWeeklyShortUrlAccessListStubToBeanList(WeeklyShortUrlAccessListStub listStub, StringCursor forwardCursor)
    {
        List<WeeklyShortUrlAccess> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<WeeklyShortUrlAccess>();
        } else {
            beanList = new ArrayList<WeeklyShortUrlAccess>();
            for(WeeklyShortUrlAccessStub stub : listStub.getList()) {
            	beanList.add(convertWeeklyShortUrlAccessToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static WeeklyShortUrlAccessStub convertWeeklyShortUrlAccessToStub(WeeklyShortUrlAccess bean)
    {
        WeeklyShortUrlAccessStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new WeeklyShortUrlAccessStub();
        } else if(bean instanceof WeeklyShortUrlAccessStub) {
            stub = (WeeklyShortUrlAccessStub) bean;
        } else if(bean instanceof WeeklyShortUrlAccessBean && ((WeeklyShortUrlAccessBean) bean).isWrapper()) {
            stub = ((WeeklyShortUrlAccessBean) bean).getStub();
        } else {
            stub = WeeklyShortUrlAccessStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static DailyShortUrlAccessBean convertDailyShortUrlAccessToBean(DailyShortUrlAccess stub)
    {
        DailyShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new DailyShortUrlAccessBean();
        } else if(stub instanceof DailyShortUrlAccessBean) {
        	bean = (DailyShortUrlAccessBean) stub;
        } else {
        	bean = new DailyShortUrlAccessBean(stub);
        }
        return bean;
    }
    public static List<DailyShortUrlAccessBean> convertDailyShortUrlAccessListToBeanList(List<DailyShortUrlAccess> stubList)
    {
        List<DailyShortUrlAccessBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<DailyShortUrlAccess>();
        } else {
            beanList = new ArrayList<DailyShortUrlAccessBean>();
            for(DailyShortUrlAccess stub : stubList) {
                beanList.add(convertDailyShortUrlAccessToBean(stub));
            }
        }
        return beanList;
    }
    public static List<DailyShortUrlAccess> convertDailyShortUrlAccessListStubToBeanList(DailyShortUrlAccessListStub listStub)
    {
        return convertDailyShortUrlAccessListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<DailyShortUrlAccess> convertDailyShortUrlAccessListStubToBeanList(DailyShortUrlAccessListStub listStub, StringCursor forwardCursor)
    {
        List<DailyShortUrlAccess> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<DailyShortUrlAccess>();
        } else {
            beanList = new ArrayList<DailyShortUrlAccess>();
            for(DailyShortUrlAccessStub stub : listStub.getList()) {
            	beanList.add(convertDailyShortUrlAccessToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static DailyShortUrlAccessStub convertDailyShortUrlAccessToStub(DailyShortUrlAccess bean)
    {
        DailyShortUrlAccessStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new DailyShortUrlAccessStub();
        } else if(bean instanceof DailyShortUrlAccessStub) {
            stub = (DailyShortUrlAccessStub) bean;
        } else if(bean instanceof DailyShortUrlAccessBean && ((DailyShortUrlAccessBean) bean).isWrapper()) {
            stub = ((DailyShortUrlAccessBean) bean).getStub();
        } else {
            stub = DailyShortUrlAccessStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static HourlyShortUrlAccessBean convertHourlyShortUrlAccessToBean(HourlyShortUrlAccess stub)
    {
        HourlyShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new HourlyShortUrlAccessBean();
        } else if(stub instanceof HourlyShortUrlAccessBean) {
        	bean = (HourlyShortUrlAccessBean) stub;
        } else {
        	bean = new HourlyShortUrlAccessBean(stub);
        }
        return bean;
    }
    public static List<HourlyShortUrlAccessBean> convertHourlyShortUrlAccessListToBeanList(List<HourlyShortUrlAccess> stubList)
    {
        List<HourlyShortUrlAccessBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<HourlyShortUrlAccess>();
        } else {
            beanList = new ArrayList<HourlyShortUrlAccessBean>();
            for(HourlyShortUrlAccess stub : stubList) {
                beanList.add(convertHourlyShortUrlAccessToBean(stub));
            }
        }
        return beanList;
    }
    public static List<HourlyShortUrlAccess> convertHourlyShortUrlAccessListStubToBeanList(HourlyShortUrlAccessListStub listStub)
    {
        return convertHourlyShortUrlAccessListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<HourlyShortUrlAccess> convertHourlyShortUrlAccessListStubToBeanList(HourlyShortUrlAccessListStub listStub, StringCursor forwardCursor)
    {
        List<HourlyShortUrlAccess> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<HourlyShortUrlAccess>();
        } else {
            beanList = new ArrayList<HourlyShortUrlAccess>();
            for(HourlyShortUrlAccessStub stub : listStub.getList()) {
            	beanList.add(convertHourlyShortUrlAccessToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static HourlyShortUrlAccessStub convertHourlyShortUrlAccessToStub(HourlyShortUrlAccess bean)
    {
        HourlyShortUrlAccessStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new HourlyShortUrlAccessStub();
        } else if(bean instanceof HourlyShortUrlAccessStub) {
            stub = (HourlyShortUrlAccessStub) bean;
        } else if(bean instanceof HourlyShortUrlAccessBean && ((HourlyShortUrlAccessBean) bean).isWrapper()) {
            stub = ((HourlyShortUrlAccessBean) bean).getStub();
        } else {
            stub = HourlyShortUrlAccessStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static CumulativeShortUrlAccessBean convertCumulativeShortUrlAccessToBean(CumulativeShortUrlAccess stub)
    {
        CumulativeShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new CumulativeShortUrlAccessBean();
        } else if(stub instanceof CumulativeShortUrlAccessBean) {
        	bean = (CumulativeShortUrlAccessBean) stub;
        } else {
        	bean = new CumulativeShortUrlAccessBean(stub);
        }
        return bean;
    }
    public static List<CumulativeShortUrlAccessBean> convertCumulativeShortUrlAccessListToBeanList(List<CumulativeShortUrlAccess> stubList)
    {
        List<CumulativeShortUrlAccessBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<CumulativeShortUrlAccess>();
        } else {
            beanList = new ArrayList<CumulativeShortUrlAccessBean>();
            for(CumulativeShortUrlAccess stub : stubList) {
                beanList.add(convertCumulativeShortUrlAccessToBean(stub));
            }
        }
        return beanList;
    }
    public static List<CumulativeShortUrlAccess> convertCumulativeShortUrlAccessListStubToBeanList(CumulativeShortUrlAccessListStub listStub)
    {
        return convertCumulativeShortUrlAccessListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<CumulativeShortUrlAccess> convertCumulativeShortUrlAccessListStubToBeanList(CumulativeShortUrlAccessListStub listStub, StringCursor forwardCursor)
    {
        List<CumulativeShortUrlAccess> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<CumulativeShortUrlAccess>();
        } else {
            beanList = new ArrayList<CumulativeShortUrlAccess>();
            for(CumulativeShortUrlAccessStub stub : listStub.getList()) {
            	beanList.add(convertCumulativeShortUrlAccessToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static CumulativeShortUrlAccessStub convertCumulativeShortUrlAccessToStub(CumulativeShortUrlAccess bean)
    {
        CumulativeShortUrlAccessStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new CumulativeShortUrlAccessStub();
        } else if(bean instanceof CumulativeShortUrlAccessStub) {
            stub = (CumulativeShortUrlAccessStub) bean;
        } else if(bean instanceof CumulativeShortUrlAccessBean && ((CumulativeShortUrlAccessBean) bean).isWrapper()) {
            stub = ((CumulativeShortUrlAccessBean) bean).getStub();
        } else {
            stub = CumulativeShortUrlAccessStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static CurrentShortUrlAccessBean convertCurrentShortUrlAccessToBean(CurrentShortUrlAccess stub)
    {
        CurrentShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new CurrentShortUrlAccessBean();
        } else if(stub instanceof CurrentShortUrlAccessBean) {
        	bean = (CurrentShortUrlAccessBean) stub;
        } else {
        	bean = new CurrentShortUrlAccessBean(stub);
        }
        return bean;
    }
    public static List<CurrentShortUrlAccessBean> convertCurrentShortUrlAccessListToBeanList(List<CurrentShortUrlAccess> stubList)
    {
        List<CurrentShortUrlAccessBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<CurrentShortUrlAccess>();
        } else {
            beanList = new ArrayList<CurrentShortUrlAccessBean>();
            for(CurrentShortUrlAccess stub : stubList) {
                beanList.add(convertCurrentShortUrlAccessToBean(stub));
            }
        }
        return beanList;
    }
    public static List<CurrentShortUrlAccess> convertCurrentShortUrlAccessListStubToBeanList(CurrentShortUrlAccessListStub listStub)
    {
        return convertCurrentShortUrlAccessListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<CurrentShortUrlAccess> convertCurrentShortUrlAccessListStubToBeanList(CurrentShortUrlAccessListStub listStub, StringCursor forwardCursor)
    {
        List<CurrentShortUrlAccess> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<CurrentShortUrlAccess>();
        } else {
            beanList = new ArrayList<CurrentShortUrlAccess>();
            for(CurrentShortUrlAccessStub stub : listStub.getList()) {
            	beanList.add(convertCurrentShortUrlAccessToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static CurrentShortUrlAccessStub convertCurrentShortUrlAccessToStub(CurrentShortUrlAccess bean)
    {
        CurrentShortUrlAccessStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new CurrentShortUrlAccessStub();
        } else if(bean instanceof CurrentShortUrlAccessStub) {
            stub = (CurrentShortUrlAccessStub) bean;
        } else if(bean instanceof CurrentShortUrlAccessBean && ((CurrentShortUrlAccessBean) bean).isWrapper()) {
            stub = ((CurrentShortUrlAccessBean) bean).getStub();
        } else {
            stub = CurrentShortUrlAccessStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TotalShortUrlAccessBean convertTotalShortUrlAccessToBean(TotalShortUrlAccess stub)
    {
        TotalShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TotalShortUrlAccessBean();
        } else if(stub instanceof TotalShortUrlAccessBean) {
        	bean = (TotalShortUrlAccessBean) stub;
        } else {
        	bean = new TotalShortUrlAccessBean(stub);
        }
        return bean;
    }
    public static List<TotalShortUrlAccessBean> convertTotalShortUrlAccessListToBeanList(List<TotalShortUrlAccess> stubList)
    {
        List<TotalShortUrlAccessBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TotalShortUrlAccess>();
        } else {
            beanList = new ArrayList<TotalShortUrlAccessBean>();
            for(TotalShortUrlAccess stub : stubList) {
                beanList.add(convertTotalShortUrlAccessToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TotalShortUrlAccess> convertTotalShortUrlAccessListStubToBeanList(TotalShortUrlAccessListStub listStub)
    {
        return convertTotalShortUrlAccessListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TotalShortUrlAccess> convertTotalShortUrlAccessListStubToBeanList(TotalShortUrlAccessListStub listStub, StringCursor forwardCursor)
    {
        List<TotalShortUrlAccess> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TotalShortUrlAccess>();
        } else {
            beanList = new ArrayList<TotalShortUrlAccess>();
            for(TotalShortUrlAccessStub stub : listStub.getList()) {
            	beanList.add(convertTotalShortUrlAccessToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TotalShortUrlAccessStub convertTotalShortUrlAccessToStub(TotalShortUrlAccess bean)
    {
        TotalShortUrlAccessStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TotalShortUrlAccessStub();
        } else if(bean instanceof TotalShortUrlAccessStub) {
            stub = (TotalShortUrlAccessStub) bean;
        } else if(bean instanceof TotalShortUrlAccessBean && ((TotalShortUrlAccessBean) bean).isWrapper()) {
            stub = ((TotalShortUrlAccessBean) bean).getStub();
        } else {
            stub = TotalShortUrlAccessStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TotalLongUrlAccessBean convertTotalLongUrlAccessToBean(TotalLongUrlAccess stub)
    {
        TotalLongUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TotalLongUrlAccessBean();
        } else if(stub instanceof TotalLongUrlAccessBean) {
        	bean = (TotalLongUrlAccessBean) stub;
        } else {
        	bean = new TotalLongUrlAccessBean(stub);
        }
        return bean;
    }
    public static List<TotalLongUrlAccessBean> convertTotalLongUrlAccessListToBeanList(List<TotalLongUrlAccess> stubList)
    {
        List<TotalLongUrlAccessBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TotalLongUrlAccess>();
        } else {
            beanList = new ArrayList<TotalLongUrlAccessBean>();
            for(TotalLongUrlAccess stub : stubList) {
                beanList.add(convertTotalLongUrlAccessToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TotalLongUrlAccess> convertTotalLongUrlAccessListStubToBeanList(TotalLongUrlAccessListStub listStub)
    {
        return convertTotalLongUrlAccessListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TotalLongUrlAccess> convertTotalLongUrlAccessListStubToBeanList(TotalLongUrlAccessListStub listStub, StringCursor forwardCursor)
    {
        List<TotalLongUrlAccess> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TotalLongUrlAccess>();
        } else {
            beanList = new ArrayList<TotalLongUrlAccess>();
            for(TotalLongUrlAccessStub stub : listStub.getList()) {
            	beanList.add(convertTotalLongUrlAccessToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TotalLongUrlAccessStub convertTotalLongUrlAccessToStub(TotalLongUrlAccess bean)
    {
        TotalLongUrlAccessStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TotalLongUrlAccessStub();
        } else if(bean instanceof TotalLongUrlAccessStub) {
            stub = (TotalLongUrlAccessStub) bean;
        } else if(bean instanceof TotalLongUrlAccessBean && ((TotalLongUrlAccessBean) bean).isWrapper()) {
            stub = ((TotalLongUrlAccessBean) bean).getStub();
        } else {
            stub = TotalLongUrlAccessStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ServiceInfoBean convertServiceInfoToBean(ServiceInfo stub)
    {
        ServiceInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ServiceInfoBean();
        } else if(stub instanceof ServiceInfoBean) {
        	bean = (ServiceInfoBean) stub;
        } else {
        	bean = new ServiceInfoBean(stub);
        }
        return bean;
    }
    public static List<ServiceInfoBean> convertServiceInfoListToBeanList(List<ServiceInfo> stubList)
    {
        List<ServiceInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfoBean>();
            for(ServiceInfo stub : stubList) {
                beanList.add(convertServiceInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub)
    {
        return convertServiceInfoListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub, StringCursor forwardCursor)
    {
        List<ServiceInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfo>();
            for(ServiceInfoStub stub : listStub.getList()) {
            	beanList.add(convertServiceInfoToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ServiceInfoStub convertServiceInfoToStub(ServiceInfo bean)
    {
        ServiceInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ServiceInfoStub();
        } else if(bean instanceof ServiceInfoStub) {
            stub = (ServiceInfoStub) bean;
        } else if(bean instanceof ServiceInfoBean && ((ServiceInfoBean) bean).isWrapper()) {
            stub = ((ServiceInfoBean) bean).getStub();
        } else {
            stub = ServiceInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FiveTenBean convertFiveTenToBean(FiveTen stub)
    {
        FiveTenBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FiveTenBean();
        } else if(stub instanceof FiveTenBean) {
        	bean = (FiveTenBean) stub;
        } else {
        	bean = new FiveTenBean(stub);
        }
        return bean;
    }
    public static List<FiveTenBean> convertFiveTenListToBeanList(List<FiveTen> stubList)
    {
        List<FiveTenBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTenBean>();
            for(FiveTen stub : stubList) {
                beanList.add(convertFiveTenToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub)
    {
        return convertFiveTenListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub, StringCursor forwardCursor)
    {
        List<FiveTen> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTen>();
            for(FiveTenStub stub : listStub.getList()) {
            	beanList.add(convertFiveTenToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FiveTenStub convertFiveTenToStub(FiveTen bean)
    {
        FiveTenStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FiveTenStub();
        } else if(bean instanceof FiveTenStub) {
            stub = (FiveTenStub) bean;
        } else if(bean instanceof FiveTenBean && ((FiveTenBean) bean).isWrapper()) {
            stub = ((FiveTenBean) bean).getStub();
        } else {
            stub = FiveTenStub.convertBeanToStub(bean);
        }
        return stub;
    }

    
}
