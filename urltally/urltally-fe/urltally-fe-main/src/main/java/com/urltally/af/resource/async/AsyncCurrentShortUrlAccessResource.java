package com.urltally.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.stub.CurrentShortUrlAccessStub;
import com.urltally.ws.stub.CurrentShortUrlAccessListStub;
import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.proxy.CurrentShortUrlAccessServiceProxy;
import com.urltally.af.proxy.remote.RemoteProxyFactory;
import com.urltally.af.proxy.remote.RemoteCurrentShortUrlAccessServiceProxy;
import com.urltally.af.resource.CurrentShortUrlAccessResource;


@Path("/_task/r/currentShortUrlAccesses/")
public class AsyncCurrentShortUrlAccessResource extends BaseAsyncResource implements CurrentShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(AsyncCurrentShortUrlAccessResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncCurrentShortUrlAccessResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getCurrentShortUrlAccessList(List<CurrentShortUrlAccess> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findCurrentShortUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getCurrentShortUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getCurrentShortUrlAccess(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCurrentShortUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCurrentShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructCurrentShortUrlAccess(CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructCurrentShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            CurrentShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                CurrentShortUrlAccessStub realStub = (CurrentShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertCurrentShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertCurrentShortUrlAccessStubToBean(currentShortUrlAccess);
            }
            //bean = (CurrentShortUrlAccessBean) RemoteProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy().constructCurrentShortUrlAccess(bean);
            //currentShortUrlAccess = CurrentShortUrlAccessStub.convertBeanToStub(bean);
            //String guid = currentShortUrlAccess.getGuid();
            // TBD: createCurrentShortUrlAccess() or constructCurrentShortUrlAccess()???  (constructCurrentShortUrlAccess() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy().createCurrentShortUrlAccess(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructCurrentShortUrlAccess(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(currentShortUrlAccess).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createCurrentShortUrlAccess(CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createCurrentShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            CurrentShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                CurrentShortUrlAccessStub realStub = (CurrentShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertCurrentShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertCurrentShortUrlAccessStubToBean(currentShortUrlAccess);
            }
            String guid = RemoteProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy().createCurrentShortUrlAccess(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createCurrentShortUrlAccess(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createCurrentShortUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshCurrentShortUrlAccess(String guid, CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshCurrentShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(currentShortUrlAccess == null || !guid.equals(currentShortUrlAccess.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from currentShortUrlAccess guid = " + currentShortUrlAccess.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the currentShortUrlAccess with guid = " + guid);
            }
            CurrentShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                CurrentShortUrlAccessStub realStub = (CurrentShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertCurrentShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertCurrentShortUrlAccessStubToBean(currentShortUrlAccess);
            }
            //bean = (CurrentShortUrlAccessBean) RemoteProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy().refreshCurrentShortUrlAccess(bean);
            //if(bean == null) {
            //    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the currentShortUrlAccess with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the currentShortUrlAccess with guid = " + guid);
            //}
            //currentShortUrlAccess = CurrentShortUrlAccessStub.convertBeanToStub(bean);
            // TBD: updateCurrentShortUrlAccess() or refreshCurrentShortUrlAccess()???  (refreshCurrentShortUrlAccess() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy().updateCurrentShortUrlAccess(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refrefsh the currentShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the currentShortUrlAccess with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshCurrentShortUrlAccess(): Successfully processed the request: guid = " + guid);
            return Response.ok(currentShortUrlAccess).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateCurrentShortUrlAccess(String guid, CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateCurrentShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(currentShortUrlAccess == null || !guid.equals(currentShortUrlAccess.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from currentShortUrlAccess guid = " + currentShortUrlAccess.getGuid());
                throw new RequestForbiddenRsException("Failed to update the currentShortUrlAccess with guid = " + guid);
            }
            CurrentShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                CurrentShortUrlAccessStub realStub = (CurrentShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertCurrentShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertCurrentShortUrlAccessStubToBean(currentShortUrlAccess);
            }
            boolean suc = RemoteProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy().updateCurrentShortUrlAccess(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the currentShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the currentShortUrlAccess with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateCurrentShortUrlAccess(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateCurrentShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
    public Response updateCurrentShortUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteCurrentShortUrlAccess(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteCurrentShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy().deleteCurrentShortUrlAccess(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the currentShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the currentShortUrlAccess with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteCurrentShortUrlAccess(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteCurrentShortUrlAccesses(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy().deleteCurrentShortUrlAccesses(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createCurrentShortUrlAccesses(CurrentShortUrlAccessListStub currentShortUrlAccesses) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createCurrentShortUrlAccesses(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            CurrentShortUrlAccessListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                CurrentShortUrlAccessListStub realStubs = (CurrentShortUrlAccessListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = currentShortUrlAccesses;
            }

            List<CurrentShortUrlAccessStub> stubList = currentShortUrlAccesses.getList();
            List<VisitorSetting> beans = new ArrayList<CurrentShortUrlAccess>();
            for(CurrentShortUrlAccessStub stub : stubList) {
                CurrentShortUrlAccessBean bean = convertCurrentShortUrlAccessStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy().createCurrentShortUrlAccesses(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static CurrentShortUrlAccessBean convertCurrentShortUrlAccessStubToBean(CurrentShortUrlAccess stub)
    {
        CurrentShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new CurrentShortUrlAccessBean();
            bean.setGuid(stub.getGuid());
            bean.setTallyTime(stub.getTallyTime());
            bean.setTallyEpoch(stub.getTallyEpoch());
            bean.setCount(stub.getCount());
            bean.setShortUrl(stub.getShortUrl());
            bean.setShortUrlDomain(stub.getShortUrlDomain());
            bean.setLongUrl(stub.getLongUrl());
            bean.setLongUrlDomain(stub.getLongUrlDomain());
            bean.setRedirectType(stub.getRedirectType());
            bean.setRefererDomain(stub.getRefererDomain());
            bean.setUserAgent(stub.getUserAgent());
            bean.setLanguage(stub.getLanguage());
            bean.setCountry(stub.getCountry());
            bean.setTalliedTime(stub.getTalliedTime());
            bean.setStartDayHour(stub.getStartDayHour());
            bean.setStartTime(stub.getStartTime());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
