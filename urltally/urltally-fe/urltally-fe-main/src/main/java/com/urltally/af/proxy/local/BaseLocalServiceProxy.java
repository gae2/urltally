package com.urltally.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.service.ApiConsumerService;
import com.urltally.ws.service.UserService;
import com.urltally.ws.service.AccessTallyMasterService;
import com.urltally.ws.service.AccessTallyStatusService;
import com.urltally.ws.service.MonthlyShortUrlAccessService;
import com.urltally.ws.service.WeeklyShortUrlAccessService;
import com.urltally.ws.service.DailyShortUrlAccessService;
import com.urltally.ws.service.HourlyShortUrlAccessService;
import com.urltally.ws.service.CumulativeShortUrlAccessService;
import com.urltally.ws.service.CurrentShortUrlAccessService;
import com.urltally.ws.service.TotalShortUrlAccessService;
import com.urltally.ws.service.TotalLongUrlAccessService;
import com.urltally.ws.service.ServiceInfoService;
import com.urltally.ws.service.FiveTenService;


// TBD: How to best inject the service instances?
// (The current implementation does not seem to make sense...)
public abstract class BaseLocalServiceProxy
{
    private static final Logger log = Logger.getLogger(BaseLocalServiceProxy.class.getName());

    private ApiConsumerService apiConsumerService;
    private UserService userService;
    private AccessTallyMasterService accessTallyMasterService;
    private AccessTallyStatusService accessTallyStatusService;
    private MonthlyShortUrlAccessService monthlyShortUrlAccessService;
    private WeeklyShortUrlAccessService weeklyShortUrlAccessService;
    private DailyShortUrlAccessService dailyShortUrlAccessService;
    private HourlyShortUrlAccessService hourlyShortUrlAccessService;
    private CumulativeShortUrlAccessService cumulativeShortUrlAccessService;
    private CurrentShortUrlAccessService currentShortUrlAccessService;
    private TotalShortUrlAccessService totalShortUrlAccessService;
    private TotalLongUrlAccessService totalLongUrlAccessService;
    private ServiceInfoService serviceInfoService;
    private FiveTenService fiveTenService;

    public BaseLocalServiceProxy()
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService)
    {
        this(apiConsumerService, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserService userService)
    {
        this(null, userService, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(AccessTallyMasterService accessTallyMasterService)
    {
        this(null, null, accessTallyMasterService, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(AccessTallyStatusService accessTallyStatusService)
    {
        this(null, null, null, accessTallyStatusService, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(MonthlyShortUrlAccessService monthlyShortUrlAccessService)
    {
        this(null, null, null, null, monthlyShortUrlAccessService, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(WeeklyShortUrlAccessService weeklyShortUrlAccessService)
    {
        this(null, null, null, null, null, weeklyShortUrlAccessService, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(DailyShortUrlAccessService dailyShortUrlAccessService)
    {
        this(null, null, null, null, null, null, dailyShortUrlAccessService, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(HourlyShortUrlAccessService hourlyShortUrlAccessService)
    {
        this(null, null, null, null, null, null, null, hourlyShortUrlAccessService, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(CumulativeShortUrlAccessService cumulativeShortUrlAccessService)
    {
        this(null, null, null, null, null, null, null, null, cumulativeShortUrlAccessService, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(CurrentShortUrlAccessService currentShortUrlAccessService)
    {
        this(null, null, null, null, null, null, null, null, null, currentShortUrlAccessService, null, null, null, null);
    }
    public BaseLocalServiceProxy(TotalShortUrlAccessService totalShortUrlAccessService)
    {
        this(null, null, null, null, null, null, null, null, null, null, totalShortUrlAccessService, null, null, null);
    }
    public BaseLocalServiceProxy(TotalLongUrlAccessService totalLongUrlAccessService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, totalLongUrlAccessService, null, null);
    }
    public BaseLocalServiceProxy(ServiceInfoService serviceInfoService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, serviceInfoService, null);
    }
    public BaseLocalServiceProxy(FiveTenService fiveTenService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, fiveTenService);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService, UserService userService, AccessTallyMasterService accessTallyMasterService, AccessTallyStatusService accessTallyStatusService, MonthlyShortUrlAccessService monthlyShortUrlAccessService, WeeklyShortUrlAccessService weeklyShortUrlAccessService, DailyShortUrlAccessService dailyShortUrlAccessService, HourlyShortUrlAccessService hourlyShortUrlAccessService, CumulativeShortUrlAccessService cumulativeShortUrlAccessService, CurrentShortUrlAccessService currentShortUrlAccessService, TotalShortUrlAccessService totalShortUrlAccessService, TotalLongUrlAccessService totalLongUrlAccessService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.accessTallyMasterService = accessTallyMasterService;
        this.accessTallyStatusService = accessTallyStatusService;
        this.monthlyShortUrlAccessService = monthlyShortUrlAccessService;
        this.weeklyShortUrlAccessService = weeklyShortUrlAccessService;
        this.dailyShortUrlAccessService = dailyShortUrlAccessService;
        this.hourlyShortUrlAccessService = hourlyShortUrlAccessService;
        this.cumulativeShortUrlAccessService = cumulativeShortUrlAccessService;
        this.currentShortUrlAccessService = currentShortUrlAccessService;
        this.totalShortUrlAccessService = totalShortUrlAccessService;
        this.totalLongUrlAccessService = totalLongUrlAccessService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }
    
    // Inject dependencies.
    public void setApiConsumerService(ApiConsumerService apiConsumerService)
    {
        this.apiConsumerService = apiConsumerService;
    }
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }
    public void setAccessTallyMasterService(AccessTallyMasterService accessTallyMasterService)
    {
        this.accessTallyMasterService = accessTallyMasterService;
    }
    public void setAccessTallyStatusService(AccessTallyStatusService accessTallyStatusService)
    {
        this.accessTallyStatusService = accessTallyStatusService;
    }
    public void setMonthlyShortUrlAccessService(MonthlyShortUrlAccessService monthlyShortUrlAccessService)
    {
        this.monthlyShortUrlAccessService = monthlyShortUrlAccessService;
    }
    public void setWeeklyShortUrlAccessService(WeeklyShortUrlAccessService weeklyShortUrlAccessService)
    {
        this.weeklyShortUrlAccessService = weeklyShortUrlAccessService;
    }
    public void setDailyShortUrlAccessService(DailyShortUrlAccessService dailyShortUrlAccessService)
    {
        this.dailyShortUrlAccessService = dailyShortUrlAccessService;
    }
    public void setHourlyShortUrlAccessService(HourlyShortUrlAccessService hourlyShortUrlAccessService)
    {
        this.hourlyShortUrlAccessService = hourlyShortUrlAccessService;
    }
    public void setCumulativeShortUrlAccessService(CumulativeShortUrlAccessService cumulativeShortUrlAccessService)
    {
        this.cumulativeShortUrlAccessService = cumulativeShortUrlAccessService;
    }
    public void setCurrentShortUrlAccessService(CurrentShortUrlAccessService currentShortUrlAccessService)
    {
        this.currentShortUrlAccessService = currentShortUrlAccessService;
    }
    public void setTotalShortUrlAccessService(TotalShortUrlAccessService totalShortUrlAccessService)
    {
        this.totalShortUrlAccessService = totalShortUrlAccessService;
    }
    public void setTotalLongUrlAccessService(TotalLongUrlAccessService totalLongUrlAccessService)
    {
        this.totalLongUrlAccessService = totalLongUrlAccessService;
    }
    public void setServiceInfoService(ServiceInfoService serviceInfoService)
    {
        this.serviceInfoService = serviceInfoService;
    }
    public void setFiveTenService(FiveTenService fiveTenService)
    {
        this.fiveTenService = fiveTenService;
    }
   
    // Returns a ApiConsumerService instance.
    public ApiConsumerService getApiConsumerService() 
    {
        return apiConsumerService;
    }

    // Returns a UserService instance.
    public UserService getUserService() 
    {
        return userService;
    }

    // Returns a AccessTallyMasterService instance.
    public AccessTallyMasterService getAccessTallyMasterService() 
    {
        return accessTallyMasterService;
    }

    // Returns a AccessTallyStatusService instance.
    public AccessTallyStatusService getAccessTallyStatusService() 
    {
        return accessTallyStatusService;
    }

    // Returns a MonthlyShortUrlAccessService instance.
    public MonthlyShortUrlAccessService getMonthlyShortUrlAccessService() 
    {
        return monthlyShortUrlAccessService;
    }

    // Returns a WeeklyShortUrlAccessService instance.
    public WeeklyShortUrlAccessService getWeeklyShortUrlAccessService() 
    {
        return weeklyShortUrlAccessService;
    }

    // Returns a DailyShortUrlAccessService instance.
    public DailyShortUrlAccessService getDailyShortUrlAccessService() 
    {
        return dailyShortUrlAccessService;
    }

    // Returns a HourlyShortUrlAccessService instance.
    public HourlyShortUrlAccessService getHourlyShortUrlAccessService() 
    {
        return hourlyShortUrlAccessService;
    }

    // Returns a CumulativeShortUrlAccessService instance.
    public CumulativeShortUrlAccessService getCumulativeShortUrlAccessService() 
    {
        return cumulativeShortUrlAccessService;
    }

    // Returns a CurrentShortUrlAccessService instance.
    public CurrentShortUrlAccessService getCurrentShortUrlAccessService() 
    {
        return currentShortUrlAccessService;
    }

    // Returns a TotalShortUrlAccessService instance.
    public TotalShortUrlAccessService getTotalShortUrlAccessService() 
    {
        return totalShortUrlAccessService;
    }

    // Returns a TotalLongUrlAccessService instance.
    public TotalLongUrlAccessService getTotalLongUrlAccessService() 
    {
        return totalLongUrlAccessService;
    }

    // Returns a ServiceInfoService instance.
    public ServiceInfoService getServiceInfoService() 
    {
        return serviceInfoService;
    }

    // Returns a FiveTenService instance.
    public FiveTenService getFiveTenService() 
    {
        return fiveTenService;
    }

}
