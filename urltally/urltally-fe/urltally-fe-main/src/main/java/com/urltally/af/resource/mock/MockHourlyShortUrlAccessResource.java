package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.stub.HourlyShortUrlAccessStub;
import com.urltally.ws.stub.HourlyShortUrlAccessListStub;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.resource.HourlyShortUrlAccessResource;


// MockHourlyShortUrlAccessResource is a decorator.
// It can be used as a base class to mock HourlyShortUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/hourlyShortUrlAccesses/")
public abstract class MockHourlyShortUrlAccessResource implements HourlyShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockHourlyShortUrlAccessResource.class.getName());

    // MockHourlyShortUrlAccessResource uses the decorator design pattern.
    private HourlyShortUrlAccessResource decoratedResource;

    public MockHourlyShortUrlAccessResource(HourlyShortUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected HourlyShortUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(HourlyShortUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllHourlyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllHourlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findHourlyShortUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findHourlyShortUrlAccessesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getHourlyShortUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getHourlyShortUrlAccessAsHtml(guid);
//    }

    @Override
    public Response getHourlyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getHourlyShortUrlAccess(guid);
    }

    @Override
    public Response getHourlyShortUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getHourlyShortUrlAccessAsJsonp(guid, callback);
    }

    @Override
    public Response getHourlyShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getHourlyShortUrlAccess(guid, field);
    }

    // TBD
    @Override
    public Response constructHourlyShortUrlAccess(HourlyShortUrlAccessStub hourlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.constructHourlyShortUrlAccess(hourlyShortUrlAccess);
    }

    @Override
    public Response createHourlyShortUrlAccess(HourlyShortUrlAccessStub hourlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createHourlyShortUrlAccess(hourlyShortUrlAccess);
    }

//    @Override
//    public Response createHourlyShortUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createHourlyShortUrlAccess(formParams);
//    }

    // TBD
    @Override
    public Response refreshHourlyShortUrlAccess(String guid, HourlyShortUrlAccessStub hourlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.refreshHourlyShortUrlAccess(guid, hourlyShortUrlAccess);
    }

    @Override
    public Response updateHourlyShortUrlAccess(String guid, HourlyShortUrlAccessStub hourlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateHourlyShortUrlAccess(guid, hourlyShortUrlAccess);
    }

    @Override
    public Response updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour)
    {
        return decoratedResource.updateHourlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
    }

//    @Override
//    public Response updateHourlyShortUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateHourlyShortUrlAccess(guid, formParams);
//    }

    @Override
    public Response deleteHourlyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteHourlyShortUrlAccess(guid);
    }

    @Override
    public Response deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteHourlyShortUrlAccesses(filter, params, values);
    }


// TBD ....
    @Override
    public Response createHourlyShortUrlAccesses(HourlyShortUrlAccessListStub hourlyShortUrlAccesses) throws BaseResourceException
    {
        return decoratedResource.createHourlyShortUrlAccesses(hourlyShortUrlAccesses);
    }


}
