package com.urltally.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.GeoPointStruct;
import com.urltally.ws.StreetAddressStruct;
import com.urltally.ws.GaeAppStruct;
import com.urltally.ws.FullNameStruct;
import com.urltally.ws.GaeUserStruct;
import com.urltally.ws.User;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GaeUserStructBean;
import com.urltally.af.bean.UserBean;
import com.urltally.ws.service.UserService;
import com.urltally.af.proxy.UserServiceProxy;


// MockUserServiceProxy is a decorator.
// It can be used as a base class to mock UserServiceProxy objects.
public abstract class MockUserServiceProxy implements UserServiceProxy
{
    private static final Logger log = Logger.getLogger(MockUserServiceProxy.class.getName());

    // MockUserServiceProxy uses the decorator design pattern.
    private UserServiceProxy decoratedProxy;

    public MockUserServiceProxy(UserServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected UserServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(UserServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public User getUser(String guid) throws BaseException
    {
        return decoratedProxy.getUser(guid);
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        return decoratedProxy.getUser(guid, field);       
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        return decoratedProxy.getUsers(guids);
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllUsers(ordering, offset, count);
        return getAllUsers(ordering, offset, count, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUsers(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllUserKeys(ordering, offset, count);
        return getAllUserKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllUserKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findUsers(filter, ordering, params, values, grouping, unique, offset, count);
        return findUsers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return decoratedProxy.createUser(managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        return decoratedProxy.createUser(user);
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return decoratedProxy.updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        return decoratedProxy.updateUser(user);
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        return decoratedProxy.deleteUser(guid);
    }

    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        String guid = user.getGuid();
        return deleteUser(guid);
    }

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteUsers(filter, params, values);
    }

}
