package com.urltally.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.urltally.ws.BaseException;
import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.af.service.impl.WeeklyShortUrlAccessServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class WeeklyShortUrlAccessAppService extends WeeklyShortUrlAccessServiceImpl implements WeeklyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(WeeklyShortUrlAccessAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public WeeklyShortUrlAccessAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // WeeklyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public WeeklyShortUrlAccess getWeeklyShortUrlAccess(String guid) throws BaseException
    {
        return super.getWeeklyShortUrlAccess(guid);
    }

    @Override
    public Object getWeeklyShortUrlAccess(String guid, String field) throws BaseException
    {
        return super.getWeeklyShortUrlAccess(guid, field);
    }

    @Override
    public List<WeeklyShortUrlAccess> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return super.getWeeklyShortUrlAccesses(guids);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses() throws BaseException
    {
        return super.getAllWeeklyShortUrlAccesses();
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllWeeklyShortUrlAccessKeys(ordering, offset, count);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        return super.createWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }

    @Override
    public WeeklyShortUrlAccess constructWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        return super.constructWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }


    @Override
    public Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        return super.updateWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }
        
    @Override
    public WeeklyShortUrlAccess refreshWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        return super.refreshWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException
    {
        return super.deleteWeeklyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        return super.deleteWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }

    @Override
    public Integer createWeeklyShortUrlAccesses(List<WeeklyShortUrlAccess> weeklyShortUrlAccesses) throws BaseException
    {
        return super.createWeeklyShortUrlAccesses(weeklyShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateWeeklyShortUrlAccesses(List<WeeklyShortUrlAccess> weeklyShortUrlAccesses) throws BaseException
    //{
    //}

}
