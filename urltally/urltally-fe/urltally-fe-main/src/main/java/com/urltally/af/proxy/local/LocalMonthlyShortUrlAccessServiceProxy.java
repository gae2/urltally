package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.MonthlyShortUrlAccess;
// import com.urltally.ws.bean.MonthlyShortUrlAccessBean;
import com.urltally.ws.service.MonthlyShortUrlAccessService;
import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.af.proxy.MonthlyShortUrlAccessServiceProxy;


public class LocalMonthlyShortUrlAccessServiceProxy extends BaseLocalServiceProxy implements MonthlyShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalMonthlyShortUrlAccessServiceProxy.class.getName());

    public LocalMonthlyShortUrlAccessServiceProxy()
    {
    }

    @Override
    public MonthlyShortUrlAccess getMonthlyShortUrlAccess(String guid) throws BaseException
    {
        MonthlyShortUrlAccess serverBean = getMonthlyShortUrlAccessService().getMonthlyShortUrlAccess(guid);
        MonthlyShortUrlAccess appBean = convertServerMonthlyShortUrlAccessBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getMonthlyShortUrlAccess(String guid, String field) throws BaseException
    {
        return getMonthlyShortUrlAccessService().getMonthlyShortUrlAccess(guid, field);       
    }

    @Override
    public List<MonthlyShortUrlAccess> getMonthlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        List<MonthlyShortUrlAccess> serverBeanList = getMonthlyShortUrlAccessService().getMonthlyShortUrlAccesses(guids);
        List<MonthlyShortUrlAccess> appBeanList = convertServerMonthlyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses() throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getMonthlyShortUrlAccessService().getAllMonthlyShortUrlAccesses(ordering, offset, count);
        return getAllMonthlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<MonthlyShortUrlAccess> serverBeanList = getMonthlyShortUrlAccessService().getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
        List<MonthlyShortUrlAccess> appBeanList = convertServerMonthlyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getMonthlyShortUrlAccessService().getAllMonthlyShortUrlAccessKeys(ordering, offset, count);
        return getAllMonthlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getMonthlyShortUrlAccessService().getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getMonthlyShortUrlAccessService().findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<MonthlyShortUrlAccess> serverBeanList = getMonthlyShortUrlAccessService().findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<MonthlyShortUrlAccess> appBeanList = convertServerMonthlyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getMonthlyShortUrlAccessService().findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getMonthlyShortUrlAccessService().findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getMonthlyShortUrlAccessService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createMonthlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        return getMonthlyShortUrlAccessService().createMonthlyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
    }

    @Override
    public String createMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.MonthlyShortUrlAccessBean serverBean =  convertAppMonthlyShortUrlAccessBeanToServerBean(monthlyShortUrlAccess);
        return getMonthlyShortUrlAccessService().createMonthlyShortUrlAccess(serverBean);
    }

    @Override
    public Boolean updateMonthlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        return getMonthlyShortUrlAccessService().updateMonthlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
    }

    @Override
    public Boolean updateMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.MonthlyShortUrlAccessBean serverBean =  convertAppMonthlyShortUrlAccessBeanToServerBean(monthlyShortUrlAccess);
        return getMonthlyShortUrlAccessService().updateMonthlyShortUrlAccess(serverBean);
    }

    @Override
    public Boolean deleteMonthlyShortUrlAccess(String guid) throws BaseException
    {
        return getMonthlyShortUrlAccessService().deleteMonthlyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.MonthlyShortUrlAccessBean serverBean =  convertAppMonthlyShortUrlAccessBeanToServerBean(monthlyShortUrlAccess);
        return getMonthlyShortUrlAccessService().deleteMonthlyShortUrlAccess(serverBean);
    }

    @Override
    public Long deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return getMonthlyShortUrlAccessService().deleteMonthlyShortUrlAccesses(filter, params, values);
    }




    public static MonthlyShortUrlAccessBean convertServerMonthlyShortUrlAccessBeanToAppBean(MonthlyShortUrlAccess serverBean)
    {
        MonthlyShortUrlAccessBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new MonthlyShortUrlAccessBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setCount(serverBean.getCount());
            bean.setShortUrl(serverBean.getShortUrl());
            bean.setShortUrlDomain(serverBean.getShortUrlDomain());
            bean.setLongUrl(serverBean.getLongUrl());
            bean.setLongUrlDomain(serverBean.getLongUrlDomain());
            bean.setRedirectType(serverBean.getRedirectType());
            bean.setRefererDomain(serverBean.getRefererDomain());
            bean.setUserAgent(serverBean.getUserAgent());
            bean.setLanguage(serverBean.getLanguage());
            bean.setCountry(serverBean.getCountry());
            bean.setTalliedTime(serverBean.getTalliedTime());
            bean.setYear(serverBean.getYear());
            bean.setMonth(serverBean.getMonth());
            bean.setNumberOfDays(serverBean.getNumberOfDays());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<MonthlyShortUrlAccess> convertServerMonthlyShortUrlAccessBeanListToAppBeanList(List<MonthlyShortUrlAccess> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<MonthlyShortUrlAccess> beanList = new ArrayList<MonthlyShortUrlAccess>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(MonthlyShortUrlAccess sb : serverBeanList) {
                MonthlyShortUrlAccessBean bean = convertServerMonthlyShortUrlAccessBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.MonthlyShortUrlAccessBean convertAppMonthlyShortUrlAccessBeanToServerBean(MonthlyShortUrlAccess appBean)
    {
        com.urltally.ws.bean.MonthlyShortUrlAccessBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.MonthlyShortUrlAccessBean();
            bean.setGuid(appBean.getGuid());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setCount(appBean.getCount());
            bean.setShortUrl(appBean.getShortUrl());
            bean.setShortUrlDomain(appBean.getShortUrlDomain());
            bean.setLongUrl(appBean.getLongUrl());
            bean.setLongUrlDomain(appBean.getLongUrlDomain());
            bean.setRedirectType(appBean.getRedirectType());
            bean.setRefererDomain(appBean.getRefererDomain());
            bean.setUserAgent(appBean.getUserAgent());
            bean.setLanguage(appBean.getLanguage());
            bean.setCountry(appBean.getCountry());
            bean.setTalliedTime(appBean.getTalliedTime());
            bean.setYear(appBean.getYear());
            bean.setMonth(appBean.getMonth());
            bean.setNumberOfDays(appBean.getNumberOfDays());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<MonthlyShortUrlAccess> convertAppMonthlyShortUrlAccessBeanListToServerBeanList(List<MonthlyShortUrlAccess> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<MonthlyShortUrlAccess> beanList = new ArrayList<MonthlyShortUrlAccess>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(MonthlyShortUrlAccess sb : appBeanList) {
                com.urltally.ws.bean.MonthlyShortUrlAccessBean bean = convertAppMonthlyShortUrlAccessBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
