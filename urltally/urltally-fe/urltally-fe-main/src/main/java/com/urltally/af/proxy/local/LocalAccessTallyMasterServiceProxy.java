package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyMaster;
// import com.urltally.ws.bean.AccessTallyMasterBean;
import com.urltally.ws.service.AccessTallyMasterService;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.proxy.AccessTallyMasterServiceProxy;


public class LocalAccessTallyMasterServiceProxy extends BaseLocalServiceProxy implements AccessTallyMasterServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalAccessTallyMasterServiceProxy.class.getName());

    public LocalAccessTallyMasterServiceProxy()
    {
    }

    @Override
    public AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException
    {
        AccessTallyMaster serverBean = getAccessTallyMasterService().getAccessTallyMaster(guid);
        AccessTallyMaster appBean = convertServerAccessTallyMasterBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getAccessTallyMaster(String guid, String field) throws BaseException
    {
        return getAccessTallyMasterService().getAccessTallyMaster(guid, field);       
    }

    @Override
    public List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        List<AccessTallyMaster> serverBeanList = getAccessTallyMasterService().getAccessTallyMasters(guids);
        List<AccessTallyMaster> appBeanList = convertServerAccessTallyMasterBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException
    {
        return getAllAccessTallyMasters(null, null, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getAccessTallyMasterService().getAllAccessTallyMasters(ordering, offset, count);
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<AccessTallyMaster> serverBeanList = getAccessTallyMasterService().getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
        List<AccessTallyMaster> appBeanList = convertServerAccessTallyMasterBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getAccessTallyMasterService().getAllAccessTallyMasterKeys(ordering, offset, count);
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getAccessTallyMasterService().getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getAccessTallyMasterService().findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<AccessTallyMaster> serverBeanList = getAccessTallyMasterService().findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<AccessTallyMaster> appBeanList = convertServerAccessTallyMasterBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getAccessTallyMasterService().findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getAccessTallyMasterService().findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getAccessTallyMasterService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        return getAccessTallyMasterService().createAccessTallyMaster(tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }

    @Override
    public String createAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        com.urltally.ws.bean.AccessTallyMasterBean serverBean =  convertAppAccessTallyMasterBeanToServerBean(accessTallyMaster);
        return getAccessTallyMasterService().createAccessTallyMaster(serverBean);
    }

    @Override
    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        return getAccessTallyMasterService().updateAccessTallyMaster(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }

    @Override
    public Boolean updateAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        com.urltally.ws.bean.AccessTallyMasterBean serverBean =  convertAppAccessTallyMasterBeanToServerBean(accessTallyMaster);
        return getAccessTallyMasterService().updateAccessTallyMaster(serverBean);
    }

    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        return getAccessTallyMasterService().deleteAccessTallyMaster(guid);
    }

    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        com.urltally.ws.bean.AccessTallyMasterBean serverBean =  convertAppAccessTallyMasterBeanToServerBean(accessTallyMaster);
        return getAccessTallyMasterService().deleteAccessTallyMaster(serverBean);
    }

    @Override
    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException
    {
        return getAccessTallyMasterService().deleteAccessTallyMasters(filter, params, values);
    }




    public static AccessTallyMasterBean convertServerAccessTallyMasterBeanToAppBean(AccessTallyMaster serverBean)
    {
        AccessTallyMasterBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new AccessTallyMasterBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTallyType(serverBean.getTallyType());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setTallyStatus(serverBean.getTallyStatus());
            bean.setAccessRecordCount(serverBean.getAccessRecordCount());
            bean.setProcesingStartedTime(serverBean.getProcesingStartedTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<AccessTallyMaster> convertServerAccessTallyMasterBeanListToAppBeanList(List<AccessTallyMaster> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<AccessTallyMaster> beanList = new ArrayList<AccessTallyMaster>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(AccessTallyMaster sb : serverBeanList) {
                AccessTallyMasterBean bean = convertServerAccessTallyMasterBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.AccessTallyMasterBean convertAppAccessTallyMasterBeanToServerBean(AccessTallyMaster appBean)
    {
        com.urltally.ws.bean.AccessTallyMasterBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.AccessTallyMasterBean();
            bean.setGuid(appBean.getGuid());
            bean.setTallyType(appBean.getTallyType());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setTallyStatus(appBean.getTallyStatus());
            bean.setAccessRecordCount(appBean.getAccessRecordCount());
            bean.setProcesingStartedTime(appBean.getProcesingStartedTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<AccessTallyMaster> convertAppAccessTallyMasterBeanListToServerBeanList(List<AccessTallyMaster> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<AccessTallyMaster> beanList = new ArrayList<AccessTallyMaster>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(AccessTallyMaster sb : appBeanList) {
                com.urltally.ws.bean.AccessTallyMasterBean bean = convertAppAccessTallyMasterBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
