package com.urltally.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.af.config.Config;

import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.AccessTallyStatusService;


// The primary purpose of AccessTallyStatusDummyService is to fake the service api, AccessTallyStatusService.
// It has no real implementation.
public class AccessTallyStatusDummyService implements AccessTallyStatusService
{
    private static final Logger log = Logger.getLogger(AccessTallyStatusDummyService.class.getName());

    public AccessTallyStatusDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // AccessTallyStatus related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAccessTallyStatus(): guid = " + guid);
        return null;
    }

    @Override
    public Object getAccessTallyStatus(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAccessTallyStatus(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        log.fine("getAccessTallyStatuses()");
        return null;
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException
    {
        return getAllAccessTallyStatuses(null, null, null);
    }


    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyStatuses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyStatusKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusDummyService.findAccessTallyStatuses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusDummyService.findAccessTallyStatusKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        log.finer("createAccessTallyStatus()");
        return null;
    }

    @Override
    public String createAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("createAccessTallyStatus()");
        return null;
    }

    @Override
    public AccessTallyStatus constructAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("constructAccessTallyStatus()");
        return null;
    }

    @Override
    public Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        log.finer("updateAccessTallyStatus()");
        return null;
    }
        
    @Override
    public Boolean updateAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("updateAccessTallyStatus()");
        return null;
    }

    @Override
    public AccessTallyStatus refreshAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("refreshAccessTallyStatus()");
        return null;
    }

    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteAccessTallyStatus(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("deleteAccessTallyStatus()");
        return null;
    }

    // TBD
    @Override
    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteAccessTallyStatus(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    {
        log.finer("createAccessTallyStatuses()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    //{
    //}

}
