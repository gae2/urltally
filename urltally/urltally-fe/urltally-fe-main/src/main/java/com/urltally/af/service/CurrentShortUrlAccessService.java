package com.urltally.af.service;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CurrentShortUrlAccess;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface CurrentShortUrlAccessService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    CurrentShortUrlAccess getCurrentShortUrlAccess(String guid) throws BaseException;
    Object getCurrentShortUrlAccess(String guid, String field) throws BaseException;
    List<CurrentShortUrlAccess> getCurrentShortUrlAccesses(List<String> guids) throws BaseException;
    List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createCurrentShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException;
    //String createCurrentShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return CurrentShortUrlAccess?)
    String createCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException;
    CurrentShortUrlAccess constructCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException;
    Boolean updateCurrentShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException;
    //Boolean updateCurrentShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException;
    CurrentShortUrlAccess refreshCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException;
    Boolean deleteCurrentShortUrlAccess(String guid) throws BaseException;
    Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException;
    Long deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createCurrentShortUrlAccesses(List<CurrentShortUrlAccess> currentShortUrlAccesses) throws BaseException;
//    Boolean updateCurrentShortUrlAccesses(List<CurrentShortUrlAccess> currentShortUrlAccesses) throws BaseException;

}
