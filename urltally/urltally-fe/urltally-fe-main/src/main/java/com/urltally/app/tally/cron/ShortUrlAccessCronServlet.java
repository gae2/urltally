package com.urltally.app.tally.cron;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.urltally.common.TallyType;
import com.urltally.ws.core.StatusCode;


// Mainly, for ajax calls....
public class ShortUrlAccessCronServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortUrlAccessCronServlet.class.getName());
    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        //JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singletong instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("SendStatusCronServlet::doGet() called.");
        
        // TBD:
        // Check the header X-AppEngine-Cron: true
        // ...
        boolean isGaeCron = false;
        String headerXGAECron = req.getHeader("X-AppEngine-Cron");
        if(headerXGAECron != null) {
            isGaeCron = Boolean.parseBoolean(headerXGAECron);
        }
        log.fine("isGaeCron = " + isGaeCron);

        
        // TBD:
        // Based on config,
        // Adjust max mail count, duration, etc.. ??
        // ...

        
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);


        String arg = null;
        if(pathInfo != null && !pathInfo.isEmpty()) {
            if(pathInfo.startsWith("/")) {
                arg = pathInfo.substring(1);
            } else {
                arg = pathInfo;
            }
        }
        
        String tallyType = null;
        if(TallyType.isValid(arg)) {
            tallyType = arg;
        } else {
            tallyType = TallyType.getDefaultType();
        }
        
        
        // ...
        // TBD:
        long lastTime = ShortUrlAccessCronManager.getInstance().processShortUrlAccesses(tallyType);
        log.info("Processed shortUrls: lastTime = " + lastTime);

        
        // Always return 200 ????
        // return 200 only if cnt > 0 ????
        resp.setStatus(StatusCode.OK);
    }

    
    // TBD:
    // depending on mail.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
}
