package com.urltally.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.ws.service.HourlyShortUrlAccessService;
import com.urltally.af.proxy.HourlyShortUrlAccessServiceProxy;


// MockHourlyShortUrlAccessServiceProxy is a decorator.
// It can be used as a base class to mock HourlyShortUrlAccessServiceProxy objects.
public abstract class MockHourlyShortUrlAccessServiceProxy implements HourlyShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(MockHourlyShortUrlAccessServiceProxy.class.getName());

    // MockHourlyShortUrlAccessServiceProxy uses the decorator design pattern.
    private HourlyShortUrlAccessServiceProxy decoratedProxy;

    public MockHourlyShortUrlAccessServiceProxy(HourlyShortUrlAccessServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected HourlyShortUrlAccessServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(HourlyShortUrlAccessServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public HourlyShortUrlAccess getHourlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.getHourlyShortUrlAccess(guid);
    }

    @Override
    public Object getHourlyShortUrlAccess(String guid, String field) throws BaseException
    {
        return decoratedProxy.getHourlyShortUrlAccess(guid, field);       
    }

    @Override
    public List<HourlyShortUrlAccess> getHourlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getHourlyShortUrlAccesses(guids);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses() throws BaseException
    {
        return getAllHourlyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllHourlyShortUrlAccesses(ordering, offset, count);
        return getAllHourlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllHourlyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllHourlyShortUrlAccessKeys(ordering, offset, count);
        return getAllHourlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllHourlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createHourlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {
        return decoratedProxy.createHourlyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
    }

    @Override
    public String createHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        return decoratedProxy.createHourlyShortUrlAccess(hourlyShortUrlAccess);
    }

    @Override
    public Boolean updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {
        return decoratedProxy.updateHourlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
    }

    @Override
    public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        return decoratedProxy.updateHourlyShortUrlAccess(hourlyShortUrlAccess);
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.deleteHourlyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        String guid = hourlyShortUrlAccess.getGuid();
        return deleteHourlyShortUrlAccess(guid);
    }

    @Override
    public Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteHourlyShortUrlAccesses(filter, params, values);
    }

}
