package com.urltally.af.service.proto;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.service.AbstractServiceFactory;
import com.urltally.af.service.ApiConsumerService;
import com.urltally.af.service.UserService;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.af.service.ServiceInfoService;
import com.urltally.af.service.FiveTenService;

public class ProtoServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ProtoServiceFactory.class.getName());

    private ProtoServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ProtoServiceFactoryHolder
    {
        private static final ProtoServiceFactory INSTANCE = new ProtoServiceFactory();
    }

    // Singleton method
    public static ProtoServiceFactory getInstance()
    {
        return ProtoServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerProtoService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserProtoService();
    }

    @Override
    public AccessTallyMasterService getAccessTallyMasterService()
    {
        return new AccessTallyMasterProtoService();
    }

    @Override
    public AccessTallyStatusService getAccessTallyStatusService()
    {
        return new AccessTallyStatusProtoService();
    }

    @Override
    public MonthlyShortUrlAccessService getMonthlyShortUrlAccessService()
    {
        return new MonthlyShortUrlAccessProtoService();
    }

    @Override
    public WeeklyShortUrlAccessService getWeeklyShortUrlAccessService()
    {
        return new WeeklyShortUrlAccessProtoService();
    }

    @Override
    public DailyShortUrlAccessService getDailyShortUrlAccessService()
    {
        return new DailyShortUrlAccessProtoService();
    }

    @Override
    public HourlyShortUrlAccessService getHourlyShortUrlAccessService()
    {
        return new HourlyShortUrlAccessProtoService();
    }

    @Override
    public CumulativeShortUrlAccessService getCumulativeShortUrlAccessService()
    {
        return new CumulativeShortUrlAccessProtoService();
    }

    @Override
    public CurrentShortUrlAccessService getCurrentShortUrlAccessService()
    {
        return new CurrentShortUrlAccessProtoService();
    }

    @Override
    public TotalShortUrlAccessService getTotalShortUrlAccessService()
    {
        return new TotalShortUrlAccessProtoService();
    }

    @Override
    public TotalLongUrlAccessService getTotalLongUrlAccessService()
    {
        return new TotalLongUrlAccessProtoService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoProtoService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenProtoService();
    }


}
