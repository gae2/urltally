package com.urltally.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.af.bean.TotalLongUrlAccessBean;
import com.urltally.ws.service.TotalLongUrlAccessService;
import com.urltally.af.proxy.TotalLongUrlAccessServiceProxy;


// MockTotalLongUrlAccessServiceProxy is a decorator.
// It can be used as a base class to mock TotalLongUrlAccessServiceProxy objects.
public abstract class MockTotalLongUrlAccessServiceProxy implements TotalLongUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(MockTotalLongUrlAccessServiceProxy.class.getName());

    // MockTotalLongUrlAccessServiceProxy uses the decorator design pattern.
    private TotalLongUrlAccessServiceProxy decoratedProxy;

    public MockTotalLongUrlAccessServiceProxy(TotalLongUrlAccessServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TotalLongUrlAccessServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TotalLongUrlAccessServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TotalLongUrlAccess getTotalLongUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.getTotalLongUrlAccess(guid);
    }

    @Override
    public Object getTotalLongUrlAccess(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTotalLongUrlAccess(guid, field);       
    }

    @Override
    public List<TotalLongUrlAccess> getTotalLongUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTotalLongUrlAccesses(guids);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses() throws BaseException
    {
        return getAllTotalLongUrlAccesses(null, null, null);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllTotalLongUrlAccesses(ordering, offset, count);
        return getAllTotalLongUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTotalLongUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllTotalLongUrlAccessKeys(ordering, offset, count);
        return getAllTotalLongUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTotalLongUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTotalLongUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {
        return decoratedProxy.createTotalLongUrlAccess(tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
    }

    @Override
    public String createTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        return decoratedProxy.createTotalLongUrlAccess(totalLongUrlAccess);
    }

    @Override
    public Boolean updateTotalLongUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {
        return decoratedProxy.updateTotalLongUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
    }

    @Override
    public Boolean updateTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        return decoratedProxy.updateTotalLongUrlAccess(totalLongUrlAccess);
    }

    @Override
    public Boolean deleteTotalLongUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.deleteTotalLongUrlAccess(guid);
    }

    @Override
    public Boolean deleteTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        String guid = totalLongUrlAccess.getGuid();
        return deleteTotalLongUrlAccess(guid);
    }

    @Override
    public Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTotalLongUrlAccesses(filter, params, values);
    }

}
