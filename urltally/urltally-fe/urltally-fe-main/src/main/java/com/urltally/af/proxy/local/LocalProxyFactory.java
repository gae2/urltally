package com.urltally.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.ApiConsumerServiceProxy;
import com.urltally.af.proxy.UserServiceProxy;
import com.urltally.af.proxy.AccessTallyMasterServiceProxy;
import com.urltally.af.proxy.AccessTallyStatusServiceProxy;
import com.urltally.af.proxy.MonthlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.WeeklyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.DailyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.HourlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.CumulativeShortUrlAccessServiceProxy;
import com.urltally.af.proxy.CurrentShortUrlAccessServiceProxy;
import com.urltally.af.proxy.TotalShortUrlAccessServiceProxy;
import com.urltally.af.proxy.TotalLongUrlAccessServiceProxy;
import com.urltally.af.proxy.ServiceInfoServiceProxy;
import com.urltally.af.proxy.FiveTenServiceProxy;
import com.urltally.ws.service.ApiConsumerService;
import com.urltally.ws.service.UserService;
import com.urltally.ws.service.AccessTallyMasterService;
import com.urltally.ws.service.AccessTallyStatusService;
import com.urltally.ws.service.MonthlyShortUrlAccessService;
import com.urltally.ws.service.WeeklyShortUrlAccessService;
import com.urltally.ws.service.DailyShortUrlAccessService;
import com.urltally.ws.service.HourlyShortUrlAccessService;
import com.urltally.ws.service.CumulativeShortUrlAccessService;
import com.urltally.ws.service.CurrentShortUrlAccessService;
import com.urltally.ws.service.TotalShortUrlAccessService;
import com.urltally.ws.service.TotalLongUrlAccessService;
import com.urltally.ws.service.ServiceInfoService;
import com.urltally.ws.service.FiveTenService;
import com.urltally.ws.service.impl.ApiConsumerServiceImpl;
import com.urltally.ws.service.impl.UserServiceImpl;
import com.urltally.ws.service.impl.AccessTallyMasterServiceImpl;
import com.urltally.ws.service.impl.AccessTallyStatusServiceImpl;
import com.urltally.ws.service.impl.MonthlyShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.WeeklyShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.DailyShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.HourlyShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.CumulativeShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.CurrentShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.TotalShortUrlAccessServiceImpl;
import com.urltally.ws.service.impl.TotalLongUrlAccessServiceImpl;
import com.urltally.ws.service.impl.ServiceInfoServiceImpl;
import com.urltally.ws.service.impl.FiveTenServiceImpl;


// TBD: How to best inject the ws service instances?
public class LocalProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(LocalProxyFactory.class.getName());

    // Lazy initialization.
    private ApiConsumerServiceProxy apiConsumerServiceProxy = null;
    private UserServiceProxy userServiceProxy = null;
    private AccessTallyMasterServiceProxy accessTallyMasterServiceProxy = null;
    private AccessTallyStatusServiceProxy accessTallyStatusServiceProxy = null;
    private MonthlyShortUrlAccessServiceProxy monthlyShortUrlAccessServiceProxy = null;
    private WeeklyShortUrlAccessServiceProxy weeklyShortUrlAccessServiceProxy = null;
    private DailyShortUrlAccessServiceProxy dailyShortUrlAccessServiceProxy = null;
    private HourlyShortUrlAccessServiceProxy hourlyShortUrlAccessServiceProxy = null;
    private CumulativeShortUrlAccessServiceProxy cumulativeShortUrlAccessServiceProxy = null;
    private CurrentShortUrlAccessServiceProxy currentShortUrlAccessServiceProxy = null;
    private TotalShortUrlAccessServiceProxy totalShortUrlAccessServiceProxy = null;
    private TotalLongUrlAccessServiceProxy totalLongUrlAccessServiceProxy = null;
    private ServiceInfoServiceProxy serviceInfoServiceProxy = null;
    private FiveTenServiceProxy fiveTenServiceProxy = null;


    private LocalProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class LocalProxyFactoryHolder
    {
        private static final LocalProxyFactory INSTANCE = new LocalProxyFactory();
    }

    // Singleton method
    public static LocalProxyFactory getInstance()
    {
        return LocalProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        if(apiConsumerServiceProxy == null) {
            apiConsumerServiceProxy = new LocalApiConsumerServiceProxy();
            ApiConsumerService apiConsumerService = new ApiConsumerServiceImpl();
            ((LocalApiConsumerServiceProxy) apiConsumerServiceProxy).setApiConsumerService(apiConsumerService);
        }
        return apiConsumerServiceProxy;
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        if(userServiceProxy == null) {
            userServiceProxy = new LocalUserServiceProxy();
            UserService userService = new UserServiceImpl();
            ((LocalUserServiceProxy) userServiceProxy).setUserService(userService);
        }
        return userServiceProxy;
    }

    @Override
    public AccessTallyMasterServiceProxy getAccessTallyMasterServiceProxy()
    {
        if(accessTallyMasterServiceProxy == null) {
            accessTallyMasterServiceProxy = new LocalAccessTallyMasterServiceProxy();
            AccessTallyMasterService accessTallyMasterService = new AccessTallyMasterServiceImpl();
            ((LocalAccessTallyMasterServiceProxy) accessTallyMasterServiceProxy).setAccessTallyMasterService(accessTallyMasterService);
        }
        return accessTallyMasterServiceProxy;
    }

    @Override
    public AccessTallyStatusServiceProxy getAccessTallyStatusServiceProxy()
    {
        if(accessTallyStatusServiceProxy == null) {
            accessTallyStatusServiceProxy = new LocalAccessTallyStatusServiceProxy();
            AccessTallyStatusService accessTallyStatusService = new AccessTallyStatusServiceImpl();
            ((LocalAccessTallyStatusServiceProxy) accessTallyStatusServiceProxy).setAccessTallyStatusService(accessTallyStatusService);
        }
        return accessTallyStatusServiceProxy;
    }

    @Override
    public MonthlyShortUrlAccessServiceProxy getMonthlyShortUrlAccessServiceProxy()
    {
        if(monthlyShortUrlAccessServiceProxy == null) {
            monthlyShortUrlAccessServiceProxy = new LocalMonthlyShortUrlAccessServiceProxy();
            MonthlyShortUrlAccessService monthlyShortUrlAccessService = new MonthlyShortUrlAccessServiceImpl();
            ((LocalMonthlyShortUrlAccessServiceProxy) monthlyShortUrlAccessServiceProxy).setMonthlyShortUrlAccessService(monthlyShortUrlAccessService);
        }
        return monthlyShortUrlAccessServiceProxy;
    }

    @Override
    public WeeklyShortUrlAccessServiceProxy getWeeklyShortUrlAccessServiceProxy()
    {
        if(weeklyShortUrlAccessServiceProxy == null) {
            weeklyShortUrlAccessServiceProxy = new LocalWeeklyShortUrlAccessServiceProxy();
            WeeklyShortUrlAccessService weeklyShortUrlAccessService = new WeeklyShortUrlAccessServiceImpl();
            ((LocalWeeklyShortUrlAccessServiceProxy) weeklyShortUrlAccessServiceProxy).setWeeklyShortUrlAccessService(weeklyShortUrlAccessService);
        }
        return weeklyShortUrlAccessServiceProxy;
    }

    @Override
    public DailyShortUrlAccessServiceProxy getDailyShortUrlAccessServiceProxy()
    {
        if(dailyShortUrlAccessServiceProxy == null) {
            dailyShortUrlAccessServiceProxy = new LocalDailyShortUrlAccessServiceProxy();
            DailyShortUrlAccessService dailyShortUrlAccessService = new DailyShortUrlAccessServiceImpl();
            ((LocalDailyShortUrlAccessServiceProxy) dailyShortUrlAccessServiceProxy).setDailyShortUrlAccessService(dailyShortUrlAccessService);
        }
        return dailyShortUrlAccessServiceProxy;
    }

    @Override
    public HourlyShortUrlAccessServiceProxy getHourlyShortUrlAccessServiceProxy()
    {
        if(hourlyShortUrlAccessServiceProxy == null) {
            hourlyShortUrlAccessServiceProxy = new LocalHourlyShortUrlAccessServiceProxy();
            HourlyShortUrlAccessService hourlyShortUrlAccessService = new HourlyShortUrlAccessServiceImpl();
            ((LocalHourlyShortUrlAccessServiceProxy) hourlyShortUrlAccessServiceProxy).setHourlyShortUrlAccessService(hourlyShortUrlAccessService);
        }
        return hourlyShortUrlAccessServiceProxy;
    }

    @Override
    public CumulativeShortUrlAccessServiceProxy getCumulativeShortUrlAccessServiceProxy()
    {
        if(cumulativeShortUrlAccessServiceProxy == null) {
            cumulativeShortUrlAccessServiceProxy = new LocalCumulativeShortUrlAccessServiceProxy();
            CumulativeShortUrlAccessService cumulativeShortUrlAccessService = new CumulativeShortUrlAccessServiceImpl();
            ((LocalCumulativeShortUrlAccessServiceProxy) cumulativeShortUrlAccessServiceProxy).setCumulativeShortUrlAccessService(cumulativeShortUrlAccessService);
        }
        return cumulativeShortUrlAccessServiceProxy;
    }

    @Override
    public CurrentShortUrlAccessServiceProxy getCurrentShortUrlAccessServiceProxy()
    {
        if(currentShortUrlAccessServiceProxy == null) {
            currentShortUrlAccessServiceProxy = new LocalCurrentShortUrlAccessServiceProxy();
            CurrentShortUrlAccessService currentShortUrlAccessService = new CurrentShortUrlAccessServiceImpl();
            ((LocalCurrentShortUrlAccessServiceProxy) currentShortUrlAccessServiceProxy).setCurrentShortUrlAccessService(currentShortUrlAccessService);
        }
        return currentShortUrlAccessServiceProxy;
    }

    @Override
    public TotalShortUrlAccessServiceProxy getTotalShortUrlAccessServiceProxy()
    {
        if(totalShortUrlAccessServiceProxy == null) {
            totalShortUrlAccessServiceProxy = new LocalTotalShortUrlAccessServiceProxy();
            TotalShortUrlAccessService totalShortUrlAccessService = new TotalShortUrlAccessServiceImpl();
            ((LocalTotalShortUrlAccessServiceProxy) totalShortUrlAccessServiceProxy).setTotalShortUrlAccessService(totalShortUrlAccessService);
        }
        return totalShortUrlAccessServiceProxy;
    }

    @Override
    public TotalLongUrlAccessServiceProxy getTotalLongUrlAccessServiceProxy()
    {
        if(totalLongUrlAccessServiceProxy == null) {
            totalLongUrlAccessServiceProxy = new LocalTotalLongUrlAccessServiceProxy();
            TotalLongUrlAccessService totalLongUrlAccessService = new TotalLongUrlAccessServiceImpl();
            ((LocalTotalLongUrlAccessServiceProxy) totalLongUrlAccessServiceProxy).setTotalLongUrlAccessService(totalLongUrlAccessService);
        }
        return totalLongUrlAccessServiceProxy;
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoServiceProxy == null) {
            serviceInfoServiceProxy = new LocalServiceInfoServiceProxy();
            ServiceInfoService serviceInfoService = new ServiceInfoServiceImpl();
            ((LocalServiceInfoServiceProxy) serviceInfoServiceProxy).setServiceInfoService(serviceInfoService);
        }
        return serviceInfoServiceProxy;
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenServiceProxy == null) {
            fiveTenServiceProxy = new LocalFiveTenServiceProxy();
            FiveTenService fiveTenService = new FiveTenServiceImpl();
            ((LocalFiveTenServiceProxy) fiveTenServiceProxy).setFiveTenService(fiveTenService);
        }
        return fiveTenServiceProxy;
    }

}
