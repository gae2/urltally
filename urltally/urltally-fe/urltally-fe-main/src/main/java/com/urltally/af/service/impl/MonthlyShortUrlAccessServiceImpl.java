package com.urltally.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.MonthlyShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class MonthlyShortUrlAccessServiceImpl implements MonthlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MonthlyShortUrlAccessServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "MonthlyShortUrlAccess-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("MonthlyShortUrlAccess:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public MonthlyShortUrlAccessServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // MonthlyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public MonthlyShortUrlAccess getMonthlyShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getMonthlyShortUrlAccess(): guid = " + guid);

        MonthlyShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (MonthlyShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (MonthlyShortUrlAccessBean) getProxyFactory().getMonthlyShortUrlAccessServiceProxy().getMonthlyShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "MonthlyShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccessBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getMonthlyShortUrlAccess(String guid, String field) throws BaseException
    {
        MonthlyShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (MonthlyShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (MonthlyShortUrlAccessBean) getProxyFactory().getMonthlyShortUrlAccessServiceProxy().getMonthlyShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "MonthlyShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccessBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return bean.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return bean.getTallyEpoch();
        } else if(field.equals("count")) {
            return bean.getCount();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return bean.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return bean.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return bean.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return bean.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return bean.getUserAgent();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("country")) {
            return bean.getCountry();
        } else if(field.equals("talliedTime")) {
            return bean.getTalliedTime();
        } else if(field.equals("year")) {
            return bean.getYear();
        } else if(field.equals("month")) {
            return bean.getMonth();
        } else if(field.equals("numberOfDays")) {
            return bean.getNumberOfDays();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<MonthlyShortUrlAccess> getMonthlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getMonthlyShortUrlAccesses()");

        // TBD: Is there a better way????
        List<MonthlyShortUrlAccess> monthlyShortUrlAccesses = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().getMonthlyShortUrlAccesses(guids);
        if(monthlyShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccessBean list.");
        }

        log.finer("END");
        return monthlyShortUrlAccesses;
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses() throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllMonthlyShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<MonthlyShortUrlAccess> monthlyShortUrlAccesses = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(monthlyShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccessBean list.");
        }

        log.finer("END");
        return monthlyShortUrlAccesses;
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllMonthlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllMonthlyShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve MonthlyShortUrlAccessBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty MonthlyShortUrlAccessBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("MonthlyShortUrlAccessServiceImpl.findMonthlyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<MonthlyShortUrlAccess> monthlyShortUrlAccesses = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(monthlyShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to find monthlyShortUrlAccesses for the given criterion.");
        }

        log.finer("END");
        return monthlyShortUrlAccesses;
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("MonthlyShortUrlAccessServiceImpl.findMonthlyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find MonthlyShortUrlAccess keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty MonthlyShortUrlAccess key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("MonthlyShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createMonthlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        MonthlyShortUrlAccessBean bean = new MonthlyShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        return createMonthlyShortUrlAccess(bean);
    }

    @Override
    public String createMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //MonthlyShortUrlAccess bean = constructMonthlyShortUrlAccess(monthlyShortUrlAccess);
        //return bean.getGuid();

        // Param monthlyShortUrlAccess cannot be null.....
        if(monthlyShortUrlAccess == null) {
            log.log(Level.INFO, "Param monthlyShortUrlAccess is null!");
            throw new BadRequestException("Param monthlyShortUrlAccess object is null!");
        }
        MonthlyShortUrlAccessBean bean = null;
        if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessBean) {
            bean = (MonthlyShortUrlAccessBean) monthlyShortUrlAccess;
        } else if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccess) {
            // bean = new MonthlyShortUrlAccessBean(null, monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new MonthlyShortUrlAccessBean(monthlyShortUrlAccess.getGuid(), monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
        } else {
            log.log(Level.WARNING, "createMonthlyShortUrlAccess(): Arg monthlyShortUrlAccess is of an unknown type.");
            //bean = new MonthlyShortUrlAccessBean();
            bean = new MonthlyShortUrlAccessBean(monthlyShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().createMonthlyShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public MonthlyShortUrlAccess constructMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param monthlyShortUrlAccess cannot be null.....
        if(monthlyShortUrlAccess == null) {
            log.log(Level.INFO, "Param monthlyShortUrlAccess is null!");
            throw new BadRequestException("Param monthlyShortUrlAccess object is null!");
        }
        MonthlyShortUrlAccessBean bean = null;
        if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessBean) {
            bean = (MonthlyShortUrlAccessBean) monthlyShortUrlAccess;
        } else if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccess) {
            // bean = new MonthlyShortUrlAccessBean(null, monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new MonthlyShortUrlAccessBean(monthlyShortUrlAccess.getGuid(), monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
        } else {
            log.log(Level.WARNING, "createMonthlyShortUrlAccess(): Arg monthlyShortUrlAccess is of an unknown type.");
            //bean = new MonthlyShortUrlAccessBean();
            bean = new MonthlyShortUrlAccessBean(monthlyShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().createMonthlyShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateMonthlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        MonthlyShortUrlAccessBean bean = new MonthlyShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        return updateMonthlyShortUrlAccess(bean);
    }
        
    @Override
    public Boolean updateMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //MonthlyShortUrlAccess bean = refreshMonthlyShortUrlAccess(monthlyShortUrlAccess);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param monthlyShortUrlAccess cannot be null.....
        if(monthlyShortUrlAccess == null || monthlyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param monthlyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param monthlyShortUrlAccess object or its guid is null!");
        }
        MonthlyShortUrlAccessBean bean = null;
        if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessBean) {
            bean = (MonthlyShortUrlAccessBean) monthlyShortUrlAccess;
        } else {  // if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccess)
            bean = new MonthlyShortUrlAccessBean(monthlyShortUrlAccess.getGuid(), monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
        }
        Boolean suc = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().updateMonthlyShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public MonthlyShortUrlAccess refreshMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param monthlyShortUrlAccess cannot be null.....
        if(monthlyShortUrlAccess == null || monthlyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param monthlyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param monthlyShortUrlAccess object or its guid is null!");
        }
        MonthlyShortUrlAccessBean bean = null;
        if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessBean) {
            bean = (MonthlyShortUrlAccessBean) monthlyShortUrlAccess;
        } else {  // if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccess)
            bean = new MonthlyShortUrlAccessBean(monthlyShortUrlAccess.getGuid(), monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
        }
        Boolean suc = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().updateMonthlyShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteMonthlyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().deleteMonthlyShortUrlAccess(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            MonthlyShortUrlAccess monthlyShortUrlAccess = null;
            try {
                monthlyShortUrlAccess = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().getMonthlyShortUrlAccess(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch monthlyShortUrlAccess with a key, " + guid);
                return false;
            }
            if(monthlyShortUrlAccess != null) {
                String beanGuid = monthlyShortUrlAccess.getGuid();
                Boolean suc1 = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().deleteMonthlyShortUrlAccess(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("monthlyShortUrlAccess with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param monthlyShortUrlAccess cannot be null.....
        if(monthlyShortUrlAccess == null || monthlyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param monthlyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param monthlyShortUrlAccess object or its guid is null!");
        }
        MonthlyShortUrlAccessBean bean = null;
        if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccessBean) {
            bean = (MonthlyShortUrlAccessBean) monthlyShortUrlAccess;
        } else {  // if(monthlyShortUrlAccess instanceof MonthlyShortUrlAccess)
            // ????
            log.warning("monthlyShortUrlAccess is not an instance of MonthlyShortUrlAccessBean.");
            bean = new MonthlyShortUrlAccessBean(monthlyShortUrlAccess.getGuid(), monthlyShortUrlAccess.getTallyTime(), monthlyShortUrlAccess.getTallyEpoch(), monthlyShortUrlAccess.getCount(), monthlyShortUrlAccess.getShortUrl(), monthlyShortUrlAccess.getShortUrlDomain(), monthlyShortUrlAccess.getLongUrl(), monthlyShortUrlAccess.getLongUrlDomain(), monthlyShortUrlAccess.getRedirectType(), monthlyShortUrlAccess.getRefererDomain(), monthlyShortUrlAccess.getUserAgent(), monthlyShortUrlAccess.getLanguage(), monthlyShortUrlAccess.getCountry(), monthlyShortUrlAccess.getTalliedTime(), monthlyShortUrlAccess.getYear(), monthlyShortUrlAccess.getMonth(), monthlyShortUrlAccess.getNumberOfDays());
        }
        Boolean suc = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().deleteMonthlyShortUrlAccess(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getMonthlyShortUrlAccessServiceProxy().deleteMonthlyShortUrlAccesses(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createMonthlyShortUrlAccesses(List<MonthlyShortUrlAccess> monthlyShortUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");

        if(monthlyShortUrlAccesses == null) {
            log.log(Level.WARNING, "createMonthlyShortUrlAccesses() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = monthlyShortUrlAccesses.size();
        if(size == 0) {
            log.log(Level.WARNING, "createMonthlyShortUrlAccesses() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(MonthlyShortUrlAccess monthlyShortUrlAccess : monthlyShortUrlAccesses) {
            String guid = createMonthlyShortUrlAccess(monthlyShortUrlAccess);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createMonthlyShortUrlAccesses() failed for at least one monthlyShortUrlAccess. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateMonthlyShortUrlAccesses(List<MonthlyShortUrlAccess> monthlyShortUrlAccesses) throws BaseException
    //{
    //}

}
