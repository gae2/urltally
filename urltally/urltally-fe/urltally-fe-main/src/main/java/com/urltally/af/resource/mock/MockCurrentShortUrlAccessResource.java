package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.stub.CurrentShortUrlAccessStub;
import com.urltally.ws.stub.CurrentShortUrlAccessListStub;
import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.resource.CurrentShortUrlAccessResource;


// MockCurrentShortUrlAccessResource is a decorator.
// It can be used as a base class to mock CurrentShortUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/currentShortUrlAccesses/")
public abstract class MockCurrentShortUrlAccessResource implements CurrentShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockCurrentShortUrlAccessResource.class.getName());

    // MockCurrentShortUrlAccessResource uses the decorator design pattern.
    private CurrentShortUrlAccessResource decoratedResource;

    public MockCurrentShortUrlAccessResource(CurrentShortUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected CurrentShortUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(CurrentShortUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllCurrentShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllCurrentShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findCurrentShortUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findCurrentShortUrlAccessesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getCurrentShortUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getCurrentShortUrlAccessAsHtml(guid);
//    }

    @Override
    public Response getCurrentShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getCurrentShortUrlAccess(guid);
    }

    @Override
    public Response getCurrentShortUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getCurrentShortUrlAccessAsJsonp(guid, callback);
    }

    @Override
    public Response getCurrentShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getCurrentShortUrlAccess(guid, field);
    }

    // TBD
    @Override
    public Response constructCurrentShortUrlAccess(CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.constructCurrentShortUrlAccess(currentShortUrlAccess);
    }

    @Override
    public Response createCurrentShortUrlAccess(CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createCurrentShortUrlAccess(currentShortUrlAccess);
    }

//    @Override
//    public Response createCurrentShortUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createCurrentShortUrlAccess(formParams);
//    }

    // TBD
    @Override
    public Response refreshCurrentShortUrlAccess(String guid, CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.refreshCurrentShortUrlAccess(guid, currentShortUrlAccess);
    }

    @Override
    public Response updateCurrentShortUrlAccess(String guid, CurrentShortUrlAccessStub currentShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateCurrentShortUrlAccess(guid, currentShortUrlAccess);
    }

    @Override
    public Response updateCurrentShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime)
    {
        return decoratedResource.updateCurrentShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
    }

//    @Override
//    public Response updateCurrentShortUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateCurrentShortUrlAccess(guid, formParams);
//    }

    @Override
    public Response deleteCurrentShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteCurrentShortUrlAccess(guid);
    }

    @Override
    public Response deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteCurrentShortUrlAccesses(filter, params, values);
    }


// TBD ....
    @Override
    public Response createCurrentShortUrlAccesses(CurrentShortUrlAccessListStub currentShortUrlAccesses) throws BaseResourceException
    {
        return decoratedResource.createCurrentShortUrlAccesses(currentShortUrlAccesses);
    }


}
