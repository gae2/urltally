package com.urltally.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.service.ApiConsumerService;
import com.urltally.af.service.UserService;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.af.service.ServiceInfoService;
import com.urltally.af.service.FiveTenService;

// TBD: DI
// (For now, this class is used to make ServiceManager "DI-capable".)
public class ServiceController
{
    private static final Logger log = Logger.getLogger(ServiceController.class.getName());

    // "Injectable" services.
    private ApiConsumerService apiConsumerService = null;
    private UserService userService = null;
    private AccessTallyMasterService accessTallyMasterService = null;
    private AccessTallyStatusService accessTallyStatusService = null;
    private MonthlyShortUrlAccessService monthlyShortUrlAccessService = null;
    private WeeklyShortUrlAccessService weeklyShortUrlAccessService = null;
    private DailyShortUrlAccessService dailyShortUrlAccessService = null;
    private HourlyShortUrlAccessService hourlyShortUrlAccessService = null;
    private CumulativeShortUrlAccessService cumulativeShortUrlAccessService = null;
    private CurrentShortUrlAccessService currentShortUrlAccessService = null;
    private TotalShortUrlAccessService totalShortUrlAccessService = null;
    private TotalLongUrlAccessService totalLongUrlAccessService = null;
    private ServiceInfoService serviceInfoService = null;
    private FiveTenService fiveTenService = null;

    // Ctor injection.
    public ServiceController(ApiConsumerService apiConsumerService, UserService userService, AccessTallyMasterService accessTallyMasterService, AccessTallyStatusService accessTallyStatusService, MonthlyShortUrlAccessService monthlyShortUrlAccessService, WeeklyShortUrlAccessService weeklyShortUrlAccessService, DailyShortUrlAccessService dailyShortUrlAccessService, HourlyShortUrlAccessService hourlyShortUrlAccessService, CumulativeShortUrlAccessService cumulativeShortUrlAccessService, CurrentShortUrlAccessService currentShortUrlAccessService, TotalShortUrlAccessService totalShortUrlAccessService, TotalLongUrlAccessService totalLongUrlAccessService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.accessTallyMasterService = accessTallyMasterService;
        this.accessTallyStatusService = accessTallyStatusService;
        this.monthlyShortUrlAccessService = monthlyShortUrlAccessService;
        this.weeklyShortUrlAccessService = weeklyShortUrlAccessService;
        this.dailyShortUrlAccessService = dailyShortUrlAccessService;
        this.hourlyShortUrlAccessService = hourlyShortUrlAccessService;
        this.cumulativeShortUrlAccessService = cumulativeShortUrlAccessService;
        this.currentShortUrlAccessService = currentShortUrlAccessService;
        this.totalShortUrlAccessService = totalShortUrlAccessService;
        this.totalLongUrlAccessService = totalLongUrlAccessService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }

    // Returns a ApiConsumerService instance.
	public ApiConsumerService getApiConsumerService() 
    {
        return this.apiConsumerService;
    }
    // Setter injection for ApiConsumerService.
	public void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        this.apiConsumerService = apiConsumerService;
    }

    // Returns a UserService instance.
	public UserService getUserService() 
    {
        return this.userService;
    }
    // Setter injection for UserService.
	public void setUserService(UserService userService) 
    {
        this.userService = userService;
    }

    // Returns a AccessTallyMasterService instance.
	public AccessTallyMasterService getAccessTallyMasterService() 
    {
        return this.accessTallyMasterService;
    }
    // Setter injection for AccessTallyMasterService.
	public void setAccessTallyMasterService(AccessTallyMasterService accessTallyMasterService) 
    {
        this.accessTallyMasterService = accessTallyMasterService;
    }

    // Returns a AccessTallyStatusService instance.
	public AccessTallyStatusService getAccessTallyStatusService() 
    {
        return this.accessTallyStatusService;
    }
    // Setter injection for AccessTallyStatusService.
	public void setAccessTallyStatusService(AccessTallyStatusService accessTallyStatusService) 
    {
        this.accessTallyStatusService = accessTallyStatusService;
    }

    // Returns a MonthlyShortUrlAccessService instance.
	public MonthlyShortUrlAccessService getMonthlyShortUrlAccessService() 
    {
        return this.monthlyShortUrlAccessService;
    }
    // Setter injection for MonthlyShortUrlAccessService.
	public void setMonthlyShortUrlAccessService(MonthlyShortUrlAccessService monthlyShortUrlAccessService) 
    {
        this.monthlyShortUrlAccessService = monthlyShortUrlAccessService;
    }

    // Returns a WeeklyShortUrlAccessService instance.
	public WeeklyShortUrlAccessService getWeeklyShortUrlAccessService() 
    {
        return this.weeklyShortUrlAccessService;
    }
    // Setter injection for WeeklyShortUrlAccessService.
	public void setWeeklyShortUrlAccessService(WeeklyShortUrlAccessService weeklyShortUrlAccessService) 
    {
        this.weeklyShortUrlAccessService = weeklyShortUrlAccessService;
    }

    // Returns a DailyShortUrlAccessService instance.
	public DailyShortUrlAccessService getDailyShortUrlAccessService() 
    {
        return this.dailyShortUrlAccessService;
    }
    // Setter injection for DailyShortUrlAccessService.
	public void setDailyShortUrlAccessService(DailyShortUrlAccessService dailyShortUrlAccessService) 
    {
        this.dailyShortUrlAccessService = dailyShortUrlAccessService;
    }

    // Returns a HourlyShortUrlAccessService instance.
	public HourlyShortUrlAccessService getHourlyShortUrlAccessService() 
    {
        return this.hourlyShortUrlAccessService;
    }
    // Setter injection for HourlyShortUrlAccessService.
	public void setHourlyShortUrlAccessService(HourlyShortUrlAccessService hourlyShortUrlAccessService) 
    {
        this.hourlyShortUrlAccessService = hourlyShortUrlAccessService;
    }

    // Returns a CumulativeShortUrlAccessService instance.
	public CumulativeShortUrlAccessService getCumulativeShortUrlAccessService() 
    {
        return this.cumulativeShortUrlAccessService;
    }
    // Setter injection for CumulativeShortUrlAccessService.
	public void setCumulativeShortUrlAccessService(CumulativeShortUrlAccessService cumulativeShortUrlAccessService) 
    {
        this.cumulativeShortUrlAccessService = cumulativeShortUrlAccessService;
    }

    // Returns a CurrentShortUrlAccessService instance.
	public CurrentShortUrlAccessService getCurrentShortUrlAccessService() 
    {
        return this.currentShortUrlAccessService;
    }
    // Setter injection for CurrentShortUrlAccessService.
	public void setCurrentShortUrlAccessService(CurrentShortUrlAccessService currentShortUrlAccessService) 
    {
        this.currentShortUrlAccessService = currentShortUrlAccessService;
    }

    // Returns a TotalShortUrlAccessService instance.
	public TotalShortUrlAccessService getTotalShortUrlAccessService() 
    {
        return this.totalShortUrlAccessService;
    }
    // Setter injection for TotalShortUrlAccessService.
	public void setTotalShortUrlAccessService(TotalShortUrlAccessService totalShortUrlAccessService) 
    {
        this.totalShortUrlAccessService = totalShortUrlAccessService;
    }

    // Returns a TotalLongUrlAccessService instance.
	public TotalLongUrlAccessService getTotalLongUrlAccessService() 
    {
        return this.totalLongUrlAccessService;
    }
    // Setter injection for TotalLongUrlAccessService.
	public void setTotalLongUrlAccessService(TotalLongUrlAccessService totalLongUrlAccessService) 
    {
        this.totalLongUrlAccessService = totalLongUrlAccessService;
    }

    // Returns a ServiceInfoService instance.
	public ServiceInfoService getServiceInfoService() 
    {
        return this.serviceInfoService;
    }
    // Setter injection for ServiceInfoService.
	public void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        this.serviceInfoService = serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public FiveTenService getFiveTenService() 
    {
        return this.fiveTenService;
    }
    // Setter injection for FiveTenService.
	public void setFiveTenService(FiveTenService fiveTenService) 
    {
        this.fiveTenService = fiveTenService;
    }

}
