package com.urltally.af.service;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.DailyShortUrlAccess;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface DailyShortUrlAccessService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    DailyShortUrlAccess getDailyShortUrlAccess(String guid) throws BaseException;
    Object getDailyShortUrlAccess(String guid, String field) throws BaseException;
    List<DailyShortUrlAccess> getDailyShortUrlAccesses(List<String> guids) throws BaseException;
    List<DailyShortUrlAccess> getAllDailyShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createDailyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException;
    //String createDailyShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return DailyShortUrlAccess?)
    String createDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException;
    DailyShortUrlAccess constructDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException;
    Boolean updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException;
    //Boolean updateDailyShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException;
    DailyShortUrlAccess refreshDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException;
    Boolean deleteDailyShortUrlAccess(String guid) throws BaseException;
    Boolean deleteDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException;
    Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createDailyShortUrlAccesses(List<DailyShortUrlAccess> dailyShortUrlAccesses) throws BaseException;
//    Boolean updateDailyShortUrlAccesses(List<DailyShortUrlAccess> dailyShortUrlAccesses) throws BaseException;

}
