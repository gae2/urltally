package com.urltally.app.util;

import java.util.logging.Logger;

import com.urltally.af.config.Config;


// For fetching system default values.
// User-specific default values/settings should be read from somewhere else...
public class ConfigUtil
{
    private static final Logger log = Logger.getLogger(ConfigUtil.class.getName());
    private static Config sConfig = null;

    // Config keys... 
    private static final String KEY_APPLICATION_BRAND = "urltallyapp.application.brand";
    private static final String KEY_TALLY_PROCESSING_MAXCOUNT = "urltallyapp.tally.processing.maxcount";
    // ...
    // etc...
    
    // "Cache" values.  (TBD: Convert types to enums????)
    private static String sApplicationBrand = null;
    private static Integer sTallyProcessingMaxCount = null;
    // ...
 
    
    // Static methods only.
    private ConfigUtil() {}

    
//    // App "brand" 
//    public static String getApplicationBrand()
//    {
//        if(sApplicationBrand == null) {
//            sApplicationBrand = Config.getInstance().getString(KEY_APPLICATION_BRAND, AppBrand.getDefaultBrand());
//        }
//        return sApplicationBrand;
//    }

    // Max record count for processing...
    public static int getTallyProcessingMaxCount()
    {
        if(sTallyProcessingMaxCount == null) {
            sTallyProcessingMaxCount = Config.getInstance().getInteger(KEY_TALLY_PROCESSING_MAXCOUNT, 1000);   // ????
        }
        return sTallyProcessingMaxCount;
    }


}
