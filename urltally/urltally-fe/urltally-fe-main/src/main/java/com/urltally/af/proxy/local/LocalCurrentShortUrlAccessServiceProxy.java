package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CurrentShortUrlAccess;
// import com.urltally.ws.bean.CurrentShortUrlAccessBean;
import com.urltally.ws.service.CurrentShortUrlAccessService;
import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.proxy.CurrentShortUrlAccessServiceProxy;


public class LocalCurrentShortUrlAccessServiceProxy extends BaseLocalServiceProxy implements CurrentShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalCurrentShortUrlAccessServiceProxy.class.getName());

    public LocalCurrentShortUrlAccessServiceProxy()
    {
    }

    @Override
    public CurrentShortUrlAccess getCurrentShortUrlAccess(String guid) throws BaseException
    {
        CurrentShortUrlAccess serverBean = getCurrentShortUrlAccessService().getCurrentShortUrlAccess(guid);
        CurrentShortUrlAccess appBean = convertServerCurrentShortUrlAccessBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getCurrentShortUrlAccess(String guid, String field) throws BaseException
    {
        return getCurrentShortUrlAccessService().getCurrentShortUrlAccess(guid, field);       
    }

    @Override
    public List<CurrentShortUrlAccess> getCurrentShortUrlAccesses(List<String> guids) throws BaseException
    {
        List<CurrentShortUrlAccess> serverBeanList = getCurrentShortUrlAccessService().getCurrentShortUrlAccesses(guids);
        List<CurrentShortUrlAccess> appBeanList = convertServerCurrentShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses() throws BaseException
    {
        return getAllCurrentShortUrlAccesses(null, null, null);
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getCurrentShortUrlAccessService().getAllCurrentShortUrlAccesses(ordering, offset, count);
        return getAllCurrentShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<CurrentShortUrlAccess> serverBeanList = getCurrentShortUrlAccessService().getAllCurrentShortUrlAccesses(ordering, offset, count, forwardCursor);
        List<CurrentShortUrlAccess> appBeanList = convertServerCurrentShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getCurrentShortUrlAccessService().getAllCurrentShortUrlAccessKeys(ordering, offset, count);
        return getAllCurrentShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getCurrentShortUrlAccessService().getAllCurrentShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getCurrentShortUrlAccessService().findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<CurrentShortUrlAccess> serverBeanList = getCurrentShortUrlAccessService().findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<CurrentShortUrlAccess> appBeanList = convertServerCurrentShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getCurrentShortUrlAccessService().findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getCurrentShortUrlAccessService().findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getCurrentShortUrlAccessService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createCurrentShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException
    {
        return getCurrentShortUrlAccessService().createCurrentShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
    }

    @Override
    public String createCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.CurrentShortUrlAccessBean serverBean =  convertAppCurrentShortUrlAccessBeanToServerBean(currentShortUrlAccess);
        return getCurrentShortUrlAccessService().createCurrentShortUrlAccess(serverBean);
    }

    @Override
    public Boolean updateCurrentShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException
    {
        return getCurrentShortUrlAccessService().updateCurrentShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
    }

    @Override
    public Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.CurrentShortUrlAccessBean serverBean =  convertAppCurrentShortUrlAccessBeanToServerBean(currentShortUrlAccess);
        return getCurrentShortUrlAccessService().updateCurrentShortUrlAccess(serverBean);
    }

    @Override
    public Boolean deleteCurrentShortUrlAccess(String guid) throws BaseException
    {
        return getCurrentShortUrlAccessService().deleteCurrentShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.CurrentShortUrlAccessBean serverBean =  convertAppCurrentShortUrlAccessBeanToServerBean(currentShortUrlAccess);
        return getCurrentShortUrlAccessService().deleteCurrentShortUrlAccess(serverBean);
    }

    @Override
    public Long deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return getCurrentShortUrlAccessService().deleteCurrentShortUrlAccesses(filter, params, values);
    }




    public static CurrentShortUrlAccessBean convertServerCurrentShortUrlAccessBeanToAppBean(CurrentShortUrlAccess serverBean)
    {
        CurrentShortUrlAccessBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new CurrentShortUrlAccessBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setCount(serverBean.getCount());
            bean.setShortUrl(serverBean.getShortUrl());
            bean.setShortUrlDomain(serverBean.getShortUrlDomain());
            bean.setLongUrl(serverBean.getLongUrl());
            bean.setLongUrlDomain(serverBean.getLongUrlDomain());
            bean.setRedirectType(serverBean.getRedirectType());
            bean.setRefererDomain(serverBean.getRefererDomain());
            bean.setUserAgent(serverBean.getUserAgent());
            bean.setLanguage(serverBean.getLanguage());
            bean.setCountry(serverBean.getCountry());
            bean.setTalliedTime(serverBean.getTalliedTime());
            bean.setStartDayHour(serverBean.getStartDayHour());
            bean.setStartTime(serverBean.getStartTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<CurrentShortUrlAccess> convertServerCurrentShortUrlAccessBeanListToAppBeanList(List<CurrentShortUrlAccess> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<CurrentShortUrlAccess> beanList = new ArrayList<CurrentShortUrlAccess>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(CurrentShortUrlAccess sb : serverBeanList) {
                CurrentShortUrlAccessBean bean = convertServerCurrentShortUrlAccessBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.CurrentShortUrlAccessBean convertAppCurrentShortUrlAccessBeanToServerBean(CurrentShortUrlAccess appBean)
    {
        com.urltally.ws.bean.CurrentShortUrlAccessBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.CurrentShortUrlAccessBean();
            bean.setGuid(appBean.getGuid());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setCount(appBean.getCount());
            bean.setShortUrl(appBean.getShortUrl());
            bean.setShortUrlDomain(appBean.getShortUrlDomain());
            bean.setLongUrl(appBean.getLongUrl());
            bean.setLongUrlDomain(appBean.getLongUrlDomain());
            bean.setRedirectType(appBean.getRedirectType());
            bean.setRefererDomain(appBean.getRefererDomain());
            bean.setUserAgent(appBean.getUserAgent());
            bean.setLanguage(appBean.getLanguage());
            bean.setCountry(appBean.getCountry());
            bean.setTalliedTime(appBean.getTalliedTime());
            bean.setStartDayHour(appBean.getStartDayHour());
            bean.setStartTime(appBean.getStartTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<CurrentShortUrlAccess> convertAppCurrentShortUrlAccessBeanListToServerBeanList(List<CurrentShortUrlAccess> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<CurrentShortUrlAccess> beanList = new ArrayList<CurrentShortUrlAccess>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(CurrentShortUrlAccess sb : appBeanList) {
                com.urltally.ws.bean.CurrentShortUrlAccessBean bean = convertAppCurrentShortUrlAccessBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
