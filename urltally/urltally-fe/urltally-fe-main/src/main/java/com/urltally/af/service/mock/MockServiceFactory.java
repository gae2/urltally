package com.urltally.af.service.mock;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.service.AbstractServiceFactory;
import com.urltally.af.service.ApiConsumerService;
import com.urltally.af.service.UserService;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.af.service.ServiceInfoService;
import com.urltally.af.service.FiveTenService;


// Create your own mock object factory using MockServiceFactory as a template.
public class MockServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(MockServiceFactory.class.getName());

    // Using the Decorator pattern.
    private AbstractServiceFactory decoratedServiceFactory;
    private MockServiceFactory()
    {
        this(null);   // ????
    }
    private MockServiceFactory(AbstractServiceFactory decoratedServiceFactory)
    {
        this.decoratedServiceFactory = decoratedServiceFactory;
    }

    // Initialization-on-demand holder.
    private static class MockServiceFactoryHolder
    {
        private static final MockServiceFactory INSTANCE = new MockServiceFactory();
    }

    // Singleton method
    public static MockServiceFactory getInstance()
    {
        return MockServiceFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public AbstractServiceFactory getDecoratedServiceFactory()
    {
        return decoratedServiceFactory;
    }
    public void setDecoratedServiceFactory(AbstractServiceFactory decoratedServiceFactory)
    {
        this.decoratedServiceFactory = decoratedServiceFactory;
    }


    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerMockService(decoratedServiceFactory.getApiConsumerService()) {};
    }

    @Override
    public UserService getUserService()
    {
        return new UserMockService(decoratedServiceFactory.getUserService()) {};
    }

    @Override
    public AccessTallyMasterService getAccessTallyMasterService()
    {
        return new AccessTallyMasterMockService(decoratedServiceFactory.getAccessTallyMasterService()) {};
    }

    @Override
    public AccessTallyStatusService getAccessTallyStatusService()
    {
        return new AccessTallyStatusMockService(decoratedServiceFactory.getAccessTallyStatusService()) {};
    }

    @Override
    public MonthlyShortUrlAccessService getMonthlyShortUrlAccessService()
    {
        return new MonthlyShortUrlAccessMockService(decoratedServiceFactory.getMonthlyShortUrlAccessService()) {};
    }

    @Override
    public WeeklyShortUrlAccessService getWeeklyShortUrlAccessService()
    {
        return new WeeklyShortUrlAccessMockService(decoratedServiceFactory.getWeeklyShortUrlAccessService()) {};
    }

    @Override
    public DailyShortUrlAccessService getDailyShortUrlAccessService()
    {
        return new DailyShortUrlAccessMockService(decoratedServiceFactory.getDailyShortUrlAccessService()) {};
    }

    @Override
    public HourlyShortUrlAccessService getHourlyShortUrlAccessService()
    {
        return new HourlyShortUrlAccessMockService(decoratedServiceFactory.getHourlyShortUrlAccessService()) {};
    }

    @Override
    public CumulativeShortUrlAccessService getCumulativeShortUrlAccessService()
    {
        return new CumulativeShortUrlAccessMockService(decoratedServiceFactory.getCumulativeShortUrlAccessService()) {};
    }

    @Override
    public CurrentShortUrlAccessService getCurrentShortUrlAccessService()
    {
        return new CurrentShortUrlAccessMockService(decoratedServiceFactory.getCurrentShortUrlAccessService()) {};
    }

    @Override
    public TotalShortUrlAccessService getTotalShortUrlAccessService()
    {
        return new TotalShortUrlAccessMockService(decoratedServiceFactory.getTotalShortUrlAccessService()) {};
    }

    @Override
    public TotalLongUrlAccessService getTotalLongUrlAccessService()
    {
        return new TotalLongUrlAccessMockService(decoratedServiceFactory.getTotalLongUrlAccessService()) {};
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoMockService(decoratedServiceFactory.getServiceInfoService()) {};
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenMockService(decoratedServiceFactory.getFiveTenService()) {};
    }


}
