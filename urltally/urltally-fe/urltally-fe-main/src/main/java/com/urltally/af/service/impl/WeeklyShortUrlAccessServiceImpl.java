package com.urltally.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.WeeklyShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class WeeklyShortUrlAccessServiceImpl implements WeeklyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(WeeklyShortUrlAccessServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "WeeklyShortUrlAccess-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("WeeklyShortUrlAccess:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public WeeklyShortUrlAccessServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // WeeklyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public WeeklyShortUrlAccess getWeeklyShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getWeeklyShortUrlAccess(): guid = " + guid);

        WeeklyShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (WeeklyShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (WeeklyShortUrlAccessBean) getProxyFactory().getWeeklyShortUrlAccessServiceProxy().getWeeklyShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "WeeklyShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccessBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getWeeklyShortUrlAccess(String guid, String field) throws BaseException
    {
        WeeklyShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (WeeklyShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (WeeklyShortUrlAccessBean) getProxyFactory().getWeeklyShortUrlAccessServiceProxy().getWeeklyShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "WeeklyShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccessBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return bean.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return bean.getTallyEpoch();
        } else if(field.equals("count")) {
            return bean.getCount();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return bean.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return bean.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return bean.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return bean.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return bean.getUserAgent();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("country")) {
            return bean.getCountry();
        } else if(field.equals("talliedTime")) {
            return bean.getTalliedTime();
        } else if(field.equals("year")) {
            return bean.getYear();
        } else if(field.equals("week")) {
            return bean.getWeek();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<WeeklyShortUrlAccess> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getWeeklyShortUrlAccesses()");

        // TBD: Is there a better way????
        List<WeeklyShortUrlAccess> weeklyShortUrlAccesses = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().getWeeklyShortUrlAccesses(guids);
        if(weeklyShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccessBean list.");
        }

        log.finer("END");
        return weeklyShortUrlAccesses;
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses() throws BaseException
    {
        return getAllWeeklyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllWeeklyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllWeeklyShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<WeeklyShortUrlAccess> weeklyShortUrlAccesses = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().getAllWeeklyShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(weeklyShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccessBean list.");
        }

        log.finer("END");
        return weeklyShortUrlAccesses;
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllWeeklyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllWeeklyShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().getAllWeeklyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve WeeklyShortUrlAccessBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty WeeklyShortUrlAccessBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("WeeklyShortUrlAccessServiceImpl.findWeeklyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<WeeklyShortUrlAccess> weeklyShortUrlAccesses = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(weeklyShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to find weeklyShortUrlAccesses for the given criterion.");
        }

        log.finer("END");
        return weeklyShortUrlAccesses;
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("WeeklyShortUrlAccessServiceImpl.findWeeklyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find WeeklyShortUrlAccess keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty WeeklyShortUrlAccess key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("WeeklyShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createWeeklyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        WeeklyShortUrlAccessBean bean = new WeeklyShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
        return createWeeklyShortUrlAccess(bean);
    }

    @Override
    public String createWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //WeeklyShortUrlAccess bean = constructWeeklyShortUrlAccess(weeklyShortUrlAccess);
        //return bean.getGuid();

        // Param weeklyShortUrlAccess cannot be null.....
        if(weeklyShortUrlAccess == null) {
            log.log(Level.INFO, "Param weeklyShortUrlAccess is null!");
            throw new BadRequestException("Param weeklyShortUrlAccess object is null!");
        }
        WeeklyShortUrlAccessBean bean = null;
        if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessBean) {
            bean = (WeeklyShortUrlAccessBean) weeklyShortUrlAccess;
        } else if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccess) {
            // bean = new WeeklyShortUrlAccessBean(null, weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new WeeklyShortUrlAccessBean(weeklyShortUrlAccess.getGuid(), weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
        } else {
            log.log(Level.WARNING, "createWeeklyShortUrlAccess(): Arg weeklyShortUrlAccess is of an unknown type.");
            //bean = new WeeklyShortUrlAccessBean();
            bean = new WeeklyShortUrlAccessBean(weeklyShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().createWeeklyShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public WeeklyShortUrlAccess constructWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param weeklyShortUrlAccess cannot be null.....
        if(weeklyShortUrlAccess == null) {
            log.log(Level.INFO, "Param weeklyShortUrlAccess is null!");
            throw new BadRequestException("Param weeklyShortUrlAccess object is null!");
        }
        WeeklyShortUrlAccessBean bean = null;
        if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessBean) {
            bean = (WeeklyShortUrlAccessBean) weeklyShortUrlAccess;
        } else if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccess) {
            // bean = new WeeklyShortUrlAccessBean(null, weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new WeeklyShortUrlAccessBean(weeklyShortUrlAccess.getGuid(), weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
        } else {
            log.log(Level.WARNING, "createWeeklyShortUrlAccess(): Arg weeklyShortUrlAccess is of an unknown type.");
            //bean = new WeeklyShortUrlAccessBean();
            bean = new WeeklyShortUrlAccessBean(weeklyShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().createWeeklyShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateWeeklyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        WeeklyShortUrlAccessBean bean = new WeeklyShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
        return updateWeeklyShortUrlAccess(bean);
    }
        
    @Override
    public Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //WeeklyShortUrlAccess bean = refreshWeeklyShortUrlAccess(weeklyShortUrlAccess);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param weeklyShortUrlAccess cannot be null.....
        if(weeklyShortUrlAccess == null || weeklyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param weeklyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param weeklyShortUrlAccess object or its guid is null!");
        }
        WeeklyShortUrlAccessBean bean = null;
        if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessBean) {
            bean = (WeeklyShortUrlAccessBean) weeklyShortUrlAccess;
        } else {  // if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccess)
            bean = new WeeklyShortUrlAccessBean(weeklyShortUrlAccess.getGuid(), weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
        }
        Boolean suc = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().updateWeeklyShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public WeeklyShortUrlAccess refreshWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param weeklyShortUrlAccess cannot be null.....
        if(weeklyShortUrlAccess == null || weeklyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param weeklyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param weeklyShortUrlAccess object or its guid is null!");
        }
        WeeklyShortUrlAccessBean bean = null;
        if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessBean) {
            bean = (WeeklyShortUrlAccessBean) weeklyShortUrlAccess;
        } else {  // if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccess)
            bean = new WeeklyShortUrlAccessBean(weeklyShortUrlAccess.getGuid(), weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
        }
        Boolean suc = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().updateWeeklyShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().deleteWeeklyShortUrlAccess(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            WeeklyShortUrlAccess weeklyShortUrlAccess = null;
            try {
                weeklyShortUrlAccess = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().getWeeklyShortUrlAccess(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch weeklyShortUrlAccess with a key, " + guid);
                return false;
            }
            if(weeklyShortUrlAccess != null) {
                String beanGuid = weeklyShortUrlAccess.getGuid();
                Boolean suc1 = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().deleteWeeklyShortUrlAccess(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("weeklyShortUrlAccess with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param weeklyShortUrlAccess cannot be null.....
        if(weeklyShortUrlAccess == null || weeklyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param weeklyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param weeklyShortUrlAccess object or its guid is null!");
        }
        WeeklyShortUrlAccessBean bean = null;
        if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccessBean) {
            bean = (WeeklyShortUrlAccessBean) weeklyShortUrlAccess;
        } else {  // if(weeklyShortUrlAccess instanceof WeeklyShortUrlAccess)
            // ????
            log.warning("weeklyShortUrlAccess is not an instance of WeeklyShortUrlAccessBean.");
            bean = new WeeklyShortUrlAccessBean(weeklyShortUrlAccess.getGuid(), weeklyShortUrlAccess.getTallyTime(), weeklyShortUrlAccess.getTallyEpoch(), weeklyShortUrlAccess.getCount(), weeklyShortUrlAccess.getShortUrl(), weeklyShortUrlAccess.getShortUrlDomain(), weeklyShortUrlAccess.getLongUrl(), weeklyShortUrlAccess.getLongUrlDomain(), weeklyShortUrlAccess.getRedirectType(), weeklyShortUrlAccess.getRefererDomain(), weeklyShortUrlAccess.getUserAgent(), weeklyShortUrlAccess.getLanguage(), weeklyShortUrlAccess.getCountry(), weeklyShortUrlAccess.getTalliedTime(), weeklyShortUrlAccess.getYear(), weeklyShortUrlAccess.getWeek());
        }
        Boolean suc = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().deleteWeeklyShortUrlAccess(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getWeeklyShortUrlAccessServiceProxy().deleteWeeklyShortUrlAccesses(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createWeeklyShortUrlAccesses(List<WeeklyShortUrlAccess> weeklyShortUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");

        if(weeklyShortUrlAccesses == null) {
            log.log(Level.WARNING, "createWeeklyShortUrlAccesses() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = weeklyShortUrlAccesses.size();
        if(size == 0) {
            log.log(Level.WARNING, "createWeeklyShortUrlAccesses() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(WeeklyShortUrlAccess weeklyShortUrlAccess : weeklyShortUrlAccesses) {
            String guid = createWeeklyShortUrlAccess(weeklyShortUrlAccess);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createWeeklyShortUrlAccesses() failed for at least one weeklyShortUrlAccess. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateWeeklyShortUrlAccesses(List<WeeklyShortUrlAccess> weeklyShortUrlAccesses) throws BaseException
    //{
    //}

}
