package com.urltally.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.af.service.impl.CurrentShortUrlAccessServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class CurrentShortUrlAccessProtoService extends CurrentShortUrlAccessServiceImpl implements CurrentShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public CurrentShortUrlAccessProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // CurrentShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public CurrentShortUrlAccess getCurrentShortUrlAccess(String guid) throws BaseException
    {
        return super.getCurrentShortUrlAccess(guid);
    }

    @Override
    public Object getCurrentShortUrlAccess(String guid, String field) throws BaseException
    {
        return super.getCurrentShortUrlAccess(guid, field);
    }

    @Override
    public List<CurrentShortUrlAccess> getCurrentShortUrlAccesses(List<String> guids) throws BaseException
    {
        return super.getCurrentShortUrlAccesses(guids);
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses() throws BaseException
    {
        return super.getAllCurrentShortUrlAccesses();
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllCurrentShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllCurrentShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        return super.createCurrentShortUrlAccess(currentShortUrlAccess);
    }

    @Override
    public CurrentShortUrlAccess constructCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        return super.constructCurrentShortUrlAccess(currentShortUrlAccess);
    }


    @Override
    public Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        return super.updateCurrentShortUrlAccess(currentShortUrlAccess);
    }
        
    @Override
    public CurrentShortUrlAccess refreshCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        return super.refreshCurrentShortUrlAccess(currentShortUrlAccess);
    }

    @Override
    public Boolean deleteCurrentShortUrlAccess(String guid) throws BaseException
    {
        return super.deleteCurrentShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        return super.deleteCurrentShortUrlAccess(currentShortUrlAccess);
    }

    @Override
    public Integer createCurrentShortUrlAccesses(List<CurrentShortUrlAccess> currentShortUrlAccesses) throws BaseException
    {
        return super.createCurrentShortUrlAccesses(currentShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateCurrentShortUrlAccesses(List<CurrentShortUrlAccess> currentShortUrlAccesses) throws BaseException
    //{
    //}

}
