package com.urltally.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.stub.ShortUrlAccessStub;
import com.urltally.ws.stub.CurrentShortUrlAccessStub;


// Wrapper class + bean combo.
public class CurrentShortUrlAccessBean extends ShortUrlAccessBean implements CurrentShortUrlAccess, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessBean.class.getName());


    // [2] Or, without an embedded object.
    private String startDayHour;
    private Long startTime;

    // Ctors.
    public CurrentShortUrlAccessBean()
    {
        //this((String) null);
    }
    public CurrentShortUrlAccessBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public CurrentShortUrlAccessBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime)
    {
        this(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime, null, null);
    }
    public CurrentShortUrlAccessBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime, Long createdTime, Long modifiedTime)
    {
        super(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, createdTime, modifiedTime);

        this.startDayHour = startDayHour;
        this.startTime = startTime;
    }
    public CurrentShortUrlAccessBean(CurrentShortUrlAccess stub)
    {
        if(stub instanceof CurrentShortUrlAccessStub) {
            super.setStub((ShortUrlAccessStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setTallyTime(stub.getTallyTime());   
            setTallyEpoch(stub.getTallyEpoch());   
            setCount(stub.getCount());   
            setShortUrl(stub.getShortUrl());   
            setShortUrlDomain(stub.getShortUrlDomain());   
            setLongUrl(stub.getLongUrl());   
            setLongUrlDomain(stub.getLongUrlDomain());   
            setRedirectType(stub.getRedirectType());   
            setRefererDomain(stub.getRefererDomain());   
            setUserAgent(stub.getUserAgent());   
            setLanguage(stub.getLanguage());   
            setCountry(stub.getCountry());   
            setTalliedTime(stub.getTalliedTime());   
            setStartDayHour(stub.getStartDayHour());   
            setStartTime(stub.getStartTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getTallyTime()
    {
        return super.getTallyTime();
    }
    public void setTallyTime(String tallyTime)
    {
        super.setTallyTime(tallyTime);
    }

    public Long getTallyEpoch()
    {
        return super.getTallyEpoch();
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        super.setTallyEpoch(tallyEpoch);
    }

    public Integer getCount()
    {
        return super.getCount();
    }
    public void setCount(Integer count)
    {
        super.setCount(count);
    }

    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    public String getShortUrlDomain()
    {
        return super.getShortUrlDomain();
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        super.setShortUrlDomain(shortUrlDomain);
    }

    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    public String getLongUrlDomain()
    {
        return super.getLongUrlDomain();
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        super.setLongUrlDomain(longUrlDomain);
    }

    public String getRedirectType()
    {
        return super.getRedirectType();
    }
    public void setRedirectType(String redirectType)
    {
        super.setRedirectType(redirectType);
    }

    public String getRefererDomain()
    {
        return super.getRefererDomain();
    }
    public void setRefererDomain(String refererDomain)
    {
        super.setRefererDomain(refererDomain);
    }

    public String getUserAgent()
    {
        return super.getUserAgent();
    }
    public void setUserAgent(String userAgent)
    {
        super.setUserAgent(userAgent);
    }

    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    public String getCountry()
    {
        return super.getCountry();
    }
    public void setCountry(String country)
    {
        super.setCountry(country);
    }

    public Long getTalliedTime()
    {
        return super.getTalliedTime();
    }
    public void setTalliedTime(Long talliedTime)
    {
        super.setTalliedTime(talliedTime);
    }

    public String getStartDayHour()
    {
        if(getStub() != null) {
            return getStub().getStartDayHour();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.startDayHour;
        }
    }
    public void setStartDayHour(String startDayHour)
    {
        if(getStub() != null) {
            getStub().setStartDayHour(startDayHour);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.startDayHour = startDayHour;
        }
    }

    public Long getStartTime()
    {
        if(getStub() != null) {
            return getStub().getStartTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.startTime;
        }
    }
    public void setStartTime(Long startTime)
    {
        if(getStub() != null) {
            getStub().setStartTime(startTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.startTime = startTime;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public CurrentShortUrlAccessStub getStub()
    {
        return (CurrentShortUrlAccessStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("startDayHour = " + this.startDayHour).append(";");
            sb.append("startTime = " + this.startTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = startDayHour == null ? 0 : startDayHour.hashCode();
            _hash = 31 * _hash + delta;
            delta = startTime == null ? 0 : startTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
