package com.urltally.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CommonConstants;
import com.urltally.ws.GeoCoordinateStruct;
// import com.urltally.ws.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;


public class GeoCoordinateStructProxyUtil
{
    private static final Logger log = Logger.getLogger(GeoCoordinateStructProxyUtil.class.getName());

    // Static methods only.
    private GeoCoordinateStructProxyUtil() {}

    public static GeoCoordinateStructBean convertServerGeoCoordinateStructBeanToAppBean(GeoCoordinateStruct serverBean)
    {
        GeoCoordinateStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new GeoCoordinateStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setLatitude(serverBean.getLatitude());
            bean.setLongitude(serverBean.getLongitude());
            bean.setAltitude(serverBean.getAltitude());
            bean.setSensorUsed(serverBean.isSensorUsed());
            bean.setAccuracy(serverBean.getAccuracy());
            bean.setAltitudeAccuracy(serverBean.getAltitudeAccuracy());
            bean.setHeading(serverBean.getHeading());
            bean.setSpeed(serverBean.getSpeed());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.urltally.ws.bean.GeoCoordinateStructBean convertAppGeoCoordinateStructBeanToServerBean(GeoCoordinateStruct appBean)
    {
        com.urltally.ws.bean.GeoCoordinateStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.GeoCoordinateStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setLatitude(appBean.getLatitude());
            bean.setLongitude(appBean.getLongitude());
            bean.setAltitude(appBean.getAltitude());
            bean.setSensorUsed(appBean.isSensorUsed());
            bean.setAccuracy(appBean.getAccuracy());
            bean.setAltitudeAccuracy(appBean.getAltitudeAccuracy());
            bean.setHeading(appBean.getHeading());
            bean.setSpeed(appBean.getSpeed());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
