package com.urltally.common;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class TallyType
{
    private static final Logger log = Logger.getLogger(TallyType.class.getName());

    private TallyType() {}

    // TBD
    // public static final String TALLY_YEARLY = "yearly";
    // public static final String TALLY_QUARTERLY = "quarterly";
    public static final String TALLY_MONTHLY = "monthly";
    public static final String TALLY_WEEKLY = "weekly";
    public static final String TALLY_DAILY = "daily";
    public static final String TALLY_HOURLY = "hourly";
    // public static final String TALLY_FIFTEEN = "fifteen";  // 15 minute interval???
    public static final String TALLY_CUMULATIVE = "cumulative";
    public static final String TALLY_CURRENT = "current";
    
    private static final Set<String> sTypes = new HashSet<String>();
    static {
        sTypes.add(TALLY_MONTHLY);
        sTypes.add(TALLY_WEEKLY);
        sTypes.add(TALLY_DAILY);
        sTypes.add(TALLY_HOURLY);
        sTypes.add(TALLY_CUMULATIVE);
        sTypes.add(TALLY_CURRENT);
    }
    
    public static boolean isValid(String type)
    {
        if(sTypes.contains(type)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static String getDefaultType()
    {
        // temporary
        //return TALLY_CURRENT;
        return TALLY_HOURLY;
    }

}
