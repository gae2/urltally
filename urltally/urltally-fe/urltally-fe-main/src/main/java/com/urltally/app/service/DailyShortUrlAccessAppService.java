package com.urltally.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.urltally.ws.BaseException;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.service.impl.DailyShortUrlAccessServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class DailyShortUrlAccessAppService extends DailyShortUrlAccessServiceImpl implements DailyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(DailyShortUrlAccessAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public DailyShortUrlAccessAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // DailyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public DailyShortUrlAccess getDailyShortUrlAccess(String guid) throws BaseException
    {
        return super.getDailyShortUrlAccess(guid);
    }

    @Override
    public Object getDailyShortUrlAccess(String guid, String field) throws BaseException
    {
        return super.getDailyShortUrlAccess(guid, field);
    }

    @Override
    public List<DailyShortUrlAccess> getDailyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return super.getDailyShortUrlAccesses(guids);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses() throws BaseException
    {
        return super.getAllDailyShortUrlAccesses();
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllDailyShortUrlAccessKeys(ordering, offset, count);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        return super.createDailyShortUrlAccess(dailyShortUrlAccess);
    }

    @Override
    public DailyShortUrlAccess constructDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        return super.constructDailyShortUrlAccess(dailyShortUrlAccess);
    }


    @Override
    public Boolean updateDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        return super.updateDailyShortUrlAccess(dailyShortUrlAccess);
    }
        
    @Override
    public DailyShortUrlAccess refreshDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        return super.refreshDailyShortUrlAccess(dailyShortUrlAccess);
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(String guid) throws BaseException
    {
        return super.deleteDailyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        return super.deleteDailyShortUrlAccess(dailyShortUrlAccess);
    }

    @Override
    public Integer createDailyShortUrlAccesses(List<DailyShortUrlAccess> dailyShortUrlAccesses) throws BaseException
    {
        return super.createDailyShortUrlAccesses(dailyShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateDailyShortUrlAccesses(List<DailyShortUrlAccess> dailyShortUrlAccesses) throws BaseException
    //{
    //}

}
