package com.urltally.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.app.service.CumulativeShortUrlAccessAppService;
import com.urltally.app.service.CurrentShortUrlAccessAppService;
import com.urltally.app.service.DailyShortUrlAccessAppService;
import com.urltally.app.service.HourlyShortUrlAccessAppService;
import com.urltally.app.service.MonthlyShortUrlAccessAppService;
import com.urltally.app.service.UserAppService;
import com.urltally.app.service.WeeklyShortUrlAccessAppService;
import com.urltally.app.util.TallyTimeUtil;
import com.urltally.common.TallyType;
import com.urltally.ws.BaseException;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.ShortUrlAccess;
import com.urltally.ws.WeeklyShortUrlAccess;


public class ShortUrlAccessHelper
{
    private static final Logger log = Logger.getLogger(ShortUrlAccessHelper.class.getName());

    private ShortUrlAccessHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private MonthlyShortUrlAccessAppService mMonthlyAccessAppService = null;
    private WeeklyShortUrlAccessAppService mWeeklyAccessAppService = null;
    private DailyShortUrlAccessAppService mDailyAccessAppService = null;
    private HourlyShortUrlAccessAppService mHourlyAccessAppService = null;
    private CumulativeShortUrlAccessAppService mCumulativeAccessAppService = null;
    private CurrentShortUrlAccessAppService mCurrentAccessAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private MonthlyShortUrlAccessAppService getMonthlyShortUrlAccessAppService()
    {
        if(mMonthlyAccessAppService == null) {
            mMonthlyAccessAppService = new MonthlyShortUrlAccessAppService();
        }
        return mMonthlyAccessAppService;
    }
    private WeeklyShortUrlAccessAppService getWeeklyShortUrlAccessAppService()
    {
        if(mWeeklyAccessAppService == null) {
            mWeeklyAccessAppService = new WeeklyShortUrlAccessAppService();
        }
        return mWeeklyAccessAppService;
    }
    private DailyShortUrlAccessAppService getDailyShortUrlAccessAppService()
    {
        if(mDailyAccessAppService == null) {
            mDailyAccessAppService = new DailyShortUrlAccessAppService();
        }
        return mDailyAccessAppService;
    }
    private HourlyShortUrlAccessAppService getHourlyShortUrlAccessAppService()
    {
        if(mHourlyAccessAppService == null) {
            mHourlyAccessAppService = new HourlyShortUrlAccessAppService();
        }
        return mHourlyAccessAppService;
    }
    private CumulativeShortUrlAccessAppService getCumulativeShortUrlAccessAppService()
    {
        if(mCumulativeAccessAppService == null) {
            mCumulativeAccessAppService = new CumulativeShortUrlAccessAppService();
        }
        return mCumulativeAccessAppService;
    }
    private CurrentShortUrlAccessAppService getCurrentShortUrlAccessAppService()
    {
        if(mCurrentAccessAppService == null) {
            mCurrentAccessAppService = new CurrentShortUrlAccessAppService();
        }
        return mCurrentAccessAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class ShortUrlAccessHelperHolder
    {
        private static final ShortUrlAccessHelper INSTANCE = new ShortUrlAccessHelper();
    }

    // Singleton method
    public static ShortUrlAccessHelper getInstance()
    {
        return ShortUrlAccessHelperHolder.INSTANCE;
    }

    
    @SuppressWarnings("unchecked")
    public int createShortUrlAccesses(String tallyType, List<ShortUrlAccess> beans)
    {
        int count = -1;
        if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            count = createMonthlyShortUrlAccesses((List<MonthlyShortUrlAccess>) ((List<?>) beans));
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            count = createWeeklyShortUrlAccesses((List<WeeklyShortUrlAccess>) ((List<?>) beans));
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            count = createDailyShortUrlAccesses((List<DailyShortUrlAccess>) ((List<?>) beans));
        } else if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            count = createHourlyShortUrlAccesses((List<HourlyShortUrlAccess>) ((List<?>) beans));
        } else if(TallyType.TALLY_CUMULATIVE.equals(tallyType)) {
            count = createCumulativeShortUrlAccesses((List<CumulativeShortUrlAccess>) ((List<?>) beans));
        } else if(TallyType.TALLY_CURRENT.equals(tallyType)) {
            count = createCurrentShortUrlAccesses((List<CurrentShortUrlAccess>) ((List<?>) beans));
        } else {
            // ????
        }
        return count;
    }

    public int createMonthlyShortUrlAccesses(List<MonthlyShortUrlAccess> beans)
    {
        int count = -1;
        try {
            Integer cnt = getMonthlyShortUrlAccessAppService().createMonthlyShortUrlAccesses(beans);
            if(cnt != null) {
                count = cnt;
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create a ShortUrlAccess.", e);
        }
        return count;
    }
    
    public int createWeeklyShortUrlAccesses(List<WeeklyShortUrlAccess> beans)
    {
        int count = -1;
        try {
            Integer cnt = getWeeklyShortUrlAccessAppService().createWeeklyShortUrlAccesses(beans);
            if(cnt != null) {
                count = cnt;
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create a ShortUrlAccess.", e);
        }
        return count;
    }
    
    public int createDailyShortUrlAccesses(List<DailyShortUrlAccess> beans)
    {
        int count = -1;
        try {
            Integer cnt = getDailyShortUrlAccessAppService().createDailyShortUrlAccesses(beans);
            if(cnt != null) {
                count = cnt;
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create a ShortUrlAccess.", e);
        }
        return count;
    }
    
    public int createHourlyShortUrlAccesses(List<HourlyShortUrlAccess> beans)
    {
        int count = -1;
        try {
            Integer cnt = getHourlyShortUrlAccessAppService().createHourlyShortUrlAccesses(beans);
            if(cnt != null) {
                count = cnt;
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create a ShortUrlAccess.", e);
        }
        return count;
    }
    
    public int createCumulativeShortUrlAccesses(List<CumulativeShortUrlAccess> beans)
    {
        int count = -1;
        try {
            Integer cnt = getCumulativeShortUrlAccessAppService().createCumulativeShortUrlAccesses(beans);
            if(cnt != null) {
                count = cnt;
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create a ShortUrlAccess.", e);
        }
        return count;
    }
    
    public int createCurrentShortUrlAccesses(List<CurrentShortUrlAccess> beans)
    {
        int count = -1;
        try {
            Integer cnt = getCurrentShortUrlAccessAppService().createCurrentShortUrlAccesses(beans);
            if(cnt != null) {
                count = cnt;
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create a ShortUrlAccess.", e);
        }
        return count;
    }

    
    @SuppressWarnings("unchecked")
    public ShortUrlAccess getShortUrlAccess(String tallyType, String guid)
    {
        ShortUrlAccess bean = null;
        if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            bean = getMonthlyShortUrlAccess(guid);
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            bean = getWeeklyShortUrlAccess(guid);
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            bean = getDailyShortUrlAccess(guid);
        } else if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            bean = getHourlyShortUrlAccess(guid);
        } else if(TallyType.TALLY_CUMULATIVE.equals(tallyType)) {
            bean = getCumulativeShortUrlAccess(guid);
        } else if(TallyType.TALLY_CURRENT.equals(tallyType)) {
            bean = getCurrentShortUrlAccess(guid);
        } else {
            // ????
        }
        return bean;
    }
    
    public MonthlyShortUrlAccess getMonthlyShortUrlAccess(String guid) 
    {
        MonthlyShortUrlAccess bean = null;
        try {
            bean = getMonthlyShortUrlAccessAppService().getMonthlyShortUrlAccess(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    public WeeklyShortUrlAccess getWeeklyShortUrlAccess(String guid) 
    {
        WeeklyShortUrlAccess bean = null;
        try {
            bean = getWeeklyShortUrlAccessAppService().getWeeklyShortUrlAccess(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    public DailyShortUrlAccess getDailyShortUrlAccess(String guid) 
    {
        DailyShortUrlAccess bean = null;
        try {
            bean = getDailyShortUrlAccessAppService().getDailyShortUrlAccess(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    public HourlyShortUrlAccess getHourlyShortUrlAccess(String guid) 
    {
        HourlyShortUrlAccess bean = null;
        try {
            bean = getHourlyShortUrlAccessAppService().getHourlyShortUrlAccess(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    public CumulativeShortUrlAccess getCumulativeShortUrlAccess(String guid) 
    {
        CumulativeShortUrlAccess bean = null;
        try {
            bean = getCumulativeShortUrlAccessAppService().getCumulativeShortUrlAccess(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    public CurrentShortUrlAccess getCurrentShortUrlAccess(String guid) 
    {
        CurrentShortUrlAccess bean = null;
        try {
            bean = getCurrentShortUrlAccessAppService().getCurrentShortUrlAccess(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    
    @SuppressWarnings("unchecked")
    public int getShortUrlAccessTally(String tallyType, String tallyTime, String shortUrl)
    {
        int count = -1;
        if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            count = getMonthlyShortUrlAccessTally(tallyTime, shortUrl);
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            count = getWeeklyShortUrlAccessTally(tallyTime, shortUrl);
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            count = getDailyShortUrlAccessTally(tallyTime, shortUrl);
        } else if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            count = getHourlyShortUrlAccessTally(tallyTime, shortUrl);
        } else if(TallyType.TALLY_CUMULATIVE.equals(tallyType)) {
            count = getCumulativeShortUrlAccessTally(tallyTime, shortUrl);
        } else if(TallyType.TALLY_CURRENT.equals(tallyType)) {
            count = getCurrentShortUrlAccessTally(tallyTime, shortUrl);
        } else {
            // ????
        }
        return count;
    }
    
    public int getMonthlyShortUrlAccessTally(String tallyTime, String shortUrl) 
    {
        int count = -1;

        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            return count;
        }
        
        try {
            String filter = "";
            if(tallyTime != null) {
                filter = "tallyTime == '" + tallyTime + "'";
            }
            if(shortUrl != null) {
                if(!filter.isEmpty()) {
                    filter += " && ";
                }
                filter += "shortUrl == '" + shortUrl + "'";
            }
            String ordering = null;
            int maxCount = 10000;  // ???
            List<MonthlyShortUrlAccess> beans = getMonthlyShortUrlAccessAppService().findMonthlyShortUrlAccesses(filter, ordering, null, null, null, null, 0L, maxCount);
            count = 0;
            if(beans != null) {
                int size = beans.size();
                if(size == maxCount) {
                    log.warning("Failed to retrieve all beans. maxCount = " + maxCount);
                }
                for(MonthlyShortUrlAccess b : beans) {
                    count += b.getCount();
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the beans for shortUrl = " + shortUrl, e);
        }
        
        return count;
    }

    
    public int getWeeklyShortUrlAccessTally(String tallyTime, String shortUrl) 
    {
        int count = -1;

        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            return count;
        }
        
        try {
            String filter = "";
            if(tallyTime != null) {
                filter = "tallyTime == '" + tallyTime + "'";
            }
            if(shortUrl != null) {
                if(!filter.isEmpty()) {
                    filter += " && ";
                }
                filter += "shortUrl == '" + shortUrl + "'";
            }
            String ordering = null;
            int maxCount = 10000;  // ???
            List<WeeklyShortUrlAccess> beans = getWeeklyShortUrlAccessAppService().findWeeklyShortUrlAccesses(filter, ordering, null, null, null, null, 0L, maxCount);
            count = 0;
            if(beans != null) {
                int size = beans.size();
                if(size == maxCount) {
                    log.warning("Failed to retrieve all beans. maxCount = " + maxCount);
                }
                for(WeeklyShortUrlAccess b : beans) {
                    count += b.getCount();
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the beans for shortUrl = " + shortUrl, e);
        }
        
        return count;
    }

    
    public int getDailyShortUrlAccessTally(String tallyTime, String shortUrl) 
    {
        int count = -1;

        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            return count;
        }
        
        try {
            String filter = "";
            if(tallyTime != null) {
                filter = "tallyTime == '" + tallyTime + "'";
            }
            if(shortUrl != null) {
                if(!filter.isEmpty()) {
                    filter += " && ";
                }
                filter += "shortUrl == '" + shortUrl + "'";
            }
            String ordering = null;
            int maxCount = 10000;  // ???
            List<DailyShortUrlAccess> beans = getDailyShortUrlAccessAppService().findDailyShortUrlAccesses(filter, ordering, null, null, null, null, 0L, maxCount);
            count = 0;
            if(beans != null) {
                int size = beans.size();
                if(size == maxCount) {
                    log.warning("Failed to retrieve all beans. maxCount = " + maxCount);
                }
                for(DailyShortUrlAccess b : beans) {
                    count += b.getCount();
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the beans for shortUrl = " + shortUrl, e);
        }
        
        return count;
    }

    public int getHourlyShortUrlAccessTally(String tallyTime, String shortUrl) 
    {
        int count = -1;

        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            return count;
        }
        
        try {
            String filter = "";
            if(tallyTime != null) {
                filter = "tallyTime == '" + tallyTime + "'";
            }
            if(shortUrl != null) {
                if(!filter.isEmpty()) {
                    filter += " && ";
                }
                filter += "shortUrl == '" + shortUrl + "'";
            }
            String ordering = null;
            int maxCount = 10000;  // ???
            List<HourlyShortUrlAccess> beans = getHourlyShortUrlAccessAppService().findHourlyShortUrlAccesses(filter, ordering, null, null, null, null, 0L, maxCount);
            count = 0;
            if(beans != null) {
                int size = beans.size();
                if(size == maxCount) {
                    log.warning("Failed to retrieve all beans. maxCount = " + maxCount);
                }
                for(HourlyShortUrlAccess b : beans) {
                    count += b.getCount();
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the beans for shortUrl = " + shortUrl, e);
        }
        
        return count;
    }

    public int getCumulativeShortUrlAccessTally(String tallyTime, String shortUrl) 
    {
        int count = -1;

        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            return count;
        }
        
        try {
            String filter = "";
            if(tallyTime != null) {
                filter = "tallyTime == '" + tallyTime + "'";
            }
            if(shortUrl != null) {
                if(!filter.isEmpty()) {
                    filter += " && ";
                }
                filter += "shortUrl == '" + shortUrl + "'";
            }
            String ordering = null;
            int maxCount = 10000;  // ???
            List<CumulativeShortUrlAccess> beans = getCumulativeShortUrlAccessAppService().findCumulativeShortUrlAccesses(filter, ordering, null, null, null, null, 0L, maxCount);
            count = 0;
            if(beans != null) {
                int size = beans.size();
                if(size == maxCount) {
                    log.warning("Failed to retrieve all beans. maxCount = " + maxCount);
                }
                for(CumulativeShortUrlAccess b : beans) {
                    count += b.getCount();
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the beans for shortUrl = " + shortUrl, e);
        }
        
        return count;
    }

    public int getCurrentShortUrlAccessTally(String tallyTime, String shortUrl) 
    {
        int count = -1;

        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            return count;
        }
        
        try {
            String filter = "";
            if(tallyTime != null) {
                filter = "tallyTime == '" + tallyTime + "'";
            }
            if(shortUrl != null) {
                if(!filter.isEmpty()) {
                    filter += " && ";
                }
                filter += "shortUrl == '" + shortUrl + "'";
            }
            String ordering = null;
            int maxCount = 10000;  // ???
            List<CurrentShortUrlAccess> beans = getCurrentShortUrlAccessAppService().findCurrentShortUrlAccesses(filter, ordering, null, null, null, null, 0L, maxCount);
            count = 0;
            if(beans != null) {
                int size = beans.size();
                if(size == maxCount) {
                    log.warning("Failed to retrieve all beans. maxCount = " + maxCount);
                }
                for(CurrentShortUrlAccess b : beans) {
                    count += b.getCount();
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the beans for shortUrl = " + shortUrl, e);
        }
        
        return count;
    }


    
    
}
