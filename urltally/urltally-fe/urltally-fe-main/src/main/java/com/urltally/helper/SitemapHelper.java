package com.urltally.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.fe.WebException;
import com.urltally.fe.bean.CurrentShortUrlAccessJsBean;
import com.urltally.wa.service.CurrentShortUrlAccessWebService;
import com.urltally.wa.service.UserWebService;


public class SitemapHelper
{
    private static final Logger log = Logger.getLogger(SitemapHelper.class.getName());
    
    // temporary
    private static final int MAX_MEMO_COUNT = 5000;   // Sitemap.xml max count...
    private static final int DEFAULT_MAX_MEMO_COUNT = 2500;
    // temporary

    private UserWebService userWebService = null;
    private CurrentShortUrlAccessWebService messageWebService = null;
    // ...

    private SitemapHelper() {}

    // Initialization-on-demand holder.
    private static final class SitemapHelperHolder
    {
        private static final SitemapHelper INSTANCE = new SitemapHelper();
    }

    // Singleton method
    public static SitemapHelper getInstance()
    {
        return SitemapHelperHolder.INSTANCE;
    }
    
    
    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private CurrentShortUrlAccessWebService getCurrentShortUrlAccessWebService()
    {
        if(messageWebService == null) {
            messageWebService = new CurrentShortUrlAccessWebService();
        }
        return messageWebService;
    }

    public List<CurrentShortUrlAccessJsBean> findRecentMessages()
    {
        return findRecentMessages(DEFAULT_MAX_MEMO_COUNT);
    }

    public List<CurrentShortUrlAccessJsBean> findRecentMessages(int maxCount)
    {
        List<CurrentShortUrlAccessJsBean> messages = null;
        try {
            String filter = null;
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = maxCount;   // TBD: Validation??? ( maxCount < MAX_MEMO_COUNT ???? )
            // TBD....
            messages = getCurrentShortUrlAccessWebService().findCurrentShortUrlAccesses(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.SEVERE, "Failed to find messages.", e);
            return null;
        }

        return messages;
    }

    
    // Format the timestamp to W3C date format: "yyyy-mm-dd".
    public static String formatDate(Long time)
    {
        if(time == null) {
            return null;  // ???
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date(time));
        return date;
    }
    
}
