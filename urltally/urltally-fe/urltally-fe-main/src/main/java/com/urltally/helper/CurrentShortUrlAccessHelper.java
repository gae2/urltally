package com.urltally.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.fe.WebException;
import com.urltally.fe.bean.CurrentShortUrlAccessJsBean;
import com.urltally.wa.service.CurrentShortUrlAccessWebService;
import com.urltally.wa.service.UserWebService;


public class CurrentShortUrlAccessHelper
{
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessHelper.class.getName());

    private UserWebService userWebService = null;
    private CurrentShortUrlAccessWebService mCurrentShortUrlAccessWebService = null;

    private CurrentShortUrlAccessHelper() {}

    // Initialization-on-demand holder.
    private static final class MessageHelperHolder
    {
        private static final CurrentShortUrlAccessHelper INSTANCE = new CurrentShortUrlAccessHelper();
    }

    // Singleton method
    public static CurrentShortUrlAccessHelper getInstance()
    {
        return MessageHelperHolder.INSTANCE;
    }


    private UserWebService getUserWebService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private CurrentShortUrlAccessWebService getCurrentShortUrlAccessWebService()
    {
        if(mCurrentShortUrlAccessWebService == null) {
            mCurrentShortUrlAccessWebService = new CurrentShortUrlAccessWebService();
        }
        return mCurrentShortUrlAccessWebService;
    }
   

    public List<CurrentShortUrlAccessJsBean> findCurrentShortUrlAccesssForUser(String userGuid)
    {
        return findCurrentShortUrlAccesssForUser(userGuid, null, null);
    }

    // TBD: sorting/ordering????
    public List<CurrentShortUrlAccessJsBean> findCurrentShortUrlAccesssForUser(String userGuid, Long offset, Integer count)
    {
        List<CurrentShortUrlAccessJsBean> beans = null;
        
        try {
            String filter = "user=='" + userGuid + "'";   // Status?
            String ordering = "createdTime desc";
            // TBD...
            beans = getCurrentShortUrlAccessWebService().findCurrentShortUrlAccesses(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to find beans for given user = " + userGuid, e);
        }

        return beans;
    }

    public List<CurrentShortUrlAccessJsBean> findCurrentShortUrlAccesssForPoster(String posterGuid)
    {
        return findCurrentShortUrlAccesssForPoster(posterGuid, null, null);
    }

    // TBD: sorting/ordering????
    public List<CurrentShortUrlAccessJsBean> findCurrentShortUrlAccesssForPoster(String posterGuid, Long offset, Integer count)
    {
        List<CurrentShortUrlAccessJsBean> beans = null;
        
        try {
            String filter = "poster=='" + posterGuid + "'";   // Status?
            String ordering = "createdTime desc";
            // TBD ...
            beans = getCurrentShortUrlAccessWebService().findCurrentShortUrlAccesses(filter, ordering, null, null, null, null, offset, count);    
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to find beans for given poster = " + posterGuid, e);
        }

        return beans;
    }


}
