package com.urltally.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.app.service.CurrentShortUrlAccessAppService;
import com.urltally.app.service.UserAppService;
import com.urltally.ws.BaseException;
import com.urltally.ws.CurrentShortUrlAccess;



// Place holder
// Currently not being used...
// To be deleted ???
public class CurrentShortUrlAccessHelper
{
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessHelper.class.getName());

    private CurrentShortUrlAccessHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private CurrentShortUrlAccessAppService messageAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private CurrentShortUrlAccessAppService getCurrentShortUrlAccessService()
    {
        if(messageAppService == null) {
            messageAppService = new CurrentShortUrlAccessAppService();
        }
        return messageAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class CurrentShortUrlAccessHelperHolder
    {
        private static final CurrentShortUrlAccessHelper INSTANCE = new CurrentShortUrlAccessHelper();
    }

    // Singleton method
    public static CurrentShortUrlAccessHelper getInstance()
    {
        return CurrentShortUrlAccessHelperHolder.INSTANCE;
    }

    
    public CurrentShortUrlAccess getCurrentShortUrlAccess(String guid) 
    {
        CurrentShortUrlAccess bean = null;
        try {
            bean = getCurrentShortUrlAccessService().getCurrentShortUrlAccess(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    

    
    // TBD:
    public List<String> getMessageKeysForProcessing(int maxCount)
    {
        List<String> keys = null;
        
        long now = System.currentTimeMillis();
        //String filter = "sendStatus < " + SendStatus.STATUS_POSTED + " && ( scheduledTime == null || scheduledTime <= " + now + " )";  // Error: 'or' filters can only check equality  ?????
        //String filter = "sendStatus == " + SendStatus.STATUS_SCHEDULED + " && scheduledTime <= " + now;  // (now + delta?)
        String filter = null;
        String ordering = "createdTime desc";
        try {
            keys = getCurrentShortUrlAccessService().findCurrentShortUrlAccessKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve status message key list.", e);
        }

        return keys;
    }

}
