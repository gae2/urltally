package com.urltally.common;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;


public class TallyStatus
{
    private static final Logger log = Logger.getLogger(TallyStatus.class.getName());

    private TallyStatus() {}

    // temporary
    public static final String STATUS_INITIALIZED = "initialized";
    public static final String STATUS_PREPARED = "prepared";
    public static final String STATUS_STARTED = "started";
    public static final String STATUS_PROCESSING = "processing";
    public static final String STATUS_COMPLETED = "completed";
    // ...

    private static final Set<String> sStatuses = new HashSet<String>();
    static {
        sStatuses.add(STATUS_INITIALIZED);
        sStatuses.add(STATUS_PREPARED);
        sStatuses.add(STATUS_STARTED);
        sStatuses.add(STATUS_PROCESSING);
        sStatuses.add(STATUS_COMPLETED);
    }
    
    public static boolean isValid(String status)
    {
        if(sStatuses.contains(status)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isCompleted(String status)
    {
        if(STATUS_COMPLETED.equals(status)) {
            return true;
        } else {
            return false;
        }
    }

    // Compare the status based on the "progress".
    // Note: Invalid values, including null, come before valid values.
    public static int compare(String status1, String status2)
    {
    	if(!isValid(status1)) {
    		if(!isValid(status2)) {
    			return 0;
    		} else {
    			return -1;
    		}
    	} else {
    		if(!isValid(status2)) {
    			return 1;
    		} else {
    		    if(STATUS_INITIALIZED.equals(status1)) {
        		    if(STATUS_INITIALIZED.equals(status2)) {
        		    	return 0;
        		    } else {
        		    	return -1;
        		    }    		    	
    		    } else if(STATUS_PREPARED.equals(status1)) {
        		    if(STATUS_INITIALIZED.equals(status2)) {
        		    	return 1;
        		    } else if(STATUS_PREPARED.equals(status2)) {
        		    	return 0;
        		    } else {
        		    	return -1;
        		    }
    		    } else if(STATUS_STARTED.equals(status1)) {
        		    if(STATUS_INITIALIZED.equals(status2) || STATUS_PREPARED.equals(status2)) {
        		    	return 1;
        		    } else if(STATUS_STARTED.equals(status2)) {
        		    	return 0;
        		    } else {
        		    	return -1;
        		    }
    		    } else if(STATUS_PROCESSING.equals(status1)) {
        		    if(STATUS_INITIALIZED.equals(status2) || STATUS_PREPARED.equals(status2) || STATUS_STARTED.equals(status2)) {
        		    	return 1;
        		    } else if(STATUS_PROCESSING.equals(status2)) {
        		    	return 0;
        		    } else {
        		    	return -1;
        		    }
    		    } else if(STATUS_COMPLETED.equals(status1)) {
        		    if(STATUS_COMPLETED.equals(status2)) {
        		    	return 0;
        		    } else {
        		    	return 1;
        		    }
    		    } else {
    		    	// This should not happen
    		    	return 0;
    		    }    		
    		}
    	}
    }

}
