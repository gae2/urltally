package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalLongUrlAccess;
// import com.urltally.ws.bean.TotalLongUrlAccessBean;
import com.urltally.ws.service.TotalLongUrlAccessService;
import com.urltally.af.bean.TotalLongUrlAccessBean;
import com.urltally.af.proxy.TotalLongUrlAccessServiceProxy;


public class LocalTotalLongUrlAccessServiceProxy extends BaseLocalServiceProxy implements TotalLongUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalTotalLongUrlAccessServiceProxy.class.getName());

    public LocalTotalLongUrlAccessServiceProxy()
    {
    }

    @Override
    public TotalLongUrlAccess getTotalLongUrlAccess(String guid) throws BaseException
    {
        TotalLongUrlAccess serverBean = getTotalLongUrlAccessService().getTotalLongUrlAccess(guid);
        TotalLongUrlAccess appBean = convertServerTotalLongUrlAccessBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getTotalLongUrlAccess(String guid, String field) throws BaseException
    {
        return getTotalLongUrlAccessService().getTotalLongUrlAccess(guid, field);       
    }

    @Override
    public List<TotalLongUrlAccess> getTotalLongUrlAccesses(List<String> guids) throws BaseException
    {
        List<TotalLongUrlAccess> serverBeanList = getTotalLongUrlAccessService().getTotalLongUrlAccesses(guids);
        List<TotalLongUrlAccess> appBeanList = convertServerTotalLongUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses() throws BaseException
    {
        return getAllTotalLongUrlAccesses(null, null, null);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTotalLongUrlAccessService().getAllTotalLongUrlAccesses(ordering, offset, count);
        return getAllTotalLongUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TotalLongUrlAccess> serverBeanList = getTotalLongUrlAccessService().getAllTotalLongUrlAccesses(ordering, offset, count, forwardCursor);
        List<TotalLongUrlAccess> appBeanList = convertServerTotalLongUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getTotalLongUrlAccessService().getAllTotalLongUrlAccessKeys(ordering, offset, count);
        return getAllTotalLongUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTotalLongUrlAccessService().getAllTotalLongUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTotalLongUrlAccessService().findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<TotalLongUrlAccess> serverBeanList = getTotalLongUrlAccessService().findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<TotalLongUrlAccess> appBeanList = convertServerTotalLongUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getTotalLongUrlAccessService().findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getTotalLongUrlAccessService().findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getTotalLongUrlAccessService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTotalLongUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {
        return getTotalLongUrlAccessService().createTotalLongUrlAccess(tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
    }

    @Override
    public String createTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.TotalLongUrlAccessBean serverBean =  convertAppTotalLongUrlAccessBeanToServerBean(totalLongUrlAccess);
        return getTotalLongUrlAccessService().createTotalLongUrlAccess(serverBean);
    }

    @Override
    public Boolean updateTotalLongUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {
        return getTotalLongUrlAccessService().updateTotalLongUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
    }

    @Override
    public Boolean updateTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.TotalLongUrlAccessBean serverBean =  convertAppTotalLongUrlAccessBeanToServerBean(totalLongUrlAccess);
        return getTotalLongUrlAccessService().updateTotalLongUrlAccess(serverBean);
    }

    @Override
    public Boolean deleteTotalLongUrlAccess(String guid) throws BaseException
    {
        return getTotalLongUrlAccessService().deleteTotalLongUrlAccess(guid);
    }

    @Override
    public Boolean deleteTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.TotalLongUrlAccessBean serverBean =  convertAppTotalLongUrlAccessBeanToServerBean(totalLongUrlAccess);
        return getTotalLongUrlAccessService().deleteTotalLongUrlAccess(serverBean);
    }

    @Override
    public Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return getTotalLongUrlAccessService().deleteTotalLongUrlAccesses(filter, params, values);
    }




    public static TotalLongUrlAccessBean convertServerTotalLongUrlAccessBeanToAppBean(TotalLongUrlAccess serverBean)
    {
        TotalLongUrlAccessBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new TotalLongUrlAccessBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTallyType(serverBean.getTallyType());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setCount(serverBean.getCount());
            bean.setLongUrl(serverBean.getLongUrl());
            bean.setLongUrlDomain(serverBean.getLongUrlDomain());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TotalLongUrlAccess> convertServerTotalLongUrlAccessBeanListToAppBeanList(List<TotalLongUrlAccess> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<TotalLongUrlAccess> beanList = new ArrayList<TotalLongUrlAccess>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(TotalLongUrlAccess sb : serverBeanList) {
                TotalLongUrlAccessBean bean = convertServerTotalLongUrlAccessBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.TotalLongUrlAccessBean convertAppTotalLongUrlAccessBeanToServerBean(TotalLongUrlAccess appBean)
    {
        com.urltally.ws.bean.TotalLongUrlAccessBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.TotalLongUrlAccessBean();
            bean.setGuid(appBean.getGuid());
            bean.setTallyType(appBean.getTallyType());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setCount(appBean.getCount());
            bean.setLongUrl(appBean.getLongUrl());
            bean.setLongUrlDomain(appBean.getLongUrlDomain());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<TotalLongUrlAccess> convertAppTotalLongUrlAccessBeanListToServerBeanList(List<TotalLongUrlAccess> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<TotalLongUrlAccess> beanList = new ArrayList<TotalLongUrlAccess>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(TotalLongUrlAccess sb : appBeanList) {
                com.urltally.ws.bean.TotalLongUrlAccessBean bean = convertAppTotalLongUrlAccessBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
