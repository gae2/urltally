package com.urltally.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.stub.TotalShortUrlAccessStub;


// Wrapper class + bean combo.
public class TotalShortUrlAccessBean implements TotalShortUrlAccess, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessBean.class.getName());

    // [1] With an embedded object.
    private TotalShortUrlAccessStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String tallyType;
    private String tallyTime;
    private Long tallyEpoch;
    private Integer count;
    private String shortUrl;
    private String shortUrlDomain;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public TotalShortUrlAccessBean()
    {
        //this((String) null);
    }
    public TotalShortUrlAccessBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public TotalShortUrlAccessBean(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain)
    {
        this(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, null, null);
    }
    public TotalShortUrlAccessBean(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.tallyType = tallyType;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.count = count;
        this.shortUrl = shortUrl;
        this.shortUrlDomain = shortUrlDomain;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public TotalShortUrlAccessBean(TotalShortUrlAccess stub)
    {
        if(stub instanceof TotalShortUrlAccessStub) {
            this.stub = (TotalShortUrlAccessStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setTallyType(stub.getTallyType());   
            setTallyTime(stub.getTallyTime());   
            setTallyEpoch(stub.getTallyEpoch());   
            setCount(stub.getCount());   
            setShortUrl(stub.getShortUrl());   
            setShortUrlDomain(stub.getShortUrlDomain());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getTallyType()
    {
        if(getStub() != null) {
            return getStub().getTallyType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyType;
        }
    }
    public void setTallyType(String tallyType)
    {
        if(getStub() != null) {
            getStub().setTallyType(tallyType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyType = tallyType;
        }
    }

    public String getTallyTime()
    {
        if(getStub() != null) {
            return getStub().getTallyTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyTime;
        }
    }
    public void setTallyTime(String tallyTime)
    {
        if(getStub() != null) {
            getStub().setTallyTime(tallyTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyTime = tallyTime;
        }
    }

    public Long getTallyEpoch()
    {
        if(getStub() != null) {
            return getStub().getTallyEpoch();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyEpoch;
        }
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        if(getStub() != null) {
            getStub().setTallyEpoch(tallyEpoch);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyEpoch = tallyEpoch;
        }
    }

    public Integer getCount()
    {
        if(getStub() != null) {
            return getStub().getCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.count;
        }
    }
    public void setCount(Integer count)
    {
        if(getStub() != null) {
            getStub().setCount(count);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.count = count;
        }
    }

    public String getShortUrl()
    {
        if(getStub() != null) {
            return getStub().getShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrl;
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getStub() != null) {
            getStub().setShortUrl(shortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrl = shortUrl;
        }
    }

    public String getShortUrlDomain()
    {
        if(getStub() != null) {
            return getStub().getShortUrlDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrlDomain;
        }
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        if(getStub() != null) {
            getStub().setShortUrlDomain(shortUrlDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrlDomain = shortUrlDomain;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public TotalShortUrlAccessStub getStub()
    {
        return this.stub;
    }
    protected void setStub(TotalShortUrlAccessStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("tallyType = " + this.tallyType).append(";");
            sb.append("tallyTime = " + this.tallyTime).append(";");
            sb.append("tallyEpoch = " + this.tallyEpoch).append(";");
            sb.append("count = " + this.count).append(";");
            sb.append("shortUrl = " + this.shortUrl).append(";");
            sb.append("shortUrlDomain = " + this.shortUrlDomain).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyType == null ? 0 : tallyType.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyTime == null ? 0 : tallyTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyEpoch == null ? 0 : tallyEpoch.hashCode();
            _hash = 31 * _hash + delta;
            delta = count == null ? 0 : count.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrl == null ? 0 : shortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrlDomain == null ? 0 : shortUrlDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
