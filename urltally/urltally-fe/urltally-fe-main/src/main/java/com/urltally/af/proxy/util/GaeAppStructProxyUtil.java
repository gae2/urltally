package com.urltally.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CommonConstants;
import com.urltally.ws.GaeAppStruct;
// import com.urltally.ws.bean.GaeAppStructBean;
import com.urltally.af.bean.GaeAppStructBean;


public class GaeAppStructProxyUtil
{
    private static final Logger log = Logger.getLogger(GaeAppStructProxyUtil.class.getName());

    // Static methods only.
    private GaeAppStructProxyUtil() {}

    public static GaeAppStructBean convertServerGaeAppStructBeanToAppBean(GaeAppStruct serverBean)
    {
        GaeAppStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new GaeAppStructBean();
            bean.setGroupId(serverBean.getGroupId());
            bean.setAppId(serverBean.getAppId());
            bean.setAppDomain(serverBean.getAppDomain());
            bean.setNamespace(serverBean.getNamespace());
            bean.setAcl(serverBean.getAcl());
            bean.setNote(serverBean.getNote());
        }
        return bean;
    }

    public static com.urltally.ws.bean.GaeAppStructBean convertAppGaeAppStructBeanToServerBean(GaeAppStruct appBean)
    {
        com.urltally.ws.bean.GaeAppStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.GaeAppStructBean();
            bean.setGroupId(appBean.getGroupId());
            bean.setAppId(appBean.getAppId());
            bean.setAppDomain(appBean.getAppDomain());
            bean.setNamespace(appBean.getNamespace());
            bean.setAcl(appBean.getAcl());
            bean.setNote(appBean.getNote());
        }
        return bean;
    }

}
