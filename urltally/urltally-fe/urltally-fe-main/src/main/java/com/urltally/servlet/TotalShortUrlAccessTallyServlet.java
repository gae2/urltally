package com.urltally.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.SortedMap;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.urltally.app.helper.ShortUrlAccessHelper;
import com.urltally.app.helper.TotalShortUrlTallyHelper;
import com.urltally.common.TallyType;
import com.urltally.ws.core.StatusCode;


// Mainly, for ajax calls....
public class TotalShortUrlAccessTallyServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessTallyServlet.class.getName());
    
    // temporary
    private static final String QUERY_PARAM_STARTTIME = "startTime";
    private static final String QUERY_PARAM_ENDTIME = "endTime";
    private static final String QUERY_PARAM_SHORTURL = "shortUrl";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        //JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singletong instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("TotalDailyShortUrlAccessServlet::doGet() called.");
               
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);


        String arg = null;
        if(pathInfo != null && !pathInfo.isEmpty()) {
            if(pathInfo.startsWith("/")) {
                arg = pathInfo.substring(1);
            } else {
                arg = pathInfo;
            }
        }
        
        String tallyType = null;
        if(TallyType.isValid(arg)) {
            tallyType = arg;
        } else {
            tallyType = TallyType.getDefaultType();
        }
        
        
        // Temporary
        // spans a period from startTallyTime to endTallyTime...
        String startTime = req.getParameter(QUERY_PARAM_STARTTIME);
        String endTime = req.getParameter(QUERY_PARAM_ENDTIME);  // This can be null.
        String shortUrl = req.getParameter(QUERY_PARAM_SHORTURL);
        if(shortUrl == null || shortUrl.isEmpty() || startTime == null || startTime.isEmpty()) {
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        
        // TBD:
        // Sanitize startTime???
        // e.g., if tallyTime=="monthly" and the date is not the first day of the month,
        //       then, make it so ????
        // ...
        
        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        


        // Get the tally map....
        SortedMap<String, Integer> tallyMap = TotalShortUrlTallyHelper.getInstance().getTotalShortUrlAccessTallies(tallyType, startTime, endTime, shortUrl);
        
        if(tallyMap == null) {
            // This cannot happen.... ???
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
            return;
        }

        int size = tallyMap.size();
        log.info("tallyMap size = " + size + " for tallyType = " + tallyType + "; startTime = " + startTime + "; shortUrl = " + shortUrl);

        ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
        StringWriter writer = new StringWriter();
        mapper.writeValue(writer, tallyMap);
        String jsonStr = writer.toString();

        if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
            jsonStr = jsonpCallback + "(" + jsonStr + ")";
            //resp.setContentType("application/javascript");  // ???
            resp.setContentType("application/javascript;charset=UTF-8");
        } else {
            //resp.setContentType("application/json");  // ????
            resp.setContentType("application/json;charset=UTF-8");  // ???                
        }

        PrintWriter out = resp.getWriter();
        out.write(jsonStr);
        
        // Always return 200 ????
        // return 200 only if cnt > 0 ????
        resp.setStatus(StatusCode.OK);
    }

    
    // TBD:
    // depending on mail.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
}
