package com.urltally.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.urltally.ws.BaseException;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.service.impl.HourlyShortUrlAccessServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class HourlyShortUrlAccessAppService extends HourlyShortUrlAccessServiceImpl implements HourlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(HourlyShortUrlAccessAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public HourlyShortUrlAccessAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // HourlyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public HourlyShortUrlAccess getHourlyShortUrlAccess(String guid) throws BaseException
    {
        return super.getHourlyShortUrlAccess(guid);
    }

    @Override
    public Object getHourlyShortUrlAccess(String guid, String field) throws BaseException
    {
        return super.getHourlyShortUrlAccess(guid, field);
    }

    @Override
    public List<HourlyShortUrlAccess> getHourlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return super.getHourlyShortUrlAccesses(guids);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses() throws BaseException
    {
        return super.getAllHourlyShortUrlAccesses();
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllHourlyShortUrlAccessKeys(ordering, offset, count);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        return super.createHourlyShortUrlAccess(hourlyShortUrlAccess);
    }

    @Override
    public HourlyShortUrlAccess constructHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        return super.constructHourlyShortUrlAccess(hourlyShortUrlAccess);
    }


    @Override
    public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        return super.updateHourlyShortUrlAccess(hourlyShortUrlAccess);
    }
        
    @Override
    public HourlyShortUrlAccess refreshHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        return super.refreshHourlyShortUrlAccess(hourlyShortUrlAccess);
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException
    {
        return super.deleteHourlyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        return super.deleteHourlyShortUrlAccess(hourlyShortUrlAccess);
    }

    @Override
    public Integer createHourlyShortUrlAccesses(List<HourlyShortUrlAccess> hourlyShortUrlAccesses) throws BaseException
    {
        return super.createHourlyShortUrlAccesses(hourlyShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateHourlyShortUrlAccesses(List<HourlyShortUrlAccess> hourlyShortUrlAccesses) throws BaseException
    //{
    //}

}
