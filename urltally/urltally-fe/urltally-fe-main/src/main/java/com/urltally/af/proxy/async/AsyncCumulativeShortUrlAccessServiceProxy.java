package com.urltally.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.CumulativeShortUrlAccessStub;
import com.urltally.ws.stub.CumulativeShortUrlAccessListStub;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.bean.CumulativeShortUrlAccessBean;
import com.urltally.ws.service.CumulativeShortUrlAccessService;
import com.urltally.af.proxy.CumulativeShortUrlAccessServiceProxy;
import com.urltally.af.proxy.remote.RemoteCumulativeShortUrlAccessServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncCumulativeShortUrlAccessServiceProxy extends BaseAsyncServiceProxy implements CumulativeShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncCumulativeShortUrlAccessServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteCumulativeShortUrlAccessServiceProxy remoteProxy;

    public AsyncCumulativeShortUrlAccessServiceProxy()
    {
        remoteProxy = new RemoteCumulativeShortUrlAccessServiceProxy();
    }

    @Override
    public CumulativeShortUrlAccess getCumulativeShortUrlAccess(String guid) throws BaseException
    {
        return remoteProxy.getCumulativeShortUrlAccess(guid);
    }

    @Override
    public Object getCumulativeShortUrlAccess(String guid, String field) throws BaseException
    {
        return remoteProxy.getCumulativeShortUrlAccess(guid, field);       
    }

    @Override
    public List<CumulativeShortUrlAccess> getCumulativeShortUrlAccesses(List<String> guids) throws BaseException
    {
        return remoteProxy.getCumulativeShortUrlAccesses(guids);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses() throws BaseException
    {
        return getAllCumulativeShortUrlAccesses(null, null, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllCumulativeShortUrlAccesses(ordering, offset, count);
        return getAllCumulativeShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllCumulativeShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllCumulativeShortUrlAccessKeys(ordering, offset, count);
        return getAllCumulativeShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllCumulativeShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createCumulativeShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {
        CumulativeShortUrlAccessBean bean = new CumulativeShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
        return createCumulativeShortUrlAccess(bean);        
    }

    @Override
    public String createCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        String guid = cumulativeShortUrlAccess.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((CumulativeShortUrlAccessBean) cumulativeShortUrlAccess).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateCumulativeShortUrlAccess-" + guid;
        String taskName = "RsCreateCumulativeShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        CumulativeShortUrlAccessStub stub = MarshalHelper.convertCumulativeShortUrlAccessToStub(cumulativeShortUrlAccess);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(CumulativeShortUrlAccessStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = cumulativeShortUrlAccess.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    CumulativeShortUrlAccessStub dummyStub = new CumulativeShortUrlAccessStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createCumulativeShortUrlAccess(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "cumulativeShortUrlAccesses/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createCumulativeShortUrlAccess(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "cumulativeShortUrlAccesses/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateCumulativeShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "CumulativeShortUrlAccess guid is invalid.");
        	throw new BaseException("CumulativeShortUrlAccess guid is invalid.");
        }
        CumulativeShortUrlAccessBean bean = new CumulativeShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
        return updateCumulativeShortUrlAccess(bean);        
    }

    @Override
    public Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        String guid = cumulativeShortUrlAccess.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "CumulativeShortUrlAccess object is invalid.");
        	throw new BaseException("CumulativeShortUrlAccess object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateCumulativeShortUrlAccess-" + guid;
        String taskName = "RsUpdateCumulativeShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        CumulativeShortUrlAccessStub stub = MarshalHelper.convertCumulativeShortUrlAccessToStub(cumulativeShortUrlAccess);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(CumulativeShortUrlAccessStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = cumulativeShortUrlAccess.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    CumulativeShortUrlAccessStub dummyStub = new CumulativeShortUrlAccessStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateCumulativeShortUrlAccess(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "cumulativeShortUrlAccesses/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateCumulativeShortUrlAccess(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "cumulativeShortUrlAccesses/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteCumulativeShortUrlAccess-" + guid;
        String taskName = "RsDeleteCumulativeShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "cumulativeShortUrlAccesses/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        String guid = cumulativeShortUrlAccess.getGuid();
        return deleteCumulativeShortUrlAccess(guid);
    }

    @Override
    public Long deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteCumulativeShortUrlAccesses(filter, params, values);
    }

}
