package com.urltally.af.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.google.appengine.api.search.Field;
//import com.google.appengine.api.search.GeoPoint;
//import com.google.appengine.api.search.Document;
//import com.google.appengine.api.search.Document.Builder;

import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.util.CommonUtil;
import com.urltally.ws.GeoPointStruct;
import com.urltally.ws.StreetAddressStruct;
import com.urltally.ws.GaeAppStruct;
import com.urltally.ws.FullNameStruct;
import com.urltally.ws.GaeUserStruct;
import com.urltally.ws.User;
import com.urltally.ws.search.bean.UserQueryBean;
import com.urltally.ws.search.gae.UserIndexBuilder;
import com.urltally.af.config.Config;
import com.urltally.af.search.gae.UserQueryHelper;


public class UserSearchManager
{
    private static final Logger log = Logger.getLogger(UserSearchManager.class.getName());
        
    // TBD: Use AbstractFactory....
    private UserIndexBuilder userIndexBuilder;
    private UserQueryHelper userQueryHelper;
    
    private UserSearchManager()
    {
        userIndexBuilder = new UserIndexBuilder();
        userQueryHelper = new UserQueryHelper(userIndexBuilder);
    }

    
    // Initialization-on-demand holder.
    private static final class UserSearchManagerHolder
    {
        private static final UserSearchManager INSTANCE = new UserSearchManager();
    }

    // Singleton method
    public static UserSearchManager getInstance()
    {
        return UserSearchManagerHolder.INSTANCE;
    }


    public boolean addDocument(User user)
    {
        return userIndexBuilder.addDocument(user);
    }


    // TBD:
    public List<UserQueryBean> findUserQueryBeans(String queryString, String ordering, Long offset, Integer count)
    {
        return findUserQueryBeans(queryString, ordering, offset, count, null);
    }
    // Note: strCursor is an inout param. 
    public List<UserQueryBean> findUserQueryBeans(String queryString, String ordering, Long offset, Integer count, StringCursor strCursor)
    {
        if("local".equals(Config.getInstance().getSearchIndexMode())) {
            return userQueryHelper.findUserQueryBeans(queryString, ordering, offset, count, strCursor);
        } else {
        	throw new NotImplementedRsException("Unsupported searchIndexMode.");
        }
    }

}

