package com.urltally.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.AccessTallyMasterStub;
import com.urltally.ws.stub.AccessTallyMasterListStub;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.ws.service.AccessTallyMasterService;
import com.urltally.af.proxy.AccessTallyMasterServiceProxy;
import com.urltally.af.proxy.remote.RemoteAccessTallyMasterServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncAccessTallyMasterServiceProxy extends BaseAsyncServiceProxy implements AccessTallyMasterServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncAccessTallyMasterServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteAccessTallyMasterServiceProxy remoteProxy;

    public AsyncAccessTallyMasterServiceProxy()
    {
        remoteProxy = new RemoteAccessTallyMasterServiceProxy();
    }

    @Override
    public AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException
    {
        return remoteProxy.getAccessTallyMaster(guid);
    }

    @Override
    public Object getAccessTallyMaster(String guid, String field) throws BaseException
    {
        return remoteProxy.getAccessTallyMaster(guid, field);       
    }

    @Override
    public List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        return remoteProxy.getAccessTallyMasters(guids);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException
    {
        return getAllAccessTallyMasters(null, null, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllAccessTallyMasters(ordering, offset, count);
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllAccessTallyMasterKeys(ordering, offset, count);
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        AccessTallyMasterBean bean = new AccessTallyMasterBean(null, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        return createAccessTallyMaster(bean);        
    }

    @Override
    public String createAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        String guid = accessTallyMaster.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((AccessTallyMasterBean) accessTallyMaster).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateAccessTallyMaster-" + guid;
        String taskName = "RsCreateAccessTallyMaster-" + guid + "-" + (new Date()).getTime();
        AccessTallyMasterStub stub = MarshalHelper.convertAccessTallyMasterToStub(accessTallyMaster);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(AccessTallyMasterStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = accessTallyMaster.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    AccessTallyMasterStub dummyStub = new AccessTallyMasterStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createAccessTallyMaster(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "accessTallyMasters/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createAccessTallyMaster(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "accessTallyMasters/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "AccessTallyMaster guid is invalid.");
        	throw new BaseException("AccessTallyMaster guid is invalid.");
        }
        AccessTallyMasterBean bean = new AccessTallyMasterBean(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        return updateAccessTallyMaster(bean);        
    }

    @Override
    public Boolean updateAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        String guid = accessTallyMaster.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "AccessTallyMaster object is invalid.");
        	throw new BaseException("AccessTallyMaster object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateAccessTallyMaster-" + guid;
        String taskName = "RsUpdateAccessTallyMaster-" + guid + "-" + (new Date()).getTime();
        AccessTallyMasterStub stub = MarshalHelper.convertAccessTallyMasterToStub(accessTallyMaster);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(AccessTallyMasterStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = accessTallyMaster.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    AccessTallyMasterStub dummyStub = new AccessTallyMasterStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateAccessTallyMaster(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "accessTallyMasters/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateAccessTallyMaster(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "accessTallyMasters/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteAccessTallyMaster-" + guid;
        String taskName = "RsDeleteAccessTallyMaster-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "accessTallyMasters/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        String guid = accessTallyMaster.getGuid();
        return deleteAccessTallyMaster(guid);
    }

    @Override
    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteAccessTallyMasters(filter, params, values);
    }

}
