package com.urltally.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.stub.TotalShortUrlAccessStub;
import com.urltally.ws.stub.TotalShortUrlAccessListStub;
import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.proxy.TotalShortUrlAccessServiceProxy;
import com.urltally.af.proxy.remote.RemoteProxyFactory;
import com.urltally.af.proxy.remote.RemoteTotalShortUrlAccessServiceProxy;
import com.urltally.af.resource.TotalShortUrlAccessResource;


@Path("/_task/r/totalShortUrlAccesses/")
public class AsyncTotalShortUrlAccessResource extends BaseAsyncResource implements TotalShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(AsyncTotalShortUrlAccessResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncTotalShortUrlAccessResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getTotalShortUrlAccessList(List<TotalShortUrlAccess> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findTotalShortUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getTotalShortUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getTotalShortUrlAccess(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getTotalShortUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getTotalShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructTotalShortUrlAccess(TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructTotalShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            TotalShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                TotalShortUrlAccessStub realStub = (TotalShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertTotalShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertTotalShortUrlAccessStubToBean(totalShortUrlAccess);
            }
            //bean = (TotalShortUrlAccessBean) RemoteProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy().constructTotalShortUrlAccess(bean);
            //totalShortUrlAccess = TotalShortUrlAccessStub.convertBeanToStub(bean);
            //String guid = totalShortUrlAccess.getGuid();
            // TBD: createTotalShortUrlAccess() or constructTotalShortUrlAccess()???  (constructTotalShortUrlAccess() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy().createTotalShortUrlAccess(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructTotalShortUrlAccess(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(totalShortUrlAccess).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createTotalShortUrlAccess(TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createTotalShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            TotalShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                TotalShortUrlAccessStub realStub = (TotalShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertTotalShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertTotalShortUrlAccessStubToBean(totalShortUrlAccess);
            }
            String guid = RemoteProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy().createTotalShortUrlAccess(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createTotalShortUrlAccess(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createTotalShortUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshTotalShortUrlAccess(String guid, TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshTotalShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(totalShortUrlAccess == null || !guid.equals(totalShortUrlAccess.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from totalShortUrlAccess guid = " + totalShortUrlAccess.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the totalShortUrlAccess with guid = " + guid);
            }
            TotalShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                TotalShortUrlAccessStub realStub = (TotalShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertTotalShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertTotalShortUrlAccessStubToBean(totalShortUrlAccess);
            }
            //bean = (TotalShortUrlAccessBean) RemoteProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy().refreshTotalShortUrlAccess(bean);
            //if(bean == null) {
            //    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the totalShortUrlAccess with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the totalShortUrlAccess with guid = " + guid);
            //}
            //totalShortUrlAccess = TotalShortUrlAccessStub.convertBeanToStub(bean);
            // TBD: updateTotalShortUrlAccess() or refreshTotalShortUrlAccess()???  (refreshTotalShortUrlAccess() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy().updateTotalShortUrlAccess(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refrefsh the totalShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the totalShortUrlAccess with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshTotalShortUrlAccess(): Successfully processed the request: guid = " + guid);
            return Response.ok(totalShortUrlAccess).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateTotalShortUrlAccess(String guid, TotalShortUrlAccessStub totalShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateTotalShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(totalShortUrlAccess == null || !guid.equals(totalShortUrlAccess.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from totalShortUrlAccess guid = " + totalShortUrlAccess.getGuid());
                throw new RequestForbiddenRsException("Failed to update the totalShortUrlAccess with guid = " + guid);
            }
            TotalShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                TotalShortUrlAccessStub realStub = (TotalShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertTotalShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertTotalShortUrlAccessStubToBean(totalShortUrlAccess);
            }
            boolean suc = RemoteProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy().updateTotalShortUrlAccess(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the totalShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the totalShortUrlAccess with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateTotalShortUrlAccess(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
    public Response updateTotalShortUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteTotalShortUrlAccess(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteTotalShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy().deleteTotalShortUrlAccess(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the totalShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the totalShortUrlAccess with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteTotalShortUrlAccess(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteTotalShortUrlAccesses(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy().deleteTotalShortUrlAccesses(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createTotalShortUrlAccesses(TotalShortUrlAccessListStub totalShortUrlAccesses) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createTotalShortUrlAccesses(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            TotalShortUrlAccessListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                TotalShortUrlAccessListStub realStubs = (TotalShortUrlAccessListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = totalShortUrlAccesses;
            }

            List<TotalShortUrlAccessStub> stubList = totalShortUrlAccesses.getList();
            List<VisitorSetting> beans = new ArrayList<TotalShortUrlAccess>();
            for(TotalShortUrlAccessStub stub : stubList) {
                TotalShortUrlAccessBean bean = convertTotalShortUrlAccessStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy().createTotalShortUrlAccesses(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static TotalShortUrlAccessBean convertTotalShortUrlAccessStubToBean(TotalShortUrlAccess stub)
    {
        TotalShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new TotalShortUrlAccessBean();
            bean.setGuid(stub.getGuid());
            bean.setTallyType(stub.getTallyType());
            bean.setTallyTime(stub.getTallyTime());
            bean.setTallyEpoch(stub.getTallyEpoch());
            bean.setCount(stub.getCount());
            bean.setShortUrl(stub.getShortUrl());
            bean.setShortUrlDomain(stub.getShortUrlDomain());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
