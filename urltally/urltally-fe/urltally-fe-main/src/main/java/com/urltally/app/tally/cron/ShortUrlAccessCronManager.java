package com.urltally.app.tally.cron;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.urltally.app.helper.AccessTallyAccessHelper;
import com.urltally.app.service.CurrentShortUrlAccessAppService;
import com.urltally.app.service.UserAppService;
import com.urltally.app.tally.TallyManager;
import com.urltally.app.util.ConfigUtil;
import com.urltally.app.util.TallyTimeUtil;
import com.urltally.common.TallyStatus;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.AccessTallyStatus;


// TBD:
// Due to a bug/incorrectness in the algorithm (in the way crons are run and/or taskqueues are used for delayed saving, etc.),
// multiple tallyMasters can be stored (even with the same statuses) at the same time...
// and they can run concurrently (with overlapping run time)....
// ...
// To fix this (rather serious) problem, we have at least two choices
// (1) Use some kind of "lock" so that no processes can run for a given {tallyTime, tallyTime}, or
// (2) Make sure that all operations are "idempotent", that is, running any step more than once should not alter the resulting data (stats)
// ....
// Unfortunately, both are very difficult to accomplish....
public class ShortUrlAccessCronManager
{
    private static final Logger log = Logger.getLogger(ShortUrlAccessCronManager.class.getName());   

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private CurrentShortUrlAccessAppService messageAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private CurrentShortUrlAccessAppService getCurrentShortUrlAccessService()
    {
        if(messageAppService == null) {
            messageAppService = new CurrentShortUrlAccessAppService();
        }
        return messageAppService;
    }
    // etc. ...

    
    private ShortUrlAccessCronManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class SendStatusCronManagerHolder
    {
        private static final ShortUrlAccessCronManager INSTANCE = new ShortUrlAccessCronManager();
    }

    // Singleton method
    public static ShortUrlAccessCronManager getInstance()
    {
        return SendStatusCronManagerHolder.INSTANCE;
    }

    private void init()
    {
        // TBD: ...
    }


    // TBD....
    public long processShortUrlAccesses(String tallyType)
    {
        // TBD::
        
        // Note:
        // Cron process is limited to 15 mins (?)...
        // what happens if the time runs out during processing?????
        // ....
        // Alternatively,
        // What happens if a new cron is triggered while another is already running?
        // Is it safe???
        // (e.g., hourly cron will be run every few mins, e.g., 2~3 mins... and each process might run several mins or longer....)
        // ?????
        // ????
        
        
        AccessTallyMaster activeMaster = TallyManager.getInstance().getActiveAccessTallyMaster(tallyType);
        if(activeMaster == null) {
            // ????
            log.warning("Failed to get the active AccessTallyMaster for tallyType = " + tallyType);
            return 0L;   // ???
        }
        
        String tallyTime = activeMaster.getTallyTime();
        
        // TBD:
        // if tallyTime is too old, start a new one with the "following time" ????
        // (The concern is, for whatever reason, what happens if the tally processing is "stuck" at one time/date ????)
        // ....
        
        boolean needsProcessing = TallyManager.getInstance().isReadyForProcessing(activeMaster);
        if(needsProcessing == false) {
            log.info("Not ready for processing. tallyTime = " + tallyTime + "; tallyType = " + tallyType);
            return 0L;   // ???
        }
        
        String tallyStatus = activeMaster.getTallyStatus();

        
        
        // Note:
        // Notice the "else" in these if/else if blocks below....
        // Due to the asyc update...., some odd things might happen if we don't give "enough" time between these steps...
        // It's probably better to process these steps separately from different cron runs...
        // ....
        
        
        // [1] Initialized -> Prepared
        if(!TallyStatus.isValid(tallyStatus) || tallyStatus.equals(TallyStatus.STATUS_INITIALIZED)) {
            activeMaster = TallyManager.getInstance().markAccessTallyMasterAsPrepared(activeMaster);
            if(activeMaster == null) {
                // TBD:
                log.warning("Failed to mark activeMaster as Prepared. tallyTime = " + tallyTime + "; tallyType = " + tallyType);
                // Bail out???? What to do????
                return 0L;   // ???
            } else {
                tallyStatus = activeMaster.getTallyStatus();
            }
        }
        
        // [2] Prepared -> Started
        // (this step is probably idempotent...)
        else if(tallyStatus.equals(TallyStatus.STATUS_PREPARED)) {
            
            // Delete "old" AccessTallyStatus records...

            // ???
            String prevTallyTime = TallyTimeUtil.getPrevious(tallyType, tallyTime);
            String prevPrevTallyTime = TallyTimeUtil.getPrevious(tallyType, prevTallyTime);

            // TBD:
            // Move this loop into TallyManager???
            // ...

            // Note: if you use maxIter > 1, due to the async update, same record may be multiply processed....
            boolean allDeleted = false;
            int maxCount = 100;  // 100 at a time.  --> TBD: Read these from config???
            int maxIters = 10;   // 10 times, up to 1,000 records...
            
            // Hack: To avoid multiple processing of same records (in a single cron run)...
            Set<String> tallyStatusGuidSet = new HashSet<String>();
            // hack....
            
            int iterCnt = 0;
            while(allDeleted == false && iterCnt < maxIters) {
                
            	// Temporary
                List<AccessTallyStatus> beans = AccessTallyAccessHelper.getInstance().getAccessTallyStatusesForDeletion(tallyType, prevPrevTallyTime, maxCount);
                ////List<String> keys = AccessTallyAccessHelper.getInstance().getAccessTallyStatusKeysForDeletion(tallyType, prevPrevTallyTime, maxCount);
    
                // ????
                if(beans == null) {
                ////if(keys == null) {
                    // it's probably an error.
                    log.warning("Failed to retrieve AccessTallyStatus list for deletion.");
                    return 0L;  // ????
                    // ???
                    //allDeleted = true;
                    // ????
                } else if(beans.isEmpty()) {
                ////} else if(keys.isEmpty()) {
                    // ??? Probably, all done????
                    log.info("Empty AccessTallyStatus list retrieved for tallyType = " + tallyType + "; prevPrevTallyTime = " + prevPrevTallyTime);
                    allDeleted = true;
                } else {
                    for(AccessTallyStatus bean : beans) {
                    ////for(String key : keys) {
                    	String guid = bean.getGuid();
                        ////String guid = key;
                        if(tallyStatusGuidSet.contains(guid)) {
                            log.fine("This AccessTallyStatus has already been processed/deleted in this cron run. Skipping this: guid = " + guid);
                            continue;
                        }
                        tallyStatusGuidSet.add(guid);

                        ////boolean suc = AccessTallyAccessHelper.getInstance().deleteAccessTallyStatus(guid);
                        ////if(suc == false) {
                        ////    // ??? what to do ????
                        ////    log.warning("Failed to delete AccessTallyStatus bean. key = " + key);
                        ////}
                        boolean processed = bean.isProcessed();
                        if(processed) {
                            boolean suc = AccessTallyAccessHelper.getInstance().deleteAccessTallyStatus(guid);
                            if(suc == false) {
                                // ??? what to do ????
                                log.warning("Failed to delete AccessTallyStatus bean = " + bean.toString());
                            }
                        } else {
                            // error???
                            log.warning("Non-processed AccessTallyStatus found for tallyType = " + tallyType + "; prevPrevTallyTime = " + prevPrevTallyTime);
                            log.warning("AccessTallyStatus = " + bean.toString());
                            // Continue??? Bail out??? Delete it anyway??? ????
                            // Just skip this, and continue.... ???
                            // For now, delete only the "old" ones...
                            if(bean.getCreatedTime() < System.currentTimeMillis() - 2 * 24 * 3600L) {  // Arbitrary.  2 days...
                                boolean suc = AccessTallyAccessHelper.getInstance().deleteAccessTallyStatus(guid);
                                if(suc == false) {
                                    // ??? what to do ????
                                    log.warning("Failed to delete an old AccessTallyStatus bean = " + bean.toString());
                                }                            	
                            } else {
                            	// ignore for now...
                            }
                        }
                    }
                }
                
                ++iterCnt;
            }            
            
            if(allDeleted) {
                // Mark tallyStatus as Started...
                activeMaster = TallyManager.getInstance().markAccessTallyMasterAsStarted(activeMaster);
                if(activeMaster == null) {
                    // TBD:
                    log.warning("Failed to mark activeMaster as Started. tallyTime = " + tallyTime + "; tallyType = " + tallyType);
                    // Bail out???? What to do????
                    return 0L;   // ???
                } else {
                    tallyStatus = activeMaster.getTallyStatus();
                }
            }
        }
        
        // [3] Started -> Processing
        // It's probably idempotent, depending on buildAccessTallyStatusList() implementation...
        else if(tallyStatus.equals(TallyStatus.STATUS_STARTED)) {
            
            // TBD:
            // Need to modify the implementation here
            // to be able to support "partial" or chunk data fetch from IngressDB...
            // .....
            
            // Fetch accessRecords from IngressDB
            // And, create a list of AccessTallyStatus
            long offset = 0L;
            if(activeMaster != null && activeMaster.getAccessRecordCount() != null) {
                offset = activeMaster.getAccessRecordCount();
                if(offset > 0) {
                    log.info("Appears that access records have been previously fetched. This time, starting from offset = " + offset);
                }
            }
            int maxCount = ConfigUtil.getTallyProcessingMaxCount();
            int cntRecords = TallyManager.getInstance().buildAccessTallyStatusList(tallyType, tallyTime, offset, maxCount);
            log.info("AccessTallyStatusList created: cntRecords = " + cntRecords);
            
            
            // ????
            if(cntRecords >= 0) {
                // if a complete list has been created, then
                // mark tallyStauts as Processing...
                if(cntRecords == maxCount) {
                    // ??? Try one more time... ????
                    activeMaster = TallyManager.getInstance().markAccessTallyMasterAsStarted(activeMaster, (int) (offset + cntRecords));
                    // TBD: Is there any risk of getting "stuck" in this step forever????  --> Probably, no.
                } else {
                    // Note that if we fail to save all records of the partial list, cntRecords will be less than maxCount even if there could be more data in IngressDB
                    // In the current implementation, we move on to the next step...
                    // TBD: This need to be fixed....
                    activeMaster = TallyManager.getInstance().markAccessTallyMasterAsProcessing(activeMaster, (int) (offset + cntRecords));
                }
                if(activeMaster == null || !TallyStatus.isCompleted(activeMaster.getTallyStatus())) {
                    // TBD:
                    log.warning("Failed to mark activeMaster as Processing. tallyTime = " + tallyTime + "; tallyType = " + tallyType);
                    // Bail out???? What to do????
                    return 0L;   // ???
                } else {
                    tallyStatus = activeMaster.getTallyStatus();
                }
            } else {
            	// ????
            	// retry at the next cron run???
            }
        } 
        
        // [4] Processing -> Completed
        // Not idempotent --> Most likely safe though (two cron runs going through the same status record at the same time is rather unlikely..)
        // (The only way we can make this safe/idempotent is to process each AccessTallyStatus one at a time,
        //   and make each processing of one AccessTallyStatus "atomic"...  
        // --> Is this possible????)
        else if(tallyStatus.equals(TallyStatus.STATUS_PROCESSING)) {
            
            // Get a list of AccessTallyStatus with processed == false
            // Iterate... ???
            // For now, we do one per processing???
            // ????
            
            // TBD:
            // Move this loop into TallyManager???
            // ...
            
            Long processingStartedTime = activeMaster.getProcesingStartedTime();
            if(processingStartedTime == null || processingStartedTime == 0L) {
                // Processing has never started (or, processing just started by another process, but haven't had time to save the record yet.... --> What to do???)
                // Continue
            } else {
                // GAE cron max processing time is 10 mins == 600 secs
                if(processingStartedTime < System.currentTimeMillis() - 600 * 1000L) {
                    // Previous process has probably failed.
                    // Continue with a new processing...
                } else {
                    // We don't know whether another cron process is running, or the previous process has failed...
                    // Just skip it and wait for up to 10 mins, just to be safe...
                    log.warning("Skipping processing because another cron process is currently running or a previous processing has not successfully finished.");
                    return 0L;   // ????
                }
            }

            // Sort of "locking"
            processingStartedTime = System.currentTimeMillis();
            activeMaster = TallyManager.getInstance().markAccessTallyMasterAsProcessingStarted(activeMaster, processingStartedTime);
            // ...


            // Note: if you use maxIter > 1, due to the async update, same record may be multiply processed....
            boolean allProcessed = false;
            int maxCount = 10;    // 10 at a time.  This number should be kept small to avoid two or more cron processes processing same records at the same time....
            int maxIters = 100;   // 100 times, up to 1,000 records...  

            // Hack: To avoid multiple processing of same records (in a single cron run)...
            Set<String> tallyStatusGuidSet = new HashSet<String>();
            // hack....

            int iterCnt = 0;
            while(allProcessed == false && iterCnt < maxIters) {
                // Returns records with isProcessed == false...
                List<AccessTallyStatus> beans = AccessTallyAccessHelper.getInstance().getAccessTallyStatusesForProcessing(tallyType, tallyTime, maxCount);
    
                if(beans == null) {
                    // it's probably an error.
                    log.warning("Failed to retrieve AccessTallyStatus list for processing.");
                    return 0L;  // ????
                }
                
                if(beans.isEmpty()) {
                    // ??? Probably, all done????
                    log.info("Empty AccessTallyStatus list retrieved for tallyType = " + tallyType + "; tallyTime = " + tallyTime);
                    allProcessed = true;
                } else {
                    for(AccessTallyStatus bean : beans) {
                        String guid = bean.getGuid();
                        if(tallyStatusGuidSet.contains(guid)) {
                            log.fine("This AccessTallyStatus has already been processed in this cron run. Skipping this: guid = " + guid);
                            continue;
                        }
                        tallyStatusGuidSet.add(guid);

                        boolean processed = bean.isProcessed();
                        if(processed == false) {
                            boolean suc = TallyManager.getInstance().processAccessTally(bean);                            
                            if(suc == false) {
                                // ??? what to do ????
                                log.warning("Failed to process AccessTallyStatus bean = " + bean.toString());
                                
                                // What to do???
                                // If we cannot process this, it can get stuck here forever.....
                                // We should be able to mark the record and move on....
                                // temporary... just delete it, for now...
                                boolean sucDel = AccessTallyAccessHelper.getInstance().deleteAccessTallyStatus(guid);
                                if(sucDel == false) {
                                    // ??? what to do ????
                                    log.severe("Failed to delete unprocessible AccessTallyStatus bean. This could be a serious problem. guid = " + guid);
                                }
                            }
                        } else {
                            // error???
                            // This cannot happen..
                        }
                    }
                }
                
                ++iterCnt;
            }

            // TBD: If an error occurs while processing access tallies,
            //      It can get stuck in this step forever....
            // What to do????
            if(allProcessed) {
                // if all AccessTallyStatus records have been processed, then
                // mark tallyStatus as Processing...
                activeMaster = TallyManager.getInstance().markAccessTallyMasterAsCompleted(activeMaster);
                if(activeMaster == null) {
                    // TBD:
                    log.warning("Failed to mark activeMaster as Completed. tallyTime = " + tallyTime + "; tallyType = " + tallyType);
                    // Bail out???? What to do????
                    return 0L;   // ???
                } else {
                    tallyStatus = activeMaster.getTallyStatus();
                }
            }
        } 
        
        // [5] TBD
        else {
            // This cannot happen...
            log.warning("Invalid tallyStatus. " + tallyStatus);
            return 0L;   // ???
        }

        
        // temporary
        return 0L;
    }
    
    
}
