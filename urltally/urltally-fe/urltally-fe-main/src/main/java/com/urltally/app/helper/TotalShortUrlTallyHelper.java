package com.urltally.app.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.app.service.TotalShortUrlAccessAppService;
import com.urltally.app.service.UserAppService;
import com.urltally.app.util.TallyTimeUtil;
import com.urltally.ws.BaseException;
import com.urltally.ws.TotalShortUrlAccess;


public class TotalShortUrlTallyHelper
{
    private static final Logger log = Logger.getLogger(TotalShortUrlTallyHelper.class.getName());

    private TotalShortUrlTallyHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private TotalShortUrlAccessAppService mTotalShortUrlAccessAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private TotalShortUrlAccessAppService getTotalShortUrlAccessAppService()
    {
        if(mTotalShortUrlAccessAppService == null) {
            mTotalShortUrlAccessAppService = new TotalShortUrlAccessAppService();
        }
        return mTotalShortUrlAccessAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class TotalShortUrlTallyHelperHolder
    {
        private static final TotalShortUrlTallyHelper INSTANCE = new TotalShortUrlTallyHelper();
    }

    // Singleton method
    public static TotalShortUrlTallyHelper getInstance()
    {
        return TotalShortUrlTallyHelperHolder.INSTANCE;
    }

    

    public String createTotalShortUrlAccess(TotalShortUrlAccess bean)
    {
        String guid = null;
        try {
            guid = getTotalShortUrlAccessAppService().createTotalShortUrlAccess(bean);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create a ShortUrlAccess.", e);
        }
        return guid;
    }

    public int createTotalShortUrlAccesses(List<TotalShortUrlAccess> beans)
    {
        int count = -1;
        try {
            Integer cnt = getTotalShortUrlAccessAppService().createTotalShortUrlAccesses(beans);
            if(cnt != null) {
                count = cnt;
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to create ShortUrlAccess.", e);
        }
        return count;
    }
    
    
    public TotalShortUrlAccess getTotalShortUrlAccess(String guid) 
    {
        TotalShortUrlAccess bean = null;
        try {
            bean = getTotalShortUrlAccessAppService().getTotalShortUrlAccess(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    
    public int getTotalShortUrlAccessTally(String tallyType, String tallyTime, String shortUrl) 
    {
        int count = -1;

        if(! TallyTimeUtil.isValid(tallyTime)) {
            // ????
            return count;
        }
        
        try {
            String filter = "";
            if(tallyType != null) {
                filter = "tallyType == '" + tallyType + "'";
            }
            if(tallyTime != null) {
                if(!filter.isEmpty()) {
                    filter += " && ";
                }
                filter = "tallyTime == '" + tallyTime + "'";
            }
            if(shortUrl != null) {
                if(!filter.isEmpty()) {
                    filter += " && ";
                }
                filter += "shortUrl == '" + shortUrl + "'";
            }
            String ordering = null;
            // There should be at most one....
            List<TotalShortUrlAccess> beans = getTotalShortUrlAccessAppService().findTotalShortUrlAccesses(filter, ordering, null, null, null, null, 0L, 2);
            count = 0;  // empty list means that the count is zero...
            if(beans != null) {
                int size = beans.size();
                if(size > 1) {
                    log.warning("More than one TotalShortUrlAccess record found for tallyType = " + tallyType + "; tallyTime = " + tallyTime + "; shortUrl = " + shortUrl);
                }
                count = beans.get(0).getCount();
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the beans for shortUrl = " + shortUrl, e);
        }
        
        return count;
    }

    
    // startDayHour: inclusive
    // endDayHour: exclusive. Can be null -> Use "now".
    // Returns Map of { dayHour -> count }
    public SortedMap<String, Integer> getTotalShortUrlAccessTallies(String tallyType, String startDayHour, String endDayHour, String shortUrl) 
    {
        if(! TallyTimeUtil.isValid(startDayHour)) {
            // ????
            return null;
        }

        String[] range = TallyTimeUtil.getRange(tallyType, startDayHour, endDayHour);
        if(range == null) {
            return null;
        }
        int numSlices = range.length;
        if(numSlices == 0) {
            return new TreeMap<String, Integer>();  // ????
        }

        
        // TBD
        // Temporary implementation
        // Use a single fetch!!!!
                
//        int[] values = new int[numSlices];
//        for(int i=0; i<numSlices; i++) {
//            values[i] = getTotalShortUrlAccessTally(tallyType, range[i], shortUrl);
//        }


        List<TotalShortUrlAccess> beans = null;
        try {
            String filter = "tallyType == '" + tallyType + "'";
            filter += " && tallyTime >= '" + startDayHour + "' && tallyTime < '" + endDayHour + "'";
            filter += " && shortUrl == '" + shortUrl + "'";
            String ordering = null;
            int maxCount = 10000;   // Note that numSlices still can be bigger than maxCount (because of empty slices w/ count == 0).
            beans = getTotalShortUrlAccessAppService().findTotalShortUrlAccesses(filter, ordering, null, null, null, null, 0L, maxCount);
            if(beans != null) {
                int size = beans.size();
                if(size == maxCount) {
                    log.warning("Failed to retrieve all records: maxCount = " + maxCount);
                }
            }
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the beans for shortUrl = " + shortUrl, e);
        }

        // [1] Build the tally Map
        // [2] Calculate the total/sum over this period.
        int sum = 0;
        SortedMap<String, Integer> tallyMap = new TreeMap<String, Integer>();
        if(beans != null) {
            for(TotalShortUrlAccess b : beans) {
                String t = b.getTallyTime();
                Integer cnt = b.getCount();
                tallyMap.put(t, cnt);
                sum += cnt;
            }
        }
                
        // [3] Fill in the empty slots...
        for(int i=0; i<numSlices; i++) {
            if(! tallyMap.containsKey(range[i])) {
                tallyMap.put(range[i], 0);
            }
        }

        // [4] This is a hack.
        // Add the total/sum to the last date/time field.
        String lastDayHour = TallyTimeUtil.getNext(tallyType, range[numSlices-1]);
        tallyMap.put(lastDayHour, sum);

        // Returns the order list/map of tallies/counts.
        return tallyMap;
    }


}
