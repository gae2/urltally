package com.urltally.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.af.bean.TotalLongUrlAccessBean;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.af.service.manager.ServiceManager;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.TotalLongUrlAccessJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TotalLongUrlAccessWebService // implements TotalLongUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessWebService.class.getName());
     
    // Af service interface.
    private TotalLongUrlAccessService mService = null;

    public TotalLongUrlAccessWebService()
    {
        this(ServiceManager.getTotalLongUrlAccessService());
    }
    public TotalLongUrlAccessWebService(TotalLongUrlAccessService service)
    {
        mService = service;
    }
    
    private TotalLongUrlAccessService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getTotalLongUrlAccessService();
        }
        return mService;
    }
    
    
    public TotalLongUrlAccessJsBean getTotalLongUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            TotalLongUrlAccess totalLongUrlAccess = getService().getTotalLongUrlAccess(guid);
            TotalLongUrlAccessJsBean bean = convertTotalLongUrlAccessToJsBean(totalLongUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTotalLongUrlAccess(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getTotalLongUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TotalLongUrlAccessJsBean> getTotalLongUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TotalLongUrlAccessJsBean> jsBeans = new ArrayList<TotalLongUrlAccessJsBean>();
            List<TotalLongUrlAccess> totalLongUrlAccesses = getService().getTotalLongUrlAccesses(guids);
            if(totalLongUrlAccesses != null) {
                for(TotalLongUrlAccess totalLongUrlAccess : totalLongUrlAccesses) {
                    jsBeans.add(convertTotalLongUrlAccessToJsBean(totalLongUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TotalLongUrlAccessJsBean> getAllTotalLongUrlAccesses() throws WebException
    {
        return getAllTotalLongUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<TotalLongUrlAccessJsBean> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTotalLongUrlAccesses(ordering, offset, count, null);
    }

    public List<TotalLongUrlAccessJsBean> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TotalLongUrlAccessJsBean> jsBeans = new ArrayList<TotalLongUrlAccessJsBean>();
            List<TotalLongUrlAccess> totalLongUrlAccesses = getService().getAllTotalLongUrlAccesses(ordering, offset, count, forwardCursor);
            if(totalLongUrlAccesses != null) {
                for(TotalLongUrlAccess totalLongUrlAccess : totalLongUrlAccesses) {
                    jsBeans.add(convertTotalLongUrlAccessToJsBean(totalLongUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTotalLongUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllTotalLongUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<TotalLongUrlAccessJsBean> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<TotalLongUrlAccessJsBean> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<TotalLongUrlAccessJsBean> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TotalLongUrlAccessJsBean> jsBeans = new ArrayList<TotalLongUrlAccessJsBean>();
            List<TotalLongUrlAccess> totalLongUrlAccesses = getService().findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(totalLongUrlAccesses != null) {
                for(TotalLongUrlAccess totalLongUrlAccess : totalLongUrlAccesses) {
                    jsBeans.add(convertTotalLongUrlAccessToJsBean(totalLongUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTotalLongUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws WebException
    {
        try {
            return getService().createTotalLongUrlAccess(tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTotalLongUrlAccess(String jsonStr) throws WebException
    {
        return createTotalLongUrlAccess(TotalLongUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public String createTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            return getService().createTotalLongUrlAccess(totalLongUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TotalLongUrlAccessJsBean constructTotalLongUrlAccess(String jsonStr) throws WebException
    {
        return constructTotalLongUrlAccess(TotalLongUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public TotalLongUrlAccessJsBean constructTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            totalLongUrlAccess = getService().constructTotalLongUrlAccess(totalLongUrlAccess);
            jsBean = convertTotalLongUrlAccessToJsBean(totalLongUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTotalLongUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws WebException
    {
        try {
            return getService().updateTotalLongUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTotalLongUrlAccess(String jsonStr) throws WebException
    {
        return updateTotalLongUrlAccess(TotalLongUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            return getService().updateTotalLongUrlAccess(totalLongUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TotalLongUrlAccessJsBean refreshTotalLongUrlAccess(String jsonStr) throws WebException
    {
        return refreshTotalLongUrlAccess(TotalLongUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public TotalLongUrlAccessJsBean refreshTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            totalLongUrlAccess = getService().refreshTotalLongUrlAccess(totalLongUrlAccess);
            jsBean = convertTotalLongUrlAccessToJsBean(totalLongUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTotalLongUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteTotalLongUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            return getService().deleteTotalLongUrlAccess(totalLongUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteTotalLongUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static TotalLongUrlAccessJsBean convertTotalLongUrlAccessToJsBean(TotalLongUrlAccess totalLongUrlAccess)
    {
        TotalLongUrlAccessJsBean jsBean = null;
        if(totalLongUrlAccess != null) {
            jsBean = new TotalLongUrlAccessJsBean();
            jsBean.setGuid(totalLongUrlAccess.getGuid());
            jsBean.setTallyType(totalLongUrlAccess.getTallyType());
            jsBean.setTallyTime(totalLongUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(totalLongUrlAccess.getTallyEpoch());
            jsBean.setCount(totalLongUrlAccess.getCount());
            jsBean.setLongUrl(totalLongUrlAccess.getLongUrl());
            jsBean.setLongUrlDomain(totalLongUrlAccess.getLongUrlDomain());
            jsBean.setCreatedTime(totalLongUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(totalLongUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static TotalLongUrlAccess convertTotalLongUrlAccessJsBeanToBean(TotalLongUrlAccessJsBean jsBean)
    {
        TotalLongUrlAccessBean totalLongUrlAccess = null;
        if(jsBean != null) {
            totalLongUrlAccess = new TotalLongUrlAccessBean();
            totalLongUrlAccess.setGuid(jsBean.getGuid());
            totalLongUrlAccess.setTallyType(jsBean.getTallyType());
            totalLongUrlAccess.setTallyTime(jsBean.getTallyTime());
            totalLongUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            totalLongUrlAccess.setCount(jsBean.getCount());
            totalLongUrlAccess.setLongUrl(jsBean.getLongUrl());
            totalLongUrlAccess.setLongUrlDomain(jsBean.getLongUrlDomain());
            totalLongUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            totalLongUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return totalLongUrlAccess;
    }

}
