package com.urltally.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.urltally.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class ShortUrlAccessJsBean implements Serializable  //, ShortUrlAccess
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortUrlAccessJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String tallyTime;
    private Long tallyEpoch;
    private Integer count;
    private String shortUrl;
    private String shortUrlDomain;
    private String longUrl;
    private String longUrlDomain;
    private String redirectType;
    private String refererDomain;
    private String userAgent;
    private String language;
    private String country;
    private Long talliedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ShortUrlAccessJsBean()
    {
        //this((String) null);
    }
    public ShortUrlAccessJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ShortUrlAccessJsBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime)
    {
        this(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, null, null);
    }
    public ShortUrlAccessJsBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.count = count;
        this.shortUrl = shortUrl;
        this.shortUrlDomain = shortUrlDomain;
        this.longUrl = longUrl;
        this.longUrlDomain = longUrlDomain;
        this.redirectType = redirectType;
        this.refererDomain = refererDomain;
        this.userAgent = userAgent;
        this.language = language;
        this.country = country;
        this.talliedTime = talliedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ShortUrlAccessJsBean(ShortUrlAccessJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setTallyTime(bean.getTallyTime());
            setTallyEpoch(bean.getTallyEpoch());
            setCount(bean.getCount());
            setShortUrl(bean.getShortUrl());
            setShortUrlDomain(bean.getShortUrlDomain());
            setLongUrl(bean.getLongUrl());
            setLongUrlDomain(bean.getLongUrlDomain());
            setRedirectType(bean.getRedirectType());
            setRefererDomain(bean.getRefererDomain());
            setUserAgent(bean.getUserAgent());
            setLanguage(bean.getLanguage());
            setCountry(bean.getCountry());
            setTalliedTime(bean.getTalliedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ShortUrlAccessJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ShortUrlAccessJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ShortUrlAccessJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ShortUrlAccessJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getTallyTime()
    {
        return this.tallyTime;
    }
    public void setTallyTime(String tallyTime)
    {
        this.tallyTime = tallyTime;
    }

    public Long getTallyEpoch()
    {
        return this.tallyEpoch;
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        this.tallyEpoch = tallyEpoch;
    }

    public Integer getCount()
    {
        return this.count;
    }
    public void setCount(Integer count)
    {
        this.count = count;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public String getShortUrlDomain()
    {
        return this.shortUrlDomain;
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        this.shortUrlDomain = shortUrlDomain;
    }

    public String getLongUrl()
    {
        return this.longUrl;
    }
    public void setLongUrl(String longUrl)
    {
        this.longUrl = longUrl;
    }

    public String getLongUrlDomain()
    {
        return this.longUrlDomain;
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        this.longUrlDomain = longUrlDomain;
    }

    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    public String getRefererDomain()
    {
        return this.refererDomain;
    }
    public void setRefererDomain(String refererDomain)
    {
        this.refererDomain = refererDomain;
    }

    public String getUserAgent()
    {
        return this.userAgent;
    }
    public void setUserAgent(String userAgent)
    {
        this.userAgent = userAgent;
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getCountry()
    {
        return this.country;
    }
    public void setCountry(String country)
    {
        this.country = country;
    }

    public Long getTalliedTime()
    {
        return this.talliedTime;
    }
    public void setTalliedTime(Long talliedTime)
    {
        this.talliedTime = talliedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("tallyTime:null, ");
        sb.append("tallyEpoch:0, ");
        sb.append("count:0, ");
        sb.append("shortUrl:null, ");
        sb.append("shortUrlDomain:null, ");
        sb.append("longUrl:null, ");
        sb.append("longUrlDomain:null, ");
        sb.append("redirectType:null, ");
        sb.append("refererDomain:null, ");
        sb.append("userAgent:null, ");
        sb.append("language:null, ");
        sb.append("country:null, ");
        sb.append("talliedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("tallyTime:");
        if(this.getTallyTime() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTallyTime()).append("\", ");
        }
        sb.append("tallyEpoch:" + this.getTallyEpoch()).append(", ");
        sb.append("count:" + this.getCount()).append(", ");
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("shortUrlDomain:");
        if(this.getShortUrlDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrlDomain()).append("\", ");
        }
        sb.append("longUrl:");
        if(this.getLongUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrl()).append("\", ");
        }
        sb.append("longUrlDomain:");
        if(this.getLongUrlDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrlDomain()).append("\", ");
        }
        sb.append("redirectType:");
        if(this.getRedirectType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRedirectType()).append("\", ");
        }
        sb.append("refererDomain:");
        if(this.getRefererDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRefererDomain()).append("\", ");
        }
        sb.append("userAgent:");
        if(this.getUserAgent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUserAgent()).append("\", ");
        }
        sb.append("language:");
        if(this.getLanguage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLanguage()).append("\", ");
        }
        sb.append("country:");
        if(this.getCountry() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCountry()).append("\", ");
        }
        sb.append("talliedTime:" + this.getTalliedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getTallyTime() != null) {
            sb.append("\"tallyTime\":").append("\"").append(this.getTallyTime()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyTime\":").append("null, ");
        }
        if(this.getTallyEpoch() != null) {
            sb.append("\"tallyEpoch\":").append("").append(this.getTallyEpoch()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyEpoch\":").append("null, ");
        }
        if(this.getCount() != null) {
            sb.append("\"count\":").append("").append(this.getCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"count\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        if(this.getShortUrlDomain() != null) {
            sb.append("\"shortUrlDomain\":").append("\"").append(this.getShortUrlDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrlDomain\":").append("null, ");
        }
        if(this.getLongUrl() != null) {
            sb.append("\"longUrl\":").append("\"").append(this.getLongUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrl\":").append("null, ");
        }
        if(this.getLongUrlDomain() != null) {
            sb.append("\"longUrlDomain\":").append("\"").append(this.getLongUrlDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrlDomain\":").append("null, ");
        }
        if(this.getRedirectType() != null) {
            sb.append("\"redirectType\":").append("\"").append(this.getRedirectType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"redirectType\":").append("null, ");
        }
        if(this.getRefererDomain() != null) {
            sb.append("\"refererDomain\":").append("\"").append(this.getRefererDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refererDomain\":").append("null, ");
        }
        if(this.getUserAgent() != null) {
            sb.append("\"userAgent\":").append("\"").append(this.getUserAgent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAgent\":").append("null, ");
        }
        if(this.getLanguage() != null) {
            sb.append("\"language\":").append("\"").append(this.getLanguage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"language\":").append("null, ");
        }
        if(this.getCountry() != null) {
            sb.append("\"country\":").append("\"").append(this.getCountry()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"country\":").append("null, ");
        }
        if(this.getTalliedTime() != null) {
            sb.append("\"talliedTime\":").append("").append(this.getTalliedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"talliedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("tallyTime = " + this.tallyTime).append(";");
        sb.append("tallyEpoch = " + this.tallyEpoch).append(";");
        sb.append("count = " + this.count).append(";");
        sb.append("shortUrl = " + this.shortUrl).append(";");
        sb.append("shortUrlDomain = " + this.shortUrlDomain).append(";");
        sb.append("longUrl = " + this.longUrl).append(";");
        sb.append("longUrlDomain = " + this.longUrlDomain).append(";");
        sb.append("redirectType = " + this.redirectType).append(";");
        sb.append("refererDomain = " + this.refererDomain).append(";");
        sb.append("userAgent = " + this.userAgent).append(";");
        sb.append("language = " + this.language).append(";");
        sb.append("country = " + this.country).append(";");
        sb.append("talliedTime = " + this.talliedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }


}
