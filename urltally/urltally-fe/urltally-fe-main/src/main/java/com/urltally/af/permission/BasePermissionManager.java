package com.urltally.af.permission;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.permission.ApiConsumerBasePermission;
import com.urltally.ws.permission.UserBasePermission;
import com.urltally.ws.permission.AccessTallyMasterBasePermission;
import com.urltally.ws.permission.AccessTallyStatusBasePermission;
import com.urltally.ws.permission.MonthlyShortUrlAccessBasePermission;
import com.urltally.ws.permission.WeeklyShortUrlAccessBasePermission;
import com.urltally.ws.permission.DailyShortUrlAccessBasePermission;
import com.urltally.ws.permission.HourlyShortUrlAccessBasePermission;
import com.urltally.ws.permission.CumulativeShortUrlAccessBasePermission;
import com.urltally.ws.permission.CurrentShortUrlAccessBasePermission;
import com.urltally.ws.permission.TotalShortUrlAccessBasePermission;
import com.urltally.ws.permission.TotalLongUrlAccessBasePermission;
import com.urltally.ws.permission.ServiceInfoBasePermission;
import com.urltally.ws.permission.FiveTenBasePermission;


// TBD:
public class BasePermissionManager
{
    private static final Logger log = Logger.getLogger(BasePermissionManager.class.getName());

    private ApiConsumerBasePermission apiConsumerPermission = null;
    private UserBasePermission userPermission = null;
    private AccessTallyMasterBasePermission accessTallyMasterPermission = null;
    private AccessTallyStatusBasePermission accessTallyStatusPermission = null;
    private MonthlyShortUrlAccessBasePermission monthlyShortUrlAccessPermission = null;
    private WeeklyShortUrlAccessBasePermission weeklyShortUrlAccessPermission = null;
    private DailyShortUrlAccessBasePermission dailyShortUrlAccessPermission = null;
    private HourlyShortUrlAccessBasePermission hourlyShortUrlAccessPermission = null;
    private CumulativeShortUrlAccessBasePermission cumulativeShortUrlAccessPermission = null;
    private CurrentShortUrlAccessBasePermission currentShortUrlAccessPermission = null;
    private TotalShortUrlAccessBasePermission totalShortUrlAccessPermission = null;
    private TotalLongUrlAccessBasePermission totalLongUrlAccessPermission = null;
    private ServiceInfoBasePermission serviceInfoPermission = null;
    private FiveTenBasePermission fiveTenPermission = null;

    // Ctor.
    public BasePermissionManager()
    {
        // TBD:
    }

	public ApiConsumerBasePermission getApiConsumerPermission() 
    {
        if(apiConsumerPermission == null) {
            apiConsumerPermission = new ApiConsumerBasePermission();
        }
        return apiConsumerPermission;
    }
	public void setApiConsumerPermission(ApiConsumerBasePermission apiConsumerPermission) 
    {
        this.apiConsumerPermission = apiConsumerPermission;
    }

	public UserBasePermission getUserPermission() 
    {
        if(userPermission == null) {
            userPermission = new UserBasePermission();
        }
        return userPermission;
    }
	public void setUserPermission(UserBasePermission userPermission) 
    {
        this.userPermission = userPermission;
    }

	public AccessTallyMasterBasePermission getAccessTallyMasterPermission() 
    {
        if(accessTallyMasterPermission == null) {
            accessTallyMasterPermission = new AccessTallyMasterBasePermission();
        }
        return accessTallyMasterPermission;
    }
	public void setAccessTallyMasterPermission(AccessTallyMasterBasePermission accessTallyMasterPermission) 
    {
        this.accessTallyMasterPermission = accessTallyMasterPermission;
    }

	public AccessTallyStatusBasePermission getAccessTallyStatusPermission() 
    {
        if(accessTallyStatusPermission == null) {
            accessTallyStatusPermission = new AccessTallyStatusBasePermission();
        }
        return accessTallyStatusPermission;
    }
	public void setAccessTallyStatusPermission(AccessTallyStatusBasePermission accessTallyStatusPermission) 
    {
        this.accessTallyStatusPermission = accessTallyStatusPermission;
    }

	public MonthlyShortUrlAccessBasePermission getMonthlyShortUrlAccessPermission() 
    {
        if(monthlyShortUrlAccessPermission == null) {
            monthlyShortUrlAccessPermission = new MonthlyShortUrlAccessBasePermission();
        }
        return monthlyShortUrlAccessPermission;
    }
	public void setMonthlyShortUrlAccessPermission(MonthlyShortUrlAccessBasePermission monthlyShortUrlAccessPermission) 
    {
        this.monthlyShortUrlAccessPermission = monthlyShortUrlAccessPermission;
    }

	public WeeklyShortUrlAccessBasePermission getWeeklyShortUrlAccessPermission() 
    {
        if(weeklyShortUrlAccessPermission == null) {
            weeklyShortUrlAccessPermission = new WeeklyShortUrlAccessBasePermission();
        }
        return weeklyShortUrlAccessPermission;
    }
	public void setWeeklyShortUrlAccessPermission(WeeklyShortUrlAccessBasePermission weeklyShortUrlAccessPermission) 
    {
        this.weeklyShortUrlAccessPermission = weeklyShortUrlAccessPermission;
    }

	public DailyShortUrlAccessBasePermission getDailyShortUrlAccessPermission() 
    {
        if(dailyShortUrlAccessPermission == null) {
            dailyShortUrlAccessPermission = new DailyShortUrlAccessBasePermission();
        }
        return dailyShortUrlAccessPermission;
    }
	public void setDailyShortUrlAccessPermission(DailyShortUrlAccessBasePermission dailyShortUrlAccessPermission) 
    {
        this.dailyShortUrlAccessPermission = dailyShortUrlAccessPermission;
    }

	public HourlyShortUrlAccessBasePermission getHourlyShortUrlAccessPermission() 
    {
        if(hourlyShortUrlAccessPermission == null) {
            hourlyShortUrlAccessPermission = new HourlyShortUrlAccessBasePermission();
        }
        return hourlyShortUrlAccessPermission;
    }
	public void setHourlyShortUrlAccessPermission(HourlyShortUrlAccessBasePermission hourlyShortUrlAccessPermission) 
    {
        this.hourlyShortUrlAccessPermission = hourlyShortUrlAccessPermission;
    }

	public CumulativeShortUrlAccessBasePermission getCumulativeShortUrlAccessPermission() 
    {
        if(cumulativeShortUrlAccessPermission == null) {
            cumulativeShortUrlAccessPermission = new CumulativeShortUrlAccessBasePermission();
        }
        return cumulativeShortUrlAccessPermission;
    }
	public void setCumulativeShortUrlAccessPermission(CumulativeShortUrlAccessBasePermission cumulativeShortUrlAccessPermission) 
    {
        this.cumulativeShortUrlAccessPermission = cumulativeShortUrlAccessPermission;
    }

	public CurrentShortUrlAccessBasePermission getCurrentShortUrlAccessPermission() 
    {
        if(currentShortUrlAccessPermission == null) {
            currentShortUrlAccessPermission = new CurrentShortUrlAccessBasePermission();
        }
        return currentShortUrlAccessPermission;
    }
	public void setCurrentShortUrlAccessPermission(CurrentShortUrlAccessBasePermission currentShortUrlAccessPermission) 
    {
        this.currentShortUrlAccessPermission = currentShortUrlAccessPermission;
    }

	public TotalShortUrlAccessBasePermission getTotalShortUrlAccessPermission() 
    {
        if(totalShortUrlAccessPermission == null) {
            totalShortUrlAccessPermission = new TotalShortUrlAccessBasePermission();
        }
        return totalShortUrlAccessPermission;
    }
	public void setTotalShortUrlAccessPermission(TotalShortUrlAccessBasePermission totalShortUrlAccessPermission) 
    {
        this.totalShortUrlAccessPermission = totalShortUrlAccessPermission;
    }

	public TotalLongUrlAccessBasePermission getTotalLongUrlAccessPermission() 
    {
        if(totalLongUrlAccessPermission == null) {
            totalLongUrlAccessPermission = new TotalLongUrlAccessBasePermission();
        }
        return totalLongUrlAccessPermission;
    }
	public void setTotalLongUrlAccessPermission(TotalLongUrlAccessBasePermission totalLongUrlAccessPermission) 
    {
        this.totalLongUrlAccessPermission = totalLongUrlAccessPermission;
    }

	public ServiceInfoBasePermission getServiceInfoPermission() 
    {
        if(serviceInfoPermission == null) {
            serviceInfoPermission = new ServiceInfoBasePermission();
        }
        return serviceInfoPermission;
    }
	public void setServiceInfoPermission(ServiceInfoBasePermission serviceInfoPermission) 
    {
        this.serviceInfoPermission = serviceInfoPermission;
    }

	public FiveTenBasePermission getFiveTenPermission() 
    {
        if(fiveTenPermission == null) {
            fiveTenPermission = new FiveTenBasePermission();
        }
        return fiveTenPermission;
    }
	public void setFiveTenPermission(FiveTenBasePermission fiveTenPermission) 
    {
        this.fiveTenPermission = fiveTenPermission;
    }


	public boolean isPermissionRequired(String resource, String action) 
    {
        if(resource == null || resource.isEmpty()) {
            return false;   // ???
        } else if(resource.equals("ApiConsumer")) {
            return getApiConsumerPermission().isPermissionRequired(action);
        } else if(resource.equals("User")) {
            return getUserPermission().isPermissionRequired(action);
        } else if(resource.equals("AccessTallyMaster")) {
            return getAccessTallyMasterPermission().isPermissionRequired(action);
        } else if(resource.equals("AccessTallyStatus")) {
            return getAccessTallyStatusPermission().isPermissionRequired(action);
        } else if(resource.equals("MonthlyShortUrlAccess")) {
            return getMonthlyShortUrlAccessPermission().isPermissionRequired(action);
        } else if(resource.equals("WeeklyShortUrlAccess")) {
            return getWeeklyShortUrlAccessPermission().isPermissionRequired(action);
        } else if(resource.equals("DailyShortUrlAccess")) {
            return getDailyShortUrlAccessPermission().isPermissionRequired(action);
        } else if(resource.equals("HourlyShortUrlAccess")) {
            return getHourlyShortUrlAccessPermission().isPermissionRequired(action);
        } else if(resource.equals("CumulativeShortUrlAccess")) {
            return getCumulativeShortUrlAccessPermission().isPermissionRequired(action);
        } else if(resource.equals("CurrentShortUrlAccess")) {
            return getCurrentShortUrlAccessPermission().isPermissionRequired(action);
        } else if(resource.equals("TotalShortUrlAccess")) {
            return getTotalShortUrlAccessPermission().isPermissionRequired(action);
        } else if(resource.equals("TotalLongUrlAccess")) {
            return getTotalLongUrlAccessPermission().isPermissionRequired(action);
        } else if(resource.equals("ServiceInfo")) {
            return getServiceInfoPermission().isPermissionRequired(action);
        } else if(resource.equals("FiveTen")) {
            return getFiveTenPermission().isPermissionRequired(action);
        } else {
            log.warning("Unrecognized resource = " + resource);
            return true;   // ???
        }
    }

    // TBD: "instance" field is currently ignored...
	public boolean isPermissionRequired(String permissionName) 
    {
        if(permissionName == null || permissionName.isEmpty()) {
            return false;   // ???
        } else if(permissionName.startsWith("ApiConsumer::")) {
            String resource = "ApiConsumer";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("User::")) {
            String resource = "User";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("AccessTallyMaster::")) {
            String resource = "AccessTallyMaster";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("AccessTallyStatus::")) {
            String resource = "AccessTallyStatus";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("MonthlyShortUrlAccess::")) {
            String resource = "MonthlyShortUrlAccess";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("WeeklyShortUrlAccess::")) {
            String resource = "WeeklyShortUrlAccess";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("DailyShortUrlAccess::")) {
            String resource = "DailyShortUrlAccess";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("HourlyShortUrlAccess::")) {
            String resource = "HourlyShortUrlAccess";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("CumulativeShortUrlAccess::")) {
            String resource = "CumulativeShortUrlAccess";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("CurrentShortUrlAccess::")) {
            String resource = "CurrentShortUrlAccess";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TotalShortUrlAccess::")) {
            String resource = "TotalShortUrlAccess";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("TotalLongUrlAccess::")) {
            String resource = "TotalLongUrlAccess";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("ServiceInfo::")) {
            String resource = "ServiceInfo";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else if(permissionName.startsWith("FiveTen::")) {
            String resource = "FiveTen";
            String action = permissionName.substring(permissionName.indexOf("::") + 2);
            return isPermissionRequired(resource, action);
        } else {
            log.warning("Unrecognized permissionName = " + permissionName);
            return true;   // ???
        }
    }

}
