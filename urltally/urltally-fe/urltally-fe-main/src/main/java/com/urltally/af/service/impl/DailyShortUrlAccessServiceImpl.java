package com.urltally.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.DailyShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DailyShortUrlAccessServiceImpl implements DailyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(DailyShortUrlAccessServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "DailyShortUrlAccess-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("DailyShortUrlAccess:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public DailyShortUrlAccessServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // DailyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DailyShortUrlAccess getDailyShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDailyShortUrlAccess(): guid = " + guid);

        DailyShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (DailyShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DailyShortUrlAccessBean) getProxyFactory().getDailyShortUrlAccessServiceProxy().getDailyShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DailyShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccessBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getDailyShortUrlAccess(String guid, String field) throws BaseException
    {
        DailyShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (DailyShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DailyShortUrlAccessBean) getProxyFactory().getDailyShortUrlAccessServiceProxy().getDailyShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DailyShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccessBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return bean.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return bean.getTallyEpoch();
        } else if(field.equals("count")) {
            return bean.getCount();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return bean.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return bean.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return bean.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return bean.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return bean.getUserAgent();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("country")) {
            return bean.getCountry();
        } else if(field.equals("talliedTime")) {
            return bean.getTalliedTime();
        } else if(field.equals("year")) {
            return bean.getYear();
        } else if(field.equals("day")) {
            return bean.getDay();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DailyShortUrlAccess> getDailyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getDailyShortUrlAccesses()");

        // TBD: Is there a better way????
        List<DailyShortUrlAccess> dailyShortUrlAccesses = getProxyFactory().getDailyShortUrlAccessServiceProxy().getDailyShortUrlAccesses(guids);
        if(dailyShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccessBean list.");
        }

        log.finer("END");
        return dailyShortUrlAccesses;
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses() throws BaseException
    {
        return getAllDailyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDailyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDailyShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DailyShortUrlAccess> dailyShortUrlAccesses = getProxyFactory().getDailyShortUrlAccessServiceProxy().getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(dailyShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccessBean list.");
        }

        log.finer("END");
        return dailyShortUrlAccesses;
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDailyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDailyShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDailyShortUrlAccessServiceProxy().getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve DailyShortUrlAccessBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DailyShortUrlAccessBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DailyShortUrlAccessServiceImpl.findDailyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DailyShortUrlAccess> dailyShortUrlAccesses = getProxyFactory().getDailyShortUrlAccessServiceProxy().findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dailyShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to find dailyShortUrlAccesses for the given criterion.");
        }

        log.finer("END");
        return dailyShortUrlAccesses;
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DailyShortUrlAccessServiceImpl.findDailyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDailyShortUrlAccessServiceProxy().findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find DailyShortUrlAccess keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DailyShortUrlAccess key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DailyShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getDailyShortUrlAccessServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDailyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        DailyShortUrlAccessBean bean = new DailyShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
        return createDailyShortUrlAccess(bean);
    }

    @Override
    public String createDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DailyShortUrlAccess bean = constructDailyShortUrlAccess(dailyShortUrlAccess);
        //return bean.getGuid();

        // Param dailyShortUrlAccess cannot be null.....
        if(dailyShortUrlAccess == null) {
            log.log(Level.INFO, "Param dailyShortUrlAccess is null!");
            throw new BadRequestException("Param dailyShortUrlAccess object is null!");
        }
        DailyShortUrlAccessBean bean = null;
        if(dailyShortUrlAccess instanceof DailyShortUrlAccessBean) {
            bean = (DailyShortUrlAccessBean) dailyShortUrlAccess;
        } else if(dailyShortUrlAccess instanceof DailyShortUrlAccess) {
            // bean = new DailyShortUrlAccessBean(null, dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DailyShortUrlAccessBean(dailyShortUrlAccess.getGuid(), dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
        } else {
            log.log(Level.WARNING, "createDailyShortUrlAccess(): Arg dailyShortUrlAccess is of an unknown type.");
            //bean = new DailyShortUrlAccessBean();
            bean = new DailyShortUrlAccessBean(dailyShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDailyShortUrlAccessServiceProxy().createDailyShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public DailyShortUrlAccess constructDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param dailyShortUrlAccess cannot be null.....
        if(dailyShortUrlAccess == null) {
            log.log(Level.INFO, "Param dailyShortUrlAccess is null!");
            throw new BadRequestException("Param dailyShortUrlAccess object is null!");
        }
        DailyShortUrlAccessBean bean = null;
        if(dailyShortUrlAccess instanceof DailyShortUrlAccessBean) {
            bean = (DailyShortUrlAccessBean) dailyShortUrlAccess;
        } else if(dailyShortUrlAccess instanceof DailyShortUrlAccess) {
            // bean = new DailyShortUrlAccessBean(null, dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DailyShortUrlAccessBean(dailyShortUrlAccess.getGuid(), dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
        } else {
            log.log(Level.WARNING, "createDailyShortUrlAccess(): Arg dailyShortUrlAccess is of an unknown type.");
            //bean = new DailyShortUrlAccessBean();
            bean = new DailyShortUrlAccessBean(dailyShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDailyShortUrlAccessServiceProxy().createDailyShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DailyShortUrlAccessBean bean = new DailyShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
        return updateDailyShortUrlAccess(bean);
    }
        
    @Override
    public Boolean updateDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DailyShortUrlAccess bean = refreshDailyShortUrlAccess(dailyShortUrlAccess);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param dailyShortUrlAccess cannot be null.....
        if(dailyShortUrlAccess == null || dailyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param dailyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param dailyShortUrlAccess object or its guid is null!");
        }
        DailyShortUrlAccessBean bean = null;
        if(dailyShortUrlAccess instanceof DailyShortUrlAccessBean) {
            bean = (DailyShortUrlAccessBean) dailyShortUrlAccess;
        } else {  // if(dailyShortUrlAccess instanceof DailyShortUrlAccess)
            bean = new DailyShortUrlAccessBean(dailyShortUrlAccess.getGuid(), dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
        }
        Boolean suc = getProxyFactory().getDailyShortUrlAccessServiceProxy().updateDailyShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public DailyShortUrlAccess refreshDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param dailyShortUrlAccess cannot be null.....
        if(dailyShortUrlAccess == null || dailyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param dailyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param dailyShortUrlAccess object or its guid is null!");
        }
        DailyShortUrlAccessBean bean = null;
        if(dailyShortUrlAccess instanceof DailyShortUrlAccessBean) {
            bean = (DailyShortUrlAccessBean) dailyShortUrlAccess;
        } else {  // if(dailyShortUrlAccess instanceof DailyShortUrlAccess)
            bean = new DailyShortUrlAccessBean(dailyShortUrlAccess.getGuid(), dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
        }
        Boolean suc = getProxyFactory().getDailyShortUrlAccessServiceProxy().updateDailyShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getDailyShortUrlAccessServiceProxy().deleteDailyShortUrlAccess(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            DailyShortUrlAccess dailyShortUrlAccess = null;
            try {
                dailyShortUrlAccess = getProxyFactory().getDailyShortUrlAccessServiceProxy().getDailyShortUrlAccess(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch dailyShortUrlAccess with a key, " + guid);
                return false;
            }
            if(dailyShortUrlAccess != null) {
                String beanGuid = dailyShortUrlAccess.getGuid();
                Boolean suc1 = getProxyFactory().getDailyShortUrlAccessServiceProxy().deleteDailyShortUrlAccess(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("dailyShortUrlAccess with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param dailyShortUrlAccess cannot be null.....
        if(dailyShortUrlAccess == null || dailyShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param dailyShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param dailyShortUrlAccess object or its guid is null!");
        }
        DailyShortUrlAccessBean bean = null;
        if(dailyShortUrlAccess instanceof DailyShortUrlAccessBean) {
            bean = (DailyShortUrlAccessBean) dailyShortUrlAccess;
        } else {  // if(dailyShortUrlAccess instanceof DailyShortUrlAccess)
            // ????
            log.warning("dailyShortUrlAccess is not an instance of DailyShortUrlAccessBean.");
            bean = new DailyShortUrlAccessBean(dailyShortUrlAccess.getGuid(), dailyShortUrlAccess.getTallyTime(), dailyShortUrlAccess.getTallyEpoch(), dailyShortUrlAccess.getCount(), dailyShortUrlAccess.getShortUrl(), dailyShortUrlAccess.getShortUrlDomain(), dailyShortUrlAccess.getLongUrl(), dailyShortUrlAccess.getLongUrlDomain(), dailyShortUrlAccess.getRedirectType(), dailyShortUrlAccess.getRefererDomain(), dailyShortUrlAccess.getUserAgent(), dailyShortUrlAccess.getLanguage(), dailyShortUrlAccess.getCountry(), dailyShortUrlAccess.getTalliedTime(), dailyShortUrlAccess.getYear(), dailyShortUrlAccess.getDay());
        }
        Boolean suc = getProxyFactory().getDailyShortUrlAccessServiceProxy().deleteDailyShortUrlAccess(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getDailyShortUrlAccessServiceProxy().deleteDailyShortUrlAccesses(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDailyShortUrlAccesses(List<DailyShortUrlAccess> dailyShortUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");

        if(dailyShortUrlAccesses == null) {
            log.log(Level.WARNING, "createDailyShortUrlAccesses() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = dailyShortUrlAccesses.size();
        if(size == 0) {
            log.log(Level.WARNING, "createDailyShortUrlAccesses() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(DailyShortUrlAccess dailyShortUrlAccess : dailyShortUrlAccesses) {
            String guid = createDailyShortUrlAccess(dailyShortUrlAccess);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createDailyShortUrlAccesses() failed for at least one dailyShortUrlAccess. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateDailyShortUrlAccesses(List<DailyShortUrlAccess> dailyShortUrlAccesses) throws BaseException
    //{
    //}

}
