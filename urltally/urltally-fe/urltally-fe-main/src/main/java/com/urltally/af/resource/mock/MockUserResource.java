package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.GeoPointStruct;
import com.urltally.ws.StreetAddressStruct;
import com.urltally.ws.GaeAppStruct;
import com.urltally.ws.FullNameStruct;
import com.urltally.ws.GaeUserStruct;
import com.urltally.ws.User;
import com.urltally.ws.stub.UserStub;
import com.urltally.ws.stub.UserListStub;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GaeUserStructBean;
import com.urltally.af.bean.UserBean;
import com.urltally.af.resource.UserResource;
import com.urltally.af.resource.util.GeoPointStructResourceUtil;
import com.urltally.af.resource.util.StreetAddressStructResourceUtil;
import com.urltally.af.resource.util.GaeAppStructResourceUtil;
import com.urltally.af.resource.util.FullNameStructResourceUtil;
import com.urltally.af.resource.util.GaeUserStructResourceUtil;


// MockUserResource is a decorator.
// It can be used as a base class to mock UserResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/users/")
public abstract class MockUserResource implements UserResource
{
    private static final Logger log = Logger.getLogger(MockUserResource.class.getName());

    // MockUserResource uses the decorator design pattern.
    private UserResource decoratedResource;

    public MockUserResource(UserResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected UserResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(UserResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUsers(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllUserKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUsers(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findUsersAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findUsersAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getUserAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getUserAsHtml(guid);
//    }

    @Override
    public Response getUser(String guid) throws BaseResourceException
    {
        return decoratedResource.getUser(guid);
    }

    @Override
    public Response getUserAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getUserAsJsonp(guid, callback);
    }

    @Override
    public Response getUser(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getUser(guid, field);
    }

    // TBD
    @Override
    public Response constructUser(UserStub user) throws BaseResourceException
    {
        return decoratedResource.constructUser(user);
    }

    @Override
    public Response createUser(UserStub user) throws BaseResourceException
    {
        return decoratedResource.createUser(user);
    }

//    @Override
//    public Response createUser(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createUser(formParams);
//    }

    // TBD
    @Override
    public Response refreshUser(String guid, UserStub user) throws BaseResourceException
    {
        return decoratedResource.refreshUser(guid, user);
    }

    @Override
    public Response updateUser(String guid, UserStub user) throws BaseResourceException
    {
        return decoratedResource.updateUser(guid, user);
    }

    @Override
    public Response updateUser(String guid, String managerApp, Long appAcl, String gaeApp, String aeryId, String sessionId, String ancestorGuid, String name, String usercode, String username, String nickname, String avatar, String email, String openId, String gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, String streetAddress, String geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime)
    {
        return decoratedResource.updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

//    @Override
//    public Response updateUser(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateUser(guid, formParams);
//    }

    @Override
    public Response deleteUser(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteUser(guid);
    }

    @Override
    public Response deleteUsers(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteUsers(filter, params, values);
    }


// TBD ....
    @Override
    public Response createUsers(UserListStub users) throws BaseResourceException
    {
        return decoratedResource.createUsers(users);
    }


}
