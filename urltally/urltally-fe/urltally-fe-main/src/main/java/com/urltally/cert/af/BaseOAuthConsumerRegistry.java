package com.urltally.cert.af;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseOAuthConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseOAuthConsumerRegistry.class.getName());

    // Consumer key-secret map.
    private Map<String, String> consumerSecretMap;

    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD:
        consumerSecretMap.put("5a23b183-eba6-49ae-a665-87094fa3f3e4", "5bb5b93c-c7ff-431a-98b9-c88a0a56eaf8");  // UrlTallyApp
        consumerSecretMap.put("ae331103-ef56-4f5f-b808-e1be8189789e", "f41b59cb-913f-44ad-93d7-8f4f8bbfe04a");  // UrlTallyApp + UrlTallyWeb
        consumerSecretMap.put("1d69aa70-9199-4e2e-9bd6-3a01b40299e8", "993a3c2f-f382-4aec-8dbb-0c254d3e0bfe");  // UrlTallyApp + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
