package com.urltally.af.resource.impl;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.DatatypeConverter;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.api.json.JSONWithPadding;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.af.config.Config;
import com.urltally.af.auth.TwoLeggedOAuthProvider;
import com.urltally.af.auth.common.AuthMethod;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;
import com.urltally.ws.resource.exception.UnauthorizedRsException;

import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.ws.stub.KeyListStub;
import com.urltally.ws.stub.WeeklyShortUrlAccessStub;
import com.urltally.ws.stub.WeeklyShortUrlAccessListStub;
import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.af.service.manager.ServiceManager;
import com.urltally.af.resource.WeeklyShortUrlAccessResource;


@Path("/weeklyShortUrlAccesses/")
public class WeeklyShortUrlAccessResourceImpl extends BaseResourceImpl implements WeeklyShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(WeeklyShortUrlAccessResourceImpl.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    public WeeklyShortUrlAccessResourceImpl(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        if(log.isLoggable(Level.INFO)) log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }        

        this.httpContext = httpContext;

        // TBD:
        // The auth-related checking should be done here rather than putting the same code in every method... ???
        // ...
    }

    // Throws exception if auth check fails
    private void doAuthCheck() throws BaseResourceException
    {
        // if(isRunningOnDevel && isIgnoreAuth()) {
        if(isIgnoreAuth()) {
            log.warning("OAuth is being bypassed! If it's a production environment, then it could be a serious error.");
        } else {
            String authMethod = getAuthMethod();
            if(authMethod.equals(AuthMethod.NONE)) {
                // Auth ignored.
            } else if(authMethod.equals(AuthMethod.BASIC)) {
                String authType = null;
                String encodedAuthToken = null;
                if(httpHeaders != null) {
                    List<String> authList = httpHeaders.getRequestHeader("Authorization");
                    if(authList != null && !authList.isEmpty()) {
                        String authLine = authList.get(0);    // Use the first value.
                        String[] parts = authLine.split("\\s+", 2);
                        authType = parts[0];
                        encodedAuthToken = parts[1];
                    }
                }
                if(authType == null || ! authType.equalsIgnoreCase("Basic")) {
                    log.warning("HTTP Basic authentication failed. Request authType = " + authType);
                    throw new UnauthorizedRsException("HTTP Basic authentication failed. Request authType = " + authType, resourceUri);
                }
                String authToken = null;
                if(encodedAuthToken != null) {
                    byte[] decodedBytes = DatatypeConverter.parseBase64Binary(encodedAuthToken);
                    authToken = new String(decodedBytes);
                }
                String serverUsernamePasswordPair = getHttpBasicAuthUsernameAndPassword(null);
                if(authToken == null || ! authToken.equals(serverUsernamePasswordPair)) {
                    log.warning("HTTP Basic authentication failed: Invalid username/password.");
                    throw new UnauthorizedRsException("HTTP Basic authentication failed: Invalid username/password.", resourceUri);                        
                }
            } else if(authMethod.equals(AuthMethod.BEARER)) {
                String tokenType = null;
                String bearerToken = null;
                if(httpHeaders != null) {
                    List<String> authList = httpHeaders.getRequestHeader("Authorization");
                    if(authList != null && !authList.isEmpty()) {
                        String authLine = authList.get(0);    // Use the first value.
                        String[] parts = authLine.split("\\s+", 2);
                        tokenType = parts[0];
                        bearerToken = parts[1];
                    }
                }
                if(tokenType == null || ! tokenType.equalsIgnoreCase("Bearer")) {
                    log.warning("OAuth2 authorization failed. Request tokenType = " + tokenType);
                    throw new UnauthorizedRsException("OAuth2 authorization failed. Request tokenType = " + tokenType, resourceUri);
                }
                String serverUserAccessToken = getUserOAuth2AccessToken(null);
                if(bearerToken == null || ! bearerToken.equals(serverUserAccessToken)) {
                    log.warning("OAuth2 authorization failed: Invalid bearer token");
                    throw new UnauthorizedRsException("OAuth2 authorization failed: Invalid bearer token", resourceUri);                        
                }
            } else if(authMethod.equals(AuthMethod.TWOLEGGED)) {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            } else {
                // Unsupported auth method.
                log.warning("AuthMethod currently unsupported: " + authMethod);
                throw new UnauthorizedRsException("AuthMethod currently unsupported: " + authMethod, resourceUri);
            }
        }
    }


    private Response getWeeklyShortUrlAccessList(List<WeeklyShortUrlAccess> beans, StringCursor forwardCursor) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        WeeklyShortUrlAccessListStub listStub = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            listStub = new WeeklyShortUrlAccessListStub();
        } else {
            List<WeeklyShortUrlAccessStub> stubs = new ArrayList<WeeklyShortUrlAccessStub>();
            Iterator<WeeklyShortUrlAccess> it = beans.iterator();
            while(it.hasNext()) {
                WeeklyShortUrlAccess bean = (WeeklyShortUrlAccess) it.next();
                stubs.add(WeeklyShortUrlAccessStub.convertBeanToStub(bean));
            }
            listStub = new WeeklyShortUrlAccessListStub(stubs);
        }
        String cursorString = null;
        if(forwardCursor != null) {
            cursorString = forwardCursor.getWebSafeString();
        }
        listStub.setForwardCursor(cursorString);
        return Response.ok(listStub).build();        
    }

    // TBD
    private Response getWeeklyShortUrlAccessListAsJsonp(List<WeeklyShortUrlAccess> beans, StringCursor forwardCursor, String callback) throws BaseResourceException
    {
        if(beans == null) {
            log.log(Level.WARNING, "Failed to retrieve the list.");
            throw new RequestForbiddenRsException("Failed to retrieve the list.");
        }

        WeeklyShortUrlAccessListStub listStub = null;
        if(beans.size() == 0) {
            log.log(Level.INFO, "Retrieved an empty list.");
            listStub = new WeeklyShortUrlAccessListStub();
        } else {
            List<WeeklyShortUrlAccessStub> stubs = new ArrayList<WeeklyShortUrlAccessStub>();
            Iterator<WeeklyShortUrlAccess> it = beans.iterator();
            while(it.hasNext()) {
                WeeklyShortUrlAccess bean = (WeeklyShortUrlAccess) it.next();
                stubs.add(WeeklyShortUrlAccessStub.convertBeanToStub(bean));
            }
            listStub = new WeeklyShortUrlAccessListStub(stubs);
        }
        String cursorString = null;
        if(forwardCursor != null) {
            cursorString = forwardCursor.getWebSafeString();
        }
        listStub.setForwardCursor(cursorString);
        return Response.ok(new JSONWithPadding(listStub, callback)).build();        
    }

    @Override
    public Response getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<WeeklyShortUrlAccess> beans = ServiceManager.getWeeklyShortUrlAccessService().getAllWeeklyShortUrlAccesses(ordering, offset, count, forwardCursor);
            return getWeeklyShortUrlAccessList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<String> keys = ServiceManager.getWeeklyShortUrlAccessService().getAllWeeklyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<String> keys = ServiceManager.getWeeklyShortUrlAccessService().findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            KeyListStub listStub = new KeyListStub(keys);
            String cursorString = null;
            if(forwardCursor != null) {
                cursorString = forwardCursor.getWebSafeString();
            }
            listStub.setForwardCursor(cursorString);
            return Response.ok(listStub).cacheControl(CacheControl.valueOf("no-cache")).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<WeeklyShortUrlAccess> beans = ServiceManager.getWeeklyShortUrlAccessService().findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getWeeklyShortUrlAccessList(beans, forwardCursor);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response findWeeklyShortUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            List<WeeklyShortUrlAccess> beans = ServiceManager.getWeeklyShortUrlAccessService().findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return getWeeklyShortUrlAccessListAsJsonp(beans, forwardCursor, callback);
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            Long count = ServiceManager.getWeeklyShortUrlAccessService().getCount(filter, params, values, aggregate);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
//    public Response getWeeklyShortUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        // TBD
//        throw new NotImplementedRsException("Html format currently not supported.", resourceUri);
//    }

    @Override
    public Response getWeeklyShortUrlAccess(String guid) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            WeeklyShortUrlAccess bean = ServiceManager.getWeeklyShortUrlAccessService().getWeeklyShortUrlAccess(guid);
            WeeklyShortUrlAccessStub stub = WeeklyShortUrlAccessStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("WeeklyShortUrlAccess stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full WeeklyShortUrlAccess stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(stub).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(stub).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getWeeklyShortUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            WeeklyShortUrlAccess bean = ServiceManager.getWeeklyShortUrlAccessService().getWeeklyShortUrlAccess(guid);
            WeeklyShortUrlAccessStub stub = WeeklyShortUrlAccessStub.convertBeanToStub(bean);

            EntityTag eTag = new EntityTag(Integer.toString(stub.hashCode()));
            Response.ResponseBuilder responseBuilder = request.evaluatePreconditions(eTag);
            if (responseBuilder != null) {  // Etag match
                log.info("WeeklyShortUrlAccess stub object has not changed. Returning unmodified response code.");
                return responseBuilder.build();
            }
            log.info("Returning a full WeeklyShortUrlAccess stub object.");

            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag).expires(expirationDate);
            ResponseBuilder builder = Response.ok(new JSONWithPadding( stub, callback )).tag(eTag);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response getWeeklyShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            doAuthCheck();
        }

        try {
            if(guid != null) {
                guid = guid.toLowerCase();  // TBD: Validate/normalize guid....
            }
            if(field == null || field.trim().length() == 0) {
                return getWeeklyShortUrlAccess(guid);
            }
            WeeklyShortUrlAccess bean = ServiceManager.getWeeklyShortUrlAccessService().getWeeklyShortUrlAccess(guid);
            String value = null;
            if(bean != null) {
                if(field.equals("guid")) {
                    value = bean.getGuid();
                } else if(field.equals("tallyTime")) {
                    String fval = bean.getTallyTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("tallyEpoch")) {
                    Long fval = bean.getTallyEpoch();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("count")) {
                    Integer fval = bean.getCount();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("shortUrl")) {
                    String fval = bean.getShortUrl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("shortUrlDomain")) {
                    String fval = bean.getShortUrlDomain();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("longUrl")) {
                    String fval = bean.getLongUrl();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("longUrlDomain")) {
                    String fval = bean.getLongUrlDomain();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("redirectType")) {
                    String fval = bean.getRedirectType();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("refererDomain")) {
                    String fval = bean.getRefererDomain();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("userAgent")) {
                    String fval = bean.getUserAgent();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("language")) {
                    String fval = bean.getLanguage();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("country")) {
                    String fval = bean.getCountry();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("talliedTime")) {
                    Long fval = bean.getTalliedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("year")) {
                    Integer fval = bean.getYear();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("week")) {
                    Integer fval = bean.getWeek();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("createdTime")) {
                    Long fval = bean.getCreatedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                } else if(field.equals("modifiedTime")) {
                    Long fval = bean.getModifiedTime();
                    //if(fval instanceof String) {
                    //    value = fval;
                    //} else {
                        value = (fval == null) ? "" : fval.toString();
                    //}
                }
            }
            //Date expirationDate = new Date(System.currentTimeMillis() + CommonConstants.CACHE_EXPIRATION_TIME);
            //ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN).expires(expirationDate);
            ResponseBuilder builder = Response.ok(value).type(MediaType.TEXT_PLAIN);
            Date modifiedDate = null;
            if(bean.getModifiedTime() != null) {
                modifiedDate = new Date(bean.getModifiedTime());
            } else {
                if(bean.getCreatedTime() != null) {
                    modifiedDate = new Date(bean.getCreatedTime());
                } else {   // This should not happen.
                    modifiedDate = new Date();  // Just use "now".                        
                }
            }
            return builder.lastModified(modifiedDate).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }        
    }

    // TBD
    @Override
    public Response constructWeeklyShortUrlAccess(WeeklyShortUrlAccessStub weeklyShortUrlAccess) throws BaseResourceException
    {
        doAuthCheck();

        try {
            WeeklyShortUrlAccessBean bean = convertWeeklyShortUrlAccessStubToBean(weeklyShortUrlAccess);
            bean = (WeeklyShortUrlAccessBean) ServiceManager.getWeeklyShortUrlAccessService().constructWeeklyShortUrlAccess(bean);
            weeklyShortUrlAccess = WeeklyShortUrlAccessStub.convertBeanToStub(bean);
            String guid = weeklyShortUrlAccess.getGuid();
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(weeklyShortUrlAccess).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createWeeklyShortUrlAccess(WeeklyShortUrlAccessStub weeklyShortUrlAccess) throws BaseResourceException
    {
        doAuthCheck();

        try {
            WeeklyShortUrlAccessBean bean = convertWeeklyShortUrlAccessStubToBean(weeklyShortUrlAccess);
            String guid = ServiceManager.getWeeklyShortUrlAccessService().createWeeklyShortUrlAccess(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createWeeklyShortUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        doAuthCheck();

        try {
            WeeklyShortUrlAccessBean bean = convertFormParamsToBean(formParams);
            String guid = ServiceManager.getWeeklyShortUrlAccessService().createWeeklyShortUrlAccess(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            return Response.created(createdUri).entity(guid).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    // TBD
    @Override
    public Response refreshWeeklyShortUrlAccess(String guid, WeeklyShortUrlAccessStub weeklyShortUrlAccess) throws BaseResourceException
    {
        doAuthCheck();

        try {
            if(weeklyShortUrlAccess == null || !guid.equals(weeklyShortUrlAccess.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from weeklyShortUrlAccess guid = " + weeklyShortUrlAccess.getGuid());
                throw new RequestForbiddenException("Failed to refresh the weeklyShortUrlAccess with guid = " + guid);
            }
            WeeklyShortUrlAccessBean bean = convertWeeklyShortUrlAccessStubToBean(weeklyShortUrlAccess);
            bean = (WeeklyShortUrlAccessBean) ServiceManager.getWeeklyShortUrlAccessService().refreshWeeklyShortUrlAccess(bean);
            if(bean == null) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the weeklyShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to refresh the weeklyShortUrlAccess with guid = " + guid);
            }
            weeklyShortUrlAccess = WeeklyShortUrlAccessStub.convertBeanToStub(bean);
            return Response.ok(weeklyShortUrlAccess).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateWeeklyShortUrlAccess(String guid, WeeklyShortUrlAccessStub weeklyShortUrlAccess) throws BaseResourceException
    {
        doAuthCheck();

        try {
            if(weeklyShortUrlAccess == null || !guid.equals(weeklyShortUrlAccess.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from weeklyShortUrlAccess guid = " + weeklyShortUrlAccess.getGuid());
                throw new RequestForbiddenException("Failed to update the weeklyShortUrlAccess with guid = " + guid);
            }
            WeeklyShortUrlAccessBean bean = convertWeeklyShortUrlAccessStubToBean(weeklyShortUrlAccess);
            boolean suc = ServiceManager.getWeeklyShortUrlAccessService().updateWeeklyShortUrlAccess(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the weeklyShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the weeklyShortUrlAccess with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateWeeklyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week)
    {
        doAuthCheck();

        try {
            /*
            boolean suc = ServiceManager.getWeeklyShortUrlAccessService().updateWeeklyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the weeklyShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the weeklyShortUrlAccess with guid = " + guid);
            }
            return Response.noContent().build();
            */
            throw new NotImplementedException("This method has not been implemented yet.");
        //} catch(BadRequestException ex) {
        //    throw new BadRequestRsException(ex, resourceUri);
        //} catch(ResourceNotFoundException ex) {
        //    throw new ResourceNotFoundRsException(ex, resourceUri);
        //} catch(ResourceGoneException ex) {
        //    throw new ResourceGoneRsException(ex, resourceUri);
        //} catch(RequestForbiddenException ex) {
        //    throw new RequestForbiddenRsException(ex, resourceUri);
        //} catch(RequestConflictException ex) {
        //    throw new RequestConflictRsException(ex, resourceUri);
        //} catch(ServiceUnavailableException ex) {
        //    throw new ServiceUnavailableRsException(ex, resourceUri);
        //} catch(InternalServerErrorException ex) {
        //    throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(NotImplementedException ex) {
            throw new NotImplementedRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response updateWeeklyShortUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        doAuthCheck();

        try {
            WeeklyShortUrlAccessBean bean = convertFormParamsToBean(formParams);
            boolean suc = ServiceManager.getWeeklyShortUrlAccessService().updateWeeklyShortUrlAccess(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the weeklyShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the weeklyShortUrlAccess with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteWeeklyShortUrlAccess(String guid) throws BaseResourceException
    {
        doAuthCheck();

        try {
            boolean suc = ServiceManager.getWeeklyShortUrlAccessService().deleteWeeklyShortUrlAccess(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the weeklyShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the weeklyShortUrlAccess with guid = " + guid);
            }
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        doAuthCheck();

        try {
            Long count = ServiceManager.getWeeklyShortUrlAccessService().deleteWeeklyShortUrlAccesses(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    @Override
    public Response createWeeklyShortUrlAccesses(WeeklyShortUrlAccessListStub weeklyShortUrlAccesses) throws BaseResourceException
    {
        doAuthCheck();

        try {
            List<WeeklyShortUrlAccessStub> stubs = weeklyShortUrlAccesses.getList();
            List<WeeklyShortUrlAccess> beans = new ArrayList<WeeklyShortUrlAccess>();
            for(WeeklyShortUrlAccessStub stub : stubs) {
                WeeklyShortUrlAccessBean bean = convertWeeklyShortUrlAccessStubToBean(stub);
                beans.add(bean);
            }
            Integer count = ServiceManager.getWeeklyShortUrlAccessService().createWeeklyShortUrlAccesses(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


    public static WeeklyShortUrlAccessBean convertWeeklyShortUrlAccessStubToBean(WeeklyShortUrlAccess stub)
    {
        WeeklyShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new WeeklyShortUrlAccessBean();
            bean.setGuid(stub.getGuid());
            bean.setTallyTime(stub.getTallyTime());
            bean.setTallyEpoch(stub.getTallyEpoch());
            bean.setCount(stub.getCount());
            bean.setShortUrl(stub.getShortUrl());
            bean.setShortUrlDomain(stub.getShortUrlDomain());
            bean.setLongUrl(stub.getLongUrl());
            bean.setLongUrlDomain(stub.getLongUrlDomain());
            bean.setRedirectType(stub.getRedirectType());
            bean.setRefererDomain(stub.getRefererDomain());
            bean.setUserAgent(stub.getUserAgent());
            bean.setLanguage(stub.getLanguage());
            bean.setCountry(stub.getCountry());
            bean.setTalliedTime(stub.getTalliedTime());
            bean.setYear(stub.getYear());
            bean.setWeek(stub.getWeek());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

    public static List<WeeklyShortUrlAccessBean> convertWeeklyShortUrlAccessListStubToBeanList(WeeklyShortUrlAccessListStub listStub)
    {
        if(listStub == null) {
            log.log(Level.INFO, "listStub is null. Null list is returned.");
            return null;
        } else {
            List<WeeklyShortUrlAccessStub> stubList = listStub.getList();
            if(stubList == null) {
                log.log(Level.INFO, "Stub list is null. Null list is returned.");
                return null;                
            }
            List<WeeklyShortUrlAccessBean> beanList = new ArrayList<WeeklyShortUrlAccessBean>();
            if(stubList.isEmpty()) {
                log.log(Level.INFO, "Stub list is empty. Empty list is returned.");
            } else {
                for(WeeklyShortUrlAccessStub stub : stubList) {
                    WeeklyShortUrlAccessBean bean = convertWeeklyShortUrlAccessStubToBean(stub);
                    beanList.add(bean);                            
                }
            }
            return beanList;
        }
    }


    // TBD
    // This needs to be implemented before url-encoded form create/update can be supported 
    public static WeeklyShortUrlAccessBean convertFormParamsToBean(MultivaluedMap<String, String> formParams)
    {
        WeeklyShortUrlAccessBean bean = new WeeklyShortUrlAccessBean();
        if(formParams == null) {
            log.log(Level.INFO, "FormParams is null. Empty bean is returned.");
        } else {
            Iterator<MultivaluedMap.Entry<String,List<String>>> it = formParams.entrySet().iterator();
            while(it.hasNext()) {
                MultivaluedMap.Entry<String,List<String>> m =(MultivaluedMap.Entry<String,List<String>>) it.next();
                String key = (String) m.getKey();
                String val = null;
                List<String> list = m.getValue();
                if(list != null && list.size() > 0) {
                    val = list.get(0);
                    if(key.equals("guid")) {
                        bean.setGuid(val);                        
                    } else if(key.equals("tallyTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setTallyTime(v);
                    } else if(key.equals("tallyEpoch")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setTallyEpoch(v);
                    } else if(key.equals("count")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setCount(v);
                    } else if(key.equals("shortUrl")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setShortUrl(v);
                    } else if(key.equals("shortUrlDomain")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setShortUrlDomain(v);
                    } else if(key.equals("longUrl")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setLongUrl(v);
                    } else if(key.equals("longUrlDomain")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setLongUrlDomain(v);
                    } else if(key.equals("redirectType")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setRedirectType(v);
                    } else if(key.equals("refererDomain")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setRefererDomain(v);
                    } else if(key.equals("userAgent")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setUserAgent(v);
                    } else if(key.equals("language")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setLanguage(v);
                    } else if(key.equals("country")) {
                        // TBD: Needs to use "parse()" methods.
                        //String v = (String) val;
                        //bean.setCountry(v);
                    } else if(key.equals("talliedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setTalliedTime(v);
                    } else if(key.equals("year")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setYear(v);
                    } else if(key.equals("week")) {
                        // TBD: Needs to use "parse()" methods.
                        //Integer v = (Integer) val;
                        //bean.setWeek(v);
                    } else if(key.equals("createdTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setCreatedTime(v);
                    } else if(key.equals("modifiedTime")) {
                        // TBD: Needs to use "parse()" methods.
                        //Long v = (Long) val;
                        //bean.setModifiedTime(v);
                    }
                }
            }
        }
        return bean;
    }

}
