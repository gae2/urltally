package com.urltally.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.ws.service.DailyShortUrlAccessService;
import com.urltally.af.proxy.DailyShortUrlAccessServiceProxy;


// MockDailyShortUrlAccessServiceProxy is a decorator.
// It can be used as a base class to mock DailyShortUrlAccessServiceProxy objects.
public abstract class MockDailyShortUrlAccessServiceProxy implements DailyShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(MockDailyShortUrlAccessServiceProxy.class.getName());

    // MockDailyShortUrlAccessServiceProxy uses the decorator design pattern.
    private DailyShortUrlAccessServiceProxy decoratedProxy;

    public MockDailyShortUrlAccessServiceProxy(DailyShortUrlAccessServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected DailyShortUrlAccessServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(DailyShortUrlAccessServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public DailyShortUrlAccess getDailyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.getDailyShortUrlAccess(guid);
    }

    @Override
    public Object getDailyShortUrlAccess(String guid, String field) throws BaseException
    {
        return decoratedProxy.getDailyShortUrlAccess(guid, field);       
    }

    @Override
    public List<DailyShortUrlAccess> getDailyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getDailyShortUrlAccesses(guids);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses() throws BaseException
    {
        return getAllDailyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllDailyShortUrlAccesses(ordering, offset, count);
        return getAllDailyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllDailyShortUrlAccessKeys(ordering, offset, count);
        return getAllDailyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDailyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        return decoratedProxy.createDailyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
    }

    @Override
    public String createDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        return decoratedProxy.createDailyShortUrlAccess(dailyShortUrlAccess);
    }

    @Override
    public Boolean updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        return decoratedProxy.updateDailyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
    }

    @Override
    public Boolean updateDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        return decoratedProxy.updateDailyShortUrlAccess(dailyShortUrlAccess);
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.deleteDailyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        String guid = dailyShortUrlAccess.getGuid();
        return deleteDailyShortUrlAccess(guid);
    }

    @Override
    public Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteDailyShortUrlAccesses(filter, params, values);
    }

}
