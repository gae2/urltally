package com.urltally.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CommonConstants;
import com.urltally.ws.CellLatitudeLongitude;
import com.urltally.ws.stub.CellLatitudeLongitudeStub;
import com.urltally.af.bean.CellLatitudeLongitudeBean;


public class CellLatitudeLongitudeResourceUtil
{
    private static final Logger log = Logger.getLogger(CellLatitudeLongitudeResourceUtil.class.getName());

    // Static methods only.
    private CellLatitudeLongitudeResourceUtil() {}

    public static CellLatitudeLongitudeBean convertCellLatitudeLongitudeStubToBean(CellLatitudeLongitude stub)
    {
        CellLatitudeLongitudeBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new CellLatitudeLongitudeBean();
            bean.setScale(stub.getScale());
            bean.setLatitude(stub.getLatitude());
            bean.setLongitude(stub.getLongitude());
        }
        return bean;
    }

}
