package com.urltally.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ingressdb.af.bean.AccessRecordBean;
import com.ingressdb.af.service.AccessRecordService;
import com.ingressdb.rf.proxy.ServiceProxyFactory;
import com.ingressdb.rw.service.AccessRecordWebService;
import com.ingressdb.ws.AccessRecord;
import com.urltally.app.service.UserAppService;


public class IngressDBAccessHelper
{
    private static final Logger log = Logger.getLogger(IngressDBAccessHelper.class.getName());

    private IngressDBAccessHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private AccessRecordWebService beanAppService = null;
    private AccessRecordService accessRecordService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private AccessRecordService getAccessRecordRemoteService()
    {
        if(accessRecordService == null) {
            accessRecordService = ServiceProxyFactory.getInstance().getAccessRecordServiceProxy();
        }
        return accessRecordService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class IngressDBAccessHelperHolder
    {
        private static final IngressDBAccessHelper INSTANCE = new IngressDBAccessHelper();
    }

    // Singleton method
    public static IngressDBAccessHelper getInstance()
    {
        return IngressDBAccessHelperHolder.INSTANCE;
    }

    
    public AccessRecordBean getAccessRecord(String guid) 
    {
        AccessRecordBean bean = null;
        try {
            bean = (AccessRecordBean) getAccessRecordRemoteService().getAccessRecord(guid);
        } catch (com.ingressdb.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    
    
    
    /// TBD
    
    
    public List<String> getAccessRecordKeys(Long startTime, Long endTime, Integer maxCount) 
    {
        List<String> keys = null;
        
        String filter = "";
        if(startTime != null) {
            filter += "firstAccessTime >= " + startTime;
        }
        if(endTime != null) {
            if(!filter.isEmpty()) {
                filter += " && ";
            }
            filter += "firstAccessTime < " + endTime;
        }
        String ordering = "firstAccessTime asc";
        //String grouping = "requestUrl";
        //String grouping = "customField1";  // For CannyUrlApp, customField1 stores shortUrl.
        String grouping = null;   // GAE does not support GROUP BY... !!!!
        //Boolean unique = Boolean.TRUE;
        Boolean unique = null;   // ???
        try {
            keys = getAccessRecordRemoteService().findAccessRecordKeys(filter, ordering, null, null, grouping, unique, 0L, maxCount);
            if(maxCount != null && maxCount > 0 && keys != null) {
                int count = keys.size();
                if(count == maxCount) {
                    // We couldn't retrieve all records. What to do???? TBD....
                    log.warning("Did not retrieve all records. count = " + count);
                }
            }
        } catch (com.ingressdb.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the AccessRecord keys", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error while retrieving the AccessRecord keys", e);
        }
        
        return keys;
    }

    
    // TBD: Need to return "unique" records....
    public List<AccessRecord> getAccessRecords(Long startTime, Long endTime, Long offset, Integer maxCount) 
    {
        List<AccessRecord> beans = null;

        String filter = "";
        if(startTime != null) {
            filter += "firstAccessTime >= " + startTime;
        }
        if(endTime != null) {
            if(!filter.isEmpty()) {
                filter += " && ";
            }
            filter += "firstAccessTime < " + endTime;
        }
        String ordering = "firstAccessTime asc";
        //String grouping = "requestUrl";
        //String grouping = "customField1";  // For CannyUrlApp, customField1 stores shortUrl.
        String grouping = null;   // GAE does not support GROUP BY... !!!!
        //Boolean unique = Boolean.TRUE;  // This does not seem to work for some reason????
        Boolean unique = null;   // ???
        // TBD: ... unique!!!
        try {
            beans = getAccessRecordRemoteService().findAccessRecords(filter, ordering, null, null, grouping, unique, offset, maxCount);
            if(maxCount != null && maxCount > 0 && beans != null) {
                int count = beans.size();
                if(count == maxCount) {
                    // We couldn't retrieve all records. What to do???? TBD....
                    log.warning("Did not retrieve all records. count = " + count);
                    // TBD: ...
                }
            }
        } catch (com.ingressdb.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the AccessRecord list", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error while retrieving the AccessRecord list", e);
        }
        return beans;
    }

 
    
    public List<AccessRecord> getAccessRecords(String shortUrl, Long startTime, Long endTime, Integer maxCount) 
    {
        log.finer("BEGIN: getAccessRecords()");
        //log.warning("BEGIN: getAccessRecords()");

        List<AccessRecord> beans = null;

        String filter = "";
        if(shortUrl != null) {
            //shortUrl += "requestUrl == '" + shortUrl + "'";   // ...
            filter += "customField1 == '" + shortUrl + "'";   // For CannyUrlApp, customField1 stores shortUrl.
        } else {
            // temporary
            log.warning("getAccessRecords(): shortUrl is NULL!");
            //throw new RuntimeException("getAccessRecords(): shortUrl is NULL!");
        }
        if(startTime != null) {
            if(!filter.isEmpty()) {
                filter += " && ";
            }
            filter += "firstAccessTime >= " + startTime;
        }
        if(endTime != null) {
            if(!filter.isEmpty()) {
                filter += " && ";
            }
            filter += "firstAccessTime < " + endTime;
        }
        String ordering = "firstAccessTime asc";
        try {
            beans = getAccessRecordRemoteService().findAccessRecords(filter, ordering, null, null, null, null, 0L, maxCount);
            if(maxCount != null && maxCount > 0 && beans != null) {
                int count = beans.size();
                if(count == maxCount) {
                    // We couldn't retrieve all records. What to do???? TBD....
                    log.warning("Did not retrieve all records. count = " + count);
                }
            }
        } catch (com.ingressdb.ws.BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the beans for the given shortUrl = " + shortUrl, e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unknown error while retrieving the beans for the given shortUrl = " + shortUrl, e);
        }

        log.finer("END: getAccessRecords()");
        return beans;
    }


}
