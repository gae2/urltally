package com.urltally.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.TotalShortUrlAccessService;


// The primary purpose of TotalShortUrlAccessDummyService is to fake the service api, TotalShortUrlAccessService.
// It has no real implementation.
public class TotalShortUrlAccessDummyService implements TotalShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessDummyService.class.getName());

    public TotalShortUrlAccessDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // TotalShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TotalShortUrlAccess getTotalShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTotalShortUrlAccess(): guid = " + guid);
        return null;
    }

    @Override
    public Object getTotalShortUrlAccess(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTotalShortUrlAccess(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<TotalShortUrlAccess> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getTotalShortUrlAccesses()");
        return null;
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses() throws BaseException
    {
        return getAllTotalShortUrlAccesses(null, null, null);
    }


    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessDummyService.findTotalShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessDummyService.findTotalShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        log.finer("createTotalShortUrlAccess()");
        return null;
    }

    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("createTotalShortUrlAccess()");
        return null;
    }

    @Override
    public TotalShortUrlAccess constructTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("constructTotalShortUrlAccess()");
        return null;
    }

    @Override
    public Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        log.finer("updateTotalShortUrlAccess()");
        return null;
    }
        
    @Override
    public Boolean updateTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("updateTotalShortUrlAccess()");
        return null;
    }

    @Override
    public TotalShortUrlAccess refreshTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("refreshTotalShortUrlAccess()");
        return null;
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTotalShortUrlAccess(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("deleteTotalShortUrlAccess()");
        return null;
    }

    // TBD
    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteTotalShortUrlAccess(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException
    {
        log.finer("createTotalShortUrlAccesses()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException
    //{
    //}

}
