package com.urltally.app.tally;

import java.util.logging.Logger;

import com.urltally.ws.BaseException;


// Not being used.
// To be deleted...
public class ShortUrlAccessProcessor
{
    private static final Logger log = Logger.getLogger(ShortUrlAccessProcessor.class.getName());   

    private ShortUrlAccessProcessor()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class ShortUrlAccessProcessorHolder
    {
        private static final ShortUrlAccessProcessor INSTANCE = new ShortUrlAccessProcessor();
    }

    // Singleton method
    public static ShortUrlAccessProcessor getInstance()
    {
        return ShortUrlAccessProcessorHolder.INSTANCE;
    }

    private void init()
    {
        // TBD: ...
    }


//    public ShortUrlAccess sendShortUrlAccess(ShortUrlAccess shortUrlAccess) throws BaseException
//    {
//        log.finer("sendShortUrlAccess() called with shortUrlAccess = " + shortUrlAccess);
//        if(shortUrlAccess == null) {
//            // ????
//            throw new BaseException("shortUrlAccess is null.");
//            //log.warning("shortUrlAccess is null.");
//            //return null;   // ???
//        }
//
//        // Publish the message
//        // TBD ...
//
//
//
//        
//        return shortUrlAccess;
//    }
    

}
