package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.WeeklyShortUrlAccess;
// import com.urltally.ws.bean.WeeklyShortUrlAccessBean;
import com.urltally.ws.service.WeeklyShortUrlAccessService;
import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.af.proxy.WeeklyShortUrlAccessServiceProxy;


public class LocalWeeklyShortUrlAccessServiceProxy extends BaseLocalServiceProxy implements WeeklyShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalWeeklyShortUrlAccessServiceProxy.class.getName());

    public LocalWeeklyShortUrlAccessServiceProxy()
    {
    }

    @Override
    public WeeklyShortUrlAccess getWeeklyShortUrlAccess(String guid) throws BaseException
    {
        WeeklyShortUrlAccess serverBean = getWeeklyShortUrlAccessService().getWeeklyShortUrlAccess(guid);
        WeeklyShortUrlAccess appBean = convertServerWeeklyShortUrlAccessBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getWeeklyShortUrlAccess(String guid, String field) throws BaseException
    {
        return getWeeklyShortUrlAccessService().getWeeklyShortUrlAccess(guid, field);       
    }

    @Override
    public List<WeeklyShortUrlAccess> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException
    {
        List<WeeklyShortUrlAccess> serverBeanList = getWeeklyShortUrlAccessService().getWeeklyShortUrlAccesses(guids);
        List<WeeklyShortUrlAccess> appBeanList = convertServerWeeklyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses() throws BaseException
    {
        return getAllWeeklyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getWeeklyShortUrlAccessService().getAllWeeklyShortUrlAccesses(ordering, offset, count);
        return getAllWeeklyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<WeeklyShortUrlAccess> serverBeanList = getWeeklyShortUrlAccessService().getAllWeeklyShortUrlAccesses(ordering, offset, count, forwardCursor);
        List<WeeklyShortUrlAccess> appBeanList = convertServerWeeklyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getWeeklyShortUrlAccessService().getAllWeeklyShortUrlAccessKeys(ordering, offset, count);
        return getAllWeeklyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getWeeklyShortUrlAccessService().getAllWeeklyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getWeeklyShortUrlAccessService().findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<WeeklyShortUrlAccess> serverBeanList = getWeeklyShortUrlAccessService().findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<WeeklyShortUrlAccess> appBeanList = convertServerWeeklyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getWeeklyShortUrlAccessService().findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getWeeklyShortUrlAccessService().findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getWeeklyShortUrlAccessService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createWeeklyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {
        return getWeeklyShortUrlAccessService().createWeeklyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
    }

    @Override
    public String createWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.WeeklyShortUrlAccessBean serverBean =  convertAppWeeklyShortUrlAccessBeanToServerBean(weeklyShortUrlAccess);
        return getWeeklyShortUrlAccessService().createWeeklyShortUrlAccess(serverBean);
    }

    @Override
    public Boolean updateWeeklyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {
        return getWeeklyShortUrlAccessService().updateWeeklyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
    }

    @Override
    public Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.WeeklyShortUrlAccessBean serverBean =  convertAppWeeklyShortUrlAccessBeanToServerBean(weeklyShortUrlAccess);
        return getWeeklyShortUrlAccessService().updateWeeklyShortUrlAccess(serverBean);
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException
    {
        return getWeeklyShortUrlAccessService().deleteWeeklyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.WeeklyShortUrlAccessBean serverBean =  convertAppWeeklyShortUrlAccessBeanToServerBean(weeklyShortUrlAccess);
        return getWeeklyShortUrlAccessService().deleteWeeklyShortUrlAccess(serverBean);
    }

    @Override
    public Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return getWeeklyShortUrlAccessService().deleteWeeklyShortUrlAccesses(filter, params, values);
    }




    public static WeeklyShortUrlAccessBean convertServerWeeklyShortUrlAccessBeanToAppBean(WeeklyShortUrlAccess serverBean)
    {
        WeeklyShortUrlAccessBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new WeeklyShortUrlAccessBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setCount(serverBean.getCount());
            bean.setShortUrl(serverBean.getShortUrl());
            bean.setShortUrlDomain(serverBean.getShortUrlDomain());
            bean.setLongUrl(serverBean.getLongUrl());
            bean.setLongUrlDomain(serverBean.getLongUrlDomain());
            bean.setRedirectType(serverBean.getRedirectType());
            bean.setRefererDomain(serverBean.getRefererDomain());
            bean.setUserAgent(serverBean.getUserAgent());
            bean.setLanguage(serverBean.getLanguage());
            bean.setCountry(serverBean.getCountry());
            bean.setTalliedTime(serverBean.getTalliedTime());
            bean.setYear(serverBean.getYear());
            bean.setWeek(serverBean.getWeek());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<WeeklyShortUrlAccess> convertServerWeeklyShortUrlAccessBeanListToAppBeanList(List<WeeklyShortUrlAccess> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<WeeklyShortUrlAccess> beanList = new ArrayList<WeeklyShortUrlAccess>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(WeeklyShortUrlAccess sb : serverBeanList) {
                WeeklyShortUrlAccessBean bean = convertServerWeeklyShortUrlAccessBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.WeeklyShortUrlAccessBean convertAppWeeklyShortUrlAccessBeanToServerBean(WeeklyShortUrlAccess appBean)
    {
        com.urltally.ws.bean.WeeklyShortUrlAccessBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.WeeklyShortUrlAccessBean();
            bean.setGuid(appBean.getGuid());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setCount(appBean.getCount());
            bean.setShortUrl(appBean.getShortUrl());
            bean.setShortUrlDomain(appBean.getShortUrlDomain());
            bean.setLongUrl(appBean.getLongUrl());
            bean.setLongUrlDomain(appBean.getLongUrlDomain());
            bean.setRedirectType(appBean.getRedirectType());
            bean.setRefererDomain(appBean.getRefererDomain());
            bean.setUserAgent(appBean.getUserAgent());
            bean.setLanguage(appBean.getLanguage());
            bean.setCountry(appBean.getCountry());
            bean.setTalliedTime(appBean.getTalliedTime());
            bean.setYear(appBean.getYear());
            bean.setWeek(appBean.getWeek());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<WeeklyShortUrlAccess> convertAppWeeklyShortUrlAccessBeanListToServerBeanList(List<WeeklyShortUrlAccess> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<WeeklyShortUrlAccess> beanList = new ArrayList<WeeklyShortUrlAccess>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(WeeklyShortUrlAccess sb : appBeanList) {
                com.urltally.ws.bean.WeeklyShortUrlAccessBean bean = convertAppWeeklyShortUrlAccessBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
