package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.DailyShortUrlAccess;
// import com.urltally.ws.bean.DailyShortUrlAccessBean;
import com.urltally.ws.service.DailyShortUrlAccessService;
import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.af.proxy.DailyShortUrlAccessServiceProxy;


public class LocalDailyShortUrlAccessServiceProxy extends BaseLocalServiceProxy implements DailyShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalDailyShortUrlAccessServiceProxy.class.getName());

    public LocalDailyShortUrlAccessServiceProxy()
    {
    }

    @Override
    public DailyShortUrlAccess getDailyShortUrlAccess(String guid) throws BaseException
    {
        DailyShortUrlAccess serverBean = getDailyShortUrlAccessService().getDailyShortUrlAccess(guid);
        DailyShortUrlAccess appBean = convertServerDailyShortUrlAccessBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getDailyShortUrlAccess(String guid, String field) throws BaseException
    {
        return getDailyShortUrlAccessService().getDailyShortUrlAccess(guid, field);       
    }

    @Override
    public List<DailyShortUrlAccess> getDailyShortUrlAccesses(List<String> guids) throws BaseException
    {
        List<DailyShortUrlAccess> serverBeanList = getDailyShortUrlAccessService().getDailyShortUrlAccesses(guids);
        List<DailyShortUrlAccess> appBeanList = convertServerDailyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses() throws BaseException
    {
        return getAllDailyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDailyShortUrlAccessService().getAllDailyShortUrlAccesses(ordering, offset, count);
        return getAllDailyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<DailyShortUrlAccess> serverBeanList = getDailyShortUrlAccessService().getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
        List<DailyShortUrlAccess> appBeanList = convertServerDailyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDailyShortUrlAccessService().getAllDailyShortUrlAccessKeys(ordering, offset, count);
        return getAllDailyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDailyShortUrlAccessService().getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDailyShortUrlAccessService().findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<DailyShortUrlAccess> serverBeanList = getDailyShortUrlAccessService().findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<DailyShortUrlAccess> appBeanList = convertServerDailyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDailyShortUrlAccessService().findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDailyShortUrlAccessService().findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getDailyShortUrlAccessService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDailyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        return getDailyShortUrlAccessService().createDailyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
    }

    @Override
    public String createDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.DailyShortUrlAccessBean serverBean =  convertAppDailyShortUrlAccessBeanToServerBean(dailyShortUrlAccess);
        return getDailyShortUrlAccessService().createDailyShortUrlAccess(serverBean);
    }

    @Override
    public Boolean updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        return getDailyShortUrlAccessService().updateDailyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
    }

    @Override
    public Boolean updateDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.DailyShortUrlAccessBean serverBean =  convertAppDailyShortUrlAccessBeanToServerBean(dailyShortUrlAccess);
        return getDailyShortUrlAccessService().updateDailyShortUrlAccess(serverBean);
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(String guid) throws BaseException
    {
        return getDailyShortUrlAccessService().deleteDailyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.DailyShortUrlAccessBean serverBean =  convertAppDailyShortUrlAccessBeanToServerBean(dailyShortUrlAccess);
        return getDailyShortUrlAccessService().deleteDailyShortUrlAccess(serverBean);
    }

    @Override
    public Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return getDailyShortUrlAccessService().deleteDailyShortUrlAccesses(filter, params, values);
    }




    public static DailyShortUrlAccessBean convertServerDailyShortUrlAccessBeanToAppBean(DailyShortUrlAccess serverBean)
    {
        DailyShortUrlAccessBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new DailyShortUrlAccessBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setCount(serverBean.getCount());
            bean.setShortUrl(serverBean.getShortUrl());
            bean.setShortUrlDomain(serverBean.getShortUrlDomain());
            bean.setLongUrl(serverBean.getLongUrl());
            bean.setLongUrlDomain(serverBean.getLongUrlDomain());
            bean.setRedirectType(serverBean.getRedirectType());
            bean.setRefererDomain(serverBean.getRefererDomain());
            bean.setUserAgent(serverBean.getUserAgent());
            bean.setLanguage(serverBean.getLanguage());
            bean.setCountry(serverBean.getCountry());
            bean.setTalliedTime(serverBean.getTalliedTime());
            bean.setYear(serverBean.getYear());
            bean.setDay(serverBean.getDay());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<DailyShortUrlAccess> convertServerDailyShortUrlAccessBeanListToAppBeanList(List<DailyShortUrlAccess> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<DailyShortUrlAccess> beanList = new ArrayList<DailyShortUrlAccess>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(DailyShortUrlAccess sb : serverBeanList) {
                DailyShortUrlAccessBean bean = convertServerDailyShortUrlAccessBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.DailyShortUrlAccessBean convertAppDailyShortUrlAccessBeanToServerBean(DailyShortUrlAccess appBean)
    {
        com.urltally.ws.bean.DailyShortUrlAccessBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.DailyShortUrlAccessBean();
            bean.setGuid(appBean.getGuid());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setCount(appBean.getCount());
            bean.setShortUrl(appBean.getShortUrl());
            bean.setShortUrlDomain(appBean.getShortUrlDomain());
            bean.setLongUrl(appBean.getLongUrl());
            bean.setLongUrlDomain(appBean.getLongUrlDomain());
            bean.setRedirectType(appBean.getRedirectType());
            bean.setRefererDomain(appBean.getRefererDomain());
            bean.setUserAgent(appBean.getUserAgent());
            bean.setLanguage(appBean.getLanguage());
            bean.setCountry(appBean.getCountry());
            bean.setTalliedTime(appBean.getTalliedTime());
            bean.setYear(appBean.getYear());
            bean.setDay(appBean.getDay());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<DailyShortUrlAccess> convertAppDailyShortUrlAccessBeanListToServerBeanList(List<DailyShortUrlAccess> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<DailyShortUrlAccess> beanList = new ArrayList<DailyShortUrlAccess>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(DailyShortUrlAccess sb : appBeanList) {
                com.urltally.ws.bean.DailyShortUrlAccessBean bean = convertAppDailyShortUrlAccessBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
