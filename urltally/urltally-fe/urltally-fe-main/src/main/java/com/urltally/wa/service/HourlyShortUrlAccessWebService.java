package com.urltally.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.service.manager.ServiceManager;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.HourlyShortUrlAccessJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class HourlyShortUrlAccessWebService // implements HourlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(HourlyShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private HourlyShortUrlAccessService mService = null;

    public HourlyShortUrlAccessWebService()
    {
        this(ServiceManager.getHourlyShortUrlAccessService());
    }
    public HourlyShortUrlAccessWebService(HourlyShortUrlAccessService service)
    {
        mService = service;
    }
    
    private HourlyShortUrlAccessService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getHourlyShortUrlAccessService();
        }
        return mService;
    }
    
    
    public HourlyShortUrlAccessJsBean getHourlyShortUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = getService().getHourlyShortUrlAccess(guid);
            HourlyShortUrlAccessJsBean bean = convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getHourlyShortUrlAccess(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getHourlyShortUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<HourlyShortUrlAccessJsBean> getHourlyShortUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<HourlyShortUrlAccessJsBean> jsBeans = new ArrayList<HourlyShortUrlAccessJsBean>();
            List<HourlyShortUrlAccess> hourlyShortUrlAccesses = getService().getHourlyShortUrlAccesses(guids);
            if(hourlyShortUrlAccesses != null) {
                for(HourlyShortUrlAccess hourlyShortUrlAccess : hourlyShortUrlAccesses) {
                    jsBeans.add(convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<HourlyShortUrlAccessJsBean> getAllHourlyShortUrlAccesses() throws WebException
    {
        return getAllHourlyShortUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<HourlyShortUrlAccessJsBean> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllHourlyShortUrlAccesses(ordering, offset, count, null);
    }

    public List<HourlyShortUrlAccessJsBean> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<HourlyShortUrlAccessJsBean> jsBeans = new ArrayList<HourlyShortUrlAccessJsBean>();
            List<HourlyShortUrlAccess> hourlyShortUrlAccesses = getService().getAllHourlyShortUrlAccesses(ordering, offset, count, forwardCursor);
            if(hourlyShortUrlAccesses != null) {
                for(HourlyShortUrlAccess hourlyShortUrlAccess : hourlyShortUrlAccesses) {
                    jsBeans.add(convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllHourlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllHourlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<HourlyShortUrlAccessJsBean> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<HourlyShortUrlAccessJsBean> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<HourlyShortUrlAccessJsBean> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<HourlyShortUrlAccessJsBean> jsBeans = new ArrayList<HourlyShortUrlAccessJsBean>();
            List<HourlyShortUrlAccess> hourlyShortUrlAccesses = getService().findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(hourlyShortUrlAccesses != null) {
                for(HourlyShortUrlAccess hourlyShortUrlAccess : hourlyShortUrlAccesses) {
                    jsBeans.add(convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createHourlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws WebException
    {
        try {
            return getService().createHourlyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createHourlyShortUrlAccess(String jsonStr) throws WebException
    {
        return createHourlyShortUrlAccess(HourlyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public String createHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            return getService().createHourlyShortUrlAccess(hourlyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public HourlyShortUrlAccessJsBean constructHourlyShortUrlAccess(String jsonStr) throws WebException
    {
        return constructHourlyShortUrlAccess(HourlyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public HourlyShortUrlAccessJsBean constructHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            hourlyShortUrlAccess = getService().constructHourlyShortUrlAccess(hourlyShortUrlAccess);
            jsBean = convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws WebException
    {
        try {
            return getService().updateHourlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateHourlyShortUrlAccess(String jsonStr) throws WebException
    {
        return updateHourlyShortUrlAccess(HourlyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            return getService().updateHourlyShortUrlAccess(hourlyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public HourlyShortUrlAccessJsBean refreshHourlyShortUrlAccess(String jsonStr) throws WebException
    {
        return refreshHourlyShortUrlAccess(HourlyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public HourlyShortUrlAccessJsBean refreshHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            hourlyShortUrlAccess = getService().refreshHourlyShortUrlAccess(hourlyShortUrlAccess);
            jsBean = convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteHourlyShortUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteHourlyShortUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            return getService().deleteHourlyShortUrlAccess(hourlyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteHourlyShortUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static HourlyShortUrlAccessJsBean convertHourlyShortUrlAccessToJsBean(HourlyShortUrlAccess hourlyShortUrlAccess)
    {
        HourlyShortUrlAccessJsBean jsBean = null;
        if(hourlyShortUrlAccess != null) {
            jsBean = new HourlyShortUrlAccessJsBean();
            jsBean.setGuid(hourlyShortUrlAccess.getGuid());
            jsBean.setTallyTime(hourlyShortUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(hourlyShortUrlAccess.getTallyEpoch());
            jsBean.setCount(hourlyShortUrlAccess.getCount());
            jsBean.setShortUrl(hourlyShortUrlAccess.getShortUrl());
            jsBean.setShortUrlDomain(hourlyShortUrlAccess.getShortUrlDomain());
            jsBean.setLongUrl(hourlyShortUrlAccess.getLongUrl());
            jsBean.setLongUrlDomain(hourlyShortUrlAccess.getLongUrlDomain());
            jsBean.setRedirectType(hourlyShortUrlAccess.getRedirectType());
            jsBean.setRefererDomain(hourlyShortUrlAccess.getRefererDomain());
            jsBean.setUserAgent(hourlyShortUrlAccess.getUserAgent());
            jsBean.setLanguage(hourlyShortUrlAccess.getLanguage());
            jsBean.setCountry(hourlyShortUrlAccess.getCountry());
            jsBean.setTalliedTime(hourlyShortUrlAccess.getTalliedTime());
            jsBean.setYear(hourlyShortUrlAccess.getYear());
            jsBean.setDay(hourlyShortUrlAccess.getDay());
            jsBean.setHour(hourlyShortUrlAccess.getHour());
            jsBean.setCreatedTime(hourlyShortUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(hourlyShortUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static HourlyShortUrlAccess convertHourlyShortUrlAccessJsBeanToBean(HourlyShortUrlAccessJsBean jsBean)
    {
        HourlyShortUrlAccessBean hourlyShortUrlAccess = null;
        if(jsBean != null) {
            hourlyShortUrlAccess = new HourlyShortUrlAccessBean();
            hourlyShortUrlAccess.setGuid(jsBean.getGuid());
            hourlyShortUrlAccess.setTallyTime(jsBean.getTallyTime());
            hourlyShortUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            hourlyShortUrlAccess.setCount(jsBean.getCount());
            hourlyShortUrlAccess.setShortUrl(jsBean.getShortUrl());
            hourlyShortUrlAccess.setShortUrlDomain(jsBean.getShortUrlDomain());
            hourlyShortUrlAccess.setLongUrl(jsBean.getLongUrl());
            hourlyShortUrlAccess.setLongUrlDomain(jsBean.getLongUrlDomain());
            hourlyShortUrlAccess.setRedirectType(jsBean.getRedirectType());
            hourlyShortUrlAccess.setRefererDomain(jsBean.getRefererDomain());
            hourlyShortUrlAccess.setUserAgent(jsBean.getUserAgent());
            hourlyShortUrlAccess.setLanguage(jsBean.getLanguage());
            hourlyShortUrlAccess.setCountry(jsBean.getCountry());
            hourlyShortUrlAccess.setTalliedTime(jsBean.getTalliedTime());
            hourlyShortUrlAccess.setYear(jsBean.getYear());
            hourlyShortUrlAccess.setDay(jsBean.getDay());
            hourlyShortUrlAccess.setHour(jsBean.getHour());
            hourlyShortUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            hourlyShortUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return hourlyShortUrlAccess;
    }

}
