package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.ws.stub.WeeklyShortUrlAccessStub;
import com.urltally.ws.stub.WeeklyShortUrlAccessListStub;
import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.af.resource.WeeklyShortUrlAccessResource;


// MockWeeklyShortUrlAccessResource is a decorator.
// It can be used as a base class to mock WeeklyShortUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/weeklyShortUrlAccesses/")
public abstract class MockWeeklyShortUrlAccessResource implements WeeklyShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockWeeklyShortUrlAccessResource.class.getName());

    // MockWeeklyShortUrlAccessResource uses the decorator design pattern.
    private WeeklyShortUrlAccessResource decoratedResource;

    public MockWeeklyShortUrlAccessResource(WeeklyShortUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected WeeklyShortUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(WeeklyShortUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllWeeklyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllWeeklyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findWeeklyShortUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findWeeklyShortUrlAccessesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getWeeklyShortUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getWeeklyShortUrlAccessAsHtml(guid);
//    }

    @Override
    public Response getWeeklyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getWeeklyShortUrlAccess(guid);
    }

    @Override
    public Response getWeeklyShortUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getWeeklyShortUrlAccessAsJsonp(guid, callback);
    }

    @Override
    public Response getWeeklyShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getWeeklyShortUrlAccess(guid, field);
    }

    // TBD
    @Override
    public Response constructWeeklyShortUrlAccess(WeeklyShortUrlAccessStub weeklyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.constructWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }

    @Override
    public Response createWeeklyShortUrlAccess(WeeklyShortUrlAccessStub weeklyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }

//    @Override
//    public Response createWeeklyShortUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createWeeklyShortUrlAccess(formParams);
//    }

    // TBD
    @Override
    public Response refreshWeeklyShortUrlAccess(String guid, WeeklyShortUrlAccessStub weeklyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.refreshWeeklyShortUrlAccess(guid, weeklyShortUrlAccess);
    }

    @Override
    public Response updateWeeklyShortUrlAccess(String guid, WeeklyShortUrlAccessStub weeklyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateWeeklyShortUrlAccess(guid, weeklyShortUrlAccess);
    }

    @Override
    public Response updateWeeklyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week)
    {
        return decoratedResource.updateWeeklyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
    }

//    @Override
//    public Response updateWeeklyShortUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateWeeklyShortUrlAccess(guid, formParams);
//    }

    @Override
    public Response deleteWeeklyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteWeeklyShortUrlAccess(guid);
    }

    @Override
    public Response deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteWeeklyShortUrlAccesses(filter, params, values);
    }


// TBD ....
    @Override
    public Response createWeeklyShortUrlAccesses(WeeklyShortUrlAccessListStub weeklyShortUrlAccesses) throws BaseResourceException
    {
        return decoratedResource.createWeeklyShortUrlAccesses(weeklyShortUrlAccesses);
    }


}
