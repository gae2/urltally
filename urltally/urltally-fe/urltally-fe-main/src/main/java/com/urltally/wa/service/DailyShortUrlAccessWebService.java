package com.urltally.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.service.manager.ServiceManager;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.DailyShortUrlAccessJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DailyShortUrlAccessWebService // implements DailyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(DailyShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private DailyShortUrlAccessService mService = null;

    public DailyShortUrlAccessWebService()
    {
        this(ServiceManager.getDailyShortUrlAccessService());
    }
    public DailyShortUrlAccessWebService(DailyShortUrlAccessService service)
    {
        mService = service;
    }
    
    private DailyShortUrlAccessService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getDailyShortUrlAccessService();
        }
        return mService;
    }
    
    
    public DailyShortUrlAccessJsBean getDailyShortUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            DailyShortUrlAccess dailyShortUrlAccess = getService().getDailyShortUrlAccess(guid);
            DailyShortUrlAccessJsBean bean = convertDailyShortUrlAccessToJsBean(dailyShortUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getDailyShortUrlAccess(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getDailyShortUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DailyShortUrlAccessJsBean> getDailyShortUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<DailyShortUrlAccessJsBean> jsBeans = new ArrayList<DailyShortUrlAccessJsBean>();
            List<DailyShortUrlAccess> dailyShortUrlAccesses = getService().getDailyShortUrlAccesses(guids);
            if(dailyShortUrlAccesses != null) {
                for(DailyShortUrlAccess dailyShortUrlAccess : dailyShortUrlAccesses) {
                    jsBeans.add(convertDailyShortUrlAccessToJsBean(dailyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<DailyShortUrlAccessJsBean> getAllDailyShortUrlAccesses() throws WebException
    {
        return getAllDailyShortUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<DailyShortUrlAccessJsBean> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDailyShortUrlAccesses(ordering, offset, count, null);
    }

    public List<DailyShortUrlAccessJsBean> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<DailyShortUrlAccessJsBean> jsBeans = new ArrayList<DailyShortUrlAccessJsBean>();
            List<DailyShortUrlAccess> dailyShortUrlAccesses = getService().getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
            if(dailyShortUrlAccesses != null) {
                for(DailyShortUrlAccess dailyShortUrlAccess : dailyShortUrlAccesses) {
                    jsBeans.add(convertDailyShortUrlAccessToJsBean(dailyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllDailyShortUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<DailyShortUrlAccessJsBean> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<DailyShortUrlAccessJsBean> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<DailyShortUrlAccessJsBean> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<DailyShortUrlAccessJsBean> jsBeans = new ArrayList<DailyShortUrlAccessJsBean>();
            List<DailyShortUrlAccess> dailyShortUrlAccesses = getService().findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(dailyShortUrlAccesses != null) {
                for(DailyShortUrlAccess dailyShortUrlAccess : dailyShortUrlAccesses) {
                    jsBeans.add(convertDailyShortUrlAccessToJsBean(dailyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDailyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws WebException
    {
        try {
            return getService().createDailyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createDailyShortUrlAccess(String jsonStr) throws WebException
    {
        return createDailyShortUrlAccess(DailyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public String createDailyShortUrlAccess(DailyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DailyShortUrlAccess dailyShortUrlAccess = convertDailyShortUrlAccessJsBeanToBean(jsBean);
            return getService().createDailyShortUrlAccess(dailyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DailyShortUrlAccessJsBean constructDailyShortUrlAccess(String jsonStr) throws WebException
    {
        return constructDailyShortUrlAccess(DailyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public DailyShortUrlAccessJsBean constructDailyShortUrlAccess(DailyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DailyShortUrlAccess dailyShortUrlAccess = convertDailyShortUrlAccessJsBeanToBean(jsBean);
            dailyShortUrlAccess = getService().constructDailyShortUrlAccess(dailyShortUrlAccess);
            jsBean = convertDailyShortUrlAccessToJsBean(dailyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws WebException
    {
        try {
            return getService().updateDailyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateDailyShortUrlAccess(String jsonStr) throws WebException
    {
        return updateDailyShortUrlAccess(DailyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateDailyShortUrlAccess(DailyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DailyShortUrlAccess dailyShortUrlAccess = convertDailyShortUrlAccessJsBeanToBean(jsBean);
            return getService().updateDailyShortUrlAccess(dailyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public DailyShortUrlAccessJsBean refreshDailyShortUrlAccess(String jsonStr) throws WebException
    {
        return refreshDailyShortUrlAccess(DailyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public DailyShortUrlAccessJsBean refreshDailyShortUrlAccess(DailyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DailyShortUrlAccess dailyShortUrlAccess = convertDailyShortUrlAccessJsBeanToBean(jsBean);
            dailyShortUrlAccess = getService().refreshDailyShortUrlAccess(dailyShortUrlAccess);
            jsBean = convertDailyShortUrlAccessToJsBean(dailyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDailyShortUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteDailyShortUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            DailyShortUrlAccess dailyShortUrlAccess = convertDailyShortUrlAccessJsBeanToBean(jsBean);
            return getService().deleteDailyShortUrlAccess(dailyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteDailyShortUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static DailyShortUrlAccessJsBean convertDailyShortUrlAccessToJsBean(DailyShortUrlAccess dailyShortUrlAccess)
    {
        DailyShortUrlAccessJsBean jsBean = null;
        if(dailyShortUrlAccess != null) {
            jsBean = new DailyShortUrlAccessJsBean();
            jsBean.setGuid(dailyShortUrlAccess.getGuid());
            jsBean.setTallyTime(dailyShortUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(dailyShortUrlAccess.getTallyEpoch());
            jsBean.setCount(dailyShortUrlAccess.getCount());
            jsBean.setShortUrl(dailyShortUrlAccess.getShortUrl());
            jsBean.setShortUrlDomain(dailyShortUrlAccess.getShortUrlDomain());
            jsBean.setLongUrl(dailyShortUrlAccess.getLongUrl());
            jsBean.setLongUrlDomain(dailyShortUrlAccess.getLongUrlDomain());
            jsBean.setRedirectType(dailyShortUrlAccess.getRedirectType());
            jsBean.setRefererDomain(dailyShortUrlAccess.getRefererDomain());
            jsBean.setUserAgent(dailyShortUrlAccess.getUserAgent());
            jsBean.setLanguage(dailyShortUrlAccess.getLanguage());
            jsBean.setCountry(dailyShortUrlAccess.getCountry());
            jsBean.setTalliedTime(dailyShortUrlAccess.getTalliedTime());
            jsBean.setYear(dailyShortUrlAccess.getYear());
            jsBean.setDay(dailyShortUrlAccess.getDay());
            jsBean.setCreatedTime(dailyShortUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(dailyShortUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static DailyShortUrlAccess convertDailyShortUrlAccessJsBeanToBean(DailyShortUrlAccessJsBean jsBean)
    {
        DailyShortUrlAccessBean dailyShortUrlAccess = null;
        if(jsBean != null) {
            dailyShortUrlAccess = new DailyShortUrlAccessBean();
            dailyShortUrlAccess.setGuid(jsBean.getGuid());
            dailyShortUrlAccess.setTallyTime(jsBean.getTallyTime());
            dailyShortUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            dailyShortUrlAccess.setCount(jsBean.getCount());
            dailyShortUrlAccess.setShortUrl(jsBean.getShortUrl());
            dailyShortUrlAccess.setShortUrlDomain(jsBean.getShortUrlDomain());
            dailyShortUrlAccess.setLongUrl(jsBean.getLongUrl());
            dailyShortUrlAccess.setLongUrlDomain(jsBean.getLongUrlDomain());
            dailyShortUrlAccess.setRedirectType(jsBean.getRedirectType());
            dailyShortUrlAccess.setRefererDomain(jsBean.getRefererDomain());
            dailyShortUrlAccess.setUserAgent(jsBean.getUserAgent());
            dailyShortUrlAccess.setLanguage(jsBean.getLanguage());
            dailyShortUrlAccess.setCountry(jsBean.getCountry());
            dailyShortUrlAccess.setTalliedTime(jsBean.getTalliedTime());
            dailyShortUrlAccess.setYear(jsBean.getYear());
            dailyShortUrlAccess.setDay(jsBean.getDay());
            dailyShortUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            dailyShortUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return dailyShortUrlAccess;
    }

}
