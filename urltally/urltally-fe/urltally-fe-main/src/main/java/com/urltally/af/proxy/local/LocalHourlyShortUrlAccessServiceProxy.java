package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.HourlyShortUrlAccess;
// import com.urltally.ws.bean.HourlyShortUrlAccessBean;
import com.urltally.ws.service.HourlyShortUrlAccessService;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.proxy.HourlyShortUrlAccessServiceProxy;


public class LocalHourlyShortUrlAccessServiceProxy extends BaseLocalServiceProxy implements HourlyShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalHourlyShortUrlAccessServiceProxy.class.getName());

    public LocalHourlyShortUrlAccessServiceProxy()
    {
    }

    @Override
    public HourlyShortUrlAccess getHourlyShortUrlAccess(String guid) throws BaseException
    {
        HourlyShortUrlAccess serverBean = getHourlyShortUrlAccessService().getHourlyShortUrlAccess(guid);
        HourlyShortUrlAccess appBean = convertServerHourlyShortUrlAccessBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getHourlyShortUrlAccess(String guid, String field) throws BaseException
    {
        return getHourlyShortUrlAccessService().getHourlyShortUrlAccess(guid, field);       
    }

    @Override
    public List<HourlyShortUrlAccess> getHourlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        List<HourlyShortUrlAccess> serverBeanList = getHourlyShortUrlAccessService().getHourlyShortUrlAccesses(guids);
        List<HourlyShortUrlAccess> appBeanList = convertServerHourlyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses() throws BaseException
    {
        return getAllHourlyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getHourlyShortUrlAccessService().getAllHourlyShortUrlAccesses(ordering, offset, count);
        return getAllHourlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<HourlyShortUrlAccess> serverBeanList = getHourlyShortUrlAccessService().getAllHourlyShortUrlAccesses(ordering, offset, count, forwardCursor);
        List<HourlyShortUrlAccess> appBeanList = convertServerHourlyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getHourlyShortUrlAccessService().getAllHourlyShortUrlAccessKeys(ordering, offset, count);
        return getAllHourlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getHourlyShortUrlAccessService().getAllHourlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getHourlyShortUrlAccessService().findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<HourlyShortUrlAccess> serverBeanList = getHourlyShortUrlAccessService().findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<HourlyShortUrlAccess> appBeanList = convertServerHourlyShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getHourlyShortUrlAccessService().findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getHourlyShortUrlAccessService().findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getHourlyShortUrlAccessService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createHourlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {
        return getHourlyShortUrlAccessService().createHourlyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
    }

    @Override
    public String createHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.HourlyShortUrlAccessBean serverBean =  convertAppHourlyShortUrlAccessBeanToServerBean(hourlyShortUrlAccess);
        return getHourlyShortUrlAccessService().createHourlyShortUrlAccess(serverBean);
    }

    @Override
    public Boolean updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {
        return getHourlyShortUrlAccessService().updateHourlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
    }

    @Override
    public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.HourlyShortUrlAccessBean serverBean =  convertAppHourlyShortUrlAccessBeanToServerBean(hourlyShortUrlAccess);
        return getHourlyShortUrlAccessService().updateHourlyShortUrlAccess(serverBean);
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException
    {
        return getHourlyShortUrlAccessService().deleteHourlyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.HourlyShortUrlAccessBean serverBean =  convertAppHourlyShortUrlAccessBeanToServerBean(hourlyShortUrlAccess);
        return getHourlyShortUrlAccessService().deleteHourlyShortUrlAccess(serverBean);
    }

    @Override
    public Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return getHourlyShortUrlAccessService().deleteHourlyShortUrlAccesses(filter, params, values);
    }




    public static HourlyShortUrlAccessBean convertServerHourlyShortUrlAccessBeanToAppBean(HourlyShortUrlAccess serverBean)
    {
        HourlyShortUrlAccessBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new HourlyShortUrlAccessBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setCount(serverBean.getCount());
            bean.setShortUrl(serverBean.getShortUrl());
            bean.setShortUrlDomain(serverBean.getShortUrlDomain());
            bean.setLongUrl(serverBean.getLongUrl());
            bean.setLongUrlDomain(serverBean.getLongUrlDomain());
            bean.setRedirectType(serverBean.getRedirectType());
            bean.setRefererDomain(serverBean.getRefererDomain());
            bean.setUserAgent(serverBean.getUserAgent());
            bean.setLanguage(serverBean.getLanguage());
            bean.setCountry(serverBean.getCountry());
            bean.setTalliedTime(serverBean.getTalliedTime());
            bean.setYear(serverBean.getYear());
            bean.setDay(serverBean.getDay());
            bean.setHour(serverBean.getHour());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<HourlyShortUrlAccess> convertServerHourlyShortUrlAccessBeanListToAppBeanList(List<HourlyShortUrlAccess> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<HourlyShortUrlAccess> beanList = new ArrayList<HourlyShortUrlAccess>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(HourlyShortUrlAccess sb : serverBeanList) {
                HourlyShortUrlAccessBean bean = convertServerHourlyShortUrlAccessBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.HourlyShortUrlAccessBean convertAppHourlyShortUrlAccessBeanToServerBean(HourlyShortUrlAccess appBean)
    {
        com.urltally.ws.bean.HourlyShortUrlAccessBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.HourlyShortUrlAccessBean();
            bean.setGuid(appBean.getGuid());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setCount(appBean.getCount());
            bean.setShortUrl(appBean.getShortUrl());
            bean.setShortUrlDomain(appBean.getShortUrlDomain());
            bean.setLongUrl(appBean.getLongUrl());
            bean.setLongUrlDomain(appBean.getLongUrlDomain());
            bean.setRedirectType(appBean.getRedirectType());
            bean.setRefererDomain(appBean.getRefererDomain());
            bean.setUserAgent(appBean.getUserAgent());
            bean.setLanguage(appBean.getLanguage());
            bean.setCountry(appBean.getCountry());
            bean.setTalliedTime(appBean.getTalliedTime());
            bean.setYear(appBean.getYear());
            bean.setDay(appBean.getDay());
            bean.setHour(appBean.getHour());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<HourlyShortUrlAccess> convertAppHourlyShortUrlAccessBeanListToServerBeanList(List<HourlyShortUrlAccess> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<HourlyShortUrlAccess> beanList = new ArrayList<HourlyShortUrlAccess>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(HourlyShortUrlAccess sb : appBeanList) {
                com.urltally.ws.bean.HourlyShortUrlAccessBean bean = convertAppHourlyShortUrlAccessBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
