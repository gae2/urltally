package com.urltally.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import com.urltally.common.TallyType;


// Note: All time periods (day, week, month, etc.) are [begin, end) (e.g., begin is included, but end is excluded)...
public class TallyTimeUtil
{
    private static final Logger log = Logger.getLogger(TallyTimeUtil.class.getName());

    private TallyTimeUtil() {}


    // UTC based.... for now... ???
    // TBD: Make it timezone dependent ????
    // Howt to handle daylight saving time????
    private static final String TZNAME_USPACIFIC = "US/Pacific";   // ??
    private static final TimeZone TIMEZONE_USPACIFIC;
    static {
        TIMEZONE_USPACIFIC = TimeZone.getTimeZone(TZNAME_USPACIFIC);
    }
    

    // ????
    private static Pattern sPattern;
    static {
        sPattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}");
    }
    
    // temporary
    // Need testing...
    public static boolean isValid(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            return false;
        }
        return sPattern.matcher(dayHour).matches();
    }
   
    // TBD...
    //public static boolean isValid(String tallyType, String dayHour)
    // ...
    // ...
    
    
    public static String getTallyTime(String tallyType)
    {
        if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            return getDayHour();
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            return getDay();
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            return getWeek();
        } else if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            return getMonth();
        } else {
            return null;   // ????
        }
    }

    public static String getTallyTime(String tallyType, long time)
    {
        if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            return getDayHour(time);
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            return getDay(time);
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            return getWeek(time);
        } else if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            return getMonth(time);
        } else {
            return null;   // ????
        }
    }

    
    public static String getMonth()
    {
        return getMonth(System.currentTimeMillis());
    }
    public static String getMonth(long time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-01 00:00");
        //dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static String getWeek()
    {
        return getWeek(System.currentTimeMillis());
    }
    public static String getWeek(long time)
    {
        long t = 0L;
        int day = getDayOfWeek(time);
        t = time - day * 3600 * 24 * 1000L;
        return getDay(t);
    }
    // temporary
    private static int getDayOfWeek(long time)
    {
        SimpleDateFormat df = new SimpleDateFormat("E");
        //df.setTimeZone(TIMEZONE_USPACIFIC);
        String day = df.format(new Date(time));
        log.finer("getDayOfWeek(): day = " + day + "; time = " + time);

        int dayOfWeek = 0;
        if(day.startsWith("Su")) {
            dayOfWeek = 0;
        } else if(day.startsWith("Mo")) {
            dayOfWeek = 1;
        } else if(day.startsWith("Tu")) {
            dayOfWeek = 2;
        } else if(day.startsWith("We")) {
            dayOfWeek = 3;
        } else if(day.startsWith("Th")) {
            dayOfWeek = 4;
        } else if(day.startsWith("Fr")) {
            dayOfWeek = 5;
        } else if(day.startsWith("Sa")) {
            dayOfWeek = 6;
        }        
        return dayOfWeek;
    }

    public static String getDay()
    {
        return getDay(System.currentTimeMillis());
    }
    public static String getDay(long time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00");
        //dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    public static String getDayHour()
    {
        return getDayHour(System.currentTimeMillis());
    }
    public static String getDayHour(long time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00");
        //dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        String date = dateFormat.format(new Date(time));
        return date;
    }

    
    // ???
    public static String getMonthFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        String month = dayHour.substring(0, 7) + "-01 00:00";  // ???
        return month;
    }
    public static String getWeekFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        return getWeek(getMilli(dayHour));  // ????
    }
    public static String getDayFromDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return dayHour;  // ???
        }
        String day = dayHour.substring(0, 10) + " 00:00";  // ???
        return day;
    }
    
    
    public static String[] getRange(String tallyType, String startDayHour, String endDayHour)
    {
        if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            return getDayHourRange(startDayHour, endDayHour);
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            return getDayRange(startDayHour, endDayHour);
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            return getWeekRange(startDayHour, endDayHour);
        } else if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            return getMonthRange(startDayHour, endDayHour);
        } else {
            return null;   // ????
        }
    }

    public static String[] getMonthRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getMonth();
            log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextMonth(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }

    public static String[] getWeekRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getWeek();
            log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextWeek(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }
    
    public static String[] getDayRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getDay();
            log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextDay(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }
    
    public static String[] getDayHourRange(String startDayHour, String endDayHour)
    {
        if(! isValid(startDayHour)) {
            return null;
        }
        if(endDayHour == null || ! isValid(endDayHour)) {
            endDayHour = getDayHour();
            log.info("Invalid endDayHour: New value = " + endDayHour);
        }
        
        // temporary.
        // Not the most efficient algo.
        List<String> vals = new ArrayList<String>();
        String pointer = startDayHour;
        while(pointer.compareTo(endDayHour) <= 0) {
            vals.add(pointer);
            pointer = getNextDayHour(pointer);            
        }
        
        String[] range = vals.toArray(new String[0]);
        return range;
    }

    
    public static long getMilli(String day)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        //dateFormat.setTimeZone(TIMEZONE_USPACIFIC);
        long milli = 0L;
        try {
            Date date = dateFormat.parse(day);
            milli = date.getTime();
        } catch (ParseException e) {
            log.log(Level.WARNING, "Failed to parse the input string: day = " + day, e);
        }
        return milli;
    }
    
    public static long[] getMilliRange(String tallyType, String dayHour)
    {
        long[] range = new long[2];
        range[0] = getMilli(dayHour);
        range[1] = getMilli(getNext(tallyType, dayHour));
        return range;
    }

    
    
    // TBD:
    // Methods to convert an arbitrary date string into a valid date/time based on tallyType...
    // ...

    
    
    
    public static String getNext(String tallyType, String dayHour)
    {
        if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            return getNextDayHour(dayHour);
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            return getNextDay(dayHour);
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            return getNextWeek(dayHour);
        } else if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            return getNextMonth(dayHour);
        } else {
            return null;   // ????
        }
    }
    public static String getPrevious(String tallyType, String dayHour)
    {
        if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            return getPreviousDayHour(dayHour);
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            return getPreviousDay(dayHour);
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            return getPreviousWeek(dayHour);
        } else if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            return getPreviousMonth(dayHour);
        } else {
            return null;   // ????
        }
    }
    
    
    public static String getNextMonth(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        String currentMonth = getMonthFromDayHour(dayHour);
        int year = Integer.parseInt(currentMonth.substring(0, 4));
        int month = Integer.parseInt(currentMonth.substring(5, 7)) + 1;
        if(month > 12) {
            month = 1;
            year += 1;
        }
        String mo = "";
        if(month < 10) {
            mo = "0" + month;
        } else {
            mo = "" + month;
        }
        String nextMonth = year + "-" + mo + "-01 00:00"; 
        return nextMonth;
    }
    public static String getPreviousMonth(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        String currentMonth = getMonthFromDayHour(dayHour);
        int year = Integer.parseInt(currentMonth.substring(0, 4));
        int month = Integer.parseInt(currentMonth.substring(5, 7)) - 1;
        if(month < 1) {
            month = 12;
            year -= 1;
        }
        String mo = "";
        if(month < 10) {
            mo = "0" + month;
        } else {
            mo = "" + month;
        }
        String prevMonth = year + "-" + mo + "-01 00:00"; 
        return prevMonth;
    }
    
    public static String getNextWeek(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 7 * 3600 * 24 * 1000L;
        return getWeek(milli);
    }
    public static String getPreviousWeek(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 7 * 3600 * 24 * 1000L;
        return getWeek(milli);
    }
    
    public static String getNextDay(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 3600 * 24 * 1000L;
        return getDay(milli);
    }
    public static String getPreviousDay(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 3600 * 24 * 1000L;
        return getDay(milli);
    }
    
    public static String getNextDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) + 3600 * 1000L;
        return getDayHour(milli);
    }
    public static String getPreviousDayHour(String dayHour)
    {
        if(dayHour == null || dayHour.length() < 10) {
            log.warning("Invalid input: dayHour = " + dayHour);
            return null;  // ???
        }
        long milli = getMilli(dayHour) - 3600 * 1000L;
        return getDayHour(milli);
    }

    
    public static int compareMonths(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
    public static int compareWeeks(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    
    public static int compareDays(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    } 
    public static int compareHours(String dayL, String dayR)
    {
        return compare(dayL, dayR);
    }    

    
    // ???
    public static int compareHourAndDay(String hourL, String dayR)
    {
        String dayL = getDayFromDayHour(hourL);
        return compare(dayL, dayR);
    }
    public static int compareHourAndWeek(String hourL, String dayR)
    {
        String weekL = getWeekFromDayHour(hourL);
        return compare(weekL, dayR);
    }
    public static int compareHourAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }
    public static int compareDayAndWeek(String hourL, String dayR)
    {
        String weekL = getWeekFromDayHour(hourL);
        return compare(weekL, dayR);
    }
    public static int compareDayAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }
    public static int compareWeekAndMonth(String hourL, String dayR)
    {
        String monthL = getMonthFromDayHour(hourL);
        return compare(monthL, dayR);
    }

    
    public static int compareTime(String tallyType, long time, String dayR)
    {
        if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            return compareTimeAndHour(time, dayR);
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            return compareTimeAndDay(time, dayR);
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            return compareTimeAndWeek(time, dayR);
        } else if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            return compareTimeAndMonth(time, dayR);
        } else {
            return 0;   // ????
        }
    }

    public static int compareTimeAndHour(long time, String dayR)
    {
        String dayL = getDayHour(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndDay(long time, String dayR)
    {
        String dayL = getDay(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndWeek(long time, String dayR)
    {
        String dayL = getWeek(time);
        return compare(dayL, dayR);        
    }
    public static int compareTimeAndMonth(long time, String dayR)
    {
        String dayL = getMonth(time);
        return compare(dayL, dayR);        
    }
    

    // This has somewhat ambiguous semantics. Use the above, more specialized methods...
    private static int compare(String dayL, String dayR)
    {
        if(dayL == null && dayR == null) {
            return 0;
        }
        if(dayL == null) {
            return -1;
        }
        if(dayR == null) {
            return 1;
        }
        return dayL.compareTo(dayR);
    }
    
}
