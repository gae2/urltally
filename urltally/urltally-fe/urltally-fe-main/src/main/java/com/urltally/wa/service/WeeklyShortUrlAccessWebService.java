package com.urltally.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.af.service.manager.ServiceManager;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.WeeklyShortUrlAccessJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class WeeklyShortUrlAccessWebService // implements WeeklyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(WeeklyShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private WeeklyShortUrlAccessService mService = null;

    public WeeklyShortUrlAccessWebService()
    {
        this(ServiceManager.getWeeklyShortUrlAccessService());
    }
    public WeeklyShortUrlAccessWebService(WeeklyShortUrlAccessService service)
    {
        mService = service;
    }
    
    private WeeklyShortUrlAccessService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getWeeklyShortUrlAccessService();
        }
        return mService;
    }
    
    
    public WeeklyShortUrlAccessJsBean getWeeklyShortUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            WeeklyShortUrlAccess weeklyShortUrlAccess = getService().getWeeklyShortUrlAccess(guid);
            WeeklyShortUrlAccessJsBean bean = convertWeeklyShortUrlAccessToJsBean(weeklyShortUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getWeeklyShortUrlAccess(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getWeeklyShortUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<WeeklyShortUrlAccessJsBean> getWeeklyShortUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<WeeklyShortUrlAccessJsBean> jsBeans = new ArrayList<WeeklyShortUrlAccessJsBean>();
            List<WeeklyShortUrlAccess> weeklyShortUrlAccesses = getService().getWeeklyShortUrlAccesses(guids);
            if(weeklyShortUrlAccesses != null) {
                for(WeeklyShortUrlAccess weeklyShortUrlAccess : weeklyShortUrlAccesses) {
                    jsBeans.add(convertWeeklyShortUrlAccessToJsBean(weeklyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<WeeklyShortUrlAccessJsBean> getAllWeeklyShortUrlAccesses() throws WebException
    {
        return getAllWeeklyShortUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<WeeklyShortUrlAccessJsBean> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllWeeklyShortUrlAccesses(ordering, offset, count, null);
    }

    public List<WeeklyShortUrlAccessJsBean> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<WeeklyShortUrlAccessJsBean> jsBeans = new ArrayList<WeeklyShortUrlAccessJsBean>();
            List<WeeklyShortUrlAccess> weeklyShortUrlAccesses = getService().getAllWeeklyShortUrlAccesses(ordering, offset, count, forwardCursor);
            if(weeklyShortUrlAccesses != null) {
                for(WeeklyShortUrlAccess weeklyShortUrlAccess : weeklyShortUrlAccesses) {
                    jsBeans.add(convertWeeklyShortUrlAccessToJsBean(weeklyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllWeeklyShortUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllWeeklyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<WeeklyShortUrlAccessJsBean> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<WeeklyShortUrlAccessJsBean> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<WeeklyShortUrlAccessJsBean> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<WeeklyShortUrlAccessJsBean> jsBeans = new ArrayList<WeeklyShortUrlAccessJsBean>();
            List<WeeklyShortUrlAccess> weeklyShortUrlAccesses = getService().findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(weeklyShortUrlAccesses != null) {
                for(WeeklyShortUrlAccess weeklyShortUrlAccess : weeklyShortUrlAccesses) {
                    jsBeans.add(convertWeeklyShortUrlAccessToJsBean(weeklyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createWeeklyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws WebException
    {
        try {
            return getService().createWeeklyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createWeeklyShortUrlAccess(String jsonStr) throws WebException
    {
        return createWeeklyShortUrlAccess(WeeklyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public String createWeeklyShortUrlAccess(WeeklyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            WeeklyShortUrlAccess weeklyShortUrlAccess = convertWeeklyShortUrlAccessJsBeanToBean(jsBean);
            return getService().createWeeklyShortUrlAccess(weeklyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public WeeklyShortUrlAccessJsBean constructWeeklyShortUrlAccess(String jsonStr) throws WebException
    {
        return constructWeeklyShortUrlAccess(WeeklyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public WeeklyShortUrlAccessJsBean constructWeeklyShortUrlAccess(WeeklyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            WeeklyShortUrlAccess weeklyShortUrlAccess = convertWeeklyShortUrlAccessJsBeanToBean(jsBean);
            weeklyShortUrlAccess = getService().constructWeeklyShortUrlAccess(weeklyShortUrlAccess);
            jsBean = convertWeeklyShortUrlAccessToJsBean(weeklyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateWeeklyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws WebException
    {
        try {
            return getService().updateWeeklyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateWeeklyShortUrlAccess(String jsonStr) throws WebException
    {
        return updateWeeklyShortUrlAccess(WeeklyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            WeeklyShortUrlAccess weeklyShortUrlAccess = convertWeeklyShortUrlAccessJsBeanToBean(jsBean);
            return getService().updateWeeklyShortUrlAccess(weeklyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public WeeklyShortUrlAccessJsBean refreshWeeklyShortUrlAccess(String jsonStr) throws WebException
    {
        return refreshWeeklyShortUrlAccess(WeeklyShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public WeeklyShortUrlAccessJsBean refreshWeeklyShortUrlAccess(WeeklyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            WeeklyShortUrlAccess weeklyShortUrlAccess = convertWeeklyShortUrlAccessJsBeanToBean(jsBean);
            weeklyShortUrlAccess = getService().refreshWeeklyShortUrlAccess(weeklyShortUrlAccess);
            jsBean = convertWeeklyShortUrlAccessToJsBean(weeklyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteWeeklyShortUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteWeeklyShortUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            WeeklyShortUrlAccess weeklyShortUrlAccess = convertWeeklyShortUrlAccessJsBeanToBean(jsBean);
            return getService().deleteWeeklyShortUrlAccess(weeklyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteWeeklyShortUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static WeeklyShortUrlAccessJsBean convertWeeklyShortUrlAccessToJsBean(WeeklyShortUrlAccess weeklyShortUrlAccess)
    {
        WeeklyShortUrlAccessJsBean jsBean = null;
        if(weeklyShortUrlAccess != null) {
            jsBean = new WeeklyShortUrlAccessJsBean();
            jsBean.setGuid(weeklyShortUrlAccess.getGuid());
            jsBean.setTallyTime(weeklyShortUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(weeklyShortUrlAccess.getTallyEpoch());
            jsBean.setCount(weeklyShortUrlAccess.getCount());
            jsBean.setShortUrl(weeklyShortUrlAccess.getShortUrl());
            jsBean.setShortUrlDomain(weeklyShortUrlAccess.getShortUrlDomain());
            jsBean.setLongUrl(weeklyShortUrlAccess.getLongUrl());
            jsBean.setLongUrlDomain(weeklyShortUrlAccess.getLongUrlDomain());
            jsBean.setRedirectType(weeklyShortUrlAccess.getRedirectType());
            jsBean.setRefererDomain(weeklyShortUrlAccess.getRefererDomain());
            jsBean.setUserAgent(weeklyShortUrlAccess.getUserAgent());
            jsBean.setLanguage(weeklyShortUrlAccess.getLanguage());
            jsBean.setCountry(weeklyShortUrlAccess.getCountry());
            jsBean.setTalliedTime(weeklyShortUrlAccess.getTalliedTime());
            jsBean.setYear(weeklyShortUrlAccess.getYear());
            jsBean.setWeek(weeklyShortUrlAccess.getWeek());
            jsBean.setCreatedTime(weeklyShortUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(weeklyShortUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static WeeklyShortUrlAccess convertWeeklyShortUrlAccessJsBeanToBean(WeeklyShortUrlAccessJsBean jsBean)
    {
        WeeklyShortUrlAccessBean weeklyShortUrlAccess = null;
        if(jsBean != null) {
            weeklyShortUrlAccess = new WeeklyShortUrlAccessBean();
            weeklyShortUrlAccess.setGuid(jsBean.getGuid());
            weeklyShortUrlAccess.setTallyTime(jsBean.getTallyTime());
            weeklyShortUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            weeklyShortUrlAccess.setCount(jsBean.getCount());
            weeklyShortUrlAccess.setShortUrl(jsBean.getShortUrl());
            weeklyShortUrlAccess.setShortUrlDomain(jsBean.getShortUrlDomain());
            weeklyShortUrlAccess.setLongUrl(jsBean.getLongUrl());
            weeklyShortUrlAccess.setLongUrlDomain(jsBean.getLongUrlDomain());
            weeklyShortUrlAccess.setRedirectType(jsBean.getRedirectType());
            weeklyShortUrlAccess.setRefererDomain(jsBean.getRefererDomain());
            weeklyShortUrlAccess.setUserAgent(jsBean.getUserAgent());
            weeklyShortUrlAccess.setLanguage(jsBean.getLanguage());
            weeklyShortUrlAccess.setCountry(jsBean.getCountry());
            weeklyShortUrlAccess.setTalliedTime(jsBean.getTalliedTime());
            weeklyShortUrlAccess.setYear(jsBean.getYear());
            weeklyShortUrlAccess.setWeek(jsBean.getWeek());
            weeklyShortUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            weeklyShortUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return weeklyShortUrlAccess;
    }

}
