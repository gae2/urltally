package com.urltally.af.proxy;

public abstract class AbstractProxyFactory
{
    public abstract ApiConsumerServiceProxy getApiConsumerServiceProxy();
    public abstract UserServiceProxy getUserServiceProxy();
    public abstract AccessTallyMasterServiceProxy getAccessTallyMasterServiceProxy();
    public abstract AccessTallyStatusServiceProxy getAccessTallyStatusServiceProxy();
    public abstract MonthlyShortUrlAccessServiceProxy getMonthlyShortUrlAccessServiceProxy();
    public abstract WeeklyShortUrlAccessServiceProxy getWeeklyShortUrlAccessServiceProxy();
    public abstract DailyShortUrlAccessServiceProxy getDailyShortUrlAccessServiceProxy();
    public abstract HourlyShortUrlAccessServiceProxy getHourlyShortUrlAccessServiceProxy();
    public abstract CumulativeShortUrlAccessServiceProxy getCumulativeShortUrlAccessServiceProxy();
    public abstract CurrentShortUrlAccessServiceProxy getCurrentShortUrlAccessServiceProxy();
    public abstract TotalShortUrlAccessServiceProxy getTotalShortUrlAccessServiceProxy();
    public abstract TotalLongUrlAccessServiceProxy getTotalLongUrlAccessServiceProxy();
    public abstract ServiceInfoServiceProxy getServiceInfoServiceProxy();
    public abstract FiveTenServiceProxy getFiveTenServiceProxy();
}
