package com.urltally.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.TotalShortUrlAccessStub;
import com.urltally.ws.stub.TotalShortUrlAccessListStub;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.ws.service.TotalShortUrlAccessService;
import com.urltally.af.proxy.TotalShortUrlAccessServiceProxy;
import com.urltally.af.proxy.remote.RemoteTotalShortUrlAccessServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncTotalShortUrlAccessServiceProxy extends BaseAsyncServiceProxy implements TotalShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncTotalShortUrlAccessServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteTotalShortUrlAccessServiceProxy remoteProxy;

    public AsyncTotalShortUrlAccessServiceProxy()
    {
        remoteProxy = new RemoteTotalShortUrlAccessServiceProxy();
    }

    @Override
    public TotalShortUrlAccess getTotalShortUrlAccess(String guid) throws BaseException
    {
        return remoteProxy.getTotalShortUrlAccess(guid);
    }

    @Override
    public Object getTotalShortUrlAccess(String guid, String field) throws BaseException
    {
        return remoteProxy.getTotalShortUrlAccess(guid, field);       
    }

    @Override
    public List<TotalShortUrlAccess> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        return remoteProxy.getTotalShortUrlAccesses(guids);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses() throws BaseException
    {
        return getAllTotalShortUrlAccesses(null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllTotalShortUrlAccesses(ordering, offset, count);
        return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllTotalShortUrlAccessKeys(ordering, offset, count);
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        TotalShortUrlAccessBean bean = new TotalShortUrlAccessBean(null, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        return createTotalShortUrlAccess(bean);        
    }

    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        String guid = totalShortUrlAccess.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((TotalShortUrlAccessBean) totalShortUrlAccess).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateTotalShortUrlAccess-" + guid;
        String taskName = "RsCreateTotalShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        TotalShortUrlAccessStub stub = MarshalHelper.convertTotalShortUrlAccessToStub(totalShortUrlAccess);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(TotalShortUrlAccessStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = totalShortUrlAccess.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    TotalShortUrlAccessStub dummyStub = new TotalShortUrlAccessStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createTotalShortUrlAccess(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "totalShortUrlAccesses/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createTotalShortUrlAccess(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "totalShortUrlAccesses/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "TotalShortUrlAccess guid is invalid.");
        	throw new BaseException("TotalShortUrlAccess guid is invalid.");
        }
        TotalShortUrlAccessBean bean = new TotalShortUrlAccessBean(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        return updateTotalShortUrlAccess(bean);        
    }

    @Override
    public Boolean updateTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        String guid = totalShortUrlAccess.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "TotalShortUrlAccess object is invalid.");
        	throw new BaseException("TotalShortUrlAccess object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateTotalShortUrlAccess-" + guid;
        String taskName = "RsUpdateTotalShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        TotalShortUrlAccessStub stub = MarshalHelper.convertTotalShortUrlAccessToStub(totalShortUrlAccess);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(TotalShortUrlAccessStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = totalShortUrlAccess.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    TotalShortUrlAccessStub dummyStub = new TotalShortUrlAccessStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateTotalShortUrlAccess(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "totalShortUrlAccesses/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateTotalShortUrlAccess(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "totalShortUrlAccesses/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteTotalShortUrlAccess-" + guid;
        String taskName = "RsDeleteTotalShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "totalShortUrlAccesses/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        String guid = totalShortUrlAccess.getGuid();
        return deleteTotalShortUrlAccess(guid);
    }

    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteTotalShortUrlAccesses(filter, params, values);
    }

}
