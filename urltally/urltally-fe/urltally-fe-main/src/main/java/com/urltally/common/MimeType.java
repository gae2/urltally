package com.urltally.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

// TBD: Use enum??
// TBD: File extensions???
// Note: it's a "generalized" mime type...
public class MimeType
{
    private static final Logger log = Logger.getLogger(MimeType.class.getName());

    // Static methods only.
    private MimeType() {}

    // TBD
    public static final String ITEM_LABEL_TEXT = "Text Mail";
    public static final String ITEM_TYPE_TEXT = "TextMail";     // "text/plain" ????
    // image/gif, ...
    // etc.
    //...
    
    public static final List<String[]> TYPES = new ArrayList<String[]>();
    static {
        TYPES.add(new String[]{ITEM_LABEL_TEXT, ITEM_TYPE_TEXT});
        // ..
        // ...
    }

    
}
