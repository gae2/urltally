package com.urltally.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.urltally.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class HourlyShortUrlAccessJsBean extends ShortUrlAccessJsBean implements Serializable, Cloneable  //, HourlyShortUrlAccess
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(HourlyShortUrlAccessJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private Integer year;
    private Integer day;
    private Integer hour;

    // Ctors.
    public HourlyShortUrlAccessJsBean()
    {
        //this((String) null);
    }
    public HourlyShortUrlAccessJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public HourlyShortUrlAccessJsBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour)
    {
        this(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour, null, null);
    }
    public HourlyShortUrlAccessJsBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour, Long createdTime, Long modifiedTime)
    {
        super(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, createdTime, modifiedTime);

        this.year = year;
        this.day = day;
        this.hour = hour;
    }
    public HourlyShortUrlAccessJsBean(HourlyShortUrlAccessJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setTallyTime(bean.getTallyTime());
            setTallyEpoch(bean.getTallyEpoch());
            setCount(bean.getCount());
            setShortUrl(bean.getShortUrl());
            setShortUrlDomain(bean.getShortUrlDomain());
            setLongUrl(bean.getLongUrl());
            setLongUrlDomain(bean.getLongUrlDomain());
            setRedirectType(bean.getRedirectType());
            setRefererDomain(bean.getRefererDomain());
            setUserAgent(bean.getUserAgent());
            setLanguage(bean.getLanguage());
            setCountry(bean.getCountry());
            setTalliedTime(bean.getTalliedTime());
            setYear(bean.getYear());
            setDay(bean.getDay());
            setHour(bean.getHour());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static HourlyShortUrlAccessJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        HourlyShortUrlAccessJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(HourlyShortUrlAccessJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, HourlyShortUrlAccessJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getTallyTime()
    {
        return super.getTallyTime();
    }
    public void setTallyTime(String tallyTime)
    {
        super.setTallyTime(tallyTime);
    }

    public Long getTallyEpoch()
    {
        return super.getTallyEpoch();
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        super.setTallyEpoch(tallyEpoch);
    }

    public Integer getCount()
    {
        return super.getCount();
    }
    public void setCount(Integer count)
    {
        super.setCount(count);
    }

    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    public String getShortUrlDomain()
    {
        return super.getShortUrlDomain();
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        super.setShortUrlDomain(shortUrlDomain);
    }

    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    public String getLongUrlDomain()
    {
        return super.getLongUrlDomain();
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        super.setLongUrlDomain(longUrlDomain);
    }

    public String getRedirectType()
    {
        return super.getRedirectType();
    }
    public void setRedirectType(String redirectType)
    {
        super.setRedirectType(redirectType);
    }

    public String getRefererDomain()
    {
        return super.getRefererDomain();
    }
    public void setRefererDomain(String refererDomain)
    {
        super.setRefererDomain(refererDomain);
    }

    public String getUserAgent()
    {
        return super.getUserAgent();
    }
    public void setUserAgent(String userAgent)
    {
        super.setUserAgent(userAgent);
    }

    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    public String getCountry()
    {
        return super.getCountry();
    }
    public void setCountry(String country)
    {
        super.setCountry(country);
    }

    public Long getTalliedTime()
    {
        return super.getTalliedTime();
    }
    public void setTalliedTime(Long talliedTime)
    {
        super.setTalliedTime(talliedTime);
    }

    public Integer getYear()
    {
        return this.year;
    }
    public void setYear(Integer year)
    {
        this.year = year;
    }

    public Integer getDay()
    {
        return this.day;
    }
    public void setDay(Integer day)
    {
        this.day = day;
    }

    public Integer getHour()
    {
        return this.hour;
    }
    public void setHour(Integer hour)
    {
        this.hour = hour;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("tallyTime:null, ");
        sb.append("tallyEpoch:0, ");
        sb.append("count:0, ");
        sb.append("shortUrl:null, ");
        sb.append("shortUrlDomain:null, ");
        sb.append("longUrl:null, ");
        sb.append("longUrlDomain:null, ");
        sb.append("redirectType:null, ");
        sb.append("refererDomain:null, ");
        sb.append("userAgent:null, ");
        sb.append("language:null, ");
        sb.append("country:null, ");
        sb.append("talliedTime:0, ");
        sb.append("year:0, ");
        sb.append("day:0, ");
        sb.append("hour:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("tallyTime:");
        if(this.getTallyTime() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTallyTime()).append("\", ");
        }
        sb.append("tallyEpoch:" + this.getTallyEpoch()).append(", ");
        sb.append("count:" + this.getCount()).append(", ");
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("shortUrlDomain:");
        if(this.getShortUrlDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrlDomain()).append("\", ");
        }
        sb.append("longUrl:");
        if(this.getLongUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrl()).append("\", ");
        }
        sb.append("longUrlDomain:");
        if(this.getLongUrlDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLongUrlDomain()).append("\", ");
        }
        sb.append("redirectType:");
        if(this.getRedirectType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRedirectType()).append("\", ");
        }
        sb.append("refererDomain:");
        if(this.getRefererDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRefererDomain()).append("\", ");
        }
        sb.append("userAgent:");
        if(this.getUserAgent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUserAgent()).append("\", ");
        }
        sb.append("language:");
        if(this.getLanguage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLanguage()).append("\", ");
        }
        sb.append("country:");
        if(this.getCountry() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCountry()).append("\", ");
        }
        sb.append("talliedTime:" + this.getTalliedTime()).append(", ");
        sb.append("year:" + this.getYear()).append(", ");
        sb.append("day:" + this.getDay()).append(", ");
        sb.append("hour:" + this.getHour()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getTallyTime() != null) {
            sb.append("\"tallyTime\":").append("\"").append(this.getTallyTime()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyTime\":").append("null, ");
        }
        if(this.getTallyEpoch() != null) {
            sb.append("\"tallyEpoch\":").append("").append(this.getTallyEpoch()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyEpoch\":").append("null, ");
        }
        if(this.getCount() != null) {
            sb.append("\"count\":").append("").append(this.getCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"count\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        if(this.getShortUrlDomain() != null) {
            sb.append("\"shortUrlDomain\":").append("\"").append(this.getShortUrlDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrlDomain\":").append("null, ");
        }
        if(this.getLongUrl() != null) {
            sb.append("\"longUrl\":").append("\"").append(this.getLongUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrl\":").append("null, ");
        }
        if(this.getLongUrlDomain() != null) {
            sb.append("\"longUrlDomain\":").append("\"").append(this.getLongUrlDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"longUrlDomain\":").append("null, ");
        }
        if(this.getRedirectType() != null) {
            sb.append("\"redirectType\":").append("\"").append(this.getRedirectType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"redirectType\":").append("null, ");
        }
        if(this.getRefererDomain() != null) {
            sb.append("\"refererDomain\":").append("\"").append(this.getRefererDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refererDomain\":").append("null, ");
        }
        if(this.getUserAgent() != null) {
            sb.append("\"userAgent\":").append("\"").append(this.getUserAgent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAgent\":").append("null, ");
        }
        if(this.getLanguage() != null) {
            sb.append("\"language\":").append("\"").append(this.getLanguage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"language\":").append("null, ");
        }
        if(this.getCountry() != null) {
            sb.append("\"country\":").append("\"").append(this.getCountry()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"country\":").append("null, ");
        }
        if(this.getTalliedTime() != null) {
            sb.append("\"talliedTime\":").append("").append(this.getTalliedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"talliedTime\":").append("null, ");
        }
        if(this.getYear() != null) {
            sb.append("\"year\":").append("").append(this.getYear()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"year\":").append("null, ");
        }
        if(this.getDay() != null) {
            sb.append("\"day\":").append("").append(this.getDay()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"day\":").append("null, ");
        }
        if(this.getHour() != null) {
            sb.append("\"hour\":").append("").append(this.getHour()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"hour\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("year = " + this.year).append(";");
        sb.append("day = " + this.day).append(";");
        sb.append("hour = " + this.hour).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        HourlyShortUrlAccessJsBean cloned = new HourlyShortUrlAccessJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setTallyTime(this.getTallyTime());   
        cloned.setTallyEpoch(this.getTallyEpoch());   
        cloned.setCount(this.getCount());   
        cloned.setShortUrl(this.getShortUrl());   
        cloned.setShortUrlDomain(this.getShortUrlDomain());   
        cloned.setLongUrl(this.getLongUrl());   
        cloned.setLongUrlDomain(this.getLongUrlDomain());   
        cloned.setRedirectType(this.getRedirectType());   
        cloned.setRefererDomain(this.getRefererDomain());   
        cloned.setUserAgent(this.getUserAgent());   
        cloned.setLanguage(this.getLanguage());   
        cloned.setCountry(this.getCountry());   
        cloned.setTalliedTime(this.getTalliedTime());   
        cloned.setYear(this.getYear());   
        cloned.setDay(this.getDay());   
        cloned.setHour(this.getHour());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
