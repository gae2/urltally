package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.FiveTen;
import com.urltally.ws.stub.FiveTenStub;
import com.urltally.ws.stub.FiveTenListStub;
import com.urltally.af.bean.FiveTenBean;
import com.urltally.af.resource.FiveTenResource;


// MockFiveTenResource is a decorator.
// It can be used as a base class to mock FiveTenResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/fiveTens/")
public abstract class MockFiveTenResource implements FiveTenResource
{
    private static final Logger log = Logger.getLogger(MockFiveTenResource.class.getName());

    // MockFiveTenResource uses the decorator design pattern.
    private FiveTenResource decoratedResource;

    public MockFiveTenResource(FiveTenResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected FiveTenResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(FiveTenResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllFiveTens(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllFiveTens(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllFiveTenKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllFiveTenKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findFiveTenKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findFiveTenKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findFiveTens(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findFiveTens(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findFiveTensAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findFiveTensAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getFiveTenAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getFiveTenAsHtml(guid);
//    }

    @Override
    public Response getFiveTen(String guid) throws BaseResourceException
    {
        return decoratedResource.getFiveTen(guid);
    }

    @Override
    public Response getFiveTenAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getFiveTenAsJsonp(guid, callback);
    }

    @Override
    public Response getFiveTen(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getFiveTen(guid, field);
    }

    // TBD
    @Override
    public Response constructFiveTen(FiveTenStub fiveTen) throws BaseResourceException
    {
        return decoratedResource.constructFiveTen(fiveTen);
    }

    @Override
    public Response createFiveTen(FiveTenStub fiveTen) throws BaseResourceException
    {
        return decoratedResource.createFiveTen(fiveTen);
    }

//    @Override
//    public Response createFiveTen(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createFiveTen(formParams);
//    }

    // TBD
    @Override
    public Response refreshFiveTen(String guid, FiveTenStub fiveTen) throws BaseResourceException
    {
        return decoratedResource.refreshFiveTen(guid, fiveTen);
    }

    @Override
    public Response updateFiveTen(String guid, FiveTenStub fiveTen) throws BaseResourceException
    {
        return decoratedResource.updateFiveTen(guid, fiveTen);
    }

    @Override
    public Response updateFiveTen(String guid, Integer counter, String requesterIpAddress)
    {
        return decoratedResource.updateFiveTen(guid, counter, requesterIpAddress);
    }

//    @Override
//    public Response updateFiveTen(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateFiveTen(guid, formParams);
//    }

    @Override
    public Response deleteFiveTen(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteFiveTen(guid);
    }

    @Override
    public Response deleteFiveTens(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteFiveTens(filter, params, values);
    }


// TBD ....
    @Override
    public Response createFiveTens(FiveTenListStub fiveTens) throws BaseResourceException
    {
        return decoratedResource.createFiveTens(fiveTens);
    }


}
