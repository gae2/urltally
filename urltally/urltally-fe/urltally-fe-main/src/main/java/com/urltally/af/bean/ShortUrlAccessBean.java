package com.urltally.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.ShortUrlAccess;
import com.urltally.ws.stub.ShortUrlAccessStub;


// Wrapper class + bean combo.
public abstract class ShortUrlAccessBean implements ShortUrlAccess, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortUrlAccessBean.class.getName());

    // [1] With an embedded object.
    private ShortUrlAccessStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String tallyTime;
    private Long tallyEpoch;
    private Integer count;
    private String shortUrl;
    private String shortUrlDomain;
    private String longUrl;
    private String longUrlDomain;
    private String redirectType;
    private String refererDomain;
    private String userAgent;
    private String language;
    private String country;
    private Long talliedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public ShortUrlAccessBean()
    {
        //this((String) null);
    }
    public ShortUrlAccessBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ShortUrlAccessBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime)
    {
        this(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, null, null);
    }
    public ShortUrlAccessBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.count = count;
        this.shortUrl = shortUrl;
        this.shortUrlDomain = shortUrlDomain;
        this.longUrl = longUrl;
        this.longUrlDomain = longUrlDomain;
        this.redirectType = redirectType;
        this.refererDomain = refererDomain;
        this.userAgent = userAgent;
        this.language = language;
        this.country = country;
        this.talliedTime = talliedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public ShortUrlAccessBean(ShortUrlAccess stub)
    {
        if(stub instanceof ShortUrlAccessStub) {
            this.stub = (ShortUrlAccessStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setTallyTime(stub.getTallyTime());   
            setTallyEpoch(stub.getTallyEpoch());   
            setCount(stub.getCount());   
            setShortUrl(stub.getShortUrl());   
            setShortUrlDomain(stub.getShortUrlDomain());   
            setLongUrl(stub.getLongUrl());   
            setLongUrlDomain(stub.getLongUrlDomain());   
            setRedirectType(stub.getRedirectType());   
            setRefererDomain(stub.getRefererDomain());   
            setUserAgent(stub.getUserAgent());   
            setLanguage(stub.getLanguage());   
            setCountry(stub.getCountry());   
            setTalliedTime(stub.getTalliedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getTallyTime()
    {
        if(getStub() != null) {
            return getStub().getTallyTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyTime;
        }
    }
    public void setTallyTime(String tallyTime)
    {
        if(getStub() != null) {
            getStub().setTallyTime(tallyTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyTime = tallyTime;
        }
    }

    public Long getTallyEpoch()
    {
        if(getStub() != null) {
            return getStub().getTallyEpoch();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyEpoch;
        }
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        if(getStub() != null) {
            getStub().setTallyEpoch(tallyEpoch);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyEpoch = tallyEpoch;
        }
    }

    public Integer getCount()
    {
        if(getStub() != null) {
            return getStub().getCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.count;
        }
    }
    public void setCount(Integer count)
    {
        if(getStub() != null) {
            getStub().setCount(count);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.count = count;
        }
    }

    public String getShortUrl()
    {
        if(getStub() != null) {
            return getStub().getShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrl;
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getStub() != null) {
            getStub().setShortUrl(shortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrl = shortUrl;
        }
    }

    public String getShortUrlDomain()
    {
        if(getStub() != null) {
            return getStub().getShortUrlDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrlDomain;
        }
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        if(getStub() != null) {
            getStub().setShortUrlDomain(shortUrlDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrlDomain = shortUrlDomain;
        }
    }

    public String getLongUrl()
    {
        if(getStub() != null) {
            return getStub().getLongUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrl;
        }
    }
    public void setLongUrl(String longUrl)
    {
        if(getStub() != null) {
            getStub().setLongUrl(longUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrl = longUrl;
        }
    }

    public String getLongUrlDomain()
    {
        if(getStub() != null) {
            return getStub().getLongUrlDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longUrlDomain;
        }
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        if(getStub() != null) {
            getStub().setLongUrlDomain(longUrlDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longUrlDomain = longUrlDomain;
        }
    }

    public String getRedirectType()
    {
        if(getStub() != null) {
            return getStub().getRedirectType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.redirectType;
        }
    }
    public void setRedirectType(String redirectType)
    {
        if(getStub() != null) {
            getStub().setRedirectType(redirectType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.redirectType = redirectType;
        }
    }

    public String getRefererDomain()
    {
        if(getStub() != null) {
            return getStub().getRefererDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.refererDomain;
        }
    }
    public void setRefererDomain(String refererDomain)
    {
        if(getStub() != null) {
            getStub().setRefererDomain(refererDomain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.refererDomain = refererDomain;
        }
    }

    public String getUserAgent()
    {
        if(getStub() != null) {
            return getStub().getUserAgent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userAgent;
        }
    }
    public void setUserAgent(String userAgent)
    {
        if(getStub() != null) {
            getStub().setUserAgent(userAgent);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userAgent = userAgent;
        }
    }

    public String getLanguage()
    {
        if(getStub() != null) {
            return getStub().getLanguage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.language;
        }
    }
    public void setLanguage(String language)
    {
        if(getStub() != null) {
            getStub().setLanguage(language);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.language = language;
        }
    }

    public String getCountry()
    {
        if(getStub() != null) {
            return getStub().getCountry();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.country;
        }
    }
    public void setCountry(String country)
    {
        if(getStub() != null) {
            getStub().setCountry(country);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.country = country;
        }
    }

    public Long getTalliedTime()
    {
        if(getStub() != null) {
            return getStub().getTalliedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.talliedTime;
        }
    }
    public void setTalliedTime(Long talliedTime)
    {
        if(getStub() != null) {
            getStub().setTalliedTime(talliedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.talliedTime = talliedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public ShortUrlAccessStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ShortUrlAccessStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("tallyTime = " + this.tallyTime).append(";");
            sb.append("tallyEpoch = " + this.tallyEpoch).append(";");
            sb.append("count = " + this.count).append(";");
            sb.append("shortUrl = " + this.shortUrl).append(";");
            sb.append("shortUrlDomain = " + this.shortUrlDomain).append(";");
            sb.append("longUrl = " + this.longUrl).append(";");
            sb.append("longUrlDomain = " + this.longUrlDomain).append(";");
            sb.append("redirectType = " + this.redirectType).append(";");
            sb.append("refererDomain = " + this.refererDomain).append(";");
            sb.append("userAgent = " + this.userAgent).append(";");
            sb.append("language = " + this.language).append(";");
            sb.append("country = " + this.country).append(";");
            sb.append("talliedTime = " + this.talliedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyTime == null ? 0 : tallyTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyEpoch == null ? 0 : tallyEpoch.hashCode();
            _hash = 31 * _hash + delta;
            delta = count == null ? 0 : count.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrl == null ? 0 : shortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrlDomain == null ? 0 : shortUrlDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrl == null ? 0 : longUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = longUrlDomain == null ? 0 : longUrlDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = redirectType == null ? 0 : redirectType.hashCode();
            _hash = 31 * _hash + delta;
            delta = refererDomain == null ? 0 : refererDomain.hashCode();
            _hash = 31 * _hash + delta;
            delta = userAgent == null ? 0 : userAgent.hashCode();
            _hash = 31 * _hash + delta;
            delta = language == null ? 0 : language.hashCode();
            _hash = 31 * _hash + delta;
            delta = country == null ? 0 : country.hashCode();
            _hash = 31 * _hash + delta;
            delta = talliedTime == null ? 0 : talliedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
