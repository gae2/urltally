package com.urltally.af.service.impl;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.service.AbstractServiceFactory;
import com.urltally.af.service.ApiConsumerService;
import com.urltally.af.service.UserService;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.af.service.ServiceInfoService;
import com.urltally.af.service.FiveTenService;

public class ImplServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ImplServiceFactory.class.getName());

    private ImplServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ImplServiceFactoryHolder
    {
        private static final ImplServiceFactory INSTANCE = new ImplServiceFactory();
    }

    // Singleton method
    public static ImplServiceFactory getInstance()
    {
        return ImplServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerServiceImpl();
    }

    @Override
    public UserService getUserService()
    {
        return new UserServiceImpl();
    }

    @Override
    public AccessTallyMasterService getAccessTallyMasterService()
    {
        return new AccessTallyMasterServiceImpl();
    }

    @Override
    public AccessTallyStatusService getAccessTallyStatusService()
    {
        return new AccessTallyStatusServiceImpl();
    }

    @Override
    public MonthlyShortUrlAccessService getMonthlyShortUrlAccessService()
    {
        return new MonthlyShortUrlAccessServiceImpl();
    }

    @Override
    public WeeklyShortUrlAccessService getWeeklyShortUrlAccessService()
    {
        return new WeeklyShortUrlAccessServiceImpl();
    }

    @Override
    public DailyShortUrlAccessService getDailyShortUrlAccessService()
    {
        return new DailyShortUrlAccessServiceImpl();
    }

    @Override
    public HourlyShortUrlAccessService getHourlyShortUrlAccessService()
    {
        return new HourlyShortUrlAccessServiceImpl();
    }

    @Override
    public CumulativeShortUrlAccessService getCumulativeShortUrlAccessService()
    {
        return new CumulativeShortUrlAccessServiceImpl();
    }

    @Override
    public CurrentShortUrlAccessService getCurrentShortUrlAccessService()
    {
        return new CurrentShortUrlAccessServiceImpl();
    }

    @Override
    public TotalShortUrlAccessService getTotalShortUrlAccessService()
    {
        return new TotalShortUrlAccessServiceImpl();
    }

    @Override
    public TotalLongUrlAccessService getTotalLongUrlAccessService()
    {
        return new TotalLongUrlAccessServiceImpl();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoServiceImpl();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenServiceImpl();
    }


}
