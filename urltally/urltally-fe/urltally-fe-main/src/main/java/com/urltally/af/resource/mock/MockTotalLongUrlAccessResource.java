package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.ws.stub.TotalLongUrlAccessStub;
import com.urltally.ws.stub.TotalLongUrlAccessListStub;
import com.urltally.af.bean.TotalLongUrlAccessBean;
import com.urltally.af.resource.TotalLongUrlAccessResource;


// MockTotalLongUrlAccessResource is a decorator.
// It can be used as a base class to mock TotalLongUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/totalLongUrlAccesses/")
public abstract class MockTotalLongUrlAccessResource implements TotalLongUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockTotalLongUrlAccessResource.class.getName());

    // MockTotalLongUrlAccessResource uses the decorator design pattern.
    private TotalLongUrlAccessResource decoratedResource;

    public MockTotalLongUrlAccessResource(TotalLongUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected TotalLongUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(TotalLongUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTotalLongUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllTotalLongUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findTotalLongUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findTotalLongUrlAccessesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getTotalLongUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getTotalLongUrlAccessAsHtml(guid);
//    }

    @Override
    public Response getTotalLongUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getTotalLongUrlAccess(guid);
    }

    @Override
    public Response getTotalLongUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getTotalLongUrlAccessAsJsonp(guid, callback);
    }

    @Override
    public Response getTotalLongUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getTotalLongUrlAccess(guid, field);
    }

    // TBD
    @Override
    public Response constructTotalLongUrlAccess(TotalLongUrlAccessStub totalLongUrlAccess) throws BaseResourceException
    {
        return decoratedResource.constructTotalLongUrlAccess(totalLongUrlAccess);
    }

    @Override
    public Response createTotalLongUrlAccess(TotalLongUrlAccessStub totalLongUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createTotalLongUrlAccess(totalLongUrlAccess);
    }

//    @Override
//    public Response createTotalLongUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createTotalLongUrlAccess(formParams);
//    }

    // TBD
    @Override
    public Response refreshTotalLongUrlAccess(String guid, TotalLongUrlAccessStub totalLongUrlAccess) throws BaseResourceException
    {
        return decoratedResource.refreshTotalLongUrlAccess(guid, totalLongUrlAccess);
    }

    @Override
    public Response updateTotalLongUrlAccess(String guid, TotalLongUrlAccessStub totalLongUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateTotalLongUrlAccess(guid, totalLongUrlAccess);
    }

    @Override
    public Response updateTotalLongUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain)
    {
        return decoratedResource.updateTotalLongUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
    }

//    @Override
//    public Response updateTotalLongUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateTotalLongUrlAccess(guid, formParams);
//    }

    @Override
    public Response deleteTotalLongUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteTotalLongUrlAccess(guid);
    }

    @Override
    public Response deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteTotalLongUrlAccesses(filter, params, values);
    }


// TBD ....
    @Override
    public Response createTotalLongUrlAccesses(TotalLongUrlAccessListStub totalLongUrlAccesses) throws BaseResourceException
    {
        return decoratedResource.createTotalLongUrlAccesses(totalLongUrlAccesses);
    }


}
