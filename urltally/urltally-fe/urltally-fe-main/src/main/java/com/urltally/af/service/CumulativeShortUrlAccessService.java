package com.urltally.af.service;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CumulativeShortUrlAccess;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface CumulativeShortUrlAccessService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    CumulativeShortUrlAccess getCumulativeShortUrlAccess(String guid) throws BaseException;
    Object getCumulativeShortUrlAccess(String guid, String field) throws BaseException;
    List<CumulativeShortUrlAccess> getCumulativeShortUrlAccesses(List<String> guids) throws BaseException;
    List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createCumulativeShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException;
    //String createCumulativeShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return CumulativeShortUrlAccess?)
    String createCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException;
    CumulativeShortUrlAccess constructCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException;
    Boolean updateCumulativeShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException;
    //Boolean updateCumulativeShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException;
    CumulativeShortUrlAccess refreshCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException;
    Boolean deleteCumulativeShortUrlAccess(String guid) throws BaseException;
    Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException;
    Long deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createCumulativeShortUrlAccesses(List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses) throws BaseException;
//    Boolean updateCumulativeShortUrlAccesses(List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses) throws BaseException;

}
