package com.urltally.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.urltally.ws.BaseException;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.af.bean.TotalLongUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.af.service.impl.TotalLongUrlAccessServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class TotalLongUrlAccessAppService extends TotalLongUrlAccessServiceImpl implements TotalLongUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public TotalLongUrlAccessAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // TotalLongUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public TotalLongUrlAccess getTotalLongUrlAccess(String guid) throws BaseException
    {
        return super.getTotalLongUrlAccess(guid);
    }

    @Override
    public Object getTotalLongUrlAccess(String guid, String field) throws BaseException
    {
        return super.getTotalLongUrlAccess(guid, field);
    }

    @Override
    public List<TotalLongUrlAccess> getTotalLongUrlAccesses(List<String> guids) throws BaseException
    {
        return super.getTotalLongUrlAccesses(guids);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses() throws BaseException
    {
        return super.getAllTotalLongUrlAccesses();
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllTotalLongUrlAccessKeys(ordering, offset, count);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        return super.createTotalLongUrlAccess(totalLongUrlAccess);
    }

    @Override
    public TotalLongUrlAccess constructTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        return super.constructTotalLongUrlAccess(totalLongUrlAccess);
    }


    @Override
    public Boolean updateTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        return super.updateTotalLongUrlAccess(totalLongUrlAccess);
    }
        
    @Override
    public TotalLongUrlAccess refreshTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        return super.refreshTotalLongUrlAccess(totalLongUrlAccess);
    }

    @Override
    public Boolean deleteTotalLongUrlAccess(String guid) throws BaseException
    {
        return super.deleteTotalLongUrlAccess(guid);
    }

    @Override
    public Boolean deleteTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        return super.deleteTotalLongUrlAccess(totalLongUrlAccess);
    }

    @Override
    public Integer createTotalLongUrlAccesses(List<TotalLongUrlAccess> totalLongUrlAccesses) throws BaseException
    {
        return super.createTotalLongUrlAccesses(totalLongUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateTotalLongUrlAccesses(List<TotalLongUrlAccess> totalLongUrlAccesses) throws BaseException
    //{
    //}

}
