package com.urltally.af.proxy.async;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.ApiConsumerServiceProxy;
import com.urltally.af.proxy.UserServiceProxy;
import com.urltally.af.proxy.AccessTallyMasterServiceProxy;
import com.urltally.af.proxy.AccessTallyStatusServiceProxy;
import com.urltally.af.proxy.MonthlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.WeeklyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.DailyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.HourlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.CumulativeShortUrlAccessServiceProxy;
import com.urltally.af.proxy.CurrentShortUrlAccessServiceProxy;
import com.urltally.af.proxy.TotalShortUrlAccessServiceProxy;
import com.urltally.af.proxy.TotalLongUrlAccessServiceProxy;
import com.urltally.af.proxy.ServiceInfoServiceProxy;
import com.urltally.af.proxy.FiveTenServiceProxy;

public class AsyncProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(AsyncProxyFactory.class.getName());

    private AsyncProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AsyncProxyFactoryHolder
    {
        private static final AsyncProxyFactory INSTANCE = new AsyncProxyFactory();
    }

    // Singleton method
    public static AsyncProxyFactory getInstance()
    {
        return AsyncProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new AsyncApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new AsyncUserServiceProxy();
    }

    @Override
    public AccessTallyMasterServiceProxy getAccessTallyMasterServiceProxy()
    {
        return new AsyncAccessTallyMasterServiceProxy();
    }

    @Override
    public AccessTallyStatusServiceProxy getAccessTallyStatusServiceProxy()
    {
        return new AsyncAccessTallyStatusServiceProxy();
    }

    @Override
    public MonthlyShortUrlAccessServiceProxy getMonthlyShortUrlAccessServiceProxy()
    {
        return new AsyncMonthlyShortUrlAccessServiceProxy();
    }

    @Override
    public WeeklyShortUrlAccessServiceProxy getWeeklyShortUrlAccessServiceProxy()
    {
        return new AsyncWeeklyShortUrlAccessServiceProxy();
    }

    @Override
    public DailyShortUrlAccessServiceProxy getDailyShortUrlAccessServiceProxy()
    {
        return new AsyncDailyShortUrlAccessServiceProxy();
    }

    @Override
    public HourlyShortUrlAccessServiceProxy getHourlyShortUrlAccessServiceProxy()
    {
        return new AsyncHourlyShortUrlAccessServiceProxy();
    }

    @Override
    public CumulativeShortUrlAccessServiceProxy getCumulativeShortUrlAccessServiceProxy()
    {
        return new AsyncCumulativeShortUrlAccessServiceProxy();
    }

    @Override
    public CurrentShortUrlAccessServiceProxy getCurrentShortUrlAccessServiceProxy()
    {
        return new AsyncCurrentShortUrlAccessServiceProxy();
    }

    @Override
    public TotalShortUrlAccessServiceProxy getTotalShortUrlAccessServiceProxy()
    {
        return new AsyncTotalShortUrlAccessServiceProxy();
    }

    @Override
    public TotalLongUrlAccessServiceProxy getTotalLongUrlAccessServiceProxy()
    {
        return new AsyncTotalLongUrlAccessServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new AsyncServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new AsyncFiveTenServiceProxy();
    }

}
