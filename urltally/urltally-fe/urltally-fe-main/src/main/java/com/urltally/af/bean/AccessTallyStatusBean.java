package com.urltally.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.stub.AccessTallyStatusStub;


// Wrapper class + bean combo.
public class AccessTallyStatusBean implements AccessTallyStatus, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AccessTallyStatusBean.class.getName());

    // [1] With an embedded object.
    private AccessTallyStatusStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String remoteRecordGuid;
    private String shortUrl;
    private String tallyType;
    private String tallyTime;
    private Long tallyEpoch;
    private Boolean processed;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public AccessTallyStatusBean()
    {
        //this((String) null);
    }
    public AccessTallyStatusBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public AccessTallyStatusBean(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed)
    {
        this(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed, null, null);
    }
    public AccessTallyStatusBean(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.remoteRecordGuid = remoteRecordGuid;
        this.shortUrl = shortUrl;
        this.tallyType = tallyType;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.processed = processed;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public AccessTallyStatusBean(AccessTallyStatus stub)
    {
        if(stub instanceof AccessTallyStatusStub) {
            this.stub = (AccessTallyStatusStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setRemoteRecordGuid(stub.getRemoteRecordGuid());   
            setShortUrl(stub.getShortUrl());   
            setTallyType(stub.getTallyType());   
            setTallyTime(stub.getTallyTime());   
            setTallyEpoch(stub.getTallyEpoch());   
            setProcessed(stub.isProcessed());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getRemoteRecordGuid()
    {
        if(getStub() != null) {
            return getStub().getRemoteRecordGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.remoteRecordGuid;
        }
    }
    public void setRemoteRecordGuid(String remoteRecordGuid)
    {
        if(getStub() != null) {
            getStub().setRemoteRecordGuid(remoteRecordGuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.remoteRecordGuid = remoteRecordGuid;
        }
    }

    public String getShortUrl()
    {
        if(getStub() != null) {
            return getStub().getShortUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.shortUrl;
        }
    }
    public void setShortUrl(String shortUrl)
    {
        if(getStub() != null) {
            getStub().setShortUrl(shortUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.shortUrl = shortUrl;
        }
    }

    public String getTallyType()
    {
        if(getStub() != null) {
            return getStub().getTallyType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyType;
        }
    }
    public void setTallyType(String tallyType)
    {
        if(getStub() != null) {
            getStub().setTallyType(tallyType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyType = tallyType;
        }
    }

    public String getTallyTime()
    {
        if(getStub() != null) {
            return getStub().getTallyTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyTime;
        }
    }
    public void setTallyTime(String tallyTime)
    {
        if(getStub() != null) {
            getStub().setTallyTime(tallyTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyTime = tallyTime;
        }
    }

    public Long getTallyEpoch()
    {
        if(getStub() != null) {
            return getStub().getTallyEpoch();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyEpoch;
        }
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        if(getStub() != null) {
            getStub().setTallyEpoch(tallyEpoch);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyEpoch = tallyEpoch;
        }
    }

    public Boolean isProcessed()
    {
        if(getStub() != null) {
            return getStub().isProcessed();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.processed;
        }
    }
    public void setProcessed(Boolean processed)
    {
        if(getStub() != null) {
            getStub().setProcessed(processed);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.processed = processed;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public AccessTallyStatusStub getStub()
    {
        return this.stub;
    }
    protected void setStub(AccessTallyStatusStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("remoteRecordGuid = " + this.remoteRecordGuid).append(";");
            sb.append("shortUrl = " + this.shortUrl).append(";");
            sb.append("tallyType = " + this.tallyType).append(";");
            sb.append("tallyTime = " + this.tallyTime).append(";");
            sb.append("tallyEpoch = " + this.tallyEpoch).append(";");
            sb.append("processed = " + this.processed).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = remoteRecordGuid == null ? 0 : remoteRecordGuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = shortUrl == null ? 0 : shortUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyType == null ? 0 : tallyType.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyTime == null ? 0 : tallyTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyEpoch == null ? 0 : tallyEpoch.hashCode();
            _hash = 31 * _hash + delta;
            delta = processed == null ? 0 : processed.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
