package com.urltally.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.AccessTallyMasterService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AccessTallyMasterServiceImpl implements AccessTallyMasterService
{
    private static final Logger log = Logger.getLogger(AccessTallyMasterServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "AccessTallyMaster-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("AccessTallyMaster:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public AccessTallyMasterServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // AccessTallyMaster related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAccessTallyMaster(): guid = " + guid);

        AccessTallyMasterBean bean = null;
        if(getCache() != null) {
            bean = (AccessTallyMasterBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AccessTallyMasterBean) getProxyFactory().getAccessTallyMasterServiceProxy().getAccessTallyMaster(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AccessTallyMasterBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AccessTallyMasterBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAccessTallyMaster(String guid, String field) throws BaseException
    {
        AccessTallyMasterBean bean = null;
        if(getCache() != null) {
            bean = (AccessTallyMasterBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AccessTallyMasterBean) getProxyFactory().getAccessTallyMasterServiceProxy().getAccessTallyMaster(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AccessTallyMasterBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AccessTallyMasterBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyType")) {
            return bean.getTallyType();
        } else if(field.equals("tallyTime")) {
            return bean.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return bean.getTallyEpoch();
        } else if(field.equals("tallyStatus")) {
            return bean.getTallyStatus();
        } else if(field.equals("accessRecordCount")) {
            return bean.getAccessRecordCount();
        } else if(field.equals("procesingStartedTime")) {
            return bean.getProcesingStartedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        log.fine("getAccessTallyMasters()");

        // TBD: Is there a better way????
        List<AccessTallyMaster> accessTallyMasters = getProxyFactory().getAccessTallyMasterServiceProxy().getAccessTallyMasters(guids);
        if(accessTallyMasters == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyMasterBean list.");
        }

        log.finer("END");
        return accessTallyMasters;
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException
    {
        return getAllAccessTallyMasters(null, null, null);
    }


    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyMasters(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<AccessTallyMaster> accessTallyMasters = getProxyFactory().getAccessTallyMasterServiceProxy().getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
        if(accessTallyMasters == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyMasterBean list.");
        }

        log.finer("END");
        return accessTallyMasters;
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyMasterKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAccessTallyMasterServiceProxy().getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyMasterBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AccessTallyMasterBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyMasterServiceImpl.findAccessTallyMasters(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<AccessTallyMaster> accessTallyMasters = getProxyFactory().getAccessTallyMasterServiceProxy().findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(accessTallyMasters == null) {
            log.log(Level.WARNING, "Failed to find accessTallyMasters for the given criterion.");
        }

        log.finer("END");
        return accessTallyMasters;
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyMasterServiceImpl.findAccessTallyMasterKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAccessTallyMasterServiceProxy().findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AccessTallyMaster keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AccessTallyMaster key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyMasterServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getAccessTallyMasterServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        AccessTallyMasterBean bean = new AccessTallyMasterBean(null, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        return createAccessTallyMaster(bean);
    }

    @Override
    public String createAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AccessTallyMaster bean = constructAccessTallyMaster(accessTallyMaster);
        //return bean.getGuid();

        // Param accessTallyMaster cannot be null.....
        if(accessTallyMaster == null) {
            log.log(Level.INFO, "Param accessTallyMaster is null!");
            throw new BadRequestException("Param accessTallyMaster object is null!");
        }
        AccessTallyMasterBean bean = null;
        if(accessTallyMaster instanceof AccessTallyMasterBean) {
            bean = (AccessTallyMasterBean) accessTallyMaster;
        } else if(accessTallyMaster instanceof AccessTallyMaster) {
            // bean = new AccessTallyMasterBean(null, accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AccessTallyMasterBean(accessTallyMaster.getGuid(), accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
        } else {
            log.log(Level.WARNING, "createAccessTallyMaster(): Arg accessTallyMaster is of an unknown type.");
            //bean = new AccessTallyMasterBean();
            bean = new AccessTallyMasterBean(accessTallyMaster.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAccessTallyMasterServiceProxy().createAccessTallyMaster(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public AccessTallyMaster constructAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyMaster cannot be null.....
        if(accessTallyMaster == null) {
            log.log(Level.INFO, "Param accessTallyMaster is null!");
            throw new BadRequestException("Param accessTallyMaster object is null!");
        }
        AccessTallyMasterBean bean = null;
        if(accessTallyMaster instanceof AccessTallyMasterBean) {
            bean = (AccessTallyMasterBean) accessTallyMaster;
        } else if(accessTallyMaster instanceof AccessTallyMaster) {
            // bean = new AccessTallyMasterBean(null, accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AccessTallyMasterBean(accessTallyMaster.getGuid(), accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
        } else {
            log.log(Level.WARNING, "createAccessTallyMaster(): Arg accessTallyMaster is of an unknown type.");
            //bean = new AccessTallyMasterBean();
            bean = new AccessTallyMasterBean(accessTallyMaster.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAccessTallyMasterServiceProxy().createAccessTallyMaster(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AccessTallyMasterBean bean = new AccessTallyMasterBean(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        return updateAccessTallyMaster(bean);
    }
        
    @Override
    public Boolean updateAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AccessTallyMaster bean = refreshAccessTallyMaster(accessTallyMaster);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param accessTallyMaster cannot be null.....
        if(accessTallyMaster == null || accessTallyMaster.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyMaster or its guid is null!");
            throw new BadRequestException("Param accessTallyMaster object or its guid is null!");
        }
        AccessTallyMasterBean bean = null;
        if(accessTallyMaster instanceof AccessTallyMasterBean) {
            bean = (AccessTallyMasterBean) accessTallyMaster;
        } else {  // if(accessTallyMaster instanceof AccessTallyMaster)
            bean = new AccessTallyMasterBean(accessTallyMaster.getGuid(), accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
        }
        Boolean suc = getProxyFactory().getAccessTallyMasterServiceProxy().updateAccessTallyMaster(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public AccessTallyMaster refreshAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyMaster cannot be null.....
        if(accessTallyMaster == null || accessTallyMaster.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyMaster or its guid is null!");
            throw new BadRequestException("Param accessTallyMaster object or its guid is null!");
        }
        AccessTallyMasterBean bean = null;
        if(accessTallyMaster instanceof AccessTallyMasterBean) {
            bean = (AccessTallyMasterBean) accessTallyMaster;
        } else {  // if(accessTallyMaster instanceof AccessTallyMaster)
            bean = new AccessTallyMasterBean(accessTallyMaster.getGuid(), accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
        }
        Boolean suc = getProxyFactory().getAccessTallyMasterServiceProxy().updateAccessTallyMaster(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getAccessTallyMasterServiceProxy().deleteAccessTallyMaster(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            AccessTallyMaster accessTallyMaster = null;
            try {
                accessTallyMaster = getProxyFactory().getAccessTallyMasterServiceProxy().getAccessTallyMaster(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch accessTallyMaster with a key, " + guid);
                return false;
            }
            if(accessTallyMaster != null) {
                String beanGuid = accessTallyMaster.getGuid();
                Boolean suc1 = getProxyFactory().getAccessTallyMasterServiceProxy().deleteAccessTallyMaster(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("accessTallyMaster with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyMaster cannot be null.....
        if(accessTallyMaster == null || accessTallyMaster.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyMaster or its guid is null!");
            throw new BadRequestException("Param accessTallyMaster object or its guid is null!");
        }
        AccessTallyMasterBean bean = null;
        if(accessTallyMaster instanceof AccessTallyMasterBean) {
            bean = (AccessTallyMasterBean) accessTallyMaster;
        } else {  // if(accessTallyMaster instanceof AccessTallyMaster)
            // ????
            log.warning("accessTallyMaster is not an instance of AccessTallyMasterBean.");
            bean = new AccessTallyMasterBean(accessTallyMaster.getGuid(), accessTallyMaster.getTallyType(), accessTallyMaster.getTallyTime(), accessTallyMaster.getTallyEpoch(), accessTallyMaster.getTallyStatus(), accessTallyMaster.getAccessRecordCount(), accessTallyMaster.getProcesingStartedTime());
        }
        Boolean suc = getProxyFactory().getAccessTallyMasterServiceProxy().deleteAccessTallyMaster(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getAccessTallyMasterServiceProxy().deleteAccessTallyMasters(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException
    {
        log.finer("BEGIN");

        if(accessTallyMasters == null) {
            log.log(Level.WARNING, "createAccessTallyMasters() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = accessTallyMasters.size();
        if(size == 0) {
            log.log(Level.WARNING, "createAccessTallyMasters() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(AccessTallyMaster accessTallyMaster : accessTallyMasters) {
            String guid = createAccessTallyMaster(accessTallyMaster);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createAccessTallyMasters() failed for at least one accessTallyMaster. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException
    //{
    //}

}
