package com.urltally.af.service;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.HourlyShortUrlAccess;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface HourlyShortUrlAccessService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    HourlyShortUrlAccess getHourlyShortUrlAccess(String guid) throws BaseException;
    Object getHourlyShortUrlAccess(String guid, String field) throws BaseException;
    List<HourlyShortUrlAccess> getHourlyShortUrlAccesses(List<String> guids) throws BaseException;
    List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createHourlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException;
    //String createHourlyShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return HourlyShortUrlAccess?)
    String createHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException;
    HourlyShortUrlAccess constructHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException;
    Boolean updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException;
    //Boolean updateHourlyShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException;
    HourlyShortUrlAccess refreshHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException;
    Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException;
    Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess) throws BaseException;
    Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createHourlyShortUrlAccesses(List<HourlyShortUrlAccess> hourlyShortUrlAccesses) throws BaseException;
//    Boolean updateHourlyShortUrlAccesses(List<HourlyShortUrlAccess> hourlyShortUrlAccesses) throws BaseException;

}
