package com.urltally.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.AccessTallyStatusService;


// AccessTallyStatusMockService is a decorator.
// It can be used as a base class to mock AccessTallyStatusService objects.
public abstract class AccessTallyStatusMockService implements AccessTallyStatusService
{
    private static final Logger log = Logger.getLogger(AccessTallyStatusMockService.class.getName());

    // AccessTallyStatusMockService uses the decorator design pattern.
    private AccessTallyStatusService decoratedService;

    public AccessTallyStatusMockService(AccessTallyStatusService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected AccessTallyStatusService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(AccessTallyStatusService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // AccessTallyStatus related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAccessTallyStatus(): guid = " + guid);
        AccessTallyStatus bean = decoratedService.getAccessTallyStatus(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getAccessTallyStatus(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getAccessTallyStatus(guid, field);
        return obj;
    }

    @Override
    public List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        log.fine("getAccessTallyStatuses()");
        List<AccessTallyStatus> accessTallyStatuses = decoratedService.getAccessTallyStatuses(guids);
        log.finer("END");
        return accessTallyStatuses;
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException
    {
        return getAllAccessTallyStatuses(null, null, null);
    }


    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyStatuses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<AccessTallyStatus> accessTallyStatuses = decoratedService.getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return accessTallyStatuses;
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyStatusKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusMockService.findAccessTallyStatuses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<AccessTallyStatus> accessTallyStatuses = decoratedService.findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return accessTallyStatuses;
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusMockService.findAccessTallyStatusKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        AccessTallyStatusBean bean = new AccessTallyStatusBean(null, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
        return createAccessTallyStatus(bean);
    }

    @Override
    public String createAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createAccessTallyStatus(accessTallyStatus);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public AccessTallyStatus constructAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");
        AccessTallyStatus bean = decoratedService.constructAccessTallyStatus(accessTallyStatus);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AccessTallyStatusBean bean = new AccessTallyStatusBean(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
        return updateAccessTallyStatus(bean);
    }
        
    @Override
    public Boolean updateAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateAccessTallyStatus(accessTallyStatus);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public AccessTallyStatus refreshAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");
        AccessTallyStatus bean = decoratedService.refreshAccessTallyStatus(accessTallyStatus);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteAccessTallyStatus(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteAccessTallyStatus(accessTallyStatus);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteAccessTallyStatuses(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createAccessTallyStatuses(accessTallyStatuses);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    //{
    //}

}
