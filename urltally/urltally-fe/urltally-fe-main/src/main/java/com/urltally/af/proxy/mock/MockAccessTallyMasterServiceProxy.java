package com.urltally.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.ws.service.AccessTallyMasterService;
import com.urltally.af.proxy.AccessTallyMasterServiceProxy;


// MockAccessTallyMasterServiceProxy is a decorator.
// It can be used as a base class to mock AccessTallyMasterServiceProxy objects.
public abstract class MockAccessTallyMasterServiceProxy implements AccessTallyMasterServiceProxy
{
    private static final Logger log = Logger.getLogger(MockAccessTallyMasterServiceProxy.class.getName());

    // MockAccessTallyMasterServiceProxy uses the decorator design pattern.
    private AccessTallyMasterServiceProxy decoratedProxy;

    public MockAccessTallyMasterServiceProxy(AccessTallyMasterServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected AccessTallyMasterServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(AccessTallyMasterServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException
    {
        return decoratedProxy.getAccessTallyMaster(guid);
    }

    @Override
    public Object getAccessTallyMaster(String guid, String field) throws BaseException
    {
        return decoratedProxy.getAccessTallyMaster(guid, field);       
    }

    @Override
    public List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        return decoratedProxy.getAccessTallyMasters(guids);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException
    {
        return getAllAccessTallyMasters(null, null, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllAccessTallyMasters(ordering, offset, count);
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllAccessTallyMasterKeys(ordering, offset, count);
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        return decoratedProxy.createAccessTallyMaster(tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }

    @Override
    public String createAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        return decoratedProxy.createAccessTallyMaster(accessTallyMaster);
    }

    @Override
    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        return decoratedProxy.updateAccessTallyMaster(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }

    @Override
    public Boolean updateAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        return decoratedProxy.updateAccessTallyMaster(accessTallyMaster);
    }

    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyMaster(guid);
    }

    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        String guid = accessTallyMaster.getGuid();
        return deleteAccessTallyMaster(guid);
    }

    @Override
    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyMasters(filter, params, values);
    }

}
