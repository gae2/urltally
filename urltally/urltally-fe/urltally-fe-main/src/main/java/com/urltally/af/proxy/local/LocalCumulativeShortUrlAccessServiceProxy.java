package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CumulativeShortUrlAccess;
// import com.urltally.ws.bean.CumulativeShortUrlAccessBean;
import com.urltally.ws.service.CumulativeShortUrlAccessService;
import com.urltally.af.bean.CumulativeShortUrlAccessBean;
import com.urltally.af.proxy.CumulativeShortUrlAccessServiceProxy;


public class LocalCumulativeShortUrlAccessServiceProxy extends BaseLocalServiceProxy implements CumulativeShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalCumulativeShortUrlAccessServiceProxy.class.getName());

    public LocalCumulativeShortUrlAccessServiceProxy()
    {
    }

    @Override
    public CumulativeShortUrlAccess getCumulativeShortUrlAccess(String guid) throws BaseException
    {
        CumulativeShortUrlAccess serverBean = getCumulativeShortUrlAccessService().getCumulativeShortUrlAccess(guid);
        CumulativeShortUrlAccess appBean = convertServerCumulativeShortUrlAccessBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getCumulativeShortUrlAccess(String guid, String field) throws BaseException
    {
        return getCumulativeShortUrlAccessService().getCumulativeShortUrlAccess(guid, field);       
    }

    @Override
    public List<CumulativeShortUrlAccess> getCumulativeShortUrlAccesses(List<String> guids) throws BaseException
    {
        List<CumulativeShortUrlAccess> serverBeanList = getCumulativeShortUrlAccessService().getCumulativeShortUrlAccesses(guids);
        List<CumulativeShortUrlAccess> appBeanList = convertServerCumulativeShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses() throws BaseException
    {
        return getAllCumulativeShortUrlAccesses(null, null, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getCumulativeShortUrlAccessService().getAllCumulativeShortUrlAccesses(ordering, offset, count);
        return getAllCumulativeShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<CumulativeShortUrlAccess> serverBeanList = getCumulativeShortUrlAccessService().getAllCumulativeShortUrlAccesses(ordering, offset, count, forwardCursor);
        List<CumulativeShortUrlAccess> appBeanList = convertServerCumulativeShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getCumulativeShortUrlAccessService().getAllCumulativeShortUrlAccessKeys(ordering, offset, count);
        return getAllCumulativeShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getCumulativeShortUrlAccessService().getAllCumulativeShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getCumulativeShortUrlAccessService().findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<CumulativeShortUrlAccess> serverBeanList = getCumulativeShortUrlAccessService().findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<CumulativeShortUrlAccess> appBeanList = convertServerCumulativeShortUrlAccessBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getCumulativeShortUrlAccessService().findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getCumulativeShortUrlAccessService().findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getCumulativeShortUrlAccessService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createCumulativeShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {
        return getCumulativeShortUrlAccessService().createCumulativeShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
    }

    @Override
    public String createCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.CumulativeShortUrlAccessBean serverBean =  convertAppCumulativeShortUrlAccessBeanToServerBean(cumulativeShortUrlAccess);
        return getCumulativeShortUrlAccessService().createCumulativeShortUrlAccess(serverBean);
    }

    @Override
    public Boolean updateCumulativeShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {
        return getCumulativeShortUrlAccessService().updateCumulativeShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
    }

    @Override
    public Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.CumulativeShortUrlAccessBean serverBean =  convertAppCumulativeShortUrlAccessBeanToServerBean(cumulativeShortUrlAccess);
        return getCumulativeShortUrlAccessService().updateCumulativeShortUrlAccess(serverBean);
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(String guid) throws BaseException
    {
        return getCumulativeShortUrlAccessService().deleteCumulativeShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        com.urltally.ws.bean.CumulativeShortUrlAccessBean serverBean =  convertAppCumulativeShortUrlAccessBeanToServerBean(cumulativeShortUrlAccess);
        return getCumulativeShortUrlAccessService().deleteCumulativeShortUrlAccess(serverBean);
    }

    @Override
    public Long deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return getCumulativeShortUrlAccessService().deleteCumulativeShortUrlAccesses(filter, params, values);
    }




    public static CumulativeShortUrlAccessBean convertServerCumulativeShortUrlAccessBeanToAppBean(CumulativeShortUrlAccess serverBean)
    {
        CumulativeShortUrlAccessBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new CumulativeShortUrlAccessBean();
            bean.setGuid(serverBean.getGuid());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setCount(serverBean.getCount());
            bean.setShortUrl(serverBean.getShortUrl());
            bean.setShortUrlDomain(serverBean.getShortUrlDomain());
            bean.setLongUrl(serverBean.getLongUrl());
            bean.setLongUrlDomain(serverBean.getLongUrlDomain());
            bean.setRedirectType(serverBean.getRedirectType());
            bean.setRefererDomain(serverBean.getRefererDomain());
            bean.setUserAgent(serverBean.getUserAgent());
            bean.setLanguage(serverBean.getLanguage());
            bean.setCountry(serverBean.getCountry());
            bean.setTalliedTime(serverBean.getTalliedTime());
            bean.setStartDayHour(serverBean.getStartDayHour());
            bean.setEndDayHour(serverBean.getEndDayHour());
            bean.setStartTime(serverBean.getStartTime());
            bean.setEndTime(serverBean.getEndTime());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<CumulativeShortUrlAccess> convertServerCumulativeShortUrlAccessBeanListToAppBeanList(List<CumulativeShortUrlAccess> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<CumulativeShortUrlAccess> beanList = new ArrayList<CumulativeShortUrlAccess>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(CumulativeShortUrlAccess sb : serverBeanList) {
                CumulativeShortUrlAccessBean bean = convertServerCumulativeShortUrlAccessBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.CumulativeShortUrlAccessBean convertAppCumulativeShortUrlAccessBeanToServerBean(CumulativeShortUrlAccess appBean)
    {
        com.urltally.ws.bean.CumulativeShortUrlAccessBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.CumulativeShortUrlAccessBean();
            bean.setGuid(appBean.getGuid());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setCount(appBean.getCount());
            bean.setShortUrl(appBean.getShortUrl());
            bean.setShortUrlDomain(appBean.getShortUrlDomain());
            bean.setLongUrl(appBean.getLongUrl());
            bean.setLongUrlDomain(appBean.getLongUrlDomain());
            bean.setRedirectType(appBean.getRedirectType());
            bean.setRefererDomain(appBean.getRefererDomain());
            bean.setUserAgent(appBean.getUserAgent());
            bean.setLanguage(appBean.getLanguage());
            bean.setCountry(appBean.getCountry());
            bean.setTalliedTime(appBean.getTalliedTime());
            bean.setStartDayHour(appBean.getStartDayHour());
            bean.setEndDayHour(appBean.getEndDayHour());
            bean.setStartTime(appBean.getStartTime());
            bean.setEndTime(appBean.getEndTime());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<CumulativeShortUrlAccess> convertAppCumulativeShortUrlAccessBeanListToServerBeanList(List<CumulativeShortUrlAccess> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<CumulativeShortUrlAccess> beanList = new ArrayList<CumulativeShortUrlAccess>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(CumulativeShortUrlAccess sb : appBeanList) {
                com.urltally.ws.bean.CumulativeShortUrlAccessBean bean = convertAppCumulativeShortUrlAccessBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
