package com.urltally.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

public class QueryParamUtil
{
    private static final Logger log = Logger.getLogger(QueryParamUtil.class.getName());

    private QueryParamUtil() {}

    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_COUNT = "count";

    
    public static Long getParamOffset(HttpServletRequest request)
    {
        Long paramOffset = null;
        if(request != null) {
            String strOffset = request.getParameter(PARAM_OFFSET);
            if(strOffset != null) {
                try {
                    paramOffset = Long.parseLong(strOffset);
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: offset = " + strOffset, e);
                }
            }
        }
        return paramOffset;
    }

    public static Integer getParamCount(HttpServletRequest request)
    {
        Integer paramCount = null;
        if(request != null) {
            String strCount = request.getParameter(PARAM_COUNT);
            if(strCount != null) {
                try {
                    paramCount = Integer.parseInt(strCount);
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.INFO, "Invalid param: count = " + strCount, e);
                }
            }
        }
        return paramCount;
    }

    
    
    // TBD:
    
//    public static Map<String, String> parseQueryString(HttpServletRequest request)
//    {
//        Map<String, String> paramMap = new HashMap<String, String>();
//        if(request != null) {
//           // TBD ...            
//        }
//        return paramMap;
//    }

}
