package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.stub.AccessTallyMasterStub;
import com.urltally.ws.stub.AccessTallyMasterListStub;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.resource.AccessTallyMasterResource;


// MockAccessTallyMasterResource is a decorator.
// It can be used as a base class to mock AccessTallyMasterResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/accessTallyMasters/")
public abstract class MockAccessTallyMasterResource implements AccessTallyMasterResource
{
    private static final Logger log = Logger.getLogger(MockAccessTallyMasterResource.class.getName());

    // MockAccessTallyMasterResource uses the decorator design pattern.
    private AccessTallyMasterResource decoratedResource;

    public MockAccessTallyMasterResource(AccessTallyMasterResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected AccessTallyMasterResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(AccessTallyMasterResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findAccessTallyMastersAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findAccessTallyMastersAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getAccessTallyMasterAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getAccessTallyMasterAsHtml(guid);
//    }

    @Override
    public Response getAccessTallyMaster(String guid) throws BaseResourceException
    {
        return decoratedResource.getAccessTallyMaster(guid);
    }

    @Override
    public Response getAccessTallyMasterAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getAccessTallyMasterAsJsonp(guid, callback);
    }

    @Override
    public Response getAccessTallyMaster(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getAccessTallyMaster(guid, field);
    }

    // TBD
    @Override
    public Response constructAccessTallyMaster(AccessTallyMasterStub accessTallyMaster) throws BaseResourceException
    {
        return decoratedResource.constructAccessTallyMaster(accessTallyMaster);
    }

    @Override
    public Response createAccessTallyMaster(AccessTallyMasterStub accessTallyMaster) throws BaseResourceException
    {
        return decoratedResource.createAccessTallyMaster(accessTallyMaster);
    }

//    @Override
//    public Response createAccessTallyMaster(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createAccessTallyMaster(formParams);
//    }

    // TBD
    @Override
    public Response refreshAccessTallyMaster(String guid, AccessTallyMasterStub accessTallyMaster) throws BaseResourceException
    {
        return decoratedResource.refreshAccessTallyMaster(guid, accessTallyMaster);
    }

    @Override
    public Response updateAccessTallyMaster(String guid, AccessTallyMasterStub accessTallyMaster) throws BaseResourceException
    {
        return decoratedResource.updateAccessTallyMaster(guid, accessTallyMaster);
    }

    @Override
    public Response updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime)
    {
        return decoratedResource.updateAccessTallyMaster(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }

//    @Override
//    public Response updateAccessTallyMaster(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateAccessTallyMaster(guid, formParams);
//    }

    @Override
    public Response deleteAccessTallyMaster(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteAccessTallyMaster(guid);
    }

    @Override
    public Response deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteAccessTallyMasters(filter, params, values);
    }


// TBD ....
    @Override
    public Response createAccessTallyMasters(AccessTallyMasterListStub accessTallyMasters) throws BaseResourceException
    {
        return decoratedResource.createAccessTallyMasters(accessTallyMasters);
    }


}
