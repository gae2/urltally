package com.urltally.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.af.bean.CumulativeShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.af.service.impl.CumulativeShortUrlAccessServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class CumulativeShortUrlAccessProtoService extends CumulativeShortUrlAccessServiceImpl implements CumulativeShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(CumulativeShortUrlAccessProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public CumulativeShortUrlAccessProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // CumulativeShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public CumulativeShortUrlAccess getCumulativeShortUrlAccess(String guid) throws BaseException
    {
        return super.getCumulativeShortUrlAccess(guid);
    }

    @Override
    public Object getCumulativeShortUrlAccess(String guid, String field) throws BaseException
    {
        return super.getCumulativeShortUrlAccess(guid, field);
    }

    @Override
    public List<CumulativeShortUrlAccess> getCumulativeShortUrlAccesses(List<String> guids) throws BaseException
    {
        return super.getCumulativeShortUrlAccesses(guids);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses() throws BaseException
    {
        return super.getAllCumulativeShortUrlAccesses();
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCumulativeShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllCumulativeShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCumulativeShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllCumulativeShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        return super.createCumulativeShortUrlAccess(cumulativeShortUrlAccess);
    }

    @Override
    public CumulativeShortUrlAccess constructCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        return super.constructCumulativeShortUrlAccess(cumulativeShortUrlAccess);
    }


    @Override
    public Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        return super.updateCumulativeShortUrlAccess(cumulativeShortUrlAccess);
    }
        
    @Override
    public CumulativeShortUrlAccess refreshCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        return super.refreshCumulativeShortUrlAccess(cumulativeShortUrlAccess);
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(String guid) throws BaseException
    {
        return super.deleteCumulativeShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess) throws BaseException
    {
        return super.deleteCumulativeShortUrlAccess(cumulativeShortUrlAccess);
    }

    @Override
    public Integer createCumulativeShortUrlAccesses(List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses) throws BaseException
    {
        return super.createCumulativeShortUrlAccesses(cumulativeShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateCumulativeShortUrlAccesses(List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses) throws BaseException
    //{
    //}

}
