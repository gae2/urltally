package com.urltally.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.service.AbstractServiceFactory;
import com.urltally.af.service.ApiConsumerService;
import com.urltally.af.service.UserService;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.af.service.ServiceInfoService;
import com.urltally.af.service.FiveTenService;

// TBD:
// Factory? DI? (Does the current "dual" approach make sense?)
// Make it a singleton? Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

    // Reference to the Abstract factory.
    private static AbstractServiceFactory serviceFactory = ServiceFactoryManager.getServiceFactory();

    // All service getters/setters are delegated to ServiceController.
    // TBD: Use a DI framework such as Spring or Guice....
    private static ServiceController serviceController = new ServiceController(null, null, null, null, null, null, null, null, null, null, null, null, null, null);

    // Static methods only.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(serviceController.getApiConsumerService() == null) {
            serviceController.setApiConsumerService(serviceFactory.getApiConsumerService());
        }
        return serviceController.getApiConsumerService();
    }
    // Injects a ApiConsumerService instance.
	public static void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        serviceController.setApiConsumerService(apiConsumerService);
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(serviceController.getUserService() == null) {
            serviceController.setUserService(serviceFactory.getUserService());
        }
        return serviceController.getUserService();
    }
    // Injects a UserService instance.
	public static void setUserService(UserService userService) 
    {
        serviceController.setUserService(userService);
    }

    // Returns a AccessTallyMasterService instance.
	public static AccessTallyMasterService getAccessTallyMasterService() 
    {
        if(serviceController.getAccessTallyMasterService() == null) {
            serviceController.setAccessTallyMasterService(serviceFactory.getAccessTallyMasterService());
        }
        return serviceController.getAccessTallyMasterService();
    }
    // Injects a AccessTallyMasterService instance.
	public static void setAccessTallyMasterService(AccessTallyMasterService accessTallyMasterService) 
    {
        serviceController.setAccessTallyMasterService(accessTallyMasterService);
    }

    // Returns a AccessTallyStatusService instance.
	public static AccessTallyStatusService getAccessTallyStatusService() 
    {
        if(serviceController.getAccessTallyStatusService() == null) {
            serviceController.setAccessTallyStatusService(serviceFactory.getAccessTallyStatusService());
        }
        return serviceController.getAccessTallyStatusService();
    }
    // Injects a AccessTallyStatusService instance.
	public static void setAccessTallyStatusService(AccessTallyStatusService accessTallyStatusService) 
    {
        serviceController.setAccessTallyStatusService(accessTallyStatusService);
    }

    // Returns a MonthlyShortUrlAccessService instance.
	public static MonthlyShortUrlAccessService getMonthlyShortUrlAccessService() 
    {
        if(serviceController.getMonthlyShortUrlAccessService() == null) {
            serviceController.setMonthlyShortUrlAccessService(serviceFactory.getMonthlyShortUrlAccessService());
        }
        return serviceController.getMonthlyShortUrlAccessService();
    }
    // Injects a MonthlyShortUrlAccessService instance.
	public static void setMonthlyShortUrlAccessService(MonthlyShortUrlAccessService monthlyShortUrlAccessService) 
    {
        serviceController.setMonthlyShortUrlAccessService(monthlyShortUrlAccessService);
    }

    // Returns a WeeklyShortUrlAccessService instance.
	public static WeeklyShortUrlAccessService getWeeklyShortUrlAccessService() 
    {
        if(serviceController.getWeeklyShortUrlAccessService() == null) {
            serviceController.setWeeklyShortUrlAccessService(serviceFactory.getWeeklyShortUrlAccessService());
        }
        return serviceController.getWeeklyShortUrlAccessService();
    }
    // Injects a WeeklyShortUrlAccessService instance.
	public static void setWeeklyShortUrlAccessService(WeeklyShortUrlAccessService weeklyShortUrlAccessService) 
    {
        serviceController.setWeeklyShortUrlAccessService(weeklyShortUrlAccessService);
    }

    // Returns a DailyShortUrlAccessService instance.
	public static DailyShortUrlAccessService getDailyShortUrlAccessService() 
    {
        if(serviceController.getDailyShortUrlAccessService() == null) {
            serviceController.setDailyShortUrlAccessService(serviceFactory.getDailyShortUrlAccessService());
        }
        return serviceController.getDailyShortUrlAccessService();
    }
    // Injects a DailyShortUrlAccessService instance.
	public static void setDailyShortUrlAccessService(DailyShortUrlAccessService dailyShortUrlAccessService) 
    {
        serviceController.setDailyShortUrlAccessService(dailyShortUrlAccessService);
    }

    // Returns a HourlyShortUrlAccessService instance.
	public static HourlyShortUrlAccessService getHourlyShortUrlAccessService() 
    {
        if(serviceController.getHourlyShortUrlAccessService() == null) {
            serviceController.setHourlyShortUrlAccessService(serviceFactory.getHourlyShortUrlAccessService());
        }
        return serviceController.getHourlyShortUrlAccessService();
    }
    // Injects a HourlyShortUrlAccessService instance.
	public static void setHourlyShortUrlAccessService(HourlyShortUrlAccessService hourlyShortUrlAccessService) 
    {
        serviceController.setHourlyShortUrlAccessService(hourlyShortUrlAccessService);
    }

    // Returns a CumulativeShortUrlAccessService instance.
	public static CumulativeShortUrlAccessService getCumulativeShortUrlAccessService() 
    {
        if(serviceController.getCumulativeShortUrlAccessService() == null) {
            serviceController.setCumulativeShortUrlAccessService(serviceFactory.getCumulativeShortUrlAccessService());
        }
        return serviceController.getCumulativeShortUrlAccessService();
    }
    // Injects a CumulativeShortUrlAccessService instance.
	public static void setCumulativeShortUrlAccessService(CumulativeShortUrlAccessService cumulativeShortUrlAccessService) 
    {
        serviceController.setCumulativeShortUrlAccessService(cumulativeShortUrlAccessService);
    }

    // Returns a CurrentShortUrlAccessService instance.
	public static CurrentShortUrlAccessService getCurrentShortUrlAccessService() 
    {
        if(serviceController.getCurrentShortUrlAccessService() == null) {
            serviceController.setCurrentShortUrlAccessService(serviceFactory.getCurrentShortUrlAccessService());
        }
        return serviceController.getCurrentShortUrlAccessService();
    }
    // Injects a CurrentShortUrlAccessService instance.
	public static void setCurrentShortUrlAccessService(CurrentShortUrlAccessService currentShortUrlAccessService) 
    {
        serviceController.setCurrentShortUrlAccessService(currentShortUrlAccessService);
    }

    // Returns a TotalShortUrlAccessService instance.
	public static TotalShortUrlAccessService getTotalShortUrlAccessService() 
    {
        if(serviceController.getTotalShortUrlAccessService() == null) {
            serviceController.setTotalShortUrlAccessService(serviceFactory.getTotalShortUrlAccessService());
        }
        return serviceController.getTotalShortUrlAccessService();
    }
    // Injects a TotalShortUrlAccessService instance.
	public static void setTotalShortUrlAccessService(TotalShortUrlAccessService totalShortUrlAccessService) 
    {
        serviceController.setTotalShortUrlAccessService(totalShortUrlAccessService);
    }

    // Returns a TotalLongUrlAccessService instance.
	public static TotalLongUrlAccessService getTotalLongUrlAccessService() 
    {
        if(serviceController.getTotalLongUrlAccessService() == null) {
            serviceController.setTotalLongUrlAccessService(serviceFactory.getTotalLongUrlAccessService());
        }
        return serviceController.getTotalLongUrlAccessService();
    }
    // Injects a TotalLongUrlAccessService instance.
	public static void setTotalLongUrlAccessService(TotalLongUrlAccessService totalLongUrlAccessService) 
    {
        serviceController.setTotalLongUrlAccessService(totalLongUrlAccessService);
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(serviceController.getServiceInfoService() == null) {
            serviceController.setServiceInfoService(serviceFactory.getServiceInfoService());
        }
        return serviceController.getServiceInfoService();
    }
    // Injects a ServiceInfoService instance.
	public static void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        serviceController.setServiceInfoService(serviceInfoService);
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(serviceController.getFiveTenService() == null) {
            serviceController.setFiveTenService(serviceFactory.getFiveTenService());
        }
        return serviceController.getFiveTenService();
    }
    // Injects a FiveTenService instance.
	public static void setFiveTenService(FiveTenService fiveTenService) 
    {
        serviceController.setFiveTenService(fiveTenService);
    }

}
