package com.urltally.af.proxy.remote;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.ApiConsumerServiceProxy;
import com.urltally.af.proxy.UserServiceProxy;
import com.urltally.af.proxy.AccessTallyMasterServiceProxy;
import com.urltally.af.proxy.AccessTallyStatusServiceProxy;
import com.urltally.af.proxy.MonthlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.WeeklyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.DailyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.HourlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.CumulativeShortUrlAccessServiceProxy;
import com.urltally.af.proxy.CurrentShortUrlAccessServiceProxy;
import com.urltally.af.proxy.TotalShortUrlAccessServiceProxy;
import com.urltally.af.proxy.TotalLongUrlAccessServiceProxy;
import com.urltally.af.proxy.ServiceInfoServiceProxy;
import com.urltally.af.proxy.FiveTenServiceProxy;

public class RemoteProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(RemoteProxyFactory.class.getName());

    private RemoteProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class RemoteProxyFactoryHolder
    {
        private static final RemoteProxyFactory INSTANCE = new RemoteProxyFactory();
    }

    // Singleton method
    public static RemoteProxyFactory getInstance()
    {
        return RemoteProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new RemoteApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new RemoteUserServiceProxy();
    }

    @Override
    public AccessTallyMasterServiceProxy getAccessTallyMasterServiceProxy()
    {
        return new RemoteAccessTallyMasterServiceProxy();
    }

    @Override
    public AccessTallyStatusServiceProxy getAccessTallyStatusServiceProxy()
    {
        return new RemoteAccessTallyStatusServiceProxy();
    }

    @Override
    public MonthlyShortUrlAccessServiceProxy getMonthlyShortUrlAccessServiceProxy()
    {
        return new RemoteMonthlyShortUrlAccessServiceProxy();
    }

    @Override
    public WeeklyShortUrlAccessServiceProxy getWeeklyShortUrlAccessServiceProxy()
    {
        return new RemoteWeeklyShortUrlAccessServiceProxy();
    }

    @Override
    public DailyShortUrlAccessServiceProxy getDailyShortUrlAccessServiceProxy()
    {
        return new RemoteDailyShortUrlAccessServiceProxy();
    }

    @Override
    public HourlyShortUrlAccessServiceProxy getHourlyShortUrlAccessServiceProxy()
    {
        return new RemoteHourlyShortUrlAccessServiceProxy();
    }

    @Override
    public CumulativeShortUrlAccessServiceProxy getCumulativeShortUrlAccessServiceProxy()
    {
        return new RemoteCumulativeShortUrlAccessServiceProxy();
    }

    @Override
    public CurrentShortUrlAccessServiceProxy getCurrentShortUrlAccessServiceProxy()
    {
        return new RemoteCurrentShortUrlAccessServiceProxy();
    }

    @Override
    public TotalShortUrlAccessServiceProxy getTotalShortUrlAccessServiceProxy()
    {
        return new RemoteTotalShortUrlAccessServiceProxy();
    }

    @Override
    public TotalLongUrlAccessServiceProxy getTotalLongUrlAccessServiceProxy()
    {
        return new RemoteTotalLongUrlAccessServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new RemoteServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new RemoteFiveTenServiceProxy();
    }

}
