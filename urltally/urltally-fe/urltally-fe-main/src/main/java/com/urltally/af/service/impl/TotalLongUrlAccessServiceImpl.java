package com.urltally.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.TotalLongUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.TotalLongUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TotalLongUrlAccessServiceImpl implements TotalLongUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "TotalLongUrlAccess-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("TotalLongUrlAccess:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public TotalLongUrlAccessServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // TotalLongUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TotalLongUrlAccess getTotalLongUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTotalLongUrlAccess(): guid = " + guid);

        TotalLongUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (TotalLongUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TotalLongUrlAccessBean) getProxyFactory().getTotalLongUrlAccessServiceProxy().getTotalLongUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TotalLongUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccessBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTotalLongUrlAccess(String guid, String field) throws BaseException
    {
        TotalLongUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (TotalLongUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TotalLongUrlAccessBean) getProxyFactory().getTotalLongUrlAccessServiceProxy().getTotalLongUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TotalLongUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccessBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyType")) {
            return bean.getTallyType();
        } else if(field.equals("tallyTime")) {
            return bean.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return bean.getTallyEpoch();
        } else if(field.equals("count")) {
            return bean.getCount();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return bean.getLongUrlDomain();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TotalLongUrlAccess> getTotalLongUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getTotalLongUrlAccesses()");

        // TBD: Is there a better way????
        List<TotalLongUrlAccess> totalLongUrlAccesses = getProxyFactory().getTotalLongUrlAccessServiceProxy().getTotalLongUrlAccesses(guids);
        if(totalLongUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccessBean list.");
        }

        log.finer("END");
        return totalLongUrlAccesses;
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses() throws BaseException
    {
        return getAllTotalLongUrlAccesses(null, null, null);
    }


    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalLongUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccess> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalLongUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TotalLongUrlAccess> totalLongUrlAccesses = getProxyFactory().getTotalLongUrlAccessServiceProxy().getAllTotalLongUrlAccesses(ordering, offset, count, forwardCursor);
        if(totalLongUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccessBean list.");
        }

        log.finer("END");
        return totalLongUrlAccesses;
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalLongUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalLongUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTotalLongUrlAccessServiceProxy().getAllTotalLongUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalLongUrlAccessBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TotalLongUrlAccessBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalLongUrlAccess> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalLongUrlAccessServiceImpl.findTotalLongUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TotalLongUrlAccess> totalLongUrlAccesses = getProxyFactory().getTotalLongUrlAccessServiceProxy().findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(totalLongUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to find totalLongUrlAccesses for the given criterion.");
        }

        log.finer("END");
        return totalLongUrlAccesses;
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalLongUrlAccessServiceImpl.findTotalLongUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTotalLongUrlAccessServiceProxy().findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TotalLongUrlAccess keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TotalLongUrlAccess key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalLongUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getTotalLongUrlAccessServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTotalLongUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        TotalLongUrlAccessBean bean = new TotalLongUrlAccessBean(null, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
        return createTotalLongUrlAccess(bean);
    }

    @Override
    public String createTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TotalLongUrlAccess bean = constructTotalLongUrlAccess(totalLongUrlAccess);
        //return bean.getGuid();

        // Param totalLongUrlAccess cannot be null.....
        if(totalLongUrlAccess == null) {
            log.log(Level.INFO, "Param totalLongUrlAccess is null!");
            throw new BadRequestException("Param totalLongUrlAccess object is null!");
        }
        TotalLongUrlAccessBean bean = null;
        if(totalLongUrlAccess instanceof TotalLongUrlAccessBean) {
            bean = (TotalLongUrlAccessBean) totalLongUrlAccess;
        } else if(totalLongUrlAccess instanceof TotalLongUrlAccess) {
            // bean = new TotalLongUrlAccessBean(null, totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TotalLongUrlAccessBean(totalLongUrlAccess.getGuid(), totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
        } else {
            log.log(Level.WARNING, "createTotalLongUrlAccess(): Arg totalLongUrlAccess is of an unknown type.");
            //bean = new TotalLongUrlAccessBean();
            bean = new TotalLongUrlAccessBean(totalLongUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTotalLongUrlAccessServiceProxy().createTotalLongUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TotalLongUrlAccess constructTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalLongUrlAccess cannot be null.....
        if(totalLongUrlAccess == null) {
            log.log(Level.INFO, "Param totalLongUrlAccess is null!");
            throw new BadRequestException("Param totalLongUrlAccess object is null!");
        }
        TotalLongUrlAccessBean bean = null;
        if(totalLongUrlAccess instanceof TotalLongUrlAccessBean) {
            bean = (TotalLongUrlAccessBean) totalLongUrlAccess;
        } else if(totalLongUrlAccess instanceof TotalLongUrlAccess) {
            // bean = new TotalLongUrlAccessBean(null, totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TotalLongUrlAccessBean(totalLongUrlAccess.getGuid(), totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
        } else {
            log.log(Level.WARNING, "createTotalLongUrlAccess(): Arg totalLongUrlAccess is of an unknown type.");
            //bean = new TotalLongUrlAccessBean();
            bean = new TotalLongUrlAccessBean(totalLongUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTotalLongUrlAccessServiceProxy().createTotalLongUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTotalLongUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TotalLongUrlAccessBean bean = new TotalLongUrlAccessBean(guid, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
        return updateTotalLongUrlAccess(bean);
    }
        
    @Override
    public Boolean updateTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TotalLongUrlAccess bean = refreshTotalLongUrlAccess(totalLongUrlAccess);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param totalLongUrlAccess cannot be null.....
        if(totalLongUrlAccess == null || totalLongUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalLongUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalLongUrlAccess object or its guid is null!");
        }
        TotalLongUrlAccessBean bean = null;
        if(totalLongUrlAccess instanceof TotalLongUrlAccessBean) {
            bean = (TotalLongUrlAccessBean) totalLongUrlAccess;
        } else {  // if(totalLongUrlAccess instanceof TotalLongUrlAccess)
            bean = new TotalLongUrlAccessBean(totalLongUrlAccess.getGuid(), totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
        }
        Boolean suc = getProxyFactory().getTotalLongUrlAccessServiceProxy().updateTotalLongUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TotalLongUrlAccess refreshTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalLongUrlAccess cannot be null.....
        if(totalLongUrlAccess == null || totalLongUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalLongUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalLongUrlAccess object or its guid is null!");
        }
        TotalLongUrlAccessBean bean = null;
        if(totalLongUrlAccess instanceof TotalLongUrlAccessBean) {
            bean = (TotalLongUrlAccessBean) totalLongUrlAccess;
        } else {  // if(totalLongUrlAccess instanceof TotalLongUrlAccess)
            bean = new TotalLongUrlAccessBean(totalLongUrlAccess.getGuid(), totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
        }
        Boolean suc = getProxyFactory().getTotalLongUrlAccessServiceProxy().updateTotalLongUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTotalLongUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getTotalLongUrlAccessServiceProxy().deleteTotalLongUrlAccess(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            TotalLongUrlAccess totalLongUrlAccess = null;
            try {
                totalLongUrlAccess = getProxyFactory().getTotalLongUrlAccessServiceProxy().getTotalLongUrlAccess(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch totalLongUrlAccess with a key, " + guid);
                return false;
            }
            if(totalLongUrlAccess != null) {
                String beanGuid = totalLongUrlAccess.getGuid();
                Boolean suc1 = getProxyFactory().getTotalLongUrlAccessServiceProxy().deleteTotalLongUrlAccess(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("totalLongUrlAccess with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalLongUrlAccess cannot be null.....
        if(totalLongUrlAccess == null || totalLongUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalLongUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalLongUrlAccess object or its guid is null!");
        }
        TotalLongUrlAccessBean bean = null;
        if(totalLongUrlAccess instanceof TotalLongUrlAccessBean) {
            bean = (TotalLongUrlAccessBean) totalLongUrlAccess;
        } else {  // if(totalLongUrlAccess instanceof TotalLongUrlAccess)
            // ????
            log.warning("totalLongUrlAccess is not an instance of TotalLongUrlAccessBean.");
            bean = new TotalLongUrlAccessBean(totalLongUrlAccess.getGuid(), totalLongUrlAccess.getTallyType(), totalLongUrlAccess.getTallyTime(), totalLongUrlAccess.getTallyEpoch(), totalLongUrlAccess.getCount(), totalLongUrlAccess.getLongUrl(), totalLongUrlAccess.getLongUrlDomain());
        }
        Boolean suc = getProxyFactory().getTotalLongUrlAccessServiceProxy().deleteTotalLongUrlAccess(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getTotalLongUrlAccessServiceProxy().deleteTotalLongUrlAccesses(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTotalLongUrlAccesses(List<TotalLongUrlAccess> totalLongUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");

        if(totalLongUrlAccesses == null) {
            log.log(Level.WARNING, "createTotalLongUrlAccesses() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = totalLongUrlAccesses.size();
        if(size == 0) {
            log.log(Level.WARNING, "createTotalLongUrlAccesses() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(TotalLongUrlAccess totalLongUrlAccess : totalLongUrlAccesses) {
            String guid = createTotalLongUrlAccess(totalLongUrlAccess);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createTotalLongUrlAccesses() failed for at least one totalLongUrlAccess. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTotalLongUrlAccesses(List<TotalLongUrlAccess> totalLongUrlAccesses) throws BaseException
    //{
    //}

}
