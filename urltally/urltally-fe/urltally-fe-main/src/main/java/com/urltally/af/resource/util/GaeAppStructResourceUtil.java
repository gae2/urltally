package com.urltally.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CommonConstants;
import com.urltally.ws.GaeAppStruct;
import com.urltally.ws.stub.GaeAppStructStub;
import com.urltally.af.bean.GaeAppStructBean;


public class GaeAppStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GaeAppStructResourceUtil.class.getName());

    // Static methods only.
    private GaeAppStructResourceUtil() {}

    public static GaeAppStructBean convertGaeAppStructStubToBean(GaeAppStruct stub)
    {
        GaeAppStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new GaeAppStructBean();
            bean.setGroupId(stub.getGroupId());
            bean.setAppId(stub.getAppId());
            bean.setAppDomain(stub.getAppDomain());
            bean.setNamespace(stub.getNamespace());
            bean.setAcl(stub.getAcl());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
