package com.urltally.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.stub.ShortUrlAccessStub;
import com.urltally.ws.stub.HourlyShortUrlAccessStub;


// Wrapper class + bean combo.
public class HourlyShortUrlAccessBean extends ShortUrlAccessBean implements HourlyShortUrlAccess, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(HourlyShortUrlAccessBean.class.getName());


    // [2] Or, without an embedded object.
    private Integer year;
    private Integer day;
    private Integer hour;

    // Ctors.
    public HourlyShortUrlAccessBean()
    {
        //this((String) null);
    }
    public HourlyShortUrlAccessBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public HourlyShortUrlAccessBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour)
    {
        this(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour, null, null);
    }
    public HourlyShortUrlAccessBean(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour, Long createdTime, Long modifiedTime)
    {
        super(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, createdTime, modifiedTime);

        this.year = year;
        this.day = day;
        this.hour = hour;
    }
    public HourlyShortUrlAccessBean(HourlyShortUrlAccess stub)
    {
        if(stub instanceof HourlyShortUrlAccessStub) {
            super.setStub((ShortUrlAccessStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setTallyTime(stub.getTallyTime());   
            setTallyEpoch(stub.getTallyEpoch());   
            setCount(stub.getCount());   
            setShortUrl(stub.getShortUrl());   
            setShortUrlDomain(stub.getShortUrlDomain());   
            setLongUrl(stub.getLongUrl());   
            setLongUrlDomain(stub.getLongUrlDomain());   
            setRedirectType(stub.getRedirectType());   
            setRefererDomain(stub.getRefererDomain());   
            setUserAgent(stub.getUserAgent());   
            setLanguage(stub.getLanguage());   
            setCountry(stub.getCountry());   
            setTalliedTime(stub.getTalliedTime());   
            setYear(stub.getYear());   
            setDay(stub.getDay());   
            setHour(stub.getHour());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getTallyTime()
    {
        return super.getTallyTime();
    }
    public void setTallyTime(String tallyTime)
    {
        super.setTallyTime(tallyTime);
    }

    public Long getTallyEpoch()
    {
        return super.getTallyEpoch();
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        super.setTallyEpoch(tallyEpoch);
    }

    public Integer getCount()
    {
        return super.getCount();
    }
    public void setCount(Integer count)
    {
        super.setCount(count);
    }

    public String getShortUrl()
    {
        return super.getShortUrl();
    }
    public void setShortUrl(String shortUrl)
    {
        super.setShortUrl(shortUrl);
    }

    public String getShortUrlDomain()
    {
        return super.getShortUrlDomain();
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        super.setShortUrlDomain(shortUrlDomain);
    }

    public String getLongUrl()
    {
        return super.getLongUrl();
    }
    public void setLongUrl(String longUrl)
    {
        super.setLongUrl(longUrl);
    }

    public String getLongUrlDomain()
    {
        return super.getLongUrlDomain();
    }
    public void setLongUrlDomain(String longUrlDomain)
    {
        super.setLongUrlDomain(longUrlDomain);
    }

    public String getRedirectType()
    {
        return super.getRedirectType();
    }
    public void setRedirectType(String redirectType)
    {
        super.setRedirectType(redirectType);
    }

    public String getRefererDomain()
    {
        return super.getRefererDomain();
    }
    public void setRefererDomain(String refererDomain)
    {
        super.setRefererDomain(refererDomain);
    }

    public String getUserAgent()
    {
        return super.getUserAgent();
    }
    public void setUserAgent(String userAgent)
    {
        super.setUserAgent(userAgent);
    }

    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    public String getCountry()
    {
        return super.getCountry();
    }
    public void setCountry(String country)
    {
        super.setCountry(country);
    }

    public Long getTalliedTime()
    {
        return super.getTalliedTime();
    }
    public void setTalliedTime(Long talliedTime)
    {
        super.setTalliedTime(talliedTime);
    }

    public Integer getYear()
    {
        if(getStub() != null) {
            return getStub().getYear();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.year;
        }
    }
    public void setYear(Integer year)
    {
        if(getStub() != null) {
            getStub().setYear(year);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.year = year;
        }
    }

    public Integer getDay()
    {
        if(getStub() != null) {
            return getStub().getDay();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.day;
        }
    }
    public void setDay(Integer day)
    {
        if(getStub() != null) {
            getStub().setDay(day);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.day = day;
        }
    }

    public Integer getHour()
    {
        if(getStub() != null) {
            return getStub().getHour();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.hour;
        }
    }
    public void setHour(Integer hour)
    {
        if(getStub() != null) {
            getStub().setHour(hour);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.hour = hour;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public HourlyShortUrlAccessStub getStub()
    {
        return (HourlyShortUrlAccessStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("year = " + this.year).append(";");
            sb.append("day = " + this.day).append(";");
            sb.append("hour = " + this.hour).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = year == null ? 0 : year.hashCode();
            _hash = 31 * _hash + delta;
            delta = day == null ? 0 : day.hashCode();
            _hash = 31 * _hash + delta;
            delta = hour == null ? 0 : hour.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
