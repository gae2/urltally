package com.urltally.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.TotalShortUrlAccessService;


// TotalShortUrlAccessMockService is a decorator.
// It can be used as a base class to mock TotalShortUrlAccessService objects.
public abstract class TotalShortUrlAccessMockService implements TotalShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessMockService.class.getName());

    // TotalShortUrlAccessMockService uses the decorator design pattern.
    private TotalShortUrlAccessService decoratedService;

    public TotalShortUrlAccessMockService(TotalShortUrlAccessService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected TotalShortUrlAccessService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(TotalShortUrlAccessService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // TotalShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TotalShortUrlAccess getTotalShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTotalShortUrlAccess(): guid = " + guid);
        TotalShortUrlAccess bean = decoratedService.getTotalShortUrlAccess(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getTotalShortUrlAccess(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getTotalShortUrlAccess(guid, field);
        return obj;
    }

    @Override
    public List<TotalShortUrlAccess> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getTotalShortUrlAccesses()");
        List<TotalShortUrlAccess> totalShortUrlAccesses = decoratedService.getTotalShortUrlAccesses(guids);
        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses() throws BaseException
    {
        return getAllTotalShortUrlAccesses(null, null, null);
    }


    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<TotalShortUrlAccess> totalShortUrlAccesses = decoratedService.getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessMockService.findTotalShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<TotalShortUrlAccess> totalShortUrlAccesses = decoratedService.findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessMockService.findTotalShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        TotalShortUrlAccessBean bean = new TotalShortUrlAccessBean(null, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        return createTotalShortUrlAccess(bean);
    }

    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createTotalShortUrlAccess(totalShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TotalShortUrlAccess constructTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        TotalShortUrlAccess bean = decoratedService.constructTotalShortUrlAccess(totalShortUrlAccess);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TotalShortUrlAccessBean bean = new TotalShortUrlAccessBean(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        return updateTotalShortUrlAccess(bean);
    }
        
    @Override
    public Boolean updateTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateTotalShortUrlAccess(totalShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TotalShortUrlAccess refreshTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        TotalShortUrlAccess bean = decoratedService.refreshTotalShortUrlAccess(totalShortUrlAccess);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTotalShortUrlAccess(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteTotalShortUrlAccess(totalShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteTotalShortUrlAccesses(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createTotalShortUrlAccesses(totalShortUrlAccesses);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException
    //{
    //}

}
