package com.urltally.af.service;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalShortUrlAccess;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface TotalShortUrlAccessService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    TotalShortUrlAccess getTotalShortUrlAccess(String guid) throws BaseException;
    Object getTotalShortUrlAccess(String guid, String field) throws BaseException;
    List<TotalShortUrlAccess> getTotalShortUrlAccesses(List<String> guids) throws BaseException;
    List<TotalShortUrlAccess> getAllTotalShortUrlAccesses() throws BaseException;
    /* @Deprecated */ List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException;
    List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException;
    //String createTotalShortUrlAccess(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TotalShortUrlAccess?)
    String createTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException;
    TotalShortUrlAccess constructTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException;
    Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException;
    //Boolean updateTotalShortUrlAccess(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException;
    TotalShortUrlAccess refreshTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException;
    Boolean deleteTotalShortUrlAccess(String guid) throws BaseException;
    Boolean deleteTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException;
    Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException;
//    Boolean updateTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException;

}
