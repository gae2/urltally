package com.urltally.app.tally;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ingressdb.ws.AccessRecord;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.af.bean.CumulativeShortUrlAccessBean;
import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.app.helper.AccessTallyAccessHelper;
import com.urltally.app.helper.IngressDBAccessHelper;
import com.urltally.app.helper.ShortUrlAccessHelper;
import com.urltally.app.helper.TotalShortUrlTallyHelper;
import com.urltally.app.util.ConfigUtil;
import com.urltally.app.util.TallyTimeUtil;
import com.urltally.common.TallyStatus;
import com.urltally.common.TallyType;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.ShortUrlAccess;
import com.urltally.ws.core.GUID;


// Tally OAuth Manager
// https://dev.twitter.com/docs/auth
// http://twitter4j.org/en/index.html
public class TallyManager
{
    private static final Logger log = Logger.getLogger(TallyManager.class.getName());


    
    private TallyManager()
    {
        // TBD:
        //initTallyFactory();
    }

    // Initialization-on-demand holder.
    private static class TallyManagerHolder
    {
        private static final TallyManager INSTANCE = new TallyManager();
    }

    // Singleton method
    public static TallyManager getInstance()
    {
        return TallyManagerHolder.INSTANCE;
    }
    
    

    public AccessTallyMaster getActiveAccessTallyMaster(String tallyType)
    {
        String dayHour = null;
        String tallyStatus = null;
        AccessTallyMaster bean = AccessTallyAccessHelper.getInstance().getLastAccessTallyMaster(tallyType);
        if(bean == null) {
            // First time???
            // create a new one.
            String now = TallyTimeUtil.getTallyTime(tallyType);
            dayHour = TallyTimeUtil.getPrevious(tallyType, now);   // Just start from yesterday/one-hour ago/... etc.
        } else {
            String status = bean.getTallyStatus();
            if(TallyStatus.STATUS_COMPLETED.equals(status)) {
                // create a new one.
                String last = bean.getTallyTime();
                dayHour = TallyTimeUtil.getNext(tallyType, last);
                bean = null;
            }
        }
        
        if(bean == null) {
            // dayHour cannot be null.
            bean = AccessTallyAccessHelper.getInstance().constructNewAccessTallyMaster(tallyType, dayHour);
            if(bean != null) {
                log.info("New AccessTallyMaster has been created for tallyType = " + tallyType + "; dayHour = " + dayHour);
            } else {
                // What to do???
                log.warning("Failed to create a new AccessTallyMaster for tallyType = " + tallyType + "; dayHour = " + dayHour);
            }
        }

        return bean;
    }
    
    
    // Note that none of these methods are "atomic"..
    // Meaning there is always a risk of multiple processes updating the (same) tally masters at the same time
    //   (not just the same record, but even different records which are conceptually the same (e.g., with the same tallyType and tallyTime, etc...)
    
    public AccessTallyMaster markAccessTallyMasterAsPrepared(AccessTallyMaster master)
    {
        ((AccessTallyMasterBean) master).setTallyStatus(TallyStatus.STATUS_PREPARED);
        master = AccessTallyAccessHelper.getInstance().refreshAccessTallyMaster(master);
        return master;
    }
    
    public AccessTallyMaster markAccessTallyMasterAsStarted(AccessTallyMaster master)
    {
        return markAccessTallyMasterAsStarted(master, 0);
    }
    public AccessTallyMaster markAccessTallyMasterAsStarted(AccessTallyMaster master, int accessRecordCount)
    {
        ((AccessTallyMasterBean) master).setTallyStatus(TallyStatus.STATUS_STARTED);
        ((AccessTallyMasterBean) master).setAccessRecordCount(accessRecordCount);
        master = AccessTallyAccessHelper.getInstance().refreshAccessTallyMaster(master);
        return master;
    }
    
    public AccessTallyMaster markAccessTallyMasterAsProcessing(AccessTallyMaster master)
    {
        return markAccessTallyMasterAsProcessing(master, 0);
    }
    public AccessTallyMaster markAccessTallyMasterAsProcessing(AccessTallyMaster master, int accessRecordCount)
    {
        ((AccessTallyMasterBean) master).setTallyStatus(TallyStatus.STATUS_PROCESSING);
        ((AccessTallyMasterBean) master).setProcesingStartedTime(0L);
        ((AccessTallyMasterBean) master).setAccessRecordCount(accessRecordCount);
        master = AccessTallyAccessHelper.getInstance().refreshAccessTallyMaster(master);
        return master;
    }
    
    public AccessTallyMaster markAccessTallyMasterAsProcessingStarted(AccessTallyMaster master)
    {
        return markAccessTallyMasterAsProcessingStarted(master, System.currentTimeMillis());
    }
    public AccessTallyMaster markAccessTallyMasterAsProcessingStarted(AccessTallyMaster master, long processingStartedTime)
    {
        ((AccessTallyMasterBean) master).setTallyStatus(TallyStatus.STATUS_PROCESSING);
        ((AccessTallyMasterBean) master).setProcesingStartedTime(processingStartedTime);
        master = AccessTallyAccessHelper.getInstance().refreshAccessTallyMaster(master);
        return master;
    }
    
    public AccessTallyMaster markAccessTallyMasterAsCompleted(AccessTallyMaster master)
    {
        ((AccessTallyMasterBean) master).setTallyStatus(TallyStatus.STATUS_COMPLETED);
        ((AccessTallyMasterBean) master).setProcesingStartedTime(0L);
        master = AccessTallyAccessHelper.getInstance().refreshAccessTallyMaster(master);
        return master;
    }


    public boolean isReadyForProcessing(AccessTallyMaster master)
    {
        if(master == null) {
            return false;
        }
        
        String tallyStatus = master.getTallyStatus();
        if(! TallyStatus.isValid(tallyStatus) || tallyStatus.equals(TallyStatus.STATUS_COMPLETED)) {
            return false;
        }

        String tallyType = master.getTallyType();
        if(! TallyType.isValid(tallyType)) {
            return false;
        }
        
        // TBD: Does this make sense????
        if(tallyType.equals(TallyType.TALLY_CURRENT)) {
            return true;   // ????
        }
        
        String tallyTime = master.getTallyTime();
        if(tallyTime == null || tallyTime.isEmpty()) {
            return false;
        }
        
        // TBD: Already processing.....
        if(tallyStatus.equals(TallyStatus.STATUS_STARTED) || tallyStatus.equals(TallyStatus.STATUS_PROCESSING)) {
            return true;   // ????
        }

        // At this point tallyStatus == TallyStatus.STATUS_INITIALIZED
        long now = System.currentTimeMillis() - 5 * 60 * 1000L;  // Five minute buffer...  (if we start processing too early, we may miss some access records (due to delayed saving on IngressDB)...)
        int cmp = TallyTimeUtil.compareTime(tallyType, now, tallyTime);
        if(cmp > 0) {    // the end of the period is before "now"
            return true;
        } else {
            return false;
        }
    }

    
    
    public int buildAccessTallyStatusList(String tallyType, String tallyTime)
    {
        return buildAccessTallyStatusList(tallyType, tallyTime, 0L, null);
    }
    public int buildAccessTallyStatusList(String tallyType, String tallyTime, long offset, Integer maxCount)
    {
        long[] milliRange = TallyTimeUtil.getMilliRange(tallyType, tallyTime);
        
        // TBD:
        // The time range should be divided into multiple segments
        //    to avoid errors due to timeout or too many records, etc.....
        // And/Or, divide them based on domain/hostname, etc. ???
        // ....
                

        //long offset = 0L;
        //if(maxCount == null) {
        //    maxCount = ConfigUtil.getTallyProcessingMaxCount();
        //}
        long startTime = milliRange[0];
        long endTime = milliRange[1];
        List<AccessRecord> beans = IngressDBAccessHelper.getInstance().getAccessRecords(startTime, endTime, offset, maxCount);  // Access records need to be sorted in order to be able to support "chunk" fetch..
        // ....
        
        // ????
        if(beans == null) {
            return -1;
        }
        
        int beanCount = beans.size();
        if(maxCount != null && beanCount == maxCount) {
            // ???
            log.severe("Failed to retrieve all AccessTallyStatus list. beanCount = " + beanCount);
            // ...
        }

        // Hack!!!!
        //Set<String> shortUrlSet = new HashSet<String>();
        Set<String> remoteRecordGuidSet = new HashSet<String>();

        List<AccessTallyStatus> statusList = new ArrayList<AccessTallyStatus>();
        for(AccessRecord bean : beans) {
            AccessTallyStatusBean status = new AccessTallyStatusBean();
            String remoteRecordGuid = bean.getGuid();
//            String shortUrl = bean.getRequestUrl();
            String shortUrl = bean.getCustomField1();   // CannyURL app customField1 == shortUrl
              // This does not make sense since we are using customField1 only (not requireUri) when querying IngressDB... cf. IngressDBAccessHelper...
//            if(shortUrl == null || shortUrl.isEmpty()) {
//                shortUrl = bean.getRequestUrl();
//            }
            //if(shortUrlSet.contains(shortUrl)) {   // This is wrong.... Access record table can contain multiple records for a given shortUrl... 
            if(remoteRecordGuidSet.contains(remoteRecordGuid)) {   // This check is probably not necessary 
                // skip
                log.warning("remoteRecordGuid already found: " + remoteRecordGuid);
            } else {
                if(shortUrl != null && !shortUrl.isEmpty()) {
                	status.setGuid(GUID.generate());  // ???
                    status.setShortUrl(shortUrl);
                    status.setRemoteRecordGuid(remoteRecordGuid);   // Newly added...
                    status.setProcessed(false);
                    status.setTallyType(tallyType);
                    status.setTallyTime(tallyTime);
                    status.setTallyEpoch(TallyTimeUtil.getMilli(tallyTime));
                    statusList.add(status);
                    //shortUrlSet.add(shortUrl);
                    remoteRecordGuidSet.add(remoteRecordGuid);
                    log.fine("A new AccessTallyStatus created with shortUrl = " + shortUrl + "; remoteRecordGuid = " + remoteRecordGuid);
                } else {
                    log.warning("shortUrl is missing. Skipping this record. AccessRecord guid = " + remoteRecordGuid);
                }
            }
        }

        // Add it to the UrlTally DB        
        // TBD: What happens if the insertion fails???
        int count = AccessTallyAccessHelper.getInstance().createAccessTallyStatuses(statusList, true);   // Do not save duplicate records...
        if(count != beanCount) {
            // ???
            log.warning("Failed to save all AccessTallyStatus list. Saved count = " + count + "; beanCount = " + beanCount);
        }

        // temporary
        return count;
    }
    

    
    public boolean processAccessTally(AccessTallyStatus bean)
    {
        log.finer("BEGIN: processAccessTally()");
        //log.warning("BEGIN: processAccessTally()");

        // ???
        if(bean == null) {
            return false;
        }
        
        String shortUrl = bean.getShortUrl();
        if(shortUrl == null || shortUrl.isEmpty()) {
            // ERROR!!!
            log.warning("processAccessTally(): AccessTallyStatus's shortUrl is not set. Cannot proceed.");
            return false;
        }
        
        String tallyType = bean.getTallyType();
        String tallyTime = bean.getTallyTime();
        long[] milliRange = TallyTimeUtil.getMilliRange(tallyType, tallyTime);
        long startTime = milliRange[0];
        long endTime = milliRange[1];
        int maxCount = ConfigUtil.getTallyProcessingMaxCount();  // ????

        List<AccessRecord> records = IngressDBAccessHelper.getInstance().getAccessRecords(shortUrl, startTime, endTime, maxCount);
        if(records == null) {
            // ???
            log.warning("Failed to retrieve AccessTallyStatus list.");
            return false;
        }
        
        int size = records.size();
        if(size == maxCount) {
            // ???
            log.warning("Failed to retrieve all AccessTallyStatus list for shortUrl: " + shortUrl + ". Size = " + size);
        }
        
        
        // TBD:
        if(size == 0) {
            // All processed ???
            // ???
        }
        
        Map<AccessTallyKey, ShortUrlAccess> accessMap = new HashMap<AccessTallyKey, ShortUrlAccess>();
        for(AccessRecord record : records) {
            
            String referer = record.getReferer();
            String refererDomain = null;
            if(referer != null && !referer.isEmpty()) {
                refererDomain = getDomainFromUrl(referer);
            }
            String userAgent = record.getUserAgent();
            String country = record.getCountry();
            
            String redirectType = null;
            String longUrl = null;
            List<String> attributes = record.getAttributes();
            if(attributes != null && !attributes.isEmpty()) {
                for(String a : attributes) {
                    if(a.startsWith("redirectType:")) {
                        redirectType = a.substring(13).trim();
                    } else if(a.startsWith("longUrl:")) {
                        longUrl = a.substring(8).trim();
                    }
                }
            }
            
            String language = null;
            List<String> extras = record.getExtras();
            if(extras != null && !extras.isEmpty()) {
                for(String e : extras) {
                    if(e.startsWith("acceptLanguage:")) {
                        language = e.substring(15).trim();
                    }
                }
            }

            
            AccessTallyKey key = new AccessTallyKey(redirectType, refererDomain, userAgent, language, country);
            if(longUrl != null) {
                key.setLongUrl(longUrl);
            }
            ShortUrlAccess access = null;
            if(accessMap.containsKey(key)) {
                // increase the counter...
                access = accessMap.get(key);
                access = incrementAccessCount(access);
                //accessMap.put(key, access);
            } else {
                // create a new one
                access = buildNewShortUrlAccess(tallyType, tallyTime, shortUrl, key);
                accessMap.put(key, access);
            }
            
            
            // TBD:
            // Shorturl, shorturldomain,
            // longurl, longurldomain,
            // ...
            
        }
        
        
        List<ShortUrlAccess> accessList = new ArrayList<ShortUrlAccess>(accessMap.values());
        if(log.isLoggable(Level.FINE)) {
            for(ShortUrlAccess a : accessList) {
                log.fine(">>>>>>>>>>>>>>>>>>  accessList: shortUrl = " + a.getShortUrl());
                log.fine("      ShortUrlAccess = " + a.toString());
            }            
        }
        
        // Save them.
        int count = ShortUrlAccessHelper.getInstance().createShortUrlAccesses(tallyType, accessList);
    
        
        // "Total"
        int total = 0;
        for(ShortUrlAccess a : accessList) {
            total += a.getCount();
        }
        TotalShortUrlAccessBean totalBean = new TotalShortUrlAccessBean();
        totalBean.setCount(total);
        totalBean.setShortUrl(shortUrl);
        totalBean.setShortUrlDomain(getDomainFromUrl(shortUrl));
        totalBean.setTallyTime(tallyTime);
        totalBean.setTallyType(tallyType);
        TotalShortUrlTallyHelper.getInstance().createTotalShortUrlAccess(totalBean);
        
        
        
        // TBD:
        // How to get the "total tally" for the longUrl
        // It's not easy to get the sum over longUrl in the current algorithm...
        // ....
        
        
        
        // ...
        ((AccessTallyStatusBean) bean).setProcessed(true);
        AccessTallyAccessHelper.getInstance().updateAccessTallyStatus(bean);
        
        
        // ???
        if(count >= 0) {
            return true;
        } else {
            return false;
        }
    }

    
    
    // temporary
    // TBD: Include "user domain" ??/
    private static String getDomainFromUrl(String url)
    {
        if(url == null || url.isEmpty()) {
            return url;
        }
        String domain = null;
        int slashslash = url.indexOf("//") + 2;
        if(slashslash > 0) {
            domain = url.substring(0, url.indexOf('/', slashslash) + 1);  // Include the trailing slash.
        } // else ????
        return domain;
    }
    
    // Temporary
    private static ShortUrlAccess buildNewShortUrlAccess(String tallyType, String tallyTime, String shortUrl, AccessTallyKey tallyKey)
    {
        ShortUrlAccess bean = null;
        if(TallyType.TALLY_MONTHLY.equals(tallyType)) {
            bean = new MonthlyShortUrlAccessBean();
            if(tallyKey != null) {
                MonthlyShortUrlAccessBean b = (MonthlyShortUrlAccessBean) bean;
                b.setTallyTime(tallyTime);
                b.setCount(1);
                b.setShortUrl(shortUrl);
                b.setShortUrlDomain(getDomainFromUrl(shortUrl));
                b.setLongUrl(tallyKey.getLongUrl());
                b.setLongUrlDomain(getDomainFromUrl(tallyKey.getLongUrl()));
                b.setRedirectType(tallyKey.getRedirectType());
                b.setRefererDomain(tallyKey.getRefererDomain());
                b.setUserAgent(tallyKey.getUserAgent());
                b.setLanguage(tallyKey.getLanguage());
                b.setCountry(tallyKey.getCountry());
                //bean = b;
            }
        } else if(TallyType.TALLY_WEEKLY.equals(tallyType)) {
            bean = new WeeklyShortUrlAccessBean();
            if(tallyKey != null) {
                WeeklyShortUrlAccessBean b = (WeeklyShortUrlAccessBean) bean;
                b.setTallyTime(tallyTime);
                b.setCount(1);
                b.setShortUrl(shortUrl);
                b.setShortUrlDomain(getDomainFromUrl(shortUrl));
                b.setLongUrl(tallyKey.getLongUrl());
                b.setLongUrlDomain(getDomainFromUrl(tallyKey.getLongUrl()));
                b.setRedirectType(tallyKey.getRedirectType());
                b.setRefererDomain(tallyKey.getRefererDomain());
                b.setUserAgent(tallyKey.getUserAgent());
                b.setLanguage(tallyKey.getLanguage());
                b.setCountry(tallyKey.getCountry());
                //bean = b;
            }
        } else if(TallyType.TALLY_DAILY.equals(tallyType)) {
            bean = new DailyShortUrlAccessBean();
            if(tallyKey != null) {
                DailyShortUrlAccessBean b = (DailyShortUrlAccessBean) bean;
                b.setTallyTime(tallyTime);
                b.setCount(1);
                b.setShortUrl(shortUrl);
                b.setShortUrlDomain(getDomainFromUrl(shortUrl));
                b.setLongUrl(tallyKey.getLongUrl());
                b.setLongUrlDomain(getDomainFromUrl(tallyKey.getLongUrl()));
                b.setRedirectType(tallyKey.getRedirectType());
                b.setRefererDomain(tallyKey.getRefererDomain());
                b.setUserAgent(tallyKey.getUserAgent());
                b.setLanguage(tallyKey.getLanguage());
                b.setCountry(tallyKey.getCountry());
                //bean = b;
            }
        } else if(TallyType.TALLY_HOURLY.equals(tallyType)) {
            bean = new HourlyShortUrlAccessBean();
            if(tallyKey != null) {
                HourlyShortUrlAccessBean b = (HourlyShortUrlAccessBean) bean;
                b.setTallyTime(tallyTime);
                b.setCount(1);
                b.setShortUrl(shortUrl);
                b.setShortUrlDomain(getDomainFromUrl(shortUrl));
                b.setLongUrl(tallyKey.getLongUrl());
                b.setLongUrlDomain(getDomainFromUrl(tallyKey.getLongUrl()));
                b.setRedirectType(tallyKey.getRedirectType());
                b.setRefererDomain(tallyKey.getRefererDomain());
                b.setUserAgent(tallyKey.getUserAgent());
                b.setLanguage(tallyKey.getLanguage());
                b.setCountry(tallyKey.getCountry());
                //bean = b;
            }
        } else if(TallyType.TALLY_CUMULATIVE.equals(tallyType)) {
            bean = new CumulativeShortUrlAccessBean();
            if(tallyKey != null) {
                CumulativeShortUrlAccessBean b = (CumulativeShortUrlAccessBean) bean;
                b.setTallyTime(tallyTime);
                b.setCount(1);
                b.setShortUrl(shortUrl);
                b.setShortUrlDomain(getDomainFromUrl(shortUrl));
                b.setLongUrl(tallyKey.getLongUrl());
                b.setLongUrlDomain(getDomainFromUrl(tallyKey.getLongUrl()));
                b.setRedirectType(tallyKey.getRedirectType());
                b.setRefererDomain(tallyKey.getRefererDomain());
                b.setUserAgent(tallyKey.getUserAgent());
                b.setLanguage(tallyKey.getLanguage());
                b.setCountry(tallyKey.getCountry());
                //bean = b;
            }
        } else if(TallyType.TALLY_CURRENT.equals(tallyType)) {
            bean = new CurrentShortUrlAccessBean();
            if(tallyKey != null) {
                CurrentShortUrlAccessBean b = (CurrentShortUrlAccessBean) bean;
                b.setTallyTime(tallyTime);
                b.setCount(1);
                b.setShortUrl(shortUrl);
                b.setShortUrlDomain(getDomainFromUrl(shortUrl));
                b.setLongUrl(tallyKey.getLongUrl());
                b.setLongUrlDomain(getDomainFromUrl(tallyKey.getLongUrl()));
                b.setRedirectType(tallyKey.getRedirectType());
                b.setRefererDomain(tallyKey.getRefererDomain());
                b.setUserAgent(tallyKey.getUserAgent());
                b.setLanguage(tallyKey.getLanguage());
                b.setCountry(tallyKey.getCountry());
                //bean = b;
            }
        } else {
            // ????
        }
        return bean;
    }
    
    // Temporary
    private static ShortUrlAccess incrementAccessCount(ShortUrlAccess bean)
    {
        int count = bean.getCount() + 1;
        if(bean instanceof MonthlyShortUrlAccessBean) {
            ((MonthlyShortUrlAccessBean) bean).setCount(count);
        } else if(bean instanceof WeeklyShortUrlAccessBean) {
            ((WeeklyShortUrlAccessBean) bean).setCount(count);    
        } else if(bean instanceof DailyShortUrlAccessBean) {
            ((DailyShortUrlAccessBean) bean).setCount(count);            
        } else if(bean instanceof HourlyShortUrlAccessBean) {
            ((HourlyShortUrlAccessBean) bean).setCount(count);            
        } else if(bean instanceof CumulativeShortUrlAccessBean) {
            ((CumulativeShortUrlAccessBean) bean).setCount(count);            
        } else if(bean instanceof CurrentShortUrlAccessBean) {
            ((CurrentShortUrlAccessBean) bean).setCount(count);            
        } else {
            // ???
        }
        return bean;
    }

}
