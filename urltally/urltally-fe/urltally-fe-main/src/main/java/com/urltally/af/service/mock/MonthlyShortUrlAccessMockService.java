package com.urltally.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.MonthlyShortUrlAccessService;


// MonthlyShortUrlAccessMockService is a decorator.
// It can be used as a base class to mock MonthlyShortUrlAccessService objects.
public abstract class MonthlyShortUrlAccessMockService implements MonthlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MonthlyShortUrlAccessMockService.class.getName());

    // MonthlyShortUrlAccessMockService uses the decorator design pattern.
    private MonthlyShortUrlAccessService decoratedService;

    public MonthlyShortUrlAccessMockService(MonthlyShortUrlAccessService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected MonthlyShortUrlAccessService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(MonthlyShortUrlAccessService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // MonthlyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public MonthlyShortUrlAccess getMonthlyShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getMonthlyShortUrlAccess(): guid = " + guid);
        MonthlyShortUrlAccess bean = decoratedService.getMonthlyShortUrlAccess(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getMonthlyShortUrlAccess(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getMonthlyShortUrlAccess(guid, field);
        return obj;
    }

    @Override
    public List<MonthlyShortUrlAccess> getMonthlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getMonthlyShortUrlAccesses()");
        List<MonthlyShortUrlAccess> monthlyShortUrlAccesses = decoratedService.getMonthlyShortUrlAccesses(guids);
        log.finer("END");
        return monthlyShortUrlAccesses;
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses() throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllMonthlyShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<MonthlyShortUrlAccess> monthlyShortUrlAccesses = decoratedService.getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return monthlyShortUrlAccesses;
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllMonthlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllMonthlyShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("MonthlyShortUrlAccessMockService.findMonthlyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<MonthlyShortUrlAccess> monthlyShortUrlAccesses = decoratedService.findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return monthlyShortUrlAccesses;
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("MonthlyShortUrlAccessMockService.findMonthlyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("MonthlyShortUrlAccessMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createMonthlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        MonthlyShortUrlAccessBean bean = new MonthlyShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        return createMonthlyShortUrlAccess(bean);
    }

    @Override
    public String createMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createMonthlyShortUrlAccess(monthlyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public MonthlyShortUrlAccess constructMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        MonthlyShortUrlAccess bean = decoratedService.constructMonthlyShortUrlAccess(monthlyShortUrlAccess);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateMonthlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        MonthlyShortUrlAccessBean bean = new MonthlyShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        return updateMonthlyShortUrlAccess(bean);
    }
        
    @Override
    public Boolean updateMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateMonthlyShortUrlAccess(monthlyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public MonthlyShortUrlAccess refreshMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        MonthlyShortUrlAccess bean = decoratedService.refreshMonthlyShortUrlAccess(monthlyShortUrlAccess);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteMonthlyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteMonthlyShortUrlAccess(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteMonthlyShortUrlAccess(monthlyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteMonthlyShortUrlAccesses(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createMonthlyShortUrlAccesses(List<MonthlyShortUrlAccess> monthlyShortUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createMonthlyShortUrlAccesses(monthlyShortUrlAccesses);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateMonthlyShortUrlAccesses(List<MonthlyShortUrlAccess> monthlyShortUrlAccesses) throws BaseException
    //{
    //}

}
