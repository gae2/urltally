package com.urltally.af.service;

public abstract class AbstractServiceFactory
{
    public abstract ApiConsumerService getApiConsumerService();
    public abstract UserService getUserService();
    public abstract AccessTallyMasterService getAccessTallyMasterService();
    public abstract AccessTallyStatusService getAccessTallyStatusService();
    public abstract MonthlyShortUrlAccessService getMonthlyShortUrlAccessService();
    public abstract WeeklyShortUrlAccessService getWeeklyShortUrlAccessService();
    public abstract DailyShortUrlAccessService getDailyShortUrlAccessService();
    public abstract HourlyShortUrlAccessService getHourlyShortUrlAccessService();
    public abstract CumulativeShortUrlAccessService getCumulativeShortUrlAccessService();
    public abstract CurrentShortUrlAccessService getCurrentShortUrlAccessService();
    public abstract TotalShortUrlAccessService getTotalShortUrlAccessService();
    public abstract TotalLongUrlAccessService getTotalLongUrlAccessService();
    public abstract ServiceInfoService getServiceInfoService();
    public abstract FiveTenService getFiveTenService();

}
