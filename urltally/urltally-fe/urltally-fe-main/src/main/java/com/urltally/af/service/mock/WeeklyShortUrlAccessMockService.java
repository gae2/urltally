package com.urltally.af.service.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.WeeklyShortUrlAccessService;


// WeeklyShortUrlAccessMockService is a decorator.
// It can be used as a base class to mock WeeklyShortUrlAccessService objects.
public abstract class WeeklyShortUrlAccessMockService implements WeeklyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(WeeklyShortUrlAccessMockService.class.getName());

    // WeeklyShortUrlAccessMockService uses the decorator design pattern.
    private WeeklyShortUrlAccessService decoratedService;

    public WeeklyShortUrlAccessMockService(WeeklyShortUrlAccessService decoratedService)
    {
        this.decoratedService = decoratedService;
    }

    // To be used by subclasses
    protected WeeklyShortUrlAccessService getDecoratedService()
    {
        return decoratedService;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedService(WeeklyShortUrlAccessService decoratedService)
    // {
    //     this.decoratedService = decoratedService;
    // }


    //////////////////////////////////////////////////////////////////////////
    // WeeklyShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public WeeklyShortUrlAccess getWeeklyShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getWeeklyShortUrlAccess(): guid = " + guid);
        WeeklyShortUrlAccess bean = decoratedService.getWeeklyShortUrlAccess(guid);
        log.finer("END");
        return bean;
    }

    @Override
    public Object getWeeklyShortUrlAccess(String guid, String field) throws BaseException
    {
        Object obj = decoratedService.getWeeklyShortUrlAccess(guid, field);
        return obj;
    }

    @Override
    public List<WeeklyShortUrlAccess> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getWeeklyShortUrlAccesses()");
        List<WeeklyShortUrlAccess> weeklyShortUrlAccesses = decoratedService.getWeeklyShortUrlAccesses(guids);
        log.finer("END");
        return weeklyShortUrlAccesses;
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses() throws BaseException
    {
        return getAllWeeklyShortUrlAccesses(null, null, null);
    }


    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllWeeklyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllWeeklyShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<WeeklyShortUrlAccess> weeklyShortUrlAccesses = decoratedService.getAllWeeklyShortUrlAccesses(ordering, offset, count, forwardCursor);
        log.finer("END");
        return weeklyShortUrlAccesses;
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllWeeklyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllWeeklyShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.getAllWeeklyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("WeeklyShortUrlAccessMockService.findWeeklyShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<WeeklyShortUrlAccess> weeklyShortUrlAccesses = decoratedService.findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return weeklyShortUrlAccesses;
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("WeeklyShortUrlAccessMockService.findWeeklyShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        List<String> keys = decoratedService.findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("WeeklyShortUrlAccessMockService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        Long count = decoratedService.getCount(filter, params, values, aggregate);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createWeeklyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {
        WeeklyShortUrlAccessBean bean = new WeeklyShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
        return createWeeklyShortUrlAccess(bean);
    }

    @Override
    public String createWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        String guid = decoratedService.createWeeklyShortUrlAccess(weeklyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public WeeklyShortUrlAccess constructWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        WeeklyShortUrlAccess bean = decoratedService.constructWeeklyShortUrlAccess(weeklyShortUrlAccess);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateWeeklyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        WeeklyShortUrlAccessBean bean = new WeeklyShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
        return updateWeeklyShortUrlAccess(bean);
    }
        
    @Override
    public Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.updateWeeklyShortUrlAccess(weeklyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public WeeklyShortUrlAccess refreshWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        WeeklyShortUrlAccess bean = decoratedService.refreshWeeklyShortUrlAccess(weeklyShortUrlAccess);
        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteWeeklyShortUrlAccess(guid);
        log.finer("END");
        return suc;
    }

    // ???
    @Override
    public Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");
        Boolean suc = decoratedService.deleteWeeklyShortUrlAccess(weeklyShortUrlAccess);
        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = decoratedService.deleteWeeklyShortUrlAccesses(filter, params, values);
        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createWeeklyShortUrlAccesses(List<WeeklyShortUrlAccess> weeklyShortUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");
        Integer count = decoratedService.createWeeklyShortUrlAccesses(weeklyShortUrlAccesses);
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateWeeklyShortUrlAccesses(List<WeeklyShortUrlAccess> weeklyShortUrlAccesses) throws BaseException
    //{
    //}

}
