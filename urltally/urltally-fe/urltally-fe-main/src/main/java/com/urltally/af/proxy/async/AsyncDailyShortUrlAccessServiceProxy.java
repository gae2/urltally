package com.urltally.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.DailyShortUrlAccessStub;
import com.urltally.ws.stub.DailyShortUrlAccessListStub;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.ws.service.DailyShortUrlAccessService;
import com.urltally.af.proxy.DailyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.remote.RemoteDailyShortUrlAccessServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncDailyShortUrlAccessServiceProxy extends BaseAsyncServiceProxy implements DailyShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncDailyShortUrlAccessServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteDailyShortUrlAccessServiceProxy remoteProxy;

    public AsyncDailyShortUrlAccessServiceProxy()
    {
        remoteProxy = new RemoteDailyShortUrlAccessServiceProxy();
    }

    @Override
    public DailyShortUrlAccess getDailyShortUrlAccess(String guid) throws BaseException
    {
        return remoteProxy.getDailyShortUrlAccess(guid);
    }

    @Override
    public Object getDailyShortUrlAccess(String guid, String field) throws BaseException
    {
        return remoteProxy.getDailyShortUrlAccess(guid, field);       
    }

    @Override
    public List<DailyShortUrlAccess> getDailyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return remoteProxy.getDailyShortUrlAccesses(guids);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses() throws BaseException
    {
        return getAllDailyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllDailyShortUrlAccesses(ordering, offset, count);
        return getAllDailyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllDailyShortUrlAccessKeys(ordering, offset, count);
        return getAllDailyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDailyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        DailyShortUrlAccessBean bean = new DailyShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
        return createDailyShortUrlAccess(bean);        
    }

    @Override
    public String createDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        String guid = dailyShortUrlAccess.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((DailyShortUrlAccessBean) dailyShortUrlAccess).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateDailyShortUrlAccess-" + guid;
        String taskName = "RsCreateDailyShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        DailyShortUrlAccessStub stub = MarshalHelper.convertDailyShortUrlAccessToStub(dailyShortUrlAccess);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(DailyShortUrlAccessStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = dailyShortUrlAccess.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    DailyShortUrlAccessStub dummyStub = new DailyShortUrlAccessStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createDailyShortUrlAccess(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "dailyShortUrlAccesses/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createDailyShortUrlAccess(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "dailyShortUrlAccesses/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DailyShortUrlAccess guid is invalid.");
        	throw new BaseException("DailyShortUrlAccess guid is invalid.");
        }
        DailyShortUrlAccessBean bean = new DailyShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
        return updateDailyShortUrlAccess(bean);        
    }

    @Override
    public Boolean updateDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        String guid = dailyShortUrlAccess.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "DailyShortUrlAccess object is invalid.");
        	throw new BaseException("DailyShortUrlAccess object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateDailyShortUrlAccess-" + guid;
        String taskName = "RsUpdateDailyShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        DailyShortUrlAccessStub stub = MarshalHelper.convertDailyShortUrlAccessToStub(dailyShortUrlAccess);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(DailyShortUrlAccessStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = dailyShortUrlAccess.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    DailyShortUrlAccessStub dummyStub = new DailyShortUrlAccessStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateDailyShortUrlAccess(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "dailyShortUrlAccesses/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateDailyShortUrlAccess(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "dailyShortUrlAccesses/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteDailyShortUrlAccess-" + guid;
        String taskName = "RsDeleteDailyShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "dailyShortUrlAccesses/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess) throws BaseException
    {
        String guid = dailyShortUrlAccess.getGuid();
        return deleteDailyShortUrlAccess(guid);
    }

    @Override
    public Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteDailyShortUrlAccesses(filter, params, values);
    }

}
