package com.urltally.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.af.service.manager.ServiceManager;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.CurrentShortUrlAccessJsBean;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CurrentShortUrlAccessWebService // implements CurrentShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private CurrentShortUrlAccessService mService = null;

    public CurrentShortUrlAccessWebService()
    {
        this(ServiceManager.getCurrentShortUrlAccessService());
    }
    public CurrentShortUrlAccessWebService(CurrentShortUrlAccessService service)
    {
        mService = service;
    }
    
    private CurrentShortUrlAccessService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getCurrentShortUrlAccessService();
        }
        return mService;
    }
    
    
    public CurrentShortUrlAccessJsBean getCurrentShortUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            CurrentShortUrlAccess currentShortUrlAccess = getService().getCurrentShortUrlAccess(guid);
            CurrentShortUrlAccessJsBean bean = convertCurrentShortUrlAccessToJsBean(currentShortUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getCurrentShortUrlAccess(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getCurrentShortUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<CurrentShortUrlAccessJsBean> getCurrentShortUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<CurrentShortUrlAccessJsBean> jsBeans = new ArrayList<CurrentShortUrlAccessJsBean>();
            List<CurrentShortUrlAccess> currentShortUrlAccesses = getService().getCurrentShortUrlAccesses(guids);
            if(currentShortUrlAccesses != null) {
                for(CurrentShortUrlAccess currentShortUrlAccess : currentShortUrlAccesses) {
                    jsBeans.add(convertCurrentShortUrlAccessToJsBean(currentShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<CurrentShortUrlAccessJsBean> getAllCurrentShortUrlAccesses() throws WebException
    {
        return getAllCurrentShortUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<CurrentShortUrlAccessJsBean> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllCurrentShortUrlAccesses(ordering, offset, count, null);
    }

    public List<CurrentShortUrlAccessJsBean> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<CurrentShortUrlAccessJsBean> jsBeans = new ArrayList<CurrentShortUrlAccessJsBean>();
            List<CurrentShortUrlAccess> currentShortUrlAccesses = getService().getAllCurrentShortUrlAccesses(ordering, offset, count, forwardCursor);
            if(currentShortUrlAccesses != null) {
                for(CurrentShortUrlAccess currentShortUrlAccess : currentShortUrlAccesses) {
                    jsBeans.add(convertCurrentShortUrlAccessToJsBean(currentShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllCurrentShortUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllCurrentShortUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<CurrentShortUrlAccessJsBean> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<CurrentShortUrlAccessJsBean> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<CurrentShortUrlAccessJsBean> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<CurrentShortUrlAccessJsBean> jsBeans = new ArrayList<CurrentShortUrlAccessJsBean>();
            List<CurrentShortUrlAccess> currentShortUrlAccesses = getService().findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(currentShortUrlAccesses != null) {
                for(CurrentShortUrlAccess currentShortUrlAccess : currentShortUrlAccesses) {
                    jsBeans.add(convertCurrentShortUrlAccessToJsBean(currentShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createCurrentShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws WebException
    {
        try {
            return getService().createCurrentShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createCurrentShortUrlAccess(String jsonStr) throws WebException
    {
        return createCurrentShortUrlAccess(CurrentShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public String createCurrentShortUrlAccess(CurrentShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CurrentShortUrlAccess currentShortUrlAccess = convertCurrentShortUrlAccessJsBeanToBean(jsBean);
            return getService().createCurrentShortUrlAccess(currentShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public CurrentShortUrlAccessJsBean constructCurrentShortUrlAccess(String jsonStr) throws WebException
    {
        return constructCurrentShortUrlAccess(CurrentShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public CurrentShortUrlAccessJsBean constructCurrentShortUrlAccess(CurrentShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CurrentShortUrlAccess currentShortUrlAccess = convertCurrentShortUrlAccessJsBeanToBean(jsBean);
            currentShortUrlAccess = getService().constructCurrentShortUrlAccess(currentShortUrlAccess);
            jsBean = convertCurrentShortUrlAccessToJsBean(currentShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateCurrentShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws WebException
    {
        try {
            return getService().updateCurrentShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateCurrentShortUrlAccess(String jsonStr) throws WebException
    {
        return updateCurrentShortUrlAccess(CurrentShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CurrentShortUrlAccess currentShortUrlAccess = convertCurrentShortUrlAccessJsBeanToBean(jsBean);
            return getService().updateCurrentShortUrlAccess(currentShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public CurrentShortUrlAccessJsBean refreshCurrentShortUrlAccess(String jsonStr) throws WebException
    {
        return refreshCurrentShortUrlAccess(CurrentShortUrlAccessJsBean.fromJsonString(jsonStr));
    }

    public CurrentShortUrlAccessJsBean refreshCurrentShortUrlAccess(CurrentShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CurrentShortUrlAccess currentShortUrlAccess = convertCurrentShortUrlAccessJsBeanToBean(jsBean);
            currentShortUrlAccess = getService().refreshCurrentShortUrlAccess(currentShortUrlAccess);
            jsBean = convertCurrentShortUrlAccessToJsBean(currentShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteCurrentShortUrlAccess(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteCurrentShortUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccessJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            CurrentShortUrlAccess currentShortUrlAccess = convertCurrentShortUrlAccessJsBeanToBean(jsBean);
            return getService().deleteCurrentShortUrlAccess(currentShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteCurrentShortUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static CurrentShortUrlAccessJsBean convertCurrentShortUrlAccessToJsBean(CurrentShortUrlAccess currentShortUrlAccess)
    {
        CurrentShortUrlAccessJsBean jsBean = null;
        if(currentShortUrlAccess != null) {
            jsBean = new CurrentShortUrlAccessJsBean();
            jsBean.setGuid(currentShortUrlAccess.getGuid());
            jsBean.setTallyTime(currentShortUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(currentShortUrlAccess.getTallyEpoch());
            jsBean.setCount(currentShortUrlAccess.getCount());
            jsBean.setShortUrl(currentShortUrlAccess.getShortUrl());
            jsBean.setShortUrlDomain(currentShortUrlAccess.getShortUrlDomain());
            jsBean.setLongUrl(currentShortUrlAccess.getLongUrl());
            jsBean.setLongUrlDomain(currentShortUrlAccess.getLongUrlDomain());
            jsBean.setRedirectType(currentShortUrlAccess.getRedirectType());
            jsBean.setRefererDomain(currentShortUrlAccess.getRefererDomain());
            jsBean.setUserAgent(currentShortUrlAccess.getUserAgent());
            jsBean.setLanguage(currentShortUrlAccess.getLanguage());
            jsBean.setCountry(currentShortUrlAccess.getCountry());
            jsBean.setTalliedTime(currentShortUrlAccess.getTalliedTime());
            jsBean.setStartDayHour(currentShortUrlAccess.getStartDayHour());
            jsBean.setStartTime(currentShortUrlAccess.getStartTime());
            jsBean.setCreatedTime(currentShortUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(currentShortUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static CurrentShortUrlAccess convertCurrentShortUrlAccessJsBeanToBean(CurrentShortUrlAccessJsBean jsBean)
    {
        CurrentShortUrlAccessBean currentShortUrlAccess = null;
        if(jsBean != null) {
            currentShortUrlAccess = new CurrentShortUrlAccessBean();
            currentShortUrlAccess.setGuid(jsBean.getGuid());
            currentShortUrlAccess.setTallyTime(jsBean.getTallyTime());
            currentShortUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            currentShortUrlAccess.setCount(jsBean.getCount());
            currentShortUrlAccess.setShortUrl(jsBean.getShortUrl());
            currentShortUrlAccess.setShortUrlDomain(jsBean.getShortUrlDomain());
            currentShortUrlAccess.setLongUrl(jsBean.getLongUrl());
            currentShortUrlAccess.setLongUrlDomain(jsBean.getLongUrlDomain());
            currentShortUrlAccess.setRedirectType(jsBean.getRedirectType());
            currentShortUrlAccess.setRefererDomain(jsBean.getRefererDomain());
            currentShortUrlAccess.setUserAgent(jsBean.getUserAgent());
            currentShortUrlAccess.setLanguage(jsBean.getLanguage());
            currentShortUrlAccess.setCountry(jsBean.getCountry());
            currentShortUrlAccess.setTalliedTime(jsBean.getTalliedTime());
            currentShortUrlAccess.setStartDayHour(jsBean.getStartDayHour());
            currentShortUrlAccess.setStartTime(jsBean.getStartTime());
            currentShortUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            currentShortUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return currentShortUrlAccess;
    }

}
