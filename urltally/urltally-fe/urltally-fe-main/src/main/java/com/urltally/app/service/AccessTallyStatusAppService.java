package com.urltally.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.urltally.ws.BaseException;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.service.impl.AccessTallyStatusServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class AccessTallyStatusAppService extends AccessTallyStatusServiceImpl implements AccessTallyStatusService
{
    private static final Logger log = Logger.getLogger(AccessTallyStatusAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public AccessTallyStatusAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // AccessTallyStatus related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException
    {
        return super.getAccessTallyStatus(guid);
    }

    @Override
    public Object getAccessTallyStatus(String guid, String field) throws BaseException
    {
        return super.getAccessTallyStatus(guid, field);
    }

    @Override
    public List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        return super.getAccessTallyStatuses(guids);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException
    {
        return super.getAllAccessTallyStatuses();
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllAccessTallyStatusKeys(ordering, offset, count);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        return super.createAccessTallyStatus(accessTallyStatus);
    }

    @Override
    public AccessTallyStatus constructAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        return super.constructAccessTallyStatus(accessTallyStatus);
    }


    @Override
    public Boolean updateAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        return super.updateAccessTallyStatus(accessTallyStatus);
    }
        
    @Override
    public AccessTallyStatus refreshAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        return super.refreshAccessTallyStatus(accessTallyStatus);
    }

    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        return super.deleteAccessTallyStatus(guid);
    }

    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        return super.deleteAccessTallyStatus(accessTallyStatus);
    }

    @Override
    public Integer createAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    {
        return super.createAccessTallyStatuses(accessTallyStatuses);
    }

    // TBD
    //@Override
    //public Boolean updateAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    //{
    //}

}
