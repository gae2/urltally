package com.urltally.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.urltally.ws.BaseException;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.service.impl.AccessTallyMasterServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class AccessTallyMasterAppService extends AccessTallyMasterServiceImpl implements AccessTallyMasterService
{
    private static final Logger log = Logger.getLogger(AccessTallyMasterAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public AccessTallyMasterAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // AccessTallyMaster related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException
    {
        return super.getAccessTallyMaster(guid);
    }

    @Override
    public Object getAccessTallyMaster(String guid, String field) throws BaseException
    {
        return super.getAccessTallyMaster(guid, field);
    }

    @Override
    public List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        return super.getAccessTallyMasters(guids);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException
    {
        return super.getAllAccessTallyMasters();
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllAccessTallyMasterKeys(ordering, offset, count);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        return super.createAccessTallyMaster(accessTallyMaster);
    }

    @Override
    public AccessTallyMaster constructAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        return super.constructAccessTallyMaster(accessTallyMaster);
    }


    @Override
    public Boolean updateAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        return super.updateAccessTallyMaster(accessTallyMaster);
    }
        
    @Override
    public AccessTallyMaster refreshAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        return super.refreshAccessTallyMaster(accessTallyMaster);
    }

    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        return super.deleteAccessTallyMaster(guid);
    }

    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        return super.deleteAccessTallyMaster(accessTallyMaster);
    }

    @Override
    public Integer createAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException
    {
        return super.createAccessTallyMasters(accessTallyMasters);
    }

    // TBD
    //@Override
    //public Boolean updateAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException
    //{
    //}

}
