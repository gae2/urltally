package com.urltally.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.stub.AccessTallyMasterStub;


// Wrapper class + bean combo.
public class AccessTallyMasterBean implements AccessTallyMaster, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AccessTallyMasterBean.class.getName());

    // [1] With an embedded object.
    private AccessTallyMasterStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String tallyType;
    private String tallyTime;
    private Long tallyEpoch;
    private String tallyStatus;
    private Integer accessRecordCount;
    private Long procesingStartedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public AccessTallyMasterBean()
    {
        //this((String) null);
    }
    public AccessTallyMasterBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public AccessTallyMasterBean(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime)
    {
        this(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime, null, null);
    }
    public AccessTallyMasterBean(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.tallyType = tallyType;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.tallyStatus = tallyStatus;
        this.accessRecordCount = accessRecordCount;
        this.procesingStartedTime = procesingStartedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public AccessTallyMasterBean(AccessTallyMaster stub)
    {
        if(stub instanceof AccessTallyMasterStub) {
            this.stub = (AccessTallyMasterStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setTallyType(stub.getTallyType());   
            setTallyTime(stub.getTallyTime());   
            setTallyEpoch(stub.getTallyEpoch());   
            setTallyStatus(stub.getTallyStatus());   
            setAccessRecordCount(stub.getAccessRecordCount());   
            setProcesingStartedTime(stub.getProcesingStartedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getTallyType()
    {
        if(getStub() != null) {
            return getStub().getTallyType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyType;
        }
    }
    public void setTallyType(String tallyType)
    {
        if(getStub() != null) {
            getStub().setTallyType(tallyType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyType = tallyType;
        }
    }

    public String getTallyTime()
    {
        if(getStub() != null) {
            return getStub().getTallyTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyTime;
        }
    }
    public void setTallyTime(String tallyTime)
    {
        if(getStub() != null) {
            getStub().setTallyTime(tallyTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyTime = tallyTime;
        }
    }

    public Long getTallyEpoch()
    {
        if(getStub() != null) {
            return getStub().getTallyEpoch();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyEpoch;
        }
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        if(getStub() != null) {
            getStub().setTallyEpoch(tallyEpoch);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyEpoch = tallyEpoch;
        }
    }

    public String getTallyStatus()
    {
        if(getStub() != null) {
            return getStub().getTallyStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tallyStatus;
        }
    }
    public void setTallyStatus(String tallyStatus)
    {
        if(getStub() != null) {
            getStub().setTallyStatus(tallyStatus);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tallyStatus = tallyStatus;
        }
    }

    public Integer getAccessRecordCount()
    {
        if(getStub() != null) {
            return getStub().getAccessRecordCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.accessRecordCount;
        }
    }
    public void setAccessRecordCount(Integer accessRecordCount)
    {
        if(getStub() != null) {
            getStub().setAccessRecordCount(accessRecordCount);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.accessRecordCount = accessRecordCount;
        }
    }

    public Long getProcesingStartedTime()
    {
        if(getStub() != null) {
            return getStub().getProcesingStartedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.procesingStartedTime;
        }
    }
    public void setProcesingStartedTime(Long procesingStartedTime)
    {
        if(getStub() != null) {
            getStub().setProcesingStartedTime(procesingStartedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.procesingStartedTime = procesingStartedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public AccessTallyMasterStub getStub()
    {
        return this.stub;
    }
    protected void setStub(AccessTallyMasterStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("tallyType = " + this.tallyType).append(";");
            sb.append("tallyTime = " + this.tallyTime).append(";");
            sb.append("tallyEpoch = " + this.tallyEpoch).append(";");
            sb.append("tallyStatus = " + this.tallyStatus).append(";");
            sb.append("accessRecordCount = " + this.accessRecordCount).append(";");
            sb.append("procesingStartedTime = " + this.procesingStartedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyType == null ? 0 : tallyType.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyTime == null ? 0 : tallyTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyEpoch == null ? 0 : tallyEpoch.hashCode();
            _hash = 31 * _hash + delta;
            delta = tallyStatus == null ? 0 : tallyStatus.hashCode();
            _hash = 31 * _hash + delta;
            delta = accessRecordCount == null ? 0 : accessRecordCount.hashCode();
            _hash = 31 * _hash + delta;
            delta = procesingStartedTime == null ? 0 : procesingStartedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
