package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.stub.MonthlyShortUrlAccessStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessListStub;
import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.af.resource.MonthlyShortUrlAccessResource;


// MockMonthlyShortUrlAccessResource is a decorator.
// It can be used as a base class to mock MonthlyShortUrlAccessResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/monthlyShortUrlAccesses/")
public abstract class MockMonthlyShortUrlAccessResource implements MonthlyShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(MockMonthlyShortUrlAccessResource.class.getName());

    // MockMonthlyShortUrlAccessResource uses the decorator design pattern.
    private MonthlyShortUrlAccessResource decoratedResource;

    public MockMonthlyShortUrlAccessResource(MonthlyShortUrlAccessResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected MonthlyShortUrlAccessResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(MonthlyShortUrlAccessResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findMonthlyShortUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findMonthlyShortUrlAccessesAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getMonthlyShortUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getMonthlyShortUrlAccessAsHtml(guid);
//    }

    @Override
    public Response getMonthlyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.getMonthlyShortUrlAccess(guid);
    }

    @Override
    public Response getMonthlyShortUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getMonthlyShortUrlAccessAsJsonp(guid, callback);
    }

    @Override
    public Response getMonthlyShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getMonthlyShortUrlAccess(guid, field);
    }

    // TBD
    @Override
    public Response constructMonthlyShortUrlAccess(MonthlyShortUrlAccessStub monthlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.constructMonthlyShortUrlAccess(monthlyShortUrlAccess);
    }

    @Override
    public Response createMonthlyShortUrlAccess(MonthlyShortUrlAccessStub monthlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.createMonthlyShortUrlAccess(monthlyShortUrlAccess);
    }

//    @Override
//    public Response createMonthlyShortUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createMonthlyShortUrlAccess(formParams);
//    }

    // TBD
    @Override
    public Response refreshMonthlyShortUrlAccess(String guid, MonthlyShortUrlAccessStub monthlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.refreshMonthlyShortUrlAccess(guid, monthlyShortUrlAccess);
    }

    @Override
    public Response updateMonthlyShortUrlAccess(String guid, MonthlyShortUrlAccessStub monthlyShortUrlAccess) throws BaseResourceException
    {
        return decoratedResource.updateMonthlyShortUrlAccess(guid, monthlyShortUrlAccess);
    }

    @Override
    public Response updateMonthlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays)
    {
        return decoratedResource.updateMonthlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
    }

//    @Override
//    public Response updateMonthlyShortUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateMonthlyShortUrlAccess(guid, formParams);
//    }

    @Override
    public Response deleteMonthlyShortUrlAccess(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteMonthlyShortUrlAccess(guid);
    }

    @Override
    public Response deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteMonthlyShortUrlAccesses(filter, params, values);
    }


// TBD ....
    @Override
    public Response createMonthlyShortUrlAccesses(MonthlyShortUrlAccessListStub monthlyShortUrlAccesses) throws BaseResourceException
    {
        return decoratedResource.createMonthlyShortUrlAccesses(monthlyShortUrlAccesses);
    }


}
