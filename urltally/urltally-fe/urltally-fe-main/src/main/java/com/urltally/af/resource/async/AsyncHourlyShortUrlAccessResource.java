package com.urltally.af.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.stub.HourlyShortUrlAccessStub;
import com.urltally.ws.stub.HourlyShortUrlAccessListStub;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.proxy.HourlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.remote.RemoteProxyFactory;
import com.urltally.af.proxy.remote.RemoteHourlyShortUrlAccessServiceProxy;
import com.urltally.af.resource.HourlyShortUrlAccessResource;


@Path("/_task/r/hourlyShortUrlAccesses/")
public class AsyncHourlyShortUrlAccessResource extends BaseAsyncResource implements HourlyShortUrlAccessResource
{
    private static final Logger log = Logger.getLogger(AsyncHourlyShortUrlAccessResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncHourlyShortUrlAccessResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getHourlyShortUrlAccessList(List<HourlyShortUrlAccess> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findHourlyShortUrlAccessesAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
//    public Response getHourlyShortUrlAccessAsHtml(String guid) throws BaseResourceException
//    {
//        // Note: This method should never be called.
//        throw new NotImplementedRsException(resourceUri);
//    }

    @Override
    public Response getHourlyShortUrlAccess(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getHourlyShortUrlAccessAsJsonp(String guid, String callback) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getHourlyShortUrlAccess(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response constructHourlyShortUrlAccess(HourlyShortUrlAccessStub hourlyShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructHourlyShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            HourlyShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                HourlyShortUrlAccessStub realStub = (HourlyShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertHourlyShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertHourlyShortUrlAccessStubToBean(hourlyShortUrlAccess);
            }
            //bean = (HourlyShortUrlAccessBean) RemoteProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy().constructHourlyShortUrlAccess(bean);
            //hourlyShortUrlAccess = HourlyShortUrlAccessStub.convertBeanToStub(bean);
            //String guid = hourlyShortUrlAccess.getGuid();
            // TBD: createHourlyShortUrlAccess() or constructHourlyShortUrlAccess()???  (constructHourlyShortUrlAccess() currently not implemented)
            String guid = RemoteProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy().createHourlyShortUrlAccess(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "constructHourlyShortUrlAccess(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(hourlyShortUrlAccess).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response createHourlyShortUrlAccess(HourlyShortUrlAccessStub hourlyShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createHourlyShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            HourlyShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                HourlyShortUrlAccessStub realStub = (HourlyShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertHourlyShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertHourlyShortUrlAccessStubToBean(hourlyShortUrlAccess);
            }
            String guid = RemoteProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy().createHourlyShortUrlAccess(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createHourlyShortUrlAccess(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

//    @Override
    public Response createHourlyShortUrlAccess(MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    // TBD
    @Override
    public Response refreshHourlyShortUrlAccess(String guid, HourlyShortUrlAccessStub hourlyShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshHourlyShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(hourlyShortUrlAccess == null || !guid.equals(hourlyShortUrlAccess.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from hourlyShortUrlAccess guid = " + hourlyShortUrlAccess.getGuid());
                throw new RequestForbiddenRsException("Failed to refresh the hourlyShortUrlAccess with guid = " + guid);
            }
            HourlyShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                HourlyShortUrlAccessStub realStub = (HourlyShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertHourlyShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertHourlyShortUrlAccessStubToBean(hourlyShortUrlAccess);
            }
            //bean = (HourlyShortUrlAccessBean) RemoteProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy().refreshHourlyShortUrlAccess(bean);
            //if(bean == null) {
            //    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refresh the hourlyShortUrlAccess with guid = " + guid);
            //    throw new InternalServerErrorException("Failed to refresh the hourlyShortUrlAccess with guid = " + guid);
            //}
            //hourlyShortUrlAccess = HourlyShortUrlAccessStub.convertBeanToStub(bean);
            // TBD: updateHourlyShortUrlAccess() or refreshHourlyShortUrlAccess()???  (refreshHourlyShortUrlAccess() currently not implemented)
            boolean suc = RemoteProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy().updateHourlyShortUrlAccess(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to refrefsh the hourlyShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the hourlyShortUrlAccess with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "refreshHourlyShortUrlAccess(): Successfully processed the request: guid = " + guid);
            return Response.ok(hourlyShortUrlAccess).build();  // ???
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateHourlyShortUrlAccess(String guid, HourlyShortUrlAccessStub hourlyShortUrlAccess) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateHourlyShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            if(hourlyShortUrlAccess == null || !guid.equals(hourlyShortUrlAccess.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from hourlyShortUrlAccess guid = " + hourlyShortUrlAccess.getGuid());
                throw new RequestForbiddenRsException("Failed to update the hourlyShortUrlAccess with guid = " + guid);
            }
            HourlyShortUrlAccessBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                HourlyShortUrlAccessStub realStub = (HourlyShortUrlAccessStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertHourlyShortUrlAccessStubToBean(realStub);
            } else {
                bean = convertHourlyShortUrlAccessStubToBean(hourlyShortUrlAccess);
            }
            boolean suc = RemoteProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy().updateHourlyShortUrlAccess(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the hourlyShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the hourlyShortUrlAccess with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateHourlyShortUrlAccess(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour)
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

//    @Override
    public Response updateHourlyShortUrlAccess(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteHourlyShortUrlAccess(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteHourlyShortUrlAccess(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            boolean suc = RemoteProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy().deleteHourlyShortUrlAccess(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the hourlyShortUrlAccess with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the hourlyShortUrlAccess with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteHourlyShortUrlAccess(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteHourlyShortUrlAccesses(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            Long count = RemoteProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy().deleteHourlyShortUrlAccesses(filter, params, values);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Delete count = " + count);
            return Response.ok(count.toString()).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }


// TBD ....
    @Override
    public Response createHourlyShortUrlAccesses(HourlyShortUrlAccessListStub hourlyShortUrlAccesses) throws BaseResourceException
    {
        // TBD: Do we need this method????
        throw new NotImplementedRsException(resourceUri);
/*
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createHourlyShortUrlAccesses(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            HourlyShortUrlAccessListStub stubs;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                HourlyShortUrlAccessListStub realStubs = (HourlyShortUrlAccessListStub) getCache().get(taskName);
                if(realStubs == null) {
                    throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub list retrieved from memCache. realStubs = " + realStubs);
                stubs = realStubs;
            } else {
                stubs = hourlyShortUrlAccesses;
            }

            List<HourlyShortUrlAccessStub> stubList = hourlyShortUrlAccesses.getList();
            List<VisitorSetting> beans = new ArrayList<HourlyShortUrlAccess>();
            for(HourlyShortUrlAccessStub stub : stubList) {
                HourlyShortUrlAccessBean bean = convertHourlyShortUrlAccessStubToBean(stub);
                beans.add(bean);
            }
            Integer count = RemoteProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy().createHourlyShortUrlAccesses(beans);
            return Response.ok(count.toString()).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
*/
    }


    public static HourlyShortUrlAccessBean convertHourlyShortUrlAccessStubToBean(HourlyShortUrlAccess stub)
    {
        HourlyShortUrlAccessBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null bean is returned.");
        } else {
            bean = new HourlyShortUrlAccessBean();
            bean.setGuid(stub.getGuid());
            bean.setTallyTime(stub.getTallyTime());
            bean.setTallyEpoch(stub.getTallyEpoch());
            bean.setCount(stub.getCount());
            bean.setShortUrl(stub.getShortUrl());
            bean.setShortUrlDomain(stub.getShortUrlDomain());
            bean.setLongUrl(stub.getLongUrl());
            bean.setLongUrlDomain(stub.getLongUrlDomain());
            bean.setRedirectType(stub.getRedirectType());
            bean.setRefererDomain(stub.getRefererDomain());
            bean.setUserAgent(stub.getUserAgent());
            bean.setLanguage(stub.getLanguage());
            bean.setCountry(stub.getCountry());
            bean.setTalliedTime(stub.getTalliedTime());
            bean.setYear(stub.getYear());
            bean.setDay(stub.getDay());
            bean.setHour(stub.getHour());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
