package com.urltally.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.CurrentShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CurrentShortUrlAccessServiceImpl implements CurrentShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(CurrentShortUrlAccessServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "CurrentShortUrlAccess-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("CurrentShortUrlAccess:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public CurrentShortUrlAccessServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // CurrentShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public CurrentShortUrlAccess getCurrentShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getCurrentShortUrlAccess(): guid = " + guid);

        CurrentShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (CurrentShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (CurrentShortUrlAccessBean) getProxyFactory().getCurrentShortUrlAccessServiceProxy().getCurrentShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "CurrentShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccessBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getCurrentShortUrlAccess(String guid, String field) throws BaseException
    {
        CurrentShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (CurrentShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (CurrentShortUrlAccessBean) getProxyFactory().getCurrentShortUrlAccessServiceProxy().getCurrentShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "CurrentShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccessBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyTime")) {
            return bean.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return bean.getTallyEpoch();
        } else if(field.equals("count")) {
            return bean.getCount();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return bean.getShortUrlDomain();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("longUrlDomain")) {
            return bean.getLongUrlDomain();
        } else if(field.equals("redirectType")) {
            return bean.getRedirectType();
        } else if(field.equals("refererDomain")) {
            return bean.getRefererDomain();
        } else if(field.equals("userAgent")) {
            return bean.getUserAgent();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("country")) {
            return bean.getCountry();
        } else if(field.equals("talliedTime")) {
            return bean.getTalliedTime();
        } else if(field.equals("startDayHour")) {
            return bean.getStartDayHour();
        } else if(field.equals("startTime")) {
            return bean.getStartTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<CurrentShortUrlAccess> getCurrentShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getCurrentShortUrlAccesses()");

        // TBD: Is there a better way????
        List<CurrentShortUrlAccess> currentShortUrlAccesses = getProxyFactory().getCurrentShortUrlAccessServiceProxy().getCurrentShortUrlAccesses(guids);
        if(currentShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccessBean list.");
        }

        log.finer("END");
        return currentShortUrlAccesses;
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses() throws BaseException
    {
        return getAllCurrentShortUrlAccesses(null, null, null);
    }


    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccess> getAllCurrentShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllCurrentShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<CurrentShortUrlAccess> currentShortUrlAccesses = getProxyFactory().getCurrentShortUrlAccessServiceProxy().getAllCurrentShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(currentShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccessBean list.");
        }

        log.finer("END");
        return currentShortUrlAccesses;
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCurrentShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCurrentShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllCurrentShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getCurrentShortUrlAccessServiceProxy().getAllCurrentShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve CurrentShortUrlAccessBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty CurrentShortUrlAccessBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<CurrentShortUrlAccess> findCurrentShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("CurrentShortUrlAccessServiceImpl.findCurrentShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<CurrentShortUrlAccess> currentShortUrlAccesses = getProxyFactory().getCurrentShortUrlAccessServiceProxy().findCurrentShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(currentShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to find currentShortUrlAccesses for the given criterion.");
        }

        log.finer("END");
        return currentShortUrlAccesses;
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCurrentShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("CurrentShortUrlAccessServiceImpl.findCurrentShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getCurrentShortUrlAccessServiceProxy().findCurrentShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find CurrentShortUrlAccess keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty CurrentShortUrlAccess key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("CurrentShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getCurrentShortUrlAccessServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createCurrentShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        CurrentShortUrlAccessBean bean = new CurrentShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
        return createCurrentShortUrlAccess(bean);
    }

    @Override
    public String createCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //CurrentShortUrlAccess bean = constructCurrentShortUrlAccess(currentShortUrlAccess);
        //return bean.getGuid();

        // Param currentShortUrlAccess cannot be null.....
        if(currentShortUrlAccess == null) {
            log.log(Level.INFO, "Param currentShortUrlAccess is null!");
            throw new BadRequestException("Param currentShortUrlAccess object is null!");
        }
        CurrentShortUrlAccessBean bean = null;
        if(currentShortUrlAccess instanceof CurrentShortUrlAccessBean) {
            bean = (CurrentShortUrlAccessBean) currentShortUrlAccess;
        } else if(currentShortUrlAccess instanceof CurrentShortUrlAccess) {
            // bean = new CurrentShortUrlAccessBean(null, currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new CurrentShortUrlAccessBean(currentShortUrlAccess.getGuid(), currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
        } else {
            log.log(Level.WARNING, "createCurrentShortUrlAccess(): Arg currentShortUrlAccess is of an unknown type.");
            //bean = new CurrentShortUrlAccessBean();
            bean = new CurrentShortUrlAccessBean(currentShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getCurrentShortUrlAccessServiceProxy().createCurrentShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public CurrentShortUrlAccess constructCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param currentShortUrlAccess cannot be null.....
        if(currentShortUrlAccess == null) {
            log.log(Level.INFO, "Param currentShortUrlAccess is null!");
            throw new BadRequestException("Param currentShortUrlAccess object is null!");
        }
        CurrentShortUrlAccessBean bean = null;
        if(currentShortUrlAccess instanceof CurrentShortUrlAccessBean) {
            bean = (CurrentShortUrlAccessBean) currentShortUrlAccess;
        } else if(currentShortUrlAccess instanceof CurrentShortUrlAccess) {
            // bean = new CurrentShortUrlAccessBean(null, currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new CurrentShortUrlAccessBean(currentShortUrlAccess.getGuid(), currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
        } else {
            log.log(Level.WARNING, "createCurrentShortUrlAccess(): Arg currentShortUrlAccess is of an unknown type.");
            //bean = new CurrentShortUrlAccessBean();
            bean = new CurrentShortUrlAccessBean(currentShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getCurrentShortUrlAccessServiceProxy().createCurrentShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateCurrentShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, Long startTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        CurrentShortUrlAccessBean bean = new CurrentShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, startTime);
        return updateCurrentShortUrlAccess(bean);
    }
        
    @Override
    public Boolean updateCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //CurrentShortUrlAccess bean = refreshCurrentShortUrlAccess(currentShortUrlAccess);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param currentShortUrlAccess cannot be null.....
        if(currentShortUrlAccess == null || currentShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param currentShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param currentShortUrlAccess object or its guid is null!");
        }
        CurrentShortUrlAccessBean bean = null;
        if(currentShortUrlAccess instanceof CurrentShortUrlAccessBean) {
            bean = (CurrentShortUrlAccessBean) currentShortUrlAccess;
        } else {  // if(currentShortUrlAccess instanceof CurrentShortUrlAccess)
            bean = new CurrentShortUrlAccessBean(currentShortUrlAccess.getGuid(), currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
        }
        Boolean suc = getProxyFactory().getCurrentShortUrlAccessServiceProxy().updateCurrentShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public CurrentShortUrlAccess refreshCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param currentShortUrlAccess cannot be null.....
        if(currentShortUrlAccess == null || currentShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param currentShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param currentShortUrlAccess object or its guid is null!");
        }
        CurrentShortUrlAccessBean bean = null;
        if(currentShortUrlAccess instanceof CurrentShortUrlAccessBean) {
            bean = (CurrentShortUrlAccessBean) currentShortUrlAccess;
        } else {  // if(currentShortUrlAccess instanceof CurrentShortUrlAccess)
            bean = new CurrentShortUrlAccessBean(currentShortUrlAccess.getGuid(), currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
        }
        Boolean suc = getProxyFactory().getCurrentShortUrlAccessServiceProxy().updateCurrentShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteCurrentShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getCurrentShortUrlAccessServiceProxy().deleteCurrentShortUrlAccess(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            CurrentShortUrlAccess currentShortUrlAccess = null;
            try {
                currentShortUrlAccess = getProxyFactory().getCurrentShortUrlAccessServiceProxy().getCurrentShortUrlAccess(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch currentShortUrlAccess with a key, " + guid);
                return false;
            }
            if(currentShortUrlAccess != null) {
                String beanGuid = currentShortUrlAccess.getGuid();
                Boolean suc1 = getProxyFactory().getCurrentShortUrlAccessServiceProxy().deleteCurrentShortUrlAccess(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("currentShortUrlAccess with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param currentShortUrlAccess cannot be null.....
        if(currentShortUrlAccess == null || currentShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param currentShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param currentShortUrlAccess object or its guid is null!");
        }
        CurrentShortUrlAccessBean bean = null;
        if(currentShortUrlAccess instanceof CurrentShortUrlAccessBean) {
            bean = (CurrentShortUrlAccessBean) currentShortUrlAccess;
        } else {  // if(currentShortUrlAccess instanceof CurrentShortUrlAccess)
            // ????
            log.warning("currentShortUrlAccess is not an instance of CurrentShortUrlAccessBean.");
            bean = new CurrentShortUrlAccessBean(currentShortUrlAccess.getGuid(), currentShortUrlAccess.getTallyTime(), currentShortUrlAccess.getTallyEpoch(), currentShortUrlAccess.getCount(), currentShortUrlAccess.getShortUrl(), currentShortUrlAccess.getShortUrlDomain(), currentShortUrlAccess.getLongUrl(), currentShortUrlAccess.getLongUrlDomain(), currentShortUrlAccess.getRedirectType(), currentShortUrlAccess.getRefererDomain(), currentShortUrlAccess.getUserAgent(), currentShortUrlAccess.getLanguage(), currentShortUrlAccess.getCountry(), currentShortUrlAccess.getTalliedTime(), currentShortUrlAccess.getStartDayHour(), currentShortUrlAccess.getStartTime());
        }
        Boolean suc = getProxyFactory().getCurrentShortUrlAccessServiceProxy().deleteCurrentShortUrlAccess(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteCurrentShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getCurrentShortUrlAccessServiceProxy().deleteCurrentShortUrlAccesses(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createCurrentShortUrlAccesses(List<CurrentShortUrlAccess> currentShortUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");

        if(currentShortUrlAccesses == null) {
            log.log(Level.WARNING, "createCurrentShortUrlAccesses() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = currentShortUrlAccesses.size();
        if(size == 0) {
            log.log(Level.WARNING, "createCurrentShortUrlAccesses() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(CurrentShortUrlAccess currentShortUrlAccess : currentShortUrlAccesses) {
            String guid = createCurrentShortUrlAccess(currentShortUrlAccess);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createCurrentShortUrlAccesses() failed for at least one currentShortUrlAccess. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateCurrentShortUrlAccesses(List<CurrentShortUrlAccess> currentShortUrlAccesses) throws BaseException
    //{
    //}

}
