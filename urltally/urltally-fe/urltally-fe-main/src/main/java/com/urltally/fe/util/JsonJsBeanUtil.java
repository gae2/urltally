package com.urltally.fe.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.urltally.fe.bean.KeyValuePairStructJsBean;
import com.urltally.fe.bean.KeyValueRelationStructJsBean;
import com.urltally.fe.bean.GeoPointStructJsBean;
import com.urltally.fe.bean.GeoCoordinateStructJsBean;
import com.urltally.fe.bean.CellLatitudeLongitudeJsBean;
import com.urltally.fe.bean.StreetAddressStructJsBean;
import com.urltally.fe.bean.FullNameStructJsBean;
import com.urltally.fe.bean.ReferrerInfoStructJsBean;
import com.urltally.fe.bean.GaeAppStructJsBean;
import com.urltally.fe.bean.GaeUserStructJsBean;
import com.urltally.fe.bean.ApiConsumerJsBean;
import com.urltally.fe.bean.UserJsBean;
import com.urltally.fe.bean.AccessTallyMasterJsBean;
import com.urltally.fe.bean.AccessTallyStatusJsBean;
import com.urltally.fe.bean.MonthlyShortUrlAccessJsBean;
import com.urltally.fe.bean.WeeklyShortUrlAccessJsBean;
import com.urltally.fe.bean.DailyShortUrlAccessJsBean;
import com.urltally.fe.bean.HourlyShortUrlAccessJsBean;
import com.urltally.fe.bean.CumulativeShortUrlAccessJsBean;
import com.urltally.fe.bean.CurrentShortUrlAccessJsBean;
import com.urltally.fe.bean.TotalShortUrlAccessJsBean;
import com.urltally.fe.bean.TotalLongUrlAccessJsBean;
import com.urltally.fe.bean.ServiceInfoJsBean;
import com.urltally.fe.bean.FiveTenJsBean;


// Utility functions for combining/decomposing into json string segments.
public class JsonJsBeanUtil
{
    private static final Logger log = Logger.getLogger(JsonJsBeanUtil.class.getName());

    private JsonJsBeanUtil() {}

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> objJsonString
    public static Map<String, String> parseJsonObjectMapString(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, String> jsonObjectMap = new HashMap<String, String>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);

            JsonNode topNode = parser.readValueAsTree();
            Iterator<String> fieldNames = topNode.getFieldNames();
            
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                JsonNode leafNode = topNode.get(name);
                if(! leafNode.isNull()) {
                    // ???
                    // String value = leafNode.asText();
                    String value = leafNode.toString();
                    // ...
                    jsonObjectMap.put(name, value);
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);
                } else {
                    if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: Empty node skipped. name = " + name);
                }
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> objJsonString
    public static String generateJsonObjectMapString(Map<String, String> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for(String key : jsonObjectMap.keySet()) {
            String json = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(json == null) {
                sb.append("null");  // ????
            } else {
                sb.append(json);
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMapString(): jsonStr = " + jsonStr);

        return jsonStr;
    }


    // Input = jsonStr: { object: objJsonString }
    // Output = Map: object -> jsBean
    public static Map<String, Object> parseJsonObjectMap(String jsonStr)
    {
        if(jsonStr == null) {
            log.warning("Input jsonStr is null.");
            return null;
        }

        Map<String, Object> jsonObjectMap = new HashMap<String, Object>();

        // ???
        try {
            JsonFactory factory = new JsonFactory();
            factory.setCodec(getObjectMapper());
            JsonParser parser = factory.createJsonParser(jsonStr);
            JsonNode topNode = parser.readValueAsTree();

/*
            // TBD: Remove the loop.
            Iterator<String> fieldNames = topNode.getFieldNames();
            while(fieldNames.hasNext()) {
                String name = fieldNames.next();
                String value = topNode.get(name).asText();
                if(log.isLoggable(Level.INFO)) log.info("jsonObjectMap: name = " + name + "; value = " + value);

                if(name.equals("keyValuePairStruct")) {
                    KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("keyValueRelationStruct")) {
                    KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("geoPointStruct")) {
                    GeoPointStructJsBean bean = GeoPointStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("geoCoordinateStruct")) {
                    GeoCoordinateStructJsBean bean = GeoCoordinateStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("cellLatitudeLongitude")) {
                    CellLatitudeLongitudeJsBean bean = CellLatitudeLongitudeJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("streetAddressStruct")) {
                    StreetAddressStructJsBean bean = StreetAddressStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fullNameStruct")) {
                    FullNameStructJsBean bean = FullNameStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("referrerInfoStruct")) {
                    ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeAppStruct")) {
                    GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("gaeUserStruct")) {
                    GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("apiConsumer")) {
                    ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("user")) {
                    UserJsBean bean = UserJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("accessTallyMaster")) {
                    AccessTallyMasterJsBean bean = AccessTallyMasterJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("accessTallyStatus")) {
                    AccessTallyStatusJsBean bean = AccessTallyStatusJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("monthlyShortUrlAccess")) {
                    MonthlyShortUrlAccessJsBean bean = MonthlyShortUrlAccessJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("weeklyShortUrlAccess")) {
                    WeeklyShortUrlAccessJsBean bean = WeeklyShortUrlAccessJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("dailyShortUrlAccess")) {
                    DailyShortUrlAccessJsBean bean = DailyShortUrlAccessJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("hourlyShortUrlAccess")) {
                    HourlyShortUrlAccessJsBean bean = HourlyShortUrlAccessJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("cumulativeShortUrlAccess")) {
                    CumulativeShortUrlAccessJsBean bean = CumulativeShortUrlAccessJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("currentShortUrlAccess")) {
                    CurrentShortUrlAccessJsBean bean = CurrentShortUrlAccessJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("totalShortUrlAccess")) {
                    TotalShortUrlAccessJsBean bean = TotalShortUrlAccessJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("totalLongUrlAccess")) {
                    TotalLongUrlAccessJsBean bean = TotalLongUrlAccessJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("serviceInfo")) {
                    ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
                if(name.equals("fiveTen")) {
                    FiveTenJsBean bean = FiveTenJsBean.fromJsonString(value);
                    jsonObjectMap.put(name, bean);
                }
            }
*/

            JsonNode objNode = null;
            String objValueStr = null;
            objNode = topNode.findValue("keyValuePairStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValuePairStructJsBean bean = KeyValuePairStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValuePairStruct", bean);               
            }
            objNode = topNode.findValue("keyValueRelationStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                KeyValueRelationStructJsBean bean = KeyValueRelationStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("keyValueRelationStruct", bean);               
            }
            objNode = topNode.findValue("geoPointStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GeoPointStructJsBean bean = GeoPointStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("geoPointStruct", bean);               
            }
            objNode = topNode.findValue("geoCoordinateStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GeoCoordinateStructJsBean bean = GeoCoordinateStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("geoCoordinateStruct", bean);               
            }
            objNode = topNode.findValue("cellLatitudeLongitude");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                CellLatitudeLongitudeJsBean bean = CellLatitudeLongitudeJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("cellLatitudeLongitude", bean);               
            }
            objNode = topNode.findValue("streetAddressStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                StreetAddressStructJsBean bean = StreetAddressStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("streetAddressStruct", bean);               
            }
            objNode = topNode.findValue("fullNameStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FullNameStructJsBean bean = FullNameStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fullNameStruct", bean);               
            }
            objNode = topNode.findValue("referrerInfoStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ReferrerInfoStructJsBean bean = ReferrerInfoStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("referrerInfoStruct", bean);               
            }
            objNode = topNode.findValue("gaeAppStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeAppStructJsBean bean = GaeAppStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeAppStruct", bean);               
            }
            objNode = topNode.findValue("gaeUserStruct");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                GaeUserStructJsBean bean = GaeUserStructJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("gaeUserStruct", bean);               
            }
            objNode = topNode.findValue("apiConsumer");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ApiConsumerJsBean bean = ApiConsumerJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("apiConsumer", bean);               
            }
            objNode = topNode.findValue("user");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                UserJsBean bean = UserJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("user", bean);               
            }
            objNode = topNode.findValue("accessTallyMaster");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AccessTallyMasterJsBean bean = AccessTallyMasterJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("accessTallyMaster", bean);               
            }
            objNode = topNode.findValue("accessTallyStatus");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                AccessTallyStatusJsBean bean = AccessTallyStatusJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("accessTallyStatus", bean);               
            }
            objNode = topNode.findValue("monthlyShortUrlAccess");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                MonthlyShortUrlAccessJsBean bean = MonthlyShortUrlAccessJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("monthlyShortUrlAccess", bean);               
            }
            objNode = topNode.findValue("weeklyShortUrlAccess");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                WeeklyShortUrlAccessJsBean bean = WeeklyShortUrlAccessJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("weeklyShortUrlAccess", bean);               
            }
            objNode = topNode.findValue("dailyShortUrlAccess");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                DailyShortUrlAccessJsBean bean = DailyShortUrlAccessJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("dailyShortUrlAccess", bean);               
            }
            objNode = topNode.findValue("hourlyShortUrlAccess");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                HourlyShortUrlAccessJsBean bean = HourlyShortUrlAccessJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("hourlyShortUrlAccess", bean);               
            }
            objNode = topNode.findValue("cumulativeShortUrlAccess");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                CumulativeShortUrlAccessJsBean bean = CumulativeShortUrlAccessJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("cumulativeShortUrlAccess", bean);               
            }
            objNode = topNode.findValue("currentShortUrlAccess");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                CurrentShortUrlAccessJsBean bean = CurrentShortUrlAccessJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("currentShortUrlAccess", bean);               
            }
            objNode = topNode.findValue("totalShortUrlAccess");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TotalShortUrlAccessJsBean bean = TotalShortUrlAccessJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("totalShortUrlAccess", bean);               
            }
            objNode = topNode.findValue("totalLongUrlAccess");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                TotalLongUrlAccessJsBean bean = TotalLongUrlAccessJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("totalLongUrlAccess", bean);               
            }
            objNode = topNode.findValue("serviceInfo");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                ServiceInfoJsBean bean = ServiceInfoJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("serviceInfo", bean);               
            }
            objNode = topNode.findValue("fiveTen");
            if(objNode != null) {
                objValueStr = objNode.toString();   // ????
                FiveTenJsBean bean = FiveTenJsBean.fromJsonString(objValueStr);
                jsonObjectMap.put("fiveTen", bean);               
            }
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        
        return jsonObjectMap;
    }

    // Output = jsonStr: { object: objJsonString }
    // Input = Map: object -> jsBean
    public static String generateJsonObjectMap(Map<String, Object> jsonObjectMap)
    {
        if(jsonObjectMap == null) {
            log.warning("Input jsonObjectMap is null.");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        // TBD: Remove the loop.
        for(String key : jsonObjectMap.keySet()) {
            Object jsonObj = jsonObjectMap.get(key);
            sb.append("\"").append(key).append("\":");
            if(jsonObj == null) {
                sb.append("null");  // ????
            } else {
                if(jsonObj instanceof KeyValuePairStructJsBean) {
                    sb.append(((KeyValuePairStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof KeyValueRelationStructJsBean) {
                    sb.append(((KeyValueRelationStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GeoPointStructJsBean) {
                    sb.append(((GeoPointStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GeoCoordinateStructJsBean) {
                    sb.append(((GeoCoordinateStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof CellLatitudeLongitudeJsBean) {
                    sb.append(((CellLatitudeLongitudeJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof StreetAddressStructJsBean) {
                    sb.append(((StreetAddressStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FullNameStructJsBean) {
                    sb.append(((FullNameStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ReferrerInfoStructJsBean) {
                    sb.append(((ReferrerInfoStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeAppStructJsBean) {
                    sb.append(((GaeAppStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof GaeUserStructJsBean) {
                    sb.append(((GaeUserStructJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ApiConsumerJsBean) {
                    sb.append(((ApiConsumerJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof UserJsBean) {
                    sb.append(((UserJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AccessTallyMasterJsBean) {
                    sb.append(((AccessTallyMasterJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof AccessTallyStatusJsBean) {
                    sb.append(((AccessTallyStatusJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof MonthlyShortUrlAccessJsBean) {
                    sb.append(((MonthlyShortUrlAccessJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof WeeklyShortUrlAccessJsBean) {
                    sb.append(((WeeklyShortUrlAccessJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof DailyShortUrlAccessJsBean) {
                    sb.append(((DailyShortUrlAccessJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof HourlyShortUrlAccessJsBean) {
                    sb.append(((HourlyShortUrlAccessJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof CumulativeShortUrlAccessJsBean) {
                    sb.append(((CumulativeShortUrlAccessJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof CurrentShortUrlAccessJsBean) {
                    sb.append(((CurrentShortUrlAccessJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TotalShortUrlAccessJsBean) {
                    sb.append(((TotalShortUrlAccessJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof TotalLongUrlAccessJsBean) {
                    sb.append(((TotalLongUrlAccessJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof ServiceInfoJsBean) {
                    sb.append(((ServiceInfoJsBean) jsonObj).toJsonString());
                }
                if(jsonObj instanceof FiveTenJsBean) {
                    sb.append(((FiveTenJsBean) jsonObj).toJsonString());
                }
            }
            sb.append(",");
        }
        String innerStr = sb.toString();
        if(innerStr.endsWith(",")) {
            innerStr = innerStr.substring(0, innerStr.length() - 1);
        }
        String jsonStr = "{" + innerStr + "}";
        if(log.isLoggable(Level.INFO)) log.info("generateJsonObjectMap(): jsonStr = " + jsonStr);

        return jsonStr;
    }
    
    
}
