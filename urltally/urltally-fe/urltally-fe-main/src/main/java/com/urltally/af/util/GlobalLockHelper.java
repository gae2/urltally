package com.urltally.af.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;

import com.urltally.ws.ApiConsumer;
import com.urltally.ws.User;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.ws.ServiceInfo;
import com.urltally.ws.FiveTen;


// Temporary
public final class GlobalLockHelper
{
    private static final Logger log = Logger.getLogger(GlobalLockHelper.class.getName());


    // Singleton
    private GlobalLockHelper() {}

    private static final class GlobalLockHelperHolder
    {
        private static final GlobalLockHelper INSTANCE = new GlobalLockHelper();
    }
    public static GlobalLockHelper getInstance()
    {
        return GlobalLockHelperHolder.INSTANCE;
    }


    // temporary
    // Poorman's "global locking" using a distributed cache...
    private static Cache getCache()
    {
        // 30 seconds
        return CacheHelper.getFlashInstance().getCache();
    }


    private static String getApiConsumerLockKey(ApiConsumer apiConsumer)
    {
        // Note: We assume that the arg apiConsumer is not null.
        return "ApiConsumer-Processing-Lock-" + apiConsumer.getGuid();
    }
    public String lockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            // The stored value is not important.
            String val = apiConsumer.getGuid();
            getCache().put(getApiConsumerLockKey(apiConsumer), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            String val = apiConsumer.getGuid();
            getCache().remove(getApiConsumerLockKey(apiConsumer));
            return val;
        } else {
            return null;
        }
    }
    public boolean isApiConsumerLocked(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            return getCache().containsKey(getApiConsumerLockKey(apiConsumer)); 
        } else {
            return false;
        }
    }

    private static String getUserLockKey(User user)
    {
        // Note: We assume that the arg user is not null.
        return "User-Processing-Lock-" + user.getGuid();
    }
    public String lockUser(User user)
    {
        if(getCache() != null && user != null) {
            // The stored value is not important.
            String val = user.getGuid();
            getCache().put(getUserLockKey(user), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUser(User user)
    {
        if(getCache() != null && user != null) {
            String val = user.getGuid();
            getCache().remove(getUserLockKey(user));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserLocked(User user)
    {
        if(getCache() != null && user != null) {
            return getCache().containsKey(getUserLockKey(user)); 
        } else {
            return false;
        }
    }

    private static String getAccessTallyMasterLockKey(AccessTallyMaster accessTallyMaster)
    {
        // Note: We assume that the arg accessTallyMaster is not null.
        return "AccessTallyMaster-Processing-Lock-" + accessTallyMaster.getGuid();
    }
    public String lockAccessTallyMaster(AccessTallyMaster accessTallyMaster)
    {
        if(getCache() != null && accessTallyMaster != null) {
            // The stored value is not important.
            String val = accessTallyMaster.getGuid();
            getCache().put(getAccessTallyMasterLockKey(accessTallyMaster), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockAccessTallyMaster(AccessTallyMaster accessTallyMaster)
    {
        if(getCache() != null && accessTallyMaster != null) {
            String val = accessTallyMaster.getGuid();
            getCache().remove(getAccessTallyMasterLockKey(accessTallyMaster));
            return val;
        } else {
            return null;
        }
    }
    public boolean isAccessTallyMasterLocked(AccessTallyMaster accessTallyMaster)
    {
        if(getCache() != null && accessTallyMaster != null) {
            return getCache().containsKey(getAccessTallyMasterLockKey(accessTallyMaster)); 
        } else {
            return false;
        }
    }

    private static String getAccessTallyStatusLockKey(AccessTallyStatus accessTallyStatus)
    {
        // Note: We assume that the arg accessTallyStatus is not null.
        return "AccessTallyStatus-Processing-Lock-" + accessTallyStatus.getGuid();
    }
    public String lockAccessTallyStatus(AccessTallyStatus accessTallyStatus)
    {
        if(getCache() != null && accessTallyStatus != null) {
            // The stored value is not important.
            String val = accessTallyStatus.getGuid();
            getCache().put(getAccessTallyStatusLockKey(accessTallyStatus), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockAccessTallyStatus(AccessTallyStatus accessTallyStatus)
    {
        if(getCache() != null && accessTallyStatus != null) {
            String val = accessTallyStatus.getGuid();
            getCache().remove(getAccessTallyStatusLockKey(accessTallyStatus));
            return val;
        } else {
            return null;
        }
    }
    public boolean isAccessTallyStatusLocked(AccessTallyStatus accessTallyStatus)
    {
        if(getCache() != null && accessTallyStatus != null) {
            return getCache().containsKey(getAccessTallyStatusLockKey(accessTallyStatus)); 
        } else {
            return false;
        }
    }

    private static String getMonthlyShortUrlAccessLockKey(MonthlyShortUrlAccess monthlyShortUrlAccess)
    {
        // Note: We assume that the arg monthlyShortUrlAccess is not null.
        return "MonthlyShortUrlAccess-Processing-Lock-" + monthlyShortUrlAccess.getGuid();
    }
    public String lockMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess)
    {
        if(getCache() != null && monthlyShortUrlAccess != null) {
            // The stored value is not important.
            String val = monthlyShortUrlAccess.getGuid();
            getCache().put(getMonthlyShortUrlAccessLockKey(monthlyShortUrlAccess), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess)
    {
        if(getCache() != null && monthlyShortUrlAccess != null) {
            String val = monthlyShortUrlAccess.getGuid();
            getCache().remove(getMonthlyShortUrlAccessLockKey(monthlyShortUrlAccess));
            return val;
        } else {
            return null;
        }
    }
    public boolean isMonthlyShortUrlAccessLocked(MonthlyShortUrlAccess monthlyShortUrlAccess)
    {
        if(getCache() != null && monthlyShortUrlAccess != null) {
            return getCache().containsKey(getMonthlyShortUrlAccessLockKey(monthlyShortUrlAccess)); 
        } else {
            return false;
        }
    }

    private static String getWeeklyShortUrlAccessLockKey(WeeklyShortUrlAccess weeklyShortUrlAccess)
    {
        // Note: We assume that the arg weeklyShortUrlAccess is not null.
        return "WeeklyShortUrlAccess-Processing-Lock-" + weeklyShortUrlAccess.getGuid();
    }
    public String lockWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess)
    {
        if(getCache() != null && weeklyShortUrlAccess != null) {
            // The stored value is not important.
            String val = weeklyShortUrlAccess.getGuid();
            getCache().put(getWeeklyShortUrlAccessLockKey(weeklyShortUrlAccess), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess)
    {
        if(getCache() != null && weeklyShortUrlAccess != null) {
            String val = weeklyShortUrlAccess.getGuid();
            getCache().remove(getWeeklyShortUrlAccessLockKey(weeklyShortUrlAccess));
            return val;
        } else {
            return null;
        }
    }
    public boolean isWeeklyShortUrlAccessLocked(WeeklyShortUrlAccess weeklyShortUrlAccess)
    {
        if(getCache() != null && weeklyShortUrlAccess != null) {
            return getCache().containsKey(getWeeklyShortUrlAccessLockKey(weeklyShortUrlAccess)); 
        } else {
            return false;
        }
    }

    private static String getDailyShortUrlAccessLockKey(DailyShortUrlAccess dailyShortUrlAccess)
    {
        // Note: We assume that the arg dailyShortUrlAccess is not null.
        return "DailyShortUrlAccess-Processing-Lock-" + dailyShortUrlAccess.getGuid();
    }
    public String lockDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess)
    {
        if(getCache() != null && dailyShortUrlAccess != null) {
            // The stored value is not important.
            String val = dailyShortUrlAccess.getGuid();
            getCache().put(getDailyShortUrlAccessLockKey(dailyShortUrlAccess), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockDailyShortUrlAccess(DailyShortUrlAccess dailyShortUrlAccess)
    {
        if(getCache() != null && dailyShortUrlAccess != null) {
            String val = dailyShortUrlAccess.getGuid();
            getCache().remove(getDailyShortUrlAccessLockKey(dailyShortUrlAccess));
            return val;
        } else {
            return null;
        }
    }
    public boolean isDailyShortUrlAccessLocked(DailyShortUrlAccess dailyShortUrlAccess)
    {
        if(getCache() != null && dailyShortUrlAccess != null) {
            return getCache().containsKey(getDailyShortUrlAccessLockKey(dailyShortUrlAccess)); 
        } else {
            return false;
        }
    }

    private static String getHourlyShortUrlAccessLockKey(HourlyShortUrlAccess hourlyShortUrlAccess)
    {
        // Note: We assume that the arg hourlyShortUrlAccess is not null.
        return "HourlyShortUrlAccess-Processing-Lock-" + hourlyShortUrlAccess.getGuid();
    }
    public String lockHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess)
    {
        if(getCache() != null && hourlyShortUrlAccess != null) {
            // The stored value is not important.
            String val = hourlyShortUrlAccess.getGuid();
            getCache().put(getHourlyShortUrlAccessLockKey(hourlyShortUrlAccess), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockHourlyShortUrlAccess(HourlyShortUrlAccess hourlyShortUrlAccess)
    {
        if(getCache() != null && hourlyShortUrlAccess != null) {
            String val = hourlyShortUrlAccess.getGuid();
            getCache().remove(getHourlyShortUrlAccessLockKey(hourlyShortUrlAccess));
            return val;
        } else {
            return null;
        }
    }
    public boolean isHourlyShortUrlAccessLocked(HourlyShortUrlAccess hourlyShortUrlAccess)
    {
        if(getCache() != null && hourlyShortUrlAccess != null) {
            return getCache().containsKey(getHourlyShortUrlAccessLockKey(hourlyShortUrlAccess)); 
        } else {
            return false;
        }
    }

    private static String getCumulativeShortUrlAccessLockKey(CumulativeShortUrlAccess cumulativeShortUrlAccess)
    {
        // Note: We assume that the arg cumulativeShortUrlAccess is not null.
        return "CumulativeShortUrlAccess-Processing-Lock-" + cumulativeShortUrlAccess.getGuid();
    }
    public String lockCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess)
    {
        if(getCache() != null && cumulativeShortUrlAccess != null) {
            // The stored value is not important.
            String val = cumulativeShortUrlAccess.getGuid();
            getCache().put(getCumulativeShortUrlAccessLockKey(cumulativeShortUrlAccess), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockCumulativeShortUrlAccess(CumulativeShortUrlAccess cumulativeShortUrlAccess)
    {
        if(getCache() != null && cumulativeShortUrlAccess != null) {
            String val = cumulativeShortUrlAccess.getGuid();
            getCache().remove(getCumulativeShortUrlAccessLockKey(cumulativeShortUrlAccess));
            return val;
        } else {
            return null;
        }
    }
    public boolean isCumulativeShortUrlAccessLocked(CumulativeShortUrlAccess cumulativeShortUrlAccess)
    {
        if(getCache() != null && cumulativeShortUrlAccess != null) {
            return getCache().containsKey(getCumulativeShortUrlAccessLockKey(cumulativeShortUrlAccess)); 
        } else {
            return false;
        }
    }

    private static String getCurrentShortUrlAccessLockKey(CurrentShortUrlAccess currentShortUrlAccess)
    {
        // Note: We assume that the arg currentShortUrlAccess is not null.
        return "CurrentShortUrlAccess-Processing-Lock-" + currentShortUrlAccess.getGuid();
    }
    public String lockCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess)
    {
        if(getCache() != null && currentShortUrlAccess != null) {
            // The stored value is not important.
            String val = currentShortUrlAccess.getGuid();
            getCache().put(getCurrentShortUrlAccessLockKey(currentShortUrlAccess), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockCurrentShortUrlAccess(CurrentShortUrlAccess currentShortUrlAccess)
    {
        if(getCache() != null && currentShortUrlAccess != null) {
            String val = currentShortUrlAccess.getGuid();
            getCache().remove(getCurrentShortUrlAccessLockKey(currentShortUrlAccess));
            return val;
        } else {
            return null;
        }
    }
    public boolean isCurrentShortUrlAccessLocked(CurrentShortUrlAccess currentShortUrlAccess)
    {
        if(getCache() != null && currentShortUrlAccess != null) {
            return getCache().containsKey(getCurrentShortUrlAccessLockKey(currentShortUrlAccess)); 
        } else {
            return false;
        }
    }

    private static String getTotalShortUrlAccessLockKey(TotalShortUrlAccess totalShortUrlAccess)
    {
        // Note: We assume that the arg totalShortUrlAccess is not null.
        return "TotalShortUrlAccess-Processing-Lock-" + totalShortUrlAccess.getGuid();
    }
    public String lockTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess)
    {
        if(getCache() != null && totalShortUrlAccess != null) {
            // The stored value is not important.
            String val = totalShortUrlAccess.getGuid();
            getCache().put(getTotalShortUrlAccessLockKey(totalShortUrlAccess), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess)
    {
        if(getCache() != null && totalShortUrlAccess != null) {
            String val = totalShortUrlAccess.getGuid();
            getCache().remove(getTotalShortUrlAccessLockKey(totalShortUrlAccess));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTotalShortUrlAccessLocked(TotalShortUrlAccess totalShortUrlAccess)
    {
        if(getCache() != null && totalShortUrlAccess != null) {
            return getCache().containsKey(getTotalShortUrlAccessLockKey(totalShortUrlAccess)); 
        } else {
            return false;
        }
    }

    private static String getTotalLongUrlAccessLockKey(TotalLongUrlAccess totalLongUrlAccess)
    {
        // Note: We assume that the arg totalLongUrlAccess is not null.
        return "TotalLongUrlAccess-Processing-Lock-" + totalLongUrlAccess.getGuid();
    }
    public String lockTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess)
    {
        if(getCache() != null && totalLongUrlAccess != null) {
            // The stored value is not important.
            String val = totalLongUrlAccess.getGuid();
            getCache().put(getTotalLongUrlAccessLockKey(totalLongUrlAccess), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTotalLongUrlAccess(TotalLongUrlAccess totalLongUrlAccess)
    {
        if(getCache() != null && totalLongUrlAccess != null) {
            String val = totalLongUrlAccess.getGuid();
            getCache().remove(getTotalLongUrlAccessLockKey(totalLongUrlAccess));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTotalLongUrlAccessLocked(TotalLongUrlAccess totalLongUrlAccess)
    {
        if(getCache() != null && totalLongUrlAccess != null) {
            return getCache().containsKey(getTotalLongUrlAccessLockKey(totalLongUrlAccess)); 
        } else {
            return false;
        }
    }

    private static String getServiceInfoLockKey(ServiceInfo serviceInfo)
    {
        // Note: We assume that the arg serviceInfo is not null.
        return "ServiceInfo-Processing-Lock-" + serviceInfo.getGuid();
    }
    public String lockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            // The stored value is not important.
            String val = serviceInfo.getGuid();
            getCache().put(getServiceInfoLockKey(serviceInfo), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            String val = serviceInfo.getGuid();
            getCache().remove(getServiceInfoLockKey(serviceInfo));
            return val;
        } else {
            return null;
        }
    }
    public boolean isServiceInfoLocked(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            return getCache().containsKey(getServiceInfoLockKey(serviceInfo)); 
        } else {
            return false;
        }
    }

    private static String getFiveTenLockKey(FiveTen fiveTen)
    {
        // Note: We assume that the arg fiveTen is not null.
        return "FiveTen-Processing-Lock-" + fiveTen.getGuid();
    }
    public String lockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            // The stored value is not important.
            String val = fiveTen.getGuid();
            getCache().put(getFiveTenLockKey(fiveTen), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            String val = fiveTen.getGuid();
            getCache().remove(getFiveTenLockKey(fiveTen));
            return val;
        } else {
            return null;
        }
    }
    public boolean isFiveTenLocked(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            return getCache().containsKey(getFiveTenLockKey(fiveTen)); 
        } else {
            return false;
        }
    }

 
}
