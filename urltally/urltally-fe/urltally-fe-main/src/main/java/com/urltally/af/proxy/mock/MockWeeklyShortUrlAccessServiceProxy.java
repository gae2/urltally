package com.urltally.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.ws.service.WeeklyShortUrlAccessService;
import com.urltally.af.proxy.WeeklyShortUrlAccessServiceProxy;


// MockWeeklyShortUrlAccessServiceProxy is a decorator.
// It can be used as a base class to mock WeeklyShortUrlAccessServiceProxy objects.
public abstract class MockWeeklyShortUrlAccessServiceProxy implements WeeklyShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(MockWeeklyShortUrlAccessServiceProxy.class.getName());

    // MockWeeklyShortUrlAccessServiceProxy uses the decorator design pattern.
    private WeeklyShortUrlAccessServiceProxy decoratedProxy;

    public MockWeeklyShortUrlAccessServiceProxy(WeeklyShortUrlAccessServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected WeeklyShortUrlAccessServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(WeeklyShortUrlAccessServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public WeeklyShortUrlAccess getWeeklyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.getWeeklyShortUrlAccess(guid);
    }

    @Override
    public Object getWeeklyShortUrlAccess(String guid, String field) throws BaseException
    {
        return decoratedProxy.getWeeklyShortUrlAccess(guid, field);       
    }

    @Override
    public List<WeeklyShortUrlAccess> getWeeklyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getWeeklyShortUrlAccesses(guids);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses() throws BaseException
    {
        return getAllWeeklyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllWeeklyShortUrlAccesses(ordering, offset, count);
        return getAllWeeklyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> getAllWeeklyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllWeeklyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllWeeklyShortUrlAccessKeys(ordering, offset, count);
        return getAllWeeklyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllWeeklyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllWeeklyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<WeeklyShortUrlAccess> findWeeklyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findWeeklyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findWeeklyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findWeeklyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createWeeklyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {
        return decoratedProxy.createWeeklyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
    }

    @Override
    public String createWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        return decoratedProxy.createWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }

    @Override
    public Boolean updateWeeklyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer week) throws BaseException
    {
        return decoratedProxy.updateWeeklyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, week);
    }

    @Override
    public Boolean updateWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        return decoratedProxy.updateWeeklyShortUrlAccess(weeklyShortUrlAccess);
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.deleteWeeklyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteWeeklyShortUrlAccess(WeeklyShortUrlAccess weeklyShortUrlAccess) throws BaseException
    {
        String guid = weeklyShortUrlAccess.getGuid();
        return deleteWeeklyShortUrlAccess(guid);
    }

    @Override
    public Long deleteWeeklyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteWeeklyShortUrlAccesses(filter, params, values);
    }

}
