package com.urltally.af.proxy.local;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyStatus;
// import com.urltally.ws.bean.AccessTallyStatusBean;
import com.urltally.ws.service.AccessTallyStatusService;
import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.af.proxy.AccessTallyStatusServiceProxy;


public class LocalAccessTallyStatusServiceProxy extends BaseLocalServiceProxy implements AccessTallyStatusServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalAccessTallyStatusServiceProxy.class.getName());

    public LocalAccessTallyStatusServiceProxy()
    {
    }

    @Override
    public AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException
    {
        AccessTallyStatus serverBean = getAccessTallyStatusService().getAccessTallyStatus(guid);
        AccessTallyStatus appBean = convertServerAccessTallyStatusBeanToAppBean(serverBean);
        return appBean;
    }

    @Override
    public Object getAccessTallyStatus(String guid, String field) throws BaseException
    {
        return getAccessTallyStatusService().getAccessTallyStatus(guid, field);       
    }

    @Override
    public List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        List<AccessTallyStatus> serverBeanList = getAccessTallyStatusService().getAccessTallyStatuses(guids);
        List<AccessTallyStatus> appBeanList = convertServerAccessTallyStatusBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException
    {
        return getAllAccessTallyStatuses(null, null, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getAccessTallyStatusService().getAllAccessTallyStatuses(ordering, offset, count);
        return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<AccessTallyStatus> serverBeanList = getAccessTallyStatusService().getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
        List<AccessTallyStatus> appBeanList = convertServerAccessTallyStatusBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getAccessTallyStatusService().getAllAccessTallyStatusKeys(ordering, offset, count);
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getAccessTallyStatusService().getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getAccessTallyStatusService().findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        List<AccessTallyStatus> serverBeanList = getAccessTallyStatusService().findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        List<AccessTallyStatus> appBeanList = convertServerAccessTallyStatusBeanListToAppBeanList(serverBeanList);
        return appBeanList;
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getAccessTallyStatusService().findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getAccessTallyStatusService().findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getAccessTallyStatusService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        return getAccessTallyStatusService().createAccessTallyStatus(remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
    }

    @Override
    public String createAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        com.urltally.ws.bean.AccessTallyStatusBean serverBean =  convertAppAccessTallyStatusBeanToServerBean(accessTallyStatus);
        return getAccessTallyStatusService().createAccessTallyStatus(serverBean);
    }

    @Override
    public Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        return getAccessTallyStatusService().updateAccessTallyStatus(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
    }

    @Override
    public Boolean updateAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        com.urltally.ws.bean.AccessTallyStatusBean serverBean =  convertAppAccessTallyStatusBeanToServerBean(accessTallyStatus);
        return getAccessTallyStatusService().updateAccessTallyStatus(serverBean);
    }

    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        return getAccessTallyStatusService().deleteAccessTallyStatus(guid);
    }

    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        com.urltally.ws.bean.AccessTallyStatusBean serverBean =  convertAppAccessTallyStatusBeanToServerBean(accessTallyStatus);
        return getAccessTallyStatusService().deleteAccessTallyStatus(serverBean);
    }

    @Override
    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException
    {
        return getAccessTallyStatusService().deleteAccessTallyStatuses(filter, params, values);
    }




    public static AccessTallyStatusBean convertServerAccessTallyStatusBeanToAppBean(AccessTallyStatus serverBean)
    {
        AccessTallyStatusBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Null bean is returned.");
        } else {
            bean = new AccessTallyStatusBean();
            bean.setGuid(serverBean.getGuid());
            bean.setRemoteRecordGuid(serverBean.getRemoteRecordGuid());
            bean.setShortUrl(serverBean.getShortUrl());
            bean.setTallyType(serverBean.getTallyType());
            bean.setTallyTime(serverBean.getTallyTime());
            bean.setTallyEpoch(serverBean.getTallyEpoch());
            bean.setProcessed(serverBean.isProcessed());
            bean.setCreatedTime(serverBean.getCreatedTime());
            bean.setModifiedTime(serverBean.getModifiedTime());
        }
        return bean;
    }

    public static List<AccessTallyStatus> convertServerAccessTallyStatusBeanListToAppBeanList(List<AccessTallyStatus> serverBeanList)
    {
        if(serverBeanList == null) {
            log.log(Level.INFO, "Server bean list is null. Null list is returned.");
            return null;                
        }
        List<AccessTallyStatus> beanList = new ArrayList<AccessTallyStatus>();
        if(serverBeanList.isEmpty()) {
            log.log(Level.INFO, "Server bean list is empty. Empty list is returned.");
        } else {
            for(AccessTallyStatus sb : serverBeanList) {
                AccessTallyStatusBean bean = convertServerAccessTallyStatusBeanToAppBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }



    public static com.urltally.ws.bean.AccessTallyStatusBean convertAppAccessTallyStatusBeanToServerBean(AccessTallyStatus appBean)
    {
        com.urltally.ws.bean.AccessTallyStatusBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Null bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.AccessTallyStatusBean();
            bean.setGuid(appBean.getGuid());
            bean.setRemoteRecordGuid(appBean.getRemoteRecordGuid());
            bean.setShortUrl(appBean.getShortUrl());
            bean.setTallyType(appBean.getTallyType());
            bean.setTallyTime(appBean.getTallyTime());
            bean.setTallyEpoch(appBean.getTallyEpoch());
            bean.setProcessed(appBean.isProcessed());
            bean.setCreatedTime(appBean.getCreatedTime());
            bean.setModifiedTime(appBean.getModifiedTime());
        }
        return bean;
    }

    public static List<AccessTallyStatus> convertAppAccessTallyStatusBeanListToServerBeanList(List<AccessTallyStatus> appBeanList)
    {
        if(appBeanList == null) {
            log.log(Level.INFO, "App bean list is null. Null list is returned.");
            return null;                
        }
        List<AccessTallyStatus> beanList = new ArrayList<AccessTallyStatus>();
        if(appBeanList.isEmpty()) {
            log.log(Level.INFO, "App bean list is empty. Empty list is returned.");
        } else {
            for(AccessTallyStatus sb : appBeanList) {
                com.urltally.ws.bean.AccessTallyStatusBean bean = convertAppAccessTallyStatusBeanToServerBean(sb);
                beanList.add(bean);                            
            }
        }
        return beanList;
    }


}
