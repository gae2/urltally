package com.urltally.af.proxy.mock;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.ApiConsumerServiceProxy;
import com.urltally.af.proxy.UserServiceProxy;
import com.urltally.af.proxy.AccessTallyMasterServiceProxy;
import com.urltally.af.proxy.AccessTallyStatusServiceProxy;
import com.urltally.af.proxy.MonthlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.WeeklyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.DailyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.HourlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.CumulativeShortUrlAccessServiceProxy;
import com.urltally.af.proxy.CurrentShortUrlAccessServiceProxy;
import com.urltally.af.proxy.TotalShortUrlAccessServiceProxy;
import com.urltally.af.proxy.TotalLongUrlAccessServiceProxy;
import com.urltally.af.proxy.ServiceInfoServiceProxy;
import com.urltally.af.proxy.FiveTenServiceProxy;


// Create your own mock object factory using MockProxyFactory as a template.
public class MockProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(MockProxyFactory.class.getName());

    // Using the Decorator pattern.
    private AbstractProxyFactory decoratedProxyFactory;
    private MockProxyFactory()
    {
        this(null);   // ????
    }
    private MockProxyFactory(AbstractProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }

    // Initialization-on-demand holder.
    private static class MockProxyFactoryHolder
    {
        private static final MockProxyFactory INSTANCE = new MockProxyFactory();
    }

    // Singleton method
    public static MockProxyFactory getInstance()
    {
        return MockProxyFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public AbstractProxyFactory getDecoratedProxyFactory()
    {
        return decoratedProxyFactory;
    }
    public void setDecoratedProxyFactory(AbstractProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }


    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new MockApiConsumerServiceProxy(decoratedProxyFactory.getApiConsumerServiceProxy()) {};
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new MockUserServiceProxy(decoratedProxyFactory.getUserServiceProxy()) {};
    }

    @Override
    public AccessTallyMasterServiceProxy getAccessTallyMasterServiceProxy()
    {
        return new MockAccessTallyMasterServiceProxy(decoratedProxyFactory.getAccessTallyMasterServiceProxy()) {};
    }

    @Override
    public AccessTallyStatusServiceProxy getAccessTallyStatusServiceProxy()
    {
        return new MockAccessTallyStatusServiceProxy(decoratedProxyFactory.getAccessTallyStatusServiceProxy()) {};
    }

    @Override
    public MonthlyShortUrlAccessServiceProxy getMonthlyShortUrlAccessServiceProxy()
    {
        return new MockMonthlyShortUrlAccessServiceProxy(decoratedProxyFactory.getMonthlyShortUrlAccessServiceProxy()) {};
    }

    @Override
    public WeeklyShortUrlAccessServiceProxy getWeeklyShortUrlAccessServiceProxy()
    {
        return new MockWeeklyShortUrlAccessServiceProxy(decoratedProxyFactory.getWeeklyShortUrlAccessServiceProxy()) {};
    }

    @Override
    public DailyShortUrlAccessServiceProxy getDailyShortUrlAccessServiceProxy()
    {
        return new MockDailyShortUrlAccessServiceProxy(decoratedProxyFactory.getDailyShortUrlAccessServiceProxy()) {};
    }

    @Override
    public HourlyShortUrlAccessServiceProxy getHourlyShortUrlAccessServiceProxy()
    {
        return new MockHourlyShortUrlAccessServiceProxy(decoratedProxyFactory.getHourlyShortUrlAccessServiceProxy()) {};
    }

    @Override
    public CumulativeShortUrlAccessServiceProxy getCumulativeShortUrlAccessServiceProxy()
    {
        return new MockCumulativeShortUrlAccessServiceProxy(decoratedProxyFactory.getCumulativeShortUrlAccessServiceProxy()) {};
    }

    @Override
    public CurrentShortUrlAccessServiceProxy getCurrentShortUrlAccessServiceProxy()
    {
        return new MockCurrentShortUrlAccessServiceProxy(decoratedProxyFactory.getCurrentShortUrlAccessServiceProxy()) {};
    }

    @Override
    public TotalShortUrlAccessServiceProxy getTotalShortUrlAccessServiceProxy()
    {
        return new MockTotalShortUrlAccessServiceProxy(decoratedProxyFactory.getTotalShortUrlAccessServiceProxy()) {};
    }

    @Override
    public TotalLongUrlAccessServiceProxy getTotalLongUrlAccessServiceProxy()
    {
        return new MockTotalLongUrlAccessServiceProxy(decoratedProxyFactory.getTotalLongUrlAccessServiceProxy()) {};
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new MockServiceInfoServiceProxy(decoratedProxyFactory.getServiceInfoServiceProxy()) {};
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new MockFiveTenServiceProxy(decoratedProxyFactory.getFiveTenServiceProxy()) {};
    }

}
