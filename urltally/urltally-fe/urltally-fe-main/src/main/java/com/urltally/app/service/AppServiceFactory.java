package com.urltally.app.service;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.af.service.AbstractServiceFactory;
import com.urltally.af.service.ApiConsumerService;
import com.urltally.af.service.UserService;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.af.service.ServiceInfoService;
import com.urltally.af.service.FiveTenService;

public class AppServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(AppServiceFactory.class.getName());

    private AppServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AppServiceFactoryHolder
    {
        private static final AppServiceFactory INSTANCE = new AppServiceFactory();
    }

    // Singleton method
    public static AppServiceFactory getInstance()
    {
        return AppServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerAppService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserAppService();
    }

    @Override
    public AccessTallyMasterService getAccessTallyMasterService()
    {
        return new AccessTallyMasterAppService();
    }

    @Override
    public AccessTallyStatusService getAccessTallyStatusService()
    {
        return new AccessTallyStatusAppService();
    }

    @Override
    public MonthlyShortUrlAccessService getMonthlyShortUrlAccessService()
    {
        return new MonthlyShortUrlAccessAppService();
    }

    @Override
    public WeeklyShortUrlAccessService getWeeklyShortUrlAccessService()
    {
        return new WeeklyShortUrlAccessAppService();
    }

    @Override
    public DailyShortUrlAccessService getDailyShortUrlAccessService()
    {
        return new DailyShortUrlAccessAppService();
    }

    @Override
    public HourlyShortUrlAccessService getHourlyShortUrlAccessService()
    {
        return new HourlyShortUrlAccessAppService();
    }

    @Override
    public CumulativeShortUrlAccessService getCumulativeShortUrlAccessService()
    {
        return new CumulativeShortUrlAccessAppService();
    }

    @Override
    public CurrentShortUrlAccessService getCurrentShortUrlAccessService()
    {
        return new CurrentShortUrlAccessAppService();
    }

    @Override
    public TotalShortUrlAccessService getTotalShortUrlAccessService()
    {
        return new TotalShortUrlAccessAppService();
    }

    @Override
    public TotalLongUrlAccessService getTotalLongUrlAccessService()
    {
        return new TotalLongUrlAccessAppService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoAppService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenAppService();
    }


}
