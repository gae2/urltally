package com.urltally.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.urltally.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TotalShortUrlAccessJsBean implements Serializable, Cloneable  //, TotalShortUrlAccess
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String tallyType;
    private String tallyTime;
    private Long tallyEpoch;
    private Integer count;
    private String shortUrl;
    private String shortUrlDomain;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public TotalShortUrlAccessJsBean()
    {
        //this((String) null);
    }
    public TotalShortUrlAccessJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null);
    }
    public TotalShortUrlAccessJsBean(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain)
    {
        this(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, null, null);
    }
    public TotalShortUrlAccessJsBean(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.tallyType = tallyType;
        this.tallyTime = tallyTime;
        this.tallyEpoch = tallyEpoch;
        this.count = count;
        this.shortUrl = shortUrl;
        this.shortUrlDomain = shortUrlDomain;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public TotalShortUrlAccessJsBean(TotalShortUrlAccessJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setTallyType(bean.getTallyType());
            setTallyTime(bean.getTallyTime());
            setTallyEpoch(bean.getTallyEpoch());
            setCount(bean.getCount());
            setShortUrl(bean.getShortUrl());
            setShortUrlDomain(bean.getShortUrlDomain());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static TotalShortUrlAccessJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        TotalShortUrlAccessJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(TotalShortUrlAccessJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, TotalShortUrlAccessJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getTallyType()
    {
        return this.tallyType;
    }
    public void setTallyType(String tallyType)
    {
        this.tallyType = tallyType;
    }

    public String getTallyTime()
    {
        return this.tallyTime;
    }
    public void setTallyTime(String tallyTime)
    {
        this.tallyTime = tallyTime;
    }

    public Long getTallyEpoch()
    {
        return this.tallyEpoch;
    }
    public void setTallyEpoch(Long tallyEpoch)
    {
        this.tallyEpoch = tallyEpoch;
    }

    public Integer getCount()
    {
        return this.count;
    }
    public void setCount(Integer count)
    {
        this.count = count;
    }

    public String getShortUrl()
    {
        return this.shortUrl;
    }
    public void setShortUrl(String shortUrl)
    {
        this.shortUrl = shortUrl;
    }

    public String getShortUrlDomain()
    {
        return this.shortUrlDomain;
    }
    public void setShortUrlDomain(String shortUrlDomain)
    {
        this.shortUrlDomain = shortUrlDomain;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("tallyType:null, ");
        sb.append("tallyTime:null, ");
        sb.append("tallyEpoch:0, ");
        sb.append("count:0, ");
        sb.append("shortUrl:null, ");
        sb.append("shortUrlDomain:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("tallyType:");
        if(this.getTallyType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTallyType()).append("\", ");
        }
        sb.append("tallyTime:");
        if(this.getTallyTime() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTallyTime()).append("\", ");
        }
        sb.append("tallyEpoch:" + this.getTallyEpoch()).append(", ");
        sb.append("count:" + this.getCount()).append(", ");
        sb.append("shortUrl:");
        if(this.getShortUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrl()).append("\", ");
        }
        sb.append("shortUrlDomain:");
        if(this.getShortUrlDomain() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getShortUrlDomain()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getTallyType() != null) {
            sb.append("\"tallyType\":").append("\"").append(this.getTallyType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyType\":").append("null, ");
        }
        if(this.getTallyTime() != null) {
            sb.append("\"tallyTime\":").append("\"").append(this.getTallyTime()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyTime\":").append("null, ");
        }
        if(this.getTallyEpoch() != null) {
            sb.append("\"tallyEpoch\":").append("").append(this.getTallyEpoch()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tallyEpoch\":").append("null, ");
        }
        if(this.getCount() != null) {
            sb.append("\"count\":").append("").append(this.getCount()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"count\":").append("null, ");
        }
        if(this.getShortUrl() != null) {
            sb.append("\"shortUrl\":").append("\"").append(this.getShortUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrl\":").append("null, ");
        }
        if(this.getShortUrlDomain() != null) {
            sb.append("\"shortUrlDomain\":").append("\"").append(this.getShortUrlDomain()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"shortUrlDomain\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("tallyType = " + this.tallyType).append(";");
        sb.append("tallyTime = " + this.tallyTime).append(";");
        sb.append("tallyEpoch = " + this.tallyEpoch).append(";");
        sb.append("count = " + this.count).append(";");
        sb.append("shortUrl = " + this.shortUrl).append(";");
        sb.append("shortUrlDomain = " + this.shortUrlDomain).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        TotalShortUrlAccessJsBean cloned = new TotalShortUrlAccessJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setTallyType(this.getTallyType());   
        cloned.setTallyTime(this.getTallyTime());   
        cloned.setTallyEpoch(this.getTallyEpoch());   
        cloned.setCount(this.getCount());   
        cloned.setShortUrl(this.getShortUrl());   
        cloned.setShortUrlDomain(this.getShortUrlDomain());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
