package com.urltally.af.service;

import java.util.List;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyStatus;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface AccessTallyStatusService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException;
    Object getAccessTallyStatus(String guid, String field) throws BaseException;
    List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException;
    List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException;
    /* @Deprecated */ List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException;
    List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException;
    //String createAccessTallyStatus(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return AccessTallyStatus?)
    String createAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException;
    AccessTallyStatus constructAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException;
    Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException;
    //Boolean updateAccessTallyStatus(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException;
    AccessTallyStatus refreshAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException;
    Boolean deleteAccessTallyStatus(String guid) throws BaseException;
    Boolean deleteAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException;
    Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException;
//    Boolean updateAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException;

}
