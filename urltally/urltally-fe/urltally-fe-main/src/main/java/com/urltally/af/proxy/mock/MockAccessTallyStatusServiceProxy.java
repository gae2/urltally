package com.urltally.af.proxy.mock;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.ws.service.AccessTallyStatusService;
import com.urltally.af.proxy.AccessTallyStatusServiceProxy;


// MockAccessTallyStatusServiceProxy is a decorator.
// It can be used as a base class to mock AccessTallyStatusServiceProxy objects.
public abstract class MockAccessTallyStatusServiceProxy implements AccessTallyStatusServiceProxy
{
    private static final Logger log = Logger.getLogger(MockAccessTallyStatusServiceProxy.class.getName());

    // MockAccessTallyStatusServiceProxy uses the decorator design pattern.
    private AccessTallyStatusServiceProxy decoratedProxy;

    public MockAccessTallyStatusServiceProxy(AccessTallyStatusServiceProxy decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected AccessTallyStatusServiceProxy getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(AccessTallyStatusServiceProxy decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException
    {
        return decoratedProxy.getAccessTallyStatus(guid);
    }

    @Override
    public Object getAccessTallyStatus(String guid, String field) throws BaseException
    {
        return decoratedProxy.getAccessTallyStatus(guid, field);       
    }

    @Override
    public List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getAccessTallyStatuses(guids);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException
    {
        return getAllAccessTallyStatuses(null, null, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllAccessTallyStatuses(ordering, offset, count);
        return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.getAllAccessTallyStatusKeys(ordering, offset, count);
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return decoratedProxy.findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        return decoratedProxy.createAccessTallyStatus(remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
    }

    @Override
    public String createAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        return decoratedProxy.createAccessTallyStatus(accessTallyStatus);
    }

    @Override
    public Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        return decoratedProxy.updateAccessTallyStatus(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
    }

    @Override
    public Boolean updateAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        return decoratedProxy.updateAccessTallyStatus(accessTallyStatus);
    }

    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyStatus(guid);
    }

    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        String guid = accessTallyStatus.getGuid();
        return deleteAccessTallyStatus(guid);
    }

    @Override
    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyStatuses(filter, params, values);
    }

}
