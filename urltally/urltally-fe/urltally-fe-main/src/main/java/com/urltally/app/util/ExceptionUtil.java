package com.urltally.app.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.util.CommonUtil;

public class ExceptionUtil
{
    private static final Logger log = Logger.getLogger(ExceptionUtil.class.getName());

    // Static methods only.
    private ExceptionUtil() {}


    // ...
    
    
    // Utility method...
    public static String getStackTrace(Throwable e)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

}
