package com.urltally.af.resource.mock;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

import com.urltally.ws.BaseException;
import com.urltally.ws.CommonConstants;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.exception.InternalServerErrorException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.exception.RequestConflictException;
import com.urltally.ws.exception.RequestForbiddenException;
import com.urltally.ws.exception.ResourceGoneException;
import com.urltally.ws.exception.ResourceNotFoundException;
import com.urltally.ws.exception.ServiceUnavailableException;
import com.urltally.ws.exception.resource.BaseResourceException;
import com.urltally.ws.resource.exception.BadRequestRsException;
import com.urltally.ws.resource.exception.InternalServerErrorRsException;
import com.urltally.ws.resource.exception.NotImplementedRsException;
import com.urltally.ws.resource.exception.RequestConflictRsException;
import com.urltally.ws.resource.exception.RequestForbiddenRsException;
import com.urltally.ws.resource.exception.ResourceGoneRsException;
import com.urltally.ws.resource.exception.ResourceNotFoundRsException;
import com.urltally.ws.resource.exception.ServiceUnavailableRsException;

import com.urltally.ws.ServiceInfo;
import com.urltally.ws.stub.ServiceInfoStub;
import com.urltally.ws.stub.ServiceInfoListStub;
import com.urltally.af.bean.ServiceInfoBean;
import com.urltally.af.resource.ServiceInfoResource;


// MockServiceInfoResource is a decorator.
// It can be used as a base class to mock ServiceInfoResource objects, e.g., for unit testing.
// This class is not intended to be run as a JAX-RS resource (e.g. within Jersey framework).
// @Path("/mock/r/serviceInfos/")
public abstract class MockServiceInfoResource implements ServiceInfoResource
{
    private static final Logger log = Logger.getLogger(MockServiceInfoResource.class.getName());

    // MockServiceInfoResource uses the decorator design pattern.
    private ServiceInfoResource decoratedResource;

    public MockServiceInfoResource(ServiceInfoResource decoratedResource)
    {
        this.decoratedResource = decoratedResource;
    }

    // To be used by subclasses
    protected ServiceInfoResource getdecoratedResource()
    {
        return this.decoratedResource;
    }
    // Ctor injector only. No setter injector.
    // public void setdecoratedResource(ServiceInfoResource decoratedResource)
    // {
    //     this.decoratedResource = decoratedResource;
    // }


    @Override
    public Response getAllServiceInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllServiceInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response getAllServiceInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        return decoratedResource.getAllServiceInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public Response findServiceInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findServiceInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findServiceInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
        return decoratedResource.findServiceInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Response findServiceInfosAsJsonp(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor, String callback)
    {
        return decoratedResource.findServiceInfosAsJsonp(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor, callback);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        return decoratedResource.getCount(filter, params, values, aggregate);
    }

//    @Override
//    public Response getServiceInfoAsHtml(String guid) throws BaseResourceException
//    {
//        return decoratedResource.getServiceInfoAsHtml(guid);
//    }

    @Override
    public Response getServiceInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.getServiceInfo(guid);
    }

    @Override
    public Response getServiceInfoAsJsonp(String guid, String callback) throws BaseResourceException
    {
        return decoratedResource.getServiceInfoAsJsonp(guid, callback);
    }

    @Override
    public Response getServiceInfo(String guid, String field) throws BaseResourceException
    {
        return decoratedResource.getServiceInfo(guid, field);
    }

    // TBD
    @Override
    public Response constructServiceInfo(ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        return decoratedResource.constructServiceInfo(serviceInfo);
    }

    @Override
    public Response createServiceInfo(ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        return decoratedResource.createServiceInfo(serviceInfo);
    }

//    @Override
//    public Response createServiceInfo(MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.createServiceInfo(formParams);
//    }

    // TBD
    @Override
    public Response refreshServiceInfo(String guid, ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        return decoratedResource.refreshServiceInfo(guid, serviceInfo);
    }

    @Override
    public Response updateServiceInfo(String guid, ServiceInfoStub serviceInfo) throws BaseResourceException
    {
        return decoratedResource.updateServiceInfo(guid, serviceInfo);
    }

    @Override
    public Response updateServiceInfo(String guid, String title, String content, String type, String status, Long scheduledTime)
    {
        return decoratedResource.updateServiceInfo(guid, title, content, type, status, scheduledTime);
    }

//    @Override
//    public Response updateServiceInfo(String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException
//    {
//        return decoratedResource.updateServiceInfo(guid, formParams);
//    }

    @Override
    public Response deleteServiceInfo(String guid) throws BaseResourceException
    {
        return decoratedResource.deleteServiceInfo(guid);
    }

    @Override
    public Response deleteServiceInfos(String filter, String params, List<String> values) throws BaseResourceException
    {
        return decoratedResource.deleteServiceInfos(filter, params, values);
    }


// TBD ....
    @Override
    public Response createServiceInfos(ServiceInfoListStub serviceInfos) throws BaseResourceException
    {
        return decoratedResource.createServiceInfos(serviceInfos);
    }


}
