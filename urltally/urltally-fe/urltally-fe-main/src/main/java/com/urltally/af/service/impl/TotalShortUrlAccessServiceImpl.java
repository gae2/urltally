package com.urltally.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.TotalShortUrlAccessService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TotalShortUrlAccessServiceImpl implements TotalShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "TotalShortUrlAccess-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("TotalShortUrlAccess:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public TotalShortUrlAccessServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // TotalShortUrlAccess related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TotalShortUrlAccess getTotalShortUrlAccess(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTotalShortUrlAccess(): guid = " + guid);

        TotalShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (TotalShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TotalShortUrlAccessBean) getProxyFactory().getTotalShortUrlAccessServiceProxy().getTotalShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TotalShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccessBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTotalShortUrlAccess(String guid, String field) throws BaseException
    {
        TotalShortUrlAccessBean bean = null;
        if(getCache() != null) {
            bean = (TotalShortUrlAccessBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TotalShortUrlAccessBean) getProxyFactory().getTotalShortUrlAccessServiceProxy().getTotalShortUrlAccess(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TotalShortUrlAccessBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccessBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("tallyType")) {
            return bean.getTallyType();
        } else if(field.equals("tallyTime")) {
            return bean.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return bean.getTallyEpoch();
        } else if(field.equals("count")) {
            return bean.getCount();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("shortUrlDomain")) {
            return bean.getShortUrlDomain();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TotalShortUrlAccess> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        log.fine("getTotalShortUrlAccesses()");

        // TBD: Is there a better way????
        List<TotalShortUrlAccess> totalShortUrlAccesses = getProxyFactory().getTotalShortUrlAccessServiceProxy().getTotalShortUrlAccesses(guids);
        if(totalShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccessBean list.");
        }

        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses() throws BaseException
    {
        return getAllTotalShortUrlAccesses(null, null, null);
    }


    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalShortUrlAccesses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TotalShortUrlAccess> totalShortUrlAccesses = getProxyFactory().getTotalShortUrlAccessServiceProxy().getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
        if(totalShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccessBean list.");
        }

        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTotalShortUrlAccessKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTotalShortUrlAccessServiceProxy().getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TotalShortUrlAccessBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TotalShortUrlAccessBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessServiceImpl.findTotalShortUrlAccesses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TotalShortUrlAccess> totalShortUrlAccesses = getProxyFactory().getTotalShortUrlAccessServiceProxy().findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(totalShortUrlAccesses == null) {
            log.log(Level.WARNING, "Failed to find totalShortUrlAccesses for the given criterion.");
        }

        log.finer("END");
        return totalShortUrlAccesses;
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessServiceImpl.findTotalShortUrlAccessKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTotalShortUrlAccessServiceProxy().findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TotalShortUrlAccess keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TotalShortUrlAccess key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TotalShortUrlAccessServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getTotalShortUrlAccessServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        TotalShortUrlAccessBean bean = new TotalShortUrlAccessBean(null, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        return createTotalShortUrlAccess(bean);
    }

    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TotalShortUrlAccess bean = constructTotalShortUrlAccess(totalShortUrlAccess);
        //return bean.getGuid();

        // Param totalShortUrlAccess cannot be null.....
        if(totalShortUrlAccess == null) {
            log.log(Level.INFO, "Param totalShortUrlAccess is null!");
            throw new BadRequestException("Param totalShortUrlAccess object is null!");
        }
        TotalShortUrlAccessBean bean = null;
        if(totalShortUrlAccess instanceof TotalShortUrlAccessBean) {
            bean = (TotalShortUrlAccessBean) totalShortUrlAccess;
        } else if(totalShortUrlAccess instanceof TotalShortUrlAccess) {
            // bean = new TotalShortUrlAccessBean(null, totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TotalShortUrlAccessBean(totalShortUrlAccess.getGuid(), totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
        } else {
            log.log(Level.WARNING, "createTotalShortUrlAccess(): Arg totalShortUrlAccess is of an unknown type.");
            //bean = new TotalShortUrlAccessBean();
            bean = new TotalShortUrlAccessBean(totalShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTotalShortUrlAccessServiceProxy().createTotalShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TotalShortUrlAccess constructTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalShortUrlAccess cannot be null.....
        if(totalShortUrlAccess == null) {
            log.log(Level.INFO, "Param totalShortUrlAccess is null!");
            throw new BadRequestException("Param totalShortUrlAccess object is null!");
        }
        TotalShortUrlAccessBean bean = null;
        if(totalShortUrlAccess instanceof TotalShortUrlAccessBean) {
            bean = (TotalShortUrlAccessBean) totalShortUrlAccess;
        } else if(totalShortUrlAccess instanceof TotalShortUrlAccess) {
            // bean = new TotalShortUrlAccessBean(null, totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TotalShortUrlAccessBean(totalShortUrlAccess.getGuid(), totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
        } else {
            log.log(Level.WARNING, "createTotalShortUrlAccess(): Arg totalShortUrlAccess is of an unknown type.");
            //bean = new TotalShortUrlAccessBean();
            bean = new TotalShortUrlAccessBean(totalShortUrlAccess.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTotalShortUrlAccessServiceProxy().createTotalShortUrlAccess(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TotalShortUrlAccessBean bean = new TotalShortUrlAccessBean(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        return updateTotalShortUrlAccess(bean);
    }
        
    @Override
    public Boolean updateTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TotalShortUrlAccess bean = refreshTotalShortUrlAccess(totalShortUrlAccess);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param totalShortUrlAccess cannot be null.....
        if(totalShortUrlAccess == null || totalShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalShortUrlAccess object or its guid is null!");
        }
        TotalShortUrlAccessBean bean = null;
        if(totalShortUrlAccess instanceof TotalShortUrlAccessBean) {
            bean = (TotalShortUrlAccessBean) totalShortUrlAccess;
        } else {  // if(totalShortUrlAccess instanceof TotalShortUrlAccess)
            bean = new TotalShortUrlAccessBean(totalShortUrlAccess.getGuid(), totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
        }
        Boolean suc = getProxyFactory().getTotalShortUrlAccessServiceProxy().updateTotalShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TotalShortUrlAccess refreshTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalShortUrlAccess cannot be null.....
        if(totalShortUrlAccess == null || totalShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalShortUrlAccess object or its guid is null!");
        }
        TotalShortUrlAccessBean bean = null;
        if(totalShortUrlAccess instanceof TotalShortUrlAccessBean) {
            bean = (TotalShortUrlAccessBean) totalShortUrlAccess;
        } else {  // if(totalShortUrlAccess instanceof TotalShortUrlAccess)
            bean = new TotalShortUrlAccessBean(totalShortUrlAccess.getGuid(), totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
        }
        Boolean suc = getProxyFactory().getTotalShortUrlAccessServiceProxy().updateTotalShortUrlAccess(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getTotalShortUrlAccessServiceProxy().deleteTotalShortUrlAccess(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            TotalShortUrlAccess totalShortUrlAccess = null;
            try {
                totalShortUrlAccess = getProxyFactory().getTotalShortUrlAccessServiceProxy().getTotalShortUrlAccess(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch totalShortUrlAccess with a key, " + guid);
                return false;
            }
            if(totalShortUrlAccess != null) {
                String beanGuid = totalShortUrlAccess.getGuid();
                Boolean suc1 = getProxyFactory().getTotalShortUrlAccessServiceProxy().deleteTotalShortUrlAccess(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("totalShortUrlAccess with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccess totalShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        // Param totalShortUrlAccess cannot be null.....
        if(totalShortUrlAccess == null || totalShortUrlAccess.getGuid() == null) {
            log.log(Level.INFO, "Param totalShortUrlAccess or its guid is null!");
            throw new BadRequestException("Param totalShortUrlAccess object or its guid is null!");
        }
        TotalShortUrlAccessBean bean = null;
        if(totalShortUrlAccess instanceof TotalShortUrlAccessBean) {
            bean = (TotalShortUrlAccessBean) totalShortUrlAccess;
        } else {  // if(totalShortUrlAccess instanceof TotalShortUrlAccess)
            // ????
            log.warning("totalShortUrlAccess is not an instance of TotalShortUrlAccessBean.");
            bean = new TotalShortUrlAccessBean(totalShortUrlAccess.getGuid(), totalShortUrlAccess.getTallyType(), totalShortUrlAccess.getTallyTime(), totalShortUrlAccess.getTallyEpoch(), totalShortUrlAccess.getCount(), totalShortUrlAccess.getShortUrl(), totalShortUrlAccess.getShortUrlDomain());
        }
        Boolean suc = getProxyFactory().getTotalShortUrlAccessServiceProxy().deleteTotalShortUrlAccess(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getTotalShortUrlAccessServiceProxy().deleteTotalShortUrlAccesses(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException
    {
        log.finer("BEGIN");

        if(totalShortUrlAccesses == null) {
            log.log(Level.WARNING, "createTotalShortUrlAccesses() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = totalShortUrlAccesses.size();
        if(size == 0) {
            log.log(Level.WARNING, "createTotalShortUrlAccesses() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(TotalShortUrlAccess totalShortUrlAccess : totalShortUrlAccesses) {
            String guid = createTotalShortUrlAccess(totalShortUrlAccess);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createTotalShortUrlAccesses() failed for at least one totalShortUrlAccess. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException
    //{
    //}

}
