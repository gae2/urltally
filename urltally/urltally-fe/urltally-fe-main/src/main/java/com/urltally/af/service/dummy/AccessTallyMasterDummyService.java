package com.urltally.af.service.dummy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.af.config.Config;

import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.AccessTallyMasterService;


// The primary purpose of AccessTallyMasterDummyService is to fake the service api, AccessTallyMasterService.
// It has no real implementation.
public class AccessTallyMasterDummyService implements AccessTallyMasterService
{
    private static final Logger log = Logger.getLogger(AccessTallyMasterDummyService.class.getName());

    public AccessTallyMasterDummyService()
    {
    }


    //////////////////////////////////////////////////////////////////////////
    // AccessTallyMaster related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAccessTallyMaster(): guid = " + guid);
        return null;
    }

    @Override
    public Object getAccessTallyMaster(String guid, String field) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAccessTallyMaster(): guid = " + guid + "; field = " + field);
        return null;
    }

    @Override
    public List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        log.fine("getAccessTallyMasters()");
        return null;
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException
    {
        return getAllAccessTallyMasters(null, null, null);
    }


    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyMasters(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyMasterKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyMasterDummyService.findAccessTallyMasters(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyMasterDummyService.findAccessTallyMasterKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);
        return null;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyMasterDummyService.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);
        return null;
    }

    @Override
    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        log.finer("createAccessTallyMaster()");
        return null;
    }

    @Override
    public String createAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("createAccessTallyMaster()");
        return null;
    }

    @Override
    public AccessTallyMaster constructAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("constructAccessTallyMaster()");
        return null;
    }

    @Override
    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        log.finer("updateAccessTallyMaster()");
        return null;
    }
        
    @Override
    public Boolean updateAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("updateAccessTallyMaster()");
        return null;
    }

    @Override
    public AccessTallyMaster refreshAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("refreshAccessTallyMaster()");
        return null;
    }

    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteAccessTallyMaster(): guid = " + guid);
        return null;
    }

    // ???
    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMaster accessTallyMaster) throws BaseException
    {
        log.finer("deleteAccessTallyMaster()");
        return null;
    }

    // TBD
    @Override
    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("deleteAccessTallyMaster(): filter = " + filter + "; params = " + params + "; values = " + values);
        return null;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException
    {
        log.finer("createAccessTallyMasters()");
        return null;
    }

    // TBD
    //@Override
    //public Boolean updateAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException
    //{
    //}

}
