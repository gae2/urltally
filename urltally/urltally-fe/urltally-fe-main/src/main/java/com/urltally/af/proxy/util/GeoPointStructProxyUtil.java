package com.urltally.af.proxy.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.urltally.ws.CommonConstants;
import com.urltally.ws.GeoPointStruct;
// import com.urltally.ws.bean.GeoPointStructBean;
import com.urltally.af.bean.GeoPointStructBean;


public class GeoPointStructProxyUtil
{
    private static final Logger log = Logger.getLogger(GeoPointStructProxyUtil.class.getName());

    // Static methods only.
    private GeoPointStructProxyUtil() {}

    public static GeoPointStructBean convertServerGeoPointStructBeanToAppBean(GeoPointStruct serverBean)
    {
        GeoPointStructBean bean = null;
        if(serverBean == null) {
            log.log(Level.INFO, "Server bean is null. Empty bean is returned.");
        } else {
            bean = new GeoPointStructBean();
            bean.setUuid(serverBean.getUuid());
            bean.setLatitude(serverBean.getLatitude());
            bean.setLongitude(serverBean.getLongitude());
            bean.setAltitude(serverBean.getAltitude());
            bean.setSensorUsed(serverBean.isSensorUsed());
        }
        return bean;
    }

    public static com.urltally.ws.bean.GeoPointStructBean convertAppGeoPointStructBeanToServerBean(GeoPointStruct appBean)
    {
        com.urltally.ws.bean.GeoPointStructBean bean = null;
        if(appBean == null) {
            log.log(Level.INFO, "App bean is null. Empty bean is returned.");
        } else {
            bean = new com.urltally.ws.bean.GeoPointStructBean();
            bean.setUuid(appBean.getUuid());
            bean.setLatitude(appBean.getLatitude());
            bean.setLongitude(appBean.getLongitude());
            bean.setAltitude(appBean.getAltitude());
            bean.setSensorUsed(appBean.isSensorUsed());
        }
        return bean;
    }

}
