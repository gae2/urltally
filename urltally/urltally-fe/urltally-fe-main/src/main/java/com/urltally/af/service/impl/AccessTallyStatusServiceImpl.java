package com.urltally.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.af.config.Config;

import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.af.bean.GeoPointStructBean;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.af.bean.GaeUserStructBean;

import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.af.proxy.AbstractProxyFactory;
import com.urltally.af.proxy.manager.ProxyFactoryManager;
import com.urltally.af.service.ServiceConstants;
import com.urltally.af.service.AccessTallyStatusService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AccessTallyStatusServiceImpl implements AccessTallyStatusService
{
    private static final Logger log = Logger.getLogger(AccessTallyStatusServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "AccessTallyStatus-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("AccessTallyStatus:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public AccessTallyStatusServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // AccessTallyStatus related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAccessTallyStatus(): guid = " + guid);

        AccessTallyStatusBean bean = null;
        if(getCache() != null) {
            bean = (AccessTallyStatusBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AccessTallyStatusBean) getProxyFactory().getAccessTallyStatusServiceProxy().getAccessTallyStatus(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AccessTallyStatusBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AccessTallyStatusBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getAccessTallyStatus(String guid, String field) throws BaseException
    {
        AccessTallyStatusBean bean = null;
        if(getCache() != null) {
            bean = (AccessTallyStatusBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (AccessTallyStatusBean) getProxyFactory().getAccessTallyStatusServiceProxy().getAccessTallyStatus(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "AccessTallyStatusBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve AccessTallyStatusBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("remoteRecordGuid")) {
            return bean.getRemoteRecordGuid();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("tallyType")) {
            return bean.getTallyType();
        } else if(field.equals("tallyTime")) {
            return bean.getTallyTime();
        } else if(field.equals("tallyEpoch")) {
            return bean.getTallyEpoch();
        } else if(field.equals("processed")) {
            return bean.isProcessed();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        log.fine("getAccessTallyStatuses()");

        // TBD: Is there a better way????
        List<AccessTallyStatus> accessTallyStatuses = getProxyFactory().getAccessTallyStatusServiceProxy().getAccessTallyStatuses(guids);
        if(accessTallyStatuses == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatusBean list.");
        }

        log.finer("END");
        return accessTallyStatuses;
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException
    {
        return getAllAccessTallyStatuses(null, null, null);
    }


    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyStatuses(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<AccessTallyStatus> accessTallyStatuses = getProxyFactory().getAccessTallyStatusServiceProxy().getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
        if(accessTallyStatuses == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatusBean list.");
        }

        log.finer("END");
        return accessTallyStatuses;
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllAccessTallyStatusKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAccessTallyStatusServiceProxy().getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve AccessTallyStatusBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AccessTallyStatusBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusServiceImpl.findAccessTallyStatuses(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<AccessTallyStatus> accessTallyStatuses = getProxyFactory().getAccessTallyStatusServiceProxy().findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(accessTallyStatuses == null) {
            log.log(Level.WARNING, "Failed to find accessTallyStatuses for the given criterion.");
        }

        log.finer("END");
        return accessTallyStatuses;
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusServiceImpl.findAccessTallyStatusKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getAccessTallyStatusServiceProxy().findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find AccessTallyStatus keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty AccessTallyStatus key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("AccessTallyStatusServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getAccessTallyStatusServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        AccessTallyStatusBean bean = new AccessTallyStatusBean(null, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
        return createAccessTallyStatus(bean);
    }

    @Override
    public String createAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AccessTallyStatus bean = constructAccessTallyStatus(accessTallyStatus);
        //return bean.getGuid();

        // Param accessTallyStatus cannot be null.....
        if(accessTallyStatus == null) {
            log.log(Level.INFO, "Param accessTallyStatus is null!");
            throw new BadRequestException("Param accessTallyStatus object is null!");
        }
        AccessTallyStatusBean bean = null;
        if(accessTallyStatus instanceof AccessTallyStatusBean) {
            bean = (AccessTallyStatusBean) accessTallyStatus;
        } else if(accessTallyStatus instanceof AccessTallyStatus) {
            // bean = new AccessTallyStatusBean(null, accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AccessTallyStatusBean(accessTallyStatus.getGuid(), accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
        } else {
            log.log(Level.WARNING, "createAccessTallyStatus(): Arg accessTallyStatus is of an unknown type.");
            //bean = new AccessTallyStatusBean();
            bean = new AccessTallyStatusBean(accessTallyStatus.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAccessTallyStatusServiceProxy().createAccessTallyStatus(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public AccessTallyStatus constructAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyStatus cannot be null.....
        if(accessTallyStatus == null) {
            log.log(Level.INFO, "Param accessTallyStatus is null!");
            throw new BadRequestException("Param accessTallyStatus object is null!");
        }
        AccessTallyStatusBean bean = null;
        if(accessTallyStatus instanceof AccessTallyStatusBean) {
            bean = (AccessTallyStatusBean) accessTallyStatus;
        } else if(accessTallyStatus instanceof AccessTallyStatus) {
            // bean = new AccessTallyStatusBean(null, accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new AccessTallyStatusBean(accessTallyStatus.getGuid(), accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
        } else {
            log.log(Level.WARNING, "createAccessTallyStatus(): Arg accessTallyStatus is of an unknown type.");
            //bean = new AccessTallyStatusBean();
            bean = new AccessTallyStatusBean(accessTallyStatus.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getAccessTallyStatusServiceProxy().createAccessTallyStatus(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        AccessTallyStatusBean bean = new AccessTallyStatusBean(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
        return updateAccessTallyStatus(bean);
    }
        
    @Override
    public Boolean updateAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //AccessTallyStatus bean = refreshAccessTallyStatus(accessTallyStatus);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param accessTallyStatus cannot be null.....
        if(accessTallyStatus == null || accessTallyStatus.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyStatus or its guid is null!");
            throw new BadRequestException("Param accessTallyStatus object or its guid is null!");
        }
        AccessTallyStatusBean bean = null;
        if(accessTallyStatus instanceof AccessTallyStatusBean) {
            bean = (AccessTallyStatusBean) accessTallyStatus;
        } else {  // if(accessTallyStatus instanceof AccessTallyStatus)
            bean = new AccessTallyStatusBean(accessTallyStatus.getGuid(), accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
        }
        Boolean suc = getProxyFactory().getAccessTallyStatusServiceProxy().updateAccessTallyStatus(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public AccessTallyStatus refreshAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyStatus cannot be null.....
        if(accessTallyStatus == null || accessTallyStatus.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyStatus or its guid is null!");
            throw new BadRequestException("Param accessTallyStatus object or its guid is null!");
        }
        AccessTallyStatusBean bean = null;
        if(accessTallyStatus instanceof AccessTallyStatusBean) {
            bean = (AccessTallyStatusBean) accessTallyStatus;
        } else {  // if(accessTallyStatus instanceof AccessTallyStatus)
            bean = new AccessTallyStatusBean(accessTallyStatus.getGuid(), accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
        }
        Boolean suc = getProxyFactory().getAccessTallyStatusServiceProxy().updateAccessTallyStatus(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getAccessTallyStatusServiceProxy().deleteAccessTallyStatus(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            AccessTallyStatus accessTallyStatus = null;
            try {
                accessTallyStatus = getProxyFactory().getAccessTallyStatusServiceProxy().getAccessTallyStatus(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch accessTallyStatus with a key, " + guid);
                return false;
            }
            if(accessTallyStatus != null) {
                String beanGuid = accessTallyStatus.getGuid();
                Boolean suc1 = getProxyFactory().getAccessTallyStatusServiceProxy().deleteAccessTallyStatus(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("accessTallyStatus with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatus accessTallyStatus) throws BaseException
    {
        log.finer("BEGIN");

        // Param accessTallyStatus cannot be null.....
        if(accessTallyStatus == null || accessTallyStatus.getGuid() == null) {
            log.log(Level.INFO, "Param accessTallyStatus or its guid is null!");
            throw new BadRequestException("Param accessTallyStatus object or its guid is null!");
        }
        AccessTallyStatusBean bean = null;
        if(accessTallyStatus instanceof AccessTallyStatusBean) {
            bean = (AccessTallyStatusBean) accessTallyStatus;
        } else {  // if(accessTallyStatus instanceof AccessTallyStatus)
            // ????
            log.warning("accessTallyStatus is not an instance of AccessTallyStatusBean.");
            bean = new AccessTallyStatusBean(accessTallyStatus.getGuid(), accessTallyStatus.getRemoteRecordGuid(), accessTallyStatus.getShortUrl(), accessTallyStatus.getTallyType(), accessTallyStatus.getTallyTime(), accessTallyStatus.getTallyEpoch(), accessTallyStatus.isProcessed());
        }
        Boolean suc = getProxyFactory().getAccessTallyStatusServiceProxy().deleteAccessTallyStatus(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getAccessTallyStatusServiceProxy().deleteAccessTallyStatuses(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    {
        log.finer("BEGIN");

        if(accessTallyStatuses == null) {
            log.log(Level.WARNING, "createAccessTallyStatuses() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = accessTallyStatuses.size();
        if(size == 0) {
            log.log(Level.WARNING, "createAccessTallyStatuses() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(AccessTallyStatus accessTallyStatus : accessTallyStatuses) {
            String guid = createAccessTallyStatus(accessTallyStatus);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createAccessTallyStatuses() failed for at least one accessTallyStatus. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    //{
    //}

}
