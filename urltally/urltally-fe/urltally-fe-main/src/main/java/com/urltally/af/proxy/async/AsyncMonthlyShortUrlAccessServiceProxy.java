package com.urltally.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.urltally.ws.BaseException;
import com.urltally.ws.exception.BadRequestException;
import com.urltally.ws.core.GUID;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessListStub;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.ws.service.MonthlyShortUrlAccessService;
import com.urltally.af.proxy.MonthlyShortUrlAccessServiceProxy;
import com.urltally.af.proxy.remote.RemoteMonthlyShortUrlAccessServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncMonthlyShortUrlAccessServiceProxy extends BaseAsyncServiceProxy implements MonthlyShortUrlAccessServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncMonthlyShortUrlAccessServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteMonthlyShortUrlAccessServiceProxy remoteProxy;

    public AsyncMonthlyShortUrlAccessServiceProxy()
    {
        remoteProxy = new RemoteMonthlyShortUrlAccessServiceProxy();
    }

    @Override
    public MonthlyShortUrlAccess getMonthlyShortUrlAccess(String guid) throws BaseException
    {
        return remoteProxy.getMonthlyShortUrlAccess(guid);
    }

    @Override
    public Object getMonthlyShortUrlAccess(String guid, String field) throws BaseException
    {
        return remoteProxy.getMonthlyShortUrlAccess(guid, field);       
    }

    @Override
    public List<MonthlyShortUrlAccess> getMonthlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return remoteProxy.getMonthlyShortUrlAccesses(guids);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses() throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllMonthlyShortUrlAccesses(ordering, offset, count);
        return getAllMonthlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllMonthlyShortUrlAccessKeys(ordering, offset, count);
        return getAllMonthlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count);
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createMonthlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        MonthlyShortUrlAccessBean bean = new MonthlyShortUrlAccessBean(null, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        return createMonthlyShortUrlAccess(bean);        
    }

    @Override
    public String createMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        String guid = monthlyShortUrlAccess.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((MonthlyShortUrlAccessBean) monthlyShortUrlAccess).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateMonthlyShortUrlAccess-" + guid;
        String taskName = "RsCreateMonthlyShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        MonthlyShortUrlAccessStub stub = MarshalHelper.convertMonthlyShortUrlAccessToStub(monthlyShortUrlAccess);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(MonthlyShortUrlAccessStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = monthlyShortUrlAccess.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    MonthlyShortUrlAccessStub dummyStub = new MonthlyShortUrlAccessStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createMonthlyShortUrlAccess(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "monthlyShortUrlAccesses/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createMonthlyShortUrlAccess(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "monthlyShortUrlAccesses/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateMonthlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "MonthlyShortUrlAccess guid is invalid.");
        	throw new BaseException("MonthlyShortUrlAccess guid is invalid.");
        }
        MonthlyShortUrlAccessBean bean = new MonthlyShortUrlAccessBean(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        return updateMonthlyShortUrlAccess(bean);        
    }

    @Override
    public Boolean updateMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        log.finer("BEGIN");

        String guid = monthlyShortUrlAccess.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "MonthlyShortUrlAccess object is invalid.");
        	throw new BaseException("MonthlyShortUrlAccess object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateMonthlyShortUrlAccess-" + guid;
        String taskName = "RsUpdateMonthlyShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        MonthlyShortUrlAccessStub stub = MarshalHelper.convertMonthlyShortUrlAccessToStub(monthlyShortUrlAccess);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(MonthlyShortUrlAccessStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = monthlyShortUrlAccess.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    MonthlyShortUrlAccessStub dummyStub = new MonthlyShortUrlAccessStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateMonthlyShortUrlAccess(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "monthlyShortUrlAccesses/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateMonthlyShortUrlAccess(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "monthlyShortUrlAccesses/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteMonthlyShortUrlAccess(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteMonthlyShortUrlAccess-" + guid;
        String taskName = "RsDeleteMonthlyShortUrlAccess-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "monthlyShortUrlAccesses/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteMonthlyShortUrlAccess(MonthlyShortUrlAccess monthlyShortUrlAccess) throws BaseException
    {
        String guid = monthlyShortUrlAccess.getGuid();
        return deleteMonthlyShortUrlAccess(guid);
    }

    @Override
    public Long deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteMonthlyShortUrlAccesses(filter, params, values);
    }

}
