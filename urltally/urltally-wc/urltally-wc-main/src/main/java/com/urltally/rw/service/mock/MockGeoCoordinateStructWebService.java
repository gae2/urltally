package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.GeoCoordinateStruct;
import com.urltally.af.bean.GeoCoordinateStructBean;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.GeoCoordinateStructJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.GeoCoordinateStructWebService;


// MockGeoCoordinateStructWebService is a mock object.
// It can be used as a base class to mock GeoCoordinateStructWebService objects.
public abstract class MockGeoCoordinateStructWebService extends GeoCoordinateStructWebService  // implements GeoCoordinateStructService
{
    private static final Logger log = Logger.getLogger(MockGeoCoordinateStructWebService.class.getName());
     

}
