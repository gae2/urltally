package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.DailyShortUrlAccessJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.DailyShortUrlAccessWebService;


// MockDailyShortUrlAccessWebService is a mock object.
// It can be used as a base class to mock DailyShortUrlAccessWebService objects.
public abstract class MockDailyShortUrlAccessWebService extends DailyShortUrlAccessWebService  // implements DailyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockDailyShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private DailyShortUrlAccessWebService mService = null;

    public MockDailyShortUrlAccessWebService()
    {
        this(MockServiceProxyFactory.getInstance().getDailyShortUrlAccessServiceProxy());
    }
    public MockDailyShortUrlAccessWebService(DailyShortUrlAccessService service)
    {
        super(service);
    }


}
