package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.GeoPointStruct;
import com.urltally.ws.StreetAddressStruct;
import com.urltally.ws.GaeAppStruct;
import com.urltally.ws.FullNameStruct;
import com.urltally.ws.GaeUserStruct;
import com.urltally.ws.User;
import com.urltally.af.bean.UserBean;
import com.urltally.af.service.UserService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.GeoPointStructJsBean;
import com.urltally.fe.bean.StreetAddressStructJsBean;
import com.urltally.fe.bean.GaeAppStructJsBean;
import com.urltally.fe.bean.FullNameStructJsBean;
import com.urltally.fe.bean.GaeUserStructJsBean;
import com.urltally.fe.bean.UserJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.UserWebService;


// MockUserWebService is a mock object.
// It can be used as a base class to mock UserWebService objects.
public abstract class MockUserWebService extends UserWebService  // implements UserService
{
    private static final Logger log = Logger.getLogger(MockUserWebService.class.getName());
     
    // Af service interface.
    private UserWebService mService = null;

    public MockUserWebService()
    {
        this(MockServiceProxyFactory.getInstance().getUserServiceProxy());
    }
    public MockUserWebService(UserService service)
    {
        super(service);
    }


}
