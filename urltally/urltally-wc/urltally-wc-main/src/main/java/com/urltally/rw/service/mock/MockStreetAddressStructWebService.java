package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.StreetAddressStruct;
import com.urltally.af.bean.StreetAddressStructBean;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.StreetAddressStructJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.StreetAddressStructWebService;


// MockStreetAddressStructWebService is a mock object.
// It can be used as a base class to mock StreetAddressStructWebService objects.
public abstract class MockStreetAddressStructWebService extends StreetAddressStructWebService  // implements StreetAddressStructService
{
    private static final Logger log = Logger.getLogger(MockStreetAddressStructWebService.class.getName());
     

}
