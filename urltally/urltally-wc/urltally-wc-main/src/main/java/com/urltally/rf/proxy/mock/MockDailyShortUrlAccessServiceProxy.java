package com.urltally.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.core.StatusCode;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.DailyShortUrlAccess;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.KeyListStub;
import com.urltally.ws.stub.DailyShortUrlAccessStub;
import com.urltally.ws.stub.DailyShortUrlAccessListStub;
import com.urltally.af.bean.DailyShortUrlAccessBean;
import com.urltally.af.service.DailyShortUrlAccessService;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.util.StringUtil;
import com.urltally.rf.auth.TwoLeggedOAuthClientUtil;
import com.urltally.rf.config.Config;
import com.urltally.rf.proxy.DailyShortUrlAccessServiceProxy;


// MockDailyShortUrlAccessServiceProxy is a decorator.
// It can be used as a base class to mock DailyShortUrlAccessService objects.
public abstract class MockDailyShortUrlAccessServiceProxy extends DailyShortUrlAccessServiceProxy implements DailyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockDailyShortUrlAccessServiceProxy.class.getName());

    // MockDailyShortUrlAccessServiceProxy uses the decorator design pattern.
    private DailyShortUrlAccessService decoratedProxy;

    public MockDailyShortUrlAccessServiceProxy(DailyShortUrlAccessService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected DailyShortUrlAccessService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(DailyShortUrlAccessService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public DailyShortUrlAccess getDailyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.getDailyShortUrlAccess(guid);
    }

    @Override
    public Object getDailyShortUrlAccess(String guid, String field) throws BaseException
    {
        return decoratedProxy.getDailyShortUrlAccess(guid, field);
    }

    @Override
    public List<DailyShortUrlAccess> getDailyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getDailyShortUrlAccesses(guids);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses() throws BaseException
    {
        return getAllDailyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDailyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> getAllDailyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDailyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDailyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDailyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllDailyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDailyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DailyShortUrlAccess> findDailyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDailyShortUrlAccesses(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDailyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findDailyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDailyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        return decoratedProxy.createDailyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
    }

    @Override
    public String createDailyShortUrlAccess(DailyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.createDailyShortUrlAccess(bean);
    }

    @Override
    public DailyShortUrlAccess constructDailyShortUrlAccess(DailyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.constructDailyShortUrlAccess(bean);
    }

    @Override
    public Boolean updateDailyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day) throws BaseException
    {
        return decoratedProxy.updateDailyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day);
    }

    @Override
    public Boolean updateDailyShortUrlAccess(DailyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.updateDailyShortUrlAccess(bean);
    }

    @Override
    public DailyShortUrlAccess refreshDailyShortUrlAccess(DailyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.refreshDailyShortUrlAccess(bean);
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.deleteDailyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteDailyShortUrlAccess(DailyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.deleteDailyShortUrlAccess(bean);
    }

    @Override
    public Long deleteDailyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteDailyShortUrlAccesses(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDailyShortUrlAccesses(List<DailyShortUrlAccess> dailyShortUrlAccesses) throws BaseException
    {
        return decoratedProxy.createDailyShortUrlAccesses(dailyShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateDailyShortUrlAccesses(List<DailyShortUrlAccess> dailyShortUrlAccesses) throws BaseException
    //{
    //}

}
