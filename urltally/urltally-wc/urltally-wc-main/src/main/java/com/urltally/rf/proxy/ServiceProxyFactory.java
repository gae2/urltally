package com.urltally.rf.proxy;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ServiceProxyFactory
{
    private static final Logger log = Logger.getLogger(ServiceProxyFactory.class.getName());

    private UserServiceProxy userService = null;
    private AccessTallyMasterServiceProxy accessTallyMasterService = null;
    private AccessTallyStatusServiceProxy accessTallyStatusService = null;
    private MonthlyShortUrlAccessServiceProxy monthlyShortUrlAccessService = null;
    private WeeklyShortUrlAccessServiceProxy weeklyShortUrlAccessService = null;
    private DailyShortUrlAccessServiceProxy dailyShortUrlAccessService = null;
    private HourlyShortUrlAccessServiceProxy hourlyShortUrlAccessService = null;
    private CumulativeShortUrlAccessServiceProxy cumulativeShortUrlAccessService = null;
    private CurrentShortUrlAccessServiceProxy currentShortUrlAccessService = null;
    private TotalShortUrlAccessServiceProxy totalShortUrlAccessService = null;
    private TotalLongUrlAccessServiceProxy totalLongUrlAccessService = null;
    private ServiceInfoServiceProxy serviceInfoService = null;
    private FiveTenServiceProxy fiveTenService = null;

    protected ServiceProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ServiceProxyFactoryHolder
    {
        private static final ServiceProxyFactory INSTANCE = new ServiceProxyFactory();
    }

    // Singleton method
    public static ServiceProxyFactory getInstance()
    {
        return ServiceProxyFactoryHolder.INSTANCE;
    }

    public UserServiceProxy getUserServiceProxy()
    {
        if(userService == null) {
            userService = new UserServiceProxy();
        }
        return userService;
    }

    public AccessTallyMasterServiceProxy getAccessTallyMasterServiceProxy()
    {
        if(accessTallyMasterService == null) {
            accessTallyMasterService = new AccessTallyMasterServiceProxy();
        }
        return accessTallyMasterService;
    }

    public AccessTallyStatusServiceProxy getAccessTallyStatusServiceProxy()
    {
        if(accessTallyStatusService == null) {
            accessTallyStatusService = new AccessTallyStatusServiceProxy();
        }
        return accessTallyStatusService;
    }

    public MonthlyShortUrlAccessServiceProxy getMonthlyShortUrlAccessServiceProxy()
    {
        if(monthlyShortUrlAccessService == null) {
            monthlyShortUrlAccessService = new MonthlyShortUrlAccessServiceProxy();
        }
        return monthlyShortUrlAccessService;
    }

    public WeeklyShortUrlAccessServiceProxy getWeeklyShortUrlAccessServiceProxy()
    {
        if(weeklyShortUrlAccessService == null) {
            weeklyShortUrlAccessService = new WeeklyShortUrlAccessServiceProxy();
        }
        return weeklyShortUrlAccessService;
    }

    public DailyShortUrlAccessServiceProxy getDailyShortUrlAccessServiceProxy()
    {
        if(dailyShortUrlAccessService == null) {
            dailyShortUrlAccessService = new DailyShortUrlAccessServiceProxy();
        }
        return dailyShortUrlAccessService;
    }

    public HourlyShortUrlAccessServiceProxy getHourlyShortUrlAccessServiceProxy()
    {
        if(hourlyShortUrlAccessService == null) {
            hourlyShortUrlAccessService = new HourlyShortUrlAccessServiceProxy();
        }
        return hourlyShortUrlAccessService;
    }

    public CumulativeShortUrlAccessServiceProxy getCumulativeShortUrlAccessServiceProxy()
    {
        if(cumulativeShortUrlAccessService == null) {
            cumulativeShortUrlAccessService = new CumulativeShortUrlAccessServiceProxy();
        }
        return cumulativeShortUrlAccessService;
    }

    public CurrentShortUrlAccessServiceProxy getCurrentShortUrlAccessServiceProxy()
    {
        if(currentShortUrlAccessService == null) {
            currentShortUrlAccessService = new CurrentShortUrlAccessServiceProxy();
        }
        return currentShortUrlAccessService;
    }

    public TotalShortUrlAccessServiceProxy getTotalShortUrlAccessServiceProxy()
    {
        if(totalShortUrlAccessService == null) {
            totalShortUrlAccessService = new TotalShortUrlAccessServiceProxy();
        }
        return totalShortUrlAccessService;
    }

    public TotalLongUrlAccessServiceProxy getTotalLongUrlAccessServiceProxy()
    {
        if(totalLongUrlAccessService == null) {
            totalLongUrlAccessService = new TotalLongUrlAccessServiceProxy();
        }
        return totalLongUrlAccessService;
    }

    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new ServiceInfoServiceProxy();
        }
        return serviceInfoService;
    }

    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenService == null) {
            fiveTenService = new FiveTenServiceProxy();
        }
        return fiveTenService;
    }

}
