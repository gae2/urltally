package com.urltally.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.HourlyShortUrlAccessJsBean;
import com.urltally.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class HourlyShortUrlAccessWebService // implements HourlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(HourlyShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private HourlyShortUrlAccessService mService = null;

    public HourlyShortUrlAccessWebService()
    {
        this(ServiceProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy());
    }
    public HourlyShortUrlAccessWebService(HourlyShortUrlAccessService service)
    {
        mService = service;
    }
    
    protected HourlyShortUrlAccessService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(HourlyShortUrlAccessService service)
    {
        mService = service;
    }
    
    
    public HourlyShortUrlAccessJsBean getHourlyShortUrlAccess(String guid) throws WebException
    {
        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = getServiceProxy().getHourlyShortUrlAccess(guid);
            HourlyShortUrlAccessJsBean bean = convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getHourlyShortUrlAccess(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getHourlyShortUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<HourlyShortUrlAccessJsBean> getHourlyShortUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<HourlyShortUrlAccessJsBean> jsBeans = new ArrayList<HourlyShortUrlAccessJsBean>();
            List<HourlyShortUrlAccess> hourlyShortUrlAccesses = getServiceProxy().getHourlyShortUrlAccesses(guids);
            if(hourlyShortUrlAccesses != null) {
                for(HourlyShortUrlAccess hourlyShortUrlAccess : hourlyShortUrlAccesses) {
                    jsBeans.add(convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<HourlyShortUrlAccessJsBean> getAllHourlyShortUrlAccesses() throws WebException
    {
        return getAllHourlyShortUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<HourlyShortUrlAccessJsBean> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllHourlyShortUrlAccesses(ordering, offset, count, null);
    }

    public List<HourlyShortUrlAccessJsBean> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<HourlyShortUrlAccessJsBean> jsBeans = new ArrayList<HourlyShortUrlAccessJsBean>();
            List<HourlyShortUrlAccess> hourlyShortUrlAccesses = getServiceProxy().getAllHourlyShortUrlAccesses(ordering, offset, count, forwardCursor);
            if(hourlyShortUrlAccesses != null) {
                for(HourlyShortUrlAccess hourlyShortUrlAccess : hourlyShortUrlAccesses) {
                    jsBeans.add(convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllHourlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllHourlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<HourlyShortUrlAccessJsBean> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<HourlyShortUrlAccessJsBean> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<HourlyShortUrlAccessJsBean> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<HourlyShortUrlAccessJsBean> jsBeans = new ArrayList<HourlyShortUrlAccessJsBean>();
            List<HourlyShortUrlAccess> hourlyShortUrlAccesses = getServiceProxy().findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(hourlyShortUrlAccesses != null) {
                for(HourlyShortUrlAccess hourlyShortUrlAccess : hourlyShortUrlAccesses) {
                    jsBeans.add(convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createHourlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws WebException
    {
        try {
            return getServiceProxy().createHourlyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().createHourlyShortUrlAccess(hourlyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public HourlyShortUrlAccessJsBean constructHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            hourlyShortUrlAccess = getServiceProxy().constructHourlyShortUrlAccess(hourlyShortUrlAccess);
            jsBean = convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws WebException
    {
        try {
            return getServiceProxy().updateHourlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().updateHourlyShortUrlAccess(hourlyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public HourlyShortUrlAccessJsBean refreshHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            hourlyShortUrlAccess = getServiceProxy().refreshHourlyShortUrlAccess(hourlyShortUrlAccess);
            jsBean = convertHourlyShortUrlAccessToJsBean(hourlyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteHourlyShortUrlAccess(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteHourlyShortUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            HourlyShortUrlAccess hourlyShortUrlAccess = convertHourlyShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().deleteHourlyShortUrlAccess(hourlyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteHourlyShortUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static HourlyShortUrlAccessJsBean convertHourlyShortUrlAccessToJsBean(HourlyShortUrlAccess hourlyShortUrlAccess)
    {
        HourlyShortUrlAccessJsBean jsBean = null;
        if(hourlyShortUrlAccess != null) {
            jsBean = new HourlyShortUrlAccessJsBean();
            jsBean.setGuid(hourlyShortUrlAccess.getGuid());
            jsBean.setTallyTime(hourlyShortUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(hourlyShortUrlAccess.getTallyEpoch());
            jsBean.setCount(hourlyShortUrlAccess.getCount());
            jsBean.setShortUrl(hourlyShortUrlAccess.getShortUrl());
            jsBean.setShortUrlDomain(hourlyShortUrlAccess.getShortUrlDomain());
            jsBean.setLongUrl(hourlyShortUrlAccess.getLongUrl());
            jsBean.setLongUrlDomain(hourlyShortUrlAccess.getLongUrlDomain());
            jsBean.setRedirectType(hourlyShortUrlAccess.getRedirectType());
            jsBean.setRefererDomain(hourlyShortUrlAccess.getRefererDomain());
            jsBean.setUserAgent(hourlyShortUrlAccess.getUserAgent());
            jsBean.setLanguage(hourlyShortUrlAccess.getLanguage());
            jsBean.setCountry(hourlyShortUrlAccess.getCountry());
            jsBean.setTalliedTime(hourlyShortUrlAccess.getTalliedTime());
            jsBean.setYear(hourlyShortUrlAccess.getYear());
            jsBean.setDay(hourlyShortUrlAccess.getDay());
            jsBean.setHour(hourlyShortUrlAccess.getHour());
            jsBean.setCreatedTime(hourlyShortUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(hourlyShortUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static HourlyShortUrlAccess convertHourlyShortUrlAccessJsBeanToBean(HourlyShortUrlAccessJsBean jsBean)
    {
        HourlyShortUrlAccessBean hourlyShortUrlAccess = null;
        if(jsBean != null) {
            hourlyShortUrlAccess = new HourlyShortUrlAccessBean();
            hourlyShortUrlAccess.setGuid(jsBean.getGuid());
            hourlyShortUrlAccess.setTallyTime(jsBean.getTallyTime());
            hourlyShortUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            hourlyShortUrlAccess.setCount(jsBean.getCount());
            hourlyShortUrlAccess.setShortUrl(jsBean.getShortUrl());
            hourlyShortUrlAccess.setShortUrlDomain(jsBean.getShortUrlDomain());
            hourlyShortUrlAccess.setLongUrl(jsBean.getLongUrl());
            hourlyShortUrlAccess.setLongUrlDomain(jsBean.getLongUrlDomain());
            hourlyShortUrlAccess.setRedirectType(jsBean.getRedirectType());
            hourlyShortUrlAccess.setRefererDomain(jsBean.getRefererDomain());
            hourlyShortUrlAccess.setUserAgent(jsBean.getUserAgent());
            hourlyShortUrlAccess.setLanguage(jsBean.getLanguage());
            hourlyShortUrlAccess.setCountry(jsBean.getCountry());
            hourlyShortUrlAccess.setTalliedTime(jsBean.getTalliedTime());
            hourlyShortUrlAccess.setYear(jsBean.getYear());
            hourlyShortUrlAccess.setDay(jsBean.getDay());
            hourlyShortUrlAccess.setHour(jsBean.getHour());
            hourlyShortUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            hourlyShortUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return hourlyShortUrlAccess;
    }

}
