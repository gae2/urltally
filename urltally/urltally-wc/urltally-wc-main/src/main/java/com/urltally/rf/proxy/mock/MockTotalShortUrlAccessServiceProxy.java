package com.urltally.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.core.StatusCode;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.KeyListStub;
import com.urltally.ws.stub.TotalShortUrlAccessStub;
import com.urltally.ws.stub.TotalShortUrlAccessListStub;
import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.util.StringUtil;
import com.urltally.rf.auth.TwoLeggedOAuthClientUtil;
import com.urltally.rf.config.Config;
import com.urltally.rf.proxy.TotalShortUrlAccessServiceProxy;


// MockTotalShortUrlAccessServiceProxy is a decorator.
// It can be used as a base class to mock TotalShortUrlAccessService objects.
public abstract class MockTotalShortUrlAccessServiceProxy extends TotalShortUrlAccessServiceProxy implements TotalShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockTotalShortUrlAccessServiceProxy.class.getName());

    // MockTotalShortUrlAccessServiceProxy uses the decorator design pattern.
    private TotalShortUrlAccessService decoratedProxy;

    public MockTotalShortUrlAccessServiceProxy(TotalShortUrlAccessService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected TotalShortUrlAccessService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(TotalShortUrlAccessService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public TotalShortUrlAccess getTotalShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.getTotalShortUrlAccess(guid);
    }

    @Override
    public Object getTotalShortUrlAccess(String guid, String field) throws BaseException
    {
        return decoratedProxy.getTotalShortUrlAccess(guid, field);
    }

    @Override
    public List<TotalShortUrlAccess> getTotalShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getTotalShortUrlAccesses(guids);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses() throws BaseException
    {
        return getAllTotalShortUrlAccesses(null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TotalShortUrlAccess> findTotalShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTotalShortUrlAccesses(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        return decoratedProxy.createTotalShortUrlAccess(tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
    }

    @Override
    public String createTotalShortUrlAccess(TotalShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.createTotalShortUrlAccess(bean);
    }

    @Override
    public TotalShortUrlAccess constructTotalShortUrlAccess(TotalShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.constructTotalShortUrlAccess(bean);
    }

    @Override
    public Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws BaseException
    {
        return decoratedProxy.updateTotalShortUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
    }

    @Override
    public Boolean updateTotalShortUrlAccess(TotalShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.updateTotalShortUrlAccess(bean);
    }

    @Override
    public TotalShortUrlAccess refreshTotalShortUrlAccess(TotalShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.refreshTotalShortUrlAccess(bean);
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.deleteTotalShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.deleteTotalShortUrlAccess(bean);
    }

    @Override
    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteTotalShortUrlAccesses(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException
    {
        return decoratedProxy.createTotalShortUrlAccesses(totalShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateTotalShortUrlAccesses(List<TotalShortUrlAccess> totalShortUrlAccesses) throws BaseException
    //{
    //}

}
