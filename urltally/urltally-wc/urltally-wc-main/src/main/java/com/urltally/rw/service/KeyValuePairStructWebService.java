package com.urltally.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.KeyValuePairStruct;
import com.urltally.af.bean.KeyValuePairStructBean;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.KeyValuePairStructJsBean;
import com.urltally.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeyValuePairStructWebService // implements KeyValuePairStructService
{
    private static final Logger log = Logger.getLogger(KeyValuePairStructWebService.class.getName());
     
    public static KeyValuePairStructJsBean convertKeyValuePairStructToJsBean(KeyValuePairStruct keyValuePairStruct)
    {
        KeyValuePairStructJsBean jsBean = null;
        if(keyValuePairStruct != null) {
            jsBean = new KeyValuePairStructJsBean();
            jsBean.setUuid(keyValuePairStruct.getUuid());
            jsBean.setKey(keyValuePairStruct.getKey());
            jsBean.setValue(keyValuePairStruct.getValue());
            jsBean.setNote(keyValuePairStruct.getNote());
        }
        return jsBean;
    }

    public static KeyValuePairStruct convertKeyValuePairStructJsBeanToBean(KeyValuePairStructJsBean jsBean)
    {
        KeyValuePairStructBean keyValuePairStruct = null;
        if(jsBean != null) {
            keyValuePairStruct = new KeyValuePairStructBean();
            keyValuePairStruct.setUuid(jsBean.getUuid());
            keyValuePairStruct.setKey(jsBean.getKey());
            keyValuePairStruct.setValue(jsBean.getValue());
            keyValuePairStruct.setNote(jsBean.getNote());
        }
        return keyValuePairStruct;
    }

}
