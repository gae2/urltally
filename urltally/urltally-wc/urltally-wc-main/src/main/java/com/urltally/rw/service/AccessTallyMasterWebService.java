package com.urltally.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.AccessTallyMasterJsBean;
import com.urltally.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AccessTallyMasterWebService // implements AccessTallyMasterService
{
    private static final Logger log = Logger.getLogger(AccessTallyMasterWebService.class.getName());
     
    // Af service interface.
    private AccessTallyMasterService mService = null;

    public AccessTallyMasterWebService()
    {
        this(ServiceProxyFactory.getInstance().getAccessTallyMasterServiceProxy());
    }
    public AccessTallyMasterWebService(AccessTallyMasterService service)
    {
        mService = service;
    }
    
    protected AccessTallyMasterService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getAccessTallyMasterServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(AccessTallyMasterService service)
    {
        mService = service;
    }
    
    
    public AccessTallyMasterJsBean getAccessTallyMaster(String guid) throws WebException
    {
        try {
            AccessTallyMaster accessTallyMaster = getServiceProxy().getAccessTallyMaster(guid);
            AccessTallyMasterJsBean bean = convertAccessTallyMasterToJsBean(accessTallyMaster);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getAccessTallyMaster(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getAccessTallyMaster(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AccessTallyMasterJsBean> getAccessTallyMasters(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AccessTallyMasterJsBean> jsBeans = new ArrayList<AccessTallyMasterJsBean>();
            List<AccessTallyMaster> accessTallyMasters = getServiceProxy().getAccessTallyMasters(guids);
            if(accessTallyMasters != null) {
                for(AccessTallyMaster accessTallyMaster : accessTallyMasters) {
                    jsBeans.add(convertAccessTallyMasterToJsBean(accessTallyMaster));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AccessTallyMasterJsBean> getAllAccessTallyMasters() throws WebException
    {
        return getAllAccessTallyMasters(null, null, null);
    }

    // @Deprecated
    public List<AccessTallyMasterJsBean> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    public List<AccessTallyMasterJsBean> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<AccessTallyMasterJsBean> jsBeans = new ArrayList<AccessTallyMasterJsBean>();
            List<AccessTallyMaster> accessTallyMasters = getServiceProxy().getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
            if(accessTallyMasters != null) {
                for(AccessTallyMaster accessTallyMaster : accessTallyMasters) {
                    jsBeans.add(convertAccessTallyMasterToJsBean(accessTallyMaster));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<AccessTallyMasterJsBean> findAccessTallyMasters(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<AccessTallyMasterJsBean> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<AccessTallyMasterJsBean> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<AccessTallyMasterJsBean> jsBeans = new ArrayList<AccessTallyMasterJsBean>();
            List<AccessTallyMaster> accessTallyMasters = getServiceProxy().findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(accessTallyMasters != null) {
                for(AccessTallyMaster accessTallyMaster : accessTallyMasters) {
                    jsBeans.add(convertAccessTallyMasterToJsBean(accessTallyMaster));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws WebException
    {
        try {
            return getServiceProxy().createAccessTallyMaster(tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            return getServiceProxy().createAccessTallyMaster(accessTallyMaster);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AccessTallyMasterJsBean constructAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            accessTallyMaster = getServiceProxy().constructAccessTallyMaster(accessTallyMaster);
            jsBean = convertAccessTallyMasterToJsBean(accessTallyMaster);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws WebException
    {
        try {
            return getServiceProxy().updateAccessTallyMaster(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            return getServiceProxy().updateAccessTallyMaster(accessTallyMaster);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AccessTallyMasterJsBean refreshAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            accessTallyMaster = getServiceProxy().refreshAccessTallyMaster(accessTallyMaster);
            jsBean = convertAccessTallyMasterToJsBean(accessTallyMaster);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAccessTallyMaster(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteAccessTallyMaster(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAccessTallyMaster(AccessTallyMasterJsBean jsBean) throws WebException
    {
        try {
            AccessTallyMaster accessTallyMaster = convertAccessTallyMasterJsBeanToBean(jsBean);
            return getServiceProxy().deleteAccessTallyMaster(accessTallyMaster);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteAccessTallyMasters(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static AccessTallyMasterJsBean convertAccessTallyMasterToJsBean(AccessTallyMaster accessTallyMaster)
    {
        AccessTallyMasterJsBean jsBean = null;
        if(accessTallyMaster != null) {
            jsBean = new AccessTallyMasterJsBean();
            jsBean.setGuid(accessTallyMaster.getGuid());
            jsBean.setTallyType(accessTallyMaster.getTallyType());
            jsBean.setTallyTime(accessTallyMaster.getTallyTime());
            jsBean.setTallyEpoch(accessTallyMaster.getTallyEpoch());
            jsBean.setTallyStatus(accessTallyMaster.getTallyStatus());
            jsBean.setAccessRecordCount(accessTallyMaster.getAccessRecordCount());
            jsBean.setProcesingStartedTime(accessTallyMaster.getProcesingStartedTime());
            jsBean.setCreatedTime(accessTallyMaster.getCreatedTime());
            jsBean.setModifiedTime(accessTallyMaster.getModifiedTime());
        }
        return jsBean;
    }

    public static AccessTallyMaster convertAccessTallyMasterJsBeanToBean(AccessTallyMasterJsBean jsBean)
    {
        AccessTallyMasterBean accessTallyMaster = null;
        if(jsBean != null) {
            accessTallyMaster = new AccessTallyMasterBean();
            accessTallyMaster.setGuid(jsBean.getGuid());
            accessTallyMaster.setTallyType(jsBean.getTallyType());
            accessTallyMaster.setTallyTime(jsBean.getTallyTime());
            accessTallyMaster.setTallyEpoch(jsBean.getTallyEpoch());
            accessTallyMaster.setTallyStatus(jsBean.getTallyStatus());
            accessTallyMaster.setAccessRecordCount(jsBean.getAccessRecordCount());
            accessTallyMaster.setProcesingStartedTime(jsBean.getProcesingStartedTime());
            accessTallyMaster.setCreatedTime(jsBean.getCreatedTime());
            accessTallyMaster.setModifiedTime(jsBean.getModifiedTime());
        }
        return accessTallyMaster;
    }

}
