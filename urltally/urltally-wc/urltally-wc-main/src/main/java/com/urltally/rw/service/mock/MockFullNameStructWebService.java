package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.FullNameStruct;
import com.urltally.af.bean.FullNameStructBean;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.FullNameStructJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.FullNameStructWebService;


// MockFullNameStructWebService is a mock object.
// It can be used as a base class to mock FullNameStructWebService objects.
public abstract class MockFullNameStructWebService extends FullNameStructWebService  // implements FullNameStructService
{
    private static final Logger log = Logger.getLogger(MockFullNameStructWebService.class.getName());
     

}
