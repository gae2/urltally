package com.urltally.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.core.StatusCode;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.KeyListStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessStub;
import com.urltally.ws.stub.MonthlyShortUrlAccessListStub;
import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.util.StringUtil;
import com.urltally.rf.auth.TwoLeggedOAuthClientUtil;
import com.urltally.rf.config.Config;
import com.urltally.rf.proxy.MonthlyShortUrlAccessServiceProxy;


// MockMonthlyShortUrlAccessServiceProxy is a decorator.
// It can be used as a base class to mock MonthlyShortUrlAccessService objects.
public abstract class MockMonthlyShortUrlAccessServiceProxy extends MonthlyShortUrlAccessServiceProxy implements MonthlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockMonthlyShortUrlAccessServiceProxy.class.getName());

    // MockMonthlyShortUrlAccessServiceProxy uses the decorator design pattern.
    private MonthlyShortUrlAccessService decoratedProxy;

    public MockMonthlyShortUrlAccessServiceProxy(MonthlyShortUrlAccessService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected MonthlyShortUrlAccessService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(MonthlyShortUrlAccessService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public MonthlyShortUrlAccess getMonthlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.getMonthlyShortUrlAccess(guid);
    }

    @Override
    public Object getMonthlyShortUrlAccess(String guid, String field) throws BaseException
    {
        return decoratedProxy.getMonthlyShortUrlAccess(guid, field);
    }

    @Override
    public List<MonthlyShortUrlAccess> getMonthlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getMonthlyShortUrlAccesses(guids);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses() throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllMonthlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllMonthlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<MonthlyShortUrlAccess> findMonthlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findMonthlyShortUrlAccesses(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createMonthlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        return decoratedProxy.createMonthlyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
    }

    @Override
    public String createMonthlyShortUrlAccess(MonthlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.createMonthlyShortUrlAccess(bean);
    }

    @Override
    public MonthlyShortUrlAccess constructMonthlyShortUrlAccess(MonthlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.constructMonthlyShortUrlAccess(bean);
    }

    @Override
    public Boolean updateMonthlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws BaseException
    {
        return decoratedProxy.updateMonthlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
    }

    @Override
    public Boolean updateMonthlyShortUrlAccess(MonthlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.updateMonthlyShortUrlAccess(bean);
    }

    @Override
    public MonthlyShortUrlAccess refreshMonthlyShortUrlAccess(MonthlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.refreshMonthlyShortUrlAccess(bean);
    }

    @Override
    public Boolean deleteMonthlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.deleteMonthlyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteMonthlyShortUrlAccess(MonthlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.deleteMonthlyShortUrlAccess(bean);
    }

    @Override
    public Long deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteMonthlyShortUrlAccesses(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createMonthlyShortUrlAccesses(List<MonthlyShortUrlAccess> monthlyShortUrlAccesses) throws BaseException
    {
        return decoratedProxy.createMonthlyShortUrlAccesses(monthlyShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateMonthlyShortUrlAccesses(List<MonthlyShortUrlAccess> monthlyShortUrlAccesses) throws BaseException
    //{
    //}

}
