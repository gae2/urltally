package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.GaeAppStruct;
import com.urltally.af.bean.GaeAppStructBean;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.GaeAppStructJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.GaeAppStructWebService;


// MockGaeAppStructWebService is a mock object.
// It can be used as a base class to mock GaeAppStructWebService objects.
public abstract class MockGaeAppStructWebService extends GaeAppStructWebService  // implements GaeAppStructService
{
    private static final Logger log = Logger.getLogger(MockGaeAppStructWebService.class.getName());
     

}
