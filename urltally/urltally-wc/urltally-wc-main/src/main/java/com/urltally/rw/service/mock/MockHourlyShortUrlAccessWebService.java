package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.HourlyShortUrlAccessJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.HourlyShortUrlAccessWebService;


// MockHourlyShortUrlAccessWebService is a mock object.
// It can be used as a base class to mock HourlyShortUrlAccessWebService objects.
public abstract class MockHourlyShortUrlAccessWebService extends HourlyShortUrlAccessWebService  // implements HourlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockHourlyShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private HourlyShortUrlAccessWebService mService = null;

    public MockHourlyShortUrlAccessWebService()
    {
        this(MockServiceProxyFactory.getInstance().getHourlyShortUrlAccessServiceProxy());
    }
    public MockHourlyShortUrlAccessWebService(HourlyShortUrlAccessService service)
    {
        super(service);
    }


}
