package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.ReferrerInfoStruct;
import com.urltally.af.bean.ReferrerInfoStructBean;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.ReferrerInfoStructJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.ReferrerInfoStructWebService;


// MockReferrerInfoStructWebService is a mock object.
// It can be used as a base class to mock ReferrerInfoStructWebService objects.
public abstract class MockReferrerInfoStructWebService extends ReferrerInfoStructWebService  // implements ReferrerInfoStructService
{
    private static final Logger log = Logger.getLogger(MockReferrerInfoStructWebService.class.getName());
     

}
