package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CellLatitudeLongitude;
import com.urltally.af.bean.CellLatitudeLongitudeBean;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.CellLatitudeLongitudeJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.CellLatitudeLongitudeWebService;


// MockCellLatitudeLongitudeWebService is a mock object.
// It can be used as a base class to mock CellLatitudeLongitudeWebService objects.
public abstract class MockCellLatitudeLongitudeWebService extends CellLatitudeLongitudeWebService  // implements CellLatitudeLongitudeService
{
    private static final Logger log = Logger.getLogger(MockCellLatitudeLongitudeWebService.class.getName());
     

}
