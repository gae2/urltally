package com.urltally.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.core.StatusCode;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.HourlyShortUrlAccess;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.KeyListStub;
import com.urltally.ws.stub.HourlyShortUrlAccessStub;
import com.urltally.ws.stub.HourlyShortUrlAccessListStub;
import com.urltally.af.bean.HourlyShortUrlAccessBean;
import com.urltally.af.service.HourlyShortUrlAccessService;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.util.StringUtil;
import com.urltally.rf.auth.TwoLeggedOAuthClientUtil;
import com.urltally.rf.config.Config;
import com.urltally.rf.proxy.HourlyShortUrlAccessServiceProxy;


// MockHourlyShortUrlAccessServiceProxy is a decorator.
// It can be used as a base class to mock HourlyShortUrlAccessService objects.
public abstract class MockHourlyShortUrlAccessServiceProxy extends HourlyShortUrlAccessServiceProxy implements HourlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockHourlyShortUrlAccessServiceProxy.class.getName());

    // MockHourlyShortUrlAccessServiceProxy uses the decorator design pattern.
    private HourlyShortUrlAccessService decoratedProxy;

    public MockHourlyShortUrlAccessServiceProxy(HourlyShortUrlAccessService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected HourlyShortUrlAccessService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(HourlyShortUrlAccessService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public HourlyShortUrlAccess getHourlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.getHourlyShortUrlAccess(guid);
    }

    @Override
    public Object getHourlyShortUrlAccess(String guid, String field) throws BaseException
    {
        return decoratedProxy.getHourlyShortUrlAccess(guid, field);
    }

    @Override
    public List<HourlyShortUrlAccess> getHourlyShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getHourlyShortUrlAccesses(guids);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses() throws BaseException
    {
        return getAllHourlyShortUrlAccesses(null, null, null);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllHourlyShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccess> getAllHourlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllHourlyShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllHourlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllHourlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllHourlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findHourlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<HourlyShortUrlAccess> findHourlyShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findHourlyShortUrlAccesses(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findHourlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findHourlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createHourlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {
        return decoratedProxy.createHourlyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
    }

    @Override
    public String createHourlyShortUrlAccess(HourlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.createHourlyShortUrlAccess(bean);
    }

    @Override
    public HourlyShortUrlAccess constructHourlyShortUrlAccess(HourlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.constructHourlyShortUrlAccess(bean);
    }

    @Override
    public Boolean updateHourlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer day, Integer hour) throws BaseException
    {
        return decoratedProxy.updateHourlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, day, hour);
    }

    @Override
    public Boolean updateHourlyShortUrlAccess(HourlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.updateHourlyShortUrlAccess(bean);
    }

    @Override
    public HourlyShortUrlAccess refreshHourlyShortUrlAccess(HourlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.refreshHourlyShortUrlAccess(bean);
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.deleteHourlyShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteHourlyShortUrlAccess(HourlyShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.deleteHourlyShortUrlAccess(bean);
    }

    @Override
    public Long deleteHourlyShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteHourlyShortUrlAccesses(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createHourlyShortUrlAccesses(List<HourlyShortUrlAccess> hourlyShortUrlAccesses) throws BaseException
    {
        return decoratedProxy.createHourlyShortUrlAccesses(hourlyShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateHourlyShortUrlAccesses(List<HourlyShortUrlAccess> hourlyShortUrlAccesses) throws BaseException
    //{
    //}

}
