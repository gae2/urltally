package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.af.bean.CumulativeShortUrlAccessBean;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.CumulativeShortUrlAccessJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.CumulativeShortUrlAccessWebService;


// MockCumulativeShortUrlAccessWebService is a mock object.
// It can be used as a base class to mock CumulativeShortUrlAccessWebService objects.
public abstract class MockCumulativeShortUrlAccessWebService extends CumulativeShortUrlAccessWebService  // implements CumulativeShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockCumulativeShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private CumulativeShortUrlAccessWebService mService = null;

    public MockCumulativeShortUrlAccessWebService()
    {
        this(MockServiceProxyFactory.getInstance().getCumulativeShortUrlAccessServiceProxy());
    }
    public MockCumulativeShortUrlAccessWebService(CumulativeShortUrlAccessService service)
    {
        super(service);
    }


}
