package com.urltally.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.TotalShortUrlAccessJsBean;
import com.urltally.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TotalShortUrlAccessWebService // implements TotalShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private TotalShortUrlAccessService mService = null;

    public TotalShortUrlAccessWebService()
    {
        this(ServiceProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy());
    }
    public TotalShortUrlAccessWebService(TotalShortUrlAccessService service)
    {
        mService = service;
    }
    
    protected TotalShortUrlAccessService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(TotalShortUrlAccessService service)
    {
        mService = service;
    }
    
    
    public TotalShortUrlAccessJsBean getTotalShortUrlAccess(String guid) throws WebException
    {
        try {
            TotalShortUrlAccess totalShortUrlAccess = getServiceProxy().getTotalShortUrlAccess(guid);
            TotalShortUrlAccessJsBean bean = convertTotalShortUrlAccessToJsBean(totalShortUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTotalShortUrlAccess(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getTotalShortUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TotalShortUrlAccessJsBean> getTotalShortUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TotalShortUrlAccessJsBean> jsBeans = new ArrayList<TotalShortUrlAccessJsBean>();
            List<TotalShortUrlAccess> totalShortUrlAccesses = getServiceProxy().getTotalShortUrlAccesses(guids);
            if(totalShortUrlAccesses != null) {
                for(TotalShortUrlAccess totalShortUrlAccess : totalShortUrlAccesses) {
                    jsBeans.add(convertTotalShortUrlAccessToJsBean(totalShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TotalShortUrlAccessJsBean> getAllTotalShortUrlAccesses() throws WebException
    {
        return getAllTotalShortUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<TotalShortUrlAccessJsBean> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTotalShortUrlAccesses(ordering, offset, count, null);
    }

    public List<TotalShortUrlAccessJsBean> getAllTotalShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TotalShortUrlAccessJsBean> jsBeans = new ArrayList<TotalShortUrlAccessJsBean>();
            List<TotalShortUrlAccess> totalShortUrlAccesses = getServiceProxy().getAllTotalShortUrlAccesses(ordering, offset, count, forwardCursor);
            if(totalShortUrlAccesses != null) {
                for(TotalShortUrlAccess totalShortUrlAccess : totalShortUrlAccesses) {
                    jsBeans.add(convertTotalShortUrlAccessToJsBean(totalShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTotalShortUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllTotalShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllTotalShortUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<TotalShortUrlAccessJsBean> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<TotalShortUrlAccessJsBean> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<TotalShortUrlAccessJsBean> findTotalShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TotalShortUrlAccessJsBean> jsBeans = new ArrayList<TotalShortUrlAccessJsBean>();
            List<TotalShortUrlAccess> totalShortUrlAccesses = getServiceProxy().findTotalShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(totalShortUrlAccesses != null) {
                for(TotalShortUrlAccess totalShortUrlAccess : totalShortUrlAccesses) {
                    jsBeans.add(convertTotalShortUrlAccessToJsBean(totalShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findTotalShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findTotalShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTotalShortUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws WebException
    {
        try {
            return getServiceProxy().createTotalShortUrlAccess(tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTotalShortUrlAccess(TotalShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalShortUrlAccess totalShortUrlAccess = convertTotalShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().createTotalShortUrlAccess(totalShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TotalShortUrlAccessJsBean constructTotalShortUrlAccess(TotalShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalShortUrlAccess totalShortUrlAccess = convertTotalShortUrlAccessJsBeanToBean(jsBean);
            totalShortUrlAccess = getServiceProxy().constructTotalShortUrlAccess(totalShortUrlAccess);
            jsBean = convertTotalShortUrlAccessToJsBean(totalShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTotalShortUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain) throws WebException
    {
        try {
            return getServiceProxy().updateTotalShortUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTotalShortUrlAccess(TotalShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalShortUrlAccess totalShortUrlAccess = convertTotalShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().updateTotalShortUrlAccess(totalShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TotalShortUrlAccessJsBean refreshTotalShortUrlAccess(TotalShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalShortUrlAccess totalShortUrlAccess = convertTotalShortUrlAccessJsBeanToBean(jsBean);
            totalShortUrlAccess = getServiceProxy().refreshTotalShortUrlAccess(totalShortUrlAccess);
            jsBean = convertTotalShortUrlAccessToJsBean(totalShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTotalShortUrlAccess(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteTotalShortUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTotalShortUrlAccess(TotalShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalShortUrlAccess totalShortUrlAccess = convertTotalShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().deleteTotalShortUrlAccess(totalShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTotalShortUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteTotalShortUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TotalShortUrlAccessJsBean convertTotalShortUrlAccessToJsBean(TotalShortUrlAccess totalShortUrlAccess)
    {
        TotalShortUrlAccessJsBean jsBean = null;
        if(totalShortUrlAccess != null) {
            jsBean = new TotalShortUrlAccessJsBean();
            jsBean.setGuid(totalShortUrlAccess.getGuid());
            jsBean.setTallyType(totalShortUrlAccess.getTallyType());
            jsBean.setTallyTime(totalShortUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(totalShortUrlAccess.getTallyEpoch());
            jsBean.setCount(totalShortUrlAccess.getCount());
            jsBean.setShortUrl(totalShortUrlAccess.getShortUrl());
            jsBean.setShortUrlDomain(totalShortUrlAccess.getShortUrlDomain());
            jsBean.setCreatedTime(totalShortUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(totalShortUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static TotalShortUrlAccess convertTotalShortUrlAccessJsBeanToBean(TotalShortUrlAccessJsBean jsBean)
    {
        TotalShortUrlAccessBean totalShortUrlAccess = null;
        if(jsBean != null) {
            totalShortUrlAccess = new TotalShortUrlAccessBean();
            totalShortUrlAccess.setGuid(jsBean.getGuid());
            totalShortUrlAccess.setTallyType(jsBean.getTallyType());
            totalShortUrlAccess.setTallyTime(jsBean.getTallyTime());
            totalShortUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            totalShortUrlAccess.setCount(jsBean.getCount());
            totalShortUrlAccess.setShortUrl(jsBean.getShortUrl());
            totalShortUrlAccess.setShortUrlDomain(jsBean.getShortUrlDomain());
            totalShortUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            totalShortUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return totalShortUrlAccess;
    }

}
