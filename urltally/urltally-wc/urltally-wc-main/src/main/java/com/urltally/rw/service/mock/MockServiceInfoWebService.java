package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.ServiceInfo;
import com.urltally.af.bean.ServiceInfoBean;
import com.urltally.af.service.ServiceInfoService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.ServiceInfoJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.ServiceInfoWebService;


// MockServiceInfoWebService is a mock object.
// It can be used as a base class to mock ServiceInfoWebService objects.
public abstract class MockServiceInfoWebService extends ServiceInfoWebService  // implements ServiceInfoService
{
    private static final Logger log = Logger.getLogger(MockServiceInfoWebService.class.getName());
     
    // Af service interface.
    private ServiceInfoWebService mService = null;

    public MockServiceInfoWebService()
    {
        this(MockServiceProxyFactory.getInstance().getServiceInfoServiceProxy());
    }
    public MockServiceInfoWebService(ServiceInfoService service)
    {
        super(service);
    }


}
