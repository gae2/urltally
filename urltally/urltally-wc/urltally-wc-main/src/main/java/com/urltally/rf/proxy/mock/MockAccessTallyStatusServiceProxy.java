package com.urltally.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.core.StatusCode;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.KeyListStub;
import com.urltally.ws.stub.AccessTallyStatusStub;
import com.urltally.ws.stub.AccessTallyStatusListStub;
import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.util.StringUtil;
import com.urltally.rf.auth.TwoLeggedOAuthClientUtil;
import com.urltally.rf.config.Config;
import com.urltally.rf.proxy.AccessTallyStatusServiceProxy;


// MockAccessTallyStatusServiceProxy is a decorator.
// It can be used as a base class to mock AccessTallyStatusService objects.
public abstract class MockAccessTallyStatusServiceProxy extends AccessTallyStatusServiceProxy implements AccessTallyStatusService
{
    private static final Logger log = Logger.getLogger(MockAccessTallyStatusServiceProxy.class.getName());

    // MockAccessTallyStatusServiceProxy uses the decorator design pattern.
    private AccessTallyStatusService decoratedProxy;

    public MockAccessTallyStatusServiceProxy(AccessTallyStatusService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected AccessTallyStatusService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(AccessTallyStatusService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public AccessTallyStatus getAccessTallyStatus(String guid) throws BaseException
    {
        return decoratedProxy.getAccessTallyStatus(guid);
    }

    @Override
    public Object getAccessTallyStatus(String guid, String field) throws BaseException
    {
        return decoratedProxy.getAccessTallyStatus(guid, field);
    }

    @Override
    public List<AccessTallyStatus> getAccessTallyStatuses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getAccessTallyStatuses(guids);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses() throws BaseException
    {
        return getAllAccessTallyStatuses(null, null, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatuses(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> getAllAccessTallyStatuses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAccessTallyStatuses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyStatusKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyStatusKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAccessTallyStatusKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatuses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyStatus> findAccessTallyStatuses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAccessTallyStatuses(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyStatusKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAccessTallyStatusKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAccessTallyStatus(String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        return decoratedProxy.createAccessTallyStatus(remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
    }

    @Override
    public String createAccessTallyStatus(AccessTallyStatus bean) throws BaseException
    {
        return decoratedProxy.createAccessTallyStatus(bean);
    }

    @Override
    public AccessTallyStatus constructAccessTallyStatus(AccessTallyStatus bean) throws BaseException
    {
        return decoratedProxy.constructAccessTallyStatus(bean);
    }

    @Override
    public Boolean updateAccessTallyStatus(String guid, String remoteRecordGuid, String shortUrl, String tallyType, String tallyTime, Long tallyEpoch, Boolean processed) throws BaseException
    {
        return decoratedProxy.updateAccessTallyStatus(guid, remoteRecordGuid, shortUrl, tallyType, tallyTime, tallyEpoch, processed);
    }

    @Override
    public Boolean updateAccessTallyStatus(AccessTallyStatus bean) throws BaseException
    {
        return decoratedProxy.updateAccessTallyStatus(bean);
    }

    @Override
    public AccessTallyStatus refreshAccessTallyStatus(AccessTallyStatus bean) throws BaseException
    {
        return decoratedProxy.refreshAccessTallyStatus(bean);
    }

    @Override
    public Boolean deleteAccessTallyStatus(String guid) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyStatus(guid);
    }

    @Override
    public Boolean deleteAccessTallyStatus(AccessTallyStatus bean) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyStatus(bean);
    }

    @Override
    public Long deleteAccessTallyStatuses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyStatuses(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    {
        return decoratedProxy.createAccessTallyStatuses(accessTallyStatuses);
    }

    // TBD
    //@Override
    //public Boolean updateAccessTallyStatuses(List<AccessTallyStatus> accessTallyStatuses) throws BaseException
    //{
    //}

}
