package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyStatus;
import com.urltally.af.bean.AccessTallyStatusBean;
import com.urltally.af.service.AccessTallyStatusService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.AccessTallyStatusJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.AccessTallyStatusWebService;


// MockAccessTallyStatusWebService is a mock object.
// It can be used as a base class to mock AccessTallyStatusWebService objects.
public abstract class MockAccessTallyStatusWebService extends AccessTallyStatusWebService  // implements AccessTallyStatusService
{
    private static final Logger log = Logger.getLogger(MockAccessTallyStatusWebService.class.getName());
     
    // Af service interface.
    private AccessTallyStatusWebService mService = null;

    public MockAccessTallyStatusWebService()
    {
        this(MockServiceProxyFactory.getInstance().getAccessTallyStatusServiceProxy());
    }
    public MockAccessTallyStatusWebService(AccessTallyStatusService service)
    {
        super(service);
    }


}
