package com.urltally.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.core.StatusCode;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.KeyListStub;
import com.urltally.ws.stub.AccessTallyMasterStub;
import com.urltally.ws.stub.AccessTallyMasterListStub;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.util.StringUtil;
import com.urltally.rf.auth.TwoLeggedOAuthClientUtil;
import com.urltally.rf.config.Config;
import com.urltally.rf.proxy.AccessTallyMasterServiceProxy;


// MockAccessTallyMasterServiceProxy is a decorator.
// It can be used as a base class to mock AccessTallyMasterService objects.
public abstract class MockAccessTallyMasterServiceProxy extends AccessTallyMasterServiceProxy implements AccessTallyMasterService
{
    private static final Logger log = Logger.getLogger(MockAccessTallyMasterServiceProxy.class.getName());

    // MockAccessTallyMasterServiceProxy uses the decorator design pattern.
    private AccessTallyMasterService decoratedProxy;

    public MockAccessTallyMasterServiceProxy(AccessTallyMasterService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected AccessTallyMasterService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(AccessTallyMasterService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public AccessTallyMaster getAccessTallyMaster(String guid) throws BaseException
    {
        return decoratedProxy.getAccessTallyMaster(guid);
    }

    @Override
    public Object getAccessTallyMaster(String guid, String field) throws BaseException
    {
        return decoratedProxy.getAccessTallyMaster(guid, field);
    }

    @Override
    public List<AccessTallyMaster> getAccessTallyMasters(List<String> guids) throws BaseException
    {
        return decoratedProxy.getAccessTallyMasters(guids);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters() throws BaseException
    {
        return getAllAccessTallyMasters(null, null, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasters(ordering, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> getAllAccessTallyMasters(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAccessTallyMasters(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAccessTallyMasterKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAccessTallyMasterKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllAccessTallyMasterKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasters(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AccessTallyMaster> findAccessTallyMasters(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAccessTallyMasters(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAccessTallyMasterKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findAccessTallyMasterKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAccessTallyMaster(String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        return decoratedProxy.createAccessTallyMaster(tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }

    @Override
    public String createAccessTallyMaster(AccessTallyMaster bean) throws BaseException
    {
        return decoratedProxy.createAccessTallyMaster(bean);
    }

    @Override
    public AccessTallyMaster constructAccessTallyMaster(AccessTallyMaster bean) throws BaseException
    {
        return decoratedProxy.constructAccessTallyMaster(bean);
    }

    @Override
    public Boolean updateAccessTallyMaster(String guid, String tallyType, String tallyTime, Long tallyEpoch, String tallyStatus, Integer accessRecordCount, Long procesingStartedTime) throws BaseException
    {
        return decoratedProxy.updateAccessTallyMaster(guid, tallyType, tallyTime, tallyEpoch, tallyStatus, accessRecordCount, procesingStartedTime);
    }

    @Override
    public Boolean updateAccessTallyMaster(AccessTallyMaster bean) throws BaseException
    {
        return decoratedProxy.updateAccessTallyMaster(bean);
    }

    @Override
    public AccessTallyMaster refreshAccessTallyMaster(AccessTallyMaster bean) throws BaseException
    {
        return decoratedProxy.refreshAccessTallyMaster(bean);
    }

    @Override
    public Boolean deleteAccessTallyMaster(String guid) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyMaster(guid);
    }

    @Override
    public Boolean deleteAccessTallyMaster(AccessTallyMaster bean) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyMaster(bean);
    }

    @Override
    public Long deleteAccessTallyMasters(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteAccessTallyMasters(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException
    {
        return decoratedProxy.createAccessTallyMasters(accessTallyMasters);
    }

    // TBD
    //@Override
    //public Boolean updateAccessTallyMasters(List<AccessTallyMaster> accessTallyMasters) throws BaseException
    //{
    //}

}
