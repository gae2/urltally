package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CurrentShortUrlAccess;
import com.urltally.af.bean.CurrentShortUrlAccessBean;
import com.urltally.af.service.CurrentShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.CurrentShortUrlAccessJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.CurrentShortUrlAccessWebService;


// MockCurrentShortUrlAccessWebService is a mock object.
// It can be used as a base class to mock CurrentShortUrlAccessWebService objects.
public abstract class MockCurrentShortUrlAccessWebService extends CurrentShortUrlAccessWebService  // implements CurrentShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockCurrentShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private CurrentShortUrlAccessWebService mService = null;

    public MockCurrentShortUrlAccessWebService()
    {
        this(MockServiceProxyFactory.getInstance().getCurrentShortUrlAccessServiceProxy());
    }
    public MockCurrentShortUrlAccessWebService(CurrentShortUrlAccessService service)
    {
        super(service);
    }


}
