package com.urltally.rf.proxy.mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.urltally.ws.BaseException;
import com.urltally.ws.exception.NotImplementedException;
import com.urltally.ws.core.StatusCode;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.ws.stub.ErrorStub;
import com.urltally.ws.stub.KeyListStub;
import com.urltally.ws.stub.CumulativeShortUrlAccessStub;
import com.urltally.ws.stub.CumulativeShortUrlAccessListStub;
import com.urltally.af.bean.CumulativeShortUrlAccessBean;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.af.util.MarshalHelper;
import com.urltally.af.util.StringUtil;
import com.urltally.rf.auth.TwoLeggedOAuthClientUtil;
import com.urltally.rf.config.Config;
import com.urltally.rf.proxy.CumulativeShortUrlAccessServiceProxy;


// MockCumulativeShortUrlAccessServiceProxy is a decorator.
// It can be used as a base class to mock CumulativeShortUrlAccessService objects.
public abstract class MockCumulativeShortUrlAccessServiceProxy extends CumulativeShortUrlAccessServiceProxy implements CumulativeShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockCumulativeShortUrlAccessServiceProxy.class.getName());

    // MockCumulativeShortUrlAccessServiceProxy uses the decorator design pattern.
    private CumulativeShortUrlAccessService decoratedProxy;

    public MockCumulativeShortUrlAccessServiceProxy(CumulativeShortUrlAccessService decoratedProxy)
    {
        this.decoratedProxy = decoratedProxy;
    }

    // To be used by subclasses
    protected CumulativeShortUrlAccessService getDecoratedServiceProxy()
    {
        return decoratedProxy;
    }
    // Ctor injector only. No setter injector.
    // public void setDecoratedServiceProxy(CumulativeShortUrlAccessService decoratedProxy)
    // {
    //     this.decoratedProxy = decoratedProxy;
    // }


    @Override
    public CumulativeShortUrlAccess getCumulativeShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.getCumulativeShortUrlAccess(guid);
    }

    @Override
    public Object getCumulativeShortUrlAccess(String guid, String field) throws BaseException
    {
        return decoratedProxy.getCumulativeShortUrlAccess(guid, field);
    }

    @Override
    public List<CumulativeShortUrlAccess> getCumulativeShortUrlAccesses(List<String> guids) throws BaseException
    {
        return decoratedProxy.getCumulativeShortUrlAccesses(guids);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses() throws BaseException
    {
        return getAllCumulativeShortUrlAccesses(null, null, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCumulativeShortUrlAccesses(ordering, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllCumulativeShortUrlAccesses(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllCumulativeShortUrlAccessKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.getAllCumulativeShortUrlAccessKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<CumulativeShortUrlAccess> findCumulativeShortUrlAccesses(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findCumulativeShortUrlAccesses(filter, ordering, params,
            values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return decoratedProxy.findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique,offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return decoratedProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createCumulativeShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {
        return decoratedProxy.createCumulativeShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
    }

    @Override
    public String createCumulativeShortUrlAccess(CumulativeShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.createCumulativeShortUrlAccess(bean);
    }

    @Override
    public CumulativeShortUrlAccess constructCumulativeShortUrlAccess(CumulativeShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.constructCumulativeShortUrlAccess(bean);
    }

    @Override
    public Boolean updateCumulativeShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws BaseException
    {
        return decoratedProxy.updateCumulativeShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
    }

    @Override
    public Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.updateCumulativeShortUrlAccess(bean);
    }

    @Override
    public CumulativeShortUrlAccess refreshCumulativeShortUrlAccess(CumulativeShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.refreshCumulativeShortUrlAccess(bean);
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(String guid) throws BaseException
    {
        return decoratedProxy.deleteCumulativeShortUrlAccess(guid);
    }

    @Override
    public Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccess bean) throws BaseException
    {
        return decoratedProxy.deleteCumulativeShortUrlAccess(bean);
    }

    @Override
    public Long deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws BaseException
    {
        return decoratedProxy.deleteCumulativeShortUrlAccesses(filter, params, values);
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createCumulativeShortUrlAccesses(List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses) throws BaseException
    {
        return decoratedProxy.createCumulativeShortUrlAccesses(cumulativeShortUrlAccesses);
    }

    // TBD
    //@Override
    //public Boolean updateCumulativeShortUrlAccesses(List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses) throws BaseException
    //{
    //}

}
