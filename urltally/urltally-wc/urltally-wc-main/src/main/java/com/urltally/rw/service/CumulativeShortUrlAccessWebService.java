package com.urltally.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.CumulativeShortUrlAccess;
import com.urltally.af.bean.CumulativeShortUrlAccessBean;
import com.urltally.af.service.CumulativeShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.CumulativeShortUrlAccessJsBean;
import com.urltally.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class CumulativeShortUrlAccessWebService // implements CumulativeShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(CumulativeShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private CumulativeShortUrlAccessService mService = null;

    public CumulativeShortUrlAccessWebService()
    {
        this(ServiceProxyFactory.getInstance().getCumulativeShortUrlAccessServiceProxy());
    }
    public CumulativeShortUrlAccessWebService(CumulativeShortUrlAccessService service)
    {
        mService = service;
    }
    
    protected CumulativeShortUrlAccessService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getCumulativeShortUrlAccessServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(CumulativeShortUrlAccessService service)
    {
        mService = service;
    }
    
    
    public CumulativeShortUrlAccessJsBean getCumulativeShortUrlAccess(String guid) throws WebException
    {
        try {
            CumulativeShortUrlAccess cumulativeShortUrlAccess = getServiceProxy().getCumulativeShortUrlAccess(guid);
            CumulativeShortUrlAccessJsBean bean = convertCumulativeShortUrlAccessToJsBean(cumulativeShortUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getCumulativeShortUrlAccess(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getCumulativeShortUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<CumulativeShortUrlAccessJsBean> getCumulativeShortUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<CumulativeShortUrlAccessJsBean> jsBeans = new ArrayList<CumulativeShortUrlAccessJsBean>();
            List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses = getServiceProxy().getCumulativeShortUrlAccesses(guids);
            if(cumulativeShortUrlAccesses != null) {
                for(CumulativeShortUrlAccess cumulativeShortUrlAccess : cumulativeShortUrlAccesses) {
                    jsBeans.add(convertCumulativeShortUrlAccessToJsBean(cumulativeShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<CumulativeShortUrlAccessJsBean> getAllCumulativeShortUrlAccesses() throws WebException
    {
        return getAllCumulativeShortUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<CumulativeShortUrlAccessJsBean> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllCumulativeShortUrlAccesses(ordering, offset, count, null);
    }

    public List<CumulativeShortUrlAccessJsBean> getAllCumulativeShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<CumulativeShortUrlAccessJsBean> jsBeans = new ArrayList<CumulativeShortUrlAccessJsBean>();
            List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses = getServiceProxy().getAllCumulativeShortUrlAccesses(ordering, offset, count, forwardCursor);
            if(cumulativeShortUrlAccesses != null) {
                for(CumulativeShortUrlAccess cumulativeShortUrlAccess : cumulativeShortUrlAccesses) {
                    jsBeans.add(convertCumulativeShortUrlAccessToJsBean(cumulativeShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllCumulativeShortUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllCumulativeShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllCumulativeShortUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<CumulativeShortUrlAccessJsBean> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<CumulativeShortUrlAccessJsBean> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<CumulativeShortUrlAccessJsBean> findCumulativeShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<CumulativeShortUrlAccessJsBean> jsBeans = new ArrayList<CumulativeShortUrlAccessJsBean>();
            List<CumulativeShortUrlAccess> cumulativeShortUrlAccesses = getServiceProxy().findCumulativeShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(cumulativeShortUrlAccesses != null) {
                for(CumulativeShortUrlAccess cumulativeShortUrlAccess : cumulativeShortUrlAccesses) {
                    jsBeans.add(convertCumulativeShortUrlAccessToJsBean(cumulativeShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findCumulativeShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findCumulativeShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createCumulativeShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws WebException
    {
        try {
            return getServiceProxy().createCumulativeShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createCumulativeShortUrlAccess(CumulativeShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            CumulativeShortUrlAccess cumulativeShortUrlAccess = convertCumulativeShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().createCumulativeShortUrlAccess(cumulativeShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public CumulativeShortUrlAccessJsBean constructCumulativeShortUrlAccess(CumulativeShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            CumulativeShortUrlAccess cumulativeShortUrlAccess = convertCumulativeShortUrlAccessJsBeanToBean(jsBean);
            cumulativeShortUrlAccess = getServiceProxy().constructCumulativeShortUrlAccess(cumulativeShortUrlAccess);
            jsBean = convertCumulativeShortUrlAccessToJsBean(cumulativeShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateCumulativeShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, String startDayHour, String endDayHour, Long startTime, Long endTime) throws WebException
    {
        try {
            return getServiceProxy().updateCumulativeShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, startDayHour, endDayHour, startTime, endTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateCumulativeShortUrlAccess(CumulativeShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            CumulativeShortUrlAccess cumulativeShortUrlAccess = convertCumulativeShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().updateCumulativeShortUrlAccess(cumulativeShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public CumulativeShortUrlAccessJsBean refreshCumulativeShortUrlAccess(CumulativeShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            CumulativeShortUrlAccess cumulativeShortUrlAccess = convertCumulativeShortUrlAccessJsBeanToBean(jsBean);
            cumulativeShortUrlAccess = getServiceProxy().refreshCumulativeShortUrlAccess(cumulativeShortUrlAccess);
            jsBean = convertCumulativeShortUrlAccessToJsBean(cumulativeShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteCumulativeShortUrlAccess(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteCumulativeShortUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteCumulativeShortUrlAccess(CumulativeShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            CumulativeShortUrlAccess cumulativeShortUrlAccess = convertCumulativeShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().deleteCumulativeShortUrlAccess(cumulativeShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteCumulativeShortUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteCumulativeShortUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static CumulativeShortUrlAccessJsBean convertCumulativeShortUrlAccessToJsBean(CumulativeShortUrlAccess cumulativeShortUrlAccess)
    {
        CumulativeShortUrlAccessJsBean jsBean = null;
        if(cumulativeShortUrlAccess != null) {
            jsBean = new CumulativeShortUrlAccessJsBean();
            jsBean.setGuid(cumulativeShortUrlAccess.getGuid());
            jsBean.setTallyTime(cumulativeShortUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(cumulativeShortUrlAccess.getTallyEpoch());
            jsBean.setCount(cumulativeShortUrlAccess.getCount());
            jsBean.setShortUrl(cumulativeShortUrlAccess.getShortUrl());
            jsBean.setShortUrlDomain(cumulativeShortUrlAccess.getShortUrlDomain());
            jsBean.setLongUrl(cumulativeShortUrlAccess.getLongUrl());
            jsBean.setLongUrlDomain(cumulativeShortUrlAccess.getLongUrlDomain());
            jsBean.setRedirectType(cumulativeShortUrlAccess.getRedirectType());
            jsBean.setRefererDomain(cumulativeShortUrlAccess.getRefererDomain());
            jsBean.setUserAgent(cumulativeShortUrlAccess.getUserAgent());
            jsBean.setLanguage(cumulativeShortUrlAccess.getLanguage());
            jsBean.setCountry(cumulativeShortUrlAccess.getCountry());
            jsBean.setTalliedTime(cumulativeShortUrlAccess.getTalliedTime());
            jsBean.setStartDayHour(cumulativeShortUrlAccess.getStartDayHour());
            jsBean.setEndDayHour(cumulativeShortUrlAccess.getEndDayHour());
            jsBean.setStartTime(cumulativeShortUrlAccess.getStartTime());
            jsBean.setEndTime(cumulativeShortUrlAccess.getEndTime());
            jsBean.setCreatedTime(cumulativeShortUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(cumulativeShortUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static CumulativeShortUrlAccess convertCumulativeShortUrlAccessJsBeanToBean(CumulativeShortUrlAccessJsBean jsBean)
    {
        CumulativeShortUrlAccessBean cumulativeShortUrlAccess = null;
        if(jsBean != null) {
            cumulativeShortUrlAccess = new CumulativeShortUrlAccessBean();
            cumulativeShortUrlAccess.setGuid(jsBean.getGuid());
            cumulativeShortUrlAccess.setTallyTime(jsBean.getTallyTime());
            cumulativeShortUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            cumulativeShortUrlAccess.setCount(jsBean.getCount());
            cumulativeShortUrlAccess.setShortUrl(jsBean.getShortUrl());
            cumulativeShortUrlAccess.setShortUrlDomain(jsBean.getShortUrlDomain());
            cumulativeShortUrlAccess.setLongUrl(jsBean.getLongUrl());
            cumulativeShortUrlAccess.setLongUrlDomain(jsBean.getLongUrlDomain());
            cumulativeShortUrlAccess.setRedirectType(jsBean.getRedirectType());
            cumulativeShortUrlAccess.setRefererDomain(jsBean.getRefererDomain());
            cumulativeShortUrlAccess.setUserAgent(jsBean.getUserAgent());
            cumulativeShortUrlAccess.setLanguage(jsBean.getLanguage());
            cumulativeShortUrlAccess.setCountry(jsBean.getCountry());
            cumulativeShortUrlAccess.setTalliedTime(jsBean.getTalliedTime());
            cumulativeShortUrlAccess.setStartDayHour(jsBean.getStartDayHour());
            cumulativeShortUrlAccess.setEndDayHour(jsBean.getEndDayHour());
            cumulativeShortUrlAccess.setStartTime(jsBean.getStartTime());
            cumulativeShortUrlAccess.setEndTime(jsBean.getEndTime());
            cumulativeShortUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            cumulativeShortUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return cumulativeShortUrlAccess;
    }

}
