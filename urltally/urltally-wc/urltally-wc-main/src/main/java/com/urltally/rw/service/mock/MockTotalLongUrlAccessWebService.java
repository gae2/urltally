package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.af.bean.TotalLongUrlAccessBean;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.TotalLongUrlAccessJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.TotalLongUrlAccessWebService;


// MockTotalLongUrlAccessWebService is a mock object.
// It can be used as a base class to mock TotalLongUrlAccessWebService objects.
public abstract class MockTotalLongUrlAccessWebService extends TotalLongUrlAccessWebService  // implements TotalLongUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockTotalLongUrlAccessWebService.class.getName());
     
    // Af service interface.
    private TotalLongUrlAccessWebService mService = null;

    public MockTotalLongUrlAccessWebService()
    {
        this(MockServiceProxyFactory.getInstance().getTotalLongUrlAccessServiceProxy());
    }
    public MockTotalLongUrlAccessWebService(TotalLongUrlAccessService service)
    {
        super(service);
    }


}
