package com.urltally.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.MonthlyShortUrlAccessJsBean;
import com.urltally.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class MonthlyShortUrlAccessWebService // implements MonthlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MonthlyShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private MonthlyShortUrlAccessService mService = null;

    public MonthlyShortUrlAccessWebService()
    {
        this(ServiceProxyFactory.getInstance().getMonthlyShortUrlAccessServiceProxy());
    }
    public MonthlyShortUrlAccessWebService(MonthlyShortUrlAccessService service)
    {
        mService = service;
    }
    
    protected MonthlyShortUrlAccessService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getMonthlyShortUrlAccessServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(MonthlyShortUrlAccessService service)
    {
        mService = service;
    }
    
    
    public MonthlyShortUrlAccessJsBean getMonthlyShortUrlAccess(String guid) throws WebException
    {
        try {
            MonthlyShortUrlAccess monthlyShortUrlAccess = getServiceProxy().getMonthlyShortUrlAccess(guid);
            MonthlyShortUrlAccessJsBean bean = convertMonthlyShortUrlAccessToJsBean(monthlyShortUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getMonthlyShortUrlAccess(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getMonthlyShortUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<MonthlyShortUrlAccessJsBean> getMonthlyShortUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<MonthlyShortUrlAccessJsBean> jsBeans = new ArrayList<MonthlyShortUrlAccessJsBean>();
            List<MonthlyShortUrlAccess> monthlyShortUrlAccesses = getServiceProxy().getMonthlyShortUrlAccesses(guids);
            if(monthlyShortUrlAccesses != null) {
                for(MonthlyShortUrlAccess monthlyShortUrlAccess : monthlyShortUrlAccesses) {
                    jsBeans.add(convertMonthlyShortUrlAccessToJsBean(monthlyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<MonthlyShortUrlAccessJsBean> getAllMonthlyShortUrlAccesses() throws WebException
    {
        return getAllMonthlyShortUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<MonthlyShortUrlAccessJsBean> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllMonthlyShortUrlAccesses(ordering, offset, count, null);
    }

    public List<MonthlyShortUrlAccessJsBean> getAllMonthlyShortUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<MonthlyShortUrlAccessJsBean> jsBeans = new ArrayList<MonthlyShortUrlAccessJsBean>();
            List<MonthlyShortUrlAccess> monthlyShortUrlAccesses = getServiceProxy().getAllMonthlyShortUrlAccesses(ordering, offset, count, forwardCursor);
            if(monthlyShortUrlAccesses != null) {
                for(MonthlyShortUrlAccess monthlyShortUrlAccess : monthlyShortUrlAccesses) {
                    jsBeans.add(convertMonthlyShortUrlAccessToJsBean(monthlyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllMonthlyShortUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllMonthlyShortUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllMonthlyShortUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<MonthlyShortUrlAccessJsBean> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<MonthlyShortUrlAccessJsBean> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<MonthlyShortUrlAccessJsBean> findMonthlyShortUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<MonthlyShortUrlAccessJsBean> jsBeans = new ArrayList<MonthlyShortUrlAccessJsBean>();
            List<MonthlyShortUrlAccess> monthlyShortUrlAccesses = getServiceProxy().findMonthlyShortUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(monthlyShortUrlAccesses != null) {
                for(MonthlyShortUrlAccess monthlyShortUrlAccess : monthlyShortUrlAccesses) {
                    jsBeans.add(convertMonthlyShortUrlAccessToJsBean(monthlyShortUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findMonthlyShortUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findMonthlyShortUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createMonthlyShortUrlAccess(String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws WebException
    {
        try {
            return getServiceProxy().createMonthlyShortUrlAccess(tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createMonthlyShortUrlAccess(MonthlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            MonthlyShortUrlAccess monthlyShortUrlAccess = convertMonthlyShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().createMonthlyShortUrlAccess(monthlyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public MonthlyShortUrlAccessJsBean constructMonthlyShortUrlAccess(MonthlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            MonthlyShortUrlAccess monthlyShortUrlAccess = convertMonthlyShortUrlAccessJsBeanToBean(jsBean);
            monthlyShortUrlAccess = getServiceProxy().constructMonthlyShortUrlAccess(monthlyShortUrlAccess);
            jsBean = convertMonthlyShortUrlAccessToJsBean(monthlyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateMonthlyShortUrlAccess(String guid, String tallyTime, Long tallyEpoch, Integer count, String shortUrl, String shortUrlDomain, String longUrl, String longUrlDomain, String redirectType, String refererDomain, String userAgent, String language, String country, Long talliedTime, Integer year, Integer month, Integer numberOfDays) throws WebException
    {
        try {
            return getServiceProxy().updateMonthlyShortUrlAccess(guid, tallyTime, tallyEpoch, count, shortUrl, shortUrlDomain, longUrl, longUrlDomain, redirectType, refererDomain, userAgent, language, country, talliedTime, year, month, numberOfDays);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateMonthlyShortUrlAccess(MonthlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            MonthlyShortUrlAccess monthlyShortUrlAccess = convertMonthlyShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().updateMonthlyShortUrlAccess(monthlyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public MonthlyShortUrlAccessJsBean refreshMonthlyShortUrlAccess(MonthlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            MonthlyShortUrlAccess monthlyShortUrlAccess = convertMonthlyShortUrlAccessJsBeanToBean(jsBean);
            monthlyShortUrlAccess = getServiceProxy().refreshMonthlyShortUrlAccess(monthlyShortUrlAccess);
            jsBean = convertMonthlyShortUrlAccessToJsBean(monthlyShortUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteMonthlyShortUrlAccess(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteMonthlyShortUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteMonthlyShortUrlAccess(MonthlyShortUrlAccessJsBean jsBean) throws WebException
    {
        try {
            MonthlyShortUrlAccess monthlyShortUrlAccess = convertMonthlyShortUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().deleteMonthlyShortUrlAccess(monthlyShortUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteMonthlyShortUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteMonthlyShortUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static MonthlyShortUrlAccessJsBean convertMonthlyShortUrlAccessToJsBean(MonthlyShortUrlAccess monthlyShortUrlAccess)
    {
        MonthlyShortUrlAccessJsBean jsBean = null;
        if(monthlyShortUrlAccess != null) {
            jsBean = new MonthlyShortUrlAccessJsBean();
            jsBean.setGuid(monthlyShortUrlAccess.getGuid());
            jsBean.setTallyTime(monthlyShortUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(monthlyShortUrlAccess.getTallyEpoch());
            jsBean.setCount(monthlyShortUrlAccess.getCount());
            jsBean.setShortUrl(monthlyShortUrlAccess.getShortUrl());
            jsBean.setShortUrlDomain(monthlyShortUrlAccess.getShortUrlDomain());
            jsBean.setLongUrl(monthlyShortUrlAccess.getLongUrl());
            jsBean.setLongUrlDomain(monthlyShortUrlAccess.getLongUrlDomain());
            jsBean.setRedirectType(monthlyShortUrlAccess.getRedirectType());
            jsBean.setRefererDomain(monthlyShortUrlAccess.getRefererDomain());
            jsBean.setUserAgent(monthlyShortUrlAccess.getUserAgent());
            jsBean.setLanguage(monthlyShortUrlAccess.getLanguage());
            jsBean.setCountry(monthlyShortUrlAccess.getCountry());
            jsBean.setTalliedTime(monthlyShortUrlAccess.getTalliedTime());
            jsBean.setYear(monthlyShortUrlAccess.getYear());
            jsBean.setMonth(monthlyShortUrlAccess.getMonth());
            jsBean.setNumberOfDays(monthlyShortUrlAccess.getNumberOfDays());
            jsBean.setCreatedTime(monthlyShortUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(monthlyShortUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static MonthlyShortUrlAccess convertMonthlyShortUrlAccessJsBeanToBean(MonthlyShortUrlAccessJsBean jsBean)
    {
        MonthlyShortUrlAccessBean monthlyShortUrlAccess = null;
        if(jsBean != null) {
            monthlyShortUrlAccess = new MonthlyShortUrlAccessBean();
            monthlyShortUrlAccess.setGuid(jsBean.getGuid());
            monthlyShortUrlAccess.setTallyTime(jsBean.getTallyTime());
            monthlyShortUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            monthlyShortUrlAccess.setCount(jsBean.getCount());
            monthlyShortUrlAccess.setShortUrl(jsBean.getShortUrl());
            monthlyShortUrlAccess.setShortUrlDomain(jsBean.getShortUrlDomain());
            monthlyShortUrlAccess.setLongUrl(jsBean.getLongUrl());
            monthlyShortUrlAccess.setLongUrlDomain(jsBean.getLongUrlDomain());
            monthlyShortUrlAccess.setRedirectType(jsBean.getRedirectType());
            monthlyShortUrlAccess.setRefererDomain(jsBean.getRefererDomain());
            monthlyShortUrlAccess.setUserAgent(jsBean.getUserAgent());
            monthlyShortUrlAccess.setLanguage(jsBean.getLanguage());
            monthlyShortUrlAccess.setCountry(jsBean.getCountry());
            monthlyShortUrlAccess.setTalliedTime(jsBean.getTalliedTime());
            monthlyShortUrlAccess.setYear(jsBean.getYear());
            monthlyShortUrlAccess.setMonth(jsBean.getMonth());
            monthlyShortUrlAccess.setNumberOfDays(jsBean.getNumberOfDays());
            monthlyShortUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            monthlyShortUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return monthlyShortUrlAccess;
    }

}
