package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.GaeUserStruct;
import com.urltally.af.bean.GaeUserStructBean;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.GaeUserStructJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.GaeUserStructWebService;


// MockGaeUserStructWebService is a mock object.
// It can be used as a base class to mock GaeUserStructWebService objects.
public abstract class MockGaeUserStructWebService extends GaeUserStructWebService  // implements GaeUserStructService
{
    private static final Logger log = Logger.getLogger(MockGaeUserStructWebService.class.getName());
     

}
