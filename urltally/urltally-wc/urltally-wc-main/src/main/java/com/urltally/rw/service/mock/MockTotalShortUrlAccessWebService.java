package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalShortUrlAccess;
import com.urltally.af.bean.TotalShortUrlAccessBean;
import com.urltally.af.service.TotalShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.TotalShortUrlAccessJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.TotalShortUrlAccessWebService;


// MockTotalShortUrlAccessWebService is a mock object.
// It can be used as a base class to mock TotalShortUrlAccessWebService objects.
public abstract class MockTotalShortUrlAccessWebService extends TotalShortUrlAccessWebService  // implements TotalShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockTotalShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private TotalShortUrlAccessWebService mService = null;

    public MockTotalShortUrlAccessWebService()
    {
        this(MockServiceProxyFactory.getInstance().getTotalShortUrlAccessServiceProxy());
    }
    public MockTotalShortUrlAccessWebService(TotalShortUrlAccessService service)
    {
        super(service);
    }


}
