package com.urltally.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.TotalLongUrlAccess;
import com.urltally.af.bean.TotalLongUrlAccessBean;
import com.urltally.af.service.TotalLongUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.TotalLongUrlAccessJsBean;
import com.urltally.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TotalLongUrlAccessWebService // implements TotalLongUrlAccessService
{
    private static final Logger log = Logger.getLogger(TotalLongUrlAccessWebService.class.getName());
     
    // Af service interface.
    private TotalLongUrlAccessService mService = null;

    public TotalLongUrlAccessWebService()
    {
        this(ServiceProxyFactory.getInstance().getTotalLongUrlAccessServiceProxy());
    }
    public TotalLongUrlAccessWebService(TotalLongUrlAccessService service)
    {
        mService = service;
    }
    
    protected TotalLongUrlAccessService getServiceProxy()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTotalLongUrlAccessServiceProxy();
        }
        return mService;
    }
    // Dependency injection.
    protected void setServiceProxy(TotalLongUrlAccessService service)
    {
        mService = service;
    }
    
    
    public TotalLongUrlAccessJsBean getTotalLongUrlAccess(String guid) throws WebException
    {
        try {
            TotalLongUrlAccess totalLongUrlAccess = getServiceProxy().getTotalLongUrlAccess(guid);
            TotalLongUrlAccessJsBean bean = convertTotalLongUrlAccessToJsBean(totalLongUrlAccess);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTotalLongUrlAccess(String guid, String field) throws WebException
    {
        try {
            return getServiceProxy().getTotalLongUrlAccess(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TotalLongUrlAccessJsBean> getTotalLongUrlAccesses(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TotalLongUrlAccessJsBean> jsBeans = new ArrayList<TotalLongUrlAccessJsBean>();
            List<TotalLongUrlAccess> totalLongUrlAccesses = getServiceProxy().getTotalLongUrlAccesses(guids);
            if(totalLongUrlAccesses != null) {
                for(TotalLongUrlAccess totalLongUrlAccess : totalLongUrlAccesses) {
                    jsBeans.add(convertTotalLongUrlAccessToJsBean(totalLongUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TotalLongUrlAccessJsBean> getAllTotalLongUrlAccesses() throws WebException
    {
        return getAllTotalLongUrlAccesses(null, null, null);
    }

    // @Deprecated
    public List<TotalLongUrlAccessJsBean> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTotalLongUrlAccesses(ordering, offset, count, null);
    }

    public List<TotalLongUrlAccessJsBean> getAllTotalLongUrlAccesses(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TotalLongUrlAccessJsBean> jsBeans = new ArrayList<TotalLongUrlAccessJsBean>();
            List<TotalLongUrlAccess> totalLongUrlAccesses = getServiceProxy().getAllTotalLongUrlAccesses(ordering, offset, count, forwardCursor);
            if(totalLongUrlAccesses != null) {
                for(TotalLongUrlAccess totalLongUrlAccess : totalLongUrlAccesses) {
                    jsBeans.add(convertTotalLongUrlAccessToJsBean(totalLongUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTotalLongUrlAccessKeys(ordering, offset, count, null);
    }

    public List<String> getAllTotalLongUrlAccessKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().getAllTotalLongUrlAccessKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<TotalLongUrlAccessJsBean> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<TotalLongUrlAccessJsBean> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<TotalLongUrlAccessJsBean> findTotalLongUrlAccesses(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TotalLongUrlAccessJsBean> jsBeans = new ArrayList<TotalLongUrlAccessJsBean>();
            List<TotalLongUrlAccess> totalLongUrlAccesses = getServiceProxy().findTotalLongUrlAccesses(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(totalLongUrlAccesses != null) {
                for(TotalLongUrlAccess totalLongUrlAccess : totalLongUrlAccesses) {
                    jsBeans.add(convertTotalLongUrlAccessToJsBean(totalLongUrlAccess));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findTotalLongUrlAccessKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getServiceProxy().findTotalLongUrlAccessKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getServiceProxy().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTotalLongUrlAccess(String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws WebException
    {
        try {
            return getServiceProxy().createTotalLongUrlAccess(tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().createTotalLongUrlAccess(totalLongUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TotalLongUrlAccessJsBean constructTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            totalLongUrlAccess = getServiceProxy().constructTotalLongUrlAccess(totalLongUrlAccess);
            jsBean = convertTotalLongUrlAccessToJsBean(totalLongUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTotalLongUrlAccess(String guid, String tallyType, String tallyTime, Long tallyEpoch, Integer count, String longUrl, String longUrlDomain) throws WebException
    {
        try {
            return getServiceProxy().updateTotalLongUrlAccess(guid, tallyType, tallyTime, tallyEpoch, count, longUrl, longUrlDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().updateTotalLongUrlAccess(totalLongUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TotalLongUrlAccessJsBean refreshTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            totalLongUrlAccess = getServiceProxy().refreshTotalLongUrlAccess(totalLongUrlAccess);
            jsBean = convertTotalLongUrlAccessToJsBean(totalLongUrlAccess);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTotalLongUrlAccess(String guid) throws WebException
    {
        try {
            return getServiceProxy().deleteTotalLongUrlAccess(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTotalLongUrlAccess(TotalLongUrlAccessJsBean jsBean) throws WebException
    {
        try {
            TotalLongUrlAccess totalLongUrlAccess = convertTotalLongUrlAccessJsBeanToBean(jsBean);
            return getServiceProxy().deleteTotalLongUrlAccess(totalLongUrlAccess);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTotalLongUrlAccesses(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getServiceProxy().deleteTotalLongUrlAccesses(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TotalLongUrlAccessJsBean convertTotalLongUrlAccessToJsBean(TotalLongUrlAccess totalLongUrlAccess)
    {
        TotalLongUrlAccessJsBean jsBean = null;
        if(totalLongUrlAccess != null) {
            jsBean = new TotalLongUrlAccessJsBean();
            jsBean.setGuid(totalLongUrlAccess.getGuid());
            jsBean.setTallyType(totalLongUrlAccess.getTallyType());
            jsBean.setTallyTime(totalLongUrlAccess.getTallyTime());
            jsBean.setTallyEpoch(totalLongUrlAccess.getTallyEpoch());
            jsBean.setCount(totalLongUrlAccess.getCount());
            jsBean.setLongUrl(totalLongUrlAccess.getLongUrl());
            jsBean.setLongUrlDomain(totalLongUrlAccess.getLongUrlDomain());
            jsBean.setCreatedTime(totalLongUrlAccess.getCreatedTime());
            jsBean.setModifiedTime(totalLongUrlAccess.getModifiedTime());
        }
        return jsBean;
    }

    public static TotalLongUrlAccess convertTotalLongUrlAccessJsBeanToBean(TotalLongUrlAccessJsBean jsBean)
    {
        TotalLongUrlAccessBean totalLongUrlAccess = null;
        if(jsBean != null) {
            totalLongUrlAccess = new TotalLongUrlAccessBean();
            totalLongUrlAccess.setGuid(jsBean.getGuid());
            totalLongUrlAccess.setTallyType(jsBean.getTallyType());
            totalLongUrlAccess.setTallyTime(jsBean.getTallyTime());
            totalLongUrlAccess.setTallyEpoch(jsBean.getTallyEpoch());
            totalLongUrlAccess.setCount(jsBean.getCount());
            totalLongUrlAccess.setLongUrl(jsBean.getLongUrl());
            totalLongUrlAccess.setLongUrlDomain(jsBean.getLongUrlDomain());
            totalLongUrlAccess.setCreatedTime(jsBean.getCreatedTime());
            totalLongUrlAccess.setModifiedTime(jsBean.getModifiedTime());
        }
        return totalLongUrlAccess;
    }

}
