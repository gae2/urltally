package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.FiveTen;
import com.urltally.af.bean.FiveTenBean;
import com.urltally.af.service.FiveTenService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.FiveTenJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.FiveTenWebService;


// MockFiveTenWebService is a mock object.
// It can be used as a base class to mock FiveTenWebService objects.
public abstract class MockFiveTenWebService extends FiveTenWebService  // implements FiveTenService
{
    private static final Logger log = Logger.getLogger(MockFiveTenWebService.class.getName());
     
    // Af service interface.
    private FiveTenWebService mService = null;

    public MockFiveTenWebService()
    {
        this(MockServiceProxyFactory.getInstance().getFiveTenServiceProxy());
    }
    public MockFiveTenWebService(FiveTenService service)
    {
        super(service);
    }


}
