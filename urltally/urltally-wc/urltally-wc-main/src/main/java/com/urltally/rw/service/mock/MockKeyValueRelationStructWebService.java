package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.KeyValueRelationStruct;
import com.urltally.af.bean.KeyValueRelationStructBean;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.KeyValueRelationStructJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.KeyValueRelationStructWebService;


// MockKeyValueRelationStructWebService is a mock object.
// It can be used as a base class to mock KeyValueRelationStructWebService objects.
public abstract class MockKeyValueRelationStructWebService extends KeyValueRelationStructWebService  // implements KeyValueRelationStructService
{
    private static final Logger log = Logger.getLogger(MockKeyValueRelationStructWebService.class.getName());
     

}
