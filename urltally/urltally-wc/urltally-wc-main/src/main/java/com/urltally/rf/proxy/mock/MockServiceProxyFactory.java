package com.urltally.rf.proxy.mock;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.rf.proxy.ServiceProxyFactory;
import com.urltally.rf.proxy.UserServiceProxy;
import com.urltally.rf.proxy.AccessTallyMasterServiceProxy;
import com.urltally.rf.proxy.AccessTallyStatusServiceProxy;
import com.urltally.rf.proxy.MonthlyShortUrlAccessServiceProxy;
import com.urltally.rf.proxy.WeeklyShortUrlAccessServiceProxy;
import com.urltally.rf.proxy.DailyShortUrlAccessServiceProxy;
import com.urltally.rf.proxy.HourlyShortUrlAccessServiceProxy;
import com.urltally.rf.proxy.CumulativeShortUrlAccessServiceProxy;
import com.urltally.rf.proxy.CurrentShortUrlAccessServiceProxy;
import com.urltally.rf.proxy.TotalShortUrlAccessServiceProxy;
import com.urltally.rf.proxy.TotalLongUrlAccessServiceProxy;
import com.urltally.rf.proxy.ServiceInfoServiceProxy;
import com.urltally.rf.proxy.FiveTenServiceProxy;
import com.urltally.rf.proxy.ServiceProxyFactory;


// Create your own mock object factory using MockServiceProxyFactory as a template.
public class MockServiceProxyFactory extends ServiceProxyFactory
{
    private static final Logger log = Logger.getLogger(MockServiceProxyFactory.class.getName());

    private UserServiceProxy userService = null;
    private AccessTallyMasterServiceProxy accessTallyMasterService = null;
    private AccessTallyStatusServiceProxy accessTallyStatusService = null;
    private MonthlyShortUrlAccessServiceProxy monthlyShortUrlAccessService = null;
    private WeeklyShortUrlAccessServiceProxy weeklyShortUrlAccessService = null;
    private DailyShortUrlAccessServiceProxy dailyShortUrlAccessService = null;
    private HourlyShortUrlAccessServiceProxy hourlyShortUrlAccessService = null;
    private CumulativeShortUrlAccessServiceProxy cumulativeShortUrlAccessService = null;
    private CurrentShortUrlAccessServiceProxy currentShortUrlAccessService = null;
    private TotalShortUrlAccessServiceProxy totalShortUrlAccessService = null;
    private TotalLongUrlAccessServiceProxy totalLongUrlAccessService = null;
    private ServiceInfoServiceProxy serviceInfoService = null;
    private FiveTenServiceProxy fiveTenService = null;

    // Using the Decorator pattern.
    private ServiceProxyFactory decoratedProxyFactory;
    private MockServiceProxyFactory()
    {
        this(null);   // ????
    }
    private MockServiceProxyFactory(ServiceProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }

    // Initialization-on-demand holder.
    private static class MockServiceProxyFactoryHolder
    {
        private static final MockServiceProxyFactory INSTANCE = new MockServiceProxyFactory();
    }

    // Singleton method
    public static MockServiceProxyFactory getInstance()
    {
        return MockServiceProxyFactoryHolder.INSTANCE;
    }

    // DI setter injector.
    public ServiceProxyFactory getDecoratedProxyFactory()
    {
        return decoratedProxyFactory;
    }
    public void setDecoratedProxyFactory(ServiceProxyFactory decoratedProxyFactory)
    {
        this.decoratedProxyFactory = decoratedProxyFactory;
    }


    public UserServiceProxy getUserServiceProxy()
    {
        if(userService == null) {
            userService = new MockUserServiceProxy(decoratedProxyFactory.getUserServiceProxy()) {};
        }
        return userService;
    }

    public AccessTallyMasterServiceProxy getAccessTallyMasterServiceProxy()
    {
        if(accessTallyMasterService == null) {
            accessTallyMasterService = new MockAccessTallyMasterServiceProxy(decoratedProxyFactory.getAccessTallyMasterServiceProxy()) {};
        }
        return accessTallyMasterService;
    }

    public AccessTallyStatusServiceProxy getAccessTallyStatusServiceProxy()
    {
        if(accessTallyStatusService == null) {
            accessTallyStatusService = new MockAccessTallyStatusServiceProxy(decoratedProxyFactory.getAccessTallyStatusServiceProxy()) {};
        }
        return accessTallyStatusService;
    }

    public MonthlyShortUrlAccessServiceProxy getMonthlyShortUrlAccessServiceProxy()
    {
        if(monthlyShortUrlAccessService == null) {
            monthlyShortUrlAccessService = new MockMonthlyShortUrlAccessServiceProxy(decoratedProxyFactory.getMonthlyShortUrlAccessServiceProxy()) {};
        }
        return monthlyShortUrlAccessService;
    }

    public WeeklyShortUrlAccessServiceProxy getWeeklyShortUrlAccessServiceProxy()
    {
        if(weeklyShortUrlAccessService == null) {
            weeklyShortUrlAccessService = new MockWeeklyShortUrlAccessServiceProxy(decoratedProxyFactory.getWeeklyShortUrlAccessServiceProxy()) {};
        }
        return weeklyShortUrlAccessService;
    }

    public DailyShortUrlAccessServiceProxy getDailyShortUrlAccessServiceProxy()
    {
        if(dailyShortUrlAccessService == null) {
            dailyShortUrlAccessService = new MockDailyShortUrlAccessServiceProxy(decoratedProxyFactory.getDailyShortUrlAccessServiceProxy()) {};
        }
        return dailyShortUrlAccessService;
    }

    public HourlyShortUrlAccessServiceProxy getHourlyShortUrlAccessServiceProxy()
    {
        if(hourlyShortUrlAccessService == null) {
            hourlyShortUrlAccessService = new MockHourlyShortUrlAccessServiceProxy(decoratedProxyFactory.getHourlyShortUrlAccessServiceProxy()) {};
        }
        return hourlyShortUrlAccessService;
    }

    public CumulativeShortUrlAccessServiceProxy getCumulativeShortUrlAccessServiceProxy()
    {
        if(cumulativeShortUrlAccessService == null) {
            cumulativeShortUrlAccessService = new MockCumulativeShortUrlAccessServiceProxy(decoratedProxyFactory.getCumulativeShortUrlAccessServiceProxy()) {};
        }
        return cumulativeShortUrlAccessService;
    }

    public CurrentShortUrlAccessServiceProxy getCurrentShortUrlAccessServiceProxy()
    {
        if(currentShortUrlAccessService == null) {
            currentShortUrlAccessService = new MockCurrentShortUrlAccessServiceProxy(decoratedProxyFactory.getCurrentShortUrlAccessServiceProxy()) {};
        }
        return currentShortUrlAccessService;
    }

    public TotalShortUrlAccessServiceProxy getTotalShortUrlAccessServiceProxy()
    {
        if(totalShortUrlAccessService == null) {
            totalShortUrlAccessService = new MockTotalShortUrlAccessServiceProxy(decoratedProxyFactory.getTotalShortUrlAccessServiceProxy()) {};
        }
        return totalShortUrlAccessService;
    }

    public TotalLongUrlAccessServiceProxy getTotalLongUrlAccessServiceProxy()
    {
        if(totalLongUrlAccessService == null) {
            totalLongUrlAccessService = new MockTotalLongUrlAccessServiceProxy(decoratedProxyFactory.getTotalLongUrlAccessServiceProxy()) {};
        }
        return totalLongUrlAccessService;
    }

    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new MockServiceInfoServiceProxy(decoratedProxyFactory.getServiceInfoServiceProxy()) {};
        }
        return serviceInfoService;
    }

    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenService == null) {
            fiveTenService = new MockFiveTenServiceProxy(decoratedProxyFactory.getFiveTenServiceProxy()) {};
        }
        return fiveTenService;
    }

}
