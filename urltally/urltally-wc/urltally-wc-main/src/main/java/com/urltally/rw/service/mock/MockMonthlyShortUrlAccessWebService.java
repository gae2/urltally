package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.MonthlyShortUrlAccess;
import com.urltally.af.bean.MonthlyShortUrlAccessBean;
import com.urltally.af.service.MonthlyShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.MonthlyShortUrlAccessJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.MonthlyShortUrlAccessWebService;


// MockMonthlyShortUrlAccessWebService is a mock object.
// It can be used as a base class to mock MonthlyShortUrlAccessWebService objects.
public abstract class MockMonthlyShortUrlAccessWebService extends MonthlyShortUrlAccessWebService  // implements MonthlyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockMonthlyShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private MonthlyShortUrlAccessWebService mService = null;

    public MockMonthlyShortUrlAccessWebService()
    {
        this(MockServiceProxyFactory.getInstance().getMonthlyShortUrlAccessServiceProxy());
    }
    public MockMonthlyShortUrlAccessWebService(MonthlyShortUrlAccessService service)
    {
        super(service);
    }


}
