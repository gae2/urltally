package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.AccessTallyMaster;
import com.urltally.af.bean.AccessTallyMasterBean;
import com.urltally.af.service.AccessTallyMasterService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.AccessTallyMasterJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.AccessTallyMasterWebService;


// MockAccessTallyMasterWebService is a mock object.
// It can be used as a base class to mock AccessTallyMasterWebService objects.
public abstract class MockAccessTallyMasterWebService extends AccessTallyMasterWebService  // implements AccessTallyMasterService
{
    private static final Logger log = Logger.getLogger(MockAccessTallyMasterWebService.class.getName());
     
    // Af service interface.
    private AccessTallyMasterWebService mService = null;

    public MockAccessTallyMasterWebService()
    {
        this(MockServiceProxyFactory.getInstance().getAccessTallyMasterServiceProxy());
    }
    public MockAccessTallyMasterWebService(AccessTallyMasterService service)
    {
        super(service);
    }


}
