package com.urltally.rw.service.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.urltally.ws.BaseException;
import com.urltally.ws.core.StringCursor;
import com.urltally.ws.WeeklyShortUrlAccess;
import com.urltally.af.bean.WeeklyShortUrlAccessBean;
import com.urltally.af.service.WeeklyShortUrlAccessService;
import com.urltally.fe.WebException;
import com.urltally.fe.bean.WeeklyShortUrlAccessJsBean;
import com.urltally.rf.proxy.mock.MockServiceProxyFactory;
import com.urltally.rw.service.WeeklyShortUrlAccessWebService;


// MockWeeklyShortUrlAccessWebService is a mock object.
// It can be used as a base class to mock WeeklyShortUrlAccessWebService objects.
public abstract class MockWeeklyShortUrlAccessWebService extends WeeklyShortUrlAccessWebService  // implements WeeklyShortUrlAccessService
{
    private static final Logger log = Logger.getLogger(MockWeeklyShortUrlAccessWebService.class.getName());
     
    // Af service interface.
    private WeeklyShortUrlAccessWebService mService = null;

    public MockWeeklyShortUrlAccessWebService()
    {
        this(MockServiceProxyFactory.getInstance().getWeeklyShortUrlAccessServiceProxy());
    }
    public MockWeeklyShortUrlAccessWebService(WeeklyShortUrlAccessService service)
    {
        super(service);
    }


}
